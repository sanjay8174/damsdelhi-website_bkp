<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>
<?php
$course_id = 3;
$courseNav_id = 9;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
?>
<body class="inner-bg">
<?php include 'registration.php';?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mds-quest-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest">MDS Quest</a></li>
          <li><a title="MDS Dental Regular Course" class="active-link">MDS Dental Regular Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Dental Regular Course <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> 
                 <span class="display_block">
                  <img width="100%" src="images/mds-banner.png" title="MDS Quest" alt="MDS Quest" />
                 </span>
                  <div class="test-series-content paddin-zero">
                    <ul class="duration-content">
                      <li>
                        <label>Price :</label>
                        <span class="price_font">40450/- 
                         <span class="including_taxx">(including s.tax)</span>
                        </span>
                      </li>
                    </ul>
                  </div>
                  <p>This is the premier course offered by DAMS, with detailed classes on both medical and dental subjects in DAMS Delhi classroom. Our main asset for this course is our faculty which is a mix of young MDS topper and experienced faculty members who will mentor you to success in MDS entrance examination.</p>
                  <p>The institute's aim is not only to provide specific knowledge and strengthen the foundation of the students in PG Medical Entrance, but also to infuse them with determination to crack the Entrance Exams at post graduation level. To explore the potential of the students and to help them master the subject, We, at DAMS have developed extensive scientific teaching as well as testing methods. We also have special sessions on mental training required to develop the so called “killer instinct”.<br>
                    <span class="price_font">Dental Career Counselling for MDS :</span> 09999158131, 09999322163 </p>
                </div>
                <aside class="how-to-apply paddin-zero">
                  <div class="how-to-apply-heading"><span></span> Course Highlights :-</div>
                  <div class="course-detail-content"> <span class="gry-course-box">Most popular course amongst students with comprehensive coverage of ALL SUBJECTS. We are the only medical coaching institute which teaches all subjects in manner that is required for your MDS Entrance examination. This course starts in January and we have a fresh batch every month. Classes are held on weekends and are based on advanced audio-visual learning modules. We are already the number 1 coaching institute for the PG medical entrance examinations AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr.Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for All India MDS exam , AIIMS MDS PG entrance by our sincere effort.</span> <span class="blue-course-box">We admit only limited number of students in each batch. You will receive excellent individual attention. This is a big advantage to you as you can learn better in a small class with limited students. Our class rooms are air conditioned and have state of the art audiovisual facilities. </span> <span class="gry-course-box">You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can. They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books and often such questions are asked in advanced examinations like AIIMS, AIPG, etc.</span> <span class="blue-course-box">We provide detailed and easy to understand notes. Our notes are based on standard text and are as good as your text books in terms of authenticity and are extremely concise and easy to read. They are like concentrated protein mix required for your PG Medical Entrance Examination. </span> <span class="blue-course-box">We conduct periodic tests, evaluate your performance and conduct counselling sessions. This will help you in your regular preparation for your exams.</span> </div>
                </aside>
              </div>
            </article>
            <div class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>