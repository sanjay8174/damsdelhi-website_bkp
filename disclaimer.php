<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article>
<aside class="banner-left banner-left-postion">
<h2>Be smart &amp;<br>take your future in Your Hand </h2>
<h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
who otherwise couldn't take the advantage of the DAMS teaching<br>because of various reasons like non-availability of DAMS centre<br>in the vicinity would be greatly benefited.</h3>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>Disclaimer</h4>
<article class="showme-main">
<div class="privacy-content">
<span>You expressly understand and agree that :-</span>
<p>Your use of the service is at your sole risk. The service is provided on an "as is" and "as available" basis. <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose and non-infringement.</p><p><a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> makes no warranty that
The service will meet your requirements,
The service will be uninterrupted, timely, secure, or error-free,
The results that may be obtained from the use of the service will be accurate or reliable,
The quality of any products, services, information, or other material purchased or obtained by you through the service will meet your expectations, and
Any errors in the software will be corrected.</p><p>Any material downloaded or otherwise obtained through the use of the service is done at your own discretion and risk, and you will be solely responsible for any damage to your computer system or loss of data that results from the download of any such material. No advice or information, whether oral or written, obtained by you from <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> or through or from the service will create any warranty not expressly stated in the terms.</p><ul class="privacy-list"><li><label>Registration / Information :</label>
<p>When you sign up for <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> we ask you for personal information. We may combine the information you submit under your account with information from other services / third parties in order to provide you with a better experience and to improve the quality of our services. For certain services, we may give you the opportunity to opt out of combining such information.
You may provide us with certain information such as your Name, E-mail address, Correspondence address when registering for certain services such as Online Registration / Submit Resume, Contests. This information will primarily be used for the purpose of providing personalization and verification.</p></li><li><label>Cookies :</label><p>A cookie is a small data file that certain websites write to your hard drive when you visit them. A cookie file can contain information such as a user ID that the site uses to track the pages you have visited. A cookie can contain information you supply yourself. A cookie can't read data of your hard disk or read cookie files created by other sites. <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> uses cookies to track user traffic patterns and for the personalization feature.</p></li><li><label>User Communications :</label>
<p>When you send email or other communications to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> , we may retain those communications in order to process your inquiries, respond to your requests and improve our services. When you send and receive SMS messages to or from one of our services that provides SMS functionality, we may collect and maintain information associated with those messages, such as the phone number, the content of the message, and the date and time of the transaction. We may use your email address to communicate with you about our services.</p></li>
<li><label>Log Information :</label><p>When you access <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> services via a browser, application or other client our servers automatically record certain information. These server logs may include information such as your web request, your interaction with a service, Internet Protocol address, browser type, browser language, the date and time of your request and one or more cookies that may uniquely identify your browser or your account.</p></li><li><label>Electronic Newsletter/E-mail :</label><p><a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> offers a free electronic newsletter to its users. We gather the e-mail addresses of users who voluntarily subscribe. Users may remove themselves from this mailing list by using the link provided in every newsletter.</p></li><li><label>Confidential :</label><p>DAMS Privacy Policy applies to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> services only. We do not exercise control over the sites displayed as search results, sites that include other applications, products or services, or links from within our various services. Personal information that you provide to other sites may be sent to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> in order to deliver the service. We process such information under this Privacy Policy.</p></li><li><label>Feedback :</label><p>Our site's Feedback Form requires contact information of users like their name and e-mail address and demographic information like their zip code, age etc. for better services.</p></li><li><label>Further Improvement :</label><p>Apart from the above, we may use the information to provide, maintain, protect and improve our services and develop new services.</p></li><li><label>Queries regarding the Website :</label><p>If you have any questions about the practices of this site or your dealings with this website, regarding DAMS Privacy Policy, contact DAMS Corporate office <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a>.</p></li><li><label>Electronic Newsletter/E-mail :</label><p><a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> offers a free electronic newsletter to its users. We gather the e-mail addresses of users who voluntarily subscribe. Users may remove themselves from this mailing list by using the link provided in every newsletter.</p></li></ul></div></article>

<span style="display: inline-block; width: 100%;" display:inline-block;="">
            <h1 style=" margin: 0px;color: #494949;font-size: 18px; font-family: &quot;pt_sansbold&quot;; font-weight: 400;">Student Information</h1>
            <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; float: left; margin: 10px 0px; line-height: 20px; font-family: &quot;pt_sansregular&quot;;">Students please insist on seeking a fees receipt for every payment. DAMS genuine fees receipts can only be in the following format:</p>
        </span>
        <span style="display: inline-block; width: 100%;">
            <h1 style=" margin: 0px;color: #494949;font-size: 18px; width: 100%; font-family: &quot;pt_sansbold&quot;; font-weight: 400;">DAMS ERP generated fee receipt:</h1>
            <img src="images/feereceipt.jpg" style="float: left; right: 25px; margin: 10px 0px 0px 0px; width: 100%;">
            </span>
        <span style="display: inline-block; margin: 10px 0px 0px 0px; width: 100%;">
        <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; line-height: 20px; font-family: &quot;pt_sansregular&quot;">Student please be informed that any receipt other than in the ERP generated format and generated from DAMS ERP software is not valid and shall not be considered in case of any disputes or for any discounts and other benefits from DAMS in future.

Also, after your enrolment please login to cloud.damsdelhi.com and check if your profile and personal details are correctly entered in our database.

</p>
           <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; line-height: 22px;margin-top:15px;">
Please note as a policy DAMS encourages students to pay using demand draft/pay in favour of Delhi Academy of medical sciences Pvt Ltd, DAMS Sky Pvt Ltd  or DAMS Dental Pvt. Ltd. whichever applicable. </p>
       
           <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; line-height: 22px;margin-top:15px;">DAMS is only providing Commercial Coaching to student.<br>
               It is advised to consult a legal professional/lawyer/CA before <b>taking any input credit of GST.</b><br>
GST invoice to student/student parent firm/student associate firm is issued only on their specific request.<br>
It is advised to consult a legal professional/lawyer/CA before taking any <b>80C Deduction under the Income Tax Act.</b> </p>
        </span>
</div></aside>
<aside class="gallery-right"> 
<?php include 'enquiryform.php'; ?>
</aside>
</section></div></div></section>

<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>