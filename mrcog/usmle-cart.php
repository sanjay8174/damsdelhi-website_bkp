<?php 
header('Location: https://www.damspublications.com/');exit;?>

<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Book Store</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
		
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<?php include 'enquiry.php'; ?>

<?php include '../cart-header.php'; ?>


<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article>

<aside class="banner-left">
<h2>USMLE EDGE</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<?php include'usmle-banner-btn.php'; ?>

</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
<ul>
<li class="bg_none"><a href="usmle-edge.php" title="USMLE Edge">USMLE Edge</a></li>
<!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="DAMS Store">DAMS Store</a></li>-->
<li><a href="http://damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>
<!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="Publications">Publications</a></li>-->
<li><a href="http://damspublications.com/" target="_blank" title="Publications">Publications</a></li>
<li><a title="Add To Cart" href="usmle-add-to-cart.php" >Add To Cart</a></li>
<li><a title="Cart" class="active-link">Cart</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<article class="showme-cart-main">

<link rel="stylesheet" type="text/css" href="https://cdn.webrupee.com/font" />
<script src="https://cdn.webrupee.com/js" type="text/javascript"></script> 


<aside class="cart-main">
<div class="cart-top">
<span class="cart-one">Item Name</span>
<span class="cart-two">Qty.</span>
<span class="cart-three">Unit</span>
<span class="cart-four">Total</span>
<span class="cart-five">&nbsp;</span>
</div>

<div class="cart-contener">
<div class="cart-content white-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
<div class="cart-content gry-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
<div class="cart-content white-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
</div>

<div class="cart-subtotal">
<span class="cart-left">Total Items : 3</span>
<span class="cart-right">Sub Total : <b>&#8377;</b> 321.00</span>
</div>

<div class="cart-buttons">

<div class="submit-enquiry"><a href="#" title="Checkout">Checkout</a></div>
<!--<div class="submit-enquiry" style="margin-left:10px"><a href="https://damsdelhi.com/dams-publication.php?c=4" title="Continue"><span></span>Continue</a></div>-->
<div class="submit-enquiry" style="margin-left:10px"><a href="http://damspublications.com/" target="_blank" title="Continue"><span></span>Continue</a></div>


</div>

</aside>


<!--<aside class="addcart-main">
<div class="addcart-left"><img src="images/book2.jpg" alt="book" title="book" /></div>
<div class="addcart-right">
<h6>AIIMS MDS Examination with Solution <br />
NOV. (2013)</h6>
<div class="product-code">
<ul>
<li><label>Product Code :</label><span>Product 15</span></li>
<li><label>Reward Points :</label><span>100</span></li>
<li><label>Availability :</label><span>In Stock</span></li>
</ul></div>

<div class="product-code">
<ul>
<li><label>Price :</label><span>
<b><i class="WebRupee">&#8377;</i> 119.50 </b>
<i class="WebRupee">&#8377;</i> 107.75</span></li>
<li><label>Ex Tax :</label><span>100</span></li>
<li><p>Price in reward points: 400</p></li>
</ul></div>
</div>
</aside> -->
<!--<aside class="description-section">
<div class="description-heading">
<div class="add-cart-button"><a href="#" title="Add To Cart">Add To Cart</a></div> 
<span>Description</span>
</div>

<div class="description-content">
<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae auctor elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.</p> 
<p>Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
</div>

</aside> -->

</article>
</div>

</aside>

<aside class="gallery-right">

<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->

</aside>
</section>
</div>
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css"> 
<script src="../js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->

</body></html>