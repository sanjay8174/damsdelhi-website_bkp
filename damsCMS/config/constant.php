<?php
class Constant
{
    public static $loginLink="onlinetest.damsdelhi.com";
    /** used in dams-publication,achieveajax,studentInterview and download.php **/
    /*for local
    public static $path = "..";
    public static $pathCMS = "/damswebadmin";*/

    /* for UAT 
    public static $path = "https://damsdelhi.com/demo";
    public static $pathCMS = "https://damsdelhi.com/demo/damswebadmin"; */
    /* for live */
//    public static $path = "https://damsdelhi.com";
    public static $path = "https://www.damsdelhi.com/uat_dams/";
    public static $pathCMS = "https://damsdelhi.com/damsCMS"; 
}
?>
