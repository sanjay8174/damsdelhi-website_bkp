 function htmlDecode(value) {
    if (value) {
        var newval=$('<p />').html(value).text();
        return newval;

    } else {
        return '';
    }
}

function replacePercentile22(value){
    if (value){
    return value.replace("%22",'"');

    }else
        return "";

}
function clearAll(){

 $(' .uniform div span').attr('class','');
}

//getDataAjax function for json get and send made by Shobhit Garg

        var getDataAjax = function(url,type){
            this.url = url;
            this.type = type;
            this.getData = function(str){
                //alert(str);
                var resultData;
                var dataToPass = "data="+str;
                $.ajax({
                    url : this.url,
                    data : dataToPass,
                    type : 'post',
                    cache : false,
                    async : false,
                    traditional: true,
                    dataType: type,
                    success : function(result){
//                        alert(result);
                        resultData = result;
                    },
                    error : function(result){
//                        alert(result);
                    }
                });
                return resultData;
            }

            this.convertToJson = function(str){
                var jsonStr = JSON.stringify(str);
                return jsonStr;
            }

            this.convertToArray = function(str){
                var jsonStr = JSON.parse(str);
                return jsonStr;
            }

        }


         var get = function(arr,url,type){
                   var obj = new getDataAjax(url,type);
                   var convJsonData = obj.convertToJson(arr);
                   var gotData = obj.getData(convJsonData);
                   return gotData;
         }


/********* Company START *******/

         var companyData = function(){

                         this.getDataAll = function(){



                                var company = {};
                                 company['mode'] = "getAllCompany";
                                 var companyDataRec = get(company,'index.php?p=5','json');
                                 if(companyDataRec.total>0){
                                     var str = "";
                                     $('#companyData').html('');
                                     for(var i=0;i<companyDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(companyDataRec.name[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(companyDataRec.title[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(companyDataRec.urlLink[i]))+ '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(companyDataRec.helplineNumber[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(companyDataRec.smsEnquiry[i])) + '</td>';
                                        if(companyDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callCompanyData.editData('+companyDataRec.id[i]+',\''+escape(companyDataRec.name[i])+'\',\''+escape(companyDataRec.title[i])+'\',\''+escape(companyDataRec.urlLink[i])+'\',\''+escape(companyDataRec.helplineNumber[i])+'\',\''+companyDataRec.smsEnquiry[i]+'\',\''+companyDataRec.active[i]+'\');"></i>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callCompanyData.deleteData('+companyDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';
                                        $('#companyData').append(str);
                                     }
                                 }else{
                                     $('#companyData').html("No Content Found.");
                                 }




                         }

                         this.saveData = function(){
                             var company = {};
                             var companyDataRec;
                            if($('#modeCompany').val() == 1){

                                     company['mode'] = "saveCompany";
                                     company['data'] = {};
                                     company['data']['name'] = escape(encodeURIComponent($('#companyField1').val()));
                                     company['data']['title'] = escape(encodeURIComponent($('#companyField2').val()));
                                     company['data']['urlLink'] = escape(encodeURIComponent($('#companyField3').val()));
                                     company['data']['helplineNumber'] = escape(encodeURIComponent($('#companyField4').val()));
                                     company['data']['smsEnquiry'] = escape(encodeURIComponent($('#companyField5').val()));
                                     company['data']['active'] = ($('#companyField6').is(':checked')) ? 1 : 0;
                                     if(company['data']['name']!=''){
                                            companyDataRec = get(company,'index.php?p=5','json');
                                     }
                                     else{
                                         alert("Please provide Company name");
                                     }
                                     if(companyDataRec.total>0){
                                        alert(companyDataRec.total+" Record inserted successfully.");
                                        $('#cancelCompanyData').click();
                                        this.getDataAll();
                                     }else{
                                        alert("No record inserted.");
                                     }
                              }

                               if($('#modeCompany').val() == 2){

                                     company['mode'] = "updateCompany";
                                     company['data'] = {};
                                     company['data']['name'] = escape(encodeURIComponent($('#companyField1').val()));  //for name
                                     company['data']['title'] = escape(encodeURIComponent($('#companyField2').val()));  //for title
                                     company['data']['urlLink'] = escape(encodeURIComponent($('#companyField3').val()));  //for urlLink
                                     company['data']['helplineNumber'] = escape(encodeURIComponent($('#companyField4').val()));  //for helpline
                                     company['data']['smsEnquiry'] = escape(encodeURIComponent($('#companyField5').val()));  //for sms enquiry
                                     company['data']['active'] = ($('#companyField6').is(':checked')) ? 1 : 0;
                                     company['data']['id'] = $('#idCompany').val();  //for id
                                     if(company['data']['name']!=''){
                                        companyDataRec = get(company,'index.php?p=5','json');
                                     }
                                     else{
                                         alert("Please provide Company name");
                                     }
                                     if(companyDataRec.total>0){
                                        alert(companyDataRec.total+" Record Updated successfully.");
                                        $('#cancelCompanyData').click();
                                        this.getDataAll();
                                 }else{
                                        alert("No record inserted.");
                                     }
                              }


                         }
                         $('#cancelCompanyData').click(function(){
                            $('#modeCompany').val(1);
                            $('#idCompany').val("");
                            $('#editComp').html("Add");
                            $('#companyField6').val('');
                            clearAll();
                        });
                         this.editData = function(id,name,title,urlLink,helplineNumber,smsEnquiry,active){
                             $('#editComp').html("Edit");
                             $('#modeCompany').val(2);
                             $('#idCompany').val(id);
                             $('#companyField1').val(unescape(decodeURIComponent(name)));
                             $('#companyField2').val(unescape(decodeURIComponent(title)));
                             $('#companyField3').val(unescape(decodeURIComponent(urlLink)));
                             $('#companyField4').val(unescape(decodeURIComponent(helplineNumber)));
                             $('#companyField5').val(unescape(decodeURIComponent(smsEnquiry)));

                             if(active == 1){

                                $('#uniform-companyField6 span').attr('class','checked');
                                $('#companyField6').attr('checked','checked');
                            }else{
                                $('#uniform-companyField6 span').attr('class','');
                                $('#companyField6').removeAttr('checked');
                            }


                         }

                         this.deleteData = function(id){
                             var company = {};
                             company['mode'] = "deleteCompany";
                             company['data'] = {};
                             company['data']['id'] = id;
                                var r=confirm("Are you sure you want to delete this record?");
                                if(r==true){
                                       var companyDataRec = get(company,'index.php?p=5','json');
                                       if(companyDataRec.total>0){
                                            alert(companyDataRec.total+" Record Deleted successfully.");
                                            this.getDataAll();
                                       }else{
                                            alert("No record inserted.");
                                       }
                                }
                         }
         }
/********* Company END *******/

/*****START: Home -> Course ***************************************************/
         var courseData = function(){

                         this.getDataAll = function(){



                                var course = {};
                                 course['mode'] = "getAllCourse";

                                 var courseDataRec = get(course,'index.php?p=5','json');

                                 if(courseDataRec.total>0){

                                     var str = "";
                                     $('#courseData').html('');
                                     for(var i=0;i<courseDataRec.total;i++){

                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.courseName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.courseOrder[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.urlLink[i])) + '</td>';



                                        //str = str + '<td>' + '<img width=16% height=8%  src= "images/course/' + courseDataRec.id[i]+'.'+courseDataRec.imageUploadArray[i][1]+ '" />' + '</td>';
                                        //str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.subCourseTitle[i])) + '</td>';

                                        if(courseDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=11&ln=2&t=6&e=2&id='+courseDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callCourseData.deleteData('+courseDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#courseData').append(str);
                                     }
                                 }else{
                                     $('#courseData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

                         this.saveData = function(){

                             var course = {};
                             var courseDataRec;

                            if($('#modeCourse').val() == 1){

                                     course['mode'] = "saveCourse";
                                     course['data'] = {};
                                     course['data']['courseName'] = escape(encodeURIComponent(($('#courseField1').val())));  //for name
                                     course['data']['courseOrder'] = $('#courseField2').val();  //for order
                                     course['data']['urlLink'] =    escape(encodeURIComponent($('#courseField3').val()));  //for urlLink
                                     course['data']['imageUpload'] = $('#courseField4').val();  //for imageupload
                                     course['data']['contentHome']=$('#courseField5 textarea').val();
            //                       course['data']['contentHome']= course['data']['contentHome'].replace(/(<div>|<\/div>|<p>|<\/p>)/g,"");
                                     course['data']['contentHome']=escape(encodeURIComponent(course['data']['contentHome']));
            //                       course['data']['contentHome']= course['data']['contentHome'].replace(/(%0A|%09)/g,"");


                                     //should be get data for text editor1
                                     course['data']['content'] = $('#courseField6 textarea').val();  //should be get data for text editor2
            //                       course['data']['content']= course['data']['content'].replace(/(<div>|<\/div>|<p>|<\/p>)/g,"");
                                     course['data']['content']=escape(encodeURIComponent(course['data']['content']));
            //                       course['data']['content']= course['data']['content'].replace(/(%0A|%09)/g,"");
                                     course['data']['subCourseTitle'] = escape(encodeURIComponent(($('#courseField7').val())));
                                     course['data']['courseNavigation'] = ($('#uniform-courseField8 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['headerBlock'] = ($('#uniform-courseField9 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['courseBlock'] = ($('#uniform-courseField10 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['subCourseBlock'] = ($('#uniform-courseField11 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['eventGalleryBlock'] = ($('#uniform-courseField12 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['newsAndUpdateBlock'] = ($('#uniform-courseField13 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['videoBlock'] = ($('#uniform-courseField14 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['active'] = ($('#uniform-courseField15 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     
                                     if (course['data']['courseName']==''){
                                            alert("Please provide Course Name");
                                     }
                                     else if(course['data']['urlLink']==''){
                                       alert("Please provide Url Link");
                                     }
                                     else{
                                         courseDataRec = get(course,'index.php?p=5','json');
                                     }
                                     if(courseDataRec.total>0){
                                                var lastInsertedId =courseDataRec.lastInsertedId;
                                                if ( $("#courseField4").val()!=""){
                                                    var file_data = $('#courseField4').prop('files')[0];
                                                    var form_data = new FormData();
                                                    form_data.append('file', file_data);
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "index.php?p=6&m=c&folder=course&lastInsertedId="+lastInsertedId,
                                                        data: form_data,
                                                        async: false,
                                                        processData: false,
                                                        contentType: false,
                                                        error: function (res) {
                                                            alert("Unsuccessful.");
                                                            return false;
                                                        },
                                                        success: function (res) {
                                                            //alert(res);
                                                        }
                                                    });

                                               }
                                                alert(courseDataRec.total+" Record inserted successfully.");
                                               // location.href='index.php?p=1&ln=1&t=1';
                                                $('#uniform-courseField4 span.filename').html("");
                                                $('#courseField5 textarea').val("");
                                                $('#courseField6 textarea').val("");
                                                $('#cancelCourseData').click();

                                                this.getDataAll();
                                     }else{
                                        alert("No record inserted.");
                                     }
                              }


                               if($('#modeCourse').val() == 2){

                                     course['mode'] = "updateCourse";
                                     course['data'] = {};
                                     course['data']['courseName'] = escape(encodeURIComponent(($('#courseField1').val())));  //for name
                                     course['data']['courseOrder'] = $('#courseField2').val();  //for order
                                     course['data']['urlLink'] =    escape(encodeURIComponent($('#courseField3').val()));  //for urlLink
                                     if($('#courseField4').val()== ''){
                                      course['data']['imageUpload'] = $('#courseFEdit4').val();  //for imageupload
            }                           else {
                                      course['data']['imageUpload'] = $('#courseField4').val();  //for imageupload
            }
//                                     course['data']['imageUpload'] = $('#courseField4').val();  //for imageupload
                                     course['data']['contentHome']=$('#courseField5 textarea').val();
            //                       course['data']['contentHome']= course['data']['contentHome'].replace(/(<div>|<\/div>|<p>|<\/p>)/g,"");
                                     course['data']['contentHome']=escape(encodeURIComponent(course['data']['contentHome']));
            //                       course['data']['contentHome']= course['data']['contentHome'].replace(/(%0A|%09)/g,"");       //should be get data for text editor1
                                     course['data']['content'] = $('#courseField6 textarea').val();  //should be get data for text editor2
            //                       course['data']['content']= course['data']['content'].replace(/(<div>|<\/div>|<p>|<\/p>)/g,"");
                                     course['data']['content']=escape(encodeURIComponent(course['data']['content']));
            //                       course['data']['content']= course['data']['content'].replace(/(%0A|%09)/g,"");
                                     course['data']['subCourseTitle'] = escape(encodeURIComponent(($('#courseField7').val())));
                                     course['data']['courseNavigation'] = ($('#uniform-courseField8 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['headerBlock'] = ($('#uniform-courseField9 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['courseBlock'] = ($('#uniform-courseField10 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['subCourseBlock'] = ($('#uniform-courseField11 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['eventGalleryBlock'] = ($('#uniform-courseField12 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['newsAndUpdateBlock'] = ($('#uniform-courseField13 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['videoBlock'] = ($('#uniform-courseField14 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['active'] = ($('#uniform-courseField15 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     course['data']['id'] = $('#idCourse').val();  //for id
                                     if (course['data']['courseName']==''){
                                            alert("Please provide Course Name");
                                     }
                                     else if(course['data']['urlLink']==''){
                                       alert("Please provide Url Link");
                                     }
                                     else{
                                         courseDataRec = get(course,'index.php?p=5','json');
                                     }
                                     if(courseDataRec.total>0){
                                         var lastInsertedId=courseDataRec.lastInsertedId;   
                                         var courseId = $('#idCourse').val();
                                        if ( $("#courseField4").val()!=""){
                                                    var file_data = $('#courseField4').prop('files')[0];
                                                    var form_data = new FormData();
                                                    form_data.append('file', file_data);
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "index.php?p=6&m=c&folder=course&lastInsertedId="+courseId,
                                                        data: form_data,
                                                        async: false,
                                                        processData: false,
                                                        contentType: false,
                                                        error: function (res) {
                                                            alert("Unsuccessful.");
                                                            return false;
                                                        },
                                                        success: function (res) {
                                                            //alert(res);
                                                        }
                                                    });

                                       }

                                        alert(" Record Updated successfully.");
                                        location.href='index.php?p=11&ln=2&t=6';
                                        $('#uniform-courseField4 span.filename').html("");
                                        $('#cancelCourseData').click();

                                        this.getDataAll();
                                     }else{
                                        alert("No record updated.");
                                     }
                              }
                         }
                         $('#cancelCourseData').click(function(){

                         $('#modeCourse').val(1);
                         $('#idCourse').val("");
                         $('#uniform-courseField4 span.filename').html("");
                         $('#editCourse').html('Add');
                         $('#courseField5 textarea').val(unescape(decodeURIComponent('')));
                         $('#courseField6 textarea').val(unescape(decodeURIComponent('')));
                         clearAll();
                     });



                         this.editData = function(id){
                           $('#editCourse').html('Edit');
                             $('#modeCourse').val(2);
                             $('#idCourse').val(id);
                             
                             var courseDataRec = {} ;
                             var course = {} ;
                             course['mode'] = "getAllCourse";
                             course['data'] = {};
                             course['data']['id'] = id ;
                             courseDataRec = get(course,'index.php?p=5','json');
                             setTimeout(function() {
                            $("#uniform-courseField4 span.filename").html(unescape(decodeURIComponent(id + '.' + courseDataRec.imageUpload)));
                            $('#courseFEdit4').val(id + '.' + courseDataRec.imageUpload);
                                  }, 1000);
                             $('#courseField1').val(unescape(decodeURIComponent(courseDataRec.courseName[0])));
                             $('#courseField2').val(courseDataRec.courseOrder[0]);
                             $('#courseField3').val(unescape(decodeURIComponent(courseDataRec.urlLink[0])));
                             $('#uniform-courseField4 span.filename').html(courseDataRec.imageUpload[0]);
                             $('#courseField5 textarea').val(unescape(decodeURIComponent(courseDataRec.contentHome[0])));
                             $('#courseField6 textarea').val(unescape(decodeURIComponent(courseDataRec.content[0])));
                             $('#courseField7').val(unescape(decodeURIComponent(courseDataRec.subCourseTitle[0])));


                             $('#courseField10').attr('checked','checked');



                            if(courseDataRec.active[0] == 1){

                                $('#uniform-courseField15 span').attr('class','checked');
                                $('#courseField15').attr('checked','checked');
                            }else{
                                $('#uniform-courseField15 span').attr('class','');
                                $('#courseField15').removeAttr('checked');
                            }

                         }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var course = {};
                             course['mode'] = "deleteCourse";
                             course['data'] = {};
                             course['data']['id'] = id;
                             var courseDataRec = get(course,'index.php?p=5','json');
                             if(courseDataRec.total>0){
                                alert(" Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record inserted.");
                             }
                         }
                         }
         }

/*****END: Home -> Course *****************************************************/

/********* PAGE START *******/
         var pageData = function(){

                         this.getDataAll = function(){



                                var course = {};
                                course['mode'] = "getAllPage";

                                 var courseDataRec = get(course,'index.php?p=5','json');

                                 if(courseDataRec.total>0){

                                     var str = "";
                                     $('#pageData').html('');
                                     for(var i=0;i<courseDataRec.total;i++){

                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.pageName[i])) + '</td>';
                                     // str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.pageHeader[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.urlLink[i])) + '</td>';
                                     //str = str + '<td>' + '<img width=16% height=8%  src= "images/course/' + courseDataRec.id[i]+'.'+courseDataRec.imageUploadArray[i][1]+ '" />' + '</td>';
                                     //str = str + '<td>' + unescape(decodeURIComponent(courseDataRec.pageTitle[i])) + '</td>';

                                        if(courseDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick=" location.href=\'#courseStart\',callPageData.editData('+courseDataRec.id[i]+',\''+escape(courseDataRec.pageName[i])+'\',\''+escape(courseDataRec.urlLink[i])+'\',\''+escape(courseDataRec.active[i])+'\',\''+escape(courseDataRec.pageContent[i])+'\',\''+escape(courseDataRec.pageHeader[i])+'\');"></i>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callPageData.deleteData('+courseDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#pageData').append(str);
                                     }
                                 }else{
                                     $('#pageData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

                         this.saveData = function(){

                             var course = {};
                             var courseDataRec;

                            if($('#modeCourse').val() == 1){

                                     course['mode'] = "savePage";
                                     course['data'] = {};
                                     course['data']['pageName'] = escape(encodeURIComponent(($('#courseField1').val())));  //for name
                                     course['data']['urlLink'] =    escape(encodeURIComponent($('#courseField3').val()));  //for urlLink
                                     course['data']['pageHeader']=$('#courseField5 textarea').val();
                                     course['data']['pageHeader']=escape(encodeURIComponent(course['data']['pageHeader']));
                                     course['data']['pageContent'] = $('#courseField6 textarea').val();  //should be get data for text editor2
                                     course['data']['pageContent']=escape(encodeURIComponent(course['data']['pageContent']));
                                     course['data']['active'] = ($('#uniform-courseField51 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                     if(course['data']['pageName']!=''){
                                        courseDataRec = get(course,'index.php?p=5','json');
                                     }
                                     else{
                                         alert("Please provide Page Title");
                                     }

                                     if(courseDataRec.total>0){

                                          alert(courseDataRec.total+" Record inserted successfully.");
                                      location.href='index.php?p=1&ln=1&t=3';
                                           // $('#uniform-courseField4 span.filename').html("");
                                            $('#courseField5 textarea').val("");
                                            $('#courseField6 textarea').val("");
                                            $('#cancelPageData').click();

                                        this.getDataAll();
                                     }else{
                                        alert("No record inserted.");
                                     }
                              }


                               if($('#modeCourse').val() == 2){

                                     course['mode'] = "updatePage";
                                     course['data'] = {};
                                     course['data']['pageName'] = escape(encodeURIComponent(($('#courseField1').val())));  //for name
                                     course['data']['urlLink'] =    escape(encodeURIComponent($('#courseField3').val()));  //for urlLink
                                     course['data']['pageHeader']=$('#courseField5 textarea').val();
                                     course['data']['pageHeader']=escape(encodeURIComponent(course['data']['pageHeader']));
                                     course['data']['pageContent'] = $('#courseField6 textarea').val();  //should be get data for text editor2
                                     course['data']['pageContent']=escape(encodeURIComponent(course['data']['pageContent']));
                                     course['data']['active'] = ($('#uniform-courseField51 span').attr('class')=='checked') ? 1 : 0;
                                     course['data']['id'] = $('#idPage').val();  //for id

                                     courseDataRec = get(course,'index.php?p=5','json');

                                     if(courseDataRec.total>0){

                                        alert(courseDataRec.total+" Record Updated successfully.");
                                        location.href='index.php?p=10&ln=1&t=3';

                                        $('#cancelPageData').click();

                                        this.getDataAll();
                                     }else{
                                        alert("No record inserted.");
                                     }


                               }
                         }
                         $('#cancelPageData').click(function(){
                         $('#modeCourse').val(1);
                         $('#idPage').val("");
                         $('#editCourse').html('Add');
                         $('#courseField5 textarea').val(unescape(decodeURIComponent('')));
                         $('#courseField6 textarea').val(unescape(decodeURIComponent('')));
                         clearAll();





                     });
                         this.editData = function(id,pageName,urlLink,active,pageContent,pageHeader){

                             $('#editCourse').html('Edit');
                             $('#modeCourse').val(2);
                             $('#idPage').val(id);
                             $('#courseField1').val(unescape(decodeURIComponent(pageName)));
                           //$('#courseField2').val(courseOrder);
                             $('#courseField3').val(unescape(decodeURIComponent(urlLink)));
                           //$('#uniform-courseField4 span.filename').html(imageUpload);
                             $('#courseField5 textarea').val(unescape(decodeURIComponent(pageHeader)));
                             $('#courseField6 textarea').val(unescape(decodeURIComponent(pageContent)));
                           //$('#courseField7').val(unescape(decodeURIComponent(subCourseTitle)));

            //                 if(courseNavigation == 1){
            //
            //                    $('#uniform-courseField8 span').attr('class','checked');
            //                    $('#courseField8').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField8 span').attr('class','');
            //                    $('#courseField8').removeAttr('checked');
            //                }
            //
            //                if(headerBlock == 1){
            //
            //                    $('#uniform-courseField9 span').attr('class','checked');
            //                    $('#courseField9').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField9 span').attr('class','');
            //                    $('#courseField9').removeAttr('checked');
            //                }
            //
            //                if(courseBlock == 1){
            //
            //                    $('#uniform-courseField10 span').attr('class','checked');
            //                    $('#courseField10').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField10 span').attr('class','');
            //                    $('#courseField10').removeAttr('checked');
            //                }
            //
            //
            //                if(subCourseBlock == 1){
            //
            //                    $('#uniform-courseField11 span').attr('class','checked');
            //                    $('#courseField11').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField11 span').attr('class','');
            //                    $('#courseField11').removeAttr('checked');
            //                }
            //
            //
            //                if(eventGalleryBlock == 1){
            //
            //                    $('#uniform-courseField12 span').attr('class','checked');
            //                    $('#courseField12').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField12 span').attr('class','');
            //                    $('#courseField12').removeAttr('checked');
            //                }
            //
            //                if(newsAndUpdateBlock == 1){
            //
            //                    $('#uniform-courseField13 span').attr('class','checked');
            //                    $('#courseField13').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField13 span').attr('class','');
            //                    $('#courseField13').removeAttr('checked');
            //                }
            //
            //                if(videoBlock == 1){
            //
            //                    $('#uniform-courseField14 span').attr('class','checked');
            //                    $('#courseField14').attr('checked','checked');
            //                }else{
            //                    $('#uniform-courseField14 span').attr('class','');
            //                    $('#courseField14').removeAttr('checked');
            //                }
            //
                            if(active == 1){

                                $('#uniform-courseField51 span').attr('class','checked');
                                $('#courseField51').attr('checked','checked');
                            }else{
                                $('#uniform-courseField51 span').attr('class','');
                                $('#courseField51').removeAttr('checked');
                            }
                         }
            //             }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this");
                             if(!result){
                                 return false;
                             }else{
                                 var course = {};
                                 course['mode'] = "deletePage";
                                 course['data'] = {};
                                 course['data']['id'] = id;
                                 var courseDataRec = get(course,'index.php?p=5','json');
                                 if(courseDataRec.total>0){
                                    alert(courseDataRec.total+" Record Deleted successfully.");
                                    this.getDataAll();
                                 }else{
                                    alert("No record inserted.");
                                 }
                             }
                         }


            //         $("#courseSubmit").click(function(){
            //             if($('#courseField4').val()!=""){
            //             var validImageFormat=/\.(jpg|jpeg|png)$/i;
            //             if(!validImageFormat.test($("#courseField4").val())){
            //                alert("Please upload imgae file");
            //                return false;
            //             }
            //             }
            //         })

          }

/********* PAGE END *******/

/********* CATEGORY_1 START *******/
         var categoryData = function(){

                         this.getDataAll = function(){
                                var category = {};
                                 category['mode'] = "getAllCategory";
                                 var categoryDataRec = get(category,'index.php?p=5','json');

                                 if(categoryDataRec.total>0){

                                     var str = "";
                                     $('#categoryData').html('');
                                     for(var i=0;i<categoryDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.courseName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.coursenavName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryOrder[i])) + '</td>';

                                        if(categoryDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#categoryStart\',callCategoryData.editData('+categoryDataRec.categoryId[i]+',\''+categoryDataRec.courseId[i]+'\',\''+escape(categoryDataRec.courseName[i])+'\',\''+escape(categoryDataRec.categoryName[i])+'\',\''+categoryDataRec.categoryOrder[i]+'\',\''+categoryDataRec.active[i]+'\',\''+categoryDataRec.navId[i]+'\');"></i>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callCategoryData.deleteData('+categoryDataRec.categoryId[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';
                                        $('#categoryData').append(str);
                                     }
                                 }else{
                                     $('#categoryData').html("No Content Found.");
                                 }
                         }

                         this.saveData = function(){
                             var category = {};
                             var categoryDataRec;

                                  if($('#modeCategory').val() == 1){

                                         category['mode'] = "saveCategory";
                                         category['data'] = {};
                                         category['data']['courseId'] = $('#categoryField1').val();  //for name
                                         category['data']['courseNav'] = $('#categoryNavigation').val();
                                         category['data']['categoryName'] = escape(encodeURIComponent(($('#categoryField2').val())));  //for title
                                         category['data']['categoryOrder'] = $('#categoryField3').val();  //for urlLink
                                         category['data']['active'] = ($('#categoryField4').is(':checked')) ? 1 : 0;   //for settings
                                         //category['data']['active'] = ($('#uniform-categoryField4 span').attr('class')=='checked') ? 1 : 0;  //for settings
                                         if(category['data']['courseId']==null || category['data']['courseId']=='null' ){
                                             alert("Please select Course");
                                         }
                                         else if(category['data']['courseNav']==null || category['data']['courseNav']=='null'){
                                             alert("Please select Course Navigation");
                                         }
                                         else if(category['data']['categoryName']==''){
                                            alert("Please provide Category name");
                                         }
                                         else{
                                             categoryDataRec = get(category,'index.php?p=5','json');
                                         }
                                         if(categoryDataRec.total>0){
                                            alert(categoryDataRec.total+" Record inserted successfully.");
                                            location.href="index.php?p=1&ln=2&t=1";
                                            $('#cancelCategory1Data').click();
                                            this.getDataAll();
                                         }else{
                                            alert("No record inserted.");
                                         }
                                  }

                                  if($('#modeCategory').val() == 2){

                                         category['mode'] = "updateCategory";
                                         category['data'] = {};
                                         category['data']['courseId'] =$('#categoryField1').val();
                                         category['data']['courseNav'] = $('#categoryNavigation').val();//for name
                                         category['data']['categoryName'] = escape(encodeURIComponent(($('#categoryField2').val())));  //for title
                                         category['data']['categoryOrder'] = $('#categoryField3').val();  //for urlLink
                                         category['data']['active'] = ($('#categoryField4').is(':checked')) ? 1 : 0;   //for settings
                                         //category['data']['active'] = ($('#uniform-categoryField4 span').attr('class')=='checked') ? 1 : 0;
                                         //category['data']['active'] = ($('#categoryField4').is(':checked')) ? 1 : 0;   //for settings
                                         category['data']['id'] = $('#idCategory').val();  //for category id
                                         if(category['data']['courseId']==null || category['data']['courseId']=='null' ){
                                             alert("Please select Course");
                                         }
                                         else if(category['data']['courseNav']==null || category['data']['courseNav']=='null'){
                                             alert("Please select Course Navigation");
                                         }
                                         else if(category['data']['categoryName']==''){
                                            alert("Please provide Category name");
                                         }
                                         else{
                                             categoryDataRec = get(category,'index.php?p=5','json');
                                         }
                                         if(categoryDataRec.total>0){
                                            alert(categoryDataRec.total+" Record Updated successfully.");
                                            location.href="index.php?p=1&ln=2&t=1";
                                            $('#categoryField1').val("");
                                            $('#categoryField1_chzn a span').html("Select Course");
                                            $('#cancelCategoryData').click();
                                            this.getDataAll();
                                         }else{
                                            alert("No record updated");
                                         }
                                  }


                         }

                         this.editData = function(id,courseId,courseName,categoryName,categoryOrder,active,navId){
                             $('#editCategory').html('Edit');
                             $('#modeCategory').val(2);
                             $('#idCategory').val(id);
                             $('#categoryField1').val(courseId);
                             this.changeCourse(courseId);
                             $('#categoryField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                             $('#categoryField2').val(unescape(decodeURIComponent(categoryName)));
                             $('#categoryField3').val(categoryOrder);
                             if(active == 1){
                                $('#uniform-categoryField4 span').attr('class','checked');
                                document.getElementById("categoryField4").checked=true;
                             }else{
                                $('#uniform-categoryField4 span').attr('class','');
                                $('#categoryField4').removeAttr('checked');
                             }
                             $('#categoryNavigation').val(navId);
                         }
 this.changeCourse=function(id){
             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color: red;'>*</span>Select Course Navigation</label><div class='controls'><select id='categoryNavigation' class='chzn-select'><option selected='selected' disabled='disabled' value=''>Select Navigation</option>";
             changeCourse['mode']='changeAddEventCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
             }
             option+="</div>"
             $('#categoryNavdiv').html(option);
  }
                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this");
                             if(!result){
                                 return false;
                             }else{
                                var category = {};
                                category['mode'] = "deleteCategory";
                                category['data'] = {};
                                category['data']['categoryId'] = id;  //for id
                                var categoryDataRec = get(category,'index.php?p=5','json');
                                if(categoryDataRec.total>0){
                                    alert(categoryDataRec.total+" Record Deleted successfully.");
                                    this.getDataAll();
                                }else{
                                    alert("No record inserted.");
                                }
                             }
                         }

                         $('#cancelCategory1Data').click(function(){
                         $('#modeCategory').val(1);
                         $('#idCategory').val("");
                         $('#editCategory').html('Add')
//                         $('#categoryField1_chzn a span').html(unescape(decodeURIComponent('')));
//                         $('#categoryField4').val('');
                         //clearAll();

                        });
         }
/********* CATEGORY_1 END *******/


/********* CATEGORY_2 START *******/
         var Category2 = function(){

                         this.getDataAll = function(){

                                var category = {};
                                 category['mode'] = "getAllCategory2";
                                 var categoryDataRec = get(category,'index.php?p=5','json');

                                //alert(categoryDataRec);
                                 if(categoryDataRec.total>0){

                                     var str = "";
                                     $('#categoryData2').html('');
                                     for(var i=0;i<categoryDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.courseName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryName1[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryOrder[i])) + '</td>';

                                        if(categoryDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#subcategoryStart\',callCategory2.editData('+categoryDataRec.categoryId[i]+',\''+categoryDataRec.courseId[i]+'\',\''+escape(categoryDataRec.courseName[i])+'\',\''+escape(categoryDataRec.categoryName[i])+'\',\''+categoryDataRec.categoryOrder[i]+'\',\''+categoryDataRec.active[i]+'\',\''+categoryDataRec.category1[i]+'\',\''+categoryDataRec.courseNavigation[i]+'\');"></i>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callCategory2.deleteData('+categoryDataRec.categoryId[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';
                                        $('#categoryData2').append(str);
                                     }
                                 }else{
                                     $('#categoryData2').html("No Content Found.");
                                 }

                         }

                         this.saveData = function(){
                             var category = {};
                             var categoryDataRec;

                                if($('#modeCategory2').val() == 1){

                                         category['mode'] = "saveCategory2";
                                         category['data'] = {};
                                         category['data']['courseId'] = $('#category2Field1').val();  //for name
                                         category['data']['category1'] = escape(encodeURIComponent(($('#category2Field6').val())));
                                         category['data']['categoryName'] = escape(encodeURIComponent(($('#category2Field2').val())));  //for title
                                         category['data']['categoryOrder'] = $('#category2Field3').val();  //for urlLink
                                         category['data']['courseNavigation'] = escape(encodeURIComponent(($('#category2CourseNav').val())));  //for urlLink
                                         category['data']['active'] = ($('#category2Field4').is(':checked')) ? 1 : 0;   //for settings
                                         if(category['data']['courseId']==null || category['data']['courseId']=='null'){
                                                alert("Please select Course");
                                         }
                                         else if(category['data']['courseNavigation']== null || category['data']['courseNavigation']=='null'){
                                             alert("Please select Course Navigation");
                                         }
                                         else if(category['data']['category1']== null || category['data']['category1']=='null'){
                                             alert("Please select Category");
                                         }
                                         else if(category['data']['categoryName']==''){
                                            alert("Please provide Category name")
                                         }
                                         else{
                                             categoryDataRec = get(category,'index.php?p=5','json');
                                         }
                                         if(categoryDataRec.total>0){
                                            alert(categoryDataRec.total+" Record inserted successfully.");

                                            $('#category2Field1').val("");
                                            $('#category2Field1_chzn a span').html("Select Course");
                                            $('#category2CourseNav').val("");
                                            $('#category2CourseNav_chzn a span').html("Select Course Navigation");
                                            $('#cancelCategory2').click();
                                            this.getDataAll();

                                         }else{
                                            alert("No record inserted.");
                                         }
                                  }

                               if($('#modeCategory2').val() == 2){

                                     category['mode'] = "updateCategory2";
                                     category['data'] = {};
                                     category['data']['courseId'] = $('#category2Field1').val();
                                     category['data']['category1'] = $('#category2Field6').val();
                                     category['data']['categoryName'] = escape(encodeURIComponent(($('#category2Field2').val())));  //for title
                                     category['data']['categoryOrder'] = $('#category2Field3').val();  //for urlLink
                                     category['data']['courseNavigation'] = escape(encodeURIComponent(($('#category2CourseNav').val())));
                                     category['data']['active'] = ($('#category2Field4').is(':checked')) ? 1 : 0; //for settings
                                     category['data']['id'] = $('#idCategory2').val();  //for category id
                                     if(category['data']['courseId']==null || category['data']['courseId']=='null'){
                                                alert("Please select Course");
                                         }
                                         else if(category['data']['courseNavigation']== null || category['data']['courseNavigation']=='null'){
                                             alert("Please select Course Navigation");
                                         }
                                         else if(category['data']['category1']== null || category['data']['category1']=='null'){
                                             alert("Please select Category");
                                         }
                                         else if(category['data']['categoryName']==''){
                                            alert("Please provide Category name")
                                         }
                                         else{
                                             categoryDataRec = get(category,'index.php?p=5','json');
                                         }
                                     if(categoryDataRec.total>0){
                                        alert(categoryDataRec.total+" Record Updated successfully.");

                                        $('#category2Field1').val("");
                                        $('#category2Field1_chzn a span').html("Select Course");
                                        $('#category2CourseNav').val("");
                                        $('#category2CourseNav_chzn a span').html("Select Course Navigation");
                                        $('#cancelCategory2').click();

                                        this.getDataAll();
                                     }else{
                                        alert("No record updated.");
                                     }
                                }


                         }


                         this.editData = function(id,courseId,courseName,categoryName,categoryOrder,active,category1,courseNav){
                             $('#editCategory2').html('Edit');
                             $('#modeCategory2').val(2);
                             $('#idCategory2').val(id);
                             $('#category2Field1').val(courseId);
                             this.courseChange(courseId);
                             this.courseNavChange(courseNav);
                             $('#category2Field1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                             $('#category2Field6').val(category1);
                             $('#category2Field2').val(unescape(decodeURIComponent(categoryName)));
                             $('#category2Field3').val(categoryOrder);
                             $('#category2CourseNav').val(courseNav);

                             if(active == 1){
                                $('#uniform-category2Field4 span').attr('class','checked');
                                document.getElementById("category2Field4").checked=true;
                             }else{
                                $('#uniform-category2Field4 span').attr('class','');
                                $('#category2Field4').removeAttr('checked');
                             }
                         }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this");
                             if(!result){
                                    return false;
                             }else{
                                    var category = {};
                                    category['mode'] = "deleteCategory2";
                                    category['data'] = {};
                                    category['data']['categoryId'] = id;  //for id
                                    var categoryDataRec = get(category,'index.php?p=5','json');
                                    if(categoryDataRec.total>0){
                                        alert(categoryDataRec.total+" Record Deleted successfully.");
                                        this.getDataAll();
                                    }else{
                                        alert("No record inserted.");
                                    }
                            }
                         }
                        this.courseNavChange=function(id){
                                var category={};
                                category['mode'] = "getCategory1ByNav";
                                category['data'] = {};
                                category['data']['courseId'] = id;  //for id
                                var courseData = get(category,'index.php?p=5','json');
                                var option='<select id="category2Field6" class="chzn-select"><option selected="selected" disabled="disabled" value="">Select Category</option>'
                                for(var i=0;i<courseData['TOTAL'];i++){
                                     option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
                                }
                                 option+="</select>";
                                 $('#category1Div').html(option);
                        }
                         this.courseChange=function(id){
                                 var changeCourse={};
                                 var option="<select id='category2CourseNav' class='chzn-select' onchange='callCategory2.courseNavChange(this.value);'><option selected='selected' disabled='disabled' value=''>Select Navigation</option>";
                                 changeCourse['mode']='changeAddEventCourse';
                                 changeCourse['data']={};
                                 changeCourse['data']['id']=id;
                                 var courseData = get(changeCourse,'index.php?p=5','json');

                                 for(var i=0;i<courseData['TOTAL'];i++){
                                     option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
                                 }
                                 option+="</select>"
                                 $('#CourseNavDiv').html(option);
                         }
                         $('#cancelCategoryData').click(function(){
                         $('#modeCategory2').val(1);
                         $('#idCategory').val("");
                         $('#editCategory2').html('Add')
                         $('#category2Field1_chzn a span').html(unescape(decodeURIComponent('')));
                         //$('#category2Field4').val('');
                         //clearAll();
                         });
         }

/********* CATEGORY_2 END *******/

/********* PRODUCT_CATEGORY START *******/
         var ProductCategory = function(){

                         this.getDataAll = function(){

                                var category = {};
                                 category['mode'] = "getAllProductCategory";
                                 var categoryDataRec = get(category,'index.php?p=5','json');
                                 if(categoryDataRec.total>0){

                                     var str = "";
                                     $('#PcategoryData').html('');
                                     for(var i=0;i<categoryDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.courseName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryOrder[i])) + '</td>';

                                        if(categoryDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#PcategoryStart\',callProductCategory.editData('+categoryDataRec.categoryId[i]+',\''+categoryDataRec.courseId[i]+'\',\''+escape(categoryDataRec.courseName[i])+'\',\''+escape(categoryDataRec.categoryName[i])+'\',\''+categoryDataRec.categoryOrder[i]+'\',\''+categoryDataRec.active[i]+'\');"></i>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callProductCategory.deleteData('+categoryDataRec.categoryId[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';
                                        $('#PcategoryData').append(str);
                                     }
                                 }else{
                                     $('#PcategoryData').html("No Content Found.");
                                 }

                         }

                         this.saveData = function(){
                             var category = {};
                             var categoryDataRec;

                                if($('#modePCategory').val() == 1){

                                         category['mode'] = "saveProductCategory";
                                         category['data'] = {};
                                         category['data']['courseId'] = $('#PcategoryField1').val();  //for name
                                         category['data']['categoryName'] = escape(encodeURIComponent(($('#PcategoryField2').val())));  //for title
                                         category['data']['categoryOrder'] = $('#PcategoryField3').val();  //for urlLink
                                         category['data']['active'] = ($('#PcategoryField4').prop('checked')) ? 1 : 0;   //for settings
                                         if(category['data']['courseId']==null || category['data']['courseId']=='null'){
                                             alert("Please select Course");
                                         }
                                         else if(category['data']['categoryName']=='') {
                                             alert("Please provide Category name");
                                         }
                                         else{
                                            categoryDataRec = get(category,'index.php?p=5','json');
                                         }
                                             if(categoryDataRec.total>0){
                                                alert(categoryDataRec.total+" Record inserted successfully.");

                                               $('#PcategoryField1').val("");
                                                $('#PcategoryField1_chzn a span').html("Select Course");
                                                $('#cancelProductCategory').click();

                                                this.getDataAll();
                                             }else{
                                                alert("No record inserted.");
                                             }
                                  }

                               if($('#modePCategory').val() == 2){

                                     category['mode'] = "updateProductCategory";
                                     category['data'] = {};
                                     category['data']['courseId'] =$('#PcategoryField1').val();  //for name
                                     category['data']['categoryName'] = escape(encodeURIComponent(($('#PcategoryField2').val())));  //for title
                                     category['data']['categoryOrder'] = $('#PcategoryField3').val();  //for urlLink
                                     category['data']['active'] = ($('#PcategoryField4').is(':checked')) ? 1 : 0; //for settings
//                                     alert("Updactive="+$('#PcategoryField4').is(':checked'));
                                     category['data']['id'] = $('#idPCategory').val();  //for category id

                                       categoryDataRec = get(category,'index.php?p=5','json');

                                     if(categoryDataRec.total>0){

                                        alert(categoryDataRec.total+" Record Updated successfully.");


                                        $('#PcategoryField1').val("");
                                        $('#PcategoryField1_chzn a span').html("Select Course");
                                        $('#cancelProductCategory').click();

                                        this.getDataAll();
                                     }else{
                                        alert("No record updated.");
                                     }
                              }


                         }


                         $('#PcancelCategoryData').click(function(){
                         $('#modePCategory').val(1);
                         $('#idPCategory').val("");
                         $('#editCategory3').html('Add')
                         $('#Pcategory2Field1_chzn a span').html(unescape(decodeURIComponent('')));
                         clearAll();

                         });
                         this.editData = function(id,courseId,courseName,categoryName,categoryOrder,active){

                             $('#editProductCategory').html('Edit');
                             $('#modePCategory').val(2);
                             $('#idPCategory').val(id);
                             $('#PcategoryField1').val(courseId);
                             $('#PcategoryField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                             $('#PcategoryField2').val(unescape(decodeURIComponent(categoryName)));
                             $('#PcategoryField3').val(categoryOrder);
                             if(active == 1){
                                $('#uniform-PcategoryField4 span').attr('class','checked');
                                document.getElementById("PcategoryField4").checked=true;
                                //$('#PcategoryField4').attr('checked','checked');

                            }else{
                                $('#uniform-PcategoryField4 span').attr('class','');
                                $('#PcategoryField4').removeAttr('checked');
                            }


                         }

                         this.deleteData = function(id){

                                 var result=confirm("Are you sure you want to delete this");
                                 if(!result){
                                     return false;
                                 }else{
                                    var category = {};
                                    category['mode'] = "deleteProductCategory";
                                    category['data'] = {};
                                    category['data']['categoryId'] = id;  //for id
                                    var categoryDataRec = get(category,'index.php?p=5','json');
                                    if(categoryDataRec.total>0){
                                        alert(categoryDataRec.total+" Record Deleted successfully.");
                                        this.getDataAll();
                                    }else{
                                        alert("No record updated.");
                                    }
                                 }
                         }


         }

/********* PRODUCT_CATEGORY END *******/

/********* PRODUCT START *******/
    
         var Product = function(){
                     this.changeCourse=function(id){
                         $('#chooseProducts').hide();
                         var changeCourse={};
                         var option="<label class='control-label' for='select01'><span style='color: red;'> * </span>Select Product Category</label><div class='controls'><select id='productField2' class='chzn-select' onchange='callProduct.changeProductCategory(this.value);'><option selected='selected' disabled='disabled' value=''>Select Product Category</option>";
                         changeCourse['mode']='getProductCOfCourse';
                         changeCourse['data']={};
                         changeCourse['data']['id']=id;
                         var courseData = get(changeCourse,'index.php?p=5','json');

                         for(var i=0;i<courseData['TOTAL'];i++){
                             option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
                     }
                         option+="</div>"

                         $('#categorydiv').html(option);
                    }
                    this.changeProductCategory=function(id){
                         var changeProductCategory={};
                         var div="<label class='control-label' for='optionsCheckbox'>Select Similar Products</label><div class='controls'>"
                         changeProductCategory['mode']='getProductSOfCourse';
                         changeProductCategory['data']={};
                         changeProductCategory['data']['id']=id;
                         var changeProductCategoryData = get(changeProductCategory,'index.php?p=5','json');
                         $('#countProducts').val(changeProductCategoryData['TOTAL']);
                         if($('#countProducts').val()>0){
                                 $('#chooseProducts').show();
                         }
                         for(var i=0;i<changeProductCategoryData['TOTAL'];i++){
                             div+= "<label class='uniform'>"
                             div+= "<input class='uniform_on' type='checkbox' id='productFieldLike"+i+"'"+ " "+   "value='"+changeProductCategoryData['ID'][i]+"' name='selectProduct'> " + changeProductCategoryData['NAME'][i];
                             if(changeProductCategoryData['COST'][i]!= 0 ){
                                div+="<b> Price: </b>" + "Rs" + " " + changeProductCategoryData['COST'][i];
                             }
                             div+= "</label>"
                         }
                   
                         div+="</div>"

                         $('#chooseProducts').html(div);
                    }

                    this.getDataAll = function(){
                            var category = {};
                             category['mode'] = "getAllProduct";
                             category['data'] = {};
                             category['data']['startpoint'] = $('#startpoint').val();
                             category['data']['perpage'] = $('#perpage').val();
                             var srNo = $('#srNo').val();
                             var categoryDataRec = get(category,'index.php?p=5','json');

                                if(categoryDataRec.total>0){

                                     var str = "";

                                     $('#PData').html('');

                                     for(var i=0;i<categoryDataRec.total;i++){
                                            str = "";

                                            str = str + '<tr>';
                                            str = str + '<td>' + srNo + '</td>';
                                            str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.courseName[i])) + '</td>';
                                            str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryName[i])) + '</td>';
                                            str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.categoryOrder[i])) + '</td>';
                                            str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.cost[i])) + '</td>';
                                            str = str + '<td>'+ unescape(decodeURIComponent(categoryDataRec.productAuthor[i])) +'</td>';
                                            str = str + '<td>'+ unescape(decodeURIComponent(categoryDataRec.productLang[i])) +'</td>';
                                            str = str + '<td>'+ categoryDataRec.productLength[i] +'</td>';
                                            str = str + '<td>' + unescape(decodeURIComponent(categoryDataRec.productPublisher[i])) + '</td>';
                                            if(categoryDataRec.purchaseFromThirdparty[i] == 1){
                                                str = str + '<td style="width:14%">Yes</td>';
                                            }else{
                                                str = str + '<td style="width:14%">No</td>';
                                            }
                                            if(categoryDataRec.active[i] == 1){
                                                str = str + '<td>Active</td>';
                                            }else{
                                                str = str + '<td>Inactive</td>';
                                            }
                                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#productStart\',callProduct.editData('+categoryDataRec.productId[i]+',\''+categoryDataRec.courseId[i]+'\',\''+escape(categoryDataRec.courseName[i])+'\',\''+escape(categoryDataRec.categoryName[i])+'\',\''+categoryDataRec.categoryOrder[i]+'\',\''+categoryDataRec.cost[i]+'\',\''+categoryDataRec.tax[i]+'\',\''+categoryDataRec.discountCost[i]+'\',\''+categoryDataRec.description[i]+'\',\''+categoryDataRec.Image[i]+'\',\''+categoryDataRec.active[i]+'\',\''+categoryDataRec.categoryId[i]+'\',\''+categoryDataRec.productAuthor[i]+'\',\''+categoryDataRec.productLang[i]+'\',\''+categoryDataRec.productLength[i]+'\',\''+categoryDataRec.productPublisher[i]+'\',\''+categoryDataRec.purchaseFromThirdparty[i]+'\',\''+categoryDataRec.thirdPartyLink[i]+'\',\''+categoryDataRec.likeProductIds[i]+'\');"></i>' + '</td>';
                                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callProduct.deleteData('+categoryDataRec.productId[i]+');"></i>' + '</td>';
                                            str = str + '</tr>';

                                            $('#PData').append(str);
                                      srNo++;
                                     }
                                }else{

                                 $('#PData').html("No Content Found.");

                                }
                    }
                     var countA=0;
                     this.saveData = function(){
                        var category = {};
                        var categoryDataRec;            
                       if($('#modeProduct').val() == 1){
                                var productIds=new Array();
                                var ab=0;
                                var a=document.getElementsByName('selectProduct').length;
                                for (var index=0; index < a; index++) {
                                    if(document.getElementById("productFieldLike"+index).checked){
                                        productIds[ab] = document.getElementById("productFieldLike"+index).value;
                                        ab++;
                                    }
                                }
                                 category['mode'] = "saveProduct";
                                 category['data'] = {};
                                 category['data']['productIds'] = productIds;
                                 category['data']['courseId'] = $('#productField1').val();  //for name
                                 category['data']['categoryId'] = $('#productField2').val();  //for name
                                 category['data']['productName'] = escape(encodeURIComponent(($('#productField3').val())));  //for title
                                 category['data']['Image'] = escape(encodeURIComponent(($('#productField4').val())));  //for title
                                 var dotIndex = category['data']['Image'].lastIndexOf('.'); //to get last index
                                 var ext = category['data']['Image'].substring(dotIndex); // to get extension of image
                                 category['data']['extension'] = ext;
                                 var path = category['data']['Image'];
                                 category['data']['Image'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                                 category['data']['description'] = escape(encodeURIComponent(($('#productField5').val())));  //for title
                                 category['data']['cost'] = escape(encodeURIComponent(($('#productField6').val())));  //for title
                                 category['data']['discountCost'] = escape(encodeURIComponent(($('#productField9').val())));  //for title
                                 category['data']['taxCost'] = escape(encodeURIComponent(($('#feetaxP').val())));  //for title
                                 category['data']['productOrder'] = $('#productField7').val();  //for urlLink
                                 category['data']['active'] = ($('#productField8').is(':checked')) ? 1 : 0;   //for purcahse third party
                                 category['data']['author'] = escape(encodeURIComponent(($('#productField10').val()))); //for author
                                 category['data']['language'] = $('#productField11').val();  //for language
                                 category['data']['length'] = $('#productField12').val();  //for length
                                 category['data']['publisher'] = escape($('#productField13').val()); //publisher
                                 category['data']['purchaseFromthirdparty'] = ($('#productField14').is(':checked')) ? 1 : 0;   //for purcahse third party
                                 category['data']['thirdPartyLink'] = escape($('#productField15').val());   //for purcahse third party link
                                 category['data']['comboProductPrice'] = $('#productField17').val();
                                 category['data']['comboProductELink'] = $('#productField18').val(); //combo Product E-Link
                                 category['data']['comboProductStatus'] = ($('#productField161').is(':checked')) ? 1 : 0 ; //for combo product status
                                 
                                 /* combo product images */
                                 /*var file = $('#productField19').val(); //combo Product Images
                                 var comboProductImg=new Array(); // array for combo product images
                                 var imgIndex=0;
                                 var idx = $('#addAttachdiv p').size(); // length of combo product images
                                 if(idx>1){
                                    for (var j=2;j<= idx;j++){
                                            comboProductImg[imgIndex] = document.getElementById("comboproductimg_new"+j).value;
                                            imgIndex++;
                                         }
                                 }
                                 if(file!=''){
                                            comboProductImg.push(file);
                                 }
                                 category['data']['comboProductImg']=comboProductImg;
                                 /* combo product images end */
                              var imageName = "";
            var checkBox = document.getElementById("productField161").checked == true;
            if (checkBox == true) {

                var nooffile = $("#nooffile").val();
                for (var d = 0; d <= nooffile; d++) {
                    var fileName = $("#file" + d).val();
                    if (fileName != undefined) {
                        if (imageName == "") {
                            imageName = fileName;
                        } else {
                            imageName += ',' + fileName;
                        }
                    }
                    if (fileName == "") {
                        alert("Please select an image ");
                        return false;
                    }
                }
            }
            category['data']['comboImageName'] =escape(imageName);
                             
                                 if(category['data']['courseId']==null || category['data']['courseId']=='null'){
                                     alert("Please select Course");
                                 }
                                 else if(category['data']['categoryId']==null || category['data']['categoryId']=='null'){
                                   alert("Please select Product Category");
                                 }
                                 else if(category['data']['productName']==''){
                                     alert("Please provide Product name");
                                 }
                                 else if(category['data']['Image']==''){
                                        alert("Please upload Image");
                                 }
                                 else if(category['data']['extension']!='.jpeg' && category['data']['extension']!='.png' && category['data']['extension']!='.jpg' ){
                                        alert("Please upload Image in given format");
                                 }
                                 else if(imgWidth<=1746 && imgWidth >=90 && imgHeight>=58 && imgHeight<=2699 ){
                                        categoryDataRec = get(category,'index.php?p=5','json');
                                     }
                                 else{
                                         alert("Please upload image in given dimensions");
                                 }
                                 if(categoryDataRec.total>0){

                                     var lastInsertedId =categoryDataRec.lastInsertedId;
                                     /* image upload of products*/
                                            if ( $("#productField4").val()!=""){
                                                     var datas=$('#productField4')[0].files;
                                                     if (window.FormData) {
                                                        var formdata = new FormData();
                                                     }
                                                     var i = 0, len = datas.length, img, reader, file1;

                                                     for ( ; i < len; i++ ) {
                                                        file1 = datas[i];

                                                            if ( window.FileReader ) {
                                                                    reader = new FileReader();
                                                                    reader.onloadend = function (e) {
                                                                    };
                                                                    reader.readAsDataURL(file1);
                                                            }
                                                            if (formdata) {
                                                                    formdata.append("file[]", file1);
                                                            }
                                                      }
                                                            if (formdata) {

                                                                  $.ajax({
                                                                        url: "index.php?p=6&m=p&lastInsertedId="+lastInsertedId,
                                                                        type: "POST",
                                                                        data: formdata,
                                                                        processData: false,
                                                                        contentType: false,
                                                                        async:false,
                                                                        error:function(res){
                                                                           // alert(res);
                                                                        },
                                                                        success: function (res) {
                                                                            // alert(res);
                                                                        }
                                                                   });
                                                              }
                                            }

                  
                    var checkBox=document.getElementById("productField161").checked==true;
                    if(checkBox==true){
                       
                        var nooffile=$("#nooffile").val();
                    for(var d=0;d<=nooffile;d++){
                       var fileName=$("#file"+d).val();
                       if(fileName==""){
                           alert("Please select an image ");
                           return false;
                       }
                   }
                    }
                        var formData = new FormData();
                        $.each($("input[type=file]"), function(i, obj) {
                            $.each(obj.files,function(j,file){
                                formData.append('file['+i+']', file);
                                
                            })
                        });
                        
                        $.ajax({
                            url: "index.php?p=6&m=attach&lastInsertedId=" + lastInsertedId,
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'POST'
                        }).fail(function(jqXHR) { 
//                            console.log(jqXHR.responseText);
//                            console.log(jqXHR.status);
                        }).success(function(result){
                        });
                        
                                 alert(categoryDataRec.total+" Record inserted successfully.");
                                  this.getDataAll();
                                  $('#cancelProduct').click();
                                  $("#nooffile").val(0);
                                  $("#comboProductImageLink").hide(); 
                                  for(var b=0;b<=countA;b++){
                                          document.getElementById("removeData").deleteRow(0);
                                  } 
                                      
                                       
                                                /*$('#productField1').val("");
                                                $('#productField1_chzn a span').html("Select Course");
                                                $('#productField2_chzn a span').html("Select Product Category");
                                                $('#chooseProducts').hide();
                                                $('#cancelProduct').click();*/
                                                
                                 }else{
                                    alert("No record updated.");
                                 }
                          }

                           if($('#modeProduct').val() == 2){
                                var productIds=new Array();
                                var ab=0;
                                var a=document.getElementsByName('selectProduct').length;
                                for (var index=0; index < a; index++) {
                                    if(document.getElementById("productFieldLike"+index).checked){
                                        productIds[ab] = document.getElementById("productFieldLike"+index).value;
                                        ab++;
                                    }
                                }

                                 category['mode'] = "updateProduct";
                                 category['data'] = {};
                                 category['data']['productIds'] = productIds;
                                 category['data']['courseId'] = $('#productField1').val();  //for name
                                 category['data']['categoryId'] = $('#productField2').val();  //for name
                                 category['data']['productName'] = escape(encodeURIComponent(($('#productField3').val())));  //for title
                                 if($('#productField4').val()==''){
                                                category['data']['Image'] = $('#upldproductImage').val();
                                                var dotIndex = category['data']['Image'].lastIndexOf('.'); // to get last index
                                                var ext = category['data']['Image'].substring(dotIndex);   //to get extension of image
                                                category['data']['extension'] = ext;
                                 }else{
                                                category['data']['Image'] = $('#productField4').val();
                                                var dotIndex = category['data']['Image'].lastIndexOf('.');  // to get last index
                                                var ext = category['data']['Image'].substring(dotIndex);    // to get extension of image
                                                category['data']['extension'] = ext;
                                 }
                                 var path = category['data']['Image'];
                                 category['data']['Image'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                                // category['data']['Image'] = escape(encodeURIComponent(($('#productField4').val())));  //for title
                                 category['data']['description'] = escape(encodeURIComponent(($('#productField5').val())));  //for title
                                 category['data']['cost'] = escape(encodeURIComponent(($('#productField6').val())));  //for title
                                 category['data']['discountCost'] = escape(encodeURIComponent(($('#productField9').val())));  //for title
                                 category['data']['taxCost'] = escape(encodeURIComponent(($('#feetaxP').val()))); 
                                 category['data']['productOrder'] = $('#productField7').val();  //for urlLink
                                 category['data']['id'] =$('#idProduct').val();
                                 category['data']['active'] = ($('#productField8').is(':checked')) ? 1 : 0;   //for purcahse third party 
                                 category['data']['author'] = $('#productField10').val();  //for author
                                 category['data']['language'] = $('#productField11').val();  //for language
                                 category['data']['length'] = $('#productField12').val();  //for length
                                 category['data']['publisher'] = escape($('#productField13').val()); //publisher
                                 category['data']['purchaseFromthirdparty'] = ($('#productField14').is(':checked')) ? 1 : 0;
                                 category['data']['thirdPartyLink'] = escape($('#productField15').val());   //for purcahse third party link
                                /* category['data']['comboProductStatus'] = ($('#productField16').is(':checked')) ? 1 : 0;   //for combo product status
                                 category['data']['comboProductPrice'] = $('#productField17').val();   //for combo product price
                                 category['data']['comboProductELink'] = $('#productField18').val(); // combo product E-link */
                                 if(category['data']['courseId']==null || category['data']['courseId']=='null'){
                                     alert("Please select Course");
                                 }
                                 else if(category['data']['categoryId']==null || category['data']['categoryId']=='null'){
                                   alert("Please select Product Category");
                                 }
                                 else if(category['data']['productName']==''){
                                     alert("Please provide Product name");
                                 }
                                 else if(category['data']['Image']==''){
                                        alert("Please upload Image");
                                 }
                                 else if(category['data']['extension']!='.jpeg' && category['data']['extension']!='.png' && category['data']['extension']!='.jpg' ){
                                        alert("Please upload Image in given format");
                                 }
                                 else if(imgWidth!='' && imgHeight!=''){
                                        if(imgWidth<=1746 && imgWidth >=90 && imgHeight>=58 && imgHeight<=2699 ){
                                         categoryDataRec = get(category,'index.php?p=5','json');
                                        }
                                        else{
                                            alert("Please upload image in given dimensions");
                                        }
                                 }
                                 else{
                                     categoryDataRec = get(category,'index.php?p=5','json');
                                 }
//                                 if(categoryDataRec.total>0){  /* commentd by shruti for update even if image have same name */
                                     var lastInsertedId =categoryDataRec.lastInsertedId;
                                            if ( $("#productField4").val()!=""){
                                                     var datas=$('#productField4')[0].files;
                                                     if (window.FormData) {
                                                        var formdata = new FormData();
                                                     }
                                                     var i = 0, len = datas.length, img, reader, file1;

                                                     for ( ; i < len; i++ ) {
                                                        file1 = datas[i];

                                                            if ( window.FileReader ) {
                                                                    reader = new FileReader();
                                                                    reader.onloadend = function (e) {
                                                                    };
                                                                    reader.readAsDataURL(file1);
                                                            }
                                                            if (formdata) {
                                                                    formdata.append("file[]", file1);
                                                            }
                                                      }
                                                            if (formdata) {

                                                                  $.ajax({
                                                                        url: "index.php?p=6&m=p&lastInsertedId="+lastInsertedId,
                                                                        type: "POST",
                                                                        data: formdata,
                                                                        processData: false,
                                                                        contentType: false,
                                                                        async:false,
                                                                        error:function(res){
                                                                           // alert(res);
                                                                        },
                                                                        success: function (res) {
                                                                             //alert(res);
                                                                        }
                                                                   });
                                                              }
                                            }


                                                alert(" Record updated successfully.");

                                                $('#productField1').val("");
                                                $('#productField1_chzn a span').html("Select Course");
                                                $('#productField2').val("");
                                                $('#productField2_chzn a span').html("Select Product Category");
                                                $('#chooseProducts').hide();
                                                $('#uniform-productField4 span.filename').html("");
                                                $('#uniform-file0 span.filename').html("");
//                                                var tr = document.createElement('TR');
                                                  
                                                $('#cancelProduct').click();

                                                this.getDataAll();
//                                 } /* commentd by shruti for update even if image have same name */
                                 /*categoryDataRec = get(category,'index.php?p=5','json');
                                 if(categoryDataRec.total>0){
                                    alert(categoryDataRec.total+" Record Updated successfully.");

                                    $('#productField1').val("");
                                    $('#productField1_chzn a span').html("Select Course");
                                    $('#productField2').val("");
                                    $('#productField2_chzn a span').html("Select Product Category");
                                    $('#cancelProduct').click();

                                    this.getDataAll();
                                 }else{*/
                                    /* else condition commentd by shruti for update even if image have same name */
//                                   else{
//                                        alert("No record updated.");
//                                 }
                          }


                     }

                        this.editData = function(id,courseId,courseName,categoryName,categoryOrder,cost,tax,discountCost,description,Image,active,categoryId,author,lang,length,publisher,purchasefromthirdparty,thirdpartylink,likeProductIds){
                         $('#editCategoryP').html('Edit');
                         $('#modeProduct').val(2);
                         $('#idProduct').val(id);
                         $('#productField1').val(courseId);
                         this.changeCourse(courseId);// calling CHANGE COURSE FUNCTION
                         this.changeProductCategory(categoryId); //CALLING CHNAGE PRODUCT CATEGOERY FUNCTION
                         var likeproductIdsArray=new Array();
                         likeproductIdsArray = likeProductIds.split(",");
                         var Idlen=document.getElementsByName('selectProduct').length;
                         for(var i=0;i<Idlen;i++)
                         {
                                for(var j=0;j<likeproductIdsArray.length;j++)
                                 {
                                    if(document.getElementById("productFieldLike"+i).value==likeproductIdsArray[j]){
                                        document.getElementById("productFieldLike"+i).checked=true;
                                    }
                                }
                         }
                       //this.category1changeP(categoryId);
                         $('#productField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                         $('#productField2').val(categoryId);
                         $('#productField3').val(unescape(decodeURIComponent(categoryName)));
                         $('#uniform-productField4 span.filename').html(Image);
                         $('#upldproductImage').val(Image);
                         $('#productField5').val(unescape(decodeURIComponent(description)));
                         $('#productField6').val(cost);
                         $('#productField9').val(discountCost);
                         if(tax=='null'){
                             var tax=0;
                         }
                         var costAfterDiscount=parseFloat(cost,10)-((parseFloat(cost,10)/100)*parseFloat(discountCost,10));
                         var costAfterTax=costAfterDiscount+((costAfterDiscount/100)*parseFloat(tax,10));
                         $('#feeP').val(costAfterDiscount);
                         $('#feetaxP').val(tax);
                         $('#toatlFeeP').val(costAfterTax);
                         $('#productField7').val(categoryOrder);
                         $('#productField10').val(unescape(decodeURIComponent(author)));
                         $('#productField11').val(unescape(decodeURIComponent(lang)));
                         $('#productField12').val(unescape(decodeURIComponent(length)));
                         $('#productField13').val(unescape(decodeURIComponent(publisher)));
                         $('#productField15').val(unescape(decodeURIComponent(thirdpartylink)));
                         if(active == 1){

                            $('#uniform-productField8 span').attr('class','checked');
                            //$('#productField8').attr('checked','checked');
                            document.getElementById("productField8").checked=true;
                        }else{
                            $('#uniform-productField8 span').attr('class','');
                            $('#productField8').removeAttr('checked');
                        }
                        if(purchasefromthirdparty == 1){

                            $('#uniform-productField14 span').attr('class','checked');
                            //$('#productField14').attr('checked','checked');
                            document.getElementById("productField14").checked=true;
                            this.showThirdpartylink();
                        }else{
                            $('#uniform-productField14 span').attr('class','');
                            $('#productField14').removeAttr('checked');
                        }
                       /* if(comboProductStatus == 1){

                            $('#uniform-productField16 span').attr('class','checked');
                            //$('#productField8').attr('checked','checked');
                            document.getElementById("productField16").checked=true;
                        }else{
                            $('#uniform-productField16 span').attr('class','');
                            $('#productField16').removeAttr('checked');
                        }
                        $("#productField17").val(comboProductPrice);
                        $("#productField18").val(comboProductElink);*/
                        $("#productField6").val(cost);
                        $("#productField9").val(discountCost);
                        $('#productField5').val(unescape(decodeURIComponent(description)));
                        $('#uniform-productField4 span.filename').html(Image);
                     }

                     this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this");
                             if(!result){
                                 return false;
                             }else{
                                var category = {};
                                category['mode'] = "deleteProduct";
                                category['data'] = {};
                                category['data']['productId'] = id;  //for id
                                var categoryDataRec = get(category,'index.php?p=5','json');
                                if(categoryDataRec.total>0){
                                    alert(categoryDataRec.total+" Record Deleted successfully.");
                                    this.getDataAll();
                                }else{
                                    alert("No record inserted.");
                                }
                            }
                    }
                    this.courseChange=function(id){
                        var category={};
                        category['mode'] = "getCategory1";
                                category['data'] = {};
                                category['data']['courseId'] = id;  //for id

                                var courseData = get(category,'index.php?p=5','json');

                                var option='<select id="productField1" class="chzn-select" onchange="callProduct.category12change(this.value);"><option selected="selected" disabled="disabled" value="">Select Course</option>'
                               for(var i=0;i<courseData['TOTAL'];i++){
                                     option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
                            }
                                 option+="</select>";

                                 $('#productField1').html(option);

                    }
                   this.category12change=function(id){
                            var category={};
                            category['mode'] = "getCategory2";
                                    category['data'] = {};
                                    category['data']['cId'] = id;  //for id

                                    var courseData = get(category,'index.php?p=5','json');

                                    var option='<select id="productField2" class="chzn-select" ><option selected="selected" disabled="disabled" value="">Select Product Category</option>'
                                    for(var i=0;i<courseData['TOTAL'];i++){
                                         option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
                                     }
                                     option+="</select>";

                                     $('#productField2').html(option);
                    }
                    /**********FUNCTION TO SHOW THIRD PARTY LINK BOX ********/
            this.showThirdpartylink = function(){
                if(document.getElementById("productField14").checked==true){
                        $('#thirdPartylinkdiv').show();
                }
                else{
                        $('#thirdPartylinkdiv').hide();
                }
            }
            
         /*start added by deepak*/   
            var inputs = 0;
    this.addAttachment = function() {
        var count = 1;countA++;
        var nooffile = $("#nooffile").val();
        
        if (nooffile) {
            count = nooffile;
            count++;
        }
        var table = document.getElementById('contacts');
        table.setAttribute('class', 'controls');
        var tr = document.createElement('TR');
        var td1 = document.createElement('TD');
        var td2 = document.createElement('TD');
        var td3 = document.createElement('TD');
        var inp1 = document.createElement('INPUT');
        
        if (inputs == 0) {
            var img = document.createElement('IMG');
            img.setAttribute('src', '');
            img.setAttribute('class', 'icon-remove');
            img.onclick = function() {
                removeAttachment(tr);
            }
            td2.appendChild(img);

        }
        if (inputs > 0) {
            var img = document.createElement('IMG');
            img.setAttribute('src', '');
            img.setAttribute('class', 'icon-remove');

            img.onclick = function() {
                removeAttachment(tr);
            }
            td2.appendChild(img);

        }

        inp1.setAttribute("type", "file");
        inp1.setAttribute("name", "file[]");
        inp1.setAttribute("class", "input-file uniform_on");
        inp1.setAttribute("size", "10");
        inp1.setAttribute("id", "file" + count);
        tr.setAttribute("id", "filerows" + count);
 
        td1.setAttribute("align", "left");
        td2.setAttribute("align", "right");
        td1.setAttribute('style', 'margin:0px; padding:10px 0px');
        td2.setAttribute('style', 'margin:0px; padding:10px 0px');

        table.appendChild(tr);
        tr.appendChild(td1);
        tr.appendChild(td2);

        td1.appendChild(inp1);
        $("#nooffile").val(count);
        inputs++;
       
    }
    
    function removeAttachment(tr) {
        tr.parentNode.removeChild(tr);
        var nooffile = $("#nooffile").val();
        nooffile-=1;countA-=1;
        $("#nooffile").val(nooffile);

    }
    /*end added by deepak*/
            
            this.showImageAttachmentlink=function(){
                if(document.getElementById("productField161").checked==true){
                        $('#comboProductImageLink').show();
                }
                else{
                        $('#comboProductImageLink').hide();
                } 
            }

         }
             $('#cancelProduct').click(function(){
             $('#modeCategory').val(1);
             $('#idCategory').val("");
             $('#productField1_chzn a span').html("Select Course");
             $('#productField2_chzn a span').html("Select Product Category");
             $('#productField3').val("");
             $('#uniform-productField4 span.filename').html("");
             $('#uniform-productField19 span.filename').html("");
             $('#chooseProducts').hide();
            
             //clearAll('category');
             });
            /*******To FIND DIMENSION OF PRODUCT IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#productField4").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/********* PRODUCT END *******/


  /********* SUBCOURSE START *******/

         var subCourseData = function(){

             this.getDataAll = function(){




                    var subCourse = {};
                     subCourse['mode'] = "getAllSubCourse";

                     var subCourseDataRec = get(subCourse,'index.php?p=5','json');

                     if(subCourseDataRec.total>0){

                         var str = "";
                         $('#subCourseData').html('');
                         for(var i=0;i<subCourseDataRec.total;i++){
                         //subCourseDataRec.content[i]=htmlDecode(subCourseDataRec.content[i]);
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td style="width:5%;">' + unescape(decodeURIComponent(subCourseDataRec.courseName[i])) + '</td>';
                            str = str + '<td style="width:5%;">' + unescape(decodeURIComponent(subCourseDataRec.categoryName[i])) + '</td>';
                            str = str + '<td style="width:5%;">' + unescape(decodeURIComponent(subCourseDataRec.subCategoryName[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(subCourseDataRec.subCourseName[i])) + '</td>';
                            str = str + '<td> ' + unescape(decodeURIComponent(subCourseDataRec.urlLink[i])) + ' </td>';
                            str = str + '<td style="width:10%;">' + subCourseDataRec.subCourseOrder[i] + '</td>';
                            str = str + '<td>' + subCourseDataRec.fee[i] + '</td>';
                            if(subCourseDataRec.activeSubCourse[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#subCourseStart\',callSubCourseData.editData('+subCourseDataRec.subCourseId[i]+',\''+subCourseDataRec.courseId[i]+'\',\''+escape(subCourseDataRec.courseName[i])+'\',\''+subCourseDataRec.categoryId[i]+'\',\''+escape(subCourseDataRec.categoryName[i])+'\',\''+escape(subCourseDataRec.urlLink[i])+'\',\''+escape(subCourseDataRec.subCourseName[i])+'\',\''+escape(subCourseDataRec.content[i])+'\',\''+subCourseDataRec.subCourseOrder[i]+'\',\''+subCourseDataRec.activeSubCourse[i]+'\',\''+subCourseDataRec.fee[i]+'\',\''+subCourseDataRec.tax[i]+'\',\''+subCourseDataRec.categoryId2[i]+'\',\''+escape(subCourseDataRec.courseNav[i])+'\');"></i>' + '</td>';
                           // str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#subCourseStart\',callSubCourseData.editData('+subCourseDataRec.subCourseId[i]+',\''+subCourseDataRec.courseId[i]+'\',\''+escape(subCourseDataRec.courseName[i])+'\',\''+subCourseDataRec.categoryId[i]+'\',\''+escape(subCourseDataRec.categoryName[i])+'\',\''+escape(subCourseDataRec.urlLink[i])+'\',\''+escape(subCourseDataRec.subCourseName[i])+'\',\''+escape(subCourseDataRec.content[i])+'\',\''+subCourseDataRec.subCourseOrder[i]+'\',\''+subCourseDataRec.activeSubCourse[i]+'\',\''+subCourseDataRec.fee[i]+'\',\''+subCourseDataRec.categoryId2[i]+'\',\''+escape(subCourseDataRec.courseNav[i])+'\',\''+subCourseDataRec.courseNavName[i]+'\',\''+subCourseDataRec.subCategoryName[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callSubCourseData.deleteData('+subCourseDataRec.subCourseId[i]+');"></i>' + '</td>';
                            str = str + '</tr>';

                            $('#subCourseData').append(str);
                         }
                     }else{
                         $('#subCourseData').html("No Content Found.");
                     }




             }

             this.saveData = function(){
                 var subCourse = {};
                 var subCourseDataRec;

                if($('#modeSubCourse').val() == 1){

                         subCourse['mode'] = "saveSubCourse";
                         subCourse['data'] = {};
                         subCourse['data']['courseID'] =$('#subCourseField1').val();  //for name
                         subCourse['data']['categoryId'] =$('#subCourseField2').val();  //for title
                         subCourse['data']['categoryId2'] =$('#subCourseField12').val();  //for title
                         subCourse['data']['urlLink'] = escape(encodeURIComponent(($('#subCourseField3').val())));  //for urlLink
                         subCourse['data']['courseNav'] = escape(encodeURIComponent(($('#SubCourseNav').val())));  //for urlLink
                         subCourse['data']['subCourseName'] = escape(encodeURIComponent(($('#subCourseField4').val())));  //for helpline
                         subCourse['data']['fee'] = parseFloat($('#fee').val(),10)+parseFloat((($('#fee').val()*$('#feetax').val())/100),10);  //for helpline
                         subCourse['data']['tax'] = $('#feetax').val();  //for helpline                        
                         subCourse['data']['subCourseOrder'] = $('#subCourseField5').val();  //for sms enquiry

                         subCourse['data']['content'] = escape(encodeURIComponent($('#subCourseField6 textarea').val()));  //for sms enquiry
//                        subCourse['data']['content']= subCourse['data']['content'].replace(/(<div>|<\/div>|<p>|<\/p>)/g,"");
//                          subCourse['data']['content']=encodeURIComponent(subCourse['data']['content']);
//                          subCourse['data']['content']= subCourse['data']['content'].replace(/(%0A|%09)/g,"");

                         //subCourse['data']['activeSubCourse'] = ($('#uniform-courseField7 span').attr('class')=='checked') ? 1 : 0;  //for settings
                         //subCourse['data']['active'] = ($('#subCourseField7').is(':checked')) ? 1 : 0;
                         subCourse['data']['active'] = ($('#uniform-subCourseField7 span').attr('class')=='checked') ? 1 : 0;
                         //alert(JSON.stringify(subCourse));
                         if(subCourse['data']['courseID']==null || subCourse['data']['courseID']=='null'){
                             alert("Please select course");
                         }
                         else if(subCourse['data']['subCourseName']==''){
                             alert("Please provide Sub Course name");
                         }
                         else{
                              subCourseDataRec = get(subCourse,'index.php?p=5','json');
                         }
                         //alert(JSON.stringify(subCourseDataRec));
                         if(subCourseDataRec.total>0){
                            alert(subCourseDataRec.total+" Record inserted successfully.");
                            location.href="index.php?p=1&ln=3&t=1";
                            $('#SubCourseNav').val("");
                            $('#SubCourseNav_chzn a span').html("Select Navigation");
                            $('#subCourseField6 textarea').val("");
                            $('#cancelSubCourseData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                   if($('#modeSubCourse').val() == 2){
                      // alert("scourse");

                         subCourse['mode'] = "updateSubCourse";
                         subCourse['data'] = {};
                         subCourse['data']['courseID'] = $('#subCourseField1').val();  //for name
                         subCourse['data']['categoryId'] =$('#subCourseField2').val();  //for title
                         subCourse['data']['categoryId2'] =$('#subCourseField12').val();
                         subCourse['data']['courseNav'] = escape(encodeURIComponent(($('#SubCourseNav').val())));
                         subCourse['data']['urlLink'] = escape(encodeURIComponent(($('#subCourseField3').val())));  //for urlLink
                         subCourse['data']['subCourseName'] = escape(encodeURIComponent(($('#subCourseField4').val())));
                         subCourse['data']['fee'] = parseFloat($('#fee').val(),10)+parseFloat((($('#fee').val()*$('#feetax').val())/100),10);  //for helpline
                         subCourse['data']['tax'] = $('#feetax').val();
                         subCourse['data']['subCourseOrder'] =$('#subCourseField5').val();  //for sms enquiry
                         subCourse['data']['content'] = escape(encodeURIComponent(($('#subCourseField6 textarea').val())));  //for sms enquiry
                         subCourse['data']['active'] = ($('#uniform-subCourseField7 span').attr('class')=='checked') ? 1 : 0;  //for settings
                         subCourse['data']['id'] = $('#idSubCourse').val();  //for id
                         if(subCourse['data']['courseID']==null || subCourse['data']['courseID']=='null'){
                             alert("Please select course");
                         }
                         else if(subCourse['data']['subCourseName']==''){
                             alert("Please provide Sub Course name");
                         }
                         else{
                              subCourseDataRec = get(subCourse,'index.php?p=5','json');
                         }
                         if(subCourseDataRec.total>0){

                            alert(subCourseDataRec.total+" Record Updated successfully.");
                            location.href="index.php?p=1&ln=3&t=1";
                            $('#subCourseField6 textarea').val("");
                            $('#subCourseField1').val("");
                            $('#subCourseField1_chzn a span').html("Select Course");
                            $('#SubCourseNav').val("");
                            $('#SubCourseNav_chzn a span').html("Select Navigation");

                            $('#subCourseField2').val("");
                            $('#subCourseField2_chzn a span').html("Select Category");
                            $('#cancelSubCourseData').click();
                            this.getDataAll();
                         }else{

                            alert("No record updated.");
                         }
                  }
             }

             this.editData = function(subCourseId,courseId,courseName,categoryId,categoryName,urlLink,subCourseName,content,subCourseOrder,active,fee,tax,categoryId2,courseNav){
                 var actualFee=(100/(100+parseFloat(tax,10)))*parseFloat(fee);
                 $('#editSubCourse').html('Edit');
                 $('#modeSubCourse').val(2);
                 $('#idSubCourse').val(subCourseId);
                 $('#subCourseField1').val(courseId);
                 this.courseChange(courseId);
                 this.courseNavChange(courseNav);
                 this.category1change(categoryId);
                 $('#subCourseField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 if(categoryId!=0){
                    $('#subCourseField2').val(categoryId);
                 if(categoryId2!=0)
                    $('#subCourseField12').val(categoryId2);
                 }
                 $('#subCourseField3').val(unescape(decodeURIComponent(urlLink)));
                 $('#SubCourseNav').val(unescape(decodeURIComponent(courseNav)));
                 $('#subCourseField4').val(unescape(decodeURIComponent(subCourseName)));
                 if(tax==''||tax=='NULL' || tax=='null'){
                        $('#fee').val(unescape(decodeURIComponent(fee)));
                        $('#toatlFee').val(unescape(decodeURIComponent(fee)));
                        $('#feetax').val(unescape(decodeURIComponent(0)));
                 }else{
                        $('#fee').val(unescape(decodeURIComponent(actualFee)));
                        $('#toatlFee').val(unescape(decodeURIComponent(fee)));
                        $('#feetax').val(unescape(decodeURIComponent(tax)));
                 }
                 $('#subCourseField5').val(subCourseOrder);
                 $('#subCourseField6 textarea').val(unescape(decodeURIComponent(content)));
//                 $('#fee').val(sunescape(decodeURIComponent(fee)));
                 if(active == 1){
                    $('#uniform-subCourseField7 span').attr('class','checked');
                    $('#subCourseField7').attr('checked','checked');
                }else{
                    $('#uniform-subCourseField7 span').attr('class','');
                    $('#courseField8').removeAttr('checked');
                }
//                                 if(coursenavName!='' || coursenavName!='null' || coursenavName!=null){
//                    $('#SubCourseNav_chzn  a span').html(unescape(decodeURIComponent(coursenavName)));
//                 }
//                 else{
//                     $('#SubCourseNav_chzn  a span').html(unescape(decodeURIComponent("Select Course Navigation")));
//                 }
//                 $('#subCourseField2').val(categoryId);
//                 if(categoryName!==''){
//                    $('#subCourseField2_chzn a span').html(unescape(decodeURIComponent(categoryName)));
//                 }
//                 else{
//                     $('#subCourseField2_chzn a span').html("Select Category1");
//                 }
//                 $('#subCourseField12').val(categoryId2);
//                 if(subCategoryName!==''){
//                    $('#subCourseField12_chzn a span').html(unescape(decodeURIComponent(subCategoryName)));
//                 }
//                 else{
//                     $('#subCourseField12_chzn a span').html(("Select Category2"));
//                 }
             }

             this.deleteData = function(id){
                 var result=confirm("Are you sure you want to delete this");
                 if(!result){
                     return False;
                 }
                 else{
                 var subCourse = {};
                 subCourse['mode'] = "deleteSubCourse";
                 subCourse['data'] = {};
                 subCourse['data']['subCourseid'] = id;  //for id
                 var subCourseDataRec = get(subCourse,'index.php?p=5','json');
                 if(subCourseDataRec.total>0){
                    alert(subCourseDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record updated.");
                 }
             }
             }
             this.courseChange=function(id){
                                 this.courseNavChange(0);
                                 var changeCourse={};
                                 var option="<select id='SubCourseNav' class='chzn-select' onchange='callSubCourseData.courseNavChange(this.value);'><option selected='selected' disabled='disabled' value=''>Select  Navigation</option>";
                                 changeCourse['mode']='changeAddEventCourse';
                                 changeCourse['data']={};
                                 changeCourse['data']['id']=id;
                                 var courseData = get(changeCourse,'index.php?p=5','json');

                                 for(var i=0;i<courseData['TOTAL'];i++){
                                     option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
                                 }
                                 option+="</select>"
                                 $('#SubCourseNavDiv').html(option);
                         }

              this.courseNavChange=function(id){
                    this.category1change(0);
                    var category={};
                    var option='<select id="subCourseField2" class="chzn-select" onchange="callSubCourseData.category1change(this.value);"><option selected="selected" disabled="disabled" value="">Select Category</option>'
                    category['mode'] = "getCategory1ByNav";
                    category['data'] = {};
                    category['data']['courseId'] = id;  //for id
                    var courseData = get(category,'index.php?p=5','json');
                    for(var i=0;i<courseData['TOTAL'];i++){
                         option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
                    }
                     option+="</select>";
                     $('#Subcategory1').html(option);
            }
           this.category1change=function(id){
            var category={};
            category['mode'] = "getCategory2";
                    category['data'] = {};
                    var option='<select id="subCourseField12" class="chzn-select" ><option selected="selected" disabled="disabled" value="">Select Category2</option>'
                    category['data']['cId'] = id;  //for id

                    var courseData = get(category,'index.php?p=5','json');
                    for(var i=0;i<courseData['TOTAL'];i++){
                         option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
                     }
                     option+="</select>";

                     $('#Subcategory2').html(option);
        }
                 }
                    $('#cancelSubCourseData').click(function(){
                         $('#modeSubCourse').val(1);
                         $('#idSubCourse').val("");
                         $('#subCourseField1_chzn a span').html("Select Course");
                         clearAll('subCourse');

                     });
                     
                     
                     
                     
                     


/********* SUBCOURSE END *******/


/********* NAVIGATION START *******/
          var navigationData = function(){

             this.getDataAll = function(){



                    var navigation = {};
                     navigation['mode'] = "getAllNavigation";
                     var navigationDataRec = get(navigation,'index.php?p=5','json');
                     if(navigationDataRec.total>0){
                         var str = "";
                         $('#navigationData').html('');
                         for(var i=0;i<navigationDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(navigationDataRec.name[i])) + '</td>';

                            str = str + '<td>' + unescape(decodeURIComponent(navigationDataRec.urlLink[i])) + '</td>';
                           str = str + '<td>' + navigationDataRec.order[i] + '</td>';
                            if(navigationDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#navigationStart\',callNavigationData.editData('+navigationDataRec.id[i]+',\''+escape(navigationDataRec.name[i])+'\',\''+escape(navigationDataRec.order[i])+'\',\''+navigationDataRec.urlLink[i]+'\',\''+navigationDataRec.active[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callNavigationData.deleteData('+navigationDataRec.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#navigationData').append(str);
                         }
                     }else{
                         $('#navigationData').html("No Content Found.");
                     }




             }

             this.saveData = function(){

                 var navigation = {};
                 var navigationDataRec;

                if($('#modeNavigation').val() == 1){

                         navigation['mode'] = "saveNavigation";
                         navigation['data'] = {};
                         navigation['data']['naviagtionName'] = escape(encodeURIComponent($('#navigationField1').val()));  //for name
                         navigation['data']['naviagtionOrder'] = $('#navigationField2').val();  //for title
                         navigation['data']['urlLink'] = escape(encodeURIComponent($('#navigationField3').val()));  //for urlLink
                         navigation['data']['active'] = ($('#uniform-navigationField4 span').attr('class')=='checked') ? 1 : 0;  //for settings
                         if( navigation['data']['naviagtionName']!=''){
                                navigationDataRec = get(navigation,'index.php?p=5','json');
                         }
                         else{
                                alert("Please provide Navigation Name");
                         }
                         if(navigationDataRec.total>0){
                            alert(navigationDataRec.total+" Record inserted successfully.");
                            location.href="index.php?p=2&ln=4&t=2";
                            $('#cancelNavigationData').click();

                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                   if($('#modeNavigation').val() == 2){

                         navigation['mode'] = "updateNavigation";
                         navigation['data'] = {};
                         navigation['data']['naviagtionName'] = escape(encodeURIComponent($('#navigationField1').val()));  //for name
                         navigation['data']['naviagtionOrder'] = $('#navigationField2').val();  //for title
                         navigation['data']['urlLink'] = escape(encodeURIComponent($('#navigationField3').val()));  //for urlLink
                         navigation['data']['active'] = ($('#uniform-navigationField4 span').attr('class')=='checked') ? 1 : 0;   //for settings
                         navigation['data']['id'] = $('#idNavigation').val();  //for id
                         if( navigation['data']['naviagtionName']!=''){
                                navigationDataRec = get(navigation,'index.php?p=5','json');
                         }
                         else{
                                alert("Please provide Navigation Name");
                         }

                         if(navigationDataRec.total>0){

                            alert(navigationDataRec.total+" Record Updated successfully.");
                            location.href="index.php?p=2&ln=4&t=2";

                            $('#cancelNavigationData').click();

                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }


             }

             this.editData = function(id,name,order,urlLink,active){
                 $('#editNavigation').html('Edit');
                 $('#modeNavigation').val(2);
                 $('#idNavigation').val(id);
                 $('#navigationField1').val(unescape(decodeURIComponent(name)));
                 $('#navigationField3').val(unescape(decodeURIComponent(urlLink)));
                 $('#navigationField2').val(order);
                 if(active == 1){
                    $('#uniform-navigationField4 span').attr('class','checked');
                    $('#navigationField4').attr('checked','checked');
                }else{
                    $('#uniform-navigationField4 span').attr('class','');
                    $('#navigationField4').removeAttr('checked');
                }
             }

             this.deleteData = function(id){
                 var result=confirm("Are you sure you want to delete this");
                 if(!result){
                     return false;
                 }else{
                 var navigation = {};
                 navigation['mode'] = "deleteNavigation";
                 navigation['data'] = {};
                 navigation['data']['id'] = id;  //for id
                 var navigationDataRec = get(navigation,'index.php?p=5','json');
                 if(navigationDataRec.total>0){
                    alert(navigationDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
             }
         }

         $('#cancelNavigationData').click(function(){
             $('#modeNavigation').val(1);
             $('#editNavigation').html('Add');
             $('#idNavigation').val("");
             clearAll();
         });

/********* NAVIGATION END *******/



/*********MAIN NAVIGATION START *********/


var mainNavigationData = function(){

             this.getDataAll = function(){
                    var mainNavigation = {};
                     mainNavigation['mode'] = "getAllMainNavigation";
                     var mainNavigationDataRec = get(mainNavigation,'index.php?p=5','json');
                     if(mainNavigationDataRec.total>0){

                         var str = "";
                         $('#mainNavigationData').html('');
                         for(var i=0;i<mainNavigationDataRec.total;i++){


                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(mainNavigationDataRec.name[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(mainNavigationDataRec.navigationName[i])) + '</td>';
                            str = str + '<td>' + mainNavigationDataRec.order[i] + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(mainNavigationDataRec.urlLink[i])) + '</td>';
                            if(mainNavigationDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#mainNavigationStart\',callMainNavigationData.editData('+mainNavigationDataRec.mainNavigationId[i]+',\''+escape(mainNavigationDataRec.name[i])+'\',\''+mainNavigationDataRec.navigationId[i]+'\',\''+escape(mainNavigationDataRec.navigationName[i])+'\',\''+mainNavigationDataRec.order[i]+'\',\''+escape(mainNavigationDataRec.urlLink[i])+'\',\''+mainNavigationDataRec.active[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callMainNavigationData.deleteData('+mainNavigationDataRec.mainNavigationId[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#mainNavigationData').append(str);
                         }
                     }else{
                         $('#mainNavigationData').html("No Content Found.");
                     }




             }

             this.saveData = function(){

                 var mainNavigation = {};
                 var mainNavigationDataRec;

                if($('#modeMainNavigation').val() == 1){

                         mainNavigation['mode'] = "saveMainNavigation";
                         mainNavigation['data'] = {};
                         mainNavigation['data']['name'] = escape(encodeURIComponent($('#mainNavigationField1').val()));
                         mainNavigation['data']['navigationId'] = $('#mainNavigationField2').val();
                         mainNavigation['data']['order'] = $('#mainNavigationField4').val();
                         mainNavigation['data']['urlLink'] = escape(encodeURIComponent($('#mainNavigationField5').val()));
                         mainNavigation['data']['active'] = ($('#uniform-mainNavigationField6 span').attr('class')=='checked') ? 1 : 0;
                         if(mainNavigation['data']['name']!=''){
                                mainNavigationDataRec = get(mainNavigation,'index.php?p=5','json');
                         }
                         else{
                             alert("Please provide Navigation name");
                         }
                         if(mainNavigationDataRec.total>0){
                            alert(mainNavigationDataRec.total+" Record inserted successfully.");
                            location.href="index.php?p=2&ln=5&t=2";
                            $('#mainNavigationField2').val("");
                            $('#mainNavigationField2_chzn a span').html("Select Navigation");

                            $('#cancelMainNavigationData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                   if($('#modeMainNavigation').val() == 2){

                         mainNavigation['mode'] = "updateMainNavigation";
                         mainNavigation['data'] = {};
                         mainNavigation['data']['name'] = escape(encodeURIComponent($('#mainNavigationField1').val()));
                         mainNavigation['data']['navigationId'] = $('#mainNavigationField2').val();
                         mainNavigation['data']['order'] = $('#mainNavigationField4').val();
                         mainNavigation['data']['urlLink'] = escape(encodeURIComponent($('#mainNavigationField5').val()));
                         mainNavigation['data']['active'] = ($('#uniform-mainNavigationField6 span').attr('class')=='checked') ? 1 : 0;
                         mainNavigation['data']['mainNavigationId'] = $('#idMainNavigation').val();

                         mainNavigationDataRec = get(mainNavigation,'index.php?p=5','json');
                         if(mainNavigationDataRec.total>0){

                            alert(mainNavigationDataRec.total+" Record Updated successfully.");
                            location.href="index.php?p=2&ln=5&t=2";

                            $('#mainNavigationField2').val("");
                            $('#mainNavigationField2_chzn a span').html("Select Navigation");

                            $('#cancelMainNavigationData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }


             }

             this.editData = function(mainNavigationId,name,navigationId,navigationName,order,urlLink,active){
                 $('#editMainNavigation').html('Edit');
                 $('#modeMainNavigation').val(2);
                 $('#idMainNavigation').val(mainNavigationId);

                 $('#mainNavigationField1').val(unescape(decodeURIComponent(name)));

                 $('#mainNavigationField2').val(navigationId);
                 $('#mainNavigationField2_chzn a span').html(unescape(decodeURIComponent(navigationName)));
                 //$('#mainNavigationField3').val(level);
                  //$('#mainNavigationField3_chzn a span').html(level);
                 $('#mainNavigationField4').val(order);

                 $('#mainNavigationField5').val(unescape(decodeURIComponent(urlLink)));



                 if(active == 1){

                    $('#uniform-mainNavigationField6 span').attr('class','checked');
                    $('#mainNavigationField6').attr('checked','checked');
                }else{
                    $('#uniform-mainNavigationField6 span').attr('class','');
                    $('#mainNavigationField6').removeAttr('checked');
                }


             }

             this.deleteData = function(id){
                 var result=confirm("Are you sure you want to delete this");
                 if(!result){
                     return false;
                 }else{
                 var mainNavigation = {};
                 mainNavigation['mode'] = "deleteMainNavigation";
                 mainNavigation['data'] = {};
                 mainNavigation['data']['id'] = id;
                 var mainNavigationDataRec = get(mainNavigation,'index.php?p=5','json');

                 if(mainNavigationDataRec.total>0){

                    alert(mainNavigationDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
             }
         }

         $('#cancelMainNavigationData').click(function(){
             $('#modeMainNavigation').val(1);
             $('#editMainNavigation').html('Add ');
              $('#mainNavigationField2_chzn a span').html(unescape(decodeURIComponent('')));
             $('#idMainNavigation').val("");
             clearAll();
         });

/*********MAIN NAVIGATION END *********/

/**********COURSE NAVIGATION START********/

         var courseNavigationData = function(){
             this.changeCourseNav=function(id){
                         var changeCourse={};
                         var option="<label class='control-label' for='select01'><span style='color: red;'> * </span>Select Course Navigation</label><div class='controls'><select id='courseNavigationField2' class='chzn-select'><option selected='selected' disabled='disabled' value=''>Select Course Navigation</option>";
                         changeCourse['mode']='changeAddEventCourse';
                         changeCourse['data']={};
                         changeCourse['data']['id']=id;
                         var courseData = get(changeCourse,'index.php?p=5','json');
                         for(var i=0;i<courseData['TOTAL'];i++){
                         option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
                    }
                        option+="</div>"
                        $('#coursenavdiv').html(option);                        
                        }
             
             this.getDataAll = function(){

                    var courseNavigation = {};
                     courseNavigation['mode'] = "getAllCourseNavigation";
                     var courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                     if(courseNavigationDataRec.total>0){
                        var str = "";
                         $('#courseNavigationData').html('');
                         for(var i=0;i<courseNavigationDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(courseNavigationDataRec.courseName[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(courseNavigationDataRec.name[i])) + '</td>';
//                              str = str + '<td>' + courseNavigationDataRec.order[i] + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(courseNavigationDataRec.urlLink[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(courseNavigationDataRec.backgroundImageupload[i])) + '</td>';
                            if(courseNavigationDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#courseNavigationStart\',callCourseNavigationData.editData('+courseNavigationDataRec.id[i]+',\''+courseNavigationDataRec.name[i]+'\',\''+courseNavigationDataRec.urlLink[i]+'\',\''+courseNavigationDataRec.order[i]+'\',\''+courseNavigationDataRec.active[i]+'\',\''+courseNavigationDataRec.courseId[i]+'\',\''+courseNavigationDataRec.courseName[i]+'\',\''+escape(courseNavigationDataRec.content[i])+'\',\''+ unescape(decodeURIComponent(courseNavigationDataRec.backgroundImageupload[i]))+'\',\''+escape(courseNavigationDataRec.backgroundImagecontent[i])+'\');"></i>' + '</td>';
//                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callCourseNavigationData.deleteData('+courseNavigationDataRec.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#courseNavigationData').append(str);
                         }
                     }else{
                         $('#courseNavigationData').html("No Content Found.");
                     }




             }

             this.saveData = function(){
                 var courseNavigation = {};
                 var courseNavigationDataRec;

                if($('#modeCourseNavigation').val() == 1){

                         courseNavigation['mode'] = "saveCourseNavigation";
                         courseNavigation['data'] = {};
                         courseNavigation['data']['courseId'] = $('#courseNavigationField1').val();  //for name
                         courseNavigation['data']['name'] = $('#courseNavigationField2').val();  //for title
                         courseNavigation['data']['order'] = $('#courseNavigationField3').val();  //for urlLink
                         courseNavigation['data']['content'] = $('#courseNavigationField6 textarea').val();  //for ckeditor content added by shruti
                         courseNavigation['data']['content']=escape(encodeURIComponent(courseNavigation['data']['content']));
                         courseNavigation['data']['urlLink'] = escape(encodeURIComponent($('#courseNavigationField4').val()));  //for helpline
                         courseNavigation['data']['backgroundImageupload']=escape(encodeURIComponent(($('#courseNavigationField7').val())));  //for background image
                         var dotIndex = courseNavigation['data']['backgroundImageupload'].lastIndexOf('.'); //to get last index
                         var ext = courseNavigation['data']['backgroundImageupload'].substring(dotIndex); // to get extension of image
                         var path = courseNavigation['data']['backgroundImageupload'];
                         courseNavigation['data']['backgroundImageupload'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         courseNavigation['data']['backgroundImagecontent'] = $('#courseNavigationField8 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         courseNavigation['data']['backgroundImagecontent']=escape(encodeURIComponent(courseNavigation['data']['backgroundImagecontent']));
                         courseNavigation['data']['active'] = ($('#uniform-courseNavigationField5 span').attr('class')=='checked') ? 1 : 0;
                         if(courseNavigation['data']['courseId']==null || courseNavigation['data']['courseId']=='null'){
                             alert("Please select Course");
                         }
                         else if(courseNavigation['data']['name']==null || courseNavigation['data']['name']=='null'){
                             alert("Please provide Navigation name");
                         }
                         else if(courseNavigation['data']['backgroundImageupload']!=''){
                                if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                    alert("Please upload image in given format");
                                }
                                else if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                        courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                                }
                                else {
                                    alert("Please upload image in given dimensions");
                                }
                         }
                         else {
                             courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                         }
                         if(courseNavigationDataRec.total>0){

                                     var lastInsertedId =courseNavigationDataRec.lastInsertedId;
                                            if ( $("#courseNavigationField7").val()!=""){
                                                     var datas=$('#courseNavigationField7')[0].files;
                                                     if (window.FormData) {
                                                        var formdata = new FormData();
                                                     }
                                                     var i = 0, len = datas.length, img, reader, file1;

                                                     for ( ; i < len; i++ ) {
                                                        file1 = datas[i];

                                                            if ( window.FileReader ) {
                                                                    reader = new FileReader();
                                                                    reader.onloadend = function (e) {
                                                                    };
                                                                    reader.readAsDataURL(file1);
                                                            }
                                                            if (formdata) {
                                                                    formdata.append("file[]", file1);
                                                            }
                                                      }
                                                            if (formdata) {

                                                                  $.ajax({
                                                                        url: "index.php?p=6&m=b&lastInsertedId="+lastInsertedId,
                                                                        type: "POST",
                                                                        data: formdata,
                                                                        processData: false,
                                                                        contentType: false,
                                                                        async:false,
                                                                        error:function(res){
                                                                           // alert(res);
                                                                        },
                                                                        success: function (res) {
                                                                             //alert(res);
                                                                        }
                                                                   });
                                                              }
                                            }
                                                alert(courseNavigationDataRec.total+" Record inserted successfully.");

                                                $('#courseNavigationField1').val("");
                                                $('#courseNavigationField1_chzn a span').html("Select Course");
//                                                $('#productField2').val("");
//                                                $('#productField2').html("Select Product Category");
                                                $('#cancelCourseNavigationData').click();

                                                this.getDataAll();
                                 }else{
                                    alert("No record inserted.");
                                 }
//                         if(courseNavigationDataRec.total>0){
//                            alert(courseNavigationDataRec.total+" Record inserted successfully.");
////                            $('#courseNavigationField6 textarea').val("");
//                            location.href="index.php?p=2&ln=6&t=2";
//                            $('#courseNavigationField1').val("");
//
//                             $('#courseNavigationField1_chzn a span').html("Select Course");
//
//                            $('#cancelCourseNavigationData').click();
//                            this.getDataAll();
//                         }else{
//                            alert("No record inserted.");
//                         }
                  }

                   if($('#modeCourseNavigation').val() == 2){
                         courseNavigation['mode'] = "updateCourseNavigation";
                         courseNavigation['data'] = {};
                         courseNavigation['data']['courseId'] = $('#courseNavigationField1').val();  //for name
                         courseNavigation['data']['name'] = $('#courseNavigationField2').val();  //for title
                         courseNavigation['data']['order'] = $('#courseNavigationField3').val();  //for urlLink
                         courseNavigation['data']['urlLink'] = escape(encodeURIComponent($('#courseNavigationField4').val()));  //for helpline
                         courseNavigation['data']['content'] = $('#courseNavigationField6 textarea').val();  //for ckeditor content added by shruti
                         courseNavigation['data']['content'] = escape(encodeURIComponent(courseNavigation['data']['content']));
                         //courseNavigation['data']['active'] = ($('#courseNavigationField5').is(":checked")) ? 1 : 0;
                         if($('#courseNavigationField7').val()==''){
                            courseNavigation['data']['backgroundImageupload'] = $('#upldbackgroundImage').val();
                         }else{
                             courseNavigation['data']['backgroundImageupload'] = $('#courseNavigationField7').val();
                         }
                         var dotIndex = courseNavigation['data']['backgroundImageupload'].lastIndexOf('.'); //to get last index
                         var ext = courseNavigation['data']['backgroundImageupload'].substring(dotIndex); // to get extension of image
                         var path = courseNavigation['data']['backgroundImageupload'];
                         courseNavigation['data']['backgroundImageupload'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         courseNavigation['data']['backgroundImagecontent'] = $('#courseNavigationField8 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         courseNavigation['data']['backgroundImagecontent']=escape(encodeURIComponent(courseNavigation['data']['backgroundImagecontent']));
                         courseNavigation['data']['active'] = ($('#uniform-courseNavigationField5 span').attr('class')=='checked') ? 1 : 0;  //for settings
                         courseNavigation['data']['id'] = $('#idCourseNavigation').val();  //for id
                         if(courseNavigation['data']['courseId']==null || courseNavigation['data']['courseId']=='null'){
                             alert("Please select Course");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                             alert("Please upload image in given format");
                         }
                         else if(courseNavigation['data']['name']==null || courseNavigation['data']['name']=='null'){
                             alert(courseNavigation['data']['name']);
                             alert("Please provide Navigation name");
                         }
                         else if(courseNavigation['data']['backgroundImageupload']!=''){
                                if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                    alert("Please upload image in given format");
                                }
                                else if(imgWidth!='' && imgHeight!=''){
                                    if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                        courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                                    }
                                    else {
                                        alert("Please upload image in given dimensions");
                                    }
                                 }
                                else{
                                     courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                                }
                         }
                         else {
                             courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                         }
//                                 if(courseNavigationDataRec.total>0){ /* commented by Shruti for update image even having same image name  */

                                     var lastInsertedId =courseNavigationDataRec.lastInsertedId;
                                            if ( $("#courseNavigationField7").val()!=""){
                                                     var datas=$('#courseNavigationField7')[0].files;
                                                     if (window.FormData) {
                                                        var formdata = new FormData();
                                                     }
                                                     var i = 0, len = datas.length, img, reader, file1;

                                                     for ( ; i < len; i++ ) {
                                                        file1 = datas[i];

                                                            if ( window.FileReader ) {
                                                                    reader = new FileReader();
                                                                    reader.onloadend = function (e) {
                                                                    };
                                                                    reader.readAsDataURL(file1);
                                                            }
                                                            if (formdata) {
                                                                    formdata.append("file[]", file1);
                                                            }
                                                      }
                                                            if (formdata) {

                                                                  $.ajax({
                                                                        url: "index.php?p=6&m=b&lastInsertedId="+lastInsertedId,
                                                                        type: "POST",
                                                                        data: formdata,
                                                                        processData: false,
                                                                        contentType: false,
                                                                        async:false,
                                                                        error:function(res){
                                                                           // alert(res);
                                                                        },
                                                                        success: function (res) {
                                                                             //alert(res);
                                                                        }
                                                                   });
                                                              }
                                            }


                                                alert(" Record updated successfully.");

                                                $('#courseNavigationField1').val("");
                                                $('#courseNavigationField1_chzn a span').html("Select Course");
                                                $('#uniform-courseNavigationField7 span.filename').html("");
//                                                $('#productField2').val("");
//                                                $('#productField2').html("Select Product Category");
                                                $('#cancelCourseNavigationData').click();

                                                this.getDataAll();
                                                /* commented by Shruti for update image even having same image name  */
//                                 }else{
//                                    alert("No record inserted.");
//                                 }
//                         courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
//                         if(courseNavigationDataRec.total>0){
//                            alert(courseNavigationDataRec.total+" Record Updated successfully.");
//                            location.href="index.php?p=2&ln=6&t=2";
//                            $('#courseNavigationField1').val("");
//                            $('#courseNavigationField1_chzn a span').html("Select Course");
//
//                            $('#cancelCourseNavigationData').click();
//                            this.getDataAll();
//                         }
//                         else{
//                            alert("No record inserted.");
//                         }
                  }


             }

             this.editData = function(courseNavigationId,name,urlLink,order,active,courseId,courseName,content,backgroundImage,backgroundImagecontent){
                 $('#editCourseNavigation').html('Edit');
                 $('#modeCourseNavigation').val(2);
                 $('#idCourseNavigation').val(unescape(decodeURIComponent(courseNavigationId)));
                 $('#courseNavigationField1').val(courseId);
                 $('#courseNavigationField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 this.changeCourseNav(courseId);
                 $('#courseNavigationField3').val(order);
                 $('#courseNavigationField4').val(unescape(decodeURIComponent(urlLink)));
                 $('#courseNavigationField6 textarea').val(unescape(decodeURIComponent(content)));
                 $('#upldbackgroundImage').val(backgroundImage);
                 $('#uniform-courseNavigationField7 span.filename').html(backgroundImage);
                 $('#courseNavigationField8 textarea').val(unescape(decodeURIComponent(backgroundImagecontent)));
                 if(active == 1){

                    $('#uniform-courseNavigationField5 span').attr('class','checked');
                    $('#courseNavigationField5').attr('checked','checked');
                }else{
                    $('#uniform-courseNavigationField5 span').attr('class','');
                    $('#courseNavigationField5').removeAttr('checked');
                }
                $('#courseNavigationField2').val(courseNavigationId);

             }

             this.deleteData = function(id){
                 var result=confirm("Are you sure you want to delete this");
                 if(!result){
                     return false;
                 }else{
                 var courseNavigation = {};
                 courseNavigation['mode'] = "deleteCourseNavigation";
                 courseNavigation['data'] = {};
                 courseNavigation['data']['id'] = id;
                 var courseNavigationDataRec = get(courseNavigation,'index.php?p=5','json');
                 if(courseNavigationDataRec.total>0){
                    alert(courseNavigationDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
             }
         }

         $('#cancelCourseNavigationData').click(function(){
             $('#modeCourseNavigation').val(1);
             $('#idCourseNavigation').val("");
             $('#courseNavigationField6 textarea').val(""); //added by shruti
             $('#courseNavigationField8 textarea').val(""); //added by shruti
             $('#courseNavigationField1_chzn a span').html(unescape(decodeURIComponent('')));
             $('#editCourseNavigation').html('Add');
             clearAll();
         });
         /*******To FIND DIMENSION OF NAVIGATION IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#courseNavigationField7").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/**********COURSE NAVIGATION END********/

 /******** SUBCOURSE HIGHLIGHT START*********/
         var subCourseHighlightData = function(){
             this.getDataAll = function(){
                    var subCourseHigh = {};
                     subCourseHigh['mode'] = "getAllSubCourseHighlight";
                     var subCourseHighlight = get(subCourseHigh,'index.php?p=5','json');
                     if(subCourseHighlight.total>0){
                         var str = "";
                         $('#subCourseHighlightData').html('');
                         for(var i=0;i<subCourseHighlight.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(subCourseHighlight.subCourseName[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(subCourseHighlight.order[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(subCourseHighlight.content[i])) + '</td>';
                            if(subCourseHighlight.setting[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                             str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callSubCourseHighlightData.editData('+subCourseHighlight.id[i]+',\''+subCourseHighlight.subCourseId[i]+'\',\''+escape(subCourseHighlight.subCourseName[i])+'\',\''+subCourseHighlight.order[i]+'\',\''+escape(subCourseHighlight.content[i])+'\',\''+subCourseHighlight.setting[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callSubCourseHighlightData.deleteData('+subCourseHighlight.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#subCourseHighlightData').append(str);
                         }
                     }else{
                         $('#subCourseHighlightData').html("No Content Found.");
                     }
             }

             this.saveData = function(){
                 var subCourseHigh = {};
                 var  subCourseHighlight;
                if($('#modeSubCourseHighlight').val() == 1){
                         subCourseHigh['mode'] = "saveSubCourseHighlight";
                         subCourseHigh['data'] = {};
                         subCourseHigh['data']['subCourseId'] = $('#contentCourse').val();
                         subCourseHigh['data']['order'] = $('#contentOrder').val();
                         subCourseHigh['data']['content'] = escape(encodeURIComponent($('#contentArea textarea').val()));
                         subCourseHigh['data']['setting'] = ($('#uniform-contentSetting span').attr('class')=='checked') ? 1 : 0;
                         if(subCourseHigh['data']['subCourseId']!=null){
                                subCourseHighlight = get(subCourseHigh,'index.php?p=5','json');
                         }
                         else{
                                alert("Please select atleast one Sub Course");
                         }
                         if(subCourseHighlight.total>0){
                            alert(subCourseHighlight.total+" Record inserted successfully.");
                            $('#cancelSubCourseHighlight').click();

                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                   if($('#modeSubCourseHighlight').val() == 2){
                         subCourseHigh['mode'] = "updateSubCourseHighlight";
                         subCourseHigh['data'] = {};
                         subCourseHigh['data']['subCourseId'] = $('#contentCourse').val();
                         subCourseHigh['data']['order'] = $('#contentOrder').val();
                         subCourseHigh['data']['content'] = escape(encodeURIComponent($('#contentArea textarea').val()));
                         subCourseHigh['data']['setting'] = ($('#uniform-contentSetting span').attr('class')=='checked') ? 1 : 0;
                         subCourseHigh['data']['id'] = $('#idSubCourseHigh').val();
                         var subCourseHighlight = get(subCourseHigh,'index.php?p=5','json');
                         if(subCourseHighlight.total>0){
                         alert(subCourseHighlight.total+" Record Updated successfully.");
                            $('#cancelSubCourseHighlight').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }


             }

             this.editData = function(id,subCourseId,subCourseName,order,content,setting){
                 $("#editSub").html('Edit');
                 $('#modeSubCourseHighlight').val(2);
                 $('#idSubCourseHigh').val(id);
                 $('#contentCourse').val(subCourseId);
                 $('#contentCourse_chzn a span').html(unescape(decodeURIComponent(subCourseName)));
                 $('#contentOrder').val(order);
                 $('#contentArea textarea').val(unescape(decodeURIComponent(content)));


                 if(setting == 1){

                    $('#uniform-contentSetting span').attr('class','checked');
                    $('#contentSetting').attr('checked','checked');
                }else{
                    $('#uniform-contentSetting span').attr('class','');
                    $('#contentSetting').removeAttr('checked');
                }


             }

             this.deleteData = function(id){
                 var result=confirm("Are you sure you want to delete this");
                 if(!result){
                     return false;
                 }else{
                 var subCourseHigh = {};
                 subCourseHigh['mode'] = "deleteSubCourseHighlight";
                 subCourseHigh['data'] = {};
                 subCourseHigh['data']['id'] = id;
                    var r=confirm("Are you sure you want to delete this record?");
                    if(r==true){
                 var subCourseHighlight = get(subCourseHigh,'index.php?p=5','json');
                 if(subCourseHighlight.total>0){
                   alert(subCourseHighlight.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }

 }
             }
         }
            $('#cancelSubCourseHighlight').click(function(){
             $('#contentArea textarea').val("");
             $('#contentCourse').val("");
             $('#contentCourse_chzn a span').html("Select Sub-Course");
             $('#modeSubCourseHighlight').val(1);
              $("#editSub").html('Add');
             $('#idSubCourseHigh').val("");
             clearAll();
         });

/******** SUBCOURSE HIGHLIGHT END*********/

/**************ADD EVENT START******************/
    var addEventData = function(){
             this.getDataAll = function(){
                    var addEvent = {};
                     addEvent['mode'] = "getAllAddEvent";
                     var eventData = get(addEvent,'index.php?p=5','json');
                     if(eventData.total>0){
                         var str = "";
                         $('#addEventData').html('');
                         for(var i=0;i<eventData.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(eventData.courseName[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(eventData.name[i])) + '</td>';
                            str = str + '<td>' + eventData.year[i]+ '</td>';
                            str = str + '<td>' + eventData.order[i] + '</td>';
                            if(eventData.setting[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#eventStart\',callAddEventData.editData('+eventData.id[i]+',\''+eventData.courseId[i]+'\',\''+escape(eventData.courseName[i])+'\',\''+escape(eventData.name[i])+'\',\''+eventData.order[i]+'\',\''+eventData.setting[i]+'\',\''+eventData.year[i]+'\',\''+eventData.courseNav[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddEventData.deleteData('+eventData.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#addEventData').append(str);
                         }
                     }else{
                         $('#addEventData').html("No Content Found.");
                     }
             }

             this.saveData = function(){
                 var addEvent = {};
                 var  eventData;
                if($('#modeAddEvent').val() == 1){
                         addEvent['mode'] = "saveAddEventData";
                         addEvent['data'] = {};
                         addEvent['data']['course'] = $('#eventCourse').val();
                         addEvent['data']['courseNav'] = $('#eventNavigation').val();
                         addEvent['data']['year'] = $('#eventYear').val();
                         addEvent['data']['name'] = escape(encodeURIComponent($('#eventName').val()));
                         addEvent['data']['order'] = $('#eventOrder').val();
                         addEvent['data']['setting'] = ($('#uniform-eventSetting span').attr('class')=='checked') ? 1 : 0;
                         if(addEvent['data']['course']==null || addEvent['data']['course']=='null'){
                             alert("Please select Course");
                         }
                         else if(addEvent['data']['courseNav']==null || addEvent['data']['courseNav']=='null'){
                             alert("Please select Course Navigation");
                         }
                         else if(addEvent['data']['name']==''){
                             alert("Please provide Event name");
                         }
                         else if(addEvent['data']['year']==null || addEvent['data']['year']=='null'){
                             alert("Please provide Event year");
                         }
                         else{
                            eventData = get(addEvent,'index.php?p=5','json');
                         }
                         if(eventData.total>0){
                            alert(eventData.total+" Record inserted successfully.");
                            $('#cancelAddEventData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }
                    if($('#modeAddEvent').val() == 2){
                         addEvent['mode'] = "updateAddEvent";
                         addEvent['data'] = {};
                         addEvent['data']['course'] = $('#eventCourse').val();
                         addEvent['data']['courseNav'] = $('#eventNavigation').val();
                         addEvent['data']['year'] = $('#eventYear').val();
                         addEvent['data']['name'] = escape(encodeURIComponent($('#eventName').val()));
                         addEvent['data']['order'] = $('#eventOrder').val();
                         addEvent['data']['setting'] = ($('#uniform-eventSetting span').attr('class')=='checked') ? 1 : 0;
                         addEvent['data']['id'] = $('#idEvent').val();
                         if(addEvent['data']['course']==null && addEvent['data']['course']=='null'){
                             alert("Please select Course");
                         }
                         else if(addEvent['data']['courseNav']==null && addEvent['data']['courseNav']=='null'){
                             alert("Please select Course Navigation");
                         }
                         else if(addEvent['data']['name']==''){
                             alert("Please provide Event name");
                         }
                         else if(addEvent['data']['year']==null && addEvent['data']['year']=='null'){
                             alert("Please provide Event year");
                         }
                         else{
                            eventData = get(addEvent,'index.php?p=5','json');
                         }
                         
                         if(eventData.total>0){
                            alert(eventData.total+" Record Updated successfully.");
                            $('#cancelAddEventData').click();
                            this.getDataAll();
                         }else{
                            alert("No record updated.");
                         }
                    }
             }

             this.editData = function(id,courseId,courseName,name,order,setting,year,courseNav){
                 $('#editEvent').html('Edit');
                 $('#modeAddEvent').val(2);
                 $('#idEvent').val(id);
                 $('#eventCourse').val(courseId);
                 this.changeCourse(courseId);
                 $('#eventCourse_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 $('#eventName').val(unescape(decodeURIComponent(name)));
                 $('#eventOrder').val(order);
                 $('#eventYear_chzn a span').html(unescape(decodeURIComponent(year)));
                 $('#eventYear').val(year);
                 if(setting == 1){
                    $('#uniform-eventSetting span').attr('class','checked');
                    $('#eventSetting').attr('checked','checked');
                }else{
                    $('#uniform-eventSetting span').attr('class','');
                    $('#eventSetting').removeAttr('checked');
                }
                 $('#eventNavigation').val(courseNav);
             }

             this.deleteData = function(id){
                 var addEvent = {};
                 addEvent['mode'] = "deleteAddEventData";
                 addEvent['data'] = {};
                 addEvent['data']['id'] = id;
                    var r=confirm("Are you sure you want to delete this record?");
                    if(r==true){
                 var eventData = get(addEvent,'index.php?p=5','json');
                 if(eventData.total>0){
                  alert(eventData.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
         this.changeCourse=function(id){

             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color: red;'>*</span>Select Course Navigation</label><div class='controls'><select id='eventNavigation' class='chzn-select'><option selected='selected' disabled='disabled' value=''>Select Course Navigation</option>";
             changeCourse['mode']='changeAddEventCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
             }
             option+="</div>"
             $('#eventnavdiv').html(option);
         }
             $('#cancelAddEventData').click(function(){
                 $('#eventCourse').val("");
                 $('#eventCourse_chzn a span').html("Select Course");
                 $('#modeAddEvent').val(1);
                 $('#eventYear').val("");
                 $('#eventYear_chzn a span').html("Select Event Year");
                 $('#editEvent').html('Add');
                 $('#idEvent').val("");
                 clearAll();
            });
         }

    /****************************ADD EVENT END***********************************/


   /**************ADD NEWS START******************/

 var addNewsData = function(){


      this.getDataAll = function(){



                    var news = {};
                     news['mode'] = "getAllNews";
                     var newsData = get(news,'index.php?p=5','json');
                     if(newsData.total>0){
                         var str = "";

                          $('#addNewsData').html('');

                         for(var i=0;i<newsData.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(newsData.courseName[i])) + '</td>';
                            str = str + '<td>' + unescape(newsData.heading[i]) + '</td>';
                            //str = str + '<td>' + unescape(decodeURIComponent(newsData.detail[i])) + '</td>';
                              var date =newsData.date[i];
                           var  newdate = date.split("-").reverse().join("-");
                            str = str + '<td>' +newdate + '</td>';
                            //str = str + '<td>' + newsData.order[i] + '</td>';
                            if(newsData.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#newsStart\',callAddNewsData.editData('+newsData.id[i]+',\''+newsData.courseId[i]+'\',\''+escape(newsData.courseName[i])+'\',\''+escape(newsData.heading[i])+'\',\''+escape(newsData.detail[i])+'\',\''+newsData.date[i]+'\',\''+newsData.order[i]+'\',\''+newsData.active[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddNewsData.deleteData('+newsData.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#addNewsData').append(str);
                         }
                     }else{
                         $('#addNewsData').html("No Content Found.");
                     }

                 }

                this.saveData = function(){
                 var news = {};
                 var newsDataRec;
                       if($('#modeNews').val() == 1){
                         news['mode'] = "saveNews";
//                         var date=$('#newsDate').val();
//                         date = date.replace(/\//g, '-');
                           var date =$('#newsDate').val();
                           var newdate = date.split("/").reverse().join("-");

                         news['data'] = {};
                         news['data']['courseName'] = escape(encodeURIComponent($('#newsCourse').val()));
                         news['data']['heading'] = escape(encodeURIComponent($('#newsHeading').val()));
                         news['data']['detail'] = escape(encodeURIComponent($('#newsDetail').val()));
                         news['data']['date'] =newdate;
                         news['data']['order'] = $('#newsOrder').val();
                         news['data']['active'] = ($('#uniform-newsSetting span').attr('class')=='checked') ? 1 : 0;
                         if(news['data']['courseName']==null || news['data']['courseName'] == 'null'){
                             alert("Please select atleast one Course");
                         }
                         else if(news['data']['heading']==''){
                                 alert("Please provide Heading");
                         }
                         else if(news['data']['date']==''){
                             alert("Please provide Date");
                         }
                         else{
                              newsDataRec = get(news,'index.php?p=5','json');
                         }
                         if(newsDataRec.total>0){
                            alert(newsDataRec.total+" Record inserted successfully.");
                            $('#cancelNewsData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }
                     if($('#modeNews').val() == 2){
                         var date =$('#newsDate').val();

                       var newdate = date.split("/").reverse().join("-");
                         news['mode'] = "updateNews";
                         news['data'] = {};
                         news['data']['courseName'] = escape(encodeURIComponent($('#newsCourse').val()));
                         news['data']['heading'] = escape(encodeURIComponent($('#newsHeading').val()));
                         news['data']['detail'] = escape(encodeURIComponent($('#newsDetail').val()));
                         news['data']['date'] = newdate;
                         news['data']['order'] = $('#newsOrder').val();
                         news['data']['active'] = ($('#uniform-newsSetting span').attr('class')=='checked') ? 1 : 0;
                         news['data']['id'] = $('#idNews').val();
                         if(news['data']['courseName']==null || news['data']['courseName'] == 'null'){
                             alert("Please select atleast one Course");
                         }
                         else if(news['data']['heading']==''){
                                 alert("Please provide Heading");
                         }
                         else if(news['data']['date']==''){
                             alert("Please provide Date");
                         }
                         else{
                              newsDataRec = get(news,'index.php?p=5','json');
                         }
                         if(newsDataRec.total>0){
                             alert(newsDataRec.total+" Record Updated successfully.");
                            $('#cancelNewsData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

              }
               this.editData = function(id,courseId,courseName,heading,detail,date,order,active){
                $('#editNews').html('Edit');
                $('#modeNews').val(2);
                 $('#idNews').val(id);
                 $('#newsCourse').val(courseId);
                  $('#newsCourse_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 $('#newsHeading').val(unescape(decodeURIComponent(heading)));
                 $('#newsDetail').val(unescape(decodeURIComponent(detail)));
                 var newdate = date.split("-").reverse().join("/");
                 $('#newsDate').val(newdate);
                 $('#newsOrder').val(order);
                 $('#newsSetting').val(active);



                 if(active == 1){

                    $('#uniform-newsSetting span').attr('class','checked');
                    $('#newsSetting').attr('checked','checked');
                }else{
                    $('#uniform-newsSetting span').attr('class','');
                    $('#newsSetting').removeAttr('checked');
                }


             }

             this.deleteData = function(id){
                 var news = {};
                 news['mode'] = "deleteNewsData";
                 news['data'] = {};
                 news['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var newsData = get(news,'index.php?p=5','json');
                 if(newsData.total>0){
                    alert(newsData.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }

          }
           $('#cancelNewsData').click(function(){
             $('#newsCourse').val("");
             $('#newsCourse_chzn a span').html("Select Course");
             $('#modeNews').val(1);
             $('#idNews').val("");
             $('#editNews').html('Add');
             $('#newsDate').val('');
             clearAll('news');
         });
/**************ADD NEWS END******************/


/************** ADD FILES START******************/

        var addFileData = function(){

            this.getDataAll = function(){



                    var file = {};
                     file['mode'] = "getAllFile";
                     var fileDataRec = get(file,'index.php?p=5','json');

                     if(fileDataRec.total>0){
                         var str = "";
                         $('#fileData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(fileDataRec.courseName[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(fileDataRec.fileName[i])) + '</td>';
                            str = str + '<td>' + fileDataRec.fileUpload[i] + '</td>';
                            str = str + '<td>' + fileDataRec.order[i] + '</td>';
                            if(fileDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#fileStart\',callAddFileData.editData('+fileDataRec.id[i]+',\''+fileDataRec.courseId[i]+'\',\''+fileDataRec.courseName[i]+'\',\''+fileDataRec.fileName[i]+'\',\''+fileDataRec.fileUpload[i]+'\',\''+fileDataRec.order[i]+'\',\''+fileDataRec.active[i]+'\',\''+fileDataRec.rank[i]+'\',\''+fileDataRec.fileevent[i]+'\',\''+fileDataRec.fileYear[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddFileData.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#fileData').append(str);
                         }
                     }else{
                         $('#fileData').html("No Content Found.");
                     }




             }

        this.getEventOfCourse=function(id){
             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color:red;'> * </span>Select Event</label><div class='controls'><select id='fileevent' class='chzn-select'><option selected='selected' disabled='disabled' value=''>Select Event</option>";
             changeCourse['mode']='getEventOfCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             changeCourse['data']['cid']=$('#fileField1').val();
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
             }
             option+="</div>"
             $('#fileeventdiv').html(option);
         }
        this.getYearOfCourse=function(id){
            this.getEventOfCourse(0);
             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color:red;'>*</span>Select Year</label><div class='controls'><select id='fileYear' class='chzn-select' onchange='callAddFileData.getEventOfCourse(this.value)'><option selected='selected' disabled='disabled' value=''>Select Year</option>";
             changeCourse['mode']='getYearOfCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
             }
             option+="</div>"
             $('#fileYeardiv').html(option);
         }
         this.saveData = function(){
                 var file  = {};
                 var fileDataRec;

                if($('#modeFile').val() == 1){
                    file['mode'] = "saveFile";
                    file['data'] = {};
                    file['data']['courseName'] = escape(encodeURIComponent($('#fileField1').val()));
                    var cid = $('#fileField1').val();
                    file['data']['fileRank'] = escape(encodeURIComponent($('#fileRank').val()));
                    file['data']['fileName'] = escape(encodeURIComponent($('#fileField2').val()));
                    file['data']['fileYear'] = escape(encodeURIComponent($('#fileYear').val()));
                    file['data']['fileUpload'] = $('#fileField3').val();
                    var path = file['data']['fileUpload'];
                    file['data']['fileUpload'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                    var dotIndex = file['data']['fileUpload'].lastIndexOf('.'); //to get last index
                    var ext = file['data']['fileUpload'].substring(dotIndex); // to get extension of image
                    file['data']['fileevent'] = $('#fileevent').val();
                    var eventid = $('#fileevent').val();
                    file['data']['order'] = $('#fileField4').val();
                    file['data']['active'] = ($('#uniform-fileField5 span').attr('class')=='checked') ? 1 : 0;
                   // alert(JSON.stringify(file));
                   if(file['data']['courseName']==null || file['data']['courseName']=='null'){
                       alert("Please select Course");
                   }
                   else if(file['data']['fileYear']==null || file['data']['fileYear']=='null'){
                       alert("Please select Year");
                   }
                   else if(file['data']['fileevent']==null || file['data']['fileevent']=='null'){
                       alert("Please select Event");
                   }
                   else if(file['data']['fileName']==''){
                       alert("Please provide File name");
                   }
                   else if(file['data']['fileUpload']!=''){
                        if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                            alert("Please upload file in given format");
                        }
                        else if(imgWidth>=91 && imgWidth<=250 && imgHeight>=94 && imgHeight<=275){
                            fileDataRec = get(file,'index.php?p=5','json');
                        }
                        else{
                            alert("Please upload image in given dimensions");
                        }
                   }
                   else{
                        fileDataRec = get(file,'index.php?p=5','json');
                   }
                    if(fileDataRec.total>0){
                        var lastInsertedId=fileDataRec.lastInsertedId;
                        if($('#fileField3').val()!=""){
                            //alert($('#fileField3')[0].files);
                            var datas=$('#fileField3')[0].files;
                            if (window.FormData) {
                                var formdata = new FormData();

                            }

                            var i = 0, len = datas.length, img, reader, file1;

                            for ( ; i < len; i++ ) {
                                file1 = datas[i];


                                if ( window.FileReader ) {
                                    reader = new FileReader();
                                    reader.onloadend = function (e) {

                                    };
                                    reader.readAsDataURL(file1);
                                }
                                if (formdata) {
                                    formdata.append("file[]", file1);
                                }
                            }


                            if (formdata) {

                                var fileName=$('#fileField2').val();


                                $.ajax({
                                    url: "index.php?p=6&m=f&folder=course&lastInsertedId="+lastInsertedId+"&eventid="+eventid+"&cid="+cid,
                                    type: "POST",
                                    data: formdata,
                                    processData: false,
                                    contentType: false,
                                    success: function (res) {
                                    //alert(res);
                                    }
                                });
                            }

                        }
                        //
                        //
                        alert(fileDataRec.total+" Record inserted successfully.");
                        $('#cancelFileData').click();
                        this.getDataAll();
                    }else{
                        alert("No record inserted.");
                    }
                }

                  if($('#modeFile').val() == 2){
                         file['mode'] = "updateFile";
                         file['data'] = {};
                         file['data']['courseName'] = escape(encodeURIComponent($('#fileField1').val()));
                         file['data']['fileRank'] = escape(encodeURIComponent($('#fileRank').val()));
                         file['data']['fileName'] = escape(encodeURIComponent($('#fileField2').val()));
                         file['data']['fileYear'] = escape(encodeURIComponent($('#fileYear').val()));
                         file['data']['order'] = $('#fileField4').val();
                         file['data']['fileevent'] = $('#fileevent').val(); //event id
                         var eventid = $('#fileevent').val(); 
                         var cid = $('#fileField1').val(); //course id
                         file['data']['active'] = ($('#uniform-fileField5 span').attr('class')=='checked') ? 1 : 0;
                         file['data']['id'] = $('#idFile').val(); //file id
                         if($('#fileField3').val()==''){
                                  file['data']['fileUpload'] = $('#upldFile').val();
                                  var dotIndex = file['data']['fileUpload'].lastIndexOf('.'); // to get last index
                                  var ext = file['data']['fileUpload'].substring(dotIndex);   //to get extension of image
                         }else{
                                  file['data']['fileUpload'] = $('#fileField3').val();
                                  var dotIndex = file['data']['fileUpload'].lastIndexOf('.');  // to get last index
                                  var ext = file['data']['fileUpload'].substring(dotIndex);    // to get extension of image
                        }
                        var path = file['data']['fileUpload'];
                        file['data']['fileUpload'] = path.replace(/^.*\\/, "");
                        if(file['data']['courseName']==null || file['data']['courseName']=='null'){
                            alert("Please select Course");
                        }
                         else if(file['data']['fileYear']==null || file['data']['fileYear']=='null'){
                            alert("Please select Year");
                         }
                         else if(file['data']['fileevent']==null && file['data']['fileevent']=='null'){
                            alert("Please select Event");
                         }
                         else if(file['data']['fileName']==''){
                           alert("Please provide File name");
                        }
                        else if(file['data']['fileUpload']!=''){
                            if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                                alert("Please upload file in given format");
                             }
                             else if(imgWidth!='' && imgHeight!=''){
                                if(imgWidth>=91 && imgWidth<=250 && imgHeight>=94 && imgHeight<=275){
                                    fileDataRec = get(file,'index.php?p=5','json');
                                }
                                else{
                                    alert("Please upload image in given dimensions");
                                }
                             }
                             else{
                                 fileDataRec = get(file,'index.php?p=5','json');
                             }
                        }
                       else{
                            fileDataRec = get(file,'index.php?p=5','json');
                        }
//                         if(fileDataRec.total>0){
  //                   var lastInsertedId=fileDataRec.lastInsertedId;
                      if($('#fileField3').val()!=""){
                         var datas=$('#fileField3')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
                         for ( ; i < len; i++ ) {
                            file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                        var fileName=$('#fileField2').val();
                                        $.ajax({
                                            url: "index.php?p=6&m=f&folder=course&lastInsertedId="+lastInsertedId+"&eventid="+eventid+"&cid="+cid,
                                            type: "POST",
                                            data: formdata,
                                            processData: false,
                                            contentType: false,
                                            success: function (res) {
                                                //alert(res);
                                            }
                                        });
                                    }

                }
                            alert(" Record Updated successfully.");
                            $('#cancelFileData').click();
                            this.getDataAll();
//                }
//                else{
//                            alert("No record updated.");
//                         }
                  }


             }

             this.editData = function(id,courseId,courseName,fileName,fileUpload,order,active,rank,fileevent,fileYear){
                 $('#editFile').html('Edit');
                 $('#modeFile').val(2);
                 $('#idFile').val(id);
                 $('#upldFile').val(fileUpload);
                 $('#uniform-fileField3').val(fileUpload);
                 $('#fileField1').val(courseId);
                 this.getYearOfCourse(courseId);
                 this.getEventOfCourse(fileYear);
                 
                 $('#fileField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 $('#fileField2').val(unescape(decodeURIComponent(fileName)));

                 $('#fileField4').val(order);
                 $('#fileRank').val(decodeURIComponent(rank));
                 if(active == 1){
                    $('#uniform-fileField5 span').attr('class','checked');
                    document.getElementById("fileField5").checked=true;
                }else{
                    $('#uniform-fileField5 span').attr('class','');
                    $('#fileField5').removeAttr('checked');
                }
                $('#fileevent').val(unescape(decodeURIComponent(fileevent)));
                $('#uniform-fileField3 span.filename').html(unescape(decodeURIComponent(fileUpload)));
                $('#fileYear').val(unescape(decodeURIComponent(fileYear)));
                //$('#fileField3').html(unescape(decodeURIComponent(fileUpload)));


             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deleteFile";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
         }


         $("#fileSubmit").click(function(){
            if($("#fileField3").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#fileField3").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelFileData').click(function(){
             $('#fileField1').val("");
             $('#fileField1_chzn a span').html("Select Course");
             $('#uniform-fileField3 span.filename').html("");
             $('#modeFile').val(1);
             $('#idFile').val("");
             clearAll('file');
             $('#editFile').html('Add');
         });
         /*******To FIND DIMENSION OF FILE IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#fileField3").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD FILES END******************/

/************** ADD HomePopUp START******************/

        var addHomePopupData = function(){

            this.getDataAll = function(){

                    var file = {};
                     file['mode'] = "getHomePopupData";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#homePopupData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + fileDataRec.ImagePath[i] + '</td>';
                            str = str + '<td>' + fileDataRec.urlLink[i] + '</td>';
                            if(fileDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAddHomePopup.editData(\''+fileDataRec.id[i]+'\',\''+fileDataRec.ImagePath[i]+'\',\''+fileDataRec.active[i]+'\',\''+fileDataRec.urlLink[i]+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddHomePopup.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                            $('#homePopupData').append(str);
                         }
                     }else{
                         $('#homePopupData').html("No Content Found.");
                     }
            }
             this.saveData = function(){
                 var file  = {};
                 var fileDataRec;

                if($('#modehomePopup').val() == 1){
                         file['mode'] = "savehomePopup";
                         file['data'] = {};
                         file['data']['homePopupUpload'] = $('#homePopupField1').val();
                         file['data']['urlLink'] = $('#homePopupField2').val();
                         file['data']['active'] = ($('#uniform-homePopupField3 span').attr('class')=='checked') ? 1 : 0;
                         var dotIndex = file['data']['homePopupUpload'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['homePopupUpload'].substring(dotIndex); // to get extension of image
                         var path = file['data']['homePopupUpload'];
                         file['data']['homePopupUpload'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['homePopupUpload']==''){
                             alert("Please upload image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                             alert("Please upload file in given format");
                         }
                         else if(imgWidth<=500 && imgWidth>=370 && imgHeight<=650 && imgHeight>=475){
                                fileDataRec = get(file,'index.php?p=5','json');
                         }
                         else{
                            alert("Please upload image in given dimensions");
                         }
                         if(fileDataRec.total>0){
                             if($('#homePopupField1').val()!=""){
                         var datas=$('#homePopupField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }

                         var i = 0, len = datas.length, img, reader, file1;

		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}


                                     if (formdata) {
                                    var fileName=$('#homePopupField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=h",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(fileDataRec.total+" Record inserted successfully.");
                            $('#cancelhomePopupData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                  if($('#modehomePopup').val() == 2){
                         file['mode'] = "updatehomePopup";
                         file['data'] = {};
                         if($('#homePopupField1').val()==''){
                             file['data']['homePopupUpload'] = $('#upldPopupImage').val();
                         }else{
                             file['data']['homePopupUpload'] = $('#homePopupField1').val();
                         }
                         file['data']['urlLink'] = $('#homePopupField2').val();
                         file['data']['active'] = ($('#uniform-homePopupField3 span').attr('class')=='checked') ? 1 : 0;
                         file['data']['id'] = $('#idhomePopup').val();
                         var dotIndex = file['data']['homePopupUpload'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['homePopupUpload'].substring(dotIndex); // to get extension of image
                         var path = file['data']['homePopupUpload'];
                         file['data']['homePopupUpload'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['homePopupUpload']==''){
                             alert("Please upload image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                             alert("Please upload file in given format");
                         }
                         else if(imgWidth!='' && imgHeight!=''){
                             if(imgWidth<=500 && imgWidth>=370 && imgHeight<=650 && imgHeight>=475){
                                fileDataRec = get(file,'index.php?p=5','json');
                             }
                             else{
                                alert("Please upload image in given dimensions");
                             }
                         }
                         else {
                             fileDataRec = get(file,'index.php?p=5','json');
                         }

//                         if(fileDataRec.total>0){
                             if($('#homePopupField1').val()!=""){
                         var datas=$('#homePopupField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }

                         var i = 0, len = datas.length, img, reader, file1;

		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}


                                     if (formdata) {
                                      var fileName=$('#homePopupField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=h",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(" Record updated successfully.");
                            $('#cancelhomePopupData').click();
                            this.getDataAll();
//                         }else{
//                            alert("No record inserted.");
//                         }
                  }


             }

             this.editData = function(id,homePopupimg,active,urlLink){
                 $('#homePagepopup').show();
                 $('#edithomePopup').html('Edit');
                 $('#modehomePopup').val(2);
                 $('#idhomePopup').val(id);
                 $('#upldPopupImage').val(homePopupimg);
                 $('#uniform-homePopupField1 span.filename').html(unescape(decodeURIComponent(homePopupimg)));
                 $('#homePopupField2').val(unescape(decodeURIComponent(urlLink)));
                 if(active == 1){

                    $('#uniform-homePopupField3 span').attr('class','checked');
                    $('#homePopupField3').attr('checked','checked');
                }else{
                    $('#uniform-homePopupField3 span').attr('class','');
                    $('#homePopupField3').removeAttr('checked');
                }
             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deletehomePopup";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                    $('#homePagepopup').show();
                    // location.reload();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
         }


         $("#fileSubmit").click(function(){
            if($("#homePopupField1").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#homePopupField1").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelhomePopupData').click(function(){
             $('#uniform-homePopupField1 span.filename').html("");
             $('#homePopupField2').val("");
             $('#modehomePopup').val(1);
             $('#idhomePopup').val("");
             clearAll('file');
             $('#edithomePopup').html('Add');
             $('#homePagepopup').hide();
         });

         /*******To FIND DIMENSION OF HOME POP UP IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#homePopupField1").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD HOME POPUP END******************/

/************** ADD dams store content START******************/

        var adddamsstoreContent = function(){
            this.getDataAll = function(){
                    var file = {};
                     file['mode'] = "getdamsstoreData";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#damsstoreContentData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + fileDataRec.backgroundImage[i] + '</td>';
//                            if(fileDataRec.active[i] == 1){
//                                str = str + '<td>Active</td>';
//                            }else{
//                                str = str + '<td>Inactive</td>';
//                            }
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAdddamsstorecontent.editData(\''+fileDataRec.id[i]+'\',\''+escape(fileDataRec.backgroundImage[i])+'\',\''+escape(fileDataRec.backgroundImageContent[i])+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAdddamsstorecontent.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                           $('#damsstoreContentData').append(str);
                         }
                     }else{
                         $('#damsstoreContentData').html("No Content Found.");
                     }
            }
             this.saveData = function(){
                 var file  = {};
                 var fileDataRec;

                if($('#modedamsstorecontent').val() == 1){
                         file['mode'] = "savedamsstoreContent";
                         file['data'] = {};
                         file['data']['damsstoreBackgroundimage']=escape(encodeURIComponent(($('#damsstoreContentField1').val())));  //for background image
                         file['data']['damsstorebackgroundImagecontent'] = $('#damsstoreContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['damsstorebackgroundImagecontent']=escape(encodeURIComponent(file['data']['damsstorebackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-damsstoreContentField3 span').attr('class')=='checked') ? 1 : 0;
                         //alert(JSON.stringify(file));
                         var dotIndex = file['data']['damsstoreBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['damsstoreBackgroundimage'].substring(dotIndex); // to get extension of image
                         var path = file['data']['damsstoreBackgroundimage'];
                         file['data']['damsstoreBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['damsstoreBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                             alert("Please upload image in given format");
                         }
                         else if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                               fileDataRec = get(file,'index.php?p=5','json');
                         }
                         else{
                               alert("Please upload image in given dimensions");
                         }
                         if(fileDataRec.total>0){
                             if($('#damsstoreContentField1').val()!=""){
                         var datas=$('#damsstoreContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }

                         var i = 0, len = datas.length, img, reader, file1;

		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}


                                     if (formdata) {
                                    var fileName=$('#damsstoreContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=d",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(fileDataRec.total+" Record inserted successfully.");
                            $('#canceldamsstoreContent').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

                  if($('#modedamsstorecontent').val() == 2){
                         file['mode'] = "updatedamsstoreContent";
                         file['data'] = {};
                         if($('#damsstoreContentField1').val()==''){
                             file['data']['damsstoreBackgroundimage'] = $('#uplddamsstoreBackgroundImage').val();
                         }else{
                             file['data']['damsstoreBackgroundimage'] = $('#damsstoreContentField1').val();
                         }
                         file['data']['damsstorebackgroundImagecontent'] = $('#damsstoreContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['damsstorebackgroundImagecontent']=escape(encodeURIComponent(file['data']['damsstorebackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-damsstoreContentField3 span').attr('class')=='checked') ? 1 : 0;
                         file['data']['id'] = $('#iddamsstorecontent').val();
                         var dotIndex = file['data']['damsstoreBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['damsstoreBackgroundimage'].substring(dotIndex); // to get extension of image
                         var path = file['data']['damsstoreBackgroundimage'];
                         file['data']['damsstoreBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['damsstoreBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                             alert("Please upload image in given format");
                         }
                         else if(imgWidth!='' && imgHeight!=''){
                                if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                    fileDataRec = get(file,'index.php?p=5','json');
                                }
                                else{
                                    alert("Please upload image in given dimensions");
                                }
                         }
                         else{
                            fileDataRec = get(file,'index.php?p=5','json');
                         }
//                         if(fileDataRec.total>0){
                             if($('#damsstoreContentField1').val()!=""){
                         var datas=$('#damsstoreContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }

                         var i = 0, len = datas.length, img, reader, file1;

		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}


                                     if (formdata) {
                                      var fileName=$('#damsstoreContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=d",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(" Record updated successfully.");
                            $('#canceldamsstoreContent').click();
                            this.getDataAll();
//                         }else{
//                            alert("No record updated.");
//                         }
                  }


             }

             this.editData = function(id,backgroundImageupload,backgroundImagecontent){
                 $('#damsstoreContent').show();
                 $('#editdamsstorecontent').html('Edit');
                 $('#modedamsstorecontent').val(2);
                 $('#iddamsstorecontent').val(id);
                 $('#damsstoreContentField2 textarea').val(unescape(decodeURIComponent(backgroundImagecontent)));
                 $('#uplddamsstoreBackgroundImage').val(backgroundImageupload);
                 $('#uniform-damsstoreContentField1 span.filename').html(unescape(decodeURIComponent(backgroundImageupload)));
//                 if(active == 1){
//
//                    $('#uniform-damsstoreContentField3 span').attr('class','checked');
//                    $('#damsstoreContentField3').attr('checked','checked');
//                }else{
//                    $('#uniform-damsstoreContentField3 span').attr('class','');
//                    $('#damsstoreContentField3').removeAttr('checked');
//                }
             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deletedamsstoreContent";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                    $('#damsstoreContent').show();
                    // location.reload();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
         }


         $("#fileSubmit").click(function(){
            if($("#damsstoreContentField21").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#damsstoreContentField2").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#canceldamsstoreContent').click(function(){
             $('#uniform-damsstoreContentField2 span.filename').html("");
             $('#modedamsstorecontent').val(1);
             $('#iddamsstorecontent').val("");
             clearAll('file');
             $('#editdamsstorecontent').html('Add');
             $('#damsstoreContent').hide();
         });

         /*******To FIND DIMENSION OF DAMS STORE IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#damsstoreContentField1").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD Dams Store Content END******************/

/************** ADD Virtual Tourcontent START******************/

        var addvirtualtourContent = function(){
            this.getDataAll = function(){
                    var file = {};
                     file['mode'] = "getvirtualtourData";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#virtualtourContentData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + fileDataRec.courseName[i] + '</td>';
                            str = str + '<td>' + fileDataRec.backgroundImage[i] + '</td>';
//                            if(fileDataRec.active[i] == 1){
//                                str = str + '<td>Active</td>';
//                            }else{
//                                str = str + '<td>Inactive</td>';
//                            }
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAddvirtualtourcontent.editData(\''+fileDataRec.id[i]+'\',\''+escape(fileDataRec.backgroundImage[i])+'\',\''+escape(fileDataRec.backgroundImageContent[i])+'\',\''+fileDataRec.courseName[i]+'\',\''+fileDataRec.courseId[i]+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddvirtualtourcontent.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                           $('#virtualtourContentData').append(str);
                         }
                     }else{
                         $('#virtualtourContentData').html("No Content Found.");
                     }
            }
             this.saveData = function(){
                 var file  = {};
                 var fileDataRec;
                 if($('#modevirtualtourcontent').val() == 1){
                         file['mode'] = "savevirtualtourContent";
                         file['data'] = {};
                         file['data']['virtualTourcourseId'] = $('#virtualtourContentField4').val();  //for name
                         file['data']['virtualtourBackgroundimage']=escape(encodeURIComponent(($('#virtualtourContentField1').val())));  //for background image
                         file['data']['virtualtourbackgroundImagecontent'] = $('#virtualtourContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['virtualtourbackgroundImagecontent']=escape(encodeURIComponent(file['data']['virtualtourbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-virtualtourContentField3 span').attr('class')=='checked') ? 1 : 0;
                         var dotIndex = file['data']['virtualtourBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['virtualtourBackgroundimage'].substring(dotIndex); // to get extension of image
                         var path = file['data']['virtualtourBackgroundimage'];
                         file['data']['virtualtourBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['virtualTourcourseId']=='' || file['data']['virtualTourcourseId']==null || file['data']['virtualTourcourseId']=='null'){
                             alert("Please select Course");
                         }
                         else if(file['data']['virtualtourBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                            alert("Please upload file in given format");
                         }
                         else if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                               fileDataRec = get(file,'index.php?p=5','json');
                         }
                         else{
                               alert("Please upload image in given dimensions");
                         }
                         if(fileDataRec.total>0){
                             if($('#virtualtourContentField1').val()!=""){
                         var datas=$('#virtualtourContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                    var fileName=$('#virtualtourContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=virtualtourimage",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                           alert(fileDataRec.total+" Record inserted successfully.");
                            $('#cancelvirtualtourContent').click();
                            this.getDataAll();
                 }else{
                            alert("No record inserted.");
                  }
              }

                  if($('#modevirtualtourcontent').val() == 2){
                         file['mode'] = "updatevirtualtourContent";
                         file['data'] = {};
                         file['data']['virtualTourcourseId'] = $('#virtualtourContentField4').val();  //for courseID
                         if($('#virtualtourContentField1').val()==''){
                             file['data']['virtualtourBackgroundimage'] = $('#upldvirtualtourBackgroundImage').val();
                         }else{
                             file['data']['virtualtourBackgroundimage'] = $('#virtualtourContentField1').val();
                         }
                         var dotIndex = file['data']['virtualtourBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['virtualtourBackgroundimage'].substring(dotIndex); // to get extension of image
                         var path = file['data']['virtualtourBackgroundimage'];
                         file['data']['virtualtourBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         file['data']['id'] = $('#idvirtualtourcontent').val();
                         file['data']['virtualtourbackgroundImagecontent'] = $('#virtualtourContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['virtualtourbackgroundImagecontent']=escape(encodeURIComponent(file['data']['virtualtourbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-virtualtourContentField3 span').attr('class')=='checked') ? 1 : 0;
                         //alert(JSON.stringify(file));
                         if(file['data']['virtualTourcourseId']=='' || file['data']['virtualTourcourseId']==null || file['data']['virtualTourcourseId']=='null'){
                             alert("Please select Course");
                         }
                         else if(file['data']['virtualtourBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png' ){
                            alert("Please upload file in given format");
                         }
                         else if(imgWidth!='' && imgHeight!=''){
                                if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                    fileDataRec = get(file,'index.php?p=5','json');
                                }
                                else{
                                    alert("Please upload image in given dimensions");
                                }
                         }
                         else{
                            fileDataRec = get(file,'index.php?p=5','json');
                         }
//                         if(fileDataRec.total>0){
                             if($('#virtualtourContentField1').val()!=""){
                         var datas=$('#virtualtourContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                    var fileName=$('#virtualtourContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=virtualtourimage",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(" Record updated successfully.");
                            $('#cancelvirtualtourContent').click();
                            this.getDataAll();
//                         }else{
//                            alert("No record inserted.");
//                         }
               }
             }

             this.editData = function(id,backgroundImageupload,backgroundImagecontent,courseName,courseId){
//                 $('#virualtourContent').show();
                 $('#editvirtualtourcontent').html('Edit');
                 $('#modevirtualtourcontent').val(2);
                 $('#idvirtualtourcontent').val(id);
                 $('#virtualtourContentField4').val(courseId);
                 $('#virtualtourContentField4_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 $('#virtualtourContentField2 textarea').val(unescape(decodeURIComponent(backgroundImagecontent)));
                 $('#upldvirtualtourBackgroundImage').val(backgroundImageupload);
                 $('#uniform-virtualtourContentField1 span.filename').html(unescape(decodeURIComponent(backgroundImageupload)));
//                 if(active == 1){
//
//                    $('#uniform-damsstoreContentField3 span').attr('class','checked');
//                    $('#damsstoreContentField3').attr('checked','checked');
//                }else{
//                    $('#uniform-damsstoreContentField3 span').attr('class','');
//                    $('#damsstoreContentField3').removeAttr('checked');
//                }
             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deletevirtualtourContent";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
//                    $('#virualtourContent').show();
                    // location.reload();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
      }  /* addvirtualtourContent eneded */


         $("#fileSubmit").click(function(){
            if($("#virtualtourContentField1").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#virtualtourContentField1").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelvirtualtourContent').click(function(){
             $('#virtualtourContentField4_chzn a span').html("Select Course");
             $('#uniform-virtualtourContentField1 span.filename').html("");
             $('#virtualtourContentField2 textarea').val(unescape(decodeURIComponent('')));
             $('#modevirtualtourcontent').val(1);
             $('#idvirtualtourcontent').val("");
             clearAll('file');
             $('#editvirtualtourcontent').html('Add');
//             $('#virualtourContent').hide();
         });

         /*******To FIND DIMENSION OF VIRTUAL TOUR IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#virtualtourContentField1").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });
            

/************** ADD Virtual Tour Content END******************/

/************** ADD Find Center content START******************/

        var addfindcenterContent = function(){
            this.getDataAll = function(){
                    var file = {};
                     file['mode'] = "getfindcenterData";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#findcenterContentData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + fileDataRec.backgroundImage[i] + '</td>';
//                            if(fileDataRec.active[i] == 1){
//                                str = str + '<td>Active</td>';
//                            }else{
//                                str = str + '<td>Inactive</td>';
//                            }
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAddfindcentercontent.editData(\''+fileDataRec.id[i]+'\',\''+escape(fileDataRec.backgroundImage[i])+'\',\''+escape(fileDataRec.backgroundImageContent[i])+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddfindcentercontent.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                           $('#findcenterContentData').append(str);
                         }
                     }else{
                         $('#findcenterContentData').html("No Content Found.");
                     }
            }
             this.saveData = function(){
                 var file  = {};
                 var fileDataRec;
                 if($('#modefindcentercontent').val() == 1){
                         file['mode'] = "savefindcenterContent";
                         file['data'] = {};
                         file['data']['findcenterBackgroundimage']=escape(encodeURIComponent(($('#findcenterContentField1').val())));  //for background image
                         file['data']['findcenterbackgroundImagecontent'] = $('#findcenterContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['findcenterbackgroundImagecontent']=escape(encodeURIComponent(file['data']['findcenterbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-findcenterContentField3 span').attr('class')=='checked') ? 1 : 0;
                         var dotIndex = file['data']['findcenterBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['findcenterBackgroundimage'].substring(dotIndex); // to get extension of image
                         //alert(JSON.stringify(file));
                         var path = file['data']['findcenterBackgroundimage'];
                         file['data']['findcenterBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         if(file['data']['findcenterBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                alert("Please upload image in given format");
                         }
                         else if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                fileDataRec = get(file,'index.php?p=5','json');
                         }
                         else {
                                alert("Please upload image in given dimensions");
                         }
                         if(fileDataRec.total>0){
                             if($('#findcenterContentField1').val()!=""){
                         var datas=$('#findcenterContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                    var fileName=$('#findcenterContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=findcenterimage",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                           alert(fileDataRec.total+" Record inserted successfully.");
                            $('#cancelfindcenterContent').click();
                            this.getDataAll();
                 }else{
                            alert("No record inserted.");
                  }
              }

                  if($('#modefindcentercontent').val() == 2){
                         file['mode'] = "updatefindcenterContent";
                         file['data'] = {};
                         if($('#findcenterContentField1').val()==''){
                             file['data']['findcenterBackgroundimage'] = $('#upldfindcenterBackgroundImage').val();
                         }else{
                             file['data']['findcenterBackgroundimage'] = $('#findcenterContentField1').val();
                         }
                         file['data']['id'] = $('#idfindcentercontent').val();
                         file['data']['findcenterbackgroundImagecontent'] = $('#findcenterContentField2 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['findcenterbackgroundImagecontent']=escape(encodeURIComponent(file['data']['findcenterbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-findcenterContentField3 span').attr('class')=='checked') ? 1 : 0;
                         var dotIndex = file['data']['findcenterBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['findcenterBackgroundimage'].substring(dotIndex); // to get extension of image
                         var path = file['data']['findcenterBackgroundimage'];
                         file['data']['findcenterBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         //alert(JSON.stringify(file));
                         if(file['data']['findcenterBackgroundimage']==''){
                             alert("Please upload background image");
                         }
                         else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                alert("Please upload image in given format");
                         }
                         else if(imgWidth!='' && imgHeight!=''){
                                if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                       fileDataRec = get(file,'index.php?p=5','json');
                                }
                                else {
                                        alert("Please upload image in given dimensions");
                                }
                         }
                         else {
                                fileDataRec = get(file,'index.php?p=5','json');
                         }
                         
//                         if(fileDataRec.total>0){
                             if($('#findcenterContentField1').val()!=""){
                         var datas=$('#findcenterContentField1')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                    var fileName=$('#findcenterContentField1').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=findcenterimage",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                      // alert(res);
                                                }
                                        });
                                      }
                }
                            alert(" Record updated successfully.");
                            $('#cancelfindcenterContent').click();
                            this.getDataAll();
//                         }else{
//                            alert("No record updated.");
//                         }
               }
             }

             this.editData = function(id,backgroundImageupload,backgroundImagecontent){
                 $('#findcenterContent').show();
                 $('#editfindcentercontent').html('Edit ');
                 $('#modefindcentercontent').val(2);
                 $('#idfindcentercontent').val(id);
                 $('#findcenterContentField2 textarea').val(unescape(decodeURIComponent(backgroundImagecontent)));
                 $('#upldfindcenterBackgroundImage').val(backgroundImageupload);
                 $('#uniform-findcenterContentField1 span.filename').html(unescape(decodeURIComponent(backgroundImageupload)));
//                 if(active == 1){
//
//                    $('#uniform-damsstoreContentField3 span').attr('class','checked');
//                    $('#damsstoreContentField3').attr('checked','checked');
//                }else{
//                    $('#uniform-damsstoreContentField3 span').attr('class','');
//                    $('#damsstoreContentField3').removeAttr('checked');
//                }
             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deletefindcenterContent";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                    $('#findcenterContent').show();
                    // location.reload();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
      }  /* addvirtualtourContent eneded */


         $("#fileSubmit").click(function(){
            if($("#findcenterContentField1").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#findcenterContentField1").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelfindcenterContent').click(function(){
             $('#uniform-findcenterContentField2 span.filename').html("");
             $('#modefindcentercontent').val(1);
             $('#idfindcentercontent').val("");
             clearAll('file');
             $('#editfindcentercontent').html('Add');
             $('#findcenterContent').hide();
         });
         /*******To FIND DIMENSION OF FIND CENTER IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#findcenterContentField1").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD Find Center Content END******************/

/************** ADD About Us START******************/

        var addAboutusContent = function(){
            this.getDataAll = function(){
                    var file = {};
                     file['mode'] = "getAboutusData";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#AboutusContentData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(fileDataRec.name[i]) + '</td>';
                            str = str + '<td>' + fileDataRec.urlLink[i] + '</td>';
                            str = str + '<td>' + fileDataRec.backgroundImageupload[i] + '</td>';
                            if(fileDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            } 
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#aboutStart\', callAboutuscontent.editData(\''+fileDataRec.id[i]+'\',\''+escape(fileDataRec.name[i])+'\',\''+escape(fileDataRec.urlLink[i])+'\',\''+escape(fileDataRec.backgroundImageupload[i])+'\',\''+escape(fileDataRec.backgroundImagecontent[i])+'\',\''+escape(fileDataRec.contentHome[i])+'\',\''+escape(fileDataRec.active[i])+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAboutuscontent.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                           $('#AboutusContentData').append(str);
                         }
                     }else{
                         $('#AboutusContentData').html("No Content Found.");
                     }
            }
             this.saveData = function(){
                 var file  = {};
                 var fileDataRec;
                 if($('#modeAboutuscontent').val() == 1){
                         file['mode'] = "saveAboutusContent";
                         file['data'] = {};
                         file['data']['Aboutusname'] = escape(encodeURIComponent(($('#AboutusContentField1').val())));  //for About Us name
                         file['data']['AboutusUrllink'] = escape(encodeURIComponent(($('#AboutusContentField2').val())));  //for About Us Url link
                         file['data']['AboutusContenthome'] = $('#AboutusContentField3 textarea').val();  //for About Us content home
                         file['data']['AboutusContenthome'] = escape(encodeURIComponent(file['data']['AboutusContenthome']));
                         file['data']['AboutusBackgroundimage'] = escape(encodeURIComponent(($('#AboutusContentField4').val())));  //for background image
                         file['data']['AboutusbackgroundImagecontent'] = $('#AboutusContentField5 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['AboutusbackgroundImagecontent']=escape(encodeURIComponent(file['data']['AboutusbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-AboutusContentField6 span').attr('class')=='checked') ? 1 : 0;
                        var dotIndex = file['data']['AboutusBackgroundimage'].lastIndexOf('.'); //to get last index
                        var ext = file['data']['AboutusBackgroundimage'].substring(dotIndex); // to get extension of image
                        var path = file['data']['AboutusBackgroundimage'];
                        file['data']['AboutusBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                        if(file['data']['Aboutusname']==''){
                             alert("Please provide About Us name");
                         }
                          else if(file['data']['AboutusUrllink']=='') {
                             alert("Please provide Url Link");
                         }
                         else if(file['data']['AboutusBackgroundimage']==''){
                             alert("Please upload image");      
                        }
                       else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                alert("Please upload file in given format");
                       }
                       else if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                            fileDataRec = get(file,'index.php?p=5','json');
                       }
                       else {
                             alert("Please upload image in given dimensions");
                       }
                         //alert(JSON.stringify(file));
//                         if(file['data']['Aboutusname']==''){
//                             alert("Please provide About us name");
//                         }
//                         else if(file['data']['AboutusUrllink']==''){
//                             alert("Please provide About us link");
//                         }
//                         else{
//                            fileDataRec = get(file,'index.php?p=5','json');
//                         }
                          
                         if(fileDataRec.total>0){
                             if($('#AboutusContentField4').val()!=""){
                                 var datas=$('#AboutusContentField4')[0].files;
                                 if (window.FormData) {
                                     var formdata = new FormData();
                                 }
                                 var i = 0, len = datas.length, img, reader, file1;
                                 for ( ; i < len; i++ ) {
                                 file1 = datas[i];
                                        if ( window.FileReader ) {
                                                reader = new FileReader();
                                                reader.onloadend = function (e) {

                                                };
                                                reader.readAsDataURL(file1);
                                        }
                                        if (formdata) {
                                                formdata.append("file[]", file1);
                                        }
                                      }
                                             if (formdata) {
                                            var fileName=$('#AboutusContentField4').val();
                                                $.ajax({
                                                        url: "index.php?p=6&m=aboutusImage",
                                                        type: "POST",
                                                        data: formdata,
                                                        processData: false,
                                                        contentType: false,
                                                        success: function (res) {
                                                             //alert(res);
                                                        }
                                                });
                                      }
                }
                alert(fileDataRec.total+" Record inserted successfully.");
                $('#cancelaboutusContent').click();
                this.getDataAll();
                }else{
                            alert("No record inserted.");
                  }
              }

                  if($('#modeAboutuscontent').val() == 2){
                         file['mode'] = "updateAboutusContent";
                         file['data'] = {};
                         file['data']['Aboutusname'] = escape(encodeURIComponent(($('#AboutusContentField1').val())));  //for About Us name
                         file['data']['AboutusUrllink'] = escape(encodeURIComponent(($('#AboutusContentField2').val())));  //for About Us Url link
                         file['data']['AboutusContenthome'] = $('#AboutusContentField3 textarea').val();  //for About Us content home
                         file['data']['AboutusContenthome']=escape(encodeURIComponent(file['data']['AboutusContenthome']));
                         if($('#AboutusContentField4').val()==''){
                             file['data']['AboutusBackgroundimage'] = $('#aboutusBackgroundImage').val();
                         }else{
                             file['data']['AboutusBackgroundimage'] = $('#AboutusContentField4').val();
                         }
                         var dotIndex = file['data']['AboutusBackgroundimage'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['AboutusBackgroundimage'].substring(dotIndex); // to get extension of image
                         file['data']['AboutusbackgroundImagecontent'] = $('#AboutusContentField5 textarea').val();  //for ckeditor backgroundImage content added by shruti
                         file['data']['AboutusbackgroundImagecontent'] = escape(encodeURIComponent(file['data']['AboutusbackgroundImagecontent']));
                         file['data']['active'] = ($('#uniform-AboutusContentField6 span').attr('class')=='checked') ? 1 : 0;
                         file['data']['id'] = $('#idAboutuscontent').val();
                        var dotIndex = file['data']['AboutusBackgroundimage'].lastIndexOf('.'); //to get last index
                        var ext = file['data']['AboutusBackgroundimage'].substring(dotIndex); // to get extension of image
                        var path = file['data']['AboutusBackgroundimage'];
                        file['data']['AboutusBackgroundimage'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                        if(file['data']['Aboutusname']==''){
                             alert("Please provide About Us name");
                         }
                        else if(file['data']['AboutusUrllink']=='') {
                             alert("Please provide Url Link");
                        }
                        else if(file['data']['AboutusBackgroundimage']==''){
                             alert("Please upload image");
                        }
                        else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                alert("Please upload file in given format");
                        }
                        else if(imgWidth!='' && imgHeight!=''){
                                if(imgWidth<=1280 && imgWidth>=921 && imgHeight<=500 && imgHeight>=318){
                                       fileDataRec = get(file,'index.php?p=5','json');
                                }
                                else {
                                        alert("Please upload image in given dimensions");
                                }
                         }
                         else {
                                fileDataRec = get(file,'index.php?p=5','json');
                         }
                         //alert(JSON.stringify(file));
//                         if(fileDataRec.total>0){
                             if($('#AboutusContentField4').val()!=""){
                         var datas=$('#AboutusContentField4')[0].files;
                         if (window.FormData) {
                             var formdata = new FormData();
                         }
                         var i = 0, len = datas.length, img, reader, file1;
		for ( ; i < len; i++ ) {
			file1 = datas[i];


				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {

					};
					reader.readAsDataURL(file1);
				}
				if (formdata) {
					formdata.append("file[]", file1);
				}
                       	}
                                     if (formdata) {
                                    var fileName=$('#AboutusContentField4').val();
                                        $.ajax({
                                                url: "index.php?p=6&m=aboutusImage",
                                                type: "POST",
                                                data: formdata,
                                                processData: false,
                                                contentType: false,
                                                success: function (res) {
                                                     //alert(res);
                                                }
                                        });
                                      }
                }
                           alert("Record Updated successfully.");
                            $('#cancelaboutusContent').click();
                            this.getDataAll();
//                 }else{
//                            alert("No record updated.");
//                  }
               }
             }
             this.editData = function(id,name,urlLink,backgroundImageupload,backgroundImagecontent,contentHome,active){
                $('#editAboutuscontent').html('Edit  ');
                 $('#modeAboutuscontent').val(2);
                 $('#idAboutuscontent').val(id);
                 $('#AboutusContentField1').val(unescape(decodeURIComponent(name))); //for About Us name
                 $('#AboutusContentField2').val(unescape(decodeURIComponent(urlLink)));  //for About Us Url link
                 $('#AboutusContentField3 textarea').val(unescape(decodeURIComponent(contentHome))); //for About Us content home
                 $('#uniform-AboutusContentField4 span.filename').html(unescape(decodeURIComponent(backgroundImageupload))); //for About Us background image upload
//                 $('#AboutusContentFiel4').val(backgroundImageupload);
                 $('#aboutusBackgroundImage').val(backgroundImageupload);  //for About Us background image upload
                 $('#AboutusContentField5 textarea').val(unescape(decodeURIComponent(backgroundImagecontent))); //for About Us background image conttent
                 if(active == 1){
                    $('#uniform-AboutusContentField6 span').attr('class','checked');
                    $('#AboutusContentField6').attr('checked','checked');
                }else{
                    $('#uniform-AboutusContentField6 span').attr('class','');
                    $('#AboutusContentField6').removeAttr('checked');
                }
             }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deleteAboutus";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                    $('#AboutusContentData').show();
                    // location.reload();
                 }else{
                    alert("No record deleted.");
                 }
             }
         }
      }  /* addvirtualtourContent eneded */


         $("#fileSubmit").click(function(){
            if($("#findcenterContentField1").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#findcenterContentField1").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelaboutusContent').click(function(){
             $('#uniform-AboutusContentField4 span.filename').html("");
             $('#modeAboutuscontent').val(1);
             $('#idAboutuscontent').val("");
             $('#AboutusContentField3 textarea').val(""); //for About Us content home
             $('#AboutusContentField5 textarea').val(""); //for About Us background image conttent
             clearAll('file');
             $('#editAboutuscontent').html('Add');
         });
         /*******To FIND DIMENSION OF ABOUT US IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#AboutusContentField4").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD About Us END******************/

/************** ADD FILES START******************/

        var addHomePhotogallery = function(){

            this.getDataAll = function(){
                    var file = {};
                     file['mode'] = "gethomePhotogallery";
                     var fileDataRec = get(file,'index.php?p=5','json');
                     if(fileDataRec.total>0){
                         var str = "";
                         $('#homePhotogalleryData').html('');
                         for(var i=0;i<fileDataRec.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            //str = str + '<td>' + unescape(decodeURIComponent(fileDataRec.courseName[i])) + '</td>';
                            //str = str + '<td>' + unescape(decodeURIComponent(fileDataRec.fileName[i])) + '</td>';
                            str = str + '<td>' + fileDataRec.ImagePath[i] + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(fileDataRec.homephotogalleryOrder[i])) + '</td>';
                            if(fileDataRec.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAddHomePhotogallery.editData('+fileDataRec.id[i]+',\''+fileDataRec.ImagePath[i]+'\',\''+fileDataRec.active[i]+'\',\''+fileDataRec.homephotogalleryOrder[i]+'\');"></i>' + '</td>';
                           str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddHomePhotogallery.deleteData('+fileDataRec.id[i]+');"></i>' + '</td>';
                           str = str + '</tr>';
                            $('#homePhotogalleryData').append(str);
                         }
                     }else{
                         $('#homePhotogalleryData').html("No Content Found.");
                     }
             }

                  this.saveData = function(){
                 var file  = {};
                 var fileDataRec;
                if($('#modehomePhotogallery').val() == 1){
                    file['mode'] = "saveHomePhotoGallery";
                    file['data'] = {};
                    file['data']['homePhotogalleryupld'] = $('#homePhotogalleryField1').val();
                    file['data']['active'] = ($('#uniform-homePhotogalleryField3 span').attr('class')=='checked') ? 1 : 0;
                    file['data']['homephotogalleryOrder'] = $('#homephotogalleryOrder').val();  //for order
                   // alert(JSON.stringify(file));
                   var dotIndex = file['data']['homePhotogalleryupld'].lastIndexOf('.'); //to get last index
                   var ext = file['data']['homePhotogalleryupld'].substring(dotIndex); // to get extension of image
                   var path = file['data']['homePhotogalleryupld'];
                   file['data']['homePhotogalleryupld'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                   if(file['data']['homePhotogalleryupld']==''){
                       alert("Please upload image");
                   }
                   else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                      alert("Please upload file in given format");
                   }
                   else if(imgWidth<=1920 && imgWidth>=1024 && imgHeight<=900 && imgHeight>=768 ){
                            fileDataRec = get(file,'index.php?p=5','json');
                   }
                   else{
                        alert("Please upload image in given dimensions");
                   }
                    if(fileDataRec.total>0){
                        var lastInsertedId=fileDataRec.lastInsertedId;
                        if($('#homePhotogalleryField1').val()!=""){
                            var datas=$('#homePhotogalleryField1')[0].files;
                            if (window.FormData) {
                                var formdata = new FormData();

                            }

                            var i = 0, len = datas.length, img, reader, file1;

                            for ( ; i < len; i++ ) {
                                file1 = datas[i];


                                if ( window.FileReader ) {
                                    reader = new FileReader();
                                    reader.onloadend = function (e) {

                                    };
                                    reader.readAsDataURL(file1);
                                }
                                if (formdata) {
                                    formdata.append("file[]", file1);
                                }
                            }


                            if (formdata) {
                                var fileName=$('#homePhotogalleryField1').val();
                                $.ajax({
                                    url: "index.php?p=6&m=g",
                                    type: "POST",
                                    data: formdata,
                                    processData: false,
                                    contentType: false,
                                    success: function (res) {
                                    //alert(res);
                                    }
                                });
                            }
                        }
                        alert(fileDataRec.total+" Record inserted successfully.");
                        $('#cancelhomePhotogallery').click();
                        this.getDataAll();
                    }else{
                        alert("No record inserted.");
                    }
                }

                  if($('#modehomePhotogallery').val() == 2){
                         file['mode'] = "updateHomePhotoGallery";
                         file['data'] = {};
                         if($('#homePhotogalleryField1').val()==''){
                             file['data']['homePhotogalleryupld'] = $('#upldHomePhotoGallery').val();
                         }else{
                             file['data']['homePhotogalleryupld'] = $('#homePhotogalleryField1').val();
                         }
                         file['data']['active'] = ($('#uniform-homePhotogalleryField3 span').attr('class')=='checked') ? 1 : 0;
                         file['data']['homephotogalleryOrder'] = $('#homephotogalleryOrder').val();
                         var dotIndex = file['data']['homePhotogalleryupld'].lastIndexOf('.'); //to get last index
                         var ext = file['data']['homePhotogalleryupld'].substring(dotIndex); // to get extension of image
                         var path = file['data']['homePhotogalleryupld'];
                         file['data']['homePhotogalleryupld'] = path.replace(/^.*\\/, ""); //to resolve problem of fake path
                         file['data']['id'] = $('#idhomePhotogallery').val();
                         if(file['data']['homePhotogalleryupld']==''){
                                alert("Please upload image");
                         }
                        else if(ext!='.jpeg' && ext!='.jpg' && ext!='.png'){
                                 alert("Please upload file in given format");
                         }
                         else if(imgWidth!='' && imgHeight!=''){
                             if(imgWidth<=1920 && imgWidth>=1024 && imgHeight<=900 && imgHeight>=768 ){
                                 fileDataRec = get(file,'index.php?p=5','json');
                             }
                             else {
                                 alert("Please upload image in given dimensions");
                             }
                         }
                        else{
                            fileDataRec = get(file,'index.php?p=5','json');
                        }
                         //alert(fileDataRec.total);
//                         if(fileDataRec.total>0){
                         var lastInsertedId=fileDataRec.lastInsertedId;

                             if($('#homePhotogalleryField1').val()!=""){
                             var datas=$('#homePhotogalleryField1')[0].files;
                             if (window.FormData) {
                                     var formdata = new FormData();

                             }
                         var i = 0, len = datas.length, img, reader, file1;
                         for ( ; i < len; i++ ) {
                                file1 = datas[i];
                                        if ( window.FileReader ) {
                                                reader = new FileReader();
                                                reader.onloadend = function (e) {

                                                };
                                                reader.readAsDataURL(file1);
                                        }
                                        if (formdata) {
                                                formdata.append("file[]", file1);
                                        }
                         }
                        if (formdata) {
                              var fileName=$('#homePhotogalleryField1').val();
                              $.ajax({
                                        url: "index.php?p=6&m=g",
                                        type: "POST",
                                        data: formdata,
                                        processData: false,
                                        contentType: false,
                                        success: function (res) {
                                                //alert(res);
                                        }
                                });
                             }
                }
                alert("Record Updated successfully.");
                $('#cancelhomePhotogallery').click();
                    this.getDataAll();
//                }else{
//                    alert("No record inserted.");
//                }
              }
          }

             this.editData = function(id,fileUpload,active,order){
                 $('#edithomePhotogallery').html('Edit');
                 $('#modehomePhotogallery').val(2);idhomePhotogallery
                 $('#idhomePhotogallery').val(id);
                 $('#homephotogalleryOrder').val(order);
                 $('#upldHomePhotoGallery').val(fileUpload);
               if(active == 1){
                    $('#uniform-homePhotogalleryField3 span').attr('class','checked');
                    $('#homePhotogalleryField3').attr('checked','checked');
                }else{
                    $('#uniform-homePhotogalleryField3 span').attr('class','');
                    $('#homePhotogalleryField3').removeAttr('checked');
                }
                $('#uniform-homePhotogalleryField1 span.filename').html(unescape(decodeURIComponent(fileUpload)));
         }

             this.deleteData = function(id){
                 var file = {};
                 file['mode'] = "deletehomePhotoGallery";
                 file['data'] = {};
                 file['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var fileDataRec = get(file,'index.php?p=5','json');
                 if(fileDataRec.total>0){
                    alert(fileDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record deleted.");
                 }
             }
         }
         }


         $("#fileSubmit").click(function(){
            if($("#homePhotogalleryField1").val()!=""){
             var validImageFormat=/\.(pdf)$/i;
             if(!validImageFormat.test($("#fileField3").val())){
                alert("Please upload pdf file");
                return false;
             }
            }
         })

         $('#cancelhomePhotogallery').click(function(){
             $('#uniform-homePhotogalleryField1 span.filename').html("");
             $('#modehomePhotogallery').val(1);
             $('#idhomePhotogallery').val("");
             clearAll('file');
             $('#edithomePhotogallery').html('Add');
         });
         /*******To FIND DIMENSION OF HOME PHOTO GALLERY IMAGE *********/
             var imgWidth = '';
             var imgHeight = '';
             function readImage(file) {
             var reader = new FileReader();
             var image  = new Image();
             reader.readAsDataURL(file);
             reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                     imgWidth = this.width,
                     imgHeight = this.height;
                };
             };
            }
            $("#homePhotogalleryField1").change(function (e) {
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++)
                readImage(F[i]);
            });

/************** ADD PHOTO GALLERY END******************/




/**************ADD VIDEO START******************/
 var addVideoData = function(){
    this.getDataAll = function(){
        var video = {};
        video['mode'] = "getAllVideo";
        var videoDataRec = get(video,'index.php?p=5','json');
        if(videoDataRec.total>0){
            var str = "";
            $('#videoData').html('');
            for(var i=0;i<videoDataRec.total;i++){
                str = "";
                str = str + '<tr>';
                str = str + '<td>' + (i+1) + '</td>';
                str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.courseName[i])) + '</td>';
                str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.videoName[i])) + '</td>';
                str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.videoRank[i])) + '</td>';
                 var string= videoDataRec.videoUpload[i] .toLocaleString();
                 if(string.length >20) {
                var videoString = string.substring(0,20)+"...";
               str = str + '<td title="'+string+'" onclick=openYouTube();>' + videoString + '</td>';
                }else{
                str = str + '<td onclick=openYouTube();>' + videoDataRec.videoUpload[i] + '</td>';
                }
                str = str + '<td>' + videoDataRec.order[i] + '</td>';
                if(videoDataRec.active[i] == 1){
                    str = str + '<td>Active</td>';
                }else{
                    str = str + '<td>Inactive</td>';
                }
                str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#videoStart\', callAddVideoData.editData('+videoDataRec.id[i]+',\''+videoDataRec.courseId[i]+'\',\''+escape(videoDataRec.courseName[i])+'\',\''+escape(videoDataRec.videoName[i])+'\',\''+videoDataRec.videoUpload[i]+'\',\''+videoDataRec.order[i]+'\',\''+videoDataRec.active[i]+'\',\''+videoDataRec.videoRank[i]+'\',\''+videoDataRec.videoevent[i]+'\',\''+videoDataRec.urlLink[i]+'\',\''+videoDataRec.youtubeVideos[i]+'\',\''+escape(videoDataRec.videoYear[i])+'\');"></i>' + '</td>';
                str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddVideoData.deleteData('+videoDataRec.id[i]+',\''+unescape(decodeURIComponent($.trim(videoDataRec.urlLink[i])))+'\');"></i>' + '</td>';
                str = str + '</tr>';
                $('#videoData').append(str);
             }
        }else{
             $('#videoData').html("No Content Found.");
        }
    }

    this.saveData = function(){
        var video  = {};
        var videoDataRec;
        if($('#modeVideo').val() == 1){
            video['mode'] = "saveVideo";
            video['data'] = {};
            video['data']['courseName'] = escape(encodeURIComponent($('#videoField1').val()));
            var cid = $('#videoField1').val();
            video['data']['videoevent'] = $('#videoevent').val();
            var eventid = $('#videoevent').val();
            video['data']['videoName'] = escape(encodeURIComponent($('#videoField2').val()));
            video['data']['urlLink'] = escape(encodeURIComponent($('#youTubeUrl').val()));
            video['data']['videoRank'] = escape(encodeURIComponent($('#videoRank').val()));
            video['data']['videoYear'] = escape(encodeURIComponent($('#videoYear').val()));
            video['data']['videoUpload'] = $('#videoField3').val();
            video['data']['youtubeVideos'] = $('#youtubeCombo').val();
            video['data']['order'] = $('#videoField4').val();
            video['data']['active'] =  ($('#uniform-videoField5 span').attr('class')=='checked') ? 1 : 0;
            if($('#videoField3').val()!=""){
                var datas=$('#videoField3')[0].files;
                if (window.FormData) {
                    var formdata = new FormData();
                }
                var i = 0, len = datas.length, img, reader, file1;
                for ( ; i < len; i++ ) {
                       file1 = datas[i];


                               if ( window.FileReader ) {
                                       reader = new FileReader();
                                       reader.onloadend = function (e) {

                                       };
                                       reader.readAsDataURL(file1);
                               }
                               if (formdata) {
                                       formdata.append("file[]", file1);
                               }
                }
                if (formdata) {
                    var videoName=$('#videoField2').val();
                    $.ajax({
                        url: "index.php?p=7&m=v&folder="+videoName+"&event="+eventid+"&cid="+cid,
                        type: "POST",
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                                //alert(res);
                        }
                    });
                }
            }
            if(video['data']['courseName']==null || video['data']['courseName']=='null'){
                alert("Please select Course");
            }
            else if(video['data']['videoYear']==null || video['data']['videoYear']=='null'){
                 alert("Please select Year");
            }
            else if(video['data']['videoevent']==null || video['data']['videoevent']=='null'){
                alert("Please select Event");
            }
            else if(video['data']['videoName']=='') {
                alert("Please provide Video name");
            }
            else{
                videoDataRec = get(video,'index.php?p=5','json');
            }
            if(videoDataRec.total>0){
                var lastInsertedId =videoDataRec.lastInsertedId; // photo Id
                       /*****for you tube thumb ****/
                       var data ="youTubeUrl="+unescape(decodeURIComponent($.trim($('#youTubeUrl').val())))+ "&id=" + lastInsertedId + "&m=saveYouTubeVideoThumb";
                       $.ajax({
                           type: "post",
                           url: "index.php?p=6",
                           data: data,
                           error: function(data) {
                               alert("error");
                           },
                           success: function(data){
                               alert(data);
                           }
                       });
                       /*****for you tube thumb END****/
               alert(videoDataRec.total+" Record inserted successfully.");
               $('#cancelVideoData').click();
               this.getDataAll();
            }else{
                    alert("No record inserted.");
                    $('#cancelVideoData').click();
                    this.getDataAll();
            }
        }
        if($('#modeVideo').val() == 2){
               video['mode'] = "updateVideo";
               video['data'] = {};
               video['data']['courseName'] = escape(encodeURIComponent($('#videoField1').val()));
               var cid = $('#videoField1').val();
               video['data']['videoevent'] =$('#videoevent').val();
               var eventid =$('#videoevent').val();
               video['data']['videoName'] = escape(encodeURIComponent($('#videoField2').val()));
               video['data']['videoYear'] = escape(encodeURIComponent($('#videoYear').val()));
               video['data']['urlLink'] = escape(encodeURIComponent($('#youTubeUrl').val()));
               video['data']['videoRank'] = $('#videoRank').val();
               video['data']['videoUpload'] = $('#videoField3').val();
               video['data']['youtubeVideos'] = $('#youtubeCombo').val();
               video['data']['order'] = $('#videoField4').val();

               video['data']['active'] = ($('#uniform-videoField5 span').attr('class')=='checked') ? 1 : 0;
               video['data']['id'] = $('#idVideo').val();
               
               if(video['data']['courseName']==null || video['data']['courseName']=='null'){
                   alert("Please select Course");
               }
               else if(video['data']['videoYear']==null || video['data']['videoYear']=='null'){
                    alert("Please select Year");
               }
               else if(video['data']['videoevent']==null || video['data']['videoevent']=='null'){
                   alert("Please select Event");
               }
               else if(video['data']['videoName']=='') {
                   alert("Please provide Video name");
               }
               else{
                   videoDataRec = get(video,'index.php?p=5','json');
               }
                if(videoDataRec.total>0){
                    var lastInsertedId =videoDataRec.lastInsertedId; // photo Id                    
                    /*****for you tube thumb ****/
                    var oldyoutubeUrl = $("#oldyoutubeUrl").val();     
                    var data ="youTubeUrl="+unescape(decodeURIComponent($.trim($('#youTubeUrl').val())))+"&oldyoutubeUrl="+  oldyoutubeUrl+ "&id=" + lastInsertedId + "&m=saveYouTubeVideoThumb";
                    $.ajax({
                        type: "post",
                        url: "index.php?p=6",
                        data: data,
                        error: function(data) {
                            //alert("error");
                        },
                        success: function(data){
                            //alert(data);
                        }
                    });
                    /*****for you tube thumb END****/
                  alert(videoDataRec.total+" Record Updated successfully.");
                  $('#cancelVideoData').click();
                  this.getDataAll();
               }else{
                  alert("No record updated.");
                  $('#cancelVideoData').click();
                  this.getDataAll();
               }
        }
    }

             this.editData = function(id,courseId,courseName,videoName,videoUpload,order,active,videoRank,videoevent,url,youtube,videoYear){
//                 alert("youTubevalue="+youtube);
                 $('#editVideo').html('Edit');
                 $('#modeVideo').val(2);
                 $('#idVideo').val(id);
                 $('#videoField1').val(courseId);
                 this.getYearOfCourse(courseId);
                 this.getEventOfCourse(videoYear);
                 $('#videoField1_chzn a span').html(unescape(decodeURIComponent(courseName)));
                 $('#videoField2').val(unescape(decodeURIComponent(videoName)));
                 $('#videoField4').val(order);
                 if(active == 1){

                    $('#uniform-videoField5 span').attr('class','checked');
                    $('#videoField5').attr('checked','checked');
                }else{
                    $('#uniform-videoField5 span').attr('class','');
                    $('#videoField5').removeAttr('checked');
                }
                 $('#videoRank').val(unescape(decodeURIComponent(videoRank)));
                 $('#videoevent').val(videoevent);
                 $('#uniform-videoField3 span.filename').val(videoUpload);
                 $('#youTubeUrl').val(unescape(decodeURIComponent(url)));
                 $('#oldyoutubeUrl').val(unescape(decodeURIComponent(url)));
                 $('#youtubeCombo').val(unescape(decodeURIComponent(youtube)));
                 $('#videoYear').val(unescape(decodeURIComponent(videoYear)));
             }

            this.deleteData = function(id,youtubeUrl){
                 var video = {};
                 video['mode'] = "deleteVideo";
                 video['data'] = {};
                 video['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record?");
                 if(r==true){
                 var videoDataRec = get(video,'index.php?p=5','json');
                 if(videoDataRec.total>0){
                    var data = "m=deleteYoutubethumb&videoId="+id + "&youtubeUrl=" + youtubeUrl;
                    /***delete you tube thumb *****/
                    $.ajax({
                        url: "index.php?p=6",
                        type: "POST",
                        data: data,
                        error:function(res){
                      //alert("error");
                        },
                        success: function (res) {
                         //alert(res);
                        }
                    });
                    alert(videoDataRec.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
             }
         }
         this.getYearOfCourse=function(id){
             this.getEventOfCourse(0);
             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color:red;'>*</span>Select Year</label><div class='controls'><select id='videoYear' class='chzn-select' onchange='callAddVideoData.getEventOfCourse(this.value)'><option selected='selected' disabled='disabled' value=''>Select Year</option>";
             changeCourse['mode']='getYearOfCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
             }
             option+="</div>"
             $('#videoYeardiv').html(option);
         }
         this.getEventOfCourse=function(id){
             var changeCourse={};
             var option="<label class='control-label' for='select01'><span style='color:red;'>*</span>Select Event</label><div class='controls'><select id='videoevent' class='chzn-select'><option selected='selected' disabled='disabled' value=''>Select Event</option>";
             changeCourse['mode']='getEventOfCourse';
             changeCourse['data']={};
             changeCourse['data']['id']=id;
             changeCourse['data']['cid']=$('#videoField1').val();
             var courseData = get(changeCourse,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+unescape(decodeURIComponent(courseData['NAME'][i]))+"</option>"
             }
             option+="</div>"
             $('#videoeventdiv').html(option);
         }


         }

         $("#videoSubmit").click(function(){
             if($("#videoField3").val()!=""){
             var validImageFormat=/\.(mp4|flv|avi|3gp)$/i;
             if(!validImageFormat.test($("#videoField3").val())){
                alert("Please upload video with valid format");
                return false;
             }
             }
         })

          $('#cancelVideoData').click(function(){
             $('#modeVideo').val(1);
             $('#editVideo').html('Add');
             $('#idVideo').val("");
             $('#uniform-videoField3 span.filename').html("");
             $('#videoField1').val("");
             $('#videoField1_chzn a span').html("Select Course");
             $('#videoevent').val("");
             $('#videoevent_chzn a span').html("Select Event");
             $('#youtubeCombo').val("");
             $('#youtubeCombo_chzn a span').html("No");
             clearAll();
         });
/************** ADD VIDEO END******************/


/**************CENTER START******************/
var addCenterData = function(){


      this.getDataAll = function(){

                    var center = {};
                     center['mode'] = "getAllCenter";
                     var centerData = get(center,'index.php?p=5','json');
                     if(centerData.total>0){
                         var str = "";
                         $('#addCenterData').html('');
                         for(var i=0;i<centerData.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(centerData.name[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(centerData.phone[i])) + '</td>';
                            str = str + '<td>' + unescape(decodeURIComponent(centerData.email[i])) + '</td>';
                            if(centerData.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="location.href=\'#centreStart\',callAddCenterData.editData('+centerData.id[i]+',\''+escape(centerData.courseId[i])+'\',\''+escape(centerData.name[i])+'\',\''+escape(centerData.address1[i])+'\',\''+escape(centerData.address2[i])+'\',\''+escape(centerData.address3[i])+'\',\''+escape(centerData.address4[i])+'\',\''+centerData.phone[i]+'\',\''+centerData.mobile[i]+'\',\''+escape(centerData.email[i])+'\',\''+centerData.lat[i]+'\',\''+centerData.long1[i]+'\',\''+centerData.order[i]+'\',\''+centerData.active[i]+'\',\''+centerData.state[i]+'\',\''+centerData.city[i]+'\',\''+centerData.keywords[i]+'\',\''+centerData.description[i]+'\',\''+centerData.Title[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddCenterData.deleteData('+centerData.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#addCenterData').append(str);
                         }
                     }else{
                         $('#addCenterData').html("No Content Found.");
                     }

                 }

                this.saveData = function(){
                     var center = {};
                     var centerDataRec;

                    if($('#modeCenter').val() == 1){

                             center['mode'] = "saveCenter";
                             center['data'] = {};

                             var courseName = $('#centerField1').val();
                             center['data']['courseName']=courseName;
                             center['data']['name'] = escape(encodeURIComponent($('#centerField2').val()));
                             center['data']['address1'] = escape(encodeURIComponent($('#centerField3').val()));
                             center['data']['address2'] = escape(encodeURIComponent($('#centerField4').val()));
                             center['data']['address3'] = escape(encodeURIComponent($('#centerField5').val()));
                             center['data']['address4'] = escape(encodeURIComponent($('#centerField6').val()));
                             center['data']['state'] = $('#centreState').val();
                             center['data']['city'] = $('#centerCity').val();
                             center['data']['phone'] = $('#centerField7').val();
                             center['data']['mobile'] = $('#centerField8').val();
                             center['data']['email'] = escape(encodeURIComponent($('#centerField9').val()));
                             center['data']['lat'] = $('#centerField10').val();
                             center['data']['long1'] = $('#centerField11').val();
                             center['data']['order'] = $('#centerField12').val();
                             center['data']['active'] = ($('#uniform-centerField13 span').attr('class')=='checked') ? 1 : 0;
                             center['data']['keywords'] = escape(encodeURIComponent($('#centerField14').val()));
                             center['data']['description'] = escape(encodeURIComponent($('#centerField15').val()));
                             center['data']['Title'] = escape(encodeURIComponent($('#centerField16').val()));
                            if(center['data']['courseName']==null || center['data']['courseName']=='null' || center['data']['courseName']==0 ){
                                 alert("Please provide Class name");
                             }else if(center['data']['name']==''){
                                 alert("Please provide Centre name");
                             }else if(center['data']['Title']==''){
                                 alert("Please provide Title");
                             }else if(center['data']['keywords']==''){
                                 alert("Please provide Keywords");
                             }else if(center['data']['description']==''){
                                 alert("Please provide Description");
                             } else{
                                centerDataRec = get(center,'index.php?p=5','json');
                             }
                             if(centerDataRec.total>0){
                                alert(centerDataRec.total+" Record inserted successfully.");
                                $('#cancelCenterData').click();
                                this.getDataAll();
                             }else{
                                alert("No record inserted.");
                             }
                            document.centreform.reset();
                      }
                         if($('#modeCenter').val() == 2){


                             center['mode'] = "updateCenter";
                             center['data'] = {};
    //                       center['data']['centerType'] = escape(encodeURIComponent($('#centerField0').val()));
                             var courseName = center['data']['courseName'] = $('#centerField1').val().toString();
                             center['data']['name'] = escape(encodeURIComponent($('#centerField2').val()));
                             center['data']['address1'] = escape(encodeURIComponent($('#centerField3').val()));
                             center['data']['address2'] = escape(encodeURIComponent($('#centerField4').val()));
                             center['data']['address3'] = escape(encodeURIComponent($('#centerField5').val()));
                             center['data']['address4'] = escape(encodeURIComponent($('#centerField6').val()));
                             center['data']['phone'] = $('#centerField7').val();
                             center['data']['mobile'] = $('#centerField8').val();
                             center['data']['state'] = $('#centreState').val();
                             center['data']['city'] = $('#centerCity').val();
                             center['data']['email'] = escape(encodeURIComponent($('#centerField9').val()));
                             center['data']['lat'] = $('#centerField10').val();
                             center['data']['long1'] = $('#centerField11').val();
                             center['data']['order'] = $('#centerField12').val();
                             center['data']['active'] = ($('#uniform-centerField13 span').attr('class')=='checked') ? 1 : 0;
                             center['data']['keywords'] = escape(encodeURIComponent($('#centerField14').val()));
                             center['data']['description'] = escape(encodeURIComponent($('#centerField15').val()));
                             center['data']['Title'] = escape(encodeURIComponent($('#centerField16').val()));
                             center['data']['id'] = $('#idCenter').val();
                             if(center['data']['courseName']==null || center['data']['courseName']=='null' || center['data']['courseName']==0){
                                 alert("Please provide Class name");
                             }else if(center['data']['name']==''){
                                 alert("Please provide Centre name");
                             }else if(center['data']['Title']==''){
                                 alert("Please provide Title");
                             }else if(center['data']['keywords']==''){
                                 alert("Please provide Keywords");
                             }else if(center['data']['description']==''){
                                 alert("Please provide Description");
                             }else{
                                centerDataRec = get(center,'index.php?p=5','json');
                             }
                             if(centerDataRec.total>0){
                                alert(centerDataRec.total+" Record Updated successfully.");
                                $('#cancelCenterData').click();
                                this.getDataAll();
                             }else{
                                alert("No record updated.");
                                 this.getDataAll();
                             }
                            document.centreform.reset();
                      }

              }
              this.stateChange=function(id){
                  var option='<select id="centerCity" ><option   value="0">Select City</option>';
                  var changestate={};
                  changestate['mode']='getCityOfState';
                  changestate['data']={};
                  changestate['data']['id']=id;
             var courseData = get(changestate,'index.php?p=5','json');

             for(var i=0;i<courseData['TOTAL'];i++){
                 option+="<option value='"+courseData['ID'][i]+"'>"+courseData['NAME'][i]+"</option>"
             }
             option+="</select>";
             $('#centercityDiv').html(option);
              }
               this.editData = function(id,courseName,name,address1,address2,address3,address4,phone,mobile,email,lat,long1I,order,active,state,city,keywords,description,title){
                 $('#modeCenter').val(2);
                 $('#idCenter').val(id);
                 $('#editCenter').html('Edit');
                 $('#centerField1').val(courseName);
                 $('#centerField2').val(unescape(decodeURIComponent(name)));
                 $('#centerField14').val(unescape(decodeURIComponent(keywords)));
                 $('#centerField15').val(unescape(decodeURIComponent(description)));
                 $('#centerField16').val(unescape(decodeURIComponent(title)));
                 $('#centerField3').val(unescape(decodeURIComponent(address1)));
                 $('#centerField4').val(unescape(decodeURIComponent(address2)));
                 $('#centerField5').val(unescape(decodeURIComponent(address3)));
                 $('#centerField6').val(unescape(decodeURIComponent(address4)));
                 $('#centerField7').val(phone);
                 $('#centreState').val(state);
                 this.stateChange(state);
                 $('#centerCity').val(city);
                 $('#centerField8').val(mobile);
                 $('#centerField9').val(unescape(decodeURIComponent(email)));
                 $('#centerField10').val(lat);
                 $('#centerField11').val(long1I);
                 $('#centerField12').val(order);
                 $('#centerField13').val(active);
                 if(active == 1){
                    $('#uniform-centerField13 span').attr('class','checked');
                    $('#centerField13').attr('checked','checked');
                }else{
                    $('#uniform-centerField13 span').attr('class','');
                    $('#centerField13').removeAttr('checked');
                }


             }

             this.deleteData = function(id){
                 var center = {};
                 center['mode'] = "deleteCenterData";
                 center['data'] = {};
                 center['data']['id'] = id;
                  var r=confirm("Are you sure you want to delete this record?");
                  if(r==true){
                 var centerDataRec = get(center,'index.php?p=5','json');
                 if(centerDataRec.total>0){
                     alert(centerDataRec.total+"Record Deleted Successfully.");
                   this.getDataAll();
                 }
                 else{
                     alert("No record deleted.");
                      this.getDataAll();
                 }
             }
             }
          }
 $('#cancelCentreData').click(function(){
             $('#modeCenter').val(1);
             $('#idCenter').val("");
             $('#editCenter').html('Add');
             $('#centerField13').val('');
             clearAll('center');
         });
/************** CENTER END ******************/

/************** ADD CONTACT START******************/
var addContactData = function(){


      this.getDataAll = function(){



                    var contact = {};
                     contact['mode'] = "getAllContact";
                     var contactData = get(contact,'index.php?p=5','json');

                     if(contactData.total>0){
                         var str = "";
                         $('#addContactData').html('');
                         for(var i=0;i<contactData.total;i++){
                            str = "";
                            str = str + '<tr>';
                            str = str + '<td>' + (i+1) + '</td>';
                             str = str + '<td>' + unescape(decodeURIComponent(contactData.name[i])) + '</td>';
//                            str = str + '<td>' + unescape(decodeURIComponent(contactData.address1[i])) + '</td>';
//                             str = str + '<td>' + unescape(decodeURIComponent(contactData.address2[i])) + '</td>';
//                             str = str + '<td>' + unescape(decodeURIComponent(contactData.address3[i])) + '</td>';
//                             str = str + '<td>' + unescape(decodeURIComponent(contactData.address4[i])) + '</td>';
                             str = str + '<td>' + contactData.phone[i] + '</td>';
                             str = str + '<td>' + contactData.mobile[i] + '</td>';
                             str = str + '<td>' + unescape(decodeURIComponent(contactData.email[i])) + '</td>';
                             str = str + '<td>' + contactData.lat[i] + '</td>';
                             str = str + '<td>' + contactData.long1[i] + '</td>';
                             str = str + '<td>' + contactData.order[i] + '</td>';
                            if(contactData.active[i] == 1){
                                str = str + '<td>Active</td>';
                            }else{
                                str = str + '<td>Inactive</td>';
                            }
                              str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-edit" onclick="callAddContactData.editData('+contactData.id[i]+',\''+escape(contactData.name[i])+'\',\''+escape(contactData.address1[i])+'\',\''+escape(contactData.address2[i])+'\',\''+escape(contactData.address3[i])+'\',\''+escape(contactData.address4[i])+'\',\''+contactData.phone[i]+'\',\''+contactData.mobile[i]+'\',\''+escape(contactData.email[i])+'\',\''+contactData.lat[i]+'\',\''+contactData.long1[i]+'\',\''+contactData.order[i]+'\',\''+contactData.active[i]+'\');"></i>' + '</td>';
                            str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callAddContactData.deleteData('+contactData.id[i]+');"></i>' + '</td>';
                            str = str + '</tr>';
                            $('#addContactData').append(str);
                         }
                     }else{
                         $('#addContactData').html("No Content Found.");
                     }

                 }
                this.saveData = function(){
                 var contact = {};
                 var contactDataRec;
                if($('#modeContact').val() == 1){
                         contact['mode'] = "saveContact";
                         contact['data'] = {};

                         contact['data']['name'] = escape(encodeURIComponent($('#contactField1').val()));
                         contact['data']['address1'] = escape(encodeURIComponent($('#contactField2').val()));
                         contact['data']['address2'] = escape(encodeURIComponent($('#contactField3').val()));
                         contact['data']['address3'] = escape(encodeURIComponent($('#contactField4').val()));
                         contact['data']['address4'] = escape(encodeURIComponent($('#contactField5').val()));
                         contact['data']['phone'] = $('#contactField6').val();
                         contact['data']['mobile'] = $('#contactField7').val();
                         contact['data']['email'] = escape(encodeURIComponent($('#contactField8').val()));
                         contact['data']['lat'] = $('#contactField9').val();
                         contact['data']['long1'] = $('#contactField10').val();
                         contact['data']['order'] = $('#contactField11').val();
                         contact['data']['active'] = ($('#uniform-contactField12 span').attr('class')=='checked') ? 1 : 0;
                         if(contact['data']['name']==''){
                             alert("Please provide Office name");
                         }
                         else{
                            contactDataRec = get(contact,'index.php?p=5','json');
                         }
                         if(contactDataRec.total>0){
                             alert(contactDataRec.total+" Record inserted successfully.");
                            $('#cancelContactUsData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }
                     if($('#modeContact').val() == 2){


                         contact['mode'] = "updateContact";
                         contact['data'] = {};
                         contact['data']['name'] = escape(encodeURIComponent($('#contactField1').val()));
                         contact['data']['address1'] = escape(encodeURIComponent($('#contactField2').val()));
                         contact['data']['address2'] = escape(encodeURIComponent($('#contactField3').val()));
                         contact['data']['address3'] = escape(encodeURIComponent($('#contactField4').val()));
                         contact['data']['address4'] = escape(encodeURIComponent($('#contactField5').val()));
                         contact['data']['phone'] = $('#contactField6').val();
                         contact['data']['mobile'] = $('#contactField7').val();
                         contact['data']['email'] = escape(encodeURIComponent($('#contactField8').val()));
                         contact['data']['lat'] = $('#contactField9').val();
                         contact['data']['long1'] = $('#contactField10').val();
                         contact['data']['order'] = $('#contactField11').val();
                         contact['data']['active'] = ($('#uniform-contactField12 span').attr('class')=='checked') ? 1 : 0;
                         contact['data']['id'] = $('#idContact').val();
                         if(contact['data']['name']==''){
                             alert("Please provide Office name");
                         }
                         else{
                            contactDataRec = get(contact,'index.php?p=5','json');
                         }

                         if(contactDataRec.total>0){
                            alert(contactDataRec.total+" Record Updated successfully.");
                            $('#cancelContactUsData').click();
                            this.getDataAll();
                         }else{
                            alert("No record inserted.");
                         }
                  }

              }
               this.editData = function(id,name,address1,address2,address3,address4,phone,mobile,email,lat,long1,order,active){
                 $('#editContent').html('Edit');
                 $('#modeContact').val(2);
                 $('#idContact').val(id);
                 $('#contactField1').val(unescape(decodeURIComponent(name)));
                 $('#contactField2').val(unescape(decodeURIComponent(address1)));
                 $('#contactField3').val(unescape(decodeURIComponent(address2)));
                 $('#contactField4').val(unescape(decodeURIComponent(address3)));
                 $('#contactField5').val(unescape(decodeURIComponent(address4)));
                 $('#contactField6').val(phone);
                 $('#contactField7').val(mobile);
                 $('#contactField8').val(unescape(decodeURIComponent(email)));
                 $('#contactField9').val(lat);
                 $('#contactField10').val(long1);
                 $('#contactField11').val(order);



                 if(active == 1){

                    $('#uniform-contactField12 span').attr('class','checked');
                    $('#contactField12').attr('checked','checked');
                }else{
                    $('#uniform-contactField12 span').attr('class','');
                    $('#contactField12').removeAttr('checked');
                }


             }

             this.deleteData = function(id){
                 var contact = {};
                 contact['mode'] = "deleteContactData";
                 contact['data'] = {};
                 contact['data']['id'] = id;
                 var r=confirm("Are you sure you want to delete this record ?");
                     if(r==true){
                          var contactData = get(contact,'index.php?p=5','json');
                 if(contactData.total>0){
                     alert(contactData.total+" Record Deleted successfully.");
                    this.getDataAll();
                 }else{
                    alert("No record inserted.");
                 }
                     }
                 }

             }

             $('#cancelContactUsData').click(function(){
             $('#modeContact').val(1);
             $('#idContact').val("");
             $('#editContent').html('Add');

             clearAll('contact');
         });
/************** ADD CONTACT END******************/


/***********function end by deepak -pandey for  open video*************/
 function openYouTube(){
                           window.open("http://www.youtube.com/");
                       }
/***********function end by deepak -pandey for open video*************/


//****Home  //
var homeBannerData = function(){

                         this.getDataAll = function(){
                                var home = {};
                                 home['mode'] = "getAllHomeData";
                                 var homeDataRec = get(home,'index.php?p=5','json');
                                 if(homeDataRec.total>0){
                                     var str = "";
                                     $('#homeBannerData').html('');
                                     for(var i=0;i< homeDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.BANNER_NAME[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.BANNER_TEXT[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.ORDER_BANNER[i])) + '</td>';
//                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.BANNER_PATH[i])) + '</td>';
                                        if(homeDataRec.ACTIVE[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=11&ln=1&t=6&e=2&id='+homeDataRec.ID[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callHomeData.deleteData('+homeDataRec.ID[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#homeBannerData').append(str);
                                     }
                                 }else{
                                     $('#homeBannerData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

this.saveData = function(){
        var banner = {};
        var homeDataRec;
        if ($('#modeBanner').val() == 1) {
            banner['mode'] = "saveHomeBanner";
            banner['data'] = {};
            banner['data']['bannerName'] = escape(encodeURIComponent(($('#bannerField1').val())));
            banner['data']['bannerOrder'] = $('#bannerField2').val();
            banner['data']['imageUpload'] = $('#bannerField3').val();  //for imageupload
            banner['data']['contentBanner'] = $('#bannerField4 textarea').val();
            banner['data']['contentLink'] = $('#bannerField6').val();
            banner['data']['contentBanner'] = escape(encodeURIComponent(banner['data']['contentBanner']));
            banner['data']['active'] = ($('#bannerField5').is(':checked')) ? 1 : 0;
            if (banner['data']['bannerName'] == '') {
                alert("Please provide Banner Name");
            } else if (banner['data']['bannerOrder'] == '') {
                alert("Please provide Banner Order");
            } else if (banner['data']['imageUpload'] == '') {
                alert("Please upload Banner Image");
            } else if (banner['data']['contentBanner'] == '') {
                alert("Please provide Banner Content");
            } else {
                homeDataRec = get(banner, 'index.php?p=5', 'json');
            }
            if (homeDataRec.total > 0) {
                var lastInsertedId = homeDataRec.lastInsertedId;
                var file_data = $('#bannerField3').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                var url = "index.php?p=6&m=ajaxUpload&lastInsertedId=" + lastInsertedId;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form_data,
                    async: false,
                    processData: false,
                    contentType: false,
                    error: function (res) {
                        alert("Unsuccessful.");
                        return false;
                    },
                    success: function (res) {
                        //alert(res);
                    }
                });
                alert("Record inserted successfully.");
                $('#bannerField1').val("");
                $('#bannerField2').val("");
                $('#bannerField3').val("");
                $('#bannerField4 textarea').val("");
                $('#bannerField6').val("");  
                $('#cancelBannerData').click();

                this.getDataAll();
            } else {
                alert("No record inserted.");
            }
        }


if ($('#modeBanner').val() == 2) {
            var banner = {};
            var homeDataRec;
            banner['mode'] = "updateHomeBanner"; 
            banner['data'] = {};
            banner['data']['id']    = $("#idBanner").val();
            banner['data']['bannerName']    = escape(encodeURIComponent(($('#bannerField1').val())));
            banner['data']['bannerOrder']   = $('#bannerField2').val();
            banner['data']['contentLink']   = $('#bannerField6').val();
            if($('#bannerField3').val()== ''){
                 banner['data']['imageUpload']   = $('#bannerFEdit3').val();  //for imageupload
            }else {
                 banner['data']['imageUpload']   = $('#bannerField3').val();  //for imageupload
            }
            banner['data']['contentBanner'] = $('#bannerField4 textarea').val();
            banner['data']['contentBanner'] = escape(encodeURIComponent(banner['data']['contentBanner']));
            banner['data']['active'] = ($('#bannerField5').is(':checked')) ? 1 : 0;
            if (banner['data']['bannerName'] == '') {
                alert("Please provide Banner Name");
                return false;
            } else if (banner['data']['bannerOrder'] == '') {
                alert("Please provide Banner Order");
                return false;
            } else if (banner['data']['imageUpload'] == '') {
                alert("Please upload Banner Image");
                return false;
            } else if (banner['data']['contentBanner'] == '') {
                alert("Please provide Banner Content");
                return false;
            } else {
                homeDataRec = get(banner, 'index.php?p=5', 'json');
            }
                var Id = $("#idBanner").val();
                var file_data = $('#bannerField3').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
        var url = "index.php?p=6&m=ajaxUpload&lastInsertedId=" + Id;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form_data,
                    async: false,
                    processData: false,
                    contentType: false,
                    error: function (res) {
                        alert("Unsuccessful.");
                        return false;
                    },
                    success: function (res) {
                        //alert(res);
                    }
                });
                alert("Record updated successfully.");
                $('#bannerField1').val("");
                $('#bannerField2').val("");
                $('#bannerField3').val("");
                $('#bannerField4 textarea').val("");
                 $('#bannerField6').val("");
                $('#cancelBannerData').click();
                this.getDataAll();
                window.location="index.php?p=11&ln=1&t=6" ;
        }
            
    }
    
    $("#bannerField3").change(function (){
    var imgWidth = '';
    var imgHeight = '';
    var input = document.getElementById("bannerField3");
    var fname = $("#bannerField3").val();
    var extension = fname.substr(fname.lastIndexOf(".") + 1);
    if (extension != 'png' && extension != 'jpg' && extension != 'jpeg' && extension != 'gif') {
        alert("Only Upload png,jpeg,jpg or gif type images");
        $("#bannerField3").val("");
        $("#bannerField3").focus();
        return false;
    }   
    
     
//        var fname = $("#bannerField3").val();
//
//        imgWidth = this.width;
//        imgHeight = this.height;
//        if (parseInt(imgWidth) <= parseInt('1337') && parseInt(imgHeight) <= parseInt('501') || parseInt(imgWidth) > parseInt('1438') && parseInt(imgHeight) > parseInt('502')) {
//            alert("Image dimension should be in between 1337X500 and 1438x502.");
//            $("#bannerField3").val("");
//            $("#bannerField3").focus();
//            return false;
//        }
            });       
            
                    $('#cancelBannerData').click(function(){
                      if ($('#modeBanner').val() == 2) {
                            window.location="index.php?p=11&ln=1&t=6" ;
                        }
                                $('#bannerField1').val("");
                                $('#bannerField2').val("");
                                $('#bannerField3').val("");
                                $('#bannerField4 textarea').val("");
                                clearAll();
                     });



                         this.editData = function(id){
             setTimeout(function() {
            $("#uniform-bannerField3 span.filename").html(unescape(decodeURIComponent(id + '.' + homeDataRec.BANNER_PATH)));
            $('#bannerFEdit3').val(id + '.' + homeDataRec.BANNER_PATH);
                }, 1000);
                                var home = {};
                                home['mode'] = "getEditHomeData";
                                home['id']   = id ;
                                var homeDataRec = get(home,'index.php?p=5','json');
                             $('#editBanner').html('Edit');
                             $('#modeBanner').val(2);
                             $('#idBanner').val(id);
                             $('#bannerField1').val(unescape(decodeURIComponent(homeDataRec.BANNER_NAME)));
                             $('#bannerField2').val(homeDataRec.ORDER_BANNER);
//                             $("#uniform-bannerField3 span.filename").html(unescape(decodeURIComponent(homeDataRec.BANNER_PATH)));
                             $('#bannerField4 textarea').val(unescape(decodeURIComponent(homeDataRec.BANNER_TEXT)));
//                             $('#bannerFEdit3').val(id+'.'+homeDataRec.BANNER_PATH);
                             $('#bannerField6').val(unescape(decodeURIComponent(homeDataRec.BANNER_LINK)));
                             if( homeDataRec.ACTIVE == '1'){
                                $('#bannerField5').attr('class','checked');
                                $('#bannerField5').attr('checked','checked');
                            }else{
                                $('#bannerField5').attr('class','');
                                $('#bannerField5').removeAttr('checked');
                            }
                    }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var banner = {};
                             banner['mode'] = "deleteBanner";
                             banner['data'] = {};
                             banner['data']['id'] = id;
                             var bannerDataRec = get(banner,'index.php?p=5','json');
                             if(bannerDataRec.total>0){
                                alert("Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record deleted.");
                             }
                         }
                }
}

/*****START: Home -> Useful Link ***********************************************/
var homeUseFulLinkData = function(){ 

                         this.getDataAll = function(){
                                var link = {};
                                 link['mode'] = "getAllLink";
                                 var linkDataRec = get(link,'index.php?p=5','json');
                                 if(linkDataRec.total>0){ 
                                     var str = "";
                                     $('#linkBannerData').html('');
                                     for(var i=0;i< linkDataRec.total;i++){ 
                                        var subLinkText = unescape(decodeURIComponent(linkDataRec.subLinkText[i]));
                                        if(subLinkText == "" || subLinkText == "null" || subLinkText == "NULL"){
                                           subLinkText = "--NA--" 
                                        }
                                        var SubLinkText = subLinkText.replace(/#/g,"<br>");
                                        
                                        
                                        
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(linkDataRec.linkText[i])) + '</td>';
                                        str = str + '<td>' + linkDataRec.urlLink[i] + '</td>';
                                        str = str + '<td>' + SubLinkText + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(linkDataRec.order[i])) + '</td>';
                                        if(linkDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }
                                        
                                        if(linkDataRec.openInNew[i] == 1){
                                            str = str + '<td>Yes</td>';
                                        }else{
                                            str = str + '<td>No</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=11&ln=6&t=6&e=2&id='+linkDataRec.id[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callUseFulLink.deleteData('+linkDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#linkBannerData').append(str);
                                     }
                                 }else{
                                     $('#linkBannerData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }
                         }

this.saveData = function(){
        var useLink = {};
        var homeDataRec;
if ($('#modeLink').val() == 1) {
            useLink['mode'] = "saveLinkData"; 
            useLink['data'] = {};
            useLink['data']['subLinkText'] = {};
            useLink['data']['subLink'] = {} ;
            useLink['data']['linkName']      =escape(encodeURIComponent(($('#linkField1').val())));
            useLink['data']['linkOrder']     = $('#linkField2').val();
            useLink['data']['urlLink']       = $('#linkField3').val();
            
            var noOfFields = $("#totalFilds").val();
            if(noOfFields==""){
                noOfFields= 1;
            }
            for(var i =1 ;i<=noOfFields ;i++){  
                if($("#linkField5" + i).val() !="" && $("#linkField6" + i).val() ==""){
                    alert ("Please Provide Sub Link: "+ i);
                    return false;   
                }
                if($("#linkField5" + i).val() =="" && $("#linkField6" + i).val() !=""){
                    alert ("Please Provide Sub Link Text: " +i);
                    return false;   
                }
                 
                if($("#linkField5" + i).val() !="" && $("#linkField6" + i).val() !=""){
                    useLink['data']['subLinkText'][i] = $("#linkField5" + i).val();
                    useLink['data']['subLink'][i]     = $("#linkField6" + i).val();
                }
            }
           
            useLink['data']['openInNew']      = ($('#linkField41').is(':checked')) ? 1 : 0 ;
            useLink['data']['active']      = ($('#linkField4').is(':checked')) ? 1 : 0 ;
          
            if (useLink['data']['linkName'] == '') {
                alert("Please provide Link Name");
            } else if (useLink['data']['linkOrder'] == '') {
                alert("Please provide Link Order");
            } else if (useLink['data']['urlLink'] == '') {
                alert("Please provide URL Link");
            } 
             else{
                homeDataRec = get(useLink, 'index.php?p=5', 'json');
            }
   
            if(homeDataRec.total>0){
                alert("Record inserted successfully.");
                $('#linkField1').val("");
                $('#linkField2').val("");
                $('#linkField3').val("");
                $('#linkField4').val("");
                $('#linkField41').val("");
                
                for(var i =1 ;i<=noOfFields ;i++){
                    $("#linkField5" + i).val("");
                    $("#linkField6" + i).val("");
                }
                $('#cancelLinkData').click();
                this.getDataAll();
               // window.location="index.php?p=11&ln=6&t=6" ;
            } else {
                alert("No record inserted.");
            }
        }


if ($('#modeLink').val() == 2) { 
            var link = {};
            var linkDataRec;
            link['mode'] = "updateLink"; 
            link['data'] = {};
            link['data']['subLinkText'] = {};
            link['data']['subLink'] = {} ; 
            link['data']['id']         = $("#linkId").val();
            link['data']['linkName']    = escape(encodeURIComponent(($('#linkField1').val())));
            link['data']['linkOrder']   = $('#linkField2').val();
            link['data']['urlLink']   = $('#linkField3').val();
            
       
            var noOfFields = $("#totalFilds").val();
            for(var i =1 ;i<=noOfFields ;i++){
                
                 if($("#linkField5" + i).val() !="" && $("#linkField6" + i).val() ==""){
                    alert ("Please Provide Sub URL Link: "+ i);
                    return false;   
                }
                if($("#linkField5" + i).val() =="" && $("#linkField6" + i).val() !=""){
                    alert ("Please Provide Sub Link Text: "+ i);
                    return false;   
                }
                
                if($("#linkField5" + i).val() !="" && $("#linkField6" + i).val() !=""){
                    link['data']['subLinkText'][i] = $("#linkField5" + i).val();
                    link['data']['subLink'][i]     = $("#linkField6" + i).val();
                }
            }
             
            link['data']['openInNew'] = ($('#linkField41').is(':checked')) ? 1 : 0; 
            link['data']['active'] = ($('#linkField4').is(':checked')) ? 1 : 0;
            
            if (link['data']['linkName'] == '') {
                alert("Please provide Link Name");
                return false;
            } else if (link['data']['linkOrder'] == '') {
                alert("Please provide  Order");
                return false;
            } else if (link['data']['urlLink'] == '') {
                alert("Please provide URL Link ");
                return false;
            } else {
                
                linkDataRec = get(link, 'index.php?p=5', 'json');
            }
               
                alert("Record updated successfully.");
                $('#linkField1').val("");
                $('#linkField2').val("");
                $('#linkField3').val("");
                $('#linkField4').val("");
                 $('#linkField41').val("");
                $('#cancelBannerData').click();
                this.getDataAll();
                window.location="index.php?p=11&ln=6&t=6" ;
        }
            
    }
                    $('#cancelLinkData').click(function(){
                      if ($('#modeLink').val() == 2) {
                            window.location="index.php?p=11&ln=1&t=6" ;
                        }
                            $('#linkField1').val("");
                            $('#linkField2').val("");
                            $('#linkField3').val("");
                            clearAll();
                     });



        this.editData = function(id){
            
                    var link = {};
                        link['mode'] = "getAllLink";
                        link['data']   = {};
                        link['id'] = id ;
                        var linkDataRec = get(link,'index.php?p=5','json');
                        var subLinkText = unescape(decodeURIComponent(linkDataRec.subLinkText));
                        var subLink = unescape(decodeURIComponent(linkDataRec.subLink));
                        var noOfSubLinkText = subLinkText.split("#");
                        var noOfSubLink = subLink.split("#");
                        $("#totalFilds").val(noOfSubLinkText.length);
                        
                             $('#editLink').html('Edit');
                             $('#modeLink').val(2);
                             $('#linkId').val(id);
                             $('#linkField1').val(unescape(decodeURIComponent(linkDataRec.linkText)));
                             $('#linkField2').val(unescape(decodeURIComponent(linkDataRec.order)));
                             $("#linkField3").val(unescape(decodeURIComponent(linkDataRec.urlLink)));
                      
                             if(noOfSubLinkText.length>0 && subLinkText !="") {
                                 
                                  $("#a_linkField5").click() ;
                                } 
                                
                            if(noOfSubLinkText.length > 0) {   
                               
                                for(var i=0; i<noOfSubLinkText.length-1; i++){
                                //var noOfFields = $("#totalFilds").val();
                                str = '<div id="TextBoxDiv' + (i+2) + '"><label><span>Sub Link Text '+ (i+2) +' :</span>  <span style="margin-left: 15%">Sub URL Link '+ (i+2) +' :</span> </label>' +
	                         '<input type="text" name="subLinkText' + (i+2) + '" id="linkField5' + (i+2) + '" value="" > <input type="text"  data-provide="typeahead" name="subLink' + (i+2) + '" \n\
                                 id="linkField6' + (i+2) + '" value="" ></div>';
                                
                                  $('#TextBoxesGroup').append(str);
                                 
                                }
                                
                                for(var i=0; i<noOfSubLinkText.length; i++){
                                    $("#linkField5"+(i+1)).val(noOfSubLinkText[i]);
                                    $("#linkField6"+(i+1)).val(noOfSubLink[i]);
                                }
                               
                            }
                            
                            if( linkDataRec.openInNew == '1'){
                                $('#uniform-linkField4 span').attr('class','checked');
                                $('#linkField41').attr('checked','checked');
                            }else{
                                $('#uniform-linkField3 span').attr('class','');
                                $('#linkField41').removeAttr('checked');
                            }

                            if( linkDataRec.active == '1'){
                                $('#uniform-linkField4 span').attr('class','checked');
                                $('#linkField4').attr('checked','checked');
                            }else{
                                $('#uniform-linkField3 span').attr('class','');
                                $('#linkField4').removeAttr('checked');
                            }
                         
                    }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var useFull = {};
                             useFull['mode'] = "deleteUseFulLink";
                             useFull['data'] = {};
                             useFull['data']['id'] = id;
                             var bannerDataRec = get(useFull,'index.php?p=5','json');
                             if(bannerDataRec.total>0){
                                alert("Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record deleted.");
                             }
                         }
                }
        }
        /*****END: Home -> Useful Link ****************************************/        

var TopperTalkData = function(){

                         this.getDataAll = function(){
                                var topper = {};
                                 topper['mode'] = "getAllToperTalk";
                                 var toperDataRec = get(topper,'index.php?p=5','json');
                                 if(toperDataRec.total>0){
                                     var str = "";
                                     $('#topperTalkData').html('');
                                     for(var i=0;i< toperDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(toperDataRec.url[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(toperDataRec.order[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(toperDataRec.studentName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(toperDataRec.rank[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(toperDataRec.text[i])) + '</td>';
                                        if(toperDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=11&ln=4&t=6&e=2&id='+toperDataRec.id[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="topperTalkData.deleteData('+toperDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#topperTalkData').append(str);
                                     }
                                 }else{
                                     $('#topperTalkData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

this.saveData = function(){
        var topperData = {};
        var topperDataRec;
if ($('#modeTopperTalk').val() == 1) {
            topperData['mode'] = "saveTopperData";
            topperData['data'] = {};
            topperData['data']['urlLink']      = escape(encodeURIComponent(($('#topperField1').val())));
            topperData['data']['text']         = escape(encodeURIComponent(($('#topperField4').val())));
            topperData['data']['order']        = $('#topperField2').val();
            topperData['data']['studentName']  = $('#topperField10').val();
            topperData['data']['rank']         = $('#topperField11').val();
            topperData['data']['active']       = ($('#topperField3').is(':checked')) ? 1 : 0 ;
            
            if (topperData['data']['studentName'] == '') {
                alert("Please provide Student Name");
            //}else if (topperData['data']['rank'] == '') {
                //alert("Please provide Rank");
            }else  if (topperData['data']['urlLink'] == '') {
                alert("Please provide Link");
            } else {
                topperDataRec = get(topperData, 'index.php?p=5', 'json');
            }
            
            if(topperDataRec.imgError>0){
                alert("Thumbnail not available.");
                return false; 
            }
            if(topperDataRec.total>0){
                    alert("Record inserted successfully.");
                    $('#topperField1').val("");
                    $('#topperField2').val("");
                    $('#topperField3').val("");
                    $('#cancelToperTalk').click();
                    this.getDataAll();
                } else {
                    alert("No record inserted.");
                }
            }


if ($('#modeTopperTalk').val() == 2) {
        var topperData    = {} ;
        var topperDataRec = {} ;
        
            topperData['mode'] = "updateTopperData";
            topperData['data'] = {};
            topperData['data']['id']           = $("#idTopperTalk").val();
            topperData['data']['urlLink']      = escape(encodeURIComponent(($('#topperField1').val())));
            topperData['data']['text']         = escape(encodeURIComponent(($('#topperField4').val())));
            topperData['data']['order']        = $('#topperField2').val();
            topperData['data']['active']       = ($('#topperField3').is(':checked')) ? 1 : 0 ;
            topperData['data']['studentName']  = $('#topperField10').val();
            topperData['data']['rank']         = $('#topperField11').val();
            
            if (topperData['data']['studentName'] == '') {
                alert("Please provide Student Name");
            //}else if (topperData['data']['rank'] == '') {
                //alert("Please provide Rank");
            }else if (topperData['data']['urlLink'] == '') {
                alert("Please provide Link");
            }else{    
                topperDataRec = get(topperData, 'index.php?p=5', 'json');
         
            }
                alert("Record updated successfully.");
                $('#topperField1').val("");
                $('#topperField2').val("");
                $('#topperField3').val("");
                $('#cancelToperTalk').click();
                this.getDataAll();
                window.location="index.php?p=11&ln=4&t=6" ;
        }
            
    }
                    $('#cancelToperTalk').click(function(){
                      if ($('#modeTopperTalk').val() == 2) {
                            window.location="index.php?p=11&ln=4&t=6" ;
                        }
                            $('#topperField1').val("");
                            $('#topperField2').val("");
                            $('#topperField3').val("");
                            clearAll();
                     });



        this.editData = function(id){
            
                    var topper = {};
                        topper['mode']   = "getAllToperTalk";
                        topper['data']   = {};
                        topper['id'] = id ;
                        $("#idTopperTalk").val(id);
                        var topperDataRec = get(topper,'index.php?p=5','json');
                             $('#editTopperTalk').html('Edit');
                             $('#modeTopperTalk').val(2);
                             $('#idTopperTalk').val(id);
                             $('#topperField1').val(unescape(decodeURIComponent(topperDataRec.url)));
                             
                             $('#topperField10').val(unescape(decodeURIComponent(topperDataRec.studentName)));
                             $('#topperField11').val(unescape(decodeURIComponent(topperDataRec.rank)));
                             $('#topperField4').val(unescape(decodeURIComponent(topperDataRec.text)));
                             
                             $('#topperField2').val(unescape(decodeURIComponent(topperDataRec.order)));
                            if( topperDataRec.active == '1'){
                                $('#uniform-topperField3 span').attr('class','checked');
                                $('#topperField3').attr('checked','checked');
                            }else{
                                $('#uniform-topperField3 span').attr('class','');
                                $('#topperField3').removeAttr('checked');
                            }
                    }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var topper = {};
                             topper['mode'] = "deleteTopperTalk";
                             topper['data'] = {};
                             topper['data']['id'] = id;
                             var topperDataRec = get(topper,'index.php?p=5','json');
                             if(topperDataRec.total>0){
                                alert("Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record deleted.");
                             }
                         }
                }
         }
         

         
         
var newsAndEvent = function(){

                         this.getDataAll = function(){
                                var news = {};
                                 news['mode']    = "getAllNewsAndEvent";
                                 var newsDataRec = get(news,'index.php?p=5','json');
                                 var description = "";
                                 if( newsDataRec.total>0){
                                     var str = "";
                                     $('#newsAndEventData').html('');
                                     for(var i=0;i< newsDataRec.total;i++){
                                         if(((newsDataRec.description[i]).length>35)){
                                             description = (newsDataRec.description[i]).substr(0,30)+"...";
                                         }else{
                                             description = newsDataRec.description[i];
                                         }
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(newsDataRec.date[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(newsDataRec.text[i])) + '</td>';
                                        str = str + '<td>' + description + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(newsDataRec.order[i])) + '</td>';
                                        if(newsDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=11&ln=3&t=6&e=2&id='+newsDataRec.id[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callnewsAndEvent.deleteData('+newsDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#newsAndEventData').append(str);
                                     }
                                 }else{
                                     $('#newsAndEventData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

this.saveData = function(){
        var newsData = {};
        var newsDataRec;
        
if ($('#modeNewsAndEvent').val() == 1) {
            newsData['mode'] = "saveNewsData";
            newsData['data'] = {};
            newsData['data']['date']         = $('#newsEventField3').val();
            newsData['data']['text']         = $('#newsEventField4').val();
            newsData['data']['order']        = $('#newsEventField5').val();
            newsData['data']['description']  =  escape(encodeURIComponent($('#newsEventField6 textarea').val()));
            newsData['data']['active']       = ($('#newsEventField7').is(':checked')) ? 1 : 0 ;
        
        if (newsData['data']['date'] == '') {
                alert("Please provide Date");
            } else if (newsData['data']['text'] == '') {
                    alert("Please provide Text");
            } else if (newsData['data']['order'] == '') {
                    alert("Please provide Order");
            }else{      
                newsDataRec = get(newsData, 'index.php?p=5', 'json');
            }
            
            
            if(newsDataRec.total>0){
                alert("Record inserted successfully.");
                $('#newsEventField3').val("");
                $('#newsEventField4').val("");
                $('#newsEventField5').val("");
                $('#newsEventField6 textarea').val("");
                $('#newsEventField7').val("") ;
                this.getDataAll();
            } else {
                alert("No record inserted.");
            }
        }


if ($('#modeNewsAndEvent').val() == 2) {
       newsData['mode'] = "updateNewsData";
            newsData['data'] = {};
            newsData['data']['id']           = $('#idNewsAndEvent').val();
            newsData['data']['date']         = $('#newsEventField3').val();
            newsData['data']['text']         = $('#newsEventField4').val();
            newsData['data']['order']        = $('#newsEventField5').val();
            newsData['data']['description']  = $('#newsEventField6 textarea').val();
            newsData['data']['active']       = ($('#newsEventField7').is(':checked')) ? 1 : 0 ;
        
        
        if (newsData['data']['date'] == '') {
                alert("Please provide Date");
            } else if (newsData['data']['text'] == '') {
                    alert("Please provide Text");
            } else if (newsData['data']['order'] == '') {
                    alert("Please provide Order");
            }else{      
                newsDataRec = get(newsData, 'index.php?p=5', 'json');
            }
                
                alert("Record updated successfully.");
                $('#newsEventField3').val("");
                $('#newsEventField4').val("");
                $('#newEventField5').val("");
                $('#newsEventField6 textarea').val("");
                $('#newsEventField7').val("");
                $('#cancelNewsData').click();
                this.getDataAll();
                window.location="index.php?p=11&ln=3&t=6" ;
        }
            
    }
                    $('#cancelNewsData').click(function(){
                            window.location="index.php?p=11&ln=3&t=6" ;
                            $('#newsEventField3').val("");
                            $('#newsEventField4').val("");
                            $('#newEventField5').val("");
                            $('#newsEventField6').val("");
                            $('#newsEventField7').val("");
                            clearAll();
                     });



        this.editData = function(id){
            
                    var news = {};
                        news['mode'] = "getAllNewsAndEvent";
                        news['data']   = {};
                        news['id'] = id ;
                        $("#idTopperTalk").val(id);
                        var newsDataRec = get(news,'index.php?p=5','json');
                        
                             $('#editNews').html('Edit');
                             $('#modeNewsAndEvent').val(2);
                             $('#idNewsAndEvent').val(id);
                            
                             $('#newsEventField3').val(unescape(decodeURIComponent(newsDataRec.date[0])));
                             $('#newsEventField4').val(unescape(decodeURIComponent(newsDataRec.text[0])));
                             $('#newsEventField6 textarea').val(newsDataRec.description[0]);
                             $('#newsEventField5').val(unescape(decodeURIComponent(newsDataRec.order[0])));
                            if( newsDataRec.active[0] == '1'){
                                $('#uniform-newsEventField7 span').attr('class','checked');
                                $('#newsEventField7').attr('checked','checked');
                            }else{
                                $('#uniform-newsEventField7 span').attr('class','');
                                $('#newsEventField7').removeAttr('checked');
                            }
                    }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var topper = {};
                             topper['mode'] = "deleteNewsEvent";
                             topper['data'] = {};
                             topper['data']['id'] = id;
                             var topperDataRec = get(topper,'index.php?p=5','json');
                             if(topperDataRec.total>0){
                                alert("Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record deleted.");
                             }
                         }
                }
         }
         
         
         
         
         
         
         
         
         
         
         
         
var PhotoGallery = function(){

                         this.getDataAll = function(){
                                var photo = {};
                                    photo['mode'] = "getAllPhotoGallery";
                                 var photoDataRec = get(photo,'index.php?p=5','json');
                                 if( photoDataRec.total>0){
                                     var str = "";
                                     $('#photoGallery').html('');
                                     for(var i=0;i< photoDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        
                                        str = str + '<td>' + unescape(decodeURIComponent(photoDataRec.studentName[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(photoDataRec.rank[i])) + '</td>';
                                        
                                        str = str + '<td>' + unescape(decodeURIComponent(photoDataRec.url[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(photoDataRec.photoText[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(photoDataRec.order[i])) + '</td>';
                                        if(photoDataRec.active[i] == 1){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=11&ln=5&t=6&e=2&id='+photoDataRec.id[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callPhotoGall.deleteData('+photoDataRec.id[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#photoGallery').append(str);
                                     }
                                 }else{
                                     $('#photoGallery').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                 }




                         }

this.saveData = function(){
        var photoData = {};
        var photoDataRec;
        
if ($('#modePhotoGall').val() == '1' ) {
            photoData['mode'] = "savePhotData";
            photoData['data'] = {};
            photoData['data']['urlLink']      = escape(encodeURIComponent(($('#photoField1').val())));
            photoData['data']['order']        = $('#photoField2').val();
            photoData['data']['photoPath']    = $('#photoField3').val();
            
            photoData['data']['studentName']  = $('#photoField11').val();
            photoData['data']['rank']         = $('#photoField12').val();
            
            photoData['data']['active']       = ($('#photoField5').is(':checked')) ? 1 : 0 ;
            photoData['data']['text']         = $('#photoField4').val();
            photoData['data']['text']         = escape(encodeURIComponent(photoData['data']['text']));
            
        if (photoData['data']['studentName'] == '') {
                alert("Please provide Student Name");
            }
            else if(photoData['data']['rank'] == ''){
                alert("Please provide Rank");
            }
            else if(photoData['data']['order'] == ''){
                alert("Please provide Order");
            }
            else if(photoData['data']['photoPath'] == ''){
                alert("Please provide Image");
            }else {
                photoDataRec = get(photoData, 'index.php?p=5', 'json');
            }
                var Id = photoDataRec.lastInsertedId;
                var file_data = $('#photoField3').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                var url   = "index.php?p=6&m=ajaxPhotoUpload&lastInsertedId=" + Id;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form_data,
                    async: false,
                    processData: false,
                    contentType: false,
                    error: function (res) {
                        alert("Unsuccessful.");
                        return false;
                    },
                    success: function (res) {
                        alert("Photo upload successfully.");
                    }
                });
                
            if(photoDataRec.total>0){
                alert("Record inserted successfully.");
                $('#topperField1').val("");
                $('#topperField2').val("");
                $('#topperField3').val("");
                $('#cancelToperTalk').click();
                this.getDataAll();
            } else {
                alert("No record inserted.");
            }
            
           window.location="index.php?p=11&ln=5&t=6" ;
        }


if ($('#modePhotoGall').val() == '2') {
        var photoData    = {} ;
        
            photoData['mode'] = "updatePhotoGallData";
            photoData['data'] = {};
            photoData['data']['id']           = $("#idPhotoGall").val();
            photoData['data']['urlLink']      = escape(encodeURIComponent(($('#photoField1').val())));
            photoData['data']['order']        = $('#photoField2').val();
            if($('#photoField3').val()== ''){
                 photoData['data']['photoPath']   = $('#photoFEdit3').val(); 
//                 alert($("#photo"))//for imageupload
            }else {
                 photoData['data']['photoPath']   = $('#photoField3').val();  //for imageupload
            }
//            photoData['data']['photoPath']    = $('#photoField3').val();
            
            photoData['data']['studentName']  = $('#photoField11').val();
            photoData['data']['rank']         = $('#photoField12').val();
            
            photoData['data']['active']       = ($('#photoField5').is(':checked')) ? 1 : 0 ;
            photoData['data']['text']         = $('#photoField4').val();
            photoData['data']['text']         = escape(encodeURIComponent(photoData['data']['text']));
            
            
            if (photoData['data']['studentName'] == '') {
                alert("Please provide Student Name");
            }
            else if(photoData['data']['rank'] == ''){
                alert("Please provide Rank");
            }
            else if(photoData['data']['order'] == ''){
                alert("Please provide Order");
            }
            else if(photoData['data']['photoPath'] == ''){
                alert("Please provide Image");
            }else {
                var photoDataRec = get(photoData,'index.php?p=5','json');
                
            }
                var Id        = $("#idPhotoGall").val();
                var file_data = $('#photoField3').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                var url       = "index.php?p=6&m=ajaxPhotoUpload&lastInsertedId=" + Id;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form_data,
                    async: false,
                    processData: false,
                    contentType: false,
                    error: function (res) {
                        alert("Unsuccessful.");
                        return false;
                    },
                    success: function (res) {
                        alert("Photo updated successfully.");
                    }
                });
                
            if( photoDataRec.total> 0){
                alert("Record updated successfully.");
                $('#photoField1').val("");
                $('#photoField2').val("");
                $('#photoField3').val("");
                $('#photoField4').val("");
                this.getDataAll();
                $('#cancelPhoto').click();
            } else {
                alert("No record Updated.");
            }
             window.location="index.php?p=11&ln=5&t=6" ;
        }
            
    }
     $("#photoField3").change(function (){
    var imgWidth = '';
    var imgHeight = '';
    var input = document.getElementById("photoField3");
    var fname = $("#photoField3").val();
    var extension = fname.substr(fname.lastIndexOf(".") + 1);
    if (extension != 'png' && extension != 'jpg' && extension != 'jpeg') {
        alert("Only Upload png,jpeg or jpg type images");
        $("#photoField3").val("");
        $("#photoField3").focus();
        return false;
    }   
    
     
      });

                    $('#cancelToperTalk').click(function(){
                      if ($('#modeTopperTalk').val() == 2) {
                            window.location="index.php?p=11&ln=4&t=6" ;
                        }
                            $('#topperField1').val("");
                            $('#topperField2').val("");
                            $('#topperField3').val("");
                            clearAll();
                     });



        this.editData = function(id){
           var photoGal = {};
                        photoGal['mode'] = "getAllPhotoGallery";
                        photoGal['id'] = id ;
                        $("#idPhotoGall").val(id);
                        var photoDataRec = get(photoGal,'index.php?p=5','json');
                        setTimeout(function() {
                        $("#uniform-photoField3 span.filename").html(unescape(decodeURIComponent(id + '.' + photoDataRec.photoPath)));
                        $('#photoFEdit3').val(id + '.' + photoDataRec.photoPath);
                        }, 1000);
                             $('#editPhotoGall').html('Edit');
                             $('#modePhotoGall').val(2);
                             $('#idPhotoGall').val(id);
                             $('#photoField1').val(unescape(decodeURIComponent(photoDataRec.url)));
                             $('#photoField2').val(unescape(decodeURIComponent(photoDataRec.order)));
                             
                             $('#photoField11').val(unescape(decodeURIComponent(photoDataRec.studentName)));
                             $('#photoField12').val(unescape(decodeURIComponent(photoDataRec.rank)));
                             
                             $('#photoField4').val(unescape(decodeURIComponent(photoDataRec.photoText)));
                            if( photoDataRec.active == '1'){
                                $('#uniform-photoField5 span').attr('class','checked');
                                $('#photoField5').attr('checked','checked');
                            }else{
                                $('#uniform-photoField5 span').attr('class','');
                                $('#photoField5').removeAttr('checked');
                            }
                    }

                         this.deleteData = function(id){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             var photo = {};
                             photo['mode'] = "deletePhotoGall";
                             photo['data'] = {};
                             photo['data']['id'] = id;
                             var photoDataRec = get(photo,'index.php?p=5','json');
                             if(photoDataRec.total>0){
                                alert("Record Deleted successfully.");
                                this.getDataAll();
                             }else{
                                alert("No record deleted.");
                             }
                         }
                }
         }         