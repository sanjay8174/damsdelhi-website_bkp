<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>MRCGP Coaching Institute, MRCGP</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2>MRCGP - Delhi</h2>
          <!--<h3>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS -UK</h3>-->
          <h3> INTERNATIONAL MEMBERSHIP OF ROYAL COLLEGES OF GENERAL PRACTITIONER</h3>
          <h3>MRCGP(International)EXAM COURSE PART II-Delhi</h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box mrcog_crsbox ">
       <ul class="idTabs responce-show">
        <li><a href="#part1">PART II</a></li>
       <li><a href="mrcgp-part2.php">FAQ'S</a></li>
       </ul>
      <div id="part1">
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;">
        <h3><span class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
              

<!--<span class="book-ur-seat-btn"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></span>--></h3>
        
          <div class="world_class" style="margin: 5px 0 15px; font-size:20px;"><strong>WORLD CLASS CREDENTIALS</strong></div>
        <p> DAMS in partnership with Arlington British Medical Academy UK is proud to announce first MRCGP PART 1 and 2 Courses in India for the first time. once again. The course will be delivered by all UK tutors who have been involved in GP training in the UK. This 2 day intensive course will cover all three components of the exam.<br><br>

“We had excellent pass rate after our previous course and one of our candidates scored highest
  marks in MRCGP International Exam in UAE.”<br><br></p>
          </div>
      <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
         <div class="mrcp_boxess" style="border-bottom:none;">
            <div class="mrcp_dr_img"><img src="images/dr_zubir.jpg"></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Zubair Ahmad</h1>
            <p class="mrcp_dr_sbtl">MRCGP, DRCOG, DFFP, FRCGP, PCME</p>
            <p class="mrcp_dr_sbtl">Course Facilitator</p>
            <p style="font-size:14px;"> <b>The Course Director</b> is Dr Zubair Ahmad, a GP Trainer from North West Deanery of England.  He has been teaching and helping doctors prepare for MRCGP examinations and GP exams since 2007. He is a Member of the Royal College of General Practitioners. Dr Ahmad was also awarded Fellowship from the Royal College of General Practitioner in London to recognise his immense contribution to the Primary Care in the UK. </p>
        </div>

</div>

<div class="mrcp_boxess">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/ForidaAbdullah.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Forida Abdullah</h1>
            <p class="mrcp_dr_sbtl">MBChB, MRCGP, DFFP</p>
            
            <p style="font-size:14px;">Dr Forida is our co-director. She has been involved in GP Specialist Training for MRCGP since completing her Basic Trainers Course from the Northwest Deanery of England. She is also involved in the medical education of Foundation Year doctors and Medical Students from Manchester Medical School. She is a member of the Tameside GP Trainers Group and does peer reviews and calibration of skills within this group to maintain quality and standards.
</p>
        </div>
      

</div>      </div>
    </div>
    </div>
  </div>
  
  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Structure</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
      
      <p style="font-size:18px;   font-family: sans-serif; padding-bottom: 14px;" >
Day 1: CCSA Intense Preparation: </p><p class="course_dtlsp">All day course to prepare candidates sitting the MRCGP International OSCE examination. This course offers comprehensive preparation for all parts of the OSCE examination, including communication and consultations skills, the clinical examination stations, and oral / viva stations. The course will cover the key theory or each type of station, with an opportunity for all doctors to practice ALL stations, with detailed individual and group feedback. Consultation practice is carried out with trained simulated patients (just like in the real exam).

<p style="font-size:18px;  font-family: sans-serif;padding-bottom: 14px;">Day 2: MCQ / AKT Preparation:</p>

<p class="course_dtlsp">This 1 day intense course will prepare candidates sitting the MCQ paper of the MRCGP International examinations. The course will cover the key theory and techniques for AKT, as well as allowing candidates to sit mini mock examinations with detailed feedback and discussion.

        </p>
        <div>
            <p style="  font-size: 15px;font-weight: 200; font-family: pt_sansregular;"><strong style="color:red;">Note: </strong>You will receive a course handbook, refreshments and a buffet lunch. You will also receive a certificate of attendance for your portfolio. </p>
        </div>
      </div>
    </div>
  </div>
 </div>
           <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Highlight of Course</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
      
      
<h5>MCQ / AKT</h5>

<ul  class="course-new-list" style="list-style: outside none disc; color: rgb(36, 36, 36); font-family: pt_sansregular; font-size: 14px; line-height: 22px; margin-left: 27px;" >
         <li><span class="sub-arrow"></span>Registration</li>
         <li><span class="sub-arrow"></span>Welcome and introductions </li>
         <li><span class="sub-arrow"></span>MRCGP International MCQ paper - tips and techniques</li>
         <li><span class="sub-arrow"></span>Statistics for the MCQ paper</li>
         <li><span class="sub-arrow"></span>High yield topics for the MCQ paper</li>
         <li><span class="sub-arrow"></span>Break for refreshments</li>
         <li><span class="sub-arrow"></span>Mini Mock MRCGP International MCQ paper </li>
         <li><span class="sub-arrow"></span>Mini Mock MCQ answers and feedback</li>
         <li><span class="sub-arrow"></span>Critical reading questions</li>
         <li><span class="sub-arrow"></span>Approaching Issues questions</li>
         <li><span class="sub-arrow"></span>Questions and answers</li>
         <li><span class="sub-arrow"></span>Summary and feedback</li>
         <li><span class="sub-arrow"></span>Close</li>
       
        </ul>
<h5>OSCE /CSA</h5>

<ul class="course-new-list" style="list-style: outside none disc; color: rgb(36, 36, 36); font-family: pt_sansregular; font-size: 14px; line-height: 22px; margin-left: 27px;" class="usmledge">
         <li><span class="sub-arrow"></span>Registration</li>
         <li><span class="sub-arrow"></span>Welcome and introductions </li>
         <li><span class="sub-arrow"></span>MRCGP International OSCE - what to expect / overview</li>
         <li><span class="sub-arrow"></span>Communication and consultation skills</li>
         <li><span class="sub-arrow"></span>Consultation practice 1 with feedback</li>
         <li><span class="sub-arrow"></span>Consultation practice 2 with feedback</li>
         <li><span class="sub-arrow"></span>Essential examinations and equipment - clinical skills stations </li>
         <li><span class="sub-arrow"></span>Practice clinical skills</li>
         <li><span class="sub-arrow"></span>Oral / Viva stations - what to expect</li>
         <li><span class="sub-arrow"></span>Questions and answers</li>
         <li><span class="sub-arrow"></span>Summary and feedback</li>
         <li><span class="sub-arrow"></span>Close</li>
       
        </ul>
      
      </div>
    </div>
  </div>
 </div>
</div>
      </div>
        
       <div class="course-box mrcog_crsbox ">
      
        
       </div>
      </div>
   </div>   
      
    </aside>    
    <aside class="content-right">
        <div class="content-royal-college res_css">
            <img src="images/logo-royal-college.png" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF OBSTETRICS AND GYNAECOLOGISTS OF THE UNITED KINGDOM</p>
        </div>
        <div id="part1">
        <div class="content-date-venue res_css">
            <h1 style="color:black">Who is this course for?</h1>
            <p style="color:black">
The course promises to be first of its kind in India and provides a golden opportunity to gain the prestigious qualification for General Practitioners and Residents in Family Medicine in Indiapar.</p>
        </div>      
        </div>
          <?php include 'enquiryform.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
