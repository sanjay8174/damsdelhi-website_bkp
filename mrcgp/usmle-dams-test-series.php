<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="test-series">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li class="bg_none"><a href="usml-intro.php" title="USMLE Edge">USMLE Edge</a></li>
          <!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="DAMS Store">DAMS Store</a></li>-->
          <li><a href="http://damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>
          <li><a title="Online Test Series" class="active-link">Online Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading">
            <h4>Dams Test Series</h4>
            <article class="showme-main">
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart right_mar0"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart right_mar0"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart right_mar0"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart right_mar0"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>USMLE Edge DAMS Store</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li style="border:none;"><a href="usmle-idams.php" title="iDAMS Tablet">iDAMS Tablet</a></li>
                  <!--<li><a href="usmle-dams-store-publication.php" title="Dams Publications">Dams Publications</a></li>-->
                  <li><a href="http://damspublications.com/" target="_blank" title="Dams Publications">Dams Publications</a></li>
                  <li><a href="usmle-online-test-series.php" title="Online Test Series">Online Test Series</a></li>
                  <li><a href="../usmle-test-series.php" title="DAMS Test Series" class="active-store">DAMS Test Series</a></li>
                  <li><a href="../usmle-i-dams.php" title="Mobile Applications">Mobile Applications</a></li>
                </ul>
              </div>
            </div>
          </div>
          <?php include 'usmle-buynow-right-section.php'; ?>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>