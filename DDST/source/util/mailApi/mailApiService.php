<?php  

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//These code are must for sending mails from AWS
require_once "aws/aws-autoloader.php";
//include  getcwd()."/config/constant.php";
//include  "../source/util/Constant.php";
use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;

class MailAPIService {

    function __construct() {
        
    }

    public function sendMail($email, $mailTextData, $subjectText, $senderEmail, $attachment = '0', $attachmentArry = null) { 
        /* Important Instruction
         *         Please add These three lines on the top if not added
         *          #require_once "./aws/aws-autoloader.php";
         *          #use Aws\Ses\SesClient;
         *          #use Aws\Ses\Exception\SesException;
         */

        if ($attachment == '1'):
 $txtemail = "DAMS<donotreply@damsdelhi.in>";
//        $mailTextData="hii";
            $fromEmail = (string) "$txtemail";
            $params = array(
                "to" => "$email",
                "subject" => "$subjectText",
                "message" => "$mailTextData",
                "from" => "$fromEmail",
                //OPTIONAL
//                "replyTo" => "heena.valecha@gingerwebs.in",
                //OPTIONAL
                "files" => array(
                    1 => array(
                        "name" => "$attachmentArry[name]",
                        "filepath" => "$attachmentArry[filepath]",
                        "mime" => "application/octet-stream"
                    )
                )
            );
//               print_r($params);exit();
            //$aws = $mailApiService->sendMail_AWS_Attachment($email, $params, $subjectText);
            $aws = $this->sendMail_AWS_Attachment($email, $params, $subjectText);
        else:
           echo $aws = $this->sendMail_AWS($email, $mailTextData, $subjectText,$senderEmail);
        endif;
    }

      public function sendMail_AWS($email, $mailTextData, $subjectText,$senderEmail) { 
          $senderEmail="DAMS<donotreply@damsdelhi.in>";
        $apikey = 'AKIAI4LC2F7K33U5JU7A';
    $secretKey= '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';
    $region = 'us-east-1';
        try {
            $ses = SesClient::factory(array(
                        'key' => "$apikey",
                        'secret' => "$secretKey",
                        'region' => "$region"
            ));
        } catch (Exception $e) {
            echo($e->getMessage());
        }
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('d M,Y');
        $txtemail = (string) "$senderEmail";
        $companyName = (string) "Made Easy";
//       $mailTextData="AAA"; 
        $msg = array();
        $msg['Source'] = "$txtemail";
        $msg[''] = "$companyName";
//ToAddresses must be an array
        $msg['Destination']['ToAddresses'][] = "$email";
        $msg['Message']['Subject']['Data'] = "$subjectText";
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] = "$mailTextData";
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";
//        print_r($msg);
        try {
            $result = $ses->sendEmail($msg);

            //save the MessageId which can be used to track the request
            $msg_id = $result->get('MessageId');
            return $msg_id;
        } catch (Exception $e) {
//            //An error happened and the email did not get sent 
             echo $e->getMessage();
        }
    }

    
     function sendConfirmationSMS($candiadateName,$email,$password,$mobile){
        $getName = explode(" ",$candiadateName);
        $candiadateName = $getName[0];
        $msg='Dear '.$candiadateName.', Thank you very much for REGISTRATION.'
                . 'LOGIN-ID - '. $email.
                ',PASSWORD-'.$password.'';
        $msg = urlencode($msg);
        $url = "https://www.smsjust.com/blank/sms/user/urlsms.php?username=madeeasy&pass=112233&senderid=MADESY&dest_mobileno=$mobile&
message=$msg&response=Y";
//        $url = "https://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=$mobile&msg=$msg&msg_type=TEXT&userid=2000141205&auth_scheme=plain&password=AQlCWftbK&v=1.1&format=text";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
    }
    

    public function sendMail_PHP($email, $mailTextData, $subjectText) {
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $txtemail = (string) Constant::$MANDRILL_PHP_MAIL_ID;
        $companyName = (string) Constant::$COMPANY_NAME;
        $date = date('d M,Y');

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers.="From:.$txtemail.\r\n";

        $headers1 = 'MIME-Version: 1.0' . "\r\n";
        $headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers1.='From:' . $companyName . "\r\n";
        $success = mail("$email", "$subjectText", $mailTextData, $headers1, '-f' . $email);
    }

    const version = "1.0";
    const AWS_KEY = "YOUR-KEY";
    const AWS_SEC = "YOUR-SECRET";
    const AWS_REGION = "us-east-1";
    const MAX_ATTACHMENT_NAME_LEN = 60;

    /**
     * Usage:
      $params = array(
      "to" => "email1@gmail.com",
      "subject" => "Some subject",
      "message" => "<strong>Some email body</strong>",
      "from" => "sender@verifiedbyaws",
      //OPTIONAL
      "replyTo" => "reply_to@gmail.com",
      //OPTIONAL
      "files" => array(
      1 => array(
      "name" => "filename1",
      "filepath" => "/path/to/file1.txt",
      "mime" => "application/octet-stream"
      ),
      2 => array(
      "name" => "filename2",
      "filepath" => "/path/to/file2.txt",
      "mime" => "application/octet-stream"
      ),
      )
      );

      $res = SESUtils::sendMail($params);

     * NOTE: When sending a single file, omit the key (ie. the '1 =>')
     * or use 0 => array(...) - otherwise the file will come out garbled
     * ie. use:
     *    "files" => array(
     *        0 => array( "name" => "filename", "filepath" => "path/to/file.txt",
     *          "mime" => "application/octet-stream")
     *
     * For the 'to' parameter, you can send multiple recipiants with an array
     *    "to" => array("email1@gmail.com", "other@msn.com")
     * use $res->success to check if it was successful
     * use $res->message_id to check later with Amazon for further processing
     * use $res->result_text to look for error text if the task was not successful
     *
     * @param array $params - array of parameters for the email
     * @return \ResultHelper
     */
    public static function sendMail_AWS_Attachment($email, $params, $subjectText) {
//        print_r($params);ECHO $email;exit;
//        exit();

        $to = self::getParam($params, 'to', true);
        $subject = self::getParam($params, 'subject', true);
        $body = self::getParam($params, 'message', true);
        $from = self::getParam($params, 'from', true);
        $replyTo = self::getParam($params, 'replyTo');
        $files = self::getParam($params, 'files');

        $res = new ResultHelper();
//        $apikey = "AKIAJBQMQAYMRBXPCMUQ";
//        $secretKey = "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";
//        $region = "us-west-2";
//        $apikey = "AKIAI4LC2F7K33U5JU7A";
//        $secretKey = "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";
//        $region = "us-east-1";
            $apikey = 'AKIAI4LC2F7K33U5JU7A';
    $secretKey= '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';
    $region = 'us-east-1';

        // get the client ready
        $client = SesClient::factory(array(
                    'key' => "$apikey",
                    'secret' => "$secretKey",
                    'region' => "$region"
        ));

        // build the message
        if (is_array($to)) {
            $to_str = rtrim(implode(',', $to), ',');
        } else {
            $to_str = $to;
        }

        $msg = "To: $to_str\n";
        $msg .= "From: $from\n";

        if ($replyTo) {
            $msg .= "Reply-To: $replyTo\n";
        }

        // in case you have funny characters in the subject
        $subject = mb_encode_mimeheader($subject, 'UTF-8');
        $msg .= "Subject: $subject\n";
        $msg .= "MIME-Version: 1.0\n";
        $msg .= "Content-Type: multipart/mixed;\n";
        $boundary = uniqid("_Part_" . time(), true); //random unique string
        $boundary2 = uniqid("_Part2_" . time(), true); //random unique string
        $msg .= " boundary=\"$boundary\"\n";
        $msg .= "\n";

        // now the actual body
        $msg .= "--$boundary\n";

        //since we are sending text and html emails with multiple attachments
        //we must use a combination of mixed and alternative boundaries
        //hence the use of boundary and boundary2
        $msg .= "Content-Type: multipart/alternative;\n";
        $msg .= " boundary=\"$boundary2\"\n";
        $msg .= "\n";
        $msg .= "--$boundary2\n";

        // first, the plain text
        $msg .= "Content-Type: text/plain; charset=utf-8\n";
        $msg .= "Content-Transfer-Encoding: 7bit\n";
        $msg .= "\n";
        $msg .= strip_tags($body); //remove any HTML tags
        $msg .= "\n";

        // now, the html text
        $msg .= "--$boundary2\n";
        $msg .= "Content-Type: text/html; charset=utf-8\n";
        $msg .= "Content-Transfer-Encoding: 7bit\n";
        $msg .= "\n";
        $msg .= $body;
        $msg .= "\n";
        $msg .= "--$boundary2--\n";

        // add attachments
        if (is_array($files)) {
            $count = count($files);
            foreach ($files as $file) {
                $msg .= "\n";
                $msg .= "--$boundary\n";
                $msg .= "Content-Transfer-Encoding: base64\n";
                $clean_filename = self::clean_filename($file["name"], self::MAX_ATTACHMENT_NAME_LEN);
                $msg .= "Content-Type: {$file['mime']}; name=$clean_filename;\n";
                $msg .= "Content-Disposition: attachment; filename=$clean_filename;\n";
                $msg .= "\n";
                $msg .= base64_encode(file_get_contents($file['filepath']));
//                $msg .= $file['filepath'];
                $msg .= "\n--$boundary";
            }
            // close email
            $msg .= "--\n";
        }
//        echo $msg;exit();
        // now send the email out
        $mailSent = 0;
        try {
            $ses_result = $client->sendRawEmail(
                    array(
                'RawMessage' => array(
                    'Data' => base64_encode($msg)
                )
                    ), array(
                'Source' => $from,
                'Destinations' => $to_str
                    )
            );
            if ($ses_result) {
                $mailSent = 1;
                $res->message_id = $ses_result->get('MessageId');
            } else {
                $mailSent = 0;
                $res->success = false;
                $res->result_text = "Amazon SES did not return a MessageId";
            }
        } catch (Exception $e) {
            $mailSent = 0;
            $res->success = false;
            $res->result_text = $e->getMessage() .
                    " - To: $to_str, Sender: $from, Subject: $subject";
        }
        return $mailSent;
    }

    private static function getParam($params, $param, $required = false) {
        $value = isset($params[$param]) ? $params[$param] : null;
        if ($required && empty($value)) {
            throw new Exception('"' . $param . '" parameter is required.');
        } else {
            return $value;
        }
    }

    /**
      Clean filename function - to get a file friendly
     * */
    public static function clean_filename($str, $limit = 0, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\.\/_| -]/", '', $clean);
        $clean = preg_replace("/[\/| -]+/", '-', $clean);

        if ($limit > 0) {
            //don't truncate file extension
            $arr = explode(".", $clean);
            $size = count($arr);
            $base = "";
            $ext = "";
            if ($size > 0) {
                for ($i = 0; $i < $size; $i++) {
                    if ($i < $size - 1) { //if it's not the last item, add to $bn
                        $base .= $arr[$i];
                        //if next one isn't last, add a dot
                        if ($i < $size - 2)
                            $base .= ".";
                    } else {
                        if ($i > 0)
                            $ext = ".";
                        $ext .= $arr[$i];
                    }
                }
            }
            $bn_size = mb_strlen($base);
            $ex_size = mb_strlen($ext);
            $bn_new = mb_substr($base, 0, $limit - $ex_size);
            // doing again in case extension is long
            $clean = mb_substr($bn_new . $ext, 0, $limit);
        }
        return $clean;
    }

}

class ResultHelper {

    public $success = true;
    public $result_text = "";
    public $message_id = "";

}
