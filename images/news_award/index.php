﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta  name="description" content="Ginger Webs provide innovative examination process and assessment solutions to Educational Institutions, Government/ PSU sector and leading corporate entities." />

<link rel="stylesheet" type="text/css" href="style.css" />
<title>Online Exam Software & OMR Software by Ginger Webs</title>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="menu.css" />
<link  type="text/css" href="" />
<link href="font/stylesheet.css" rel="stylesheet" media="all" type="text/css" />
<script src="js/jquery.min.js"></script>
<link href="css/jquery.tweet.css" rel="stylesheet" media="all" type="text/css" />
<script src="js/jquery.tweet.js" charset="utf-8"></script>
<script type="text/javascript" src="js/project.js"></script>


<script type="text/javascript">
    function randomString(length) {
      var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
      var str = '';
      for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
      }
      return str;
    }
    var rnd = randomString(8);

    jQuery(function($) {
      $(".rnd").replaceWith(rnd);


      $(".demo .code").hide().each(function(i,e){
        $(e).before($('<a class="show-code" href="#">&nbsp;</a>').
                      click(function(ev) {
                        $(e).slideToggle();
                        $(this).hide();
                        ev.preventDefault();
                      }));

      }).each(function(i, e){ eval($(e).text()); });
    });
  </script>



<script type="text/javascript">

function layer ()


{

var top=document.getElementById("drop");



if(top.style.display=="none"){
		top.style.display="block"
		$("#spantop3").show();
		document.getElementById('imageId').src='images/ar.png';

	}else{
		top.style.display="block"}
	}

function layer1(){

	document.getElementById('product-drop').className='product';
	 $("#drop").show();
	 $("#spantop3").show();
	  document.getElementById('imageId').src='images/ar.png';
		// $("#spantop1").attr("style","display:block;");
document.getElementById("spantop1").setAttribute("style","display:block")


	}

function layer3(){
	 $("#drop").hide();

	 document.getElementById('product-drop').className='';
	  $("#spantop3").hide();
 //$("#spantop1").attr("style","display:block;");
  document.getElementById("imageId").removeAttribute("src");
  document.getElementById('imageId').src='';
	}



</script>



   
    <!--starts banner--->


</head>

<body onunload="navigation1('nav1');timeFun2()">


<!--main back start--->

<div id="main-back-image-home">

<!--menu-cover start--->


<div id="main">


<!--menu-cover start--->


<div id="menu-cover">

<div id="logo"><a href="index.php"><img src="images/logo.png" width="179" height="65" /></a></div>


<!--new-main start--->
<?php include("include/header.php"); ?>
<!--new-main close--->


<!--clear both srart--->

<div class="clearboth"></div>

<!--clear both close--->

</div>

<!-- menu-cover close--->




<!--eliment start--->

<div id="eliment">

<!--white pannel start--->


<div id="white-pannel" class="border">

<!--starts content-->

<div id="banner-div">


<div class="main_view">
<div class="window">
<div class="image_reel"  >
<a ><img src="images/slider_1.jpg"  /></a>
<a ><img src="images/slider_2.jpg"  /></a>
<a ><img src="images/slider_3.jpg"  /></a>

</div>
</div>
<div class="paging-back">&nbsp;</div>
<div class="paging" >
<a href="#" rel="1" class="">1</a>
<a href="#" rel="2"  >2</a>
<a href="#" rel="3" class="">3</a>
</div>




</div>



</div>
<!--end banner--->

<!--sub-content start--->

<div id="sub-content">

<div id="raw">

  <h1>Know what we do</h1></div>

  <hr class="cls" />









<div id="matter-content">
<p><img src="images/office.jpg" class="img" />Ginger Webs Pvt. Ltd. is a software development company, driven by an ideology to make unique, easy to use and futuristic applications.  Our extensive research and development in education industry helps us to make products that allow end users to achieve maximum with minimum effort.  Complicated processes like examination, recruitment, and corporate training made simple by using our products.  Apart from education, we also leverage technology and management to varied verticals of the industry.</p>
<strong><a href="about-us.php">read more</a> <img src="images/arrow.jpg" /></strong><br /><br />
</div>

<!--end sub-content--->

<div id="raw">

 <h1>Some of our happy clients <span class="color"> :)</span></h1></div><hr class="cls" />

 </div>


<!--sub-content close--->


 <!--row start--->

<div id="row">
<ul>
<li class="marg1"><img src="images/world_logo.jpg"/></li>
<li class="marg1"><img src="images/career_logo.jpg"  /></li>
<li class="marg1"><img src="images/shrda.jpg"  /></li>
<li class="marg1"><img src="images/amity.jpg" /></li>
<li class="marg1"><img src="images/overly.jpg" /></li>
<li class="marg1"><img src="images/vedanta.jpg" /></li>
<li class="marg1"><img src="images/jet.jpg" /></li>
<li class="marg1"><img src="images/central.jpg" /></li>
<li><img src="images/institute.jpg" /></li>
<li class="marg2"><img src="images/reserve.jpg" /></li>
<li class="marg3"><img src="images/punjab.jpg" /></li>
<li class="marg4"><img src="images/helix.jpg" /></li>
<li class="marg5"><img src="images/peace.jpg" /></li>
<li class="marg6"><img src="images/news.jpg" /></li>

<div class="clearboth"></div>
</ul>
</div>

 <!--row close--->


<div id="heading4">
  <h1>Be a part of our Team</h1></div>


<!--line start--->

<div class="line">
&nbsp;
</div>

<!--line close--->


<!--team start--->


<div id="team"><p><img src="images/job.png" align="right" />If you’re looking to join a company where your efforts are appreciated, where you can work in all dimensions and achieve goals through team work; then you’ve come to the right place.<br />
<br />


So, if you think you have what it takes to make a difference to our business and if you are looking for an opportunity to work in a dynamic environment, where success is only limited by your ability, then take a look at the opportunities we have to offer and apply now!<br />
<br />
We are committed to making fast, reliable and user friendly applications.

</p></div>

<!--team close--->




 <!--contact start--->


    <div id="second-tag-outer"><!--start second-tag-outer--->



<div id="second-tag-mainbox"><!--start second-tag-mainbox--->

<div id="second-tag-leftbox"><!--start second-tag-leftbox---><img src="images/mobile.gif" /></div><!--end second-tag-leftbox--->

<div id="second-tag-right"><!--start second-tag-right--->
Call us at  <span>&nbsp;+91 11 27292555</span>

</div><!--end second-tag-right--->

</div><!--end second-tag-mainbox--->




<div id="second-tag-mainbox"><!--start second-tag-mainbox--->

<div id="second-tag-leftbox"><!--start second-tag-leftbox---><img src="images/pointer.jpg" /></div><!--end second-tag-leftbox--->

<div id="second-tag-right" class="second-tag-link1"><!--start second-tag-right--->
Fill out Ginger&nbsp; <a href="career.php">career form</a>

</div><!--end second-tag-right--->

</div><!--end second-tag-mainbox--->



<div id="second-tag-mainbox1"><!--start second-tag-mainbox--->

<div id="second-tag-leftbox"><!--start second-tag-leftbox---><img src="images/inbox.jpg" /></div><!--end second-tag-leftbox--->

<div id="second-tag-right" class="second-tag-link1"><!--start second-tag-right--->
Email us at &nbsp;
<a href="mailto:career@gingerwebs.com">career@gingerwebs.com</A>

</div><!--end second-tag-right--->

</div><!--end second-tag-mainbox--->




    </div>



    <!--contact close--->






</div>


<!--white panel close--->




<!--yallow start--->



<div id="third-container" class="border" ><!--start third-container--->

<div id="raw">
  <h3>News & our Social Activities</h3></div><br />

<div class="actives"><!--start actives--->



<div class="actives-main"><!--start actives-main--->

<h4>WHAT'S HAPPENING</h4>
<h1>10th Aug 2012</h1>
<p>We welcome everybody to our new website. Find out latest information about our products and services. </p>


<h1>30th July 2012</h1>
<p>More then 2500 students of DAMS Delhi gave online exam in two days using think exam platform.
 </p>





</div><!--end list--->




<div class="actives-main1"><!--start actives-main--->

  <div class="right-plug"></div>


<div id="raw">
  <h4>LATEST GINGER TWEET</h4>


  </div>

<div class="line2"> </div><!--line2--->

<div id="site-link">
<p class="second-tag-link" style="text-align:left">


   <div class="demo">
  
    <pre  class="code">
      jQuery(function($){
     
        $(".tweet").tweet({
          join_text: "auto",
          username: "gingerwebs",

          count: 1,
          auto_join_text_default: "we said,",
          auto_join_text_ed: "we",
          auto_join_text_ing: "we were",
          auto_join_text_reply: "we replied",
          auto_join_text_url: "we were checking out",
          loading_text: "loading tweets..."
        });
      });
      
    </pre>
    <div class='tweet'></div>
  </div>



</p>

</div>
</div><!--end actives-main--->



<div class="facebook-coloumn"><!--start actives-main--->

<div id="raw"><h4>GET SOCIAL WITH US!</h4></div>

<div class="line2"> </div><!--line2--->

<div id="third-tag-mainbox"><!--start third-tag-mainbox--->


<ul><a href="https://en-gb.facebook.com/pages/Ginger-Webs/327728930620164" target="_blank">Find us on facebook  <img src="images/fb.gif"  align="right"/></a></ul>

<ul><a href="https://twitter.com/Ginger_Webs" target="_blank" >Follow us on twitter <img src="images/tweet.gif" align="right" /></a></ul>


<ul><a href="https://in.linkedin.com/company/ginger-webs" target="_blank">View our linked in Profile <img src="images/in.gif" align="right" /></a></ul>




<!--end  third-tag-leftbox--->




</div><!--end third-tag-mainbox--->



</div><!--end actives--->






<!--yallow close--->




</div>

</div>
</div>


<!--eliment close--->


</div>
<!--menu-cover start--->




<!--footer start--->

<?php include("include/footer.php"); ?>

<!--footer close--->


</div>

<!--main back close--->

<!--menu-cover start--->

<!--Organization Rich Snippet-->
<!--<div itemscope itemtype="https://schema.org/Organization"> 
   <span itemprop="name">Ginger Webs Pvt Ltd</span> 
   Located at 
   <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
      <span itemprop="streetAddress">Lower Ground Floor, Plot No.2, Multitech Tower, Aggarwal Plaza</span>,
      <span itemprop="addressLocality">Sector-16, Rohini</span>,
      <span itemprop="addressRegion">New Delhi, India</span>.
   </div>
 <div itemprop="description" itemscope itemtype="https://schema.org/Text">
   <span itemprop="Text">Ginger Webs is a Software Company that makes Examination, Recruitment and Corporate Training simpler for Organizations</span>
   </div>

  <img itemprop="logo" src="https://gingerwebs.com/images/logo.png" />
  Phone: <span itemprop="telephone">+91 (11) 27292555</span>
   <a href="https://gingerwebs.com/" itemprop="url">https://gingerwebs.com</a>
</div>
-->
</body>

</html>
