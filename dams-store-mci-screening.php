<?php 
header('Location: https://www.damspublications.com/');exit;?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var fid=$('.apply-now > a').size();
//     Career Form
    for(a=1;a<=fid;a++)
	{
       $('#student-career'+a).click(function(e) {
		  $('#backPopup').show();
		 $('#careerform').show();       
    });
	}
	$('#student-career-close').click(function() {
		$('#backPopup').hide();
		$('#careerform').hide();
    });
		
//     Registration Form
    $('#student-registration').click(function(e) {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });
	
//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <div class="banner-right-btns"> 
         <!--<a href="dams-store.php" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a>--> 
         <a href="https://www.damspublications.com/" target="_blank" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a> 
            <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> 
         <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> 
        </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li style="background:none;"><a href="mci-screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="DAMS Store" class="active-link">DAMS Store</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="center-left">
          <div class="inner-center">
            <h4>DAMS Store</h4>
            <article class="showme-main">
              <div class="main-store-box">
                <ul>
                  <li>
                    <aside class="main-dams-box">
                      <div class="dams-box">
                        <div class="store-box"> <img src="images/store1.png" alt="iDAMS" title="iDAMS" />
                          <div class="store-content-box">
                            <h6>iDAMS</h6>
                            <ul>
                              <li>Tablet Based Learning Programme </li>
                              <li>Technology Unleashed</li>
                              <li>(Tablet Based Learning)</li>
                            </ul>
                            <div class="buy-now-btn"> <a href="idams.php" title="Buy Now">Buy&nbsp;Now &nbsp; <span>&nbsp;</span></a> </div>
                          </div>
                        </div>
                      </div>
                    </aside>
                    <aside class="main-dams-box">
                      <div class="dams-box">
                        <div class="store-box"> <img src="images/store4.png" alt="Dams Publications" title="Dams Publications" />
                          <div class="store-content-box">
                            <h6>MCI Screening DAMS Publications</h6>
                            <ul>
                              <li>Tablet Based Learning Programme </li>
                              <li>Technology Unleashed</li>
                              <li>(Tablet Based Learning)</li>
                            </ul>
                            <div class="buy-now-btn"> 
                             <a href="dams-store-publication.php" title="View All Books">View&nbsp;All&nbsp;Books &nbsp; <span>&nbsp;</span></a> 
                            </div>
                          </div>
                        </div>
                      </div>
                    </aside>
                    <aside class="main-dams-box">
                      <div class="dams-box">
                        <div class="store-box"> <img src="images/store3.png" alt="ONline Test series" title="ONline Test series" />
                          <div class="store-content-box">
                            <h6>MCI Screening Online Test series</h6>
                            <ul>
                              <li>Tablet Based Learning Programme </li>
                              <li>Technology Unleashed</li>
                              <li>(Tablet Based Learning)</li>
                            </ul>
                            <div class="buy-now-btn"> <a href="online-test-series.php" title="View All">View&nbsp;All &nbsp; <span>&nbsp;</span></a> </div>
                          </div>
                        </div>
                      </div>
                    </aside>
                  </li>
                  <li>
                    <aside class="main-dams-box">
                      <div class="dams-box">
                        <div class="store-box"> <img src="images/store2.png" alt="DAMS Test series" title="DAMS Test series" />
                          <div class="store-content-box">
                            <h6>MCI Screening DAMS Test series</h6>
                            <ul>
                              <li>Tablet Based Learning Programme </li>
                              <li>Technology Unleashed</li>
                              <li>(Tablet Based Learning)</li>
                            </ul>
                            <div class="buy-now-btn"> <a href="dams-test-series.php" title="View All">View&nbsp;All &nbsp; <span>&nbsp;</span></a> </div>
                          </div>
                        </div>
                      </div>
                    </aside>
                    <aside class="main-dams-box">
                      <div class="dams-box">
                        <div class="store-box"> <img src="images/store5.png" alt="DAMS Mobile applications" title="DAMS Mobile applications" />
                          <div class="store-content-box">
                            <h6>DAMS Mobile applications</h6>
                            <ul>
                              <li>Tablet Based Learning Programme </li>
                              <li>Technology Unleashed</li>
                              <li>(Tablet Based Learning)</li>
                            </ul>
                            <div class="buy-now-btn"> <a href="i-dams.php" title="View All">View&nbsp;All &nbsp; <span>&nbsp;</span></a> </div>
                          </div>
                        </div>
                      </div>
                    </aside>
                  </li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here --> 
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>