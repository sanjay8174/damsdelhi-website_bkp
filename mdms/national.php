<!DOCTYPE html>
<?php
$course_id = '1';
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, National Quiz</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>National Quiz</h3>
        <div class="franchisee-box paddin-zero">
          <p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER – DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country.</p>
          <p>This mega event kicked off recently, with two invites already sent to your respective colleges. You must have been informed of the same by now. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>It is your chance to participate and win fabulous prizes.</p>
        </div>
      </div>
      <div class="pg-medical-main tab-hide">
        <div class="pg-heading"><span></span>National Quiz Courses</div>
        <div class="course-new-section">
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Classroom Course</h5>
            <div class="coures-list-box-content"  id="di1" style="display:none;">
              <ul class="course-new-list">
                <li><a href="gray-matter.php" title="Concept of Grey Matter"><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
                <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>2012</h5>
            <div class="coures-list-box-content" id="di2" style="display:none;">
              <ul class="course-new-list">
                <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
                <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
                <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('3');"><span class="plus-ico" id="s3"></span>2013</h5>
            <div class="coures-list-box-content" id="di3" style="display:none;">
              <ul class="course-new-list">
                <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('4');"><span class="plus-ico" id="s4"></span>2014</h5>
            <div class="coures-list-box-content" id="di4" style="display:none;">
              <ul class="course-new-list">
                <li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>
<!--                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>-->
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="recent-pg-main">
        <div class="span12">
          <div class="recent-pg-heading1"> <a href="../photo-gallery.php" class="photogal photogal_inline"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
          <div class="customNavigation"> <a class="btn prev" href="javascript:void(0);"><img src="../images/left.png" alt="Left" /></a> <a class="btn next" href="javascript:void(0);"><img src="../images/right-inactive.png" alt="Right" /></a> </div>
        </div>
        <div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
          <div class="item"> <img src="../images/topper/1.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIPGE (2014)</span> </div>
          <div class="item"> <img src="../images/topper/2.jpg" alt="Toppers" /> <span class="_name">Dr. Mayank Agarwal</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">ASSAM PG (2014)</span> </div>
          <div class="item"> <img src="../images/topper/3.jpg" alt="Toppers" /> <span class="_name">Dr. Archana Badala</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">RAJASTHAN CET (2014)</span> </div>
          <div class="item"> <img src="../images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">DELHI PG (2014)</span> </div>
          <div class="item"> <img src="../images/topper/1-.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">MAHCET (2013-14)</span> </div>
          <div class="item"> <img src="../images/topper/5.jpg" alt="Toppers" /> <span class="_name">Dr. Tuhina Goel</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIIMS</span> </div>
          <div class="item"> <img src="../images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>2</strong></span> <span class="_field">DNB CET (2013)</span> </div>
          <div class="item"> <img src="../images/topper/6.jpg" alt="Toppers" /> <span class="_name">Dr. Pooja jain</span> <span class="_rank"><strong>Rank 5</strong></span> <span class="_field">DNB CET (2013)</span> </div>
        </div>
        <div class="view-all-photo"><a href="aipge_2014.php" title="View All">View&nbsp;All</a></div>
      </div>
    </aside>
    <aside class="content-right">
      <?php include 'national-quiz-accordion.php'; ?>
      <?php
include '../openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div class="news_content_inline">
            <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if ($newsDetail[$j] != '') { ?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if ($newsDetail[$j] != '') { ?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php
                        }
                        ?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                <li id="u<?php echo $i; ?>" <?php if ($i == '0') { ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
                <?php } ?>
                <!--                    <li id="u1" class="" onClick="news('1');"></li>
                                                        <li id="u2" class="" onClick="news('2');"></li>-->
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>        
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="../photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="../js/html5.js"></script>
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/registration.js"></script>
<script type="text/javascript" src="../js/add-cart.js"></script>
<script type="text/javascript" src="../gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>