<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Regular Course" class="active-link">Regular Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Regular Course <span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <aside class="course-icons">
                <div class="icons-1 icons-adjust margn-zero"><span class="monitor"></span>Weekend<span>Classes</span></div>
                <div class="icons-2 icons-adjust"><span class="monitor"></span>Topic&nbsp;Wise <span>Tests</span></div>
                <div class="icons-3 icons-adjust"><span class="monitor"></span>Grand&nbsp;Tests<span>AIPG&nbsp;(NBE/NEET)</span></div>
                <div class="icons-4 icons-adjust"><span class="monitor"></span>Dedicated<span>Study&nbsp;Material</span></div>
                <div class="icons-5 icons-adjust"><span class="monitor"></span>Revision<span>SWT</span></div>
              </aside>
              <aside class="course-detail">
                <p>Most popular PG course amongst students with comprehensive coverage of ALL SUBJECTS. We are the only medical coaching institute which teaches all subjects in manner that is required for your PG Medical Entrance examination. </p>
                <p>&nbsp;</p>
                <p>This course starts in Feburary and we have a fresh batch every month. Classes are held on weekends and are based on advanced audio-visual learning modules. This course further has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance Examination. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB &amp;  MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort..</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Course Highlights</div>
                <div class="course-detail-content"> <span class="gry-course-box">We admit only limited number of students in each batch. You will receive excellent individual attention. This is a big advantage to you as you can learn better in a small class with limited students. Our class rooms are air conditioned and have state of the art audiovisual facilities. </span> <span class="blue-course-box">You will be taught by You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can.  They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books and often such questions are asked in advanced examinations like AIIMS, AIPG(NBE/NEET) Pattern PG, and PGI Chandigarh...</span> <span class="gry-course-box">We provide detailed and easy to understand notes. Our notes are based on standard text books like Ganong, Williams, Harrison, and Robns pathology and are supplemented by question banks.  These are as good as your text books in terms of authenticity and are extremely concise and easy to read. They are like concentrated protein mix required for your PG Medical Entrance Examination.</span> <span class="blue-course-box">We conduct periodic tests, evaluate your performance and conduct counselling sessions. This will help you in your regular preparation for your exams.  In this course we provide practice with our monthly All India Grand Tests and Bounce Back Series.  This course has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance examination.</span> </div>
                <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <div class="national-quiz-add"> <a href="national.php" title="National Quiz"><img src="../images/national-quiz.jpg" alt="National Quiz" /></a> </div>
          <!--for Enquiry -->
          <?php include '../enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="../js/html5.js"></script>
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/registration.js"></script>
<script type="text/javascript" src="../js/add-cart.js"></script>
</body>
</html>