<div id="store-wrapper">
  <div class="dams-store-link" style="margin:0px;"><span></span>National Quiz</div>
  <div class="dams-store-content">
    <div class="inner-store">
      <ul>
        <li class="border_none"><a href="../gray-matter.php" title="Concept of Grey Matter" class="active-store">Concept of Grey Matter</a></li>
        <li><a href="../gray-matter-testimonial.php" title="Testimonials">Testimonials</a></li>
        <li><a href="../round1.php" title="1st Campus Round 2012">1st Campus Round 2012</a></li>
        <li><a href="../round2.php" title="2nd Campus Round 2012">2nd Campus Round 2012</a></li>
        <li><a href="../grand.php" title="Grand Finale 2012">Grand Finale 2012</a></li>
        <li><a href="../season2round1.php" title="1st Campus Round 2013">1st Campus Round 2013</a></li>
        <li><a href="../season2round2.php" title="2nd Campus Round 2013">2nd Campus Round 2013</a></li>
        <li><a href="../grand2013.php" title="Grand Finale 2013">Grand Finale 2013</a></li>
      </ul>
    </div>
  </div>
</div>
