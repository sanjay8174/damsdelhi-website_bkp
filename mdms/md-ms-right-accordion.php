<script>
function sliddes1(val)
{
   var sp1=$('div.accordionButton > span').size();
   for(var d=1;d<=sp1;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sq'+d).removeClass('arrowup');
         $('#sq'+d).addClass('arrowdwn');
		 $('#dip'+d).slideUp(400);
	   }      
   }
   
   $('#dip'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sq'+val).removeClass('arrowdwn');
            $('#sq'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sq'+val).removeClass('arrowup');
            $('#sq'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddes1('1')" style="margin-top:0px;"><span class="arrowdwn" id="sq1"></span>Interns/Post Intern Students</div>
  <div class="accordionContent" id="dip1" style="display: none;">
    <div class="inner-accor">
      <ul>
        <li>Classroom Courses</li>
        <ol>
          <li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course</a></li>
          <li><a href="t&amp;d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>		  
          <li><a href="crash_course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
        </ol>
        <li>Distant Learning Programme</li>
        <ol>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
          <li><a href="shortcut-to-nimhans.php" title="Shortcut To Nimhans"><span class="sub-arrow"></span> Shortcut To Nimhans</a></li>
          <li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
        </ol>
        <li>Test Series</li>
        <ol>
          <li><a href="online-test-series.php" title="Online"><span class="sub-arrow"></span> Online</a></li>
          <li><a href="offline-test-series.php" title="Offline"><span class="sub-arrow"></span> Offline</a></li>
          <li><a href="postal-test-series.php" title="Postal"><span class="sub-arrow"></span>Postal</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes1('2')"><span class="arrowdwn" id="sq2"></span>Prefinal/Final Year Students</div>
  <div class="accordionContent" id="dip2" style="display: none;">
    <div class="inner-accor">
      <ul>
        <li>Classroom Courses</li>
        <ol>
          <li><a href="foundation_course.php" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
          <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
          <li><a href="../dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes1('3')" ><span class="arrowdwn" id="sq3"></span>2nd Professional Students</div>
  <div class="accordionContent" id="dip3" style="display: none;">
    <div class="inner-accor">
      <ul>
        <li>Classroom Courses</li>
        <ol>
          <li><a href="prefoundation_course.php" title="Prefoundation Course"><span class="sub-arrow"></span> Prefoundation Course</a></li>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
          <li><a href="../dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes1('4')"><span class="arrowdwn" id="sq4"></span>1st Professional Students</div>
  <div class="accordionContent" id="dip4" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="the_first_step.php" title="First Step course"><span class="sub-arrow"></span> First Step Course</a></li>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
          <li><a href="../dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
