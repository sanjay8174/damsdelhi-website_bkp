<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li class="boder-none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');" class="light-blue-inner"><span id="accorsp3" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_block" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_none" id="aol4"> 
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>
</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>AIPG Result 2012</h4>
<section class="showme-main">
<ul class="idTabs idTabs1"> 
<li><a href="#jquery" class="selected" title="AIPG Result 2012">AIPG Result 2012</a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">
<ul class="main-students-list">
<li class="tp-border">
<div class="students-box border_left_none">
<img src="../images/students/ANKUR-JINDAL.jpg" alt="Dr. ANKUR JINDAL" title="Dr. ANKUR JINDAL" />
<p><span>Dr. ANKUR JINDAL</span> Rank: 2</p>
</div>

<div class="students-box">
<img src="../images/students/KAPLESH-CHOUDHARY.jpg" alt="Dr. KAPLESH CHOUDHARY" title="Dr. KAPLESH CHOUDHARY" />
<p><span>Dr. KAPLESH CHOUDHARY</span> Rank: 8</p>
</div>

<div class="students-box">
<img src="../images/students/DANNY-KUMAR.jpg" alt="Dr. DANNY KUMAR" title="Dr. DANNY KUMAR" />
<p><span>Dr. DANNY KUMAR</span> Rank: 18</p>
</div>

<div class="students-box">
<img src="../images/students/RAJDEEP-MEENA.jpg" alt="Dr. RAJDEEP MEENA" title="Dr. RAJDEEP MEENA" />
<p><span>Dr. RAJDEEP MEENA</span> Rank: 36</p>
</div>
</li>
</ul>
<br style="clear:both;" />
<br style="clear:both;" />
<div class="schedule-mini-series">

<span class="mini-heading">AIPG - Result 2011</span>

<div class="schedule-mini-top">
<span class="one-part">Rank</span>
<span class="two-part">Student Name</span>
<span class="three-part">&nbsp;</span>
</div>
<div class="schedule-mini-content">
<ul>
<li class="boder-none"><span class="one-parts">2</span>
<span class="two-parts schedule-left-line">Dr. ANKUR JINDAL , Dr. ANKUR KUMAR</span>
</li>
<li><span class="one-parts">8</span>
<span class="two-parts schedule-left-line">Dr. KAPLESH CHOUDHARY</span>
</li>
<li><span class="one-parts">10</span>
<span class="two-parts schedule-left-line">Dr. SUREKHA MEENA</span>
</li>
<li><span class="one-parts">11</span>
<span class="two-parts schedule-left-line">Dr. ARUN KUMAR</span>
</li>
<li><span class="one-parts">12</span>
<span class="two-parts schedule-left-line">Dr. ANITHA K</span>
</li>
<li><span class="one-parts">13</span>
<span class="two-parts schedule-left-line">Dr. NANI DOLO</span>
</li>
<li><span class="one-parts">15</span>
<span class="two-parts schedule-left-line">Dr. NEHA SHEERA</span>
</li>
<li><span class="one-parts">17</span>
<span class="two-parts schedule-left-line">Dr. AVISHEK BAGCHI</span>
</li>
<li><span class="one-parts">18</span>
<span class="two-parts schedule-left-line">Dr. DANNY KUMAR</span>
</li>
<li><span class="one-parts">29</span>
<span class="two-parts schedule-left-line">Dr. MANSI</span>
</li>
<li><span class="one-parts">36</span>
<span class="two-parts schedule-left-line">Dr. VIKRAM BHASKAR , Dr. RAJDEEP MEENA</span>
</li>
<li><span class="one-parts">40</span>
<span class="two-parts schedule-left-line">Dr. TONY ETE , Dr. YOGESHA K N</span>
</li>
<li><span class="one-parts">43</span>
<span class="two-parts schedule-left-line">Dr. ALKA</span>
</li>
<li><span class="one-parts">46</span>
<span class="two-parts schedule-left-line">Dr. KOLHE PRASAD CHHABAN</span>
</li>
<li><span class="one-parts">49</span>
<span class="two-parts schedule-left-line">Dr. YAMINI KANSAL</span>
</li>
<li><span class="one-parts">55</span>
<span class="two-parts schedule-left-line">Dr. NEHA SAHAY</span>
</li>
<li><span class="one-parts">57</span>
<span class="two-parts schedule-left-line">Dr. ANKIT THORA</span>
</li>
<li><span class="one-parts">58</span>
<span class="two-parts schedule-left-line">Dr. ARPITA</span>
</li>
<li><span class="one-parts">70</span>
<span class="two-parts schedule-left-line">Dr. GAURAV MUKSHI</span>
</li>
<li><span class="one-parts">86</span>
<span class="two-parts schedule-left-line">Dr. OJAS SAINI</span>
</li>
<li><span class="one-parts">93</span>
<span class="two-parts schedule-left-line">Dr. VIKASH SHARMA</span>
</li>
<li><span class="one-parts">96</span>
<span class="two-parts schedule-left-line">Dr. TASSO BYAI</span>
</li> 
<li><span class="one-parts">113</span>
<span class="two-parts schedule-left-line">Dr. RAHUL MANGAL</span>
</li>
<li><span class="one-parts">114</span>
<span class="two-parts schedule-left-line">Dr. ARTI KULDEEP</span>
</li>
<li><span class="one-parts">119</span>
<span class="two-parts schedule-left-line">Dr. ANKIT CHAWLA</span>
</li>
<li><span class="one-parts">120</span>
<span class="two-parts schedule-left-line">Dr. TANMOY W MOMIN</span>
</li>
<li><span class="one-parts">139</span>
<span class="two-parts schedule-left-line">Dr. PAWAN KUMAR</span>
</li>
<li><span class="one-parts">140</span>
<span class="two-parts schedule-left-line">Dr. SHANGCHUNGLA KEISHING</span>
</li>
<li><span class="one-parts">146</span>
<span class="two-parts schedule-left-line">Dr. RAJEEV KUMAR</span>
</li>
<li><span class="one-parts">156</span>
<span class="two-parts schedule-left-line">Dr. SARANSH JAIN</span>
</li>
<li><span class="one-parts">166</span>
<span class="two-parts schedule-left-line">Dr. RAJKUMAR MEENA</span>
</li>
<li><span class="one-parts">170</span>
<span class="two-parts schedule-left-line">Dr. SOURABH SHARMA</span>
</li>
<li><span class="one-parts">187</span>
<span class="two-parts schedule-left-line">Dr. ARSHAD AHMED</span>
</li>
<li><span class="one-parts">200</span>
<span class="two-parts schedule-left-line">Dr. D WARI AKOR RYMBAI</span>
</li>
<li><span class="one-parts">209</span>
<span class="two-parts schedule-left-line">Dr. ANIL KUMAR G V</span>
</li>
<li><span class="one-parts">226</span>
<span class="two-parts schedule-left-line">Dr. RAJESH KUMAR</span>
</li>
<li><span class="one-parts">262</span>
<span class="two-parts schedule-left-line">Dr. SHWETA SINGH</span>
</li>
<li><span class="one-parts">270</span>
<span class="two-parts schedule-left-line">Dr. SAMEER GARG</span>
</li> 	
<li><span class="one-parts">272</span>
<span class="two-parts schedule-left-line">Dr. HASSAN MUBARAK</span>
</li>
<li><span class="one-parts">280</span>
<span class="two-parts schedule-left-line">Dr. VIVEK SHARMA</span>
</li>
<li><span class="one-parts">299</span>
<span class="two-parts schedule-left-line">Dr. HIMANSHU GUPTA</span>
</li>
<li><span class="one-parts">319</span>
<span class="two-parts schedule-left-line">Dr. VIKAS VERMA</span>
</li>
<li><span class="one-parts">334</span>
<span class="two-parts schedule-left-line">Dr. SUAIPG(NBE/NEET) PatternA MEENA</span>
</li>
<li><span class="one-parts">347</span>
<span class="two-parts schedule-left-line">Dr. VEERANNA KARADI</span>
</li>
<li><span class="one-parts">348</span>
<span class="two-parts schedule-left-line">Dr. SHRUTI GUPTA</span>
</li>
<li><span class="one-parts">350</span>
<span class="two-parts schedule-left-line">Dr. ABHINAV SHARMA</span>
</li>
<li><span class="one-parts">354</span>
<span class="two-parts schedule-left-line">Dr. MEENAKSHI GAUR</span>
</li>
<li><span class="one-parts">390</span>
<span class="two-parts schedule-left-line">Dr. USHA RANI</span>
</li>
<li><span class="one-parts">400</span>
<span class="two-parts schedule-left-line">Dr. UDAY JAIBHIM CHANDANKHEDE</span>
</li>
<li><span class="one-parts">410</span>
<span class="two-parts schedule-left-line">Dr. LAISHRAM SOMENKUMAR SINGH</span>
</li>
<li><span class="one-parts">422</span>
<span class="two-parts schedule-left-line">Dr. MANGILAL DEGANWA</span>
</li>
<li><span class="one-parts">448</span>
<span class="two-parts schedule-left-line">Dr. ANAND CHELLAPPAN</span>
</li>
<li><span class="one-parts">475</span>
<span class="two-parts schedule-left-line">Dr. RAKESH KUMAR</span>
</li>
<li><span class="one-parts">490</span>
<span class="two-parts schedule-left-line">Dr. SEEMA SHARMA</span>
</li>
<li><span class="one-parts">492</span>
<span class="two-parts schedule-left-line">Dr. NIVEDITA NIMESH</span>
</li>
<li><span class="one-parts">499</span>
<span class="two-parts schedule-left-line">Dr. NEHA GARG</span>
</li>
<li><span class="one-parts">507</span>
<span class="two-parts schedule-left-line">Dr. SWETA GUPTA</span>
</li>
<li><span class="one-parts">508</span>
<span class="two-parts schedule-left-line">Dr. SHIWANI CHOUHAN</span>
</li>
<li><span class="one-parts">583</span>
<span class="two-parts schedule-left-line">Dr. MAHESH KULDEEP</span>
</li>
<li><span class="one-parts">620</span>
<span class="two-parts schedule-left-line">Dr. KANGE SWATI SHIVAJIRAO</span>
</li>
<li><span class="one-parts">642</span>
<span class="two-parts schedule-left-line">Dr. JUHI BANSAL</span>
</li>
<li><span class="one-parts">643</span>
<span class="two-parts schedule-left-line">Dr. PATIL SUDIP MADHUKAR</span>
</li>
<li><span class="one-parts">654</span>
<span class="two-parts schedule-left-line">Dr. MALCOLM PINTO</span>
</li>
<li><span class="one-parts">659</span>
<span class="two-parts schedule-left-line">Dr. LOKESH KUMAR GAUTAM</span>
</li>
<li><span class="one-parts">677</span>
<span class="two-parts schedule-left-line">Dr. NAVRATHAN SUTHAR</span>
</li>
<li><span class="one-parts">695</span>
<span class="two-parts schedule-left-line">Dr. AJMERE VAIBHAV CHANDRAKANT</span>
</li>
<li><span class="one-parts">705</span>
<span class="two-parts schedule-left-line">Dr. JAISWAL SHARMA ANIL</span>
</li>
<li><span class="one-parts">706</span>
<span class="two-parts schedule-left-line">Dr. ANUSHREE CHATURVEDI</span>
</li>
<li><span class="one-parts">708</span>
<span class="two-parts schedule-left-line">Dr. BABITA</span>
</li>
<li><span class="one-parts">712</span>
<span class="two-parts schedule-left-line">Dr. LALIT KISHORE</span>
</li>
<li><span class="one-parts">751</span>
<span class="two-parts schedule-left-line">Dr. SUMAN SAURABH</span>
</li>
<li><span class="one-parts">765</span>
<span class="two-parts schedule-left-line">Dr. SIDDIQUI SHIBLEE SABIR</span>
</li>
<li><span class="one-parts">780</span>
<span class="two-parts schedule-left-line">Dr. ARGHA SAHA</span>
</li>
<li><span class="one-parts">783</span>
<span class="two-parts schedule-left-line">Dr. PTIL SUMEET PUNDLIK</span>
</li>
<li><span class="one-parts">790</span>
<span class="two-parts schedule-left-line">Dr. GAURAV VISHAL</span>
</li>
<li><span class="one-parts">797</span>
<span class="two-parts schedule-left-line">Dr. VIKRAM SINGH TANWAR</span>
</li>
<li><span class="one-parts">840</span>
<span class="two-parts schedule-left-line">Dr. ABHISHEK KUMAR</span>
</li>
<li><span class="one-parts">848</span>
<span class="two-parts schedule-left-line">Dr. DEEPAK N</span>
</li>
<li><span class="one-parts">852</span>
<span class="two-parts schedule-left-line">Dr. NIKHIL JAIN</span>
</li>
<li><span class="one-parts">877</span>
<span class="two-parts schedule-left-line">Dr. RAKESH CHOUDHARY</span>
</li>
<li><span class="one-parts">880</span>
<span class="two-parts schedule-left-line">Dr. ARVIND KUMAR</span>
</li>
<li><span class="one-parts">923</span>
<span class="two-parts schedule-left-line">Dr. APRATIM CHATTERJEE</span>
</li>
<li><span class="one-parts">956</span>
<span class="two-parts schedule-left-line">Dr. ADITYA KUMAR</span>
</li>
<li><span class="one-parts">1041</span>
<span class="two-parts schedule-left-line">Dr. THILAK S A</span>
</li>
<li><span class="one-parts">1047</span>
<span class="two-parts schedule-left-line">Dr. KUMKUM GUPTA</span>
</li>
<li><span class="one-parts">1059</span>
<span class="two-parts schedule-left-line">Dr. MEDHA SHARMA</span>
</li>
<li><span class="one-parts">1090</span>
<span class="two-parts schedule-left-line">Dr. DEEKSHA SINGH</span>
</li> 	
<li><span class="one-parts">1124</span>
<span class="two-parts schedule-left-line">Dr. VIJAY KUMAR KHANDELWAL</span>
</li>
<li><span class="one-parts">1144</span>
<span class="two-parts schedule-left-line">Dr. ASHOK KUMAR SHARMA</span>
</li>
<li><span class="one-parts">1146</span>
<span class="two-parts schedule-left-line">Dr. RAMANUJ SAMANTA</span>
</li>
<li><span class="one-parts">1192</span>
<span class="two-parts schedule-left-line">Dr. NAVAIPG(NBE/NEET) Pattern KALA</span>
</li>
<li><span class="one-parts">1194</span>
<span class="two-parts schedule-left-line">Dr. SHIVANI GUPTA</span>
</li>
<li><span class="one-parts">1232</span>
<span class="two-parts schedule-left-line">Dr. GANPAT</span>
</li>
<li><span class="one-parts">1285</span>
<span class="two-parts schedule-left-line">Dr. PANCHMI GUPTA</span>
</li>
<li><span class="one-parts">1295</span>
<span class="two-parts schedule-left-line">Dr. JEEVIKA KATARIA</span>
</li>
<li><span class="one-parts">1316</span>
<span class="two-parts schedule-left-line">Dr. AMIT VERMA</span>
</li>
<li><span class="one-parts">1397</span>
<span class="two-parts schedule-left-line">Dr. NISHU GOYAL</span>
</li>
<li><span class="one-parts">1431</span>
<span class="two-parts schedule-left-line">Dr. MANUSHREE GAUTAM</span>
</li>
<li><span class="one-parts">1436</span>
<span class="two-parts schedule-left-line">Dr. MAMTA DHAKA</span>
</li>
<li><span class="one-parts">1596</span>
<span class="two-parts schedule-left-line">Dr. SONAL AGRAWAL</span>
</li>
<li><span class="one-parts">1634</span>
<span class="two-parts schedule-left-line">Dr. BABITA JANGID</span>
</li>
<li><span class="one-parts">1702</span>
<span class="two-parts schedule-left-line">Dr. KRISHNA KUMAR</span>
</li>
<li><span class="one-parts">1816</span>
<span class="two-parts schedule-left-line">Dr. SUMIT KUMAR</span>
</li>
<li><span class="one-parts">1834</span>
<span class="two-parts schedule-left-line">Dr. ANIL KUMAR GUPTA</span>
</li>
<li><span class="one-parts">1855</span>
<span class="two-parts schedule-left-line">Dr. DEEPAK SHARMA</span>
</li>
<li><span class="one-parts">1886</span>
<span class="two-parts schedule-left-line">Dr. ABHILASHA THANVI</span>
</li>
<li><span class="one-parts">1929</span>
<span class="two-parts schedule-left-line">Dr. GAURAV GROVER</span>
</li>
<li><span class="one-parts">1953</span>
<span class="two-parts schedule-left-line">Dr. GARIMA SHARMA</span>
</li>
<li><span class="one-parts">1960</span>
<span class="two-parts schedule-left-line">Dr. DEEPTI AGGARWAL</span>
</li>
<li><span class="one-parts">1971</span>
<span class="two-parts schedule-left-line">Dr. POOJA BIHANI</span>
</li>
<li><span class="one-parts">1983</span>
<span class="two-parts schedule-left-line">Dr. JEETENDRA KUMAR SHARMA</span>
</li>
<li><span class="one-parts">1994</span>
<span class="two-parts schedule-left-line">Dr. VINOD KUMAR</span>
</li>
<li><span class="one-parts">2021</span>
<span class="two-parts schedule-left-line">Dr. NEHA JAIN</span>
</li>
<li><span class="one-parts">2022</span>
<span class="two-parts schedule-left-line">Dr. RITU SINGH</span>
</li>
<li><span class="one-parts">2056</span>
<span class="two-parts schedule-left-line">Dr. AMIT JAIN</span>
</li>
<li><span class="one-parts">2150</span>
<span class="two-parts schedule-left-line">Dr. NITIN GUPTA</span>
</li>
<li><span class="one-parts">2154</span>
<span class="two-parts schedule-left-line">Dr. HARISH PUNIA</span>
</li>
<li><span class="one-parts">2165</span>
<span class="two-parts schedule-left-line">Dr. RACHNA GOYAL</span>
</li>
<li><span class="one-parts">2184</span>
<span class="two-parts schedule-left-line">Dr. SHEWTA SINGH</span>
</li>
<li><span class="one-parts">2246</span>
<span class="two-parts schedule-left-line">Dr. PURVA SHARMA</span>
</li>
<li><span class="one-parts">2394</span>
<span class="two-parts schedule-left-line">Dr. PATIL SUMERU BHIMRAO</span>
</li>
<li><span class="one-parts">2419</span>
<span class="two-parts schedule-left-line">Dr. AKASH GUPTA</span>
</li>
<li><span class="one-parts">2432</span>
<span class="two-parts schedule-left-line">Dr. NEHA GUPTA</span>
</li>
<li><span class="one-parts">2481</span>
<span class="two-parts schedule-left-line">Dr. NEHA AGRAWAL</span>
</li>
<li><span class="one-parts">2500</span>
<span class="two-parts schedule-left-line">Dr. GURKIRAT SINGH</span>
</li>
<li><span class="one-parts">2628</span>
<span class="two-parts schedule-left-line">Dr. PUAIPG(NBE/NEET) Pattern NAGPAL</span>
</li>
<li><span class="one-parts">2646</span>
<span class="two-parts schedule-left-line">Dr. KRISHNA VEER SINGH CHOUDHARY</span>
</li>
<li><span class="one-parts">2655</span>
<span class="two-parts schedule-left-line">Dr. SAPTARSHI MUKHOPADHYAY</span>
</li>
<li><span class="one-parts">2667</span>
<span class="two-parts schedule-left-line">Dr. HIMANSHU ROHELA</span>
</li>
<li><span class="one-parts">2691</span>
<span class="two-parts schedule-left-line">Dr. DHRUV BATRA</span>
</li>
<li><span class="one-parts">2809</span>
<span class="two-parts schedule-left-line">Dr. VINITA AGRAWAL</span>
</li>
<li><span class="one-parts">2828</span>
<span class="two-parts schedule-left-line">Dr. PRIYANKA MINOCHA</span>
</li>
<li><span class="one-parts">2915</span>
<span class="two-parts schedule-left-line">Dr. AMOL SINGHAL</span>
</li>
<li><span class="one-parts">2955</span>
<span class="two-parts schedule-left-line">Dr. MAYANK MAHAJAN</span>
</li>
<li><span class="one-parts">2985</span>
<span class="two-parts schedule-left-line">Dr. KERTHANA ANAND</span>
</li>
<li><span class="one-parts">3044</span>
<span class="two-parts schedule-left-line">Dr. MANALI JAIN</span>
</li>
<li><span class="one-parts">3087</span>
<span class="two-parts schedule-left-line">Dr. ANUPAM DATTA</span>
</li>
<li><span class="one-parts">3201</span>
<span class="two-parts schedule-left-line">Dr. HIMANSHU AWASTHI</span>
</li>
<li><span class="one-parts">3223</span>
<span class="two-parts schedule-left-line">Dr. SHIKHA MEEL</span>
</li>
<li><span class="one-parts">3315</span>
<span class="two-parts schedule-left-line">Dr. DEVENDER DUA</span>
</li>
<li><span class="one-parts">3453</span>
<span class="two-parts schedule-left-line">Dr. KUSHAGRA AGRAWAL</span>
</li>
<li><span class="one-parts">3473</span>
<span class="two-parts schedule-left-line">Dr. AMIT KUMAR PURANG</span>
</li>
<li><span class="one-parts">3481</span>
<span class="two-parts schedule-left-line">Dr. LAXMI MODI</span>
</li>
<li><span class="one-parts">3535</span>
<span class="two-parts schedule-left-line">Dr. ASHISH BADAYA</span>
</li>
<li><span class="one-parts">3690</span>
<span class="two-parts schedule-left-line">Dr. MANU SINGH</span>
</li>
<li><span class="one-parts">3699</span>
<span class="two-parts schedule-left-line">Dr. ABHINAV GUPTA</span>
</li>
<li><span class="one-parts">3745</span>
<span class="two-parts schedule-left-line">Dr. SHUBHANKUR GUPTA</span>
</li>
<li><span class="one-parts">3779</span>
<span class="two-parts schedule-left-line">Dr. DEEPAK CHOUDHARY</span>
</li>
<li><span class="one-parts">3790</span>
<span class="two-parts schedule-left-line">Dr. PRIYADARSHINI</span>
</li>
<li><span class="one-parts">3808</span>
<span class="two-parts schedule-left-line">Dr. SANDEEP PRASAD</span>
</li>
<li><span class="one-parts">3832</span>
<span class="two-parts schedule-left-line">Dr. AVNEESH KHARE</span>
</li>
<li><span class="one-parts">3853</span>
<span class="two-parts schedule-left-line">Dr. DAYARAM CHOUDHARY</span>
</li>
<li><span class="one-parts">3871</span>
<span class="two-parts schedule-left-line">Dr. ARUN KUMAR AGRAWAL</span>
</li>
<li><span class="one-parts">3878</span>
<span class="two-parts schedule-left-line">Dr. VARUN KUMAR AGRAWAL</span>
</li>
<li><span class="one-parts">3896</span>
<span class="two-parts schedule-left-line">Dr. SNEHA BHATIA</span>
</li>
<li><span class="one-parts">3972</span>
<span class="two-parts schedule-left-line">Dr. TARA CHAND KUMAWAT</span>
</li>
<li><span class="one-parts">3997</span>
<span class="two-parts schedule-left-line">Dr. DINESH KUMAR YADAV</span>
</li>
<li><span class="one-parts">4007</span>
<span class="two-parts schedule-left-line">Dr. DILIP SINGH</span>
</li>
<li><span class="one-parts">4028</span>
<span class="two-parts schedule-left-line">Dr. NIKHIL SHARMA</span>
</li>
<li><span class="one-parts">4149</span>
<span class="two-parts schedule-left-line">Dr. MANISH KUMAR SHARMA</span>
</li>
<li><span class="one-parts">4164</span>
<span class="two-parts schedule-left-line">Dr. SWATI KULHAR</span>
</li>
<li><span class="one-parts">4264</span>
<span class="two-parts schedule-left-line">Dr. HEMANT KUMAR</span>
</li>
<li><span class="one-parts">4537</span>
<span class="two-parts schedule-left-line">Dr. KALYAN SHOBHANA</span>
</li>
<li><span class="one-parts">4616</span>
<span class="two-parts schedule-left-line">Dr. NIDHI GUPTA</span>
</li>
<li><span class="one-parts">4723</span>
<span class="two-parts schedule-left-line">Dr. NITIN AGRAWAL</span>
</li>
<li><span class="one-parts">4750</span>
<span class="two-parts schedule-left-line">Dr. MANISH CHOUDHARY</span>
</li>
<li><span class="one-parts">4860</span>
<span class="two-parts schedule-left-line">Dr. NEHA GARG</span>
</li>
<li><span class="one-parts">4886</span>
<span class="two-parts schedule-left-line">Dr. NISHTHA SINGH</span>
</li>
<li><span class="one-parts">4887</span>
<span class="two-parts schedule-left-line">Dr. PANKAJ KUMAR GUPTA</span>
</li>
<li><span class="one-parts">5016</span>
<span class="two-parts schedule-left-line">Dr. ABHINAV SINGH</span>
</li>
<li><span class="one-parts">5032</span>
<span class="two-parts schedule-left-line">Dr. SUNITA KULHARI</span>
</li>
<li><span class="one-parts">5041</span>
<span class="two-parts schedule-left-line">Dr. MAHENDRA KUMAR GUPTA</span>
</li>
<li><span class="one-parts">5076</span>
<span class="two-parts schedule-left-line">Dr. PRABHASH BHAVSAR</span>
</li>
<li><span class="one-parts">5403</span>
<span class="two-parts schedule-left-line">Dr. AMIT KUMAR</span>
</li>
<li><span class="one-parts">5407</span>
<span class="two-parts schedule-left-line">Dr. RAHUL BHARGAVA</span>
</li>
<li><span class="one-parts">&nbsp;</span>
<span class="two-parts schedule-left-line" style="color:#000;">And many more...</span>
</li>
</ul>
</div>
</div>
</article></div>

<div id="official"> 
<article class="interview-photos-section"> 
<div class="achievment-videos-section">
<ul>
<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
<p>Rank: 1st AIIMS Nov 2013</p>
<div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
<p>Rank: 56th AIIMS Nov 2013</p>
<div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>

<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="boder-none"></iframe> 
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
<p>Rank: 6th PGI Nov 2013</p>
<div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
<p>Rank: 24th PGI Nov 2013</p>
<div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>
</ul>
</div>
</article> 
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="../js/html5.js"></script>
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/registration.js"></script>
<script type="text/javascript" src="../js/add-cart.js"></script>
</body>
</html>