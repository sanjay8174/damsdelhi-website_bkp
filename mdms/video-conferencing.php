<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="../js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){	
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();	
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
 });
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="../regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="../satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="../test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <!--<div class="banner-right-btns"> <a href="../dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="../find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="../photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
       <div class="banner-right-btns"> <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="../find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="../photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Video Conferencing" class="active-link">Video Conferencing</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Video Conferencing</h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <p>'Video Conferencing' Classes for PG Medical Entrance Exam available All Over India
                  Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations.</p>
                <p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>
                <p>We have seen that student in small town &amp; tier II cities are at a disadvantage that our classroom centres are mainly in metropolitan cities. Now with technology we have launched in country which can have exclusive Video Conferencing for PG Medical Entrance Exam in 242 centres across the country in partnership with Reliance World.</p>
                <p>After a huge success of Virtual Classroom Coaching in 2011. The new session for the Virtual Classroom Coaching is starting soon.</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Features :-</div>
                <ul class="benefits">
                  <li><span></span>33 Sessions</li>
                  <li><span></span>Individual sessions for 4 hrs (Total 132 hrs)</li>
                  <li><span></span>Classes twice in a week</li>
                  <li><span></span>Available in all "Reliance Web World" centre</li>
                  <li><span></span>Interactive classes by best exclusive DAMS faculty.</li>
                  <li><span></span>Best results in business</li>
                  <li><span></span>Small batches</li>
                  <li><span></span>Two way communications centers</li>
                  <li><span></span>The participants get to interact with professors just the way they would in a real classroom (which means they can see, hear &amp; even ask questions). Several premier educational institutes are successfully using this platform to expand the reach of their courses beyond campus-based programmes.</li>
                </ul>
              </aside>
              <span class="additional-heading">Additional Features :-</span>
              <p>Online Test Series (includes Grand Tests, Neet Mocks &amp; Subject wise test series)</p>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Note:- Free Demo class is also available.</div>
                <div class="download-pdf">
                  <ul>
                    <li>
                      <div class="download">
                        <div class="orange-box">Download the Registration form</div>
                        <a href="https://damsdelhi.com/REGISTRATION%20FORM%20for%20website.pdf"><img src="../images/download-arrow.png" title="Download" alt="Download"> <span>Download</span></a> </div>
                    </li>
                  </ul>
                </div>
              </aside>
            </article>
            <div class="video-conferencing">
              <h4>Video conferecing Demo Session 2012</h4>
              <article class="achievment-videos-section">
                <ul>
                  <li>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/_fmH7rgZQFs" frameborder="0" allowfullscreen=""></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List"> </div>
                      </div>
                    </div>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/BE2SCC6x_1s" frameborder="0" allowfullscreen=""></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List"> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </article>
            </div>
            <div class="video-conferencing">
              <h4>Video conferecing Demo Session 2011</h4>
              <article class="achievment-videos-section">
                <ul>
                  <li>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/fwAqUS-nWM0" frameborder="0" allowfullscreen=""></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List"> </div>
                      </div>
                    </div>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/iaGhCWPT4wk" frameborder="0" allowfullscreen=""></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List"> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </article>
            </div>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="accor-wrapper">
            <div class="accordionButton"><span></span>Interns/Post Intern students</div>
            <div class="accordionContent">
              <div class="inner-accor">
                <ul>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="#" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course (weekend)</a></li>
                    <li><a href="#" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
                    <li><a href="#" title="Crash course"><span class="sub-arrow"></span> Crash course</a></li>
                  </ol>
                  <li>Distant Learning Programme</li>
                  <ol>
                    <li><a href="#" title="Postal course"><span class="sub-arrow"></span> Postal course</a></li>
                    <li><a href="#" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                  </ol>
                  <li>Test Series</li>
                  <li>Online</li>
                  <li>Offline</li>
                </ul>
              </div>
            </div>
            <div class="accordionButton"><span></span>Prefinal/Final Year students</div>
            <div class="accordionContent">
              <div class="inner-accor">
                <ul>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="#" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
                    <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
                    <li><a href="#" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                    <li><a href="#" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
                  </ol>
                  <li>2nd Professional  students</li>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="#" title="Prefoundation course"><span class="sub-arrow"></span> Prefoundation course</a></li>
                  </ol>
                </ul>
              </div>
            </div>
            <div class="accordionButton"><span></span>Distant Learning Programme</div>
            <div class="accordionContent">
              <div class="inner-accor">
                <ul>
                  <li>Tablet Based Course - IDAMS</li>
                  <li>Test Series</li>
                  <li>1st Professional Students</li>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="#" title="First Step course"><span class="sub-arrow"></span> First Step course</a></li>
                    <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
                    <li><a href="#" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                    <li><a href="#" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
                  </ol>
                </ul>
              </div>
            </div>
          </div>
          
          <!--for Enquiry -->
          <?php include '../enquiryform.php'; ?>
          <!--for Enquiry -->
          
          <div class="virtual-class">
            <div class="virtual-heading"><a href="#" title="Click Here"><span class="white-arrow"></span>Click Here</a> Virtual Class Room Centre</div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="../navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>