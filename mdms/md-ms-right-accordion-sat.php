<script>
function sliddes1(val)
{
   var sp1=$('div.accordionButton > span').size();
   for(var d=1;d<=sp1;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sq'+d).removeClass('arrowup');
         $('#sq'+d).addClass('arrowdwn');
		 $('#dip'+d).slideUp(400);
	   }      
   }
   
   $('#dip'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sq'+val).removeClass('arrowdwn');
            $('#sq'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sq'+val).removeClass('arrowup');
            $('#sq'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddes1('1')" style="margin-top:0px;"><span class="arrowdwn" id="sq1"></span>Interns/Post Intern Students</div>
  <div class="accordionContent" id="dip1" style="display: none;">
    <div class="inner-accor">
      <ul>
        <li>Classroom Courses</li>
        <ol>
          <li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course</a></li>
          <li><a href="t&amp;d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
          <li><a href="crash_course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes1('2')"><span class="arrowdwn" id="sq2"></span>Prefinal/Final Year Students</div>
  <div class="accordionContent" id="dip2" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="foundation_course.php" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes1('3')" ><span class="arrowdwn" id="sq3"></span>1st/2nd Professional Students</div>
  <div class="accordionContent" id="dip3" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="prefoundation_course.php" title="Prefoundation Course"><span class="sub-arrow"></span> Prefoundation Course</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
