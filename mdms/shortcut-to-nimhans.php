<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php';?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article>
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Shortcut to NIMHANS" class="active-link">Shortcut to NIMHANS</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Shortcut to NIMHANS</h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <p>The National Institute of Mental Health and Neuro Sciences is a multidisciplinary Institute for patient care and academic pursuit in the frontier area of Mental Health and Neuro Sciences. The Lunatic Asylum which came into being in the latter part of the 19th Century was renamed as Mental Hospital in 1925 by the erstwhile Government of Mysore. This hospital and All India Institute of Mental Health established in 1954 by Government of India were amalgamated on 27th December 1974, and thus was formed the autonomous National Institute of Mental Health and Neuro Sciences (NIMHANS).</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span> About the exam :-</div>
                <p>NIMHANS PG Medical Entrance Exam has a single paper. This paper encompasses MCQ's of the objective nature. Questions of the graduate level shall be posed in this paper. The time period of this examination is based on the many departments. Aspirants who have a clear knowledge about the pattern and syllabus of the MBBS course will be able to perform admirably in this exam.</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Important Dates :-</div>
                <ul class="benefits">
                  <li><span></span>Availability of the admission form (offline): First week of March.</li>
                  <li><span></span>Availability of admission form (online): First week of April.</li>
                  <li><span></span>Final date for the reception of the form by courier or post: Mid week of April.</li>
                  <li><span></span>Date of admission test: Last week of June.</li>
                  <li><span></span>Commencement of the programs: First week of August.</li>
                  <li><span></span>Final date for the admittance of aspirants: Second week of August.</li>
                </ul>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Contact Address of NIMHANS :-</div>
                <p>Aspirants can contact NIMHANS at:
                  Address: Director / Vice Chancellor, <br />
                  NIMHANS, Hosur Road, Bangalore – 560029, India.<br />
                  Telephone: 91-080-26995001/5002, 26564140, 26561811, 26565822.<br />
                  Fax: 91-080-26564830.<br />
                  Website URL: www.nimhans.kar.nic.in</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>CONTENTS :-</div>
                <p>1. TWO MOCK TESTS</p>
                <p>2. HIGH YIELDING NOTES FOR NIMHANS INCLUDING-</p>
                <p>3. TOPICWISE TESTS</p>
                <br />
                <ul class="benefits">
                  <li><span></span>PSYCHIATRY</li>
                  <li><span></span>NEUROPHYSIOLOGY</li>
                  <li><span></span>NEUROANATOMY</li>
                  <li><span></span>NEUROPSYCHOPHARMACOLOGY</li>
                  <li><span></span>NEUROMEDICINE</li>
                </ul>
                <p>So come &amp; handed your future for the betterment by joining DAMS.</p>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include '../enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="../js/html5.js"></script>
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/registration.js"></script>
<script type="text/javascript" src="../js/add-cart.js"></script>
</body>
</html>