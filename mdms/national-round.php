<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="../js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
function ontab(val) {
	var len = $("#accordion > li").size();
    for(i=1;i<=len;i++)
	{
		if(val==i)
		{
			if(i==1)
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue');
			}
			else
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue-inner');				
			}
		}
		else
		{ 
		    if(i==1)
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue');
		    }
		    else
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue-inner');
		    }
		}
	}
};
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="../regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="../satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="../test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'../md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="National Quiz" href="../national.php">National Quiz</a></li>
          <li><a title="National Round" class="active-link">National Round</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li class="border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol1">
                  <li><span class="mini-arrow">&nbsp;</span><a href="../season2round1.php" title="Round 1">Round 1</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="../season2round2.php" title="Round 2">Round 2</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="../grand2013.php" title="Finale">Finale</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');" class="light-blue-inner"><span id="accorsp2" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_block" id="aol2">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="../round1.php" title="Round 1">Round 1</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="../round2.php" title="Round 2">Round 2</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="../grand.php" title="Finale">Finale</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>Grey Matter - Dams National Quiz Round - 1 Results</h4>
            <section class="showme-main"> 
              <script type="text/javascript" src="https://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
              <link type="text/css" href="https://www.sunsean.com/idTabs/main.css" />
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="ROUND -1">ROUND -1</a></li>
                <li><a href="#official" title="Video">Video</a></li>
              </ul>
              <div id="jquery" class="display_block">
                <article class="interview-photos-section">
                  <ul class="main-students-list ">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="../images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM">
                        <p><span>Dr. DEVANSHU BANSAL</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="../images/students/student5.jpg" alt="Student" title="Student">
                        <p><span>Dr. SHWETA SHUBHDARSHINI</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="../images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT">
                        <p><span>Dr. DEVIKA KIR </span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="../images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN">
                        <p><span>Dr. ANURAG CHAHAL</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>
                  <div class="schedule-mini-series  t-space-20">
                    <h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">Devanshu Bansal</span> <span class="one-parts">40</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devika Kir</span> <span class="one-parts">34</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">Arjung Gupta</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Shweta Shubhdarshini</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sarita Kumari</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Anurag Chahal</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Avin Goel</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Kaustav Majumder</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Savinay Kapur</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Rituraj Upadhyay</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Manpreet Uppal</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Nishant Gurnani</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Barun Bagga</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sahil Gupta</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devesh Kumawat</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Harshit Garg</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Vineet Kumar</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Varsha Mathews</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Neethu</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Ahmed Nawid Latifi</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUJ A</span> <span class="one-parts">20</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">Mandeep Singh Virk</span> <span class="one-parts">18</span> <span class="three-part">11</span> </li>
                      </ul>
                    </div>
                  </div>
                  <br clear="all" />
                  <br clear="all" />
                </article>
              </div>
              <div id="official" class="display_none">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/947yqPU8uHI" frameborder="0" allowfullscreen=""></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="../images/social-list.jpg" title="List" alt="List"> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="../navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>