<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		



	<title>MammoScreen India</title>
<meta name="description" content="#">
<meta name="keywords" content="#">
<meta name="#" content="#">


		


	  
		<link rel="stylesheet" href="css/styles.css" media="all" type="text/css">
		

	
		<script language="JavaScript" src="js/code.js" type="text/javascript"></script>
		

		




	


		<script type="text/javascript">
		//<![CDATA[
		hs.registerOverlay({
			html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
			position: 'top right',
			fade: 2 // fading the semi-transparent overlay looks bad in IE
		});
		
		hs.graphicsDir = '';
		hs.wrapperClassName = 'borderless';
	    hs.showCredits = false;
		//]]>
		</script>
	
	<!-- Highslide - END -->
	

	







	</head>

	<body>



                                                   <!---main table start--->
                                                   


                                                     <!---page start--->

<div id="page">

                                                     <!---header start--->


<div id="header">

<div id="had_top">
<div id="menu">
<ul>
<li><a href="index.php">HOME</a></li>
<li><a href="contact_us.php">CONTACT US</a></li>
<li><a href="#">SITE MAP</a></li>
</ul>
</div><!-- id menu -->
</div><!-- id had_top -->

<div id="logo">
<div id="logo_title">
<a href="index.php"><img src="images/title.gif" class="india_text" /></a>
</div><!-- id logo_title -->
<a href="index.php"><img src="images/logo_mammo_screen.gif" alt="lets pink"></a>
</div><!-- id logo -->

<div class="search">

<input type="text" class="search_text" /></input>
<input src="images/search.gif" alt="Search" name="search_form" onclick="#" type="image" width="34" height="17"></input>

</div> <!-- id search -->                                                          

              
<div id="navi">
<ul>

<li>
<div class="navi_open"  >
<table>
<tbody><tr>
<td >
<a href="Vision_and_Mission.php">
<img src="images/awareness_small.gif" alt="" width="130" height="90">
<div class="navi_had">Vision and Mission</div><!-- class navi_had -->
<p>To provide excellent health care service by obtaining high quality mammograms with their accurate  reports and archiving the same for future comparison, </p>
</a>
</td>
<td >
<a href="our team.php">
<img src="images/our_team_small.gif" alt="" width="130" height="90">
<div class="navi_had">Our Team</div><!-- class navi_had -->
<p>A dedicated team of health professionals from different subspecialties, but with same goal…. </p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">About Us</a>
</li>

<li>
<div class="navi_open">
<table>
<tbody><tr>
<td >
<a href="abt_mammo.php">
<img src="images/abt_mammo_small.jpg" alt="" width="130" height="90">
<div class="navi_had">Mammography</div><!-- class navi_had -->
<p>Mammography is the process of using low-energy-X-rays to examine the human breast</p>
</a>
</td>
<td >
<a href="cos.php">
<img src="images/screened_small.gif" alt="" width="130" height="90">
<div class="navi_had">Concept of Screening</div><!-- class navi_had -->
<p>Worldwide breast cancer remains the commonest cancer to affect women.  </p>
</a>
</td>
<td >
<a href="indications.php">
<img src="images/indi_small.jpg" alt="" width="135" height="90">
<div class="navi_had">Indications</div><!-- class navi_had -->
<p>Every asymptomatic female between age group of 50-69 should undergo annual screening mammogram.  </p>
</a>
</td>
<td >
<a href="limitation.php">
<img src="images/limit_small.jpg" alt="" width="135" height="90">
<div class="navi_had">Limitations</div><!-- class navi_had -->
<p>All diagnostic modalities have their limitations, so do mammograms! Few cancers in early stage may not be picked up on mammograms.  </p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">About Mammography</a>
</li>

<li>
<div class="navi_open" >
<table>
<tbody><tr>
<td >
<a href="our quality.php">
<img src="images/qua_small.gif" alt="" width="130" height="90">
<div class="navi_had">Quality Control </div><!-- class navi_had -->
<p>All staff intensively trained on regular basis by qualified professionals. </p>
</a>
</td>
<td >
<a href="why_choose_mammoscreen.php">
<img src="images/provide_small.jpg" alt="" width="130" height="90">
<div class="navi_had">We Provide</div><!-- class navi_had -->
<p>Dedicated Breast care service-First of its kind in India.</p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="why_choose_mammoscreen.php">Why Choose Us</a>
</li>

<li>
<a href="news.php">News</a>
</li>

<li>
<a href="https://mammoscreenindia.blogspot.in/">Blog</a>
</li>

<li>
<a href="career.php">Career</a>
</li>

<li>
<a href="partner_with_us.php">Partner with Us</a>
</li>

<li>
<a href="contact_us.php">Contact Us</a>
</li>

</ul>
</div><!-- id navi -->


</div><!-- id header -->
                              
	                                                <!--- header close --->

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td>

                                                   <!--- banner start --->
                                                   
<div id="banner" title="8000">

<div class="detail">
<DIV id="text0">
<h5>VISION & MISION</h5>
<h2>Mission</h2>
<p>To make state of the art Mammogram facility available at reasonable cost in every corner of our country, along with efficient storage of their database for future actions. Be the Pioneer in India in this segment.</p>
<div class="l_more">
<a href="#" class="blue"><span>Learn More</span></a>
</div><!-- class l_more -->
</div><!-- id text0 -->
<div  id="slide_image0"><img src="images/auto2.gif" alt="">
</div><!-- id slide_image0 -->
</div><!-- class detail -->

<div class="detail">
<div  id="text1">
<h5>ABOUT MAMMOGRAPHY</h5>
<h2>What is Mammography?</h2>
<p>Mammography is the process of using low-energy-X-rays to examine the human breast and is used as a diagnostic and a screening tool.The goal of mammography is the early detection of breast cancer.  </p>
<div class="l_more">
<a href="#" class="blue"><span>Learn More</span></a>
</div><!-- class l_more -->
</div><!-- id text1 -->
<div style="display: block; opacity: 1;" id="slide_image1"><img src="images/concept.jpg" alt="">
</div><!-- id slide_image1 -->
</div><!-- class detail -->

<div class="detail">
<div  id="text2">
<h5>WHY CHOOSE US</h5>
<h2>Why choose mammoscreen?</h2>
<p>Do you as responsible lady believe that you have a right to choose the best and do you agree with our philosophy of PREVENTION IS BETTER THAN CURE?</p>
<div class="l_more">
<a href="#" class="blue"><span>Learn More</span></a>
</div><!-- class l_more -->
</div><!-- id text2 -->
<div style="display: none; opacity: 0;" id="slide_image2"><img src="images/quality.jpg" alt="">
</div><!-- id slide_image2 -->
</div><!-- class detail -->

<div class="detail">
<div style="display: none;" id="text3">
<h5>Partner With Us</h5>
<h2>then, MAMMOSCREEN INDIA is your right partner.</h2>
<p>We partner with leading Doctors/ nursing homes/hospitals/Diagnostic centers in various parts of country,  we provide them our Mammography equipment and know how regarding mammography. </p>
<div class="l_more">
<a href="#" class="blue"><span>Learn More</span></a>
</div><!-- class l_more -->
</div><!-- id text3 -->
<div style="display: none; opacity: 0;" id="slide_image3"><img src="images/partner_with_us.jpg" alt="">
</div><!-- id slide_image3 -->
</div><!-- class detail -->


<div style="display: block;" id="slider_box" class="select1">

<ul>
<li class="box0" onclick="buttonClicked(0)">
<div class="img" ><img src="images/auto2.gif" alt="" width="47" height="37"></div><!-- class img -->
<div class="txt">Vision & Mission</div><!-- class txt -->
</li>

<li class="box1" onclick="buttonClicked(1)">
<div class="img"><img src="images/concept.jpg" alt="" width="47" height="37"></div><!-- class img -->
<div class="txt">about mammography</div><!-- class txt -->
</li>

<li class="box2" onclick="buttonClicked(2)">
<div class="img"><img src="images/quality.jpg" alt="" width="47" height="37"></div><!-- class img -->
<div class="txt">why
choose us</div><!-- class txt -->
</li>

<li class="box3" onclick="buttonClicked(3)">
<div class="img"><img src="images/partner_with_us.jpg" alt="" width="47" height="37"></div><!-- class img -->
<div class="txt">partner<br />
with us</div><!-- class txt -->
</li>

</ul>
</div><!-- id slider_box -->


</div><!-- id banner -->
       
                                                       <!--- banner close --->
       
                                                        <!--- left_box start --->
<div class="left_box"  >

<div class="query_box" >
    <?php include 'enquiry.php'; ?>
<!--<h1>Enquiry Form</h1>-->

		
<!--<ul>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Your Full Name') this.value='';" value="Your Full Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Full Address') this.value='';" value="Full Address" class="query_box-text1" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Hospital Name') this.value='';" value="Hospital Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Select City') this.value='';" value="Select City" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Telephone No.') this.value='';" value="Telephone No." class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='E-Mail Address') this.value='';" value="E-Mail Address" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Type Your Message') this.value='';" value="Type Your Message" class="query_box-text2" /></li>

<li><input name="" value="Submit" type="button" class="query-button" /></li>
</ul>-->

</div><!-- class query_box -->

<div class="news_box">
<div class="news" >
<div class="title"></div>
<ul >
<li >
<div class="subhead" >Mammgraphy</div><!-- class subhead -->
<h4><a href="#">Webinar Replay: Managing Global Health 
Technology Assessment Trends with a 
Real-World Evidence Lifecycle Strategy</a></h4>
...............................................
<br /><br />
</li>
<li>
<div class="subhead">Mammgraphy</div><!-- class subhead -->
<h4><a href="#" >Webinar Replay: Shaping the Biosimilars
Opportunity, a Global Perspective on the 
Evolving Biosimilars Landscape</a></h4>
...............................................
<br /><br />
</li>
<li>
<div class="subhead">Mammgraphy</div><!-- class subhead -->
<h4><a href="#" >Webinar Replay: Commercial Analytics at the Cross Roads</a></h4>
</li>
</ul>
</div><!-- class news -->
</div><!-- class news_box -->
</div><!-- class left_box -->
		                                           
                                                    <!--- left_box close --->
                                                     <!--- right_box start --->	
            
<div class="right_box">
<div class="small_ban">
<ul>
<li><a href="#"><img src="images/lady-banner.jpg" alt="" width="235" height="201">
</a></li>
<li class="last"><a href="#"><img src="images/globe.jpg" alt="" width="235" height="201">
</a></li>
</ul>
</div><!-- class small_ban -->
				
<div class="team" >
<img src="images/team.jpg" alt="" width="111" height="83">
<div class="team_content">
<h4><a href="#" onclick="# "><img src="images/team.gif" width="180" height="21"></a></h4><br />
<p>A dedicated team of health professionals from different subspecialties, but with same goal….</p><br />
<a href="#" onclick="#">Learn More</a>
</div><!-- class team_content -->
</div><!-- class team -->
</div><!-- class right_box -->
		
	                                                 <!--- right_box close --->

</td>
</tr>
</tbody></table>
                                                     
                                                     <!--- footer start --->

<div id="footer_border"></div>
<div id="footer">
<table>
<tbody><tr>
<td>
<h6>About Us</h6>
<ul>
<li><a href="Vision_and_Mission.php">Vision and Mission</a></li>
<li><a href="our team.php">Our Team</a></li>
</ul>
</td>
<td>
<h6>About Mammgraphy</h6>
<ul>
<li><a href="abt_mammo.php">Mammgraphy</a></li>
<li><a href="cos.php">Concept of screening</a></li>
<li><a href="indications.php">Indications</a></li>
<li><a href="limitation.php">Limitations</a></li>
</ul>
</td>
<td>
<h6>Why Choose Us</h6>
<ul>
<li><a href="why_choose_mammoscreen.php">We Provide</a></li>
<li><a href="our quality.php">Quality Control </a></li>
</ul>
</td>
<td class="last">
<ul>
<li><a href="news.php">News</a></li>
<li><a href="https://mammoscreenindia.blogspot.in/">Blog</a></li>
<li><a href="career.php">Career</a></li>
<li><a href="partner_with_us.php">Partner with us</a></li>
<li><a href="contact_us.php">Contact us</a></li>
</ul>
</td>
</tr>
</tbody></table>
</div><!-- id footer -->

<div id="foot">
<div class="foot_text">
<ul>
<li><a href="contact_us.php">Contact Us</a></li>
<li><a href="#">Privacy</a></li>
<li><a href="#">Trademarks</a></li>
<li><a href="#">Site Map</a></li>
<li>Copyright © 2011 lets pink. All rights reserved.</li>
<li class="last"><a href="https://www.gingerwebs.com" target="_blank">Website Design by Ginger Webs</a></li>
</ul>
</div><!-- div class foot_text -->
<div class="social">
<ul >
<li ><a href="#"><img src="images/fb.gif" /></a></li>
<li ><a href="#"><img src="images/twitter.gif" /></a></li>
<li ><a href="#"><img src="images/link.gif" /></a></li>
</ul>
</div><!-- div id social -->
</div><!-- div id foot -->



                                                  <!--- footer close --->

</div><!-- id page -->
		
		

        
                                                 <!---page close--->
        
        
   


		
		                                         <!---main table close--->
		

</body>

            

















