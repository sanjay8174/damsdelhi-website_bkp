function isEMail(s)
{
       
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var booleanValue = emailPattern.test(s);
//     alert("validation"+booleanValue);
//return false;
    if (!booleanValue)
    {
        return true;
    } else {
        return false;
    }
}

function trim(str) {
    while (str.substring(0, 1) == ' ') {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length - 1, str.length) == ' ') {
        str = str.substring(0, str.length - 1);
    }
    return str;
}
function checkEmail(s) {
    var s = trim(s);
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var booleanValue = emailPattern.test(s);
    if (booleanValue) {
//    This will return i.e email is ok
    return true;
    } else {
//    Thjis will return i.e email in not ok
    return false;
    }
}

function Submit_enquiry()
{

    var name    = $("#name").val();
    var address = $("#address").val();
    var hospita = $("#hospital").val();
    var city    = $("#city").val();
    var mobile  = $("#mobile").val();
    var email   = $("#email").val();
    var message = $("#message").val();

    if($("#name").val()== "" || $("#name").val()== null)
    {
       $("#name").focus();
       $("#name").addClass('error_msg');
       $('#name').attr({'placeholder': 'Please Enter Your Name.'});
       $("#name").keypress(function (){$("#name").removeClass('error_msg')});
        return false;
    }

    else if($("#address").val()=="" || $("#address").val()==null)
    {
      
        $("#address").removeClass('error_msg');
        $("#address").addClass('error_msg');
        $("#address").focus();
        $('#address').attr({'placeholder': 'Please Enter Your Address.'});
        $("#address").keypress(function (){$("#address").removeClass('error_msg')});
        return false;
    }
    else if($("#hospital").val()==""||$("#hospital").val()==null)
    {
      
        $("#hospital").removeClass('error_msg');
        $("#hospital").addClass('error_msg');
        $("#hospital").focus();
        $('#hospital').attr({'placeholder': 'Please Enter Your Hospital Name.'});
        $("#hospital").keypress(function (){$("#hospital").removeClass('error_msg')});
        return false;
    }
     else  if($("#city").val()==""||$("#city").val()==null)
    {
        $("#city").removeClass('error_msg');
        $("#city").addClass('error_msg');
        $('#city').attr({'placeholder': 'Please Enter City.'});
        $("#city").keypress(function (){$("#city").removeClass('error_msg')});
        $("#city").focus();
        return false;
    }
    else if($("#mobile").val()=="" || $("#mobile").val()==null)
    {
        
        $("#mobile").removeClass('error_msg');
        $("#mobile").addClass('error_msg');
        $("#mobile").focus();
        $('#mobile').attr({'placeholder': 'Please Enter Your Mobile No.'});
        $("#mobile").keypress(function (){$("#mobile").removeClass('error_msg')});
        return false;
    }
    else  if(!IsValid($("#mobile").val()))
    {
      
        $("#mobile").addClass('error_msg');
        $("#mobile").focus();
        $("#mobile").val('');
        $('#mobile').attr({'placeholder': 'Please Enter Valid Mobile No.'});
        $("#mobile").keypress(function (){$("#mobile").removeClass('error_msg')});
        return false;
    }
    else if($("#email").val()==""|| $("#email").val()== null)
    {
      
        $("#email").removeClass('error_msg');
        $("#email").addClass('error_msg');
        $("#email").focus();
        $('#email').attr({'placeholder': 'Please Enter Your EmailId.'});
        $("#email").keypress(function (){
        $("#email").removeClass('error_msg');
        });
        return false;
    }
    else if ($('#email').val() != '' && !checkEmail($("#email").val())) {
        $("#email").removeClass('error_msg');
        $("#email").addClass('error_msg');
        $('#email').focus();
        $('#email').val('');
        $('#email').attr('placeholder', 'Please enter your valid email id.');
        $("#email").removeClass('error_msg');
        return false;
    } else if($("#message").val()=="" || $("#message").val()==null){
        
        $("#message").removeClass('error_msg');
        $("#message").addClass('error_msg');
        $("#message").focus();
        $('#message').attr({'placeholder': 'Please Enter Your Message.'});
        $("#message").keypress(function (){
            $("#message").removeClass('error_msg')});
        return false;
    }
   

    else
    {
   document.getElementById('formID').submit();
}
}


function IsValid(Text)//ckecking whether phone no is valid
{
    var ValidChars = "-0123456789";
    var IsNumber=true;
    var Char;
    for (var i = 0; i < Text.length && IsNumber == true; i++)
    {
        Char = Text.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            IsNumber = false;
        }
    }
    return IsNumber;

    }