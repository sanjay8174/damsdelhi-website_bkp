<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		



	<title>MammoScreen India</title>
<meta name="description" content="#">
<meta name="keywords" content="#">
<meta name="#" content="#">


		


	  
		<link rel="stylesheet" href="css/styles.css" media="all" type="text/css">
		

	
		<script language="JavaScript" src="js/code.js" type="text/javascript"></script>
		

		




	


		<script type="text/javascript">
		//<![CDATA[
		hs.registerOverlay({
			html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
			position: 'top right',
			fade: 2 // fading the semi-transparent overlay looks bad in IE
		});
		
		hs.graphicsDir = '';
		hs.wrapperClassName = 'borderless';
	    hs.showCredits = false;
		//]]>
		</script>
	
	<!-- Highslide - END -->
	

	







	</head>

	<body>



                                                   <!---main table start--->
                                                   


                                                     <!---page start--->

<div id="page">

                                                     <!---header start--->


<div id="header">

<div id="had_top">
<div id="menu">
<ul>
<li><a href="#">HOME</a></li>
<li><a href="#">CONTACT US</a></li>
<li><a href="#">SITE MAP</a></li>
</ul>
</div><!-- id menu -->
</div><!-- id had_top -->

<div id="logo">
<div id="logo_title">
<img src="images/title.gif" class="india_text" />
</div><!-- id logo_title -->
<a href="#"><img src="images/logo_mammo_screen.gif" alt="lets pink"></a>
</div><!-- id logo -->

<div class="search">

<input type="text" class="search_text" /></input>
<input src="images/search.gif" alt="Search" name="search_form" onclick="#" type="image" width="34" height="17"></input>

</div> <!-- id search -->                                                          

              
<div id="navi">
<ul>

<li>
<div class="navi_open"  >
<table>
<tbody><tr>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">Vision and Mission</div><!-- class navi_had -->
<p>To provide excellent health care service by obtaining high quality mammograms with their accurate  reports and archiving the same for future comparison, </p>
</td>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">Our Team</div><!-- class navi_had -->
<p>A dedicated team of health professionals from different subspecialties, but with same goal…. </p></td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">About Us</a>
</li>

<li>
<div class="navi_open">
<table>
<tbody><tr>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">Mammography</div><!-- class navi_had -->
<p>Mammography is the process of using low-energy-X-rays to examine the human breast</p>
</td>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">Concept of Screening</div><!-- class navi_had -->
<p>Worldwide breast cancer remains the commonest cancer to affect women.  </p>
</td>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="135" height="90">
<div class="navi_had">Indications</div><!-- class navi_had -->
<p>Every asymptomatic female between age group of 50-69 should undergo annual screening mammogram.  </p>
</td>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="135" height="90">
<div class="navi_had">Limitations</div><!-- class navi_had -->
<p>All diagnostic modalities have their limitations, so do mammograms! Few cancers in early stage may not be picked up on mammograms.  </p>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="images/abt_mammo.php">About Mammography</a>
</li>

<li>
<div class="navi_open" >
<table>
<tbody><tr>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">Quality Control </div><!-- class navi_had -->
<p>All staff intensively trained on regular basis by qualified professionals. </p></td>
<td onclick="#">
<img src="images/AS_Tsr.jpg" alt="" width="130" height="90">
<div class="navi_had">We Provide</div><!-- class navi_had -->
<p>Dedicated Breast care service-First of its kind in India.</p></td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">Why Choose Us</a>
</li>

<li>
<a href="#">News</a>
</li>

<li>
<a href="#">Blog</a>
</li>

<li>
<a href="#">Career</a>
</li>

<li>
<a href="#">Partner with Us</a>
</li>

<li>
<a href="#">Contact Us</a>
</li>

</ul>
</div><!-- id navi -->


</div><!-- id header -->
                              
	                                                <!--- header close --->
                                                    <!--- abt_mammo start --->
<div id="abt_mammo">

<div id="banner"></div><!-- id banner -->




<div id="bannerabtmammo">

<h1>About Mammography</h1>
<ul>

<li>What is Mammography ?</li>
<li>How it is performed ?</li>
<li>How should I prepare ?</li>
</ul>

</div><!-- id bannerabtmammo -->

                                                            <!--  enquiry_form_mammo start --->
<?php include 'enquiry.php'; ?>


<!--<div class="query_box" >
<h1>Enquiry Form</h1>

		
<ul>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Your Full Name') this.value='';" value="Your Full Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Full Address') this.value='';" value="Full Address" class="query_box-text1" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Hospital Name') this.value='';" value="Hospital Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Select City') this.value='';" value="Select City" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Telephone No.') this.value='';" value="Telephone No." class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='E-Mail Address') this.value='';" value="E-Mail Address" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Type Your Message') this.value='';" value="Type Your Message" class="query_box-text2" /></li>

<li><input name="" value="Submit" type="button" class="query-button" /></li>
</ul>

</div> class query_box -->


<div id="mammodetail">

<ul>
<li>
<h1>What is Mammography?</h1>
<p>Mammography is the process of using low-energy-X-rays to examine the human breast and is used as a diagnostic and a screening tool. The goal of mammography is the early detection of breast cancer, typically through detection of characteristic masses and/or microcalcifications.</p>
</li>
<li>
<h1>How it is performed?</h1>
<p>A mammography unit is a rectangular box that houses the tube in which x-rays are produced. The unit is used exclusively for x-ray exams of the breast, with special accessories that allow only the breast to be exposed to the x-rays. Attached to the unit is a device that holds and compresses the breast and positions it so images can be obtained at different angles.</p>
</li>
<li>
<h1>How should I prepare ?</h1>
<p>Do not wear deodorant, talcum powder or lotion under your arms or on your breasts on the day of the exam. These can appear on the mammogram as calcium spots. 
<br />
Describe any breast symptoms or problems to the technologist performing the exam. 
<br />
Women should always inform their physician or x-ray technologist if there is any possibility that they are pregnant.</p>
</li>
</ul>
</div>

<div><img src="images/contact.gif" />
</div>

</div><!-- id abt_mammo -->


                                                     <!--  enquiry_form_mammo close --->
                                                     <!--- abt_mammo close --->
                                                     <!--- footer start --->

<div id="footer_border"></div>
<div id="footer">
<table>
<tbody><tr>
<td>
<h6>About Us</h6>
<ul>
<li><a href="#">Vision and Mission</a></li>
<li><a href="#">Our Team</a></li>
</ul>
</td>
<td>
<h6>About Mammgraphy</h6>
<ul>
<li><a href="#">Mammgraphy</a></li>
<li><a href="#">Concept of screening</a></li>
<li><a href="#">Indications</a></li>
<li><a href="#">Limitations</a></li>
</ul>
</td>
<td>
<h6>Why Choose Us</h6>
<ul>
<li><a href="#">We Provide</a></li>
<li><a href="#">Quality Control </a></li>
</ul>
</td>
<td class="last">
<ul>
<li><a href="#">News</a></li>
<li><a href="#">Blog</a></li>
<li><a href="#">Career</a></li>
<li><a href="#">Partner with us</a></li>
<li><a href="#">Contact us</a></li>
</ul>
</td>
</tr>
</tbody></table>
</div><!-- id footer -->

<div id="foot">
<div class="foot_text">
<ul>
<li><a href="#">Contact Us</a></li>
<li><a href="#">Privacy</a></li>
<li><a href="#">Trademarks</a></li>
<li><a href="#">Site Map</a></li>
<li>Copyright © 2011 lets pink. All rights reserved.</li>
<li class="last"><a href="https://www.gingerwebs.com" target="_blank">Website Design by Ginger Webs</a></li>
</ul>
</div><!-- div class foot_text -->
<div class="social">
<ul >
<li ><a href="#"><img src="images/fb.gif" /></a></li>
<li ><a href="#"><img src="images/twitter.gif" /></a></li>
<li ><a href="#"><img src="images/link.gif" /></a></li>
</ul>
</div><!-- div id social -->
</div><!-- div id foot -->


                                                  <!--- footer close --->

</div><!-- id page -->
		
		

        
                                                 <!---page close--->
        
        
   


		
		                                         <!---main table close--->
		

</body>
