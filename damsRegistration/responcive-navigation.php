
<div class="r-screen-b">
  <div class="demo-box-d">
    <div class="r-navigation demo1-d" style="display:block; right: 0px;">
      <div class="r-navigation_button" id="menudrop"></div>
    </div>
    <div class="demo3-d">
      <div class="close-navigation demo1-d"> X </div>
      <ul>
        <div class="modify-close">Close <span class="demo1-d">X</span></div>
        <li><a href="index.php" title="Home">Home</a></li>
        <li><a href="javascript:void(0);" title="About" onclick="menuslides(1);"><span>&nbsp;</span>About Us</a>
          <ul class="inner-d-down display_none" id="inul1">
            <span class="respo-arrow">&nbsp;</span>
            <li><a href="dams.php" title="About DAMS">About DAMS</a></li>
            <li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
            <li><a href="about_director.php" title="About Director">About Director</a></li>
            <li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
            <li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
          </ul>
        </li>
        <li><a href="javascript:void(0);" title="Courses" onclick="menuslides(2);"><span>&nbsp;</span>Courses</a>
          <ul class="inner-d-down display_none" id="inul2">
            <span class="respo-arrow">&nbsp;</span>
            <li><a href="javascript:void(0);" title="MD / MS ENTRANCE" onclick="innermenuslides(1);"><span>&nbsp;</span>MD / MS ENTRANCE</a>
              <ul class="extra-inner" id="innrul1" style="display:none;">
                <li><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
                <li><a href="satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
                <li><a href="test-series.php" title="Test Series">Test Series</a></li>
                <li><a href="aipge_2014.php" title="Achievement">Achievement</a></li>
                <li><a href="photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
              </ul>
            </li>
            <li><a href="javascript:void(0);" title="MCI SCREENING" onclick="innermenuslides(2);"><span>&nbsp;</span>MCI SCREENING</a>
              <ul class="extra-inner display_none" id="innrul2">
                <li><a href="mci_screening.php" title="Face To Face Classes">Face To Face Classes</a></li>
                <li><a href="mci-satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
                <li><a href="mci-test-series.php" title="Test Series">Test Series</a></li>
                <li><a href="mciscreening_sep_2013.php" title="Achievement">Achievement</a></li>
                <li><a href="mci-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
              </ul>
            </li>
            <li><a href="javascript:void(0);" title="MDS QUEST" onclick="innermenuslides(3);"><span>&nbsp;</span>MDS QUEST</a>
              <ul class="extra-inner display_none" id="innrul3">
                <li><a href="dams-mds-quest.php" title="Face To Face Classes">Face To Face Classes</a></li>
                <li><a href="mds-satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
                <li><a href="dams-mds-test-series.php" title="Test Series">Test Series</a></li>
                <li><a href="mds-aipge-2014.php" title="Achievement">Achievement</a></li>
                <li><a href="mds-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
              </ul>
            </li>
            <li><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
          </ul>
        </li>
        <li><a href="javascript:void(0);" title="DAMS Store" onclick="menuslides(3);"><span>&nbsp;</span>DAMS Store </a>
          <ul class="inner-d-down display_none" id="inul3">
            <span class="respo-arrow">&nbsp;</span>
<!--            <li><a href="dams-publication.php?c=1" title="MD / MS ENTRANCE">MD / MS ENTRANCE</a></li>
            <li><a href="dams-publication.php?c=2" title="MCI SCREENING">MCI SCREENING</a></li>
            <li><a href="dams-publication.php?c=3" title="MDS QUEST">MDS QUEST</a></li>
            <li><a href="dams-publication.php?c=4" title="USMLE EDGE">USMLE EDGE</a></li>-->
            <li><a href="http://damspublications.com/" target="_blank" title="MD / MS ENTRANCE">MD / MS ENTRANCE</a></li>
            <li><a href="http://damspublications.com/" target="_blank" title="MCI SCREENING">MCI SCREENING</a></li>
            <li><a href="http://damspublications.com/" target="_blank" title="MDS QUEST">MDS QUEST</a></li>
            <li><a href="http://damspublications.com/" target="_blank" title="USMLE EDGE">USMLE EDGE</a></li>
          </ul>
        </li>
        <li><a href="javascript:void(0);" title="Find a center" onclick="menuslides(4);"><span>&nbsp;</span>Find a center</a>
          <ul class="inner-d-down display_none" id="inul4">
            <span class="respo-arrow">&nbsp;</span>
            <li><a href="find-center.php?c=1" title="FACE TO FACE CENTER">FACE TO FACE CENTER</a></li>
            <li><a href="find-center.php?c=2" title="SATELLITE CENTER">SATELLITE CENTER</a></li>
            <li><a href="find-center.php?c=3" title="TEST CENTER">TEST CENTER</a></li>
          </ul>
        </li>
        <li><a href="download.php" title="Download">Download</a></li>
        <li><a href="franchisee.php" title="Franchisee">Franchisee</a></li>
        <li><a href="career.php" title="Career">Career</a></li>
        <li><a title="Blog" href="https://blog.damsdelhi.com/">Blog</a></li>
        <li><a href="contact.php" title="Contact us">Contact us</a></li>
        <li>
          <div class="res-helpline-no"> <span class="res-cell">&nbsp;</span>
            <div style="width:60%; float:left; text-align:left;"> Helpline&nbsp;no: <span class="res-mini-no">011-40094009</span> </div>
          </div>
        </li>
        <li>
          <div class="responcive-sms-enquiry"> <span class="res-sms">&nbsp;</span>
            <div style="width:60%; float:left; text-align:left;"> SMS Enquiry: <span class="iner-res-sms">sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span> </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <div class="user-use">
    <div class="demo-box-d"><a href="javascript:void(0);" class="l-r-left demo-2-d" id="student-login" title="Online Test Student login"><span></span> <b style="font-weight:normal;">Log in</b></a></div>
    <div class="demo6-d">
      <div class="close-navigation demo-2-d">X</div>
      <ul>
        <div class="modify-close">Close <span class="demo-2-d">X</span></div>
        <li><a href="javascript:void(0);" title="Online/Offline Test Login" class="active-res" id="login1" onclick="loginslides(1);"><span>&nbsp;</span>Online/Offline Test Login</a>
          <div class="responcive-login-box" id="indiv1">
            <div class="responcive-login-content">
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="email" id="email" type="text" class="career-inp-1" placeholder="Roll No. / Email">
                </div>
                <span id="emailerror" style="display:none">Please enter your email address</span></div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="email" id="email" type="text" class="career-inp-1" placeholder="Password">
                </div>
                <span id="emailerror" style="display:none">Please enter your Password</span></div>
              <div class="responcive-right-ip-1">
                <div class="responcive-fgpassword"><a href="javascript:void(0);" id="fg-password" title="Forgot Password"> Forgot Password ? </a></div>
                <div class="responcive-submit-enquiry"><a href="#" title="Submit" onclick="Submit_form_newReg()"> Submit</a></div>
              </div>
            </div>
          </div>
        </li>
        <li><a href="javascript:void(0);" title="New Registration" id="login2" onclick="loginslides(2);"><span>&nbsp;</span>New Registration</a>
          <div class="responcive-login-box" id="indiv2" style="display:none;">
            <div class="responcive-login-content"> 
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="email" id="email" type="text" class="career-inp-1" placeholder="Email Address" />
                </div>
                <span id="emailerror" style="display:none">Please enter your email address</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="password" id="password" type="password" class="career-inp-1" placeholder="Password"/>
                </div>
                <span id="passworderror" style="display:none">Please enter your password</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="Cnpassword" ID="Cnpassword" type="password" class="career-inp-1" placeholder="Confirm Password"/>
                </div>
                <span id="Cnpassworderror" style="display:none">Please enter confirm password</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="Stuidentname" ID="Stuidentname" type="text" class="career-inp-1" placeholder="Name" />
                </div>
                <span id="Stuidentnameerror" style="display:none">Please enter your name</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <input name="mobile" id="mobile" type="text" class="career-inp-1" placeholder="Mobile Number" />
                </div>
                <span id="mobileerror" style="display:none">Please enter your email address</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <select name="course_Data" id="Course" class="career-select-input-1">
                    <option value="0">Select Course</option>
                    <option value="1">Select Course</option>
                    <option value="2">Select Course</option>
                  </select>
                </div>
                <span id="Courseerror" style="display:none">Please enter your email address</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <select name="state" id="state" class="career-select-input-1" onchange="stateChange(this.value)">
                    <option value="0">Select Course</option>
                    <option value="1">Select Course</option>
                    <option value="2">Select Course</option>
                  </select>
                </div>
                <span id="stateerror" style="display:none">Please enter your email address</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1" id="citydiv">
                  <select name="city" id="city" class="career-select-input-1">
                    <option value="0">Select City</option>
                  </select>
                </div>
                <span id="cityerror" style="display:none">Please enter your email address</span> </div>
              <div class="responcive-career-box">
                <div class="responcive-right-ip-1">
                  <div class="responcive-submit-enquiry"><a href="#" title="Submit" onclick="Submit_form_newReg()"> Submit</a></div>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li><a href="https://dams.sisonline.in/" title="Dams Cloud Login">Dams Cloud Login</a></li>
      </ul>
    </div>
  </div>
</div>
