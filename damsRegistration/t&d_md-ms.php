<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MD/MS Entrance Coaching Institute, Delhi, India, NEET PG</title>
<meta name="description" content=" DAMS- Delhi Academy of Medical Sciences provides coaching for PG Entrance, and MD/MS Entrance Exam in New Delhi, India." />
<meta name="keywords" content=" MD/MS entrance coaching institute, MD/MS Coaching Institute, MD Coaching Institute, MS Coaching Institute, Post Graduate Medical Coaching, Post Graduate Coaching, Medical Coaching, Medical Coaching India" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';

$course_id = 1;
$courseNav_id = 1;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 

<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Test &amp; Discussion" class="active-link">Test &amp; Discussion</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Test &amp; Discussion Course <span class="book-ur-seat-btn book-hide"><a href="online-registration.php" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <aside class="course-icons"> <img src="images/test-image.gif" title="Test Image" alt="Test Image" /> </aside>
              <aside class="course-detail">
                <p>DAMS is the pioneer MD/MS Entrance coaching  institute which started this course in Delhi. We are the first medical coaching institute in the country to start this course. This is a specialized course which is targeted at a group of students who have already done some studying on their part and are looking to upgrade themselves as well as sharpen their skills for MD/MS entrance examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET), AIIMS, PGI,  UPSC, DNB &amp;  MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET), AIIMS PG entrance, and PGI Chandigarh by our sincere effort. This course runs once a week and we have a 5-8 batches every year with a fresh batch every month starting January. In this course we have a fixed schedule of tests which is divided subject &amp; topic wise and given to you on the day one of enrolment. Every week you have a 1hour test on prescheduled topic, which you would have prepared in that week and followed by a brainstorming discussion by the famous DAMS faculty for around 4-5 hours. This solves your doubts and emphasizes on the key points which are essential for your success. In short taking this course ensures that your brain and neurons are stimulated with maximum inputs from your famous teachers and tricky questions. Another thing to note is that your so called "Guide books"  often are written by Interns or fresh medical graduates and have many mistakes. Our teachers being in this business for so long correct those and highlight the mistakes in your guide books and save you from being misguided. If you are looking for success in the MD/MS Entrance Exam we welcome you into our Test &amp; Discussion course, which features our  exclusive selected faculty and specially designed test papers. Designed to provide highly motivated students with opportunities to broaden and enrich their academic experience, this course is geared towards deriving the desired final outcome - a good score in MD/MS entrance tests and many toppers from previous years have been a product of this series by DAMS.</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Course Highlights</div>
                <ul class="benefits">
                  <li><span></span>Weekly tests-topic based followed by discussion by top Delhi faculty.</li>
                  <li><span></span>Monthly Grand Tests with All India PG pattern and All India Ranking.</li>
                  <li><span></span>Special sessions by last year toppers.</li>
                  <li><span></span>Total tests- Around 64 which includes 40subject wise tests (approx) +12 Grand tests</li>
                </ul>
              </aside>
            </article>
            <div class="book-ur-seat-btn"><a href="online-registration.php" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <div class="national-quiz-add"> <a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a> </div>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>