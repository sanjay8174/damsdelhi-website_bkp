<div class="recent-pg-main">
     <div class="span12">
          <div class="recent-pg-heading1">
               <a href="photo-gallery.php" class="photogal photogal_title"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a>
          </div>
          <div class="customNavigation">
               <a class="btn prev" href="javascript:void(0);"><img src="images/left.png" alt="Left Arrow" /></a>
               <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png" alt="Right Arrow" /></a>
          </div>
     </div>
   <div id="owl-demo" class="owl-carousel" style="margin-left:4px;"> 
         <?php $getRecentAchievement= $Dao->getRecentAchieve($course_id);
     for($i=0;$i<count($getRecentAchievement);$i++){

    ?>
          <div class="item">
              <img src="<?php echo "https://damsdelhi.com/damsCMS/files/".$course_id."/".$getRecentAchievement[$i][3]."/". $getRecentAchievement[$i][0];?>" alt="<?php echo $getRecentAchievement[$i][0];?>" title="<?php echo $getRecentAchievement[$i][0];?>"  style="width:147px;height:94px"/>
               <span class="_name"><?php echo $getRecentAchievement[$i][4];?></span>
               <span class="_rank"><?php if($getRecentAchievement[$i][1]!=''){?>Rank :<?php }else{?><?php }?> <strong><?php echo $getRecentAchievement[$i][1];?></strong></span>
               <span class="_field"><?php echo $getRecentAchievement[$i][2]." ".$getRecentAchievement[$i][5];?></span>
          </div>
   <?php }?>
     </div>
   
     <div class="view-all-photo"><a href="achievements.php?c=<?php echo $course_id; ?>" title="View All">View&nbsp;All</a></div>
</div>