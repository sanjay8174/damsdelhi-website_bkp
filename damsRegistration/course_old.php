<!DOCTYPE html>
<?php
$course_id = '1';
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/AIPG(NBE/NEET) Pattern) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<?php include 'md-ms-big-nav.php'; ?>
<aside class="banner-left banner-l-heading">
<h3>Trusted Partners for your PGME Preparation 
<span>With Highest Percentage Selection in Top Institutes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
<div class="wrapper">
<aside class="content-left">
<div class="course-box">
<ul class="idTabs responce-show"> 
<li><a href="#MDCourses">MD/MS Courses</a></li>
<li><a href="#DNB">DNB</a></li> 
</ul>

<div id="MDCourses">
<div class="satellite-content">
<div class="idams-box1"> 
<p>Things have changed in the recent years with new pattern in AIPG with NBE conducting the exam. In 2012, NEET was conducted by NBE and 2013 onwards AIPG will be conducted by NBE. This has resulted in gross change in the pattern of examination and DAMS is the only institute offering courses on the latest AIPG (NBE) pattern.  With our special offering like Classroom programs based on NEET PG,  Special classes on Visual (DAMS Visual Treat (DVT Sessions) capsule and online GT based on new pattern we are only trusted partner for  PGME examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG (NBE), NEET, AIIMS, PGI,  UPSC and DNB. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for NEET, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.<br><br>
With our presence across the country we are the current leaders in the business of <strong>PG Medical Entrance Coaching. We have the largest foot print across India allowing our students to compete with peers across India, and we also boost of largest consortium of faculty members ever ready to help and mentor young budding Doctors in achieving their dreams.</strong>
</p></div>
</div></div>

<div id="DNB">
<div class="satellite-content">
<div class="idams-box1"> 
<p>DNB has now become an important choice for medical students with various authorities recognizing the equivalence of DNB with other post graduate qualifications and centralised counselling. Most popular course amongst students with comprehensive coverage of ALL SUBJECTS is our REGULAR COURSE. We are the only medical coaching institute which teaches all subjects in manner that is required for your primary DNB examination. This course starts in Feburary and we have a fresh batch every month. Classes are held on weekends and are based on advanced audio-visual learning modules. This course further has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance Examination.</p>
</div>

<aside class="how-to-apply paddin-zero">
<div class="how-to-apply-heading"><span></span> Course Highlights</div>
<div class="course-detail-content">

<span class="gry-course-box">We admit only limited number of students in each batch. You will receive excellent individual attention. This is a big advantage to you as you can learn better in a small class with limited students. Our class rooms are air conditioned and have state of the art audiovisual facilities.
</span>

<span class="blue-course-box">You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can. They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books..</span>

<span class="gry-course-box">We provide detailed and easy to understand notes. </span>

<span class="blue-course-box">We conduct periodic tests, evaluate your performance and conduct counselling sessions. This will help you in your regular preparation for your exam.</span>
</div>
</aside>
</div>
</div>
</div>

<?php include 'md-ms-middle-accordion.php'; ?>

<div class="recent-pg-main">
     <div class="span12">
          <div class="recent-pg-heading1">
               <a href="photo-gallery.php" class="photogal photogal_title"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a>
          </div>
          <div class="customNavigation">
               <a class="btn prev" href="javascript:void(0);"><img src="images/left.png" alt="Left Arrow" /></a>
               <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png" alt="Right Arrow" /></a>
          </div>
     </div>
     <div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
          <div class="item">
               <img src="images/topper/1.jpg" alt="Topper" />
               <span class="_name">Dr. Saba Samad Memon</span>
               <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">AIPGE (2014)</span>
          </div>
          <div class="item">
               <img src="images/topper/2.jpg" alt="Topper" />
               <span class="_name">Dr. Mayank Agarwal</span>
               <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">ASSAM PG (2014)</span>
          </div>
          <div class="item">
               <img src="images/topper/3.jpg" alt="Topper" />
               <span class="_name">Dr. Archana Badala</span>
               <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">RAJASTHAN CET (2014)</span>
          </div>
          <div class="item">
               <img src="images/topper/4.jpg" alt="Topper" />
               <span class="_name">Dr. Rahul Baweja</span>
			   <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">DELHI PG (2014)</span>
          </div>
          <div class="item">
               <img src="images/topper/1-.jpg" alt="Topper" />
               <span class="_name">Dr. Saba Samad Memon</span>
               <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">MAHCET (2013-14)</span>
          </div>
          <div class="item">
               <img src="images/topper/5.jpg" alt="Topper" />
               <span class="_name">Dr. Tuhina Goel</span>
               <span class="_rank">Rank : <strong>1</strong></span>
               <span class="_field">AIIMS</span>
          </div>
          <div class="item">
               <img src="images/topper/4.jpg" alt="Topper" />
               <span class="_name">Dr. Rahul Baweja</span>
               <span class="_rank">Rank : <strong>2</strong></span>
               <span class="_field">DNB CET (2013)</span>
          </div>               
          <div class="item">
               <img src="images/topper/6.jpg" alt="Topper" />
               <span class="_name">Dr. Pooja jain</span>
               <span class="_rank"><strong>Rank 5</strong></span>
               <span class="_field">DNB CET (2013)</span>
          </div>               
     </div>
     <div class="view-all-photo"><a href="pgi_may_2014.php" title="View All">View&nbsp;All</a></div>
</div>
</aside>

<aside class="content-right">
<?php include 'md-ms-right-accordion.php'; ?>
<div class="national-quiz-add">
<a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a>
</div>
<?php
include 'openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>

<div class="news-update-box">
<div class="n-heading"><span></span> News &amp; Updates</div>
<div class="news-content-box">
<div style="width:100%; float:left; height:228px; overflow:hidden;">
     <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
                            <ul id="ul<?php echo $i;?>" <?php if($i == '0'){ ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
                                <li>
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                            </li>
<?php if ($newsDetail[$j] != '') { ?>
                                <li class="orange">
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                                </li>
<?php } ?>
<?php if ($newsDetail[$j] != '') { ?>
                                <li>
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                                </li>
                        <?php } ?>
                            </ul><?php
                        }
                        ?>
                        
</div>
</div>
                    <div class="box-bottom-1">
                        <span class="mini-view-right"></span>
                        <div class="mini-view-midle">
                            <a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
                            <div class="slider-mini-dot">
                                <ul>
<?php $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                                    <li id="u<?php echo $i; ?>" <?php if ($i == '0') { ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
<?php } ?>
                                </ul>
                            </div>
                        </div>
                        <span class="mini-view-left"></span>
                    </div>

</div>

<div class="news-update-box">
<div class="n-videos"><span></span> Students Interview</div>
<div class="videos-content-box">

<div id="vd0" class="display_block">
<iframe width="100%" height="236" src="//www.youtube.com/embed/Atat9dha9RI" class="border_none"></iframe>
</div>

<div id="vd1" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/HIP7wjjGuoM" class="border_none"></iframe>
</div>

<div id="vd2" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/L-AnijPFb-I" class="border_none"></iframe>
</div>

<div id="vd3" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/M7L2-zLb560" class="border_none"></iframe>
</div>

</div>

<div class="box-bottom-1">
<span class="mini-view-right"></span>
<div class="mini-view-midle">
<a href="aiims_nov_2013.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
<div class="slider-mini-dot"><ul>
<li id="v0" class="current" onClick="video('0');"></li>
<li id="v1" class="" onClick="video('1');"></li>
<li id="v2" class="" onClick="video('2');"></li>
<li id="v3" class="" onClick="video('3');"></li>
</ul></div>
</div>
<span class="mini-view-left"></span>
</div>
</div>
</aside>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>
