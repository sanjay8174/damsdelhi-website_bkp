<?php
class dao extends Database{

    public function getCourse(){
         $i=0;
         $sql = parent::executeQuery("select COURSE_ID,COURSE_NAME,URL_LINK,ACTIVE from COURSE ;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseArray[$i][0] = urldecode($rows['COURSE_ID']);
          $getCourseArray[$i][1] = urldecode($rows['COURSE_NAME']);
          $getCourseArray[$i][2] = urldecode($rows['URL_LINK']);
          $getCourseArray[$i][3] = urldecode($rows['ACTIVE']);


          $i++;
        }
        return $getCourseArray;
     }
     public function getCourseName($id){
         $i=0;
         $sql = parent::executeQuery("select COURSE_NAME,ACTIVE from COURSE where COURSE_ID=$id;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseArrayName[$i][0] = urldecode($rows['COURSE_ID']);
          $getCourseArrayName[$i][1] = urldecode($rows['COURSE_NAME']);

          $i++;
        }
        return $getCourseArrayName;
     }
     public function getLikeProducts($id){
         $i=0;
         $sql = parent::executeQuery("SELECT PRODUCT_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,COST,DISCOUNT_COST,COURSE_ID,AUTHOR,LANGUAGE,LENGTH,PUBLISHER,IMAGE,IMAGE_EXTENSION,LIKE_PRODUCT_IDS FROM PRODUCT WHERE PRODUCT_ID=$id");

        while ($rows = mysql_fetch_array($sql)){

          $getLikeProductArray[$i][0] = urldecode($rows['PRODUCT_ID']);
          $getLikeProductArray[$i][1] = urldecode($rows['PRODUCT_NAME']);
          $getLikeProductArray[$i][2] = urldecode($rows['IMAGE_EXTENSION']);
          $getLikeProductArray[$i][3] = urldecode($rows['COST']);
          $getLikeProductArray[$i][4] = urldecode($rows['DISCOUNT_COST']);
          $getLikeProductArray[$i][5] = urldecode($rows['AUTHOR']);
          $getLikeProductArray[$i][6] = urldecode($rows['LANGUAGE']);
          $i++;
        }
        return $getLikeProductArray;
     }


     public function getAboutus(){
         $i=0;
         $sql = parent::executeQuery("select ID,URL_LINK,ABOUT_US_NAME from ABOUT_US where ACTIVE=1;");

        while ($rows = mysql_fetch_array($sql)){

          $getAboutusArray[$i][0] = urldecode($rows['ID']);
          $getAboutusArray[$i][1] = urldecode($rows['URL_LINK']);
          $getAboutusArray[$i][2] = urldecode($rows['ABOUT_US_NAME']);

          $i++;
        }
        return $getAboutusArray;
     }
     public function  getAboutusContent($Id){
         $i=0;
         $sql = parent::executeQuery("select BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,CONTENT_HOME from ABOUT_US where ACTIVE=1 and ID=$Id;");

        while ($rows = mysql_fetch_array($sql)){

          $getAboutusContentArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getAboutusContentArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getAboutusContentArray[$i][2] = urldecode($rows['CONTENT_HOME']);

          $i++;
        }
        return $getAboutusContentArray;
     }

     public function getCourseNav($cId){
         $i=0;

         $sql = parent::executeQuery("select COURSE_NAVIGATION_ID,NAME,URL_LINK from COURSE_NAVIGATION where COURSE_ID=$cId;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseNavArray[$i][0] = urldecode($rows['COURSE_NAVIGATION_ID']);
          $getCourseNavArray[$i][1] = urldecode($rows['NAME']);
          $getCourseNavArray[$i][2] = urldecode($rows['URL_LINK']);


          $i++;
        }
        return $getCourseNavArray;
     }
     public function getCourseNavContent($navId){
     $i=0;
     $sql = parent::executeQuery("SELECT CN.COURSE_NAVIGATION_ID,CN.NAME,CN.URL_LINK,CN.COURSE_NAVIGATION_ORDER,CN.ACTIVE,CN.COURSE_ID,CN.BACKGROUND_IMAGE_UPLOAD,CN.CONTENT,CN.BACKGROUND_IMAGE_CONTENT,C.COURSE_NAME FROM COURSE_NAVIGATION CN LEFT JOIN COURSE C ON CN.COURSE_ID=C.COURSE_ID where COURSE_NAVIGATION_ID=$navId");
     while ($rows = mysql_fetch_array($sql)){

          $getContentNavArray[$i][0] = urldecode($rows['CONTENT']);
          $getContentNavArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getContentNavArray[$i][2] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $i++;
        }
        return $getContentNavArray;
     }
     public function getDamstoreContent(){
     $i=0;
     $sql = parent::executeQuery("SELECT * FROM  DAMSTORE_CONTENT");
     while ($rows = mysql_fetch_array($sql)){

          $getContentDamsstoreArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getContentDamsstoreArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getContentDamsstoreArray[$i][2] = urldecode($rows['ACTIVE']);
          $i++;
        }
        return $getContentDamsstoreArray;
     }
     public function getFindcenterContent(){
     $i=0;
     $sql = parent::executeQuery("SELECT * FROM  FIND_CENTER_CONTENT");
     while ($rows = mysql_fetch_array($sql)){

          $getContentFindcenterArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getContentFindcenterArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getContentFindcenterArray[$i][2] = urldecode($rows['ACTIVE']);
          $i++;
        }
        return $getContentFindcenterArray;
     }
     public function getVirtualtourContent($courseId){
     $i=0;
     $sql = parent::executeQuery("SELECT * FROM VIRTUAL_TOUR_CONTENT where COURSE_ID=$courseId");
     while ($rows = mysql_fetch_array($sql)){

          $getContentVirtualtourArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getContentVirtualtourArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getContentVirtualtourArray[$i][2] = urldecode($rows['ACTIVE']);
          $i++;
        }
        return $getContentVirtualtourArray;
     }

     public function getCategory($cId,$cNId){
         $i=0;

         $sql = parent::executeQuery("select CATEGORY_NAME,CATEGORY_ID FROM CATEGORY where ACTIVE=1 and COURSE_ID=$cId and COURSE_NAVIGATION_ID=$cNId ORDER BY CATEGORY_ORDER");

        while ($rows = mysql_fetch_array($sql)){

          $getCategoryArray[$i][0] = urldecode($rows['CATEGORY_NAME']);
          $getCategoryArray[$i][1] = urldecode($rows['CATEGORY_ID']);



          $i++;
        }
        return $getCategoryArray;
     }

     public function getCategory2($categoryId,$cId){
         $i=0;

         $sqlA = parent::executeQuery("select ID,NAME from CATEGORY2 where ACTIVE=1 and CATEGORY1_ID=$categoryId and COURSE_ID=$cId ORDER BY CATEGORY_ORDER");

        while($rowsA = mysql_fetch_object($sqlA)){

          $getCategory2Array[$i][0] = urldecode($rowsA->ID);
          $getCategory2Array[$i][1] = urldecode($rowsA->NAME);



          $i++;
        }
        return $getCategory2Array;
     }

     public function getSubCourse($categoryId2,$categoryId,$cId){
         $i=0;

         $sqlB = parent::executeQuery(" select SUB_COURSE_NAME,URL_LINK from SUB_COURSE where COURSE_ID=$cId and CATEGORY_ID=$categoryId and CATEGORY_ID2=$categoryId2 and ACTIVE=1");

        while($rowsB = mysql_fetch_object($sqlB)){

          $getSubCourseArray[$i][0] = urldecode($rowsB->SUB_COURSE_NAME);
          $getSubCourseArray[$i][1] = urldecode($rowsB->URL_LINK);



          $i++;
        }
        return $getSubCourseArray;
     }

     public function getNews($cId){
         $i=0;

         $sqlD = parent::executeQuery("SELECT HEADING FROM NEWS WHERE COURSE_ID=$cId AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");

        while($rows = mysql_fetch_object($sqlD)){

          $newsDetail[$i] = urldecode($rows->HEADING);

         $i++;
        }
        return $newsDetail;
     }

     public function getRecentAchieve($cId){
         $i=0;
         if($cId!=''){
            $query   = parent::executeQuery("select EVENT_ID from EVENT where COURSE_ID = $cId ORDER BY EVENT_ID DESC,YEAR DESC  LIMIT 0,1");  
            //$query   = parent::executeQuery("select EVENT_ID from EVENT where COURSE_ID = $cId ORDER BY EVENT_ORDER,YEAR DESC  LIMIT 0,1");  
            $result  = mysql_fetch_object($query);
            $EventId = $result->EVENT_ID;
        }
        if($EventId!='' && $EventId!=NULL){
            $sql = parent::executeQuery("Select F.FILE_NAME,F.NAME,F.RANK,E.EVENT_NAME,E.YEAR,F.EVENT_ID FROM FILE F LEFT JOIN EVENT E ON F.EVENT_ID = E.EVENT_ID WHERE F.EVENT_ID=($EventId) and F.ACTIVE=1 and FILE_NAME!='' AND FILE_NAME IS NOT NULL AND RANK!='' AND RANK IS NOT NULL  ORDER BY trim(lpad(RANK,4,0)) ASC LIMIT 0,8");
        }else{
            $sql = parent::executeQuery("select F.FILE_NAME,F.NAME,F.RANK,E.EVENT_NAME,F.EVENT_ID FROM FILE F LEFT JOIN EVENT E ON F.EVENT_ID = E.EVENT_ID WHERE F.COURSE_ID =$cId and FILE_NAME!='' AND FILE_NAME IS NOT NULL  ORDER BY FILE_ID DESC LIMIT 0,9 ");
        }
        //$sql = parent::executeQuery("select F.FILE_NAME,F.NAME,F.RANK,E.EVENT_NAME,F.EVENT_ID FROM FILE F LEFT JOIN EVENT E ON F.EVENT_ID = E.EVENT_ID WHERE F.COURSE_ID =$cId  ORDER BY RANK ASC LIMIT 0,8 ");

        while($rows = mysql_fetch_object($sql)){

          $getRecentAchieve[$i][0] = urldecode($rows->FILE_NAME);
          $getRecentAchieve[$i][1] = urldecode($rows->RANK);
          $getRecentAchieve[$i][2] = urldecode($rows->EVENT_NAME);
          $getRecentAchieve[$i][3] = urldecode($rows->EVENT_ID);
          $getRecentAchieve[$i][4] = urldecode($rows->NAME);
          $getRecentAchieve[$i][5] = urldecode($rows->YEAR);


         $i++;
        }
        return $getRecentAchieve;
     }






    public function getYear($id){
         $i=0;

         $sql = parent::executeQuery("SELECT distinct(YEAR),EVENT_NAME,EVENT_ID FROM EVENT where COURSE_ID=$id AND ACTIVE=1 GROUP BY YEAR ORDER BY YEAR DESC");

        while ($rows = mysql_fetch_array($sql)){

          $getYearArray[$i][0] = urldecode($rows['YEAR']);
          $getYearArray[$i][1] = urldecode($rows['EVENT_NAME']);
          $getYearArray[$i][2] = urldecode($rows['EVENT_ID']);

          $i++;
        }
        return $getYearArray;
     }

     public function getEvent($cId,$year){
         $i=0;
          $sql = parent::executeQuery("select * from EVENT where COURSE_ID = $cId AND ACTIVE=1 and YEAR = $year order by EVENT_ID DESC ");
        // $sql = parent::executeQuery("select * from EVENT where COURSE_ID = $cId AND ACTIVE=1 and YEAR = $year order by EVENT_ORDER ");

        while ($rows = mysql_fetch_array($sql)){

          $getEventArray[$i][0]= urldecode($rows['EVENT_NAME']);
          $getEventArray[$i][1] = urldecode($rows['EVENT_ID']);
          $getEventArray[$i][2] = urldecode($rows['YEAR']);
          $i++;
        }
        return $getEventArray;
     }

     public function getData($eventId){
        $i=0;$imgecount=0;
         $sql = parent::executeQuery("select NAME,RANK,COURSE_ID,FILE_NAME from FILE where EVENT_ID =$eventId AND ACTIVE=1 ORDER BY lpad(RANK,4,0) ASC");

        while ($rows = mysql_fetch_array($sql)){

          $getDataArray[$i][0]= urldecode($rows['NAME']);
          $getDataArray[$i][1] = urldecode($rows['RANK']);
          $getDataArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getDataArray[$i][3] = urldecode($rows['FILE_NAME']);
          if(urldecode($rows['FILE_NAME'])!='' && urldecode($rows['FILE_NAME'])!=null){
              $imgecount++;
          }
          $i++;
        }
        $getDataArray[0][4]= $imgecount;
        return $getDataArray;
     }

     public function getDataVideo($eventId){
         $i=0;

         $sql = parent::executeQuery("select NAME,RANK,COURSE_ID,URL_LINK,YOU_TUBE,FILE_NAME,VIDEO_ID from VIDEO where EVENT_ID =$eventId AND ACTIVE=1 ORDER BY lpad(RANK,4,0)");

        while ($rows = mysql_fetch_array($sql)){

          $getDataVideoArray[$i][0]= urldecode($rows['NAME']);
          $getDataVideoArray[$i][1] = urldecode($rows['RANK']);
          $getDataVideoArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getDataVideoArray[$i][3] = urldecode($rows['URL_LINK']);
          $getDataVideoArray[$i][4] = urldecode($rows['YOU_TUBE']);
          $getDataVideoArray[$i][5] = urldecode($rows['FILE_NAME']);
          $getDataVideoArray[$i][6] = urldecode($rows['VIDEO_ID']);

          $i++;
        }
        return $getDataVideoArray;
     }
     public function getIntVideo($cId){
         $i=0;

         $sql = parent::executeQuery("select NAME,EVENT_ID,COURSE_ID,URL_LINK,YOU_TUBE,FILE_NAME,VIDEO_ID from VIDEO where COURSE_ID =$cId order by VIDEO_ID desc limit 0,4");

        while ($rows = mysql_fetch_array($sql)){

          $getIntVideoArray[$i][0]= urldecode($rows['NAME']);
          $getIntVideoArray[$i][1] = urldecode($rows['EVENT_ID']);
          $getIntVideoArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getIntVideoArray[$i][3] = urldecode($rows['URL_LINK']);
          $getIntVideoArray[$i][4] = urldecode($rows['YOU_TUBE']);
          $getIntVideoArray[$i][5] = urldecode($rows['FILE_NAME']);
          $getIntVideoArray[$i][6] = urldecode($rows['VIDEO_ID']);

          $i++;
        }
        return $getIntVideoArray;
     }

     public function getNavId($cid){
         $i=0;

         $sql = parent::executeQuery("select COURSE_NAVIGATION_ID from COURSE_NAVIGATION where COURSE_ID=$cid and NAME like '%face%'");

      $rows = mysql_fetch_array($sql);


        return $rows[COURSE_NAVIGATION_ID];
     }
 public function getCourseHome(){
         $i=0;

         $sql = parent::executeQuery("select COURSE_ID,CONTENT_HOME from COURSE ;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseArray[$rows['COURSE_ID']] = urldecode($rows['CONTENT_HOME']);


        }
        return $getCourseArray;
     }

}
?>