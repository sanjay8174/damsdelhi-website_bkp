<!DOCTYPE html>
<?php
$course_id = '3';
$courseNav_id = 9;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="mds-quest-banner">
<?php include'mds-big-nav.php'; ?>
<aside class="banner-left">
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include'mds-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
<div class="wrapper">
<aside class="content-left">
<div class="course-box">
<h3>MDS Quest</h3>
<p>Delhi Academy of Medical Sciences (DAMS) is  a well-established name in the field of PG Medical Entrance Education for over 15 years now. Today under the leadership of Dr Sumer Sethi, Radiologist and previous  topper in AIPG/ AIIMS. DAMS is recognized for its impeccable foresight, enviable expertise and innate acumen. We are the number 1 coaching institute for the PG medical entrance examinations AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields led by Dr. Sumer Sethi.</p>
<p>Today we see a lack of proper guidance for BDS graduates who are preparing for MDS entrance and there is no focused institute in India which genuinely helps dental graduates in their quest for MDS. DAMS with its unique legacy promises to bring  much needed quality classroom programme for MDS entrance. We are confident that with our strong faculty we will make a difference in your career as we have done for medical doctors for the last 15 years.</p>
<p>DAMS was established to create a bench-mark institution to achieve excellence in the toughest competitive examination in the country, i.e. PG Medical Entrance Exam. Over a span of 15 years, DAMS has evolved into a unique fraternity of educators and students striving together, year after year, in pursuit of a single goal. With a passion to excel, the Delhi Academy of Medical Sciences has raged with the dynamism of a river which constantly renews itself and yet remains unchanged in its resolve to reach its ultimate destination.</p>
<p>The institute's aim is not only to provide specific knowledge and strengthen the foundation of the students in PG Medical Entrance, but also to infuse them with determination to crack the PG Entrance Exams .We help students realize their  potential to the fullest and  help them master the subject. We, at DAMS have developed extensive scientific teaching as well as testing methods. We also have special sessions on mental training required to develop the so called “killer instinct”.</p>
<div class="franchisee-box paddin-zero">
<p><span class="price_font">Dental Career Counselling for MDS :</span> 09999158131, 09999322168</p>
</div>
</div>

<?php /* include 'mds-middle-accordion.php';*/ ?>
<?php include 'middle-accordion.php'; ?>
<?php include 'recentAchievement.php'; ?>
 <!--   <div class="recent-pg-main">
           <div class="span12">
             <div class="recent-pg-heading1"><a href="mds-photo-gallery.php" class="photogal photogal_title"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
              <div class="customNavigation">
                <a class="btn prev" href="javascript:void(0);"><img src="images/left.png" alt="Left" /></a>
                <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png" alt="Right" /></a>
              </div>
            </div>

<div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
                <div class="item">
                      <img src="images/topper/8.jpg" title="Topper" alt="Topper" /> 
<span class="_name">Dr. Anusar Gupta</span>
<span class="_rank">Rank : <strong>37</strong></span>
<span class="_field">AIPGE (2014)</span>
                 </div>
                <div class="item">
                     <img src="images/topper/9.jpg" title="Topper" alt="Topper" /> 
<span class="_name">Dr. Chandan Jain </span>
<span class="_rank">Rank : <strong>2</strong></span>
<span class="_field">AIPGE (2014)</span>
                      </div>
                <div class="item">
                       <img src="images/topper/10.jpg" title="Topper" alt="Topper" /> 
<span class="_name">Dr. Khushboo Rathore</span>
<span class="_rank">Rank : <strong>98</strong></span>
<span class="_field">AIPGE (2014)</span>
                       </div>
                <div class="item">
                      <img src="images/topper/11.jpg" title="Topper" alt="Topper" /> 
<span class="_name">Dr. Prateek Punyani</span>
<span class="_rank">Rank : <strong>50</strong></span>
<span class="_field">AIPGE (2014)</span>
                       </div>               
              </div>
<div class="view-all-photo"><a href="mds-aipge-2014.php" title="View All">View&nbsp;All</a></div>
</div> -->
</aside>

<aside class="content-right">
<?php include 'right-accordion.php'; ?>
<?php /* include 'mds-right-accordion.php'; */ ?>
    
<?php include 'newsRight.php'; ?>
<?php include 'studentInterview.php'; ?>
<?php /*
include 'openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
} */
?>

<!--<div class="news-update-box">
<div class="n-heading"><span></span> News &amp; Updates</div>
<div class="news-content-box">

<div style="width:100%; float:left; height:228px; overflow:hidden;">
   <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
                            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
                                <li>
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                            </li>
                            <?php if ($newsDetail[$j] != '') { ?>
                                <li class="orange">
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                                </li>
                            <?php } ?>
                            <?php if ($newsDetail[$j] != '') { ?>
                                <li>
                                    <span></span>
                                    <p><?php echo $newsDetail[$j++]; ?></p>
                                </li>
                            <?php } ?>
                            </ul><?php
                        }
                        ?>

</div>
</div>
                    <div class="box-bottom-1">
                        <span class="mini-view-right"></span>
                        <div class="mini-view-midle">
                            <a href="mds-news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
                            <div class="slider-mini-dot">
                                <ul>
                                    <?php $i=0; for($i=0;$i<ceil($count/3);$i++){  ?>
                                        <li id="u<?php echo $i; ?>" <?php if($i=='0'){ ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count/3); ?>);"></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <span class="mini-view-left"></span>
                    </div>

</div>-->

<!--<div class="news-update-box">
<div class="n-videos"><span></span> Students Interview</div>
<div class="videos-content-box">

<div id="vd0" class="display_block">
<iframe width="100%" height="236" src="//www.youtube.com/embed/MKs-1DttP4c?wmode=transparent" class="boder-none"></iframe>
</div>

<div id="vd1" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/GUMZPGghqco?wmode=transparent" class="boder-none"></iframe>
</div>

<div id="vd2" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/xtAUAo5NKR8?wmode=transparent" class="boder-none"></iframe>
</div>

<div id="vd3" class="display_none">
<iframe width="100%" height="236" src="//www.youtube.com/embed/7X1ZGMKs0NQ?wmode=transparent" class="boder-none"></iframe>
</div>
</div>
<div class="box-bottom-1">
<span class="mini-view-right"></span>
<div class="mini-view-midle">
<a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
<div class="slider-mini-dot"><ul>
<li id="v0" class="current" onClick="video('0');"></li>
<li id="v1" class="" onClick="video('1');"></li>
<li id="v2" class="" onClick="video('2');"></li>
<li id="v3" class="" onClick="video('3');"></li>
</ul></div>
</div>
<span class="mini-view-left"></span>
</div>
</div>
</aside>

</div>-->
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
<?php mysql_close($myconn);?>
</body>
</html>
