<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>
<section class="video-container">
<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li class="border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_none" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');" class="light-blue-inner"><span id="accorsp4" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_block" id="aol4"> 
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>
</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>Delhi PG 2011</h4>
<section class="showme-main">
<ul class="idTabs idTabs1"> 
<li><a href="#jquery" class="selected" title="Delhi PG 2011">Delhi PG 2011</a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">
<ul class="main-students-list">
<li class="tp-border">
<div class="students-box border_left_none">
<img src="images/students/NIKHIL-NAIR.jpg" alt="Dr. NIKHIL NAIR" title="Dr. NIKHIL NAIR" />
<p><span>Dr. NIKHIL NAIR</span> Rank: 3</p>
</div>

<div class="students-box">
<img src="images/students/SAURABH.jpg" alt="Dr. SAURABH" title="Dr. SAURABH" />
<p><span>Dr. SAURABH</span> Rank: 3</p>
</div>

<div class="students-box">
<img src="images/students/SUMIT-SETHI.jpg" alt="Dr. SUMIT SETHI" title="Dr. SUMIT SETHI" />
<p><span>Dr. SUMIT SETHI</span> Rank: 5</p>
</div>

<div class="students-box">
<img src="images/students/NEHA-SAHAI.jpg" alt="Dr. NEHA SAHAI" title="Dr. NEHA SAHAI" />
<p><span>Dr. NEHA SAHAI</span> Rank: 5</p>
</div>
</li>

<li>
<div class="students-box border_left_none">
<img src="images/students/ABHILESH-S.jpg" alt="Dr. ABHILESH S" title="Dr. ABHILESH S" />
<p><span>Dr. ABHILESH S</span> Rank: 10</p>
</div>

<div class="students-box">
<img src="images/students/MILLO-ASHA.jpg" alt="Dr. MILLO ASHA" title="Dr. MILLO ASHA" />
<p><span>Dr. MILLO ASHA</span> Rank: 11</p>
</div>

<div class="students-box">
<img src="images/students/SHUBHAM-VATSYA.jpg" alt="Dr. SHUBHAM VATSYA" title="Dr. SHUBHAM VATSYA" />
<p><span>Dr. SHUBHAM VATSYA</span> Rank: 11</p>
</div>

<div class="students-box">
<img src="images/students/SURAJ-BANSAL.jpg" alt="Dr. SURAJ BANSAL" title="Dr. SURAJ BANSAL" />
<p><span>Dr. SURAJ BANSAL</span> Rank: 13</p>
</div>
</li>

<li>
<div class="students-box border_left_none">
<img src="images/students/NITASHA-AHIR.jpg" alt="Dr. NITASHA AHIR" title="Dr. NITASHA AHIR" />
<p><span>Dr. NITASHA AHIR</span> Rank: 16</p>
</div>

<div class="students-box">
<img src="images/students/RITU-VERMA.jpg" alt="Dr. RITU VERMA" title="Dr. RITU VERMA" />
<p><span>Dr. RITU VERMA</span> Rank: 16</p>
</div>

<div class="students-box"></div>
<div class="students-box"></div>
</li>
</ul>
<br style="clear:both;" />
<br style="clear:both;" />
<div class="schedule-mini-series">

<span class="mini-heading">Delhi PG - Result 2011</span>

<div class="schedule-mini-top">
<span class="one-part">Rank</span>
<span class="two-part">Student Name</span>
<span class="three-part">&nbsp;</span>
</div>
<div class="schedule-mini-content">
<ul>
<li class="border_none"><span class="one-parts">2</span>
<span class="two-parts schedule-left-line">Dr. MONIKA KAPOOR</span>
</li>
<li><span class="one-parts">5</span>
<span class="two-parts schedule-left-line">Dr. JYOTI</span>
</li>
<li><span class="one-parts">6</span>
<span class="two-parts schedule-left-line">Dr. RAINY AGGARWAL</span>
</li>
<li><span class="one-parts">8</span>
<span class="two-parts schedule-left-line">Dr. SAKSHI KHURANA</span>
</li>
<li><span class="one-parts">9</span>
<span class="two-parts schedule-left-line">Dr. NIKHIL VERMA , Dr. SWATI S</span>
</li>
<li><span class="one-parts">11</span>
<span class="two-parts schedule-left-line">Dr. JASMINE</span>
</li>
<li><span class="one-parts">16</span>
<span class="two-parts schedule-left-line">Dr. RITU VERMA</span>
</li>
<li><span class="one-parts">18</span>
<span class="two-parts schedule-left-line">Dr. PRIYANKA THAKUR</span>
</li>
<li><span class="one-parts">19</span>
<span class="two-parts schedule-left-line">Dr. ANKIT CHAWLA</span>
</li>
<li><span class="one-parts">20</span>
<span class="two-parts schedule-left-line">Dr. PALLAVI PRASAD , Dr. SNEHLATA MEENA , Dr. APARESH MANDAL</span>
</li>
<li><span class="one-parts">22</span>
<span class="two-parts schedule-left-line">Dr. NISHA YADAV , Dr. SMRITI SINGH</span>
</li>	
<li><span class="one-parts">23</span>
<span class="two-parts schedule-left-line">Dr. SOUMYA AGARWAL</span>
</li>	
<li><span class="one-parts">24</span>
<span class="two-parts schedule-left-line">Dr. APELI SUKHALU</span>
</li>
<li><span class="one-parts">27</span>
<span class="two-parts schedule-left-line">Dr. RASHMI PASSEY , Dr. DEEKSHA SINGH , Dr. RAJEEV RANJAN</span>
</li>
<li><span class="one-parts">28</span>
<span class="two-parts schedule-left-line">Dr. NITIN KUMAR</span>
</li>
<li><span class="one-parts">31</span>
<span class="two-parts schedule-left-line">Dr. RUCHIKA KUMAR , Dr. ANKITA DEY</span>
</li>	
<li><span class="one-parts">32</span>
<span class="two-parts schedule-left-line">Dr. RUCHI GUPTA</span>
</li>
<li><span class="one-parts">34</span>
<span class="two-parts schedule-left-line">Dr. ADITI , Dr. SAPNA BALOUT , Dr. CHANDRA MOHAN</span>
</li>
<li><span class="one-parts">35</span>
<span class="two-parts schedule-left-line">Dr. RICHA MITTAL</span>
</li>
<li><span class="one-parts">37</span>
<span class="two-parts schedule-left-line">Dr. MOHIT SETHI</span>
</li>
<li><span class="one-parts">38</span>
<span class="two-parts schedule-left-line">Dr. AVERLICIA PASSAH</span>
</li>
<li><span class="one-parts">40</span>
<span class="two-parts schedule-left-line">Dr. ANISH GUPTA , Dr. SARTHAK MALIK</span>
</li>
<li><span class="one-parts">44</span>
<span class="two-parts schedule-left-line">Dr. RHYTHM AHUJA , Dr. VILI SWN , Dr. REENA RANI , Dr. HIMANI BHASIN</span>
</li>
<li><span class="one-parts">49</span>
<span class="two-parts schedule-left-line">Dr. NEELAM</span>
</li>
<li><span class="one-parts">53</span>
<span class="two-parts schedule-left-line">Dr. BRAHMITA MONGA</span>
</li>
<li><span class="one-parts">54</span>
<span class="two-parts schedule-left-line">Dr. JASMINE JAISWAR , Dr. REETA MEENA , Dr. ARUN GARG , Dr. SHILPI GUPTA</span>
</li>
<li><span class="one-parts">56</span>
<span class="two-parts schedule-left-line">Dr. POORNIMA SEN</span>
</li>
<li><span class="one-parts">57</span>
<span class="two-parts schedule-left-line">Dr. SUKRIT SUD</span>
</li>
<li><span class="one-parts">58</span>
<span class="two-parts schedule-left-line">Dr. LAXMIN LATA MEENA</span>
</li>
<li><span class="one-parts">59</span>
<span class="two-parts schedule-left-line">Dr. APARNA ARYA</span>
</li>
<li><span class="one-parts">60</span>
<span class="two-parts schedule-left-line">Dr. YASHIKA NEGI</span>
</li>
<li><span class="one-parts">72</span>
<span class="two-parts schedule-left-line">Dr. ANSHITA AGGARWAL</span>
</li>
<li><span class="one-parts">75</span>
<span class="two-parts schedule-left-line">Dr. SOURABH GUPTA</span>
</li>
<li><span class="one-parts">76</span>
<span class="two-parts schedule-left-line">Dr. RAAJIT CHANANA , Dr. GANESH KUMAR , Dr. ANIKA GUPTA</span>
</li>
<li><span class="one-parts">82</span>
<span class="two-parts schedule-left-line">Dr. RUBEENA YOUSUF</span>
</li>
<li><span class="one-parts">83</span>
<span class="two-parts schedule-left-line">Dr. PRACHI</span>
</li>
<li><span class="one-parts">87</span>
<span class="two-parts schedule-left-line">Dr. ISHWAR SINGH</span>
</li>
<li><span class="one-parts">90</span>
<span class="two-parts schedule-left-line">Dr. UPASANA JELIA</span>
</li>
<li><span class="one-parts">95</span>
<span class="two-parts schedule-left-line">Dr. PANKURI MITTAL</span>
</li>
<li><span class="one-parts">100</span>
<span class="two-parts schedule-left-line">Dr. SMRITI NAGPAL</span>
</li>
<li><span class="one-parts">&nbsp;</span>
<span class="two-parts schedule-left-line" style="color:#000;">And many more...</span>
</li>
</ul>
</div>
</div>
</article></div>

<div id="official"> 
<article class="interview-photos-section"> 
<div class="achievment-videos-section">
<ul>
<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
<p>Rank: 1st AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
<p>Rank: 56th AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>

<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="boder-none"></iframe> 
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
<p>Rank: 6th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
<p>Rank: 24th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>
</ul>
</div>
</article> 
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>