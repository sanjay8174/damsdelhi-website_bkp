<?php $forgetMsg=$_REQUEST['forgetMsg']; ?>
<input type="hidden" value="<?php echo $forgetMsg; ?>" id="forgetMsg" />
<div class="top-header">
<ul class="top-nav">
<li><a href="https://cbt.damsdelhi.com/" target="_blank" class="link">DAMS-CBT</a></li>
<!--<li><a href="https://mdscbt.damsdelhi.com/" target="_blank" class="link">DAMS MDS-CBT</a></li>-->
<li><a  href="https://dvt.damsdelhi.com/" target="_blank" class="link">DAMS-DVT</a></li>
<li><a href="studentInfo.php" title="Student Info">Student Info</a></li>
 <li><a href="contact.php" title="Contact us">Contact us</a></li>
<li><a href="franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="https://blog.damsdelhi.com/" title="Blog">Blog</a></li>
<li><a href="career.php" title="Career">Career</a></li>
<li><a href="download.php" title="Download">Download</a></li>
<li class="bg_none"><a href='https://damsdelhi.com' title="Home">Home</a></li>
</ul>
</div>
<div id="dialog" style="display: none;">

    <div class="res_pop_bg"></div>
    <div class="res_main_pop">
        <div class="res_main_main">
            <form id="loginForm" class="form" name="loginForm" action="https://onlinetest.damsdelhi.com/index.php?pageName=submit" method="post" onsubmit="return loginSubmit()">
                <div class="head_title">
                    <span onclick="showhide()"></span>
                    <div class="pop_title" id="pop_title">Login Test Series</div>
                    <div id="error"></div>
           </div>
           <p>

               <input class="student-login-text" name="email" id="emailLogin" placeholder="Roll No. / Email " type="text">
               <span id="loginemailerror" style="display:none">Please enter your email address</span>
           </p>
            <p>

                <input name="pass" id="passLogin" placeholder="Password" type="password">
                <span id="loginpasserror" style="display:none">Please enter your email address</span>
            </p>
            <div class="right-ip-1 right-submit">
                <div class="submit-enquiry">
                    <input value="Submit" title="Submit" type="submit">
                </div>
                <div class="fgpassword">Forgot Password? <a href="javascript:void(0)" onclick="show_Forgot()" title="Forgot Password"><b>Click Here</b></a></div></div>

        </form>

    </div>
    </div>
</div>
<!--forget password popup end-->
<div id="dialog_forgot" style="display: none">

    <div class="res_pop_bg"></div>
    <div class="res_main_pop">

        <form action="https://onlinetest.damsdelhi.com/index.php?pageName=fgpassword" method="post" name="forgotPassword"  id="forgotPassword" class="form" onsubmit="return forgetPassword()">
            <span onclick="showhide_Forgot()" ></span>

            <div  class="pop_title"id="forgeterror" style="color:red;margin: 30px 0 0;display: none;font-size: 12px;">ddddddd</div>
            <div class="pop_title" id="pop_title">Forgot Password</div>
            <div id="error"></div>
            <p>
                    <input type="hidden" value="1" name="web" />
                <!--<input id="forgotPasswordEmail" class="student-login-text" placeholder="Email Address" type="text" name="forgotPasswordEmail">-->
                 <input name="emaiID" id="emailF" type="text" class="student-login-text" placeholder="Email Address" />
            </p>

            <div class="right-ip-1 right-submit">
                <div class="submit-enquiry">
                    <input value="Submit" title="Submit" type="submit">
                </div></div>

        </form>


    </div>
</div>
<script>
function loginSubmit() {
    if ($("#emailLogin").val() == "") {
        $('#loginError').html("Please enter your email address or roll no.");
        $('#loginError').show();
        $("#emailLogin").focus();
        $("#emailLogin").addClass('error_msg');
        return false;
    }
    else if ($("#passLogin").val() == "") {
        $('#loginError').html("Please enter the password");
        $('#loginError').show();
        $("#passLogin").focus();
        $("#passLogin").addClass('error_msg');
        return false;
    }
    return true;
}
function forgetPassword() {
    if ($("#emailF").val() == "") {
        $('#forgetError').html("Please enter your email address");
        $('#forgetError').show();
        $('#forgetError').fadeOut(10000);
        $("#emailF").focus();
        $("#emailF").addClass('error_msg');
        return false;
    }
    else if (isEMail($("#emailF").val())) {
        $('#forgetError').html("Please enter valid email address");
        $('#forgetError').show();
        $('#forgetError').fadeOut(10000);
        $("#emailF").focus()
        $("#emailF").addClass('error_msg');
        return false;
    }
    return true;
}
function isEMail(s) {
    if (((s.indexOf("@")) == -1) || ((s.indexOf(".")) == -1))
    {
        return true;
    } else {
        return false;
    }
}
$(document).ready(function(){ 
if($('#forgetMsg').val()=='2'){alert("Your User Id and Password has been sent to your Email Id.");}
});
</script>
