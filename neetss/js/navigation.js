// JavaScript Document

Menu = {}
//++ Init
Menu.init = function(){
cuppa.button3D(".menu .button", ".state1", ".state2");
cuppa.button3D(".menu2 .button", ".state1", ".state2", null, "vertical");
cuppa.managerURL.updateLinks(".menu .link, .menu2 .link");
cuppa.managerURL.addListener(Menu.urlChange, true);
// Menu inital params
TweenMax.to(".menu2", 0, { left:-jQuery(".menu2").width(), display:"none" });
TweenMax.to(".menu", 0, { left:-jQuery(".menu").width(), display:"none" });
Menu.changeMenu(true, 0);
// Events
jQuery(".menu .button, .menu2 .button").bind("mouseenter", Menu.rollOver);
}; jQuery(document).ready(Menu.init);
//--
//++ UrlChange
Menu.urlChange = function(){
jQuery(".menu2 .content .button").removeClass("selected");
jQuery(".menu2 .content .separator").removeClass("selected");
jQuery(".menu2 .content .button_"+cuppa.managerURL.path_array[1]).addClass("selected");
jQuery(".menu2 .content .separator_"+cuppa.managerURL.path_array[1]).addClass("selected");
}
//--
//++ Change Menu
Menu.changeMenu = function(value, duration){
TweenMax.killTweensOf(Menu.changeMenu)
if(value == undefined) value = true;
if(duration == undefined) duration = 0.4;
if(value){
TweenMax.to(".menu2", 0.4, {delay:0.2, right:0, display:"block", ease:Cubic.easeOut });
}else{
TweenMax.to(".menu2", 0.4, { right:-jQuery(".menu2").width(), display:"none", ease:Cubic.easeIn });
TweenMax.to(".menu", 0.4, { delay:0.3, right:0, display:"block", ease:Cubic.easeOut });
}
}
//--
//++ RollOVer
Menu.rollOver = function(){
Stage.soundManager.stop("button");
Stage.soundManager.play("button");
}
//--
