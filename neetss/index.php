<?php
error_reporting(0);

?>
<?php $msglog=$_REQUEST['msglog'];
$forgetMsg=$_REQUEST['forgetMsg'];
$msg=$_REQUEST['msg'];
?>
<!DOCTYPE html>
<input type="hidden" value="<?php echo $msglog; ?>" id="msglog" />
<input type="hidden" value="<?php echo $forgetMsg; ?>" id="forgetMsg" />
<input type="hidden" value="<?php echo $msg; ?>" id="msg" />
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<meta name=description content=" DAMS-Delhi Academy of Medical Sciences is one of the best PG medical entrance coaching institute provides coaching for PG Entrance, and MD/MS coaching in New Delhi, India." />
<meta name=keywords content=" PG Medical entrance coaching institute, MD Coaching Institute, MS Coaching Institute, Post Graduate Medical Coaching, Post Graduate Coaching, Medical Coaching, Medical Coaching India, Medical Coaching Delhi, Medical Coaching Mumbai, All India PG Medical Entrance, AIIMS PG Medical Entrance, State PG Medical Entrance" />
<title>NEET SS,  SUPER SPECIALITY, DM/MCH Entrance</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href='css/style.css?v=1.4' rel=stylesheet type=text/css />
<link href='css/owl.carousel.css' rel=stylesheet type=text/css />
<link href='css/responcive_css.css' rel=stylesheet type=text/css />
<script src='js/jquery-3.3.1.js'></script>
<script src='js/owl.carousel.js'></script>

<style>
div#preloader { position: fixed; 
                left: 0; top: 0; z-index: 999; 
                width: 100%; 
                height: 100%; 
                overflow: visible; 
                background: transparent url('images/dams.gif') no-repeat center center; 
}
</style>
</head>
<body class="inner-bg">
    <?php include 'header.php'; ?>
    <section class="sld_section" >
<div class="demo-centering">

    <!--    <div id="sliderB" class="slider">
            <div>
            <a href="" target="_blank"><img src="https://s3.ap-south-1.amazonaws.com/dams-banner/bannerImage/73.png"></a>

        </div>
        <div>
            <a href="" target="_blank"><img src="https://s3.ap-south-1.amazonaws.com/dams-banner/bannerImage/73.png"></a>

        </div>




         </div>-->
    <div class="img_sec">
        <div>
            <img style="width:100%;float:left;" src="images/banner-new1.png" title="niitss banner" title="niitss banner for PG Examination">

    </div>





</div>
</div>

</div>
 </section>


    <section class="inner-midle-content">
        <div class="shadow"></div>
        <div class="wrapper main_wrapper_neetss">

            <div class="course-box course-box-neetss" style="width: 100%;">
                <h3>NEET SUPERSPECIALITY ENTRANCE<label class="book_ur_span"><span class="book-ur-seat-btn">
                            <!--<a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Buy Now</a></span>-->
                    <span class="book-ur-seat-btn new-btn">
                        <a href="images/NEET-SS-Brochure-New-Size.pdf" target="_blank" >NEET SS Brochures </a>
                        <a title="Login" href="javascript:void(0);" onclick="show()"> <span>&nbsp;</span> Login</a></span></label></h3>

                <div id="content-a" style="display: block;">
                    <span class="course_start">DM/ MCh entrance</span>
                    <div class="satellite-content" style="width:100%;box-sizing: border-box;padding: 1% 2% 0 2%;">
                        <aside class="how-to-apply paddin-zero">

                            <div class="course-detail-content right-vd">
                         <span class="blue-course-box">DAMS now offers the course for superspeciality aspirants. This is also a part of DAMS vision to be a one stop center for all your needs as a medical student.
                             <br><br> Now we have common entrance exam for NEET DM AND MCh entrance examination which is a computer based online test.
                             <br><br>  The ex-DAMS students who have been helped by DAMS for getting post-graduation seat, will now be offered quality education to get thru with super-specialization entrance also.
                             <br><br> A team of expert teachers from their respective fields including endocrinologists, urologists, cardiologists, pulmonologists, onco-surgeons and other specialists will be taking lectures in their respective fields in the famous DAMS test and discussion format.</span>
                     </div>
                            <div class="video-part">
                          <div class="item">
                              <span style="font-size:18px;" onclick="tprsvdeosw(&quot;NEET SS DM & MCH entrance Courses 2019 &quot;, &quot;https://www.youtube.com/embed/JtiNG6VYx-g?rel=0&amp;autoplay=0&quot;)">    <img src="images/neet.png"> 
                                <div class="coverbg"><div class="playicon sprite_parent"></div></div>
                            </span>
                        </div>
                            </div>
                 </aside>




                        <aside class="how-to-apply paddin-zero">
                            <div class="how-to-apply-heading how-to-apply-heading1" style="color:#fe7a42;/*background: url(../images/how-heading.jpg) left bottom no-repeat;*/background: none;text-align: center;"> <h3 class="course-feature-h">Online Test & Discussion Courses</h3></div>
                           <div class="course-detail-content ">
                                <div class="course-left-sec head-pro">
                                    <h3>DM Entrance</h3>
                                    <span class="blue-course-box blue-course-feature">
                                        <ul class="course-new-list feature_neetss">
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Cardiology</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Endocrinology</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Pulmonology</li>
                                        <li><span class="sub-arrow"></span>Neurology</li>
                                        <li><span class="sub-arrow"></span>Nephrology</li>


                                    </ul>
                                    </span>
                                </div>
                                <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>MCH Entrance</h3>
                                    <span class="blue-course-box course-feature-middle">
                                        <ul class="course-new-list feature_neetss">
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Urology</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Neurosurgery</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Oncosurgery</li>
                                        <li><span class="sub-arrow"></span>Surgical gastroenterology</li>
                                        </ul>
                                    </span>
                                 </div>
                                 <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM oncology Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow" style="padding:0px 3px;"></span>MD Radiation Oncology</li>
                                        </ul>
                                    </span>
                                 </div>
                            </div>
                            <div class="course-detail-content">
                                <div class="course-left-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box blue-course-feature new-dami-cls">
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow" style="padding:0px 3px;"></span>For MD Pediatrics</li>
                                    </ul>
                                    </span>
                                </div>
                                <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>MCH gynecology Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>For MD (Obstetrics & Gynecology)
Student</li>
                                        </ul>
                                    </span>
                                 </div>
                                <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>DM Entrance common course (T & D)</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">
                                        <p style="color: #505050;">DM Entrance (Common)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Cardiology</li>
                                        <li><span class="sub-arrow"></span>Neurology</li>
                                        <li><span class="sub-arrow"></span>Gastroenterology</li>
                                        <li><span class="sub-arrow"></span>Pulmonology</li>
                                        <li><span class="sub-arrow"></span>Endocrinology</li>
                                        <li><span class="sub-arrow"></span>Nephrology</li>
                                        </ul>
                                    </span>
                                 </div>
                            </div>
                                
                              <div class="course-detail-content">  
                                  
                                
                                 <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS Cardiology
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Heart failure</li>
                                        <li><span class="sub-arrow"></span>Hypertension</li>
                                        <li><span class="sub-arrow"></span>IHD</li>
                                        <li><span class="sub-arrow"></span>Arrythmias</li>
                                        <li><span class="sub-arrow"></span>Valvular & Congenital
Heart Disease</li>
                                        <li><span class="sub-arrow"></span>Miscellaneous</li>
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 100 MCQ video discussion of
important topics by cardiology faculty from
braunwald's textbook.</p>
                                           
                                        </div>
                                    </span>
                                 </div>
                                  
                                
                                  <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">Endocrinology NEET SS
dM Online Crash course</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Hypothalamus
& Pituitary</li>
                                        <li><span class="sub-arrow"></span>Adrenal Gland</li>
                                        <li><span class="sub-arrow"></span>Reproductive
Endocrinology</li>
                                        <li><span class="sub-arrow"></span>Calcium Phosphate Metabolism,
Parathyroid, Osteoporosis</li>
                                        <li><span class="sub-arrow"></span>Thyroid & Diabetes</li>
                                        
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 100 Mcq Video Discussion of
Imp Topics by Endocrinology Faculty From
Willam’s Textbook</p>
                                            <!--<ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>-->
                                        </div>
                                    </span>
                                 </div>
                                  <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS Pulmonology
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Applied Physiology and
Diagnosis of Respirataory
Disorders</li>
                                        <li><span class="sub-arrow"></span>Pulmonary Critical Care and
Acid Base Balance</li>
                                        <li><span class="sub-arrow"></span>Infections of Lung Including
Tuberculosis and Bronchiectasis</li>
                                        <li><span class="sub-arrow"></span>Obstructive and Restrictive
Diseases of Lung With Lung
Cancer and Sleep Medicine</li>
                                        
                                        
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 100 Mcq Video Discussion of
Important Topics by Pulmonology Faculty
From Fishman's Textbook.</p>
                                            <!--<ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>-->
                                        </div>
                                    </span>
                                 </div>
                              </div>
                                <div class="course-detail-content">  
                                
                                  
                                  
                                  
                             
                              

                                <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS DM Neurology
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Nerve, Muscle and Neuromuscular junction
disorders</li>
                                        <li><span class="sub-arrow"></span>Cerebrovascular diseases</li>
                                        <li><span class="sub-arrow"></span>Delirium and dementias</li>
                                        <li><span class="sub-arrow"></span>Demyelinating diseases and other disorders
of spinal cord</li>
                                        <li><span class="sub-arrow"></span>Seizures and Headache</li>
                                        <li><span class="sub-arrow"></span>Parkinson’s disease and other movement
disorders</li>
                                         <li><span class="sub-arrow"></span>Miscellaneous</li>
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 60 Q MOCK test on
neurology with explanations.</p>
                                            <!--<ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>-->
                                        </div>
                                    </span>
                                 </div>
                       
                       <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS Nephrology
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Renal Physiology</li>
                                        <li><span class="sub-arrow"></span>Acute Kidney Injury</li>
                                        <li><span class="sub-arrow"></span>CKD</li>
                                        <li><span class="sub-arrow"></span>Glomerular Diseases</li>
                                        <li><span class="sub-arrow"></span>Renal Replacement Therapy</li>
                                        <li><span class="sub-arrow"></span>Miscellaneous</li>
                                        
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 60 MCQs with detailed
explanation from Fehally’s textbook
of NEPHROLOGY</p>
                                            <!--<ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>-->
                                        </div>
                                    </span>
                                 </div>
                                    
                                     <div class="course-left-sec course-middle-sec head-pro">
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS MD Pediatric
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Neonatology</li>
                                        <li><span class="sub-arrow"></span>Pediatric cardiology</li>
                                        <li><span class="sub-arrow"></span>Growth/ development/ pubertal
disorders/ immunization</li>
                                        <li><span class="sub-arrow"></span>Genetics/ storage disorders/ inborn errors of metabolism</li>
                                        <li><span class="sub-arrow"></span>Pediatric Nephrology/ Neurology/
gastroenterology / endocrinology</li>
                                        <li><span class="sub-arrow"></span>Miscellaneous (pediatric surgery,
child psychiatry and image based
questions)</li>
                                        
                                        </ul>

                                    </span>
                                 </div>
                       </div>
                                  
                            <div class="course-detail-content" >        
                               
                                
                                <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS MD Medical oncology
(Online Crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Basics of chemotherapy</li>
                                        <li><span class="sub-arrow"></span>Basics of Radiation Oncology</li>
                                        <li><span class="sub-arrow"></span>Solid Tumours - Part 1</li>
                                        <li><span class="sub-arrow"></span>Solid Tumours - Part 2</li>
                                        <li><span class="sub-arrow"></span>Hematology and Lymphoma</li>
                                        <li><span class="sub-arrow"></span>Medicine and General Oncology</li>
                                       
                                        
                                        </ul>
                                        <div>
                                            <p style="color: #505050;font-weight: 600;">Followed by 60 Mcq discussion
of important topics from
Devita’s text book of Oncology</p>
                                            <!--<ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>-->
                                        </div>
                                    </span>
                                 </div>
                       
                       
                       
                                <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS (Online
Test & Discussion course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Urology</li>
                                        <li><span class="sub-arrow"></span>GI Surgery</li>
                                        <li><span class="sub-arrow"></span>Oncosurgery</li>
                                        <li><span class="sub-arrow"></span>Neurosurgery</li>
                                        <li><span class="sub-arrow"></span>General Surgery</li>
                                       
                                        
                                        </ul>
                                    </span>
                                 </div>
                       <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">MCH (GI Surgery)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Upper GI</li>
                                        <li><span class="sub-arrow"></span>Lower GI</li>
                                        <li><span class="sub-arrow"></span>HPB and Transplant</li>
                                        </ul>
										 <div>
                                            <p style="color: #505050;font-weight: 600;">100 Mcq Video Discussion of Important Topics
by Gastro Surgeon Faculty, from Blumgarts
& Shackelford Textbook</p>
                                        </div>
                                    </span>
                                 </div>
        
					<!--third row-->			
                    <div class="course-detail-content">       
                            <div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">neurosurgery MCh NEET SS
(Online Crash course)</p>
										<p>Video Lectures</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Spine</li>
                                        <li><span class="sub-arrow"></span>Vascular</li>
                                        <li><span class="sub-arrow"></span>Tumor</li>
                                        <li><span class="sub-arrow"></span>Trauma</li>
                                        <li><span class="sub-arrow"></span>Malformations</li>
                                        </ul>
										 <div>
                                            <p style="color: #505050;font-weight: 600;">Followed By 100 Mcq Discussion by
Neurosurgery Faculty of Important Topics from
Greenberg and Youman, with video Explanation.</p>
                                        </div>
                                    </span>
                                 </div>
                            
						
						<div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEEt SS MCH Urology
(online crash course)</p>
										<p>Video Lectures</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>General Urology Andrology,
Kidney, Ureter</li>
                                        <li><span class="sub-arrow"></span>Prostate & Pediatric Urology</li>
                                        <li><span class="sub-arrow"></span>Lower Urinary Tract</li>
                                        </ul>
										 <div>
                                            <p style="color: #505050;font-weight: 600;">100 Mcq Orology For NEET SS With Video
Discussion by Faculty of Important Topics</p>
                                        </div>
                                    </span>
                                 </div>
						
						<div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEEt SS MCH Oncosurgery
(online crash course )</p>
										<p>Video Lectures</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>GIT</li>
                                        <li><span class="sub-arrow"></span>Hepatobiliary</li>
                                        <li><span class="sub-arrow"></span>Oral Cavity</li>
                                        <li><span class="sub-arrow"></span>Breast</li>
                                        <li><span class="sub-arrow"></span>Urology</li>
                                        <li><span class="sub-arrow"></span>Miscellaneous</li>
                                        </ul>
										 <div>
                                            <p style="color: #505050;font-weight: 600;">Followed By 100 Mcq Video Discussion
by Faculty of Important Topics from
Devita's Book</p>
                                        </div>
                                    </span>
                                 </div>
                          </div>
								
								<div class="course-detail-content">
									<div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>MCH Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEEt SS MCH GYNAECOLOGY ONCOLOGY
(online crash course )</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Ovary, Cervix,
Endometrium</li>
                                        <li><span class="sub-arrow"></span>Vulva, Vagina, GTN</li>
                                        <li><span class="sub-arrow"></span>200 basic pattern MCQ
discussion on Obstetrics
and Gynaecology</li>
                                        </ul>
                                    </span>
                                 </div>
									
									<div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>DM Entrance Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">NEET SS DM (Hematopathology)
(online crash course)</p>
										<p>Modules:</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>Hematological techniques, staining
											methods, morphology of peripheral
											smear and bone marrow, cytochemistry
											of blood cells</li>
                                        <li><span class="sub-arrow"></span>Flow cytometry, molecular and
cytogenetic analysis, diagnostic
radio-isotopes</li>
                                        <li><span class="sub-arrow"></span>Thalassemias, Hemoglobin variants,
and other hemolytic anemias</li>
											
											 <li><span class="sub-arrow"></span>Myeloproliferative Neoplasms including
AML and MDS</li>
											<li><span class="sub-arrow"></span>Lymphoid Neoplasms (HL, NHL, ALL)</li>
                                        </ul>
                                    </span>
                                 </div>
									
									
									<div class="course-left-sec course-middle-sec head-pro" >
                                    <h3>NEET SS 2018 video Course</h3>
                                    <span class="blue-course-box course-feature-middle new-dami-cls">

                                        <p style="color: #505050;">paper discussion
(Neet ss 2018) video courses</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li><span class="sub-arrow"></span>General Surgery</li>
                                        <li><span class="sub-arrow"></span>Urology 2018</li>
                                        <li><span class="sub-arrow"></span>GI Surgery 2018</li>
											
											 <li><span class="sub-arrow"></span>Oncosurgery 2018</li>
                                        </ul>
                                    </span>
                                 </div>
								</div>
								
								<div class="course-detail-content">
                                 <div class="sec_cont" style="float:left;margin-left:12px;">
                                    <div class="enquery_form">
                                        <div class="form_detail head-pro" >
                                        <div> <h3 class="inr_title"><i class="spritehome_new form"></i>Quick Enquiry Form</h3></div>
                                        <div class="enquiry_content_main">
                                            <div class="enquiry_content">
                                                
                                                <form id="quickEnquiryForm" name="quickEnquiryForm" method="post" action="#" onsubmit="return validateQuickEnquiry()">
<!--                                                    <input type="hidden" name="mode" id="mode" value="quickEnquiry" />
                                                    <input type="hidden" name="redirectUrl" value="http://localhost/damswebsite_v1.2/neetss/" />-->
                                                
                                                    <div class="enquiry_content_more">
                                                        <div id="preloader" style="display: none;"></div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry1" name="userName" class="career_inp_1" value="First Name" onfocus="if(this.value=='First Name')this.value=''" onblur="if(this.value=='')this.value='Your Name'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry7" name="userName" class="career_inp_1" value="Last Name" onfocus="if(this.value=='Last Name')this.value=''" onblur="if(this.value=='')this.value='Last Name'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry2" name="userEmail" class="career_inp_1" value="Email Address" onfocus="if(this.value=='Email Address')this.value=''" onblur="if(this.value=='')this.value='Email Address'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry3" name="userMobile" class="career_inp_1" value="Phone Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onfocus="if(this.value=='Phone Number')this.value=''" onblur="if(this.value=='')this.value='Phone Number'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <select style="width: 100%;" name="userCourse" class="career_select_input_1" id="quickEnquiry4">
                                                                <option value="">Select Course</option>
                                                                <option>
                                                                    DRTP</option>
                                                                <option>
                                                                    MD/MS ENTRANCE</option>
                                                                <option>
                                                                    MCI SCREENING</option>
                                                                <option>
                                                                    MDS QUEST</option>
                                                                <option>
                                                                    USMLE EDGE</option>
                                                                <option>
                                                                    MRCP</option>
                                                                <option>
                                                                    MRCOG</option>
                                                                <option>
                                                                    PLAB</option>
                                                                <option>
                                                                    MRCGP</option>
                                                                <option>
                                                                    BLS/ACLS</option>
                                                                <option>
                                                                    DENTISTRY</option>
                                                                <!--added by Priyanka Sharma 5/jan/2018-->
                                                                 <option>
                                                                    Neet SS</option>
                                                            </select></div></div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <select style="width: 100%;" name="userCentre" class="career_select_input_1" id="quickEnquiry5">
                                                                <option value="">Centre Interested</option>
                                                                <option>Ahmedabad</option><option>Amritsar</option>
                                                                <option>Aurangabad</option><option>Bengaluru</option><option>Bhopal</option><option>Bhuvneshwar</option><option>Chandigarh</option>
                                                                <option>Chennai</option><option>Guwahati</option><option>Hyderabad</option><option>Indore</option><option>
                                                                    Jaipur</option><option>Jodhpur</option><option>Kanpur </option><option>Kolkata</option><option>Lucknow</option><option>
                                                                    Mangalore</option><option>Manipal</option><option>Nagpur</option><option>Patna</option><option>Pune</option>
                                                                <option>Raipur</option><option>Rohtak</option><option>Surat</option><option>Udaipur</option><option>Vadodara</option>
                                                                <option>Vijayawada</option><option>Agartala</option><option>Bathinda</option><option>Belgaum</option><option>Bikaner</option>
                                                                <option>Bilaspur</option><option>Davangere</option><option>Dhule</option><option>Gorakhpur</option><option>Haldwani</option><option>
                                                                    Hyderabad</option><option>Kota</option><option>Latur</option><option>Lucknow</option><option>Ludhiana</option><option>
                                                                    Mangalore</option><option>Miraj</option><option>MumbaiPune</option><option>Rohtak</option><option>
                                                                    Vijayawada</option><option>Vishakapatnam</option><option>Agroha</option><option>Aligarh</option><option>Allahabad</option><option>
                                                                    Amravati</option><option>Berhampur</option><option>Bikaner</option><option>Cuttack</option><option>Warangal</option><option>
                                                                    Dhule</option><option>Faridkot</option><option>Goa</option><option>Gwalior</option><option>Kakinada</option><option>
                                                                    Karad</option><option>Karim Nagar </option><option>Khamam</option><option>Kolhapur</option><option>Kurnool</option>
                                                                <option>Latur</option><option>Loni</option><option>Ludhiana</option><option>Meerut</option><option>Miraj</option><option>
                                                                    Patiala</option><option>Solapur</option><option>Talegaon</option><option>Tirupati</option><option>Varanasi</option><option>
                                                                    Vishakhapatnam</option><option>Tanda</option><option>Maysore</option><option>Agra</option><option>Bagalkot</option><option>Hubli</option><option>
                                                                    Dehradun</option><option>Shimla</option><option>New Delhi (Karol Bagh)</option><option>New Delhi (Gautam Nagar)</option><option>Nasik</option>
                                                                <option>Jammu</option><option>Agroha</option><option>Dibrugrah</option><option>Gulbarga</option><option>Aligarh</option><option>
                                                                    Manipal</option><option>Cochin</option><option>Ahemadabad</option><option>Aurangabad</option><option>
                                                                    Bareilly</option><option>Chennai</option><option>Cochin</option><option>Dehradun</option><option>Saifai</option><option>
                                                                    Jaipur</option><option>Jagdal Pur</option><option>Jabalpur</option><option>Kanpur</option><option>Kohlapur</option>
                                                                <option>Kolkata</option><option>Mysore</option><option>Meerut</option><option>Pondicherry</option><option>Ranchi</option><option>
                                                                    Mumbai</option><option>JALANDHAR</option><option>RABINDRANATH FOUNDATION</option><option>Srinagar J&amp;K</option><option>
                                                                    Tamilnadu</option><option>Siliguri</option><option>Solapur</option><option>Ahemadnagar </option><option>mphal</option><option>
                                                                    Ajmer</option><option>Satara</option><option>Srinagar UK</option><option>DAMS (Delhi Accademy of Medical Sciences Pvt.Ltd.)</option><option>
                                                                    Silchar</option><option>DAMS Jorhat</option><option>Nanded</option><option>Rewa</option>
                                                            </select></div></div>
                                                    <div class="right-ip-1">
                                                        <textarea style="width: 100%;box-sizing: border-box;resize: none;" id="quickEnquiry6" class="career-inp-1" placeholder="Write your Query here" rows="1" name="userMessage"></textarea>
                                                    </div>
                                                    <div class="error_msg">
                                                       <div class="error_msg_box" id="quickEnquiryerror" style="display:none">
                                                        </div>
                                                                            
                                                    </div>
                                                    <div class="career_box1" >
                                                        <div class="right_ip_1">
                                                            <div class="career_box"><div class="right_ip_1">
                                                                    <div class="submit_enquiry" style="margin-bottom:0px;margin-top:0px;"><a href="javascript:void(0)" title="Submit" onclick="validateQuickEnquiry()"> SUBMIT</a></div>

                                                                            
                                                                </div></div></div></div>
                                                    <div class="enquiry_bottom"></div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                              
                              
                          </div>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            <div class="course-detail-content" style="display:none;">
                                <div class="course-left-sec ">
                                    <h3>DM Course</h3>
                                    <span class="blue-course-box blue-course-feature">

                                        <p style="color: #505050;">5 Subject wise tests (100 Q each) followed by extensive video discussion of all important topics by subject experts in :</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>Neurology</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>‎Cardiology</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>‎Gastroenterology</li>
                                        <li ><span class="sub-arrow"></span>‎‎Pulmonology and critical care</li>
                                        <li ><span class="sub-arrow"></span>‎‎Endocrinology</li>


                                    </ul>
                                    <div>
                                        <p style="color: #505050;font-weight: 600;">Also includes<sup style="color:red;">*</sup></p>
                                        <ul>
                                            <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                            <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                        </ul>
                                    </div>
                                    </span>
                                </div>
                                <div class="course-left-sec course-middle-sec">
                                    <h3>MCh Course</h3>
                                    <span class="blue-course-box course-feature-middle">

                                        <p style="color: #505050;">5 Subject wise tests (100 Q each) followed by extensive video discussion of all important topics by subject experts in :</p>
                                        <ul class="course-new-list feature_neetss">
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>General Surgery</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>‎GI surgery</li>
                                        <li style="line-height: 28px;"><span class="sub-arrow"></span>‎GU surgery</li>
                                        <li ><span class="sub-arrow"></span>‎Neurosurgery</li>
                                        <li ><span class="sub-arrow"></span>‎Oncosurgery</li>

                                        </ul>
                                        <div >
                                            <p style="color: #505050;font-weight: 600;">Also includes<sup style="color:red;">*</sup></p>
                                            <ul>
                                                <li style="color: #505050;font-weight: 600;">5 Mock Tests of NEET SS PATTERN</li>
                                                <li style="color: #505050;font-weight: 600;">Tips to solve MCQ AND DISCUSSION OF IMPORTANT TOPICS</li>

                                            </ul>
                                        </div>
                                    </span>
                                 </div>
                                 <div class="sec_cont">
                                    <div class="enquery_form">
                                    <div class="form_detail">
                                        <div> <h3 class="inr_title"><i class="spritehome_new form"></i>Quick Enquiry Form</h3></div>
                                        <div class="enquiry_content_main">
                                            <div class="enquiry_content">
                                                
                                                <form id="quickEnquiryForm" name="quickEnquiryForm" method="post" action="#" onsubmit="return validateQuickEnquiry()">
<!--                                                    <input type="hidden" name="mode" id="mode" value="quickEnquiry" />
                                                    <input type="hidden" name="redirectUrl" value="<?php $url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url;?>" />-->
                                                
                                                    <div class="enquiry_content_more">
                                                        <div id="preloader" style="display: none;"></div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry1" name="userName" class="career_inp_1" value="First Name" onfocus="if(this.value=='First Name')this.value=''" onblur="if(this.value=='')this.value='Your Name'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry7" name="userName" class="career_inp_1" value="Last Name" onfocus="if(this.value=='Last Name')this.value=''" onblur="if(this.value=='')this.value='Last Name'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry2" name="userEmail" class="career_inp_1" value="Email Address" onfocus="if(this.value=='Email Address')this.value=''" onblur="if(this.value=='')this.value='Email Address'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <input id="quickEnquiry3" name="userMobile" class="career_inp_1" value="Phone Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onfocus="if(this.value=='Phone Number')this.value=''" onblur="if(this.value=='')this.value='Phone Number'" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <select style="width: 100%;" name="userCourse" class="career_select_input_1" id="quickEnquiry4">
                                                                <option value="">Select Course</option>
                                                                <option>
                                                                    DRTP</option>
                                                                <option>
                                                                    MD/MS ENTRANCE</option>
                                                                <option>
                                                                    MCI SCREENING</option>
                                                                <option>
                                                                    MDS QUEST</option>
                                                                <option>
                                                                    USMLE EDGE</option>
                                                                <option>
                                                                    MRCP</option>
                                                                <option>
                                                                    MRCOG</option>
                                                                <option>
                                                                    PLAB</option>
                                                                <option>
                                                                    MRCGP</option>
                                                                <option>
                                                                    BLS/ACLS</option>
                                                                <option>
                                                                    DENTISTRY</option>
                                                                <!--added by Priyanka Sharma 5/jan/2018-->
                                                                 <option>
                                                                    Neet SS</option>
                                                                      <option>
                                                                    MEDICAL-IAS</option>
                                                            </select></div></div>
                                                    <div class="career_box">
                                                        <div class="right_ip_1">
                                                            <select style="width: 100%;" name="userCentre" class="career_select_input_1" id="quickEnquiry5">
                                                                <option value="">Centre Interested</option>
                                                                <option>Ahmedabad</option><option>Amritsar</option>
                                                                <option>Aurangabad</option><option>Bengaluru</option><option>Bhopal</option><option>Bhuvneshwar</option><option>Chandigarh</option>
                                                                <option>Chennai</option><option>Guwahati</option><option>Hyderabad</option><option>Indore</option><option>
                                                                    Jaipur</option><option>Jodhpur</option><option>Kanpur </option><option>Kolkata</option><option>Lucknow</option><option>
                                                                    Mangalore</option><option>Manipal</option><option>Nagpur</option><option>Patna</option><option>Pune</option>
                                                                <option>Raipur</option><option>Rohtak</option><option>Surat</option><option>Udaipur</option><option>Vadodara</option>
                                                                <option>Vijayawada</option><option>Agartala</option><option>Bathinda</option><option>Belgaum</option><option>Bikaner</option>
                                                                <option>Bilaspur</option><option>Davangere</option><option>Dhule</option><option>Gorakhpur</option><option>Haldwani</option><option>
                                                                    Hyderabad</option><option>Kota</option><option>Latur</option><option>Lucknow</option><option>Ludhiana</option><option>
                                                                    Mangalore</option><option>Miraj</option><option>MumbaiPune</option><option>Rohtak</option><option>
                                                                    Vijayawada</option><option>Vishakapatnam</option><option>Agroha</option><option>Aligarh</option><option>Allahabad</option><option>
                                                                    Amravati</option><option>Berhampur</option><option>Bikaner</option><option>Cuttack</option><option>Warangal</option><option>
                                                                    Dhule</option><option>Faridkot</option><option>Goa</option><option>Gwalior</option><option>Kakinada</option><option>
                                                                    Karad</option><option>Karim Nagar </option><option>Khamam</option><option>Kolhapur</option><option>Kurnool</option>
                                                                <option>Latur</option><option>Loni</option><option>Ludhiana</option><option>Meerut</option><option>Miraj</option><option>
                                                                    Patiala</option><option>Solapur</option><option>Talegaon</option><option>Tirupati</option><option>Varanasi</option><option>
                                                                    Vishakhapatnam</option><option>Tanda</option><option>Maysore</option><option>Agra</option><option>Bagalkot</option><option>Hubli</option><option>
                                                                    Dehradun</option><option>Shimla</option><option>New Delhi (Karol Bagh)</option><option>New Delhi (Gautam Nagar)</option><option>Nasik</option>
                                                                <option>Jammu</option><option>Agroha</option><option>Dibrugrah</option><option>Gulbarga</option><option>Aligarh</option><option>
                                                                    Manipal</option><option>Cochin</option><option>Ahemadabad</option><option>Aurangabad</option><option>
                                                                    Bareilly</option><option>Chennai</option><option>Cochin</option><option>Dehradun</option><option>Saifai</option><option>
                                                                    Jaipur</option><option>Jagdal Pur</option><option>Jabalpur</option><option>Kanpur</option><option>Kohlapur</option>
                                                                <option>Kolkata</option><option>Mysore</option><option>Meerut</option><option>Pondicherry</option><option>Ranchi</option><option>
                                                                    Mumbai</option><option>JALANDHAR</option><option>RABINDRANATH FOUNDATION</option><option>Srinagar J&amp;K</option><option>
                                                                    Tamilnadu</option><option>Siliguri</option><option>Solapur</option><option>Ahemadnagar </option><option>mphal</option><option>
                                                                    Ajmer</option><option>Satara</option><option>Srinagar UK</option><option>DAMS (Delhi Accademy of Medical Sciences Pvt.Ltd.)</option><option>
                                                                    Silchar</option><option>DAMS Jorhat</option><option>Nanded</option><option>Rewa</option>
                                                            </select></div></div>
                                                    <div class="right-ip-1">
                                                        <textarea style="width: 100%;box-sizing: border-box;resize: none;" id="quickEnquiry6" class="career-inp-1" placeholder="Write your Query here" rows="1" name="userMessage" style="width: 318px; height: 26px;"></textarea>
                                                    </div>
                                                    <div class="error_msg">
                                                       <div class="error_msg_box" id="quickEnquiryerror" style="display:none">
                                                        </div>
                                                                            
                                                    </div>
                                                    <div class="career_box1">
                                                        <div class="right_ip_1">
                                                            <div class="career_box"><div class="right_ip_1">
                                                                    <div class="submit_enquiry"><a href="javascript:void(0)" title="Submit" onclick="validateQuickEnquiry()"> SUBMIT</a></div>

                                                                            
                                                                </div></div></div></div>
                                                    <div class="enquiry_bottom"></div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>



                <span class="course_start" >Course will be available from January 2018. <br/>
These courses are available on eMedicoz app </span>
                <div style="clear:both;"></div>
                <div class="video_cont">
                    <h1>Toppers Talk</h1>
                    <div id="owl-videoOne" class="owl-carousel carousel_parent">
						<div class="item">
                            <span onclick='tprsvdeosw("Aiims Urology Entrance Topper RANK-5 2019 Dr.Nikita Shrivastava", "https://www.youtube.com/embed/dr57RaEkrzA?rel=0&autoplay=0")'>    
                                <img src="images/new-thum.png">
                                <div class="coverbg"><div class="playicon sprite_parent"></div></div>
                            </span>
                        </div>
                        <div class="item">
                            <span onclick='tprsvdeosw("NEETSS 2018 Mch Oncosurgery Dr Chirag Bhirud TOPPER RANK-29", "https://www.youtube.com/embed/ILbXxYbA8nI?rel=0&autoplay=0")'>    <img src="images/video-3.png"> 
                                <div class="coverbg"><div class="playicon sprite_parent"></div></div>
                            </span>
                        </div>
                        <div class="item">
                            <span onclick='tprsvdeosw("NEETSS 2018 TOPPER RANK-23 Dr. Mehul Baldha", "https://www.youtube.com/embed/zpB1T-5jaNg?rel=0&autoplay=0")'><img src="images/video-2.png">  
                                <div class="coverbg"><div class="playicon sprite_parent"></div></div>
                            </span>
                        </div>
                        <div class="item">
                            <span onclick='tprsvdeosw("NEETSS 2018 TOPPER RANK-7 Dr. Akash Panigrahi", "https://www.youtube.com/embed/t87dPETZrVQ?rel=0&autoplay=0")'>    
                                <img src="images/video-1.png">
                                <div class="coverbg"><div class="playicon sprite_parent"></div></div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <?php include 'footer.php'; ?>
    
    
<div id="gettouchform" class="getin_touchpop" style="display:none;">
    <div class="mob_show"></div>
    <div id="gtformhid" class="parent_video_sec">
        <!--<span class="popclose sprite_image" onClick="gettouch_hide1()"></span>-->
        <p class="popupCancel" onClick="gettouch_hide1()"><span class="close thick"></span></p>
        <div class="getpophdr">
            <h1 id="head"></h1>
        </div>
        <div class="rgt_pg_cnct">
            <iframe width="100%" src="" frameborder="0" id="video" allowfullscreen></iframe>
        </div>
    </div>
</div>


</body>



<script>
function show(){
        $("#dialog").css("display" , "block");

        $(".res_main_pop").removeClass("pop_hide");
        }
function show_Forgot(){

        $("#dialog_forgot").css("display" , "block");
         $("#dialog").css("display" , "none");
        $(".res_main_pop").removeClass("pop_hide");
        }
function showhide(){
        $(".res_main_pop").addClass("pop_hide");
        setTimeout(function(){
            $("#dialog").css("display" , "none");
        }, 300);

        }
function showhide_Forgot(){
        $(".res_main_pop").addClass("pop_hide");
        setTimeout(function(){
            $("#dialog_forgot").css("display" , "none");
        }, 300);

        }
    
     
$(document).ready(function(){
    $('#owl-videoOne').owlCarousel({
            responsiveClass:true,
            loop:true,
            autoPlay:true,
            items:3,
            margin:10,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:1,
                    nav:false
                },
                1200:{
                    items:3,
                    nav:true
                }
            }
        })
    });
    
function tprsvdeosw(head, htm) {
        $("#gettouchform").css("display", "block");
        $("#video").attr("src", htm);
        $("#head").html(head);
        $(".parent_video_sec").removeClass("pop_hide");
        $(".parent_video_sec").addClass("pop_show")
        var scrollPos = $("#gettouchform").offset().top;
        $(window).scrollTop(scrollPos);
    }
function gettouch_hide1() {
        var playVdo = $("#video").attr('src');

        $('#video').attr('src', playVdo);
        $(".parent_video_sec").addClass("pop_hide");
        setTimeout(function () {
            $("#gettouchform").css("display", "none")
        }, 300);

    }
    
</script>

<script type="text/javascript" src="js/jquery.excoloSlider.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript">
           $(function () {
            $("#sliderB").excoloSlider();
        });
        $(document).ready(function(){
        if($('#msglog').val()=='1'){alert("Email or Password do not match");}
        if($('#forgetMsg').val()=='1'){alert("This email address is not registered.");}
if($('#forgetMsg').val()=='2'){alert("Your User Id and Password has been sent to your Email Id.");}
    });

</script>
<script>
(function(){var dep={"jQuery":"https://code.jquery.com/jquery-latest.min.js"};var init=function(){(function($){$.fn.idTabs=function(){var s={};for(var i=0;i<arguments.length;++i){var a=arguments[i];switch(a.constructor){case Object:$.extend(s,a);break;case Boolean:s.change=a;break;case Number:s.start=a;break;case Function:s.click=a;break;case String:if(a.charAt(0)=='.')s.selected=a;else if(a.charAt(0)=='!')s.event=a;else s.start=a;break;}}
if(typeof s['return']=="function")
s.change=s['return'];return this.each(function(){$.idTabs(this,s);});}
$.idTabs=function(tabs,options){var meta=($.metadata)?$(tabs).metadata():{};var s=$.extend({},$.idTabs.settings,meta,options);if(s.selected.charAt(0)=='.')s.selected=s.selected.substr(1);if(s.event.charAt(0)=='!')s.event=s.event.substr(1);if(s.start==null)s.start=-1;var showId=function(){if($(this).is('.'+s.selected))
return s.change;var id="#"+this.href.split('#')[1];var aList=[];var idList=[];$("a",tabs).each(function(){if(this.href.match(/#/)){aList.push(this);idList.push("#"+this.href.split('#')[1]);}});if(s.click&&!s.click.apply(this,[id,idList,tabs,s]))return s.change;for(i in aList)$(aList[i]).removeClass(s.selected);for(i in idList)$(idList[i]).hide();$(this).addClass(s.selected);$(id).show();return s.change;}
var list=$("a[href*='#']",tabs).unbind(s.event,showId).bind(s.event,showId);list.each(function(){$("#"+this.href.split('#')[1]).hide();});var test=false;if((test=list.filter('.'+s.selected)).length);else if(typeof s.start=="number"&&(test=list.eq(s.start)).length);else if(typeof s.start=="string"&&(test=list.filter("[href*='#"+s.start+"']")).length);if(test){test.removeClass(s.selected);test.trigger(s.event);}
return s;}
$.idTabs.settings={start:0,change:false,click:null,selected:".selected",event:"!click"};$.idTabs.version="2.2";$(function(){$(".idTabs").idTabs();});})(jQuery);}
var check=function(o,s){s=s.split('.');while(o&&s.length)o=o[s.shift()];return o;}
var head=document.getElementsByTagName("head")[0];var add=function(url){var s=document.createElement("script");s.type="text/javascript";s.src=url;head.appendChild(s);}
var s=document.getElementsByTagName('script');var src=s[s.length-1].src;var ok=true;for(d in dep){if(check(this,d))continue;ok=false;add(dep[d]);}if(ok)return init();add(src);})();
</script>


</html>
