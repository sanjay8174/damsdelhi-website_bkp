<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

//     Registration Form
    $('#student-registration').click(function(e) {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });	

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="testimonial-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <div class="banner-right-btns"> <a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a title="Testimonials" class="active-link">Testimonials</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Testimonials</h4>
            <article class="showme-main">
              <div class="main-testimonials">
                <div class="testimonials-box bg_none" style="padding:0px;">
                  <div class="testimonials-content"> Preparation for a highly competitive examination like AIIMS requires a lot of hard work and discipline. I had joined The Test Series in DAMS which helped me quickly recapitulate my subjects. This Test Series consisted of a one hour test followed by two hour discussion session with the Faculty, which helped me clear my doubts and some controversial questions </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Saurabh Kedia</a> <span>AIIMS RANK-1, PGI Rank-4</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> I was a student of the Regular batch in DAMS. I always felt that the detailed class room coaching and extensive interaction with the faculty in DAMS helped me clear the examination with flying colors. In this exam it is not only necessary to clear the exam but also to get a rank where you can get subject and institute of your choice. Good thing about DAMS regular course is that it includes a lot of weekly subject based tests in its regular course which help the student to get a grasp on the subject. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Reenu Jain</a> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> I had joined The Test Series in DAMS as I had already done one reading of the subject and knew all I needed to do clear the exam was a quick revision based on MCQs for which test series was ideal. Discussion with the faculty at the end of the test was a great help. The Test & discussion series was of particular help to me  in the small subjects like Radiology, Psychiatry etc. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Sumit Jain</a> <span>AIIMS Rank-4</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> Preparation for state PGs is slightly different from that of AIPG or AIIMS in that they include a lot of factual questions. Good thing about DAMS is that they pay emphasis to both kinds of questions thoroughly. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Jyoti Mal</a> <span>Delhi PG Rank-9</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> Thing that I like about DAMS is the extremely student friendly environment, the management is always open to positive inputs from the students. A feedback is always taken from students and there is a constant effort to keep on improving upon the course material. The emphasis is placed on MCQ solving and time management. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Akhilesh</a> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> According to me the good thing about DAMS is approachability of DAMS, they are always there to listen to us and help us. I am particularly impressed by the motivational sessions that are a unique feature to DAMS, they helped me regain my confidence in this examination and now I am confident that I will be able to get a good rank this year. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Saurabh Kedia">Dr. Rajeev</a> </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          
          <!--for Enquiry popup  -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry popup  --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>