<?php header('location: https://damsdelhi.com/csr_policy.php');exit;
?>
<!DOCTYPE html>
<html>
<head>
    <title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
</head>
<?php echo "<b> Response 204</b><br>
Server has received the request but there is no information to send back, and the client should stay in the same document view.<br> This is mainly to allow input for scripts without changing the document at the same time. ";
 exit;?> 
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<style>
.privacy-content .csr_policy{list-style: decimal outside;float: left;width: 100%;padding-left: 20px;box-sizing: border-box;  font-family: 'pt_sansbold';font-size: 16px;color: #555;} 
.ulcont_div{float: left;width: 100%;padding-left: 20px;box-sizing: border-box;font-size: 14px;}
.roman_list_type_upper{font-family: 'pt_sansregular';list-style: upper-roman outside;  }
.lower_alfa_list{font-family: 'pt_sansregular'; list-style: lower-alpha outside;}
.disc_style{font-family: 'pt_sansregular'; list-style: disc outside;}
.privacy-content .csr_policy .csr_policy_li{margin-bottom: 20px;float: left;}   
.privacy-content .csr_policy ul{margin-top: 7px;}
.privacy-content .csr_policy li label { font-family: 'pt_sansbold'; font-size: 16px;} 
/*.roman_list_type{font-family: 'pt_sansregular';font-size: 14px;list-style: lower-roman outside;float: left;width: 100%;padding-left: 20px;box-sizing: border-box;  }*/

.left_div_cont{float: left;display: inline-block;width:48%;}
.right_div_cont{float: right;width:48%;}
.privacy-content .csr_policy p { font-family: 'pt_sansregular'; font-size: 14px;font-weight: 400;text-align: justify; line-height: 20px; padding: 0 0 5px;}
.privacy-content .disclamer_pravecy p{line-height: 20px;font-family: 'pt_sansbold';font-size: 14px; font-weight: 600;padding: 0px 0px 0px 0px;}
.privacy-content .csr_policy  .approved_cont{font-family: 'pt_sansbold';font-size: 14px; font-weight: 600;padding: 7px 0px 7px 0px;}
.feetblcntr {color: #494949;width: 100%;border-collapse: collapse; margin-top: 0px; border: 1px solid #d4d4d4;border-bottom: 0px; font-size: 15px; position: relative; background-color: #fff;}
.feetblcntr td, .feetblcntr th {border: 1px solid #ccc;  font-size: 14px;padding: 10px 12px;font-weight: normal;text-align: left;}
.feetblcntr th { background: #f0f0f0;border-bottom-color: transparent; padding: 5px 12px; text-transform: uppercase; font-size: 16px;}
.feetblcntr td { word-wrap: break-word;}
.privacy-content_scr{margin-top: 40px;}
 .privacy-content .appen_2{font-weight: 400px;font-size: 14px;}
@media only screen and (max-width:539px){
    .feetblcntr td, .feetblcntr th {padding: 10px 2px;}
   
}
</style>
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include '../header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article>
<aside class="banner-left">
<h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>CSR Policy</h4>
<article class="showme-main">
<div class="privacy-content"> <span>CORPORATE SOCIAL RESPONSIBILITY (“CSR”) POLICY FOR DELHI ACADEMY OF MEDICAL SCIENCES PRIVATE LIMITED-</span>

<ul class=" csr_policy">
<li class="csr_policy_li"><label>INTRODUCTION:</label>
    <ul>
        <li><p>Corporate Social Responsibility (CSR) is the Company’s commitment to its stakeholders to conduct business in an economically, socially and environmentally sustainable manner that is transparent and ethical. Delhi Academy Of Medical Sciences Private Limited in India is committed to undertake CSR activities in accordance with the provisions of Section 135 of the Companies Act, 2013 and related Rules.</p></li>
    </ul> 
</li>
<li class="csr_policy_li"><label>AIMS &amp; OBJECTIVES:</label>
    <ul class="disc_style ulcont_div">
        <li><p>To develop a long-term vision and strategy for the Company’s CSR objectives.</p></li>
        <li><p>Establish relevance of potential CSR activities to Company’s core business and create an overview of activities to be undertaken, in line with Schedule VII of the Companies Act, 2013.</p></li>       
        <li><p>Delhi Academy Of Medical Sciences Private Limited shall promote projects that are :</p>
            <ol class="lower_alfa_list ulcont_div">
              <li><p>Sustainable and create a long term change;</p></li>
              <li><p>Have specific and measurable goals in alignment with Company’s philosophy;</p></li>
              <li><p>Address the most deserving cause or beneficiaries.</p></li>
            </ol>
        </li>
         <li><p>To establish process and mechanism for the implementation and monitoring of the CSR activities for Delhi Academy Of Medical Sciences Private Limited.</p></li>
        
    </ul> 
</li>
<li class="csr_policy_li"><label>COMMITTEE COMPOSITION</label>
    <ul class="disc_style ulcont_div">       
        <li><p>The CSR Committee of the Board shall be composed of following Two (2) Directors.</p>
            <ol class="roman_list_type_upper ulcont_div">
              <li><p>Mr. Sumer Kumar Sethi,</p></li>
              <li><p>Ms. Ridhi Bhagi,</p></li>
             
            </ol>
        </li>
         <li><p>The Members of the CSR Committee may be replaced by any other member of the Board.</p></li>
       
        
    </ul> 
</li>
<li class="csr_policy_li"><label>COMMITTEE MEETINGS</label>
    <ul class="disc_style ulcont_div">
        <li><p>The CSR Committee shall meet as often as its members deem necessary to perform the duties and responsibilities.</p></li>
   </ul> 
 </li>
<li class="csr_policy_li"><label>DUTIES AND RESPONSIBILITIES OF CSR COMMITTEE</label>
    <ul class="disc_style ulcont_div">
        <li><p>Review of the CSR activities undertaken by the Company. The CSR Committee shall be guided by the list of activities as specified in schedule VII to the Companies Act, 2013 and appended to this policy as Annexure-1. Annexure -1 may be revised in line with any amendment/ inclusion made to Schedule VII of the Companies Act, 2013.</p></li>
        <li><p>Formulate and recommend to the Board CSR Activities/ programs to be undertaken by the company.</p></li>
        <li><p>Recommend the CSR Expenditure to be incurred on CSR activities/programs.</p></li>
        <li><p>Institute a transparent mechanism for implementation of the CSR projects and activities. Effectively monitor the execution of the CSR activities.</p></li>
        <li><p>Prepare an annual report of the CSR activities undertaken for Delhi Academy of Medical Sciences Private Limited and submit such report to the Board.</p></li>
           
   </ul> 
 </li>
<li class="csr_policy_li"><label>REPONSIBILITY OF THE BOARD</label>
    <ul class="disc_style ulcont_div">
        <li><p>Approve the CSR Policy and the CSR Expenditure after taking into consideration the recommendations made by the CSR committee.</p></li>
        <li><p>Ensure the CSR spending every financial year of at least 2% of average net profits made during immediately preceding 3 financial years, in pursuance with the Policy.</p></li>
        <li><p>Ensure that CSR activities included in the CSR Policy are undertaken by Delhi Academy of Medical Sciences Private Limited and that such activities are related to the activities specified in Schedule VII of the Companies Act.</p></li>
        <li><p>Ensure disclosure of the contents of the CSR Policy on the Delhi Academy of Medical Sciences Private Limited website (if any).</p></li>
        <li><p>Directors’ Report for FY 2013-14 onwards to include:</p>
            <ol class="lower_alfa_list ulcont_div">
              <li><p>Contents of the CSR Policy and Composition of the CSR committee;</p></li>
              <li><p>An annual report on the CSR in the prescribed format as per Annexure- 2;</p></li>
              <li><p>Reasons for failure (if any) to spend required amount on CSR activities.</p></li>
            </ol>
        </li>
         <li><p>Delhi Academy of Medical Sciences Private Limited shall undertake the CSR activities directly. The Board may, in the future, decide to undertake and implement its CSR activities through a registered trust or registered society or a Section 8 company (Non-profit entity) established by the Delhi Academy of Medical Sciences Private Limited Group. In case the trust, society or a Section 8 company is not established by the company or its holding or subsidiary or associate company, then such an entity will need to have a 3 years track record of undertaking similar projects or programmes.</p></li>
           
   </ul> 
 </li>
<li class="csr_policy_li"><label>CSR EXPENDITURE</label>
    <ul class="disc_style ulcont_div">
        <li><p>In every financial year, Delhi Academy of Medical Sciences Private Limited shall spend a minimum of 2% of its average Net Profits in the immediately preceding three (3) financial years. Average Net profits shall mean the net profits of the Company as per the Profit &amp; Loss Statement prepared in accordance with the Companies Act, 2013; Net Profits shall exclude</p>

            <ol class="lower_alfa_list ulcont_div">
              <li><p>profits arising from any overseas branch or branches of Delhi Academy of Medical Sciences Private Limited (whether operated as a separate company or otherwise); or</p></li>
              <li><p>dividend received from other companies in India.</p></li>              
            </ol>
        </li>
        <li><p>CSR Expenditure shall mean all expenditure incurred in respect of specific projects/programs relating to the approved CSR activities.</p></li>
        <li><p>CSR Expenditure shall not include expenditure on an item not in conformity or not in line with activities which fall within the purview of the CSR activities listed in Schedule VII.</p></li>
        <li><p>CSR Expenditure shall not include Projects or programs or activities undertaken outside India.</p></li>
        <li><p>The surplus arising out of the CSR activities or projects shall not form part of the business profit of Delhi Academy of Medical Sciences Private Limited.</p></li>
        <li><p>Contributions by other Delhi Academy of Medical Sciences Private Limited affiliates or employees may also be received and utilized in respect of the CSR activities undertaken.</p></li>
           
   </ul> 
 </li>
<li class="csr_policy_li"><label>CSR ACTIVITES-PROJECTS</label>
    <ul class="disc_style ulcont_div">
        <li><p>Delhi Academy of Medical Sciences Private Limited shall promote CSR activities/Projects in the field of education and other medical facilities.</p> </li>
        <li><p>Delhi Academy of Medical Sciences Private Limited may also undertake other CSR activities in line with Schedule VII.</p></li>
        <li><p>The CSR activities shall be undertaken in locations within India. Delhi Academy of Medical Sciences Private Limited shall give preference to the local areas and the areas around which Delhi Academy of Medical Sciences Private Limited operates while considering the activities to be undertaken and spending the amount earmarked for CSR activities.</p></li>
       
   </ul> 
 </li>
 <li class="csr_policy_li"><label>MONITORING AND REVIEW MECHANISM</label>
    <ul class="disc_style ulcont_div">
        <li><p>The administration of the CSR Policy and the execution of the identified CSR projects, programs, activities under it shall be carried out under the overall superintendence and guidance of an internal monitoring group formed for this purpose.</p></li>
        <li><p>The following activities do not qualify as CSR Activities under the ompaies Act, 2013:</p>
            <ol class="lower_alfa_list ulcont_div">
              <li><p>Projects or activities not falling within schedule VII (Annexure 1);</p></li>
              <li><p>Activities undertaken in pursuance of normal course of business;</p></li>              
              <li><p>Projects or Programs or activities that benefit only the employees of the Company and their families;</p></li>              
              <li><p>Direct or indirect contribution to any political party.</p></li>              
            </ol>
        </li>
           
   </ul> 
 </li>
 <li class="csr_policy_li"><label>CSR REPORTING</label>
    <ul class="disc_style ulcont_div">
        <li><p>The Board in its Annual Report shall include the details of the CSR activities undertaken in the Financial Year. The particulars to be stated in the report shall be in the format as prescribed in in the Companies Act, 2013 and rules made thereunder.</p> </li>
        <li><p>The CSR Committee shall provide a responsibility statement on the implementation and monitoring of the CSR Policy and that it is in compliance with CSR objectives of Delhi Academy of Medical Sciences Private Limited, which statement shall form part of the Boards’ Report.</p></li>            
   </ul> 
 </li>
 <li class="csr_policy_li"><label>WEBSITE DISPLAY</label>
    <ul class="disc_style ulcont_div">
        <li><p>Delhi Academy of Medical Sciences Private Limited shall display on its website the contents of the CSR Policy and other information as may required to be displayed.</p> </li>
        
   </ul> 
 </li>
 <li class="csr_policy_li"><label>REVIEW AND AUDIT</label>
    <ul class="disc_style ulcont_div">
        <li><p>The CSR committee shall be apprised on the implementation of the CSR activities and the progress shall be monitored on a quarterly basis.</p> </li>
        <li><p>Delhi Academy of Medical Sciences Private Limited shall through its internal controls, monitoring and evaluation systems implement, assess, document and report the impact of its CSR activities/projects.</p></li>            
        <li><p>Records relating to the CSR activities and the CSR Expenditure shall be meticulously maintained. The Records shall be submitted for reporting and audit.</p></li>            
   </ul> 
 </li>
 <li class="csr_policy_li"><label>AMENDMENTS</label>
    <ul >
        <li><p>The Policy may be renewed and amended from time to time with the approval of theBoard. </P>
            <p class="approved_cont">Approved:</p>
            <div class="left_div_cont disclamer_pravecy">
                <p>Sumer Kumar Sethi</p>
                <p>Director</p>
                <p>DIN: 02471789</p>
            </div>
            <div class="right_div_cont disclamer_pravecy">
                <p>Ridhi Bhagi</p>
                <p>Director</p>
                <p>DIN: 06760363</p>
            </div>
        </li>
        
            
   </ul> 
 </li>


</ul>
</div>
</article>
<!--<article>
<div class="privacy-content"> <span>ANNEXURE - 1</span>
    <p>CSR Activities Listed in Schedule VII of the Companies Act, 2013 CSR shall focus on social, economic and environmental impact rather than mere output and outcome. Activities which are ad hoc and philanthropic in nature shall be avoided. Various activities that can be undertaken in general under CSR are outlined below:</p>
    <ul class=" csr_policy">
        <li><p>The CSR committee shall be apprised on the implementation of the CSR activities and the progress shall be monitored on a quarterly basis.</p> </li>
    </ul>
</div>   
</article>-->
<article>
<div class="privacy-content privacy-content_scr"> <span style="text-align: center;">ANNEXURE - 1</span>
    <p>CSR Activities Listed in Schedule VII of the Companies Act, 2013 CSR shall focus on social, economic and environmental impact rather than mere output and outcome. Activities which are ad hoc and philanthropic in nature shall be avoided. Various activities that can be undertaken in general under CSR are outlined below:</p>
    <ul class=" csr_policy appen_2">
        <li><p>eradicating extreme hunger and poverty and malnutrition, promoting preventive healthcare and sanitation and making available safe drinking water;</p> </li>
        <li><p>promotion of education; including special education and employment enhancing vocation skills especially among children, woman, elderly and the differently abled and livelihood enhancement projects ;</p> </li>
        <li><p>promoting gender equality and empowering women; setting up homes and hostels for women and orphans, setting up old age homes, day care centres, and such other facilities for senior citizens and measures for reducing inequalities faced by socially and economically backward groups;</p> </li>
        <li><p>ensuring environmental sustainability, ecological balance, protection of flora and fauna, animal welfare, agro forestry, conservation of natural resources and maintaining of quality of soil, air and water;</p> </li>
        <li><p>protection of national heritage, art and culture including restoration of buildings and sites of historical importance and works of art; setting up of public libraries; promotion and development of traditional arts and handicrafts;</p> </li>
        <li><p>measures for the benefit of armed forces veterans, war widows and their dependents;</p> </li>
        <li><p>training to promote rural sports, nationally recognized sports, and paraolympic sports and Olympic sports;</p> </li>
        <li><p>contribution to the Prime Minister&#39;s National Relief Fund or any other fund set up by the Central Government or the State Governments for socio-economic development and relief and welfare of the Scheduled Castes, the Scheduled Tribes, other backward classes, minorities and women;</p> </li>
        <li><p>Contributions or funds provided to technology incubators located within academic institutions which are approved by the Central Government; and 10. rural development projects.</p> </li>
    </ul>
</div>   
</article>
    <article>
       <div class="privacy-content privacy-content_scr"> <span style="text-align: center;">ANNEXURE - 2</span>
        <table class="feetblcntr" cellspacing="0" cellpadding="0" border="1">
                                           <thead></thead>
                                                                            
                                      <tbody> 
                                                                                    <tr>
                                            <td>1.</td>
                                            <td>CSR Project/Activity identified</td>
                                            <td>Promotion of Education and medical facilities</td>
                                            <td>TOTAL</td>
                                            
                                           
                                          </tr>
                                         <tr>
                                            <td>2.</td>
                                            <td>Sector in which the project is covered</td>
                                            <td>Education Sector</td>
                                            <td></td>
                                         </tr>
                                         <tr>
                                            <td>3.</td>
                                            <td>Projects/Programmes 1. Local area/others 2. Specify the state /district(Name of the District/s, State/s where project/programme was undertaken</td>
                                            <td>Local Areas</td>
                                            <td></td>
                                         </tr>
                                         <tr>
                                            <td>4.</td>
                                            <td>Amount outlay(budget) project/programwise</td>
                                            <td>2% Of the Average Net Profit of Immediately preceding3 Financial years</td>
                                            <td></td>
                                         </tr>
                                         <tr>
                                            <td>5.</td>
                                            <td>Amount spent on the project/program Subheads: 1.Direct expenditure on project, 2.Overheads:</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                         </tr>
                                         <tr>
                                            <td>6.</td>
                                            <td>Cumulative spend upto to the reporting period.</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                         </tr>
                                         <tr>
                                            <td>7.</td>
                                            <td>Amount outlay(budget) project/programwise</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                         </tr>
<!--                                          <tr>
                                            <td>SL NO.</td>
                                            <td>CSR Project/Activity identified</td>
                                            <td>Sector in which the project is covered</td>
                                            <td>Projects/Programmes 1. Local area/others 2. Specify the state /district(Name of the District/s, State/s where project/programme was undertaken</td>
                                            <td>Amount outlay(budget) project/programwise</td>
                                            <td>Amount spent on the project/program Subheads: 1.Direct expenditure on project, 2.Overheads:</td>
                                            <td>Cumulative spend upto to the reporting period.</td>
                                            <td>Amount spent: Direct/through implementing agency</td>
                                          </tr>-->
<!--                                          <tr>
                                            <td>1.</td>
                                            <td>Promotion of Education and medical facilities</td>
                                            <td>Education Sector</td>
                                            <td>Local Areas</td>
                                            <td>2% Of the Average Net Profit of Immediately preceding3 Financial years</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                          </tr>
                                          <tr>
                                            <td></td>
                                            <td>TOTAL</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                            <td>NIL</td>
                                          </tr>-->
                                      </tbody>
        </table>
           <ul class=" csr_policy" style="list-style: none outside; margin-top: 30px;">
        <li><p>Place: New Delhi<br>Date: 08-08- 2015</P>
            <p class="approved_cont">For and on behalf of the Board of Directors</p>
            <div class="left_div_cont disclamer_pravecy">
                <p>Sumer Kumar Sethi</p>
                <p>Director</p>
                <p>DIN: 02471789</p>
                <p>R/o D-3, Sector 55,</p>
                <p>Noida-201301, Uttar Pradesh</p>
            </div>
            <div class="right_div_cont disclamer_pravecy">
                <p>Ridhi Bhagi</p>
                <p>Director</p>
                <p>DIN: 06760363</p>
                <p>R/o 83 Sukhdev Vihar,</p>
                <p>Jamia Millia, New Delhi-110025</p>
            </div>
 </li></ul>
       </div>
    </article>

</div></aside>
<aside class="gallery-right">
<?php include 'enquiryform.php'; ?>
</aside>

    
</section></div></div></section>
    
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>