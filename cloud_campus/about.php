<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href=css/style.css rel=stylesheet type=text/css />
<link href=css/responcive_css.css rel=stylesheet type=text/css />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="dams-aboutus">
<aside class="banner-left banner-left-postion">
<h2>Number 1 Post Graduate Medical &amp;<br>Dental Test Prep Company in India </h2>
<h3 class="page_title">Since Last 15 Years </h3>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li class="bg_none"><a title="About Us" class="active-link">About Us</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>About DAMS</h4>
<article class="showme-main">
<div class="about-content">
<p>Delhi Academy of Medical Sciences (DAMS) a well-established name in the field of PG Medical Entrance Education for over 15 years now. Today under the leadership of Dr Sumer Sethi, Radiologist and pervious topper in AIPG/ AIIMS. DAMS is recognized for its impeccable foresight, enviable expertise and innate acumen. </p>
<p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>
<p>The Delhi Academy of Medical Sciences (DAMS) was established to create a bench-mark institution to achieve excellence in the toughest competitive exam in the country, i.e. PG Medical Entrance Exam. Over this long period, the DAMS has evolved into a unique fraternity of educators and students striving together, year after year, in pursuit of a single goal.</p>
<p>With a passion to excel, the Delhi Academy of Medical Sciences has raged with the dynamism of a river which constantly renews itself and yet remains unchanging in its resolve to reach its ultimate destination. </p>
<p>The institute's aim is not only to provide specific knowledge and strengthen the foundation of the students in PG Medical Entrance, but also to infuse them with determination to crack the Entrance Exams at post graduation level. To explore the potential of the students and to help them master the subject, We, at DAMS have developed extensive scientific teaching as well as testing methods. We alos have special sessions on mental training required to develop the so called "killer instinct". </p>
<p>Delhi Academy of Medical Sciences  is known as the most specialized institution of its type in the country, performing consistently at high levels and has acquired the rare distinction of achieving the highest success-rate in the PG Medical Entrance Exam. </p>
<p>In short DAMS is a institution which provides:</p>
<ul class="about-list">
<li>1. Extensive subject wise teaching by experts and authors of popular books.</li>
<li>2. Detailed Notes</li>
<li>3. MCQ based Brain storming sessions</li>
<li>4. Pioneers in test and discussion course</li>
<li>5. Only institute in the country which can claim of 85% overall success rate that is out of 100 students that join us 85 get through for sure!!</li>
<li>6. Nation wide presence and All India Ranking of tests</li>
</ul></div></article></div></aside>
<aside class="gallery-right">
<div id="store-wrapper">
<div class="dams-store-link"><span></span>About US</div>
<div class="dams-store-content">
<div class="inner-store">
<ul><li class="bg_none"><a href="dams.php" title="About DAMS" class="active-store">About DAMS</a></li>
<li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
<li><a href="about_director.php" title="About Director">About Director</a></li>
<li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
</ul></div></div></div>
<?php include 'enquiryform.php'; ?>
</aside></section></div> </div>
</section>
<?php include 'footer.php'; ?>
<script type=text/javascript async src=js/html5.js></script>
<script type=text/javascript src=js/jquery-1.10.2.min.js></script>
<script type=text/javascript async src=js/registration.js></script>
<script type=text/javascript async src=js/add-cart.js></script>
</body>
</html>