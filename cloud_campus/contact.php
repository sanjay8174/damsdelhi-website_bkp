<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS - PG Medical Coaching, NEET PG</title>
<meta name="description" content=" Address and telephone number of Delhi Academy of Medical Sciences (DAMS) one of the best pg medical coaching in New Delhi, India." />
<meta name="keywords" content=" DAMS contact, DAMS Address, PG Medical Coaching Contact, PG Medical Coaching Adddress " />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php error_reporting(0);?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="contact-banner"> 
<aside class="banner-left">
<h2>Reach out to us to be guided by the</h2>
<h3>Best team of faculty and motivators</h3>
</aside>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading paddin-zero">
<h4>Contact Us</h4>
<article class="showme-main">
<div class="main-head-office">
<div class="main-left-office">
<aside class="head-office-left">
<div class="office-location">
<h5>HEAD OFFICE /CLASS ROOM:</h5>
<ul>
<li class="location boder-none">
<span>Delhi Academy of Medical Science (P) Ltd.</span>
4B, Grover's Chamber, Pusa Road, 
Near Karol Bagh Metro Station, 
New Delhi-110 005 (India)</li>
<li class="cell">Phone: 011-40094009, 011-40464046 </li>
<li class="mobile">Mobile: 9811469990, 9953550295, 8447461115</li>
<li class="web-link">Email: <a href="mailto:info@damsdelhi.com">info@damsdelhi.com</a></li>
</ul>
</div>
</aside>
<aside class="head-office-right">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3501.538276969117!2d77.1878532!3d28.6435973!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d0297e2c854a1%3A0x9e105a2ca7d3fdbf!2sDelhi+Academy+Of+Medical+Sciences!5e0!3m2!1sen!2sin!4v1393322947642" width="264" height="200" class="boder-none"></iframe>
</aside>
</div>
<div class="main-left-office"><!-- space-nil-->
<aside class="head-office-left">
<div class="office-location"> 
<ul>
<li class="location boder-none">
<span>SOUTH DELHI CENTRE</span>
135/8,Gautam Nagar
Opp Peepal Hanuman Mandir
Gulmohar Park Road
New Delhi-110 049 (India)</li>
<li class="cell">Phone:  011-40094009, 011-40464046</li>
<li class="mobile">Mobile: 8373933276</li>
<li class="web-link">Email: <a href="mailto:info@damsdelhi.com">info@damsdelhi.com</a></li>
</ul>
</div>
</aside>
<aside class="head-office-right">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.353469576997!2d77.211274569794!3d28.55914673675161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac8d3657013edfcf!2sPrajapati+Shiv+Mandir!5e0!3m2!1sen!2sin!4v1393322867219" width="264" height="200" class="boder-none"></iframe>
</aside>
</div>

<div class="main-left-office space-nil boder-none paddin-zero margn-zero">
<aside class="position-set-left">
<div class="office-location"> 
<ul>
<li class="location boder-none">
<span>MDS Dental</span>
</li>
<li class="mobile">Mobile: 09999158131, 09999322168</li>
</ul>
</div>
</aside>
<aside class="position-set-right">
<div class="office-location"> 
<ul>
<li class="location boder-none">
<span>Dental Carrer Counselling for MDS (Dental Quest)</span>
</li>
<li class="mobile">Mobile: 09999158131</li>
</ul>
</div>
</aside>
</div>
</div>
</article>
</div>
</aside>
<aside class="gallery-right">
<!--for Enquiry popup  -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry popup  -->
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>