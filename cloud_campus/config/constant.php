<?php
class Constant
{
    //for live
    public static $loginLink        = "onlinetest.damsdelhi.com";
    public static $path             = "https://damsdelhi.com";
    public static $pathCMS          = "https://damsdelhi.com/damsCMS"; 
    public static $damsStorepath    = "https://damsdelhi.com";
    
    //for 192.168.0.250
//    public static $loginLink        = "https://192.168.0.250/damsots";
  //  public static $path             = "https://192.168.0.250/damswebsite_v1.2";
//    public static $pathCMS          = "https://192.168.0.250/damswebadmin"; 
//    public static $damsStorepath    = "https://192.168.0.250/damswebsite_v1.2";
    
    //for local
//    public static $loginLink        = "https://localhost/damsots";
//    public static $path             = "https://localhost/damswebsite_v1.2";
//    public static $pathCMS          = "https://localhost/damswebadmin"; 
//    public static $damsStorepath    = "https://localhost/damswebsite_v1.2";
//    

}
?>
