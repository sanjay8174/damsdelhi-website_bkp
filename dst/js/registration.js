/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * 
 *          Author of this Page     :   MAYANK PATHAK 
 *          Page created on         :   15/Sept/2015   14:30:35
 * 
 */


$("document").ready( function(){
    
    $("#li_a_features").click(function() {
//        alert("features");
        $("#li_a_overview").removeClass("selected");
        $("#li_a_register").removeClass("selected"); 
        $("#li_a_contactUs").removeClass("selected");
        $("#li_a_photos").removeClass("selected");
        $("#li_a_vedio").removeClass("selected");
        $("#li_a_features").addClass("selected");
        
    });
    
    
    $("#li_a_overview").click(function() {
//        alert("Overview");
        $("#li_a_features").removeClass("selected");
        $("#li_a_contactUs").removeClass("selected");
        $("#li_a_register").removeClass("selected");
        $("#li_a_photos").removeClass("selected");
        $("#li_a_vedio").removeClass("selected");
        $("#li_a_overview").addClass("selected");
        
    });
    
    
    $("#li_a_contactUs").click(function() {
//        alert("contactUs");
        $("#li_a_overview").removeClass("selected");
        $("#li_a_features").removeClass("selected");
        $("#li_a_register").removeClass("selected");
        $("#li_a_photos").removeClass("selected");
        $("#li_a_vedio").removeClass("selected");
        $("#li_a_contactUs").addClass("selected");
        
    });
    $("#li_a_photos").click(function() {
//        alert("contactUs");
        $("#li_a_overview").removeClass("selected");
        $("#li_a_features").removeClass("selected");
        $("#li_a_register").removeClass("selected");
        $("#li_a_contactUs").removeClass("selected");
        $("#li_a_vedio").removeClass("selected");
        $("#li_a_photos").addClass("selected");
        
    });
    
    $("#li_a_register").click(function() {
//        alert("contactUs");
        $("#li_a_overview").removeClass("selected");
        $("#li_a_features").removeClass("selected");
        $("#li_a_contactUs").removeClass("selected");
        $("#li_a_photos").removeClass("selected");
        $("#li_a_vedio").removeClass("selected");
        $("#li_a_register").addClass("selected");
        
    });
    $("#li_a_vedio").click(function() {
//        alert("contactUs");
        $("#li_a_overview").removeClass("selected");
        $("#li_a_features").removeClass("selected");
        $("#li_a_contactUs").removeClass("selected");
        $("#li_a_photos").removeClass("selected");
        $("#li_a_vedio").addClass("selected");
        $("#li_a_register").removeClass("selected");
        
    });
    
    
    
    
});








        function validateRegistration(){
            
//            alert("valReg");
            $('input').css('border-color', '#d8d8d8');
            var name            = $.trim($("#name").val());
            var email           = $.trim($("#email").val());
            var mobile          = $.trim($("#mobile").val());
            var password        = $.trim($("#password").val());
            var cnfrmPassword   = $.trim($("#Cnfpassword").val());
            var courseSelect    = $.trim($("#courseSelect").val());
            var stateSelect     = $.trim($("#stateSelect").val());
            var citySelect      = $.trim($("#citySelect").val());
            var countrySelect      = $.trim($("#countrySelect").val());
            var collageName     = $.trim($("#collage").val());
            var paperSelect  = $("#paperSelect").val();
            
            var stateName =     $.trim($("#stateSelect option:selected").text());
            var cityName =     $.trim($("#citySelect option:selected").text());
            $("#stateName").val(stateName);
            $("#cityName").val(cityName);
            
            if(name == ""){
                $('#name').val('');
                $('#name').focus();
                $('#name').attr('placeholder', 'Enter your name');
                $('#name').css('border-color', 'red');
                return false;
            }
            if(email == ""){
                $('#email').val('');
                $('#email').focus();
                $('#email').attr('placeholder', 'Enter your Email Id');
                $('#email').css('border-color', 'red');
                return false;
            }
            
            if(email != "" && !checkEmail(email)){
                $('#email').val('');
                $('#email').focus();
                $('#email').attr('placeholder', 'Enter your valid Email Id');
                $('#email').css('border-color', 'red');
                return false;
            }
            
            if(mobile=="" || mobile.length != 10){
                $('#mobile').val('');
                $('#mobile').focus();
                $('#mobile').attr('placeholder', 'Enter your 10 digit Phone/Mobile number');
                $('#mobile').css('border-color', 'red');
                return false;
            }
            
            if(password == ""){
                $('#password').val('');
                $('#password').focus();
                $('#password').attr('placeholder', 'Enter Password');
                $('#password').css('border-color', 'red');
                return false; 
            }
            
            if(password.length < 6){
                $('#password').val('');
                $('#password').focus();
                $('#password').attr('placeholder', 'Enter at least 6 digit');
                $('#password').css('border-color', 'red');
                return false; 
            }
            
            if(cnfrmPassword == ""){
                $('#Cnfpassword').val('');
                $('#Cnfpassword').focus();
                $('#Cnfpassword').attr('placeholder', 'Confirm Password');
                $('#Cnfpassword').css('border-color', 'red');
                return false; 
            }
            
            
            if(password != cnfrmPassword){
                $('#Cnfpassword').val('');
                $('#Cnfpassword').focus();
                $('#Cnfpassword').attr('placeholder', 'Password dont match');
                $('#Cnfpassword').css('border-color', 'red');
                return false; 
            }
            
            if(paperSelect == "0"){
                $('#paperSelect').focus();
                $('#paperSelect').css('border-color', 'red');
                return false; 
            }
            
//            
            if(courseSelect == ""){
//                $('#courseSelect').val('');
                $('#courseSelect').focus();
                $('#courseSelect').css('border-color', 'red');
                return false; 
            }
            var selectedText =  $.trim($("#courseSelect option:selected").text());
            $("#course_name").val(selectedText);
            if(countrySelect == 0){
                 $('#countrySelect').focus();
                $('#countrySelect').css('border-color', 'red');
                return false; 
            }
            if(stateSelect == "0"){
//                $('#stateSelect').val('');
                $('#stateSelect').focus();
                $('#stateSelect').css('border-color', 'red');
                return false; 
            }
            
            
            if(citySelect == "0"){
//                $('#stateSelect').val('');
                $('#citySelect').focus();
                $('#citySelect').css('border-color', 'red');
                return false; 
            }
            if(collageName==''){
               $('#collage').focus();
                $('#collage').css('border-color', 'red');
                return false;  
            }
            
            
            
            return true;

        }



//Function Added to validate Email Id added by MAYANK PATHAK
function checkEmail(s) {
//    alert(s);
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var booleanValue = emailPattern.test(s);
    if (booleanValue) {
//    This will return i.e email is ok
    return true;
    } else {
//    Thjis will return i.e email in not ok
    return false;
    }
}


function stateChange(id){
    var urldata="mode=statechange&stateId="+id;
//    alert(urldata);
    $.ajax({
            type: "post",
            url: "index.php?p=ajax",
            data: urldata,
	        async:false,
            error:function(result)
            {
                alert("Error : "+result);
                return false;
            },
            success: function (result)
            {
                document.getElementById('citySelect').innerHTML=$.trim(result);
            }
        });
}
function countryChange(id){
    var urldata="mode=countrychange&countryId="+id;
//    alert(urldata);
    $.ajax({
            type: "post",
            url: "index.php?p=ajax",
            data: urldata,
	        async:false,
            error:function(result)
            {
                alert("Error : "+result);
                return false;
            },
            success: function (result)
            { //alert(result);
                document.getElementById('stateSelect').innerHTML=$.trim(result);
            }
        });
}

function validateLogin(){
    
    
    $('input').css('border-color', '#d8d8d8');
    var email = $.trim($("#loginEmail").val()); 
    var pass  = $.trim($("#loginPassword").val()); 
    
    if(email == ""){
        
        $("#loginEmail").focus();
        $('#loginEmail').attr('placeholder', 'Please enter your Roll No./Email');
        $('#loginEmail').css('border-color', 'red');
        return false; 
    }
    
//    if(email != "" && !checkEmail(email)){
//        
//        $("#loginEmail").val("");
//        $("#loginEmail").focus();
//        $('#loginEmail').attr('placeholder', 'Please enter your Roll No./Email');
//        $('#loginEmail').css('border-color', 'red'); 
//        
//    }
    
    if(pass == ""){
        
        $("#loginPassword").focus();
        $('#loginPassword').attr('placeholder', 'Please enter your Password');
        $('#loginPassword').css('border-color', 'red');
        return false; 
    }
    
}