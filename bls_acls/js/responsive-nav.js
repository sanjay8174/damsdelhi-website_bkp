$(document).ready(function(){
	$(".demo1-d").click(function(){
		$(".demo3-d").slideToggle('normal');
	});
	$(".demo-2-d").click(function(){
		$(".demo6-d").slideToggle('normal');
	});
	$(".demo2-d").click(function(){
		$(".demo4-d").slideToggle('normal');
	});
	$('#fg-password-resp').click(function(){
		$('#loginentry').hide(); $('#forgtpasswd').show();
	});
});

function menuslides(val)
{
	var g=1; var cou=$('.demo3-d > ul > li > ul').size();
	for(g;g<=cou;g++)
	{
		if(g==val){	$("#inul"+g).slideToggle('normal'); } else { $("#inul"+g).slideUp('normal'); }
	}
}

function innermenuslides(val)
{
	var inng=1; var inncou=$('.extra-inner').size();
	for(inng;inng<=inncou;inng++)
	{
		if(inng==val){ $("#innrul"+inng).slideToggle('normal'); } else { $("#innrul"+inng).slideUp('normal'); }	
	}
}

function loginslides(val)
{
	var v=1; var count=$('.responcive-login-box').size(); 
	for(v;v<=count;v++)
	{
		if(v==val){	$("#indiv"+v).slideToggle('normal'); $("#login"+v).addClass('active-res'); } else {	$("#indiv"+v).slideUp('normal'); $("#login"+v).removeClass('active-res'); }
	}
}

function stateChangeResp(id){
    var urldata="mode=stateChangeReg&id="+id;
    $.ajax({
            type: "post",
            url: "ajax.php",
            data: urldata,
            error:function(result)
            {
                return false;
            },
            success: function (result)
            {
               document.getElementById('citydiv-resp').innerHTML=result;
            }
        });
}

function Submit_form_newReg_resp(){
        if($("#email-resp").val()==""){
           $('#regresError1').html("Please enter your email address");
           $('#regresError1').show();
           $("#email-resp").focus();
           $("#email-resp").addClass('error_msg');
           return false;
        }
        else if(isEMail($("#email-resp").val())){
           $('#regresError1').html("Please enter valid email address");
           $('#regresError1').show();
           $("#email-resp").focus();
           $("#email-resp").addClass('error_msg');
           return false;
        }
        else if($("#password-resp").val() == "" ){
           $('#regresError1').html("Please enter the password");
           $('#regresError1').show();
           $("#password-resp").focus();
           $("#password-resp").addClass('error_msg');
           return false;
        }
        else if($("#password-resp").val().length<=5){
           $('#regresError1').html("Please use at least 6 characters in password");
           $('#regresError1').show();
           $("#password-resp").focus();
           $("#password-resp").addClass('error_msg');
           return false;
        }
        else if($("#Cnpassword-resp").val() == "" ){
           $('#regresError1').html("Please enter the confirm password");
           $('#regresError1').show();
           $("#Cnpassword-resp").focus();
           $("#Cnpassword-resp").addClass('error_msg');
           return false;
        }
        else if($("#Cnpassword-resp").val()!=$("#password-resp").val()){
           $('#regresError1').html("These passwords don't match. Try again?");
           $('#regresError1').show();
           $("#Cnpassword-resp").focus();
           $("#Cnpassword-resp").addClass('error_msg');
           return false;
        }
        else if($("#Stuidentname-resp").val()==""){
           $('#regresError1').html("Please enter your name");
           $('#regresError1').show();
           $("#Stuidentname-resp").focus();
           $("#Stuidentname-resp").addClass('error_msg');
           return false;
        }
        else if($("#mobile-resp").val()==""){
           $('#regresError1').html("Please enter your mobile no");
           $('#regresError1').show();
           $("#mobile-resp").focus();
           $("#mobile-resp").addClass('error_msg');
           return false;
        }
        else if((IsNumeric($("#mobile-resp").val())==false ||$("#mobile-resp").val().length<10)){
           $('#regresError1').html("Please enter valid mobile no");
           $('#regresError1').show();
           $("#mobile-resp").focus();
           $("#mobile-resp").addClass('error_msg');
           return false;
        }
        else if($("#Course-resp").val()=="0"){
           $('#regresError1').html("Please select your course");
           $('#regresError1').show();
           $("#Course-resp").focus();
           $("#Course-resp").addClass('error_msg');
           return false;
        }
        else if($("#state-resp").val()=="0"){
           $('#regresError1').html("Please select your state");
           $('#regresError1').show();
           $("#state-resp").focus();
           $("#state-resp").addClass('error_msg');
           return false;
        }
        else if($("#city-resp").val()=="0"){
           $('#regresError1').html("Please select your city");
           $('#regresError1').show();
           $("#city-resp").focus();
           $("#city-resp").addClass('error_msg');

           return false;
        }

        var mobile=$("#mobile-resp").val();
        var url="mobile-resp="+mobile+"&mode=optres";
        $.ajax({
                type: "post",
                url: "ajax.php",
                data: url ,
                async:false,
                error: function(data) {
                },
                success: function(data)
                {
                    alert($.trim(data));
                }
            });
           return true;
}

function isEMail(s){
    if(((s.indexOf("@"))==-1) || ((s.indexOf("."))==-1))
    {
        return true ;
    }else{
        return false;
    }
}

function loginSubmit_resp(){
        if($("#emailLogin-resp").val()==""){
           $('#loginresError1').html("Please enter your email address or roll no.");
           $('#loginresError1').show();
           $("#emailLogin-resp").focus();
           $("#emailLogin-resp").addClass('error_msg');
           return false;
        }
        else if($("#passLogin-resp").val() == ""){
           $('#loginresError1').html("Please enter the password");
           $('#loginresError1').show();
           $("#passLogin-resp").focus();
           $("#passLogin-resp").addClass('error_msg');
           return false;
        }
        return true;
}

function forgetPasswordresp(){
        if($("#fgemail-resp").val()==""){
           $('#fgerror1').html("Please enter your email address");
           $('#fgerror1').show();
           $("#fgemail-resp").focus();
           $("#fgemail-resp").addClass('error_msg');
		   return false;
        }
		else if(isEMail($("#fgemail-resp").val())){
           $('#fgerror1').html("Please enter valid email address");
           $('#fgerror1').show();
           $("#fgemail-resp").focus();
           $("#fgemail-resp").addClass('error_msg');
		   return false;
        }
        return true;
}