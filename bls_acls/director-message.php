<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/html5.js"></script>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
});
</script></head>
<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="director-message">
<aside class="banner-left banner-left-postion" style=" padding:10% 0px 0px 0px;">
<h2>Mentoring Doctors for<br>Holistic Success in</h2>
<h3 class="pg_medical_inline">PG Medical Examinations </h3>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li style="background:none;"><a href="dams.php" title="About Us">About Us</a></li>
<li><a title="Director's Message" class="active-link">Director's Message</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>Director's Message</h4>
<article class="showme-main">
<div class="about-content">
<p class="p_heading_inline">Dear Doctor</p> 
<p>At the outset we will introduce ourselves as the leaders in post graduate medical entrance business country wide. We understand that the students today have their main concern as AIPG(NBE/NEET) Pattern PG Entrance. AIPG(NBE/NEET) Pattern PG 2012 was conducted by NBE as a computer based test. This was first of its kind in India. DAMS has taken the front line in AIPG(NBE/NEET) Pattern PG preparation by introducing one of its kind AIPG(NBE/NEET) Pattern LIVE TESTS which are best practice tests of its kind developed by DAMS IT team to mirror the actual exam. In a recent survey conducted by Across PG, prominent medical student website, DAMS NLT was rated as best in business to prepare for AIPG(NBE/NEET) Pattern PG.</p>
<p>Paradigm shift in this business is our tablet based course called as iDAMS which has changed the way distant learning programmes are run in this business. </p>
<p>Following is the famous DAMS step by step approach for pg exam often explained by me on various platforms. Let me introduce myself, I am Sumer Sethi, and I have been a Top ranker in AIPG, AIIMS, PGI examinations before and I have authored more than 10 books for PG entrance including the best selling Review of Radiology, which is a country wide best seller. I am the Director and course planner at DAMS, Delhi Academy of Medical Sciences which is by far the most consistent result giver in the last 14 yrs or so. Further, ours is the only institute which is run by people who actually cracked PG entrance themselves, we are backed by our faculty of people who are from the MCQ generation and according to random survey by a publishing house done last year; DAMS has the best people teaching for them amongst all so called competitors.</p>
<p class="p_heading_inline">Let's come back to the main issue of getting through PG entrance.</p>
<p><strong>Number one</strong> thing of importance to understand is the value of positive thinking. That sounds so basic, but we insist the first thing for any student of DAMS is think positive. I don't want you to say anytime that you can't do it. That's the step number-1.</p>
<p><strong>Second is hard work in guided fashion.</strong> That's the key, you put in hard work and our team of teachers and motivators guide you at each step. We are the only institute which conducts special sessions on planning your studies and counseling you at each step. Our dedicated including myself is always available at the centre to individually cater to your problems. Our family has grown over last few years with more than 10,000 students joining us for classrooms all over the country last year but still we have somehow maintained out USP to be personal attention and pushing for the results. In our classroom series we have the best faculty drawn from AIIMS, MAMC, LHMC, KEM and other major colleges. Most of them are popular authors and extremely high yielding teachers. With us being the pioneers in the <strong>Regular course and Test and discussion</strong> concept, we are a natural choice for students preparing for PG entrance.</p>
<p><strong>Third is to devise a strategy to use your time effectively.</strong> We understand you don't have enough time now to reads text books like Harrisons now ;-) (Particularly if you didn't do so in your medical school). And we provide authentic notes specially prepared by our faculty basing them on standard books like Harrisons, Schwartz's, Williams, Robins and Ganong. Any senior of yours will vouch for our notes being comprehensive yet brief. This is where we differ from the competitors who just copy and paste full textbooks, we take out the juice from the text books and give it as Fast food solution for our students. We further provide our students with notes on new drugs, notes on oncology, Harrison updates and PSM recent data every year. In short, our notes are condensed version of the highest order books. Like our medicine is based on Harrison, Surgery is combination from Schwartz, Sabsiton and Bellie, Obs from Williams and Pediatrics from Nelson.</p>
<p><strong>Fourth is testing yourselves. Our Grand Test and online, NLTs on 3rd Sunday of each month.</strong> Our tests have an exclusive feature of new questions we don't copy and paste from USMLE questions banks, we make new questions. The joke in the PG entrance business is that only DAMS, NBE and AIIMS make new questions. We provide authentic references to our answers which is not so in any other coaching. Further in addition to All India Ranking of the students which is displayed on our websites and sent on sms to students we also display subject-wise distribution of each test on our site so that you can identify your weak topics. Our teachers enjoy the challenge of predicting the patterns and questions for the upcoming years which no other institute can do. We also have Subject wise Test and Explanation Series country wide, with detailed questions and answers to all subjects.</p>
<p>Finally our publications many of the last years toppers will vouch for our books on AIIMS the make or break idea was to read DAMS AIIMS solutions. We take out our own solutions of all AIIMS in partnership with CBS publishers and our books are available throughout the country. We bet anyone who doesn't read our review before AIIMS is at a loss. It provides you with cutting edge facts and authentic questions. Along with references you can trust. Unlike some of our competitors' we don't modify questions. We have our team of dedicated students who help us create an authentic recall. </p>
<p><strong>Further to add to our bouquet of course we have iDAMS, Postal and Online courses we follow our motto "if you can't come to DAMS, DAMS will come to you"</strong> Wishing you all the best for your examinations and we are proud to be partners in success with so many PG aspirant for last 15 years now. </p>
<p>15 years of excellence &amp; service</p>
<p>Sumer Sethi, MD Radiology</p>
<p>Director DAMS and topper in AIPG, AIIMS and PGI previously.</p>
<p>Author of Review of Radiology and many more books for PG entrance.</p>
<p>Faculty of National and International fame</p>
</div></article></div></aside>
<aside class="gallery-right">
<div id="store-wrapper">
<div class="dams-store-link"><span></span>About US</div>
<div class="dams-store-content">
<div class="inner-store">
<ul>
<li style="border:none;"><a href="dams.php" title="About DAMS">About DAMS</a></li>
<li><a href="dams_director.php" title="Director's Message" class="active-store">Director's Message</a></li>
<li><a href="about_director.php" title="About Director">About Director</a></li>
<li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
</ul></div></div></div>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
</body></html>