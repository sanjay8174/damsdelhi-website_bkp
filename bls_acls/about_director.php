<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include '../header.php'; ?>
<?php $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('images/background_images/<?php echo $getAboutusContent[0][0];  ?>') right top no-repeat;width:100%;float:left;height:318px;">
<aside class="banner-left">
<?php  echo $getAboutusContent[0][1]; ?>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
<li><a title="About Director" class="active-link">Our Leadership</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<style>
#aboutdirectorcontent ul li {
list-style-type: disc;
margin-left: 30px;
padding: 4px;}
</style>
<div id="aboutdirectorcontent" class="inner-left-heading responc-left-heading">
<article class="showme-main">
<?php echo $getAboutusContent[0][2]; ?>
</article></div><div class="fullwidth_director">
    <div class="halfwidth_director">
    <img src="https://damsdelhi.com/images/DrRajivBhagi(CEO).png">
       <div class="director_Detail">Dr. Rajiv Bhagi
           <!--<div class="director_position">Founder CEO</div>-->
           
       </div>
</div>
    <div class="halfwidth_director">
    <img src="https://damsdelhi.com/images/DrSumerSethi(Director).png">
    <div class="director_Detail">Dr. Sumer Sethi
        <!--<div class="director_position">Director</div>-->
        
    </div>
</div>
       
    </div>
    <div class="fullwidth_director">
        <div class="halfwidth_director" style="float:left;">
    <img src="https://damsdelhi.com/images/DrDeeptiSethi(Director).png">
       <div class="director_Detail">Dr. Deepti Sethi
           <!--<div class="director_position">Director</div>-->
       </div>
</div>
<!--<div class="halfwidth_director">
    <img src="https://damsdelhi.com/images/DrRidhiBhagi(Director).png">
       <div class="director_Detail">Dr. Ridhi Bhagi
           <div class="director_position">Director</div>
           
       </div>
</div>-->
    </div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div> </div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<style>
    .halfwidth_director{
        display: inline-block;
        width: 48%;
        float: left;
        position: relative;
        margin-bottom: 4%;
        border: 2px solid #58B220
    }
    .halfwidth_director img{
           display: flex;
    height: 100%;
    width: 100%;
        
    }
    
.director_Detail {
    background: #58b220 none repeat scroll 0 0;
    border: 2px solid #58b220;
    bottom: -58px;
    color: #fff;
    font-size: 18px;
    height: 40px;
    left: -2px;
    padding: 6px 0;
    position: absolute;
    text-align: center;
    width: 100%;
}
    .fullwidth_director .halfwidth_director:last-child{
        float: right
        
    }
    .director_position{
        color: #eee
    }
    .fullwidth_director {
    float: left;
    width: 48%;
}
.fullwidth_director:last-child{margin-left: 10px;}
</style>
</body></html>