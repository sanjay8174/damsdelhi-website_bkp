<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 3;
$courseNav_id = 9;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mds-quest-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MDS Quest Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest ">MDS Quest </a></li>
          <li><a title="Test &amp; Discussion" class="active-link">Test &amp; Discussion</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Test &amp; Discussion Course (Via Satellite) <span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <aside class="course-detail paddin-zero">
                <p>DAMS is the pioneer MD/MS Entrance coaching institute which started this course in Delhi. We are the first medical coaching institute in the country to start this course. This is a specialized course which is targeted at a group of students who have already done some studying on their part and are looking to upgrade themselves as well as sharpen their skills for MDS entrance examinations. This course will be available in all our Satellite based centres. <br>
                  <br>
                  We assure to provide best coaching for AIPG &amp; AIIMS MDS examinations by our sincere effort. This course runs once a week, and in this course we have a fixed schedule of tests which is divided subject &amp; topic wise and given to you on the day one of enrolment. Every week you have a 1hour test on prescheduled topic, which you would have prepared in that week and followed by a brainstorming discussion by the famous DAMS faculty for around 6 hours. This solves your doubts and emphasizes on the key points which are essential for your success. In short taking this course ensures that your brain and neurons are stimulated with maximum inputs from your famous teachers and tricky questions. Another thing to note is that your so called "Guide books" often are written by Interns or fresh medical graduates and have many mistakes. Our teachers being in this business for so long correct those and highlight the mistakes in your guide books and save you from being misguided. If you are looking for success in the MDS Entrance Exam we welcome you into our Test &amp; Discussion course, which features our exclusive selected faculty and specially designed test papers. Designed to provide highly motivated students with opportunities to broaden and enrich their academic experience, this course is geared towards deriving the desired final outcome - a good score in MDS entrance tests. </p>
                <div class="franchisee-box paddin-zero">
                  <p><span class="price_font">Dental Career Counselling for MDS :</span> 09999158131</p>
                </div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script> 
</body>
</html>