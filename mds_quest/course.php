<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = 1;
$courseNav_id = 1;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/AIPG(NBE/NEET) Pattern) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<?php include 'md-ms-big-nav.php'; ?>
<aside class="banner-left banner-l-heading">
<h3>Trusted Partners for your PGME Preparation
<span>With Highest Percentage Selection in Top Institutes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
<div class="wrapper">
<aside class="content-left">
<div class="course-box">
<ul class="idTabs responce-show">
<li><a href="#MDCourses">MD/MS Courses</a></li>
<li><a href="#DNB">DNB</a></li>
</ul>

<div id="MDCourses">
<div class="satellite-content">
<div class="idams-box1">
<p>Things have changed in the recent years with new pattern in AIPG with NBE conducting the exam. In 2012, NEET was conducted by NBE and 2013 onwards AIPG will be conducted by NBE. This has resulted in gross change in the pattern of examination and DAMS is the only institute offering courses on the latest AIPG (NBE) pattern.  With our special offering like Classroom programs based on NEET PG,  Special classes on Visual (DAMS Visual Treat (DVT Sessions) capsule and online GT based on new pattern we are only trusted partner for  PGME examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG (NBE), NEET, AIIMS, PGI,  UPSC and DNB. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for NEET, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.<br><br>
With our presence across the country we are the current leaders in the business of <strong>PG Medical Entrance Coaching. We have the largest foot print across India allowing our students to compete with peers across India, and we also boost of largest consortium of faculty members ever ready to help and mentor young budding Doctors in achieving their dreams.</strong>
</p></div>
</div></div>

<div id="DNB">
<div class="satellite-content">
<div class="idams-box1">
<p>DNB has now become an important choice for medical students with various authorities recognizing the equivalence of DNB with other post graduate qualifications and centralised counselling. Most popular course amongst students with comprehensive coverage of ALL SUBJECTS is our REGULAR COURSE. We are the only medical coaching institute which teaches all subjects in manner that is required for your primary DNB examination. This course starts in Feburary and we have a fresh batch every month. Classes are held on weekends and are based on advanced audio-visual learning modules. This course further has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance Examination.</p>
</div>

<aside class="how-to-apply paddin-zero">
<div class="how-to-apply-heading"><span></span> Course Highlights</div>
<div class="course-detail-content">

<span class="gry-course-box">We admit only limited number of students in each batch. You will receive excellent individual attention. This is a big advantage to you as you can learn better in a small class with limited students. Our class rooms are air conditioned and have state of the art audiovisual facilities.
</span>

<span class="blue-course-box">You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can. They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books..</span>

<span class="gry-course-box">We provide detailed and easy to understand notes. </span>

<span class="blue-course-box">We conduct periodic tests, evaluate your performance and conduct counselling sessions. This will help you in your regular preparation for your exam.</span>
</div>
</aside>
</div>
</div>
</div>

<?php include 'middle-accordion.php'; ?>
<?php include 'recentAchievement.php'; ?>


</aside>

<aside class="content-right">
<?php include 'right-accordion.php'; ?>
<div class="national-quiz-add">
<a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a>
</div>
<?php include 'newsRight.php'; ?>
<?php include 'studentInterview.php'; ?>

</aside>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="./gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>
