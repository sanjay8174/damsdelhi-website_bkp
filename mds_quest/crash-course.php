<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Crash Course for PG Medical Entrance Examination, AIPG(NBE/NEET) Pattern PG</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
//	$('div.accordionButton').click(function() {
//		$('div.accordionContent').slideUp('normal');	
//		$(this).next().slideDown('normal');
//	});		
//	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<?php include 'enquiry.php'; ?>

<?php include 'coures-header.php'; ?>


<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">

<?php include 'md-ms-big-nav.php'; ?>

<aside class="banner-left banner-left-postion">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<?php include'md-ms-banner-btn.php'; ?>

</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li style="background:none;"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Crash Course" class="active-link">Crash Course</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>Crash Course <div class="book-ur-seat-btn book-hide"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div></h4>
<article class="showme-main">

<aside class="about-content">
<p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>

<p>Crash course starts on August / September month of every year. The duration of this course is 2 months; this course comprises of exclusive study material and will be based on class room discussions in depth &amp; revision tests. If you missed out on joining the coaching earlier or you just want to brush up everything with our famous DAMS Faculty, this is a course tailor made for you.</p>
</aside>


<aside class="how-to-apply">
<div class="how-to-apply-heading"><span></span> Course Highlights :-</div>

<ul class="benefits">
<li><span></span>We deal with all subjects in the crash course. ONLY DAMS COVERS ALL SUBJECTS IN THE CRASH COURSE.</li>
<li><span></span>We take frequent tests also in the crash course, we take our crash course seriously and in fact we produced quite a few rankers in AIIMS from this series this year.</li>
<li><span></span>Our Grand Tests and Bounce back tests are included.</li>
<li><span></span>Exclusive study material for crash course is also provided.</li>
<li><span></span>Things are done fast but in ENOUGH DETAILS.</li>
<li><span></span>GET THE COMPETITIVE EDGE BY JOINGING THIS COURSE</li>
</ul>

</aside>

</article>
<div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
</div>

</aside>

<aside class="gallery-right">

<?php include 'md-ms-right-accordion.php'; ?>

<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->

</aside>
</section>
</div>
</div>
</section>

<!-- Midle Content End Here -->


<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->

</body></html>