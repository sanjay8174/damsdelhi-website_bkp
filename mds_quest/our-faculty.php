<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
</head>
<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="dams-aboutus">
<aside class="banner-left banner-left-postion" style=" padding:10% 0px 0px 0px;">
<h2>Quality Educators &amp; Mentors </h2>
<h3 class="pg_medical_inline">With Passion For Teaching </h3>
</aside>
</article>
</div>
</section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
<ul>
<li class="bg_none"><a href="dams.php" title="About Us">About Us</a></li>
<li><a title="Our Faculty" class="active-link">Our Faculty</a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>Our Faculty</h4>
<article class="showme-main">
<div class="about-content">
<p>The faculty at Delhi Academy of Medical Sciences (DAMS) are its real assets. They are specialists in their subjects and many of them are authors of well-known text books too. Our students are excelling largely due to DAMS unique teaching methodology, commitment and dedication of our faculty. Unique feature of our faculty is that they are people with single goal in their life, that is to help you clear your PG medical entrance exam. And please note we have centralized faculty, that is we keep the teachers same wherever you join us. That is a unique feature offered only exclusively by DAMS.We further believe that every student has the potential to succeed, and this has to be brought to the fore through the sustained hard work of the student with appropriate guidance. The magnificence of a pearl can only be appreciated when it has been prized out of the oyster and polished to perfection.  We believe that when you join us, we have the responsibility of a parent and job of a teacher to get you to your goal.</p>
<ul class="about-list">
<li>1. MCQ specialists</li>
<li>2. Different teachers for  each subject</li>
<li>3. Same Faculty everywhere</li>
<li>4. Many authors of popular MCQ books in the team</li>
<li>5. Dedicated team of previous years toppers and teachers creating Tests for you to make you perfect for your final exam</li>
</ul>
</div>
</article>
</div>
</aside>
<aside class="gallery-right">
<div id="store-wrapper">
<div class="dams-store-link"><span></span>About US</div>
<div class="dams-store-content">
<div class="inner-store">
<ul>
<li style="border:none;"><a href="dams.php" title="About DAMS">About DAMS</a></li>
<li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
<li><a href="about_director.php" title="About Director">About Director</a></li>
<li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="dams_faculty.php" title="Our Faculty" class="active-store">Our Faculty</a></li>
</ul>
</div>
</div>
</div>
<?php include 'enquiryform.php'; ?>
</aside>
</section>
</div>
</div>
</section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
$('#student-registration').click(function() {
$('#backPopup').show();
$('#frontPopup1').show();       
});
$('#student-registration-close').click(function() {
$('#backPopup').hide();
$('#frontPopup1').hide();
});
$('#student-login').click(function() {
$('#backPopup').show();
$('#frontPopup2').show();
});
$('#student-login-close').click(function() {
$('#backPopup').hide();
$('#frontPopup2').hide();
});
$('#cloud-login').click(function() {
$('#backPopup').show();
$('#dams-cloud').show();
});
$('#cloud-login-close').click(function() {
$('#backPopup').hide();
$('#dams-cloud').hide();
});
$('#student-enquiry').click(function(e) {
$('#backPopup').show();
$('#quickenquiry').show();
});
$('#student-enquiry-close').click(function() {
$('#backPopup').hide();
$('#quickenquiry').hide();
});	
$('#fg-password').click(function() {
$('#backPopup').hide();
$('#frontPopup2').hide();
$('#backPopup').show();
$('#forgotpassword').show();
});
$('#fg-close').click(function() {
$('#backPopup').hide();
$('#forgotpassword').hide();
});
$('#fg-password2').click(function() {
$('#backPopup').hide();
$('#dams-cloud').hide();
$('#backPopup').show();
$('#forgotpassword2').show();
});
$('#fg-close2').click(function() {
$('#backPopup').hide();
$('#forgotpassword2').hide();
});
});
</script>
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
</body>
</html>