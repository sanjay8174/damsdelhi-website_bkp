<!DOCTYPE html>
<?php error_reporting(0);?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" rel="stylesheet" type="text/css" />
   
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<style>
    .downcrse {
    border-radius: 10px;
    background-color: #399DD7;
    color: white;
    float: right;
    border:none;
    padding:10px;
    margin-right: 15px;
    position:relative;
}
.gif-img{
    position: absolute;
    right: -13px;
    top: -7px;
    width:28px;
}
.gif-img img{
    width: 100%;
}
.course-box p{
    width: 100%;
display: inline-block;
margin-top:20px;
}
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="inner-bg">
<?php include 'registration.php';
$courseNav_id = 9;
?>
 <?php include 'social-icon.php'; ?>   
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
    $path = constant::$path;
?>
  <script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js?v=1"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
    <!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<?php $getNavContent=$Dao->getCourseNavContent($courseNav_id); ?>
<!--<article class="mds-quest-banner">-->
<article style="width:100%;float:left;height:318px;background: url('<?php echo $path."/images/background_images/".$getNavContent[0][2]; ?>') right top no-repeat;">
<?php include'mds-big-nav.php'; ?>
<aside class="banner-left">
    <?php echo $getNavContent[0][1]; ?>
</aside>
<?php include'mds-banner-btn.php'; ?>
</article>
</div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
<div class="wrapper">
<aside class="content-left">
    <div class="course-box">
        <div style=""><a   href="images/MDSQUESTcoursesdetail.pdf" target="blank" download><button class="downcrse" >DOWNLOAD COURSE DETAILS</button></a></div>
        <div style=""><a   href="images/AIIMS_and_PGI_Test_&_Discussion_Course.pdf" target="blank" ><button class="downcrse" >AIIMS and PGI  Test & Discussion Course <span class="gif-img"><img src="./images/blink.gif"></span></button></a></div>
 <?php echo $getNavContent[0][0]; ?>
        <div style="clear:both"></div>
</div>

<?php /* include 'mds-middle-accordion.php';*/ ?>
<?php include 'middle-accordion.php'; ?>
<?php include 'recentAchievement.php'; ?>
 </aside>

<aside class="content-right">
<?php include 'right-accordion.php'; ?>
<?php /* include 'mds-right-accordion.php'; */ ?>
    <div class="outer-animation" align="center" style="margin: -15px 0 5px 0; display: inline-block; width: 100%;">
		<!--<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 3</div>
  <img src="images/MDS_AIIMS_AND_MAY_Poster.jpg" style="width:100%">
  <div class="text">Caption Text</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 3</div>
  <img src="images/SCHEDULE.jpg" style="width:100%">
  <div class="text">Caption Two</div>
</div>

<a style="left:0;text-decoration:none;" class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a style="text-decoration:none;" class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
</div>-->
		
		<div class="slideshow-container">
<div class="mySlides fade">
    <img src="images/collarge.jpg" style="width:100%">
</div>
 <div class="mySlides fade">
    <img src="images/team_work_poster.jpg" style="width:100%">
</div>
<div class="mySlides fade">
  <img src="images/4th_january_poster.jpg" style="width:100%">
</div>

<div class="mySlides fade">
  <img src="images/Demo_sessions_poster.jpg" style="width:100%">
</div>
   


</div>
<div style="text-align:center;margin-top:5px;">
  <span class="dot"></span> 
  <span class="dot"></span>
  <span class="dot"></span> 
  <span class="dot"></span>
 
</div>
		
		
<!--		<img src="images/MDS_AIIMS_AND_MAY_Poster.jpg" style="height: 100%;width: 100%;">-->
	</div>
<?php include 'newsRight.php'; ?>
    
    <div class="outer-part-carasal">
        <div class="n-heading">Testimonials </div>
        <div class="news-content-box" style="border: 1px solid #00a651;padding: 0px;box-sizing: border-box;width: 100%;height:auto;">
            <div class="outer-part-slide">
                <div id="owl-demo1" class="owl-carousel owl-theme" style="display:block;"> 
                    <div class="item">
                        <img src="images/owl1.jpg">
                    </div>
                     <div class="item">
                        <img src="images/owl2.jpg">
                    </div> 
                    <div class="item">
                        <img src="images/owl3.jpg">
                    </div>
                    <div class="item">
                        <img src="images/owl4.jpg">
                    </div>
                    <div class="item">
                        <img src="images/owl5.jpg">
                    </div>
                    <div class="item">
                        <img src="images/owl6.jpg">
                    </div>
                     <div class="item">
                        <img src="images/owl7.jpg">
                    </div> 
                    <div class="item">
                        <img src="images/owl8.jpg">
                    </div>
                    <div class="item">
                        <img src="images/owl9.jpg">
                    </div>
                    <div class="item">
                        <img src="images/owl10.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn1.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn2.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn3.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn4.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn5.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn6.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn7.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn8.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn9.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn10.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn12.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn13.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn14.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn15.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn16.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn17.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn18.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn19.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn20.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn21.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn22.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn23.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn24.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn25.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn26.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn27.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn28.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn29.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn30.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn31.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn32.jpg">
                    </div>
                    <div class="item">
                        <img src="images/bn33.jpg">
                    </div>
              </div>
            </div>
        </div>
    </div>
    
<?php include 'studentInterview.php'; ?>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $('#owl-demo1').owlCarousel({
    loop:true,
    autoplay:true,
    items:1,
    responsiveClass:true,
    nav:false,
    autoplayHoverPause: true,
    dots:false
})
    </script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
	
<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 6000); // Change image every 2 seconds
}
</script>
<?php mysql_close($myconn);?>
</body>
</html>
