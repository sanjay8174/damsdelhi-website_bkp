<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="big-nav">
<ul>
<li class="face-face"><a href="mci_screening.php" title="Face To Face Classes" <?php if($pageName=="mci_screening.php" || $pageName=="mci-reguler-course.php" || $pageName=="mci-crash-course.php"){?>class="face-face-active"<?php }?>>Face To Face Classes</a></li>
<li class="satelite-b"><a href="mci-satellite-classes.php" title="Satellite Classes" <?php if($pageName=="mci-satellite-classes.php"){?>class="satelite-b-active"<?php }?>>Satellite Classes</a></li>
<li class="t-series"><a href="mci-test-series.php" title="Test Series" <?php if($pageName=="mci-test-series.php" || $pageName=="mci-online-test-series.php" || $pageName=="mci-postal-course.php"){?>class="t-series-active"<?php }?>>Test Series</a></li>
<li class="a-achievement"><a href="achievements.php?c=2" title="Achievement" <?php if($pageName=="mciscreening_sep_2013.php" || $pageName=="mciscreening_mar_2013.php" || $pageName=="mciscreening_sep_2012.php"){?>class="a-achievement-active"<?php }?>>Achievement</a></li>
</ul>
</div>
