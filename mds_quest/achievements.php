
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>

<body class="inner-bg">
<?php



error_reporting(0);
$courseId = $_REQUEST['c'];
$courseNav_id = 12; ?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
      $path = constant::$path;
?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
      <?php $getYear = $Dao->getYear($courseId);
        $getNavContent=$Dao->getCourseNavContent($courseNav_id);
        $getCourse = $Dao->getCourse();
        $getCourse = $Dao->getCourseName($courseId); ?>

        <article style="width:100%;float:left;height:318px;background: url('<?php echo $path."/images/background_images/". $getNavContent[0][2]; ?>') right top no-repeat;">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left">
       <?php  echo $getNavContent[0][1]; ?>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->

<input type="hidden" value="0" id="selectedEvent"/>
<input type="hidden" value="0" id="selectedEventName"/>

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php?c=<?php echo $courseId ?>" title="<?php echo $getCourse[0][1]?>"><?php echo $getCourse[0][1]?></a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">

                <?php for($i=0;$i<sizeof($getYear);$i++){?>
              <li  id="accor<?php echo ($i+1); ?>" onClick="ontab('<?php echo ($i+1); ?>');" <?php if($i==0){ ?>class="light-blue border_none"<?php } ?>><span id="accorsp<?php echo ($i+1); ?>" <?php if($i==0){ ?>class="bgspan"<?php }else{ ?>class="bgspanblk"<?php }?>>&nbsp;</span><a href="javascript:void(0);" title="<?php echo $getYear[$i][0];?>"><?php echo $getYear[$i][0];?></a>
                <ol <?php if($i==0){ ?>class="achievment-inner display_block"<?php }else{ ?>class="achievment-inner display_none"<?php }?> id="aol<?php echo ($i+1); ?>">
                    <?php $getEvent= $Dao->getEvent($courseId,$getYear[$i][0]);
                    for($j=0;$j<sizeof($getEvent);$j++){
                        if($j==0){?>
                    <input type="hidden" value='<?php echo $getEvent[$j][1]; ?>' id="initialEId"/>
                    <input type="hidden" value='<?php echo $getEvent[$j][0]; ?>' id="initialEName"/>
                    <input type="hidden" value="<?php echo $getEvent[$j][2];?>" id="initialEYear"/>
                    <input type="hidden" value="<?php echo $courseId;?>" id="initialCId"/>
                   <?php     }

                        ?>

                    <li <?php if($j==0){?>class="active-t"<?php }else{?>class=" " <?php }?>><span class="mini-arrow">&nbsp;</span><a href="javascript:void(0)" onclick="getEventData('<?php echo $getEvent[$j][0]; ?>',<?php echo $getEvent[$j][1];?>,<?php echo $courseId;?>,<?php echo $getEvent[$j][2];?>)" title="<?php echo $getEvent[$j][0];?>"><?php echo $getEvent[$j][0];?></a></li>
                    <?php }?>
                </ol>
              </li>
               <?php }?>



            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4 style="display:inline;" id="eventName">PGI MAY 2014</h4>&nbsp;&nbsp;<h4 style="display:inline;" id="eventYear">PGI MAY 2014</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" id="eventName1"> PGI MAY 2014</a></li>
                <li><a href="javascript:void(0)" class="" onclick="getVideoData(<?php echo $courseId;?>)" title="Interview" id="interview">Video Interview</a></li>
              </ul>


              <div id="jquery">
              </div>



              <div id="official">

             </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="js/achievement.js"></script>
<script>
  $(document).ready(function(){
$("li").click(function() {
    $(this).removeClass("active-t");
    $(this).toggleClass("active-t");
    $(this).siblings().removeClass("active-t");
});
});
</script>
</body>
</html>
