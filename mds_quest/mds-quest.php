<!DOCTYPE html>
<?php
$course_id = '3';
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MDS Quest Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false);">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="mds-quest-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
        <!--<p>Ancient times Guru Shishya Pranali of imparting education was by <br />
the Guru at Guru's premises /Ashrams. Ages passed, social pattern<br />
changed, villages turned to towns, towns to cities &amp; metros.</p>--> 
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>MDS Quest</h3>
        <p>Delhi Academy of Medical Sciences (DAMS) a well-established name in the field of PG Medical Entrance Education for over 15 years now. Today under the leadership of Dr Sumer Sethi, Radiologist and pervious topper in AIPG/ AIIMS. DAMS is recognized for its impeccable foresight, enviable expertise and innate acumen. We are the number 1 coaching institute for the PG medical entrance examinations AIPG, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. Today we see a lacuna for BDS graduates who are preparing for MDS entrance and there is no focussed institute in India which genuninely helps dental graduates in their quest for MDS. DAMS with its unique legacy promises to bring quality and much needed classroom programme for MDS entrance. We are confident that with our faculty force we will make a difference in your career as we have done for doctors for last 15 years.<br>
          <br>
          The Delhi Academy of Medical Sciences (DAMS) was established to create a bench-mark institution to achieve excellence in the toughest competitive exam in the country, i.e. PG Medical Entrance Exam. Over this long period, the DAMS has evolved into a unique fraternity of educators and students striving together, year after year, in pursuit of a single goal. With a passion to excel, the Delhi Academy of Medical Sciences has raged with the dynamism of a river which constantly renews itself and yet remains unchanging in its resolve to reach its ultimate destination.<br>
          <br>
          The institute's aim is not only to provide specific knowledge and strengthen the foundation of the students in PG Medical Entrance, but also to infuse them with determination to crack the Entrance Exams at post graduation level. To explore the potential of the students and to help them master the subject, We, at DAMS have developed extensive scientific teaching as well as testing methods. We also have special sessions on mental training required to develop the so called “killer instinct”. </p>
      </div>
      <?php include 'mds-middle-accordion.php'; ?>
      <div class="recent-pg-main">
        <div class="recent-pg-heading">
          <a href="../photo-gallery.php" class="photogal"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
        <ul id="recent">
          <li id="li0"> <img src="images/topper/8.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Anusar Gupta</span> <span class="_rank">Rank : <strong>37</strong></span> <span class="_field">AIPGE (2014)</span> </li>
          <li id="li1"> <img src="images/topper/9.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Chandan Jain </span> <span class="_rank">Rank : <strong>2</strong></span> <span class="_field">AIPGE (2014)</span> </li>
          <li id="li2"> <img src="images/topper/10.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Khushboo Rathore</span> <span class="_rank">Rank : <strong>98</strong></span> <span class="_field">AIPGE (2014)</span> </li>
          <li id="li3"> <img src="images/topper/11.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Prateek Punyani</span> <span class="_rank">Rank : <strong>50</strong></span> <span class="_field">AIPGE (2014)</span> </li>
        </ul>
        <div class="view-all-photo"><a href="mds-aipge-2014.php" title="View All">View&nbsp;All</a></div>
      </div>
    </aside>
    <aside class="content-right">
      <?php include 'mds-right-accordion.php'; ?>
      <?php
include 'openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div class="news_content_inline">
            <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if ($newsDetail[$j] != '') { ?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if ($newsDetail[$j] != '') { ?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php
                        }
                        ?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="mds-news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i=0; for($i=0;$i<ceil($count/3);$i++){  ?>
                <li id="u<?php echo $i; ?>" <?php if($i=='0'){ ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count/3); ?>);"></li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
        
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" style="display:block;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/MKs-1DttP4c" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd1" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/GUMZPGghqco" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd2" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/xtAUAo5NKR8" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd3" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/7X1ZGMKs0NQ" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages --> 

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');   
	   }
	 }
   });
   $("#next").click(function(){	 
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }	 
   });
   
//   $('div.accordionButton').click(function() {
//		$('div.accordionContent').slideUp('normal');	
//		$(this).next().slideDown('normal');
//	});		
//	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });	
   
   window.onload = function()
   {	  
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;		
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;			
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");        	
	       l++;	
        }
	    //function setnews()
//		{
//		   n=n%count3;		
//		   $("#ul"+n).slideUp(100,'swing');
//		   $("#u"+n).removeClass("current");
//	       var j=(n+1)%count3;
//		   $("#ul"+j).slideDown(4000,'linear').animate({zindex:'2'}, 100);
//		   $("#u"+j).addClass("current");        	
//	       n++;	
//        }
     videointerval = setInterval(setvideo, 5000);
	 //newsinterval = setInterval(setnews, 9000);
   }
 });
 function news(val)
 {
   //setTimeout('newsinterval', 9000);
   if(val=='0')
   {	   
	  $("#ul1").hide();
	  $("#u1").removeClass("current"); 
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");   
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current"); 
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
  // n=val;
//   newsinterval = setInterval(setnews, 9000);  
 }
 
 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {	   
      $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd0").fadeIn('fast','linear');
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd1").fadeIn('fast','linear');
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd2").fadeIn('fast','linear');
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").fadeIn('fast','linear');
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);  
 }

 function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {   
       if(val!=d)
	   { 
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }      
   }
   
   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script>
<?php mysql_close($myconn);?>
</body>
</html>
