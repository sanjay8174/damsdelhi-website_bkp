<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <div class="coures-list-box-content" id="part1">
          <div class="mrcp_boxess" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/Saleem-Khan.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Saleem Khan</h1>
            <p class="mrcp_dr_sbtl">Course Director</p>
            <p class="mrcp_dr_sbtl">MRCOG (UK), DOWH <br> Obstetrician Gynaecologist - North Manchester General Hospital, Manchester (UK)<br> Chairperson of International Foundation for Mother and Child Health UK </p>
            <p style="font-size:14px;">Dr Khan has been involved in conducting MRCOG COURSES internationally for last 5 years. He is  also involved in facilitating O&G specialist training in the UK and involved in teaching O&G specialist trainees at the North West Deanery of England. </p>
        </div>
</div>

<div class="mrcp_boxess" id="part1">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/Matthew-Izett.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Matthew Izett</h1>
            <p class="mrcp_dr_sbtl">Course Co-Director</p>
            <p class="mrcp_dr_sbtl">Obs & Gyn Specialist Registrar,<br> Kings College Hospital, London Deanery, London UK</p>
            <p style="font-size:14px;">Dr Izett is currently a registrar at Kings College Hospital, London. His undergraduate training was at the Peninsula Medical School, Exeter where he took a special interest in medical education undertaking a research project on interactive learning resources. He has been carrying on regular teaching for medical undergraduates at Kings Medical College, London. He has also been involved in post graduate exam preparation for the MRCOG Part 1 for London Deanery trainees and last year the ABMA MRCOG Part 1 course in Dubai, UAE.</p>
        </div>

</div>      </div>
<div class="mrcp_boxess" id="part1">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/Jo-Hunter.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Jo Hunter</h1>
            <p class="mrcp_dr_sbtl">Course Co-Director</p>
            <p class="mrcp_dr_sbtl">O&G Specialist Registrar <br>Guys and St Thomas' London Deanery, London UK</p>
            <p style="font-size:14px;">Dr Hunter is currently working as a specialist registrar at Guys and St Thomas' Hospital. She has taken a keen interest in both mentoring and teaching After completing her undergraduate degrees at Goldsmiths University of London.. She is currently involved in pre medical school and medical school mentoring projects. Dr Hunter teaches regularly on the MRCOG Part 1 revision group for London deanery trainees. She has also taught on the ABMA MRCOG Part 1 course in Dubai, UAE. </p>
        </div>

</div>
    </div>
    </div>
  </div>
  
  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course highlight</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
       <p class="course_dtlsp">
        Join us for an interactive, engaging and condensed preparatory course to help you pass the MRCOG Part 1.  This course will be three days of lectures, group work and practice questions used to help candidates pass the Part 1.
        </p>
        <p class="course_dtlsp">
The exam has undergone significant change in question format recently and your course tutors have an in depth knowledge of these changes and will help signpost you to study techniques, resources and specific areas to help you focus your learning.  Additionally they will be able to provide you with the latest style of questions found in the exam that are not widely available yet in published preparatory material.
       </p>
       <p>Come join us to help you prepare and pass the MRCOG Part 1 in an engaging and open learning environment.
       </p>
      </div>
    </div>
  </div>
 </div>
 
 <div class="pg-medical-main" style="display:none;">
  <div class="pg-heading"><span></span>Course Strengths</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Small teaching faculty with an active interest in medical education.</li>
          <li><span class="sub-arrow"></span>Constantly updated teaching material.</li>
          <li><span class="sub-arrow"></span>Multiple Choice Question based format to mirror the real exam.</li>
          <li><span class="sub-arrow"></span>Non-confrontational teaching style.</li> 
          <li><span class="sub-arrow"></span>Use of repetition of difficult concepts to aid learning.</li>
          <li><span class="sub-arrow"></span>Clear, well designed visual teaching aids.</li>          
        </ul>
        <p class="course_dtlsp"><strong><br>Come join us to help you prepare and pass the MRCOG Part 1 in an engaging and open learning environment.</strong></p>
      </div>
    </div>
  </div>
 </div>
</div>