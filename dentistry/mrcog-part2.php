<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>MRCOG Coaching Institute, MRCOG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2>MRCOG - UK</h2>
          <!--<h3>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS -UK</h3>-->
          <h3>ABMA MRCOG Exam Preparatory Course</h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box mrcog_crsbox ">
       <ul class="idTabs responce-show">
        <li><a href="index.php?c=6&n=" >PART 1</a></li>
        <li><a href="#part2">PART 2</a></li>
       </ul>
      
      </div>
        
       <div class="course-box mrcog_crsbox ">
      <div>
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;">
          <h3>MRCP PART-2 PREPARATION COURSE-DELHI<span class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
        <h3 style="font-size:34px;">PART-2 Written COURSE and OSCE Orientation - New Delhi</h3>
        <div class="world_class"><strong>ARLINGTON BRITISH MEDICAL ACADEMY - UK: WORLD CLASS CREDENTIALS</strong></div>
        <p>After huge success of MRCOG Part 2 Written Course in 2014 & Jan 2015, ABMA-UK in partnership with Getwell Medical Center - Dubai is proud to announce once again very popular MRCOG Part II Written Exam Preparation Course in Dubai. The tutors are well established lecturers for the MRCOG Courses in UK.
        </p>
          </div>
      <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <div class="mrcp_boxess" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/Saleem-Khan.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Saleem Khan</h1>
            <p class="mrcp_dr_sbtl">Course Director</p>
            <p class="mrcp_dr_sbtl">MRCOG (UK), DOWH <br> Obstetrician Gynaecologist - North Manchester General Hospital, Manchester (UK)<br> Chairperson of International Foundation for Mother and Child Health UK </p>
            <p style="font-size:14px;">Dr Khan has been involved in conducting MRCOG COURSES internationally for last 5 years. He is  also involved in facilitating O&G specialist training in the UK and involved in teaching O&G specialist trainees at the North West Deanery of England. </p>
        </div>
</div>

<div class="mrcp_boxess">
    <div class="mrcp_dr_img dr_imgbordr"><img style="margin-top:-7px; height:110%" src='images/JamesStuartRowland.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr James Stuart Rowland </h1>
            <p class="mrcp_dr_sbtl">MRCOG</p>
            <p style="font-size:14px;">Dr Rowland is a Senior Registrar in O&G working in the North West of England. He is currently undertaking RCOG ATSM training in benign gynaecology and advanced labour ward practice. He also has the gynaecology intermediate scan qualification in progress. Dr Rowland is very keen to promote medical education and is also working towards an MSc in Medical Education. Dr Rowland is an accredited Problem Based Learning tutor & Communication Skills tutor at the University of Manchester.</p>
        </div>

</div>      </div>

    </div>
    </div>
  </div>
         
  
  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Format:</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <div class="coures-list-box-content">
       <p class="course_dtlsp">
        An intensive two days EMQ, MCQ and SAQ course with OSCE orientation to be held in Dubai by UK tutors. Our course is totally focused on achieving MRCOG Part II ecam success.
        </p>
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>An exam-oriented course</li>
          <li><span class="sub-arrow"></span>We cover the entire syllabus</li>
          <li><span class="sub-arrow"></span>New! Comprehensive coverage of EMQ's (Extended Matching Questions)</li>
          <li><span class="sub-arrow"></span>Extended exam-based MCQ's and EMQ's covering topics from recent and past exams</li> 
          <li><span class="sub-arrow"></span>Comprehensive Short Answer Questions (SAQs) sessions with common question techniques and model answers</li>
          <li><span class="sub-arrow"></span>Techniques for intensive revising as well as effective mark-scoring in MCQ's, EMQ's ans SAQ's<br>Credible OSCE orientation to plan ahead after passing Written exam</li> 
          <li><span class="sub-arrow"></span>We supply comprehensive delegate notes</li>
        </ul>
      </div>
        
        
      </div>
    </div>
  </div>
 </div>
          
 

        
</div>
        
       </div>
      </div>
   </div>   
      
    </aside>    
    <aside class="content-right">
        <div class="content-royal-college res_css">
            <img src="images/logo-royal-college.gif" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF OBSTETRICS AND GYNAECOLOGISTS OF THE UNITED KINGDOM</p>
        </div>
        <div style="display:none;" class="content-date-venue res_css">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:24px;">Dates: 27<small class="th">th</small> 28<small class="th">th</small> and 29<small class="th">th</small> <br>July 2015</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>TBC, New Delhi</p>
        </div>
        
        <div>
      
        <div class="content-date-venue res_css">
            <h1 style="color:black">What is unique about this course</h1>
            <p style="color:black">The faculty who have experience delivering MRCOG Courses in UK is purely UK based. The tutors have attended some of the most popular UK MRCOG written and OSCE preparation courses and picked the best aspects from each to deliver this course. The format is especially designed for International doctors. We will cover all the three formats and core modules of the MRCOG written. We will also provide concise orientation for MRCOG OSCE as well. CME points may be available as will be applied for through DHA.</p>
        </div>
      
        </div>
        
        
       
          <?php include 'enquiryform.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
