<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];

?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>Basic Implant Course</title>

<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<style>
.slider {
	width: 100%;
	overflow: hidden;
	position: relative;
	height: 300px;
}

.slider-wrapper {
	width: 100%;
	height: 100%;
	position: relative;
}

.slide {
	float: left;
	position: absolute;
	width: 100%;
	height: 100%;
	background-repeat: no-repeat;
	background-size: cover;
	opacity: 0;
}

.slider-wrapper > .slide:first-child {
	opacity: 1;
}


</style>	
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
$pathCMS = constant::$pathCMS;
?>
<!-- Banner Start Here -->
<section class="inner-banner clinical">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2 >Basic Implant Course </h2>
          <h3><span>DAMS now offers Basic Implant Course </span></h3>
        <!--  <h3><span>Starting From 23-May-2016 </span></h3>-->
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>Basic Implant Course<span class="book-ur-seat-btn clinicalc"><a title="Book Your Seat " href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Buy Now</a></span></h3>
        <p style="font-size:22px;padding:5px 0px;">Course Details Format</p><br>
        <p>The aim of this course is to hone the clinical skills and knowledge of graduates in the field of basic dental implantology and to familiarize them with the latest techniques and armamentaria and help them emerge a better clinician.</p>     
      <!--  <p style="margin-top: 15px;"><strong>Starting From 23-May-2016</strong></p>
            <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
            <li><span class="sub-arrow"></span><span style="width:130px;display: inline-block;">Course Duration : </span><strong>&nbsp;2 Days</strong></li>
          <li><span class="sub-arrow"></span><span style="width:130px;display: inline-block;">Fees: </span><strong>&nbsp;Rs:5000<strong></li>
          <li><span class="sub-arrow"></span><strong><span style="width:98%;display: inline-block;"> 20% Discount to all the current or previous Damsonians</span></li>      
        </ul>
      </div>
    </div>   -->   
</div>
       
<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Highlights</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <p style="font-size: 15px; padding: 10px 0px; color: rgb(0, 0, 0);"><strong>SURGICAL MODULE – I</strong></p>
		  <ul>
				<li><strong>Day One</strong></li>
		  </ul>
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Introduction & features of an implant system with 3 implant systems </li>
          <li><span class="sub-arrow"></span>Basic concepts of implantology </li>
			<li><span class="sub-arrow"></span>Diagnosis & case selection </li>
			<li><span class="sub-arrow"></span>Overview of anatomy (Maxilla & Mandible)</li>
			<li><span class="sub-arrow"></span>Bone evaluation </li>
			<li><span class="sub-arrow"></span>Case selection & Treatment planning on CBCT scans </li>
			<li><span class="sub-arrow"></span>Multi-disciplinary approach </li>
			<li><span class="sub-arrow"></span>Complications & its management </li>
			<li><span class="sub-arrow"></span>Hands-on: Implant placement on dental model with dummy implants (1 implant per participant) </li>
			
              
          
          
        </ul>
		 <ul>
				<li><strong>Day Two</strong></li>
		  </ul>
		   <ul class="course-new-list">
		   <li><span class="sub-arrow"></span>How to operate physiodispenser </li>
          <li><span class="sub-arrow"></span>Mechanics of physiodispenser (torques and gears) </li>
			<li><span class="sub-arrow"></span>Patient preparation  </li>
			<li><span class="sub-arrow"></span>Surgery on live patients</li>
		</ul>
      </div>
    </div>
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <p style="font-size: 15px; padding: 10px 0px; color: rgb(0, 0, 0);"><strong>PROSTHETIC MODULE – II	(After 3 months)</strong></p>
		   <ul>
				<li><strong>Day One</strong></li>
		  </ul>
        <ul class="course-new-list">
            <li><span class="sub-arrow"></span>Introduction & features of prosthetic components & systems  </li>
          <li><span class="sub-arrow"></span>Cement vs screw vs UCLA abutments </li>
			<li><span class="sub-arrow"></span>Overview of restorative treatment  </li>
			<li><span class="sub-arrow"></span>Abutment selection criteria for optimizing prostheses</li>
			<li><span class="sub-arrow"></span>Prosthetic planning & solutions</li>
			<li><span class="sub-arrow"></span>Hands-on: Impression taking techniques on the model </li>
			<li><span class="sub-arrow"></span>Live: Impression taking on the live patient </li>
              </ul>
			  		 <ul>
				<li><strong>Day Two</strong></li>
		  </ul>
		   <ul class="course-new-list">
		   <li><span class="sub-arrow"></span>Live: Prosthetic delivery on the live patient </li>
          <li><span class="sub-arrow"></span>Evaluation of implant treatment</li>
			
		</ul>
          
         
      </div>
    </div>

  </div></div>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #ccc;">
        <div class="mrcp_dr_img"><img src='images/draditya.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Aditya Sao</h1>                      
            <p class="mrcp_dr_sbtl" style="font-size:14px;">Dr Aditya Sao did his BDS in 2008 and went on to pursue his Post Graduation in Oral Medicine and Maxillofacial Radiology from Bapuji Dental College Davangere Karnataka. He has worked as  a consultant for treatment planning using CBCT imaging to guide young dentists in implant dentistry. He is also an Affiliate Associate Fellow of American Academy of Implant Dentistry (AAID). He works as a consultant in various dental clinics of South Delhi.</p></br>        		   
        </div>
</div></div>
    </div>
    </div>
	<div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #ccc;">
        <div class="mrcp_dr_img"><img src='images/DrChetan.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Chetan Pathak </h1>                      
            <p class="mrcp_dr_sbtl" style="font-size:14px;">Dr Chetan Pathak Completed BDS, MDS From Bapuji Dental College Davangere Karnataka. He has worked extensively in the field of prosthodontics. He has to his credit numerous national and international publications, He has done extensive work in the field of basal implantology. He is a diplomate of ICOI and currently working as a faculty in SRCDSR Faridabad. He works as a consultant in various dental clinics of NCR and reputed hospitals.</p></br>        		   
        </div>
</div></div>
    </div>
    </div>
  </div>
<!--        <div class="pg-medical-main" style="display:block;margin:25px 0 0 0;border: 0px solid #F0AC49;line-height: 0px;">
<img src="images/course_1.jpg" style="margin:0px; border:0px solid #ccc;" width="100%">
</div>-->


    </aside>    
    <aside class="content-right">
        <div class="content-date-venue res_css" style="margin-top:0px;">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:18px;">Every Month</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 9958875337</p>
            <p>(E-mail:- aditya@damsdelhi.com)</p>
        </div>
          <?php include 'enquiryform.php'; ?>
<!--      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/BLSACLS-LOGO.png" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>    -->
        
        <div class="home-page-popup-wrapper">
    <div class="home-page-popup-container"> 
        <iframe id="video" width="100%" height="100%" src="https://www.youtube.com/embed/AYRaHOsjD5U?rel=0&amp;enablejsapi=1&amp;version=3&amp;playerapiid=ytplayer&amp;cc_load_policy=1&amp;controls=0&amp;fs=0&amp;rel=0&amp;showinfo=0&amp;color=white&amp;autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <p class="popupCancel"><span class="close thick"></span></p>
    </div>
</div>
        <div class="video-part-new" style="">
          <div class="enquiry-main">
              <div class="enquiry-heading">Video</div>
             <div class="course-new-section" style="padding-bottom:8px;">
                 <div class="vdo-img">
                     <img src="images/dnew-img.jpg" style="width:100%;">
                     <div class="vdo-play"><img class="flaticon-play39 video-bg new-video video-link flaticon-pro" src="images/youtube1.svg"></div>
                 </div>
              <p>If i hear I forgot, If i see i remember, but if i do i Understand. Feedback of a Student Dr Jyoti Virk DDS from NYU university- USA after placing implants on patients. India&#39;s most economical implant course, best mentors and maximum patient exposure. Next batch on 24th March onwards. Limited seats for personal attention. For more details call on the mentioned numbers or Inbox us.
              </p>
            </div>
            
         </div>
        </div>
    </aside>
	 <aside class="content-right">

	<div  style="display:block; width:100%;float:right;margin:25px 0 0;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;box-shadow:0 0 4px ;-webkit-box-shadow:0 0 4px rgba(0,166,81,1);-moz-box-shadow:0 0 4px rgba(0,166,81,1);border:1px solid #00a651;  ">
  <div style="width: 98%;
    float: left;
    background: #00a651; left top repeat-x;
    font-family: 'pt_sansbold';
    font-size: 18px;
    font-weight: 400;
    color: #1e1a1a;
    line-height: 37px;
    padding: 0 0 0 2%;
	color:#fff;"><span></span>Photos</div>
   <div class="course-new-section">
   <div class="coures-list-box">
      

        
		<div class="slider" id="main-slider"><!-- outermost container element -->
	<div class="slider-wrapper"><!-- innermost wrapper element -->
		<div class="slide" data-image="images/IMG_3622.jpg"></div><!-- slides -->
		<div class="slide" data-image="images/IMG_3630.jpg"></div>
		<div class="slide" data-image="images/IMG_3633.HEIC"></div>
		<div class="slide" data-image="images/IMG_3638.HEIC"></div>
	</div>
	
		
		
		
</div>
    </div>
    </div>
	</div>
	</aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
 <script>
(function( $ ) {
	$.fn.slideshow = function( options ) {
		options = $.extend({
			slides: ".slide",
			speed: 5000,
			easing: "linear"
			
		}, options);
		
		var timer = null;
		var index = 0;
		
		var slideTo = function( slide, element ) {
			var $currentSlide = $( options.slides, element ).eq( slide );
			
			$currentSlide.stop( true, true ).
			animate({
				opacity: 1
			}, options.speed, options.easing ).
			siblings( options.slides ).
			css( "opacity", 0 );	
			
		};
		
		var autoSlide = function( element ) {
			timer = setInterval(function() {
				index++;
				if( index == $( options.slides, element ).length ) {
					index = 0;
				}
				slideTo( index, element );
			}, options.speed);	
		};
		
		var startStop = function( element ) {
			element.hover(function() {
				clearInterval( timer );
				timer = null;	
			}, function() {
				autoSlide( element );	
			});
		};
		
		return this.each(function() {
			var $element = $( this ),
				$previous = $( options.previous, $element ),
				$next = $( options.next, $element ),
				index = 0,
				total = $( options.slides ).length;
				
				$( options.slides, $element ).each(function() {
					var $slide = $( this );
					var image = $slide.data( "image" );
					$slide.css( "backgroundImage", "url(" + image + ")" );
				});
				
				autoSlide( $element );
				startStop( $element );
			
		});
	};
	
	$(function() {
		$( "#main-slider" ).slideshow();
		
	});
	
})( jQuery );

</script>
</body>
</html>
