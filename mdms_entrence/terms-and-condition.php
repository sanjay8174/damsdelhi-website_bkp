<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
//     Registration Form
    $('#student-registration').click(function(e) {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });	

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <aside class="banner-left banner-left-postion">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading">
            <h4>Terms &amp; Conditions</h4>
            <article class="showme-main">
              <div class="privacy-content">
                <p>In using, the Online Service of Delhi Academy of Medical Sciences Pvt. Ltd (DAMS) and you (The User) are deemed to have accepted the terms and conditions listed below. Delhi Academy of Medical Sciences Pvt. Ltd reserves the right to add, delete, alter or modify these terms and conditions at any time. The user is therefore advised to read carefully these terms and conditions each time he or she uses the DAMS Online Service(s) of DAMS. All products / services and information displayed constitute an "invitation to offer". Your order for purchase constitutes your "offer" which shall be subject to the terms and conditions as listed below. DAMS reserve the right to accept or reject your offer.</p>
                <p>The agreement between you and the Delhi Academy of Medical Sciences Pvt. Ltd shall be subject to the following terms and conditions:</p>
                <ul class="terms-list">
                  <li><span class="list-arrow"></span> <span class="list-content">The User certifies that he/she is at least 18 (eighteen) years of age or has the consent of a parent or legal guardian.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">These terms and conditions supersede all previous representations, understandings, or agreements and shall prevail notwithstanding any variance with any other terms of any order submitted. By using the DAMS services, you agree to be bound by the Terms and Conditions.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">All prices, unless indicated otherwise are in Indian Rupees.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">All prices and availability of products are subject to change without prior notice at the sole discretion of DAMS.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve the right to refuse or cancel any order placed for a product that is listed at an incorrect price. This shall be regardless of whether the order has been confirmed and/or payment been levied via credit card. In the event the payment has been processed by DAMS online services the same shall be credited to your credit card account and duly notified to you by email or by SMS.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">In a credit card transaction, you must use your own credit card. DAMS will not be liable for any credit card fraud. The liability to use a card fraudulently will be on the user and the onus to prove otherwise shall be exclusively on the user.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Any request for cancellations of orders once duly placed on the site, shall not be entertained.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">In the event that a non-delivery occurs because of a mistake by you, (i.e. wrong name or address) any extra cost incurred by DAMS for redelivery shall be claimed from the User placing the order.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">DAMS shall not be liable for any delay / non-delivery of purchased goods (Study materials, Online Tests, any assignments) by the vendors, trade organization/s, manufacturers / shop etc. (vendors), flood, fire, wars, acts of God or any cause that is beyond the control of DAMS.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">The User agrees to use the services provided by DAMS through their website <a href="http://www.damsdelhi.com">www.damsdelhi.com</a> for lawful purposes only.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">The User agrees to provide authentic and true information. DAMS reserve the right to confirm and validate the information and other details provided by the User at any point of time. If upon confirmation such User details are found not to be true (wholly or partly), DAMS has the right in its sole discretion to reject the registration and debar the User from using <a href="http://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> and / or other affiliated websites without prior intimation whatsoever.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">DAMS will not be responsible for any damage suffered by users from use of the services on this site. This without limitation includes loss of revenue/data resulting from delays, non-deliveries, missed deliveries, or service interruptions as may occur because of any act / omission of the vendor. This disclaimer of liability also applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of contract, tortuous behavior, negligence, or under any other cause of action.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">The User expressly agrees that the use of service(s) DAMS is at the Users sole risk. The Service(s) of DAMS is provided on an as is basis without warranties of any kind, whether express or implied. Delhi Academy of Medical Sciences Pvt. Ltd. its affiliates, employees, agents, consultants, contracted companies make no warranties of any kind, whether expressed or implied, for the service it is providing or as to the results that may be obtained from use of the Service, or as to the accuracy, reliability or content of any information, service, or merchandise provided through this Service. DAMS do not represent or warrant maintaining the confidentiality of information, although DAMS current practice is to utilize reasonable efforts to maintain such confidentiality. It is also clearly understood that all warranties, implied or express take place between the vendors and the User.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">It is compulsory for DAMS users/students to send their self-attested photocopy of mark sheet of MBBS and attach recent two passport size photographs along with dully-filled Admission Form (hard copy) to registered office only.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">It is for the student to see whether he/she is eligible for a certain Entrance Exams or not. DAMS do not hold it responsible if a student's admission form cannot be forwarded or is rejected by the examination body on any ground whatsoever. Such a student cannot claim a refund of the whole or any part of the fee he/she has paid to DAMS.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">The responsibility of getting admission form forwarded to the examining body is that of the student himself/herself.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve the right to use the photograph for publicity in case the student secures position/success in any Exams national/international level test.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Any change in the correspondence address or phone number should be intimated to Registered Office immediate through a written application quoting the Name, Reference ID, and Roll No. of the student.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Students enrolled with DAMS have to provide photocopy of Admit Card of various Entrance Exams. As soon as they receive their admit cards from the examining body.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Each student will be issued an Identity Card. The entry into the classes will be permitted only with Identity Card. If any student is found misusing the I-Card, he/she will be rusticated from the Institute.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">In case if any parent/guardian/student misbehaves with any staff members of the Institute unlawfully, his/her ward can be rusticated and no claim of refund of such student will be entertained.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Registered Students can be obtained duplicate I-Card, Set of books & Bag from the Institute against the proof of registration along with payment of requisite fee as applicable from time to time.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">If at any point of time, the Govt. further increases service tax, the tax will be borne by the students from the date of enforcement of the act by the Govt.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve its right to make any alteration in its programs / venue / timing and days of classes without any prior notice to anybody. The decision of the Director will be final &amp; binding.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">In case of any dispute, arising out of present terms & conditions, it shall be referred to the sole arbitrator to be appointed by the Chairman of Delhi Academy of Medical Sciences Pvt. Ltd. The seat of arbitrator will be at Delhi only. In any case if any matter has to go to the court, only Delhi Court shall have jurisdiction to decide such matters</span></li>
                </ul>
               <span>REFUND POLICY FOR CLASSROOM COURSES :</span>
                <ul class="terms-list">
                  <li><span class="list-arrow"></span> <span class="list-content">No refund of Admission cum Scholarship Test fee shall be made under any circumstances whatsoever.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">No refund of Registration Fee shall be made under any circumstances whatsoever. It is towards administrative expenses incurred by the company.</span> </li>
                  <li><span class="list-arrow"></span> <span class="list-content">No refund will be made under any circumstances after joining the Short Term Classroom Courses like Crash Courses / Test Series Courses /Postal Course. </span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">If any student pays the fee for any course other than Short Term Classroom Courses and wants to withdraws/asks for refund before the commencement of classes / course in the Institute, the admission fee and first installment of tuition fee paid will be refunded along with PDCs submitted, if any. The registration fee and other government dues will not be refunded.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">If any student leaves the classes/Institute after 15 days from the date of commencement of the classes/course in the Institute due to whatsoever reason, then no refund of admission fee and tuition fee paid &amp; encashed will be made under any circumstances. Only the PDC's (if any) which have not been encashed on the date of refund application shall be cancelled / returned.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">All refund cheques will be issued in the name of student / parents only.</span></li>
                </ul>
                <span>REFUND POLICY FOR CLASSROOM COURSES :</span>
                <ul class="terms-list">
                  <li><span class="list-arrow"></span> <span class="list-content">The date of commencement of the batch will be considered & not the date of joining of any student in the institute.</span></li>
                  <li><span class="list-arrow"></span> <span class="list-content">Students/Parents must insist for receipt of refund application from the front office. The date of submission of refund application will be considered and not by the number of classes attended by the student or from the date, the student stopped.</span></li>
                </ul>
                <span>TRANSFER POLICY :</span>
                <p>A student after joining any center of DAMS cannot shift to any other center of DAMS. However, DAMS may help provided there is a seat vacant in the desired course at the desired center. </p>
                <p>The student will also submit proper details of fee paid at the first centre duly certified from the In-charge of that centre to the new Centre In-charge where transfer is required. Whatsoever amount of fee or its installments, a student might have paid at first centre, the balance fee of the new centre shall be paid at the new centre where transfer is required.</p>
                <p>DAMS do not guarantee the course/schedules matching the destination center student himself/herself have to check the compatibility of DAMS center transfer cases. The student himself /herself would be responsible for course /schedule compatibility or otherwise.</p>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>