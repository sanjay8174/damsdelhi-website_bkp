<!DOCTYPE html>
<?php
$course_id=$_REQUEST['c'];
$limit=7;
$page = (int) (!isset($_REQUEST["page"]) ? 1 : $_REQUEST["page"]);
if($page!='1'){
    $start = $limit*($page-1);
} else{
    $start = 0;
}
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 

<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php?c=<?php echo $course_id?>" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="index.php?c=<?php echo $course_id?>" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="News &amp; Updates" class="active-link">News &amp; Updates</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>News &amp; Updates</h4>
            <article class="showme-main">
              <?php 
                    include 'openconnection.php';
                    $count = 0;$i=0;$newsDetail=array();
                    $sql =mysql_query("SELECT * FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC limit $start,$limit");
                    while($row = mysql_fetch_array($sql)){
                        $newsDetail[$count][0] = urldecode(html_entity_decode(stripslashes($row['HEADING']),ENT_QUOTES,'UTF-8'));
                        $newsDetail[$count][1] = urldecode(html_entity_decode(stripslashes($row['DETAIL']),ENT_QUOTES,'UTF-8'));
                        $newsDetail[$count][2] = trim($row['DATE']);
                        $newsDetail[$count][3] = $row['NEWS_ID'];
                        $count++;
                    }
                    ?>
              <div class="news1-main">
                <ul id="newslist">
                  <?php for($i=0;$i<count($newsDetail);$i++){?>
                  <li>
                    <?php //$date = DateTime::createFromFormat('Y-m-d', $newsDetail[$j][2]); ?>
                    <!--                    <div class="left-cal"><?php //echo $date->format('d'); ?>
                    <span><?php //echo $date->format('M')." ".$date->format('Y'); ?></span>
                </div>-->
                    <?php $date = explode("-",$newsDetail[$i][2]);
                                                if ($date[2] == '01' || $date[2] == '1') {
                                                    $month = 'Jan';
                                                } else if ($date[2] == '02' || $date[2] == '2') {
                                                    $month = 'Feb';
                                                } else if ($date[2] == '03' || $date[2] == '3') {
                                                    $month = 'Mar';
                                                } else if ($date[2] == '04' || $date[2] == '4') {
                                                    $month = 'Apr';
                                                } else if ($date[2] == '05' || $date[2] == '5') {
                                                    $month = 'May';
                                                } else if ($date[2] == '06' || $date[2] == '6') {
                                                    $month = 'Jun';
                                                } else if ($date[2] == '07' || $date[2] == '7') {
                                                    $month = 'Jul';
                                                } else if ($date[2] == '08' || $date[2] == '8') {
                                                    $month = 'Aug';
                                                } else if ($date[2] == '09' || $date[2] == '9') {
                                                    $month = 'Sep';
                                                } else if ($date[2] == '10') {
                                                    $month = 'Oct';
                                                } else if ($date[2] == '11') {
                                                    $month = 'Nov';
                                                } else if ($date[2] == '12') {
                                                    $month = 'Dec';
                                                }
                                            ?>
                    <div class="left-cal"><?php echo $date[1]; ?> <span><?php echo $month . " " . $date[0]; ?></span> </div>
                    <span class="right-cal">
                    <?php if(strlen($newsDetail[$i][1])>150){ ?>
                        <a href="news-detail.php?&amp;id=<?php echo $newsDetail[$i][3]; ?>&c=<?php echo $course_id?>"><h5><?php echo $newsDetail[$i][0]; ?></h5></a>
                    <?php } else{?>
                        <h5><?php echo $newsDetail[$i][0]; ?></h5>
                    <?php } ?>
                    <p> <?php echo substr($newsDetail[$i][1],0,150); ?>
                      <?php if(strlen($newsDetail[$i][1])>150){ ?>
                      .....<a href="news-detail.php?&amp;id=<?php echo $newsDetail[$i][3]; ?>&c=<?php echo $course_id?>" title="read more">read more</a>
                      <?php } ?>
                    </p>
                    </span> </li>
                  <?php } ?>
                </ul>
              </div>
              <div class="news-pagination">
               <?php
                    $query = "SELECT * FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC";
                    $path = "news.php?c=$course_id&amp;";
                    $row = mysql_num_rows(mysql_query($query));
                    $total_pages = $row;
                    $adjacents = "2";
                    $prev = $page - 1;
                    $next = $page + 1;
                    if ($limit != '' or $limit != 0) {
                        $lastpage = ceil($total_pages / $limit);
                    }
                    $lpm1 = $lastpage - 1;
                    $from=$start+1;
                    $to=$start+$limit;
                    if($to>=$total_pages)
                        $to=$total_pages;
                    $pagination = "";
                    if ($lastpage > 1) {
                        $pagination .= "<ul class='pagination'>";

                        if ($page > 1)
                            $pagination.= "<li id='displayitem4'><a href='" . $path . "page=$prev'>« Previous</a></li>";
                        else
                            $pagination.= "<li id='displayitem4'><span class='disabled'>« Previous</span></li>";

                        if ($lastpage < 7 + ($adjacents * 2)) {

                            for ($counter = 1; $counter <= $lastpage; $counter++) {
                                if ($counter == $page)
                                    $pagination.= "<li class='active'><span>$counter</span></li>";
                                else
                                    $pagination.= "<li><a href='" . $path . "page=$counter'>$counter</a></li>";
                            }
                        } elseif ($lastpage > 5 + ($adjacents * 2)) {
                            if ($page < 1 + ($adjacents * 2)) {

                                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                                    if ($counter == $page)
                                        $pagination.= "<li class='active'><span>$counter</span></li>";
                                    else
                                        $pagination.= "<li><a href='" . $path . "page=$counter'>$counter</a></li>";
                                }
                                $pagination.= "<li><a href='javascript:void(0);'>...</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=$lpm1'>$lpm1</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=$lastpage'>$lastpage</a></li>";

                            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                                $pagination.= "<li><a href='" . $path . "page=1'>1</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=2'>2</a></li>";
                                $pagination.= "<li><a href='javascript:void(0);'>...</a></li>";
                                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                                    if ($counter == $page)
                                        $pagination.= "<li class='active'><span>$counter</span></li>";
                                    else
                                        $pagination.= "<li><a href='" . $path . "page=$counter'>$counter</a></li>";
                                }
                                $pagination.= "<li><a href='javascript:void(0);'>...</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=$lpm1'>$lpm1</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=$lpm1'>$lastpage</a></li>";

                            } else {

                                $pagination.= "<li><a href='" . $path . "page=1'>1</a></li>";
                                $pagination.= "<li><a href='" . $path . "page=2'>2</a></li>";
                                $pagination.= "<li><a href='javascript:void(0);'>...</a></li>";
                                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                                    if ($counter == $page)
                                        $pagination.= "<li class='active'><span>$counter</span></li>";
                                    else
                                        $pagination.= "<li><a href='" . $path . "page=$counter'>$counter</a></li>";
                                }
                            }
                        }
                        if ($page < $counter - 1)
                            $pagination.= "<li><a href='" . $path . "page=$next'>Next »</a></li>";
                        else
                            $pagination.= "<li><span class='disabled'>Next »</span></li>";
                        $pagination.= "</ul >\n";
                    }
                    echo $pagination;
                ?>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<?php mysql_close($myconn);?>
</body>
</html>
