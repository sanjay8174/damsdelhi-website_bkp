<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Sitemap</h4>
            <div class="new-sitemap">
              <ul class="some-points">
                <li><span class="blue_arrow"></span><a href="index.php">Home</a></li>
                <li><span class="blue_arrow"></span><a href="download.php">Download</a></li>
                <li><span class="blue_arrow"></span><a href="career.php">Career</a></li>
                <li><span class="blue_arrow"></span><a href="https://blog.damsdelhi.com/">Blog</a></li>
                <li><span class="blue_arrow"></span><a href="franchisee.php">Franchisee</a></li>
                <li><span class="blue_arrow"></span><a href="contact.php">Contact us</a></li>
                <li><span class="blue_arrow"></span><a>ABOUT US</a>
                  <div class="inner-accor" style="margin:0px 0px 0px 3%;">
                    <ul>
                      <li><a href="dams.php">About DAMS</a></li>
                      <li><a href="dams_director.php">Director's Message</a></li>
                      <li><a href="about_director.php">About Director</a></li>
                      <li><a href="mission_vision.php"> Mission &amp; Vision</a></li>
                      <li><a href="dams_faculty.php">Our Faculty</a></li>
                    </ul>
                   </div>
                </li>
                <li><span class="blue_arrow"></span><a>COURSES</a>
                  <div class="inner-accor" style="margin:0px 0px 0px 3%;">
                    <ul>
                      <li><a href="https://mdms.damsdelhi.com/index.php">MD / MS ENTRANCE</a></li>
                      <ol>
                        <li><a title="Regular Course (weekend)" href="https://mdms.damsdelhi.com/regular_course_for_pg_medical.php"><span class="sub-arrow"></span> Regular Course</a></li>
                        <li><a title="Test &amp; Discussion Course" href="https://mdms.damsdelhi.com/t&amp;d_md-ms.php"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
                        <li><a title="Crash Course" href="https://mdms.damsdelhi.com/crash_course.php"><span class="sub-arrow"></span> Crash Course</a></li>
                        <li><a title="Postal Course" href="https://mdms.damsdelhi.com/postal.php"><span class="sub-arrow"></span> Postal Course</a></li>
                        <li><a title="Tablet Based Course - IDAMS" href="dams-publication.php?c=1"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                        <li><a title="Online" href="https://mdms.damsdelhi.com/online-test-series.php"><span class="sub-arrow"></span> Online</a></li>
                        <li><a title="Offline" href="https://mdms.damsdelhi.com/offline-test-series.php"><span class="sub-arrow"></span> Offline</a></li>
                        <li><a title="Foundation Course" href="https://mdms.damsdelhi.com/foundation_course.php"><span class="sub-arrow"></span> Foundation Course</a></li>
                        <li><a title="Test Series" href="https://mdms.damsdelhi.com/test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Prefoundation Course" href="https://mdms.damsdelhi.com/prefoundation_course.php"><span class="sub-arrow"></span> Prefoundation Course</a></li>
                        <li><a title="First Step course" href="https://mdms.damsdelhi.com/the_first_step.php"><span class="sub-arrow"></span> First Step Course</a></li>
                        <li><a title="National Quiz" href="https://mdms.damsdelhi.com/national.php"><span class="sub-arrow"></span> National Quiz</a></li>
                        <li><a title="Concept of Grey Matter" href="https://mdms.damsdelhi.com/gray-matter.php"><span class="sub-arrow"></span> Concept of Grey Matter</a></li>
                        <li><a title="Testimonials" href="https://mdms.damsdelhi.com/gray-matter-testimonial.php"><span class="sub-arrow"></span> Testimonials</a></li>
                        <li><a title="1st Campus Round 2012" href="https://mdms.damsdelhi.com/round1.php"><span class="sub-arrow"></span> 1st Campus Round 2012</a></li>
                        <li><a title="2nd Campus Round 2012" href="https://mdms.damsdelhi.com/round2.php"><span class="sub-arrow"></span> 2nd Campus Round 2012</a></li>
                        <li><a title="Grand Finale 2012" href="https://mdms.damsdelhi.com/grand.php"><span class="sub-arrow"></span> Grand Finale 2012</a></li>
                        <li><a title="1st Campus Round 2013" href="https://mdms.damsdelhi.com/season2round1.php"><span class="sub-arrow"></span> 1st Campus Round 2013</a></li>
                        <li><a title="2nd Campus Round 2013" href="https://mdms.damsdelhi.com/season2round2.php"><span class="sub-arrow"></span> 2nd Campus Round 2013</a></li>
                        <li><a title="Grand Finale 2013" href="https://mdms.damsdelhi.com/grand2013.php"><span class="sub-arrow"></span> Grand Finale 2013</a></li>
                        <li><a title="Satellite Classes" href="https://mdms.damsdelhi.com/satellite-classes.php"><span class="sub-arrow"></span> Satellite Classes</a></li>
                        <li><a title="Test Series" href="https://mdms.damsdelhi.com/test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Achievement" href="https://mdms.damsdelhi.com/aipge_2014.php"><span class="sub-arrow"></span> Achievement</a></li>
                        <!--<li><a title="DAMS Store" href="dams-publication.php?c=1"><span class="sub-arrow"></span> DAMS Store</a></li>-->
                         <li><a title="DAMS Store" href="http://damspublications.com/" target="_blank" target="_blank"><span class="sub-arrow"></span> DAMS Store</a></li>
                        <li><a title="Find a Center" href="find-center.php"><span class="sub-arrow"></span> Find a Center</a></li>
                        <li><a title="Virtual Tour" href="https://mdms.damsdelhi.com/photo-gallery.php"><span class="sub-arrow"></span> Virtual Tour</a></li>
                      </ol>
                      <li><a href="https://mci.damsdelhi.com/index.php">MCI SCREENING</a></li>
                      <ol>
                        <li><a title="Regular Course" href="https://mci.damsdelhi.com/mci-reguler-course.php"><span class="sub-arrow"></span> Regular Course</a></li>
                        <li><a title="Crash Course" href="https://mci.damsdelhi.com/mci-crash-course.php"><span class="sub-arrow"></span> Crash Course</a></li>
                        <li><a title="Test Series" href="https://mci.damsdelhi.com/mci-test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Online Test Series" href="https://mci.damsdelhi.com/mci-online-test-series.php"><span class="sub-arrow"></span> Online Test Series</a></li>
                        <li><a title="Postal Course" href="https://mci.damsdelhi.com/mci-postal-course.php"><span class="sub-arrow"></span> Postal Course</a></li>
                        <li><a title="Tablet Based Course - IDAMS" href="dams-publication.php?c=2"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                        <li><a title="Satellite Classes" href="https://mci.damsdelhi.com/mci-satellite-classes.php"><span class="sub-arrow"></span> Satellite Classes</a></li>
                        <li><a title="Test Series" href="https://mci.damsdelhi.com/mci-test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Achievement" href="https://mci.damsdelhi.com/mciscreening_sep_2013.php"><span class="sub-arrow"></span> Achievement</a></li>
                        <!--<li><a title="DAMS Store" href="dams-publication.php?c=2"><span class="sub-arrow"></span> DAMS Store</a></li>-->
                        <li><a title="DAMS Store" href="http://damspublications.com/" target="_blank"><span class="sub-arrow"></span> DAMS Store</a></li>
                        <li><a title="Find a Center" href="find-center.php"><span class="sub-arrow"></span> Find a Center</a></li>
                        <li><a title="Virtual Tour" href="https://mci.damsdelhi.com/mci-photo-gallery.php"><span class="sub-arrow"></span> Virtual Tour</a></li>
                      </ol>
                      <li><a href="https://mds.damsdelhi.com/index.php">MDS Quest</a></li>
                      <ol>
                        <li><a title="Regular Course" href="https://mds.damsdelhi.com/dams-mds-quest-dental-regular-course.php"><span class="sub-arrow"></span> Regular Course</a></li>
                        <li><a title="Test &amp; Discussion Course" href="https://mds.damsdelhi.com/mds-test-discussion-course.php"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
                        <li><a title="Crash Course" href="https://mds.damsdelhi.com/mds-crash-course.php"><span class="sub-arrow"></span> Crash Course</a></li>
                        <li><a title="Test Series" href="https://mds.damsdelhi.com/dams-mds-test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Postal Course" href="https://mds.damsdelhi.com/mds-postal-course.php"><span class="sub-arrow"></span> Postal Course</a></li>
                        <li><a title="Online Test Series" href="dams-publication.php?c=3"><span class="sub-arrow"></span> Online Test Series</a></li>
                        <li><a title="Satellite Classes" href="https://mds.damsdelhi.com/mds-satellite-classes.php"><span class="sub-arrow"></span> Satellite Classes</a></li>
                        <li><a title="Test Series" href="https://mds.damsdelhi.com/dams-mds-test-series.php"><span class="sub-arrow"></span> Test Series</a></li>
                        <li><a title="Achievement" href="https://mds.damsdelhi.com/mds-aipge-2014.php"><span class="sub-arrow"></span> Achievement</a></li>
                        <!--<li><a title="DAMS Store" href="dams-publication.php?c=3"><span class="sub-arrow"></span> DAMS Store</a></li>-->
                           <li><a title="DAMS Store" href="http://damspublications.com/" target="_blank"><span class="sub-arrow"></span> DAMS Store</a></li>
                        <li><a title="Find a Center" href="find-center.php"><span class="sub-arrow"></span> Find a Center</a></li>
                        <li><a title="Virtual Tour" href="https://mds.damsdelhi.com/mds-photo-gallery.php"><span class="sub-arrow"></span> Virtual Tour</a></li>
                      </ol>
                      <li><a href="https://usmle.damsdelhi.com/index.php">USMLE Edge</a></li>
                      <ol>
                        <li><a title="USMLE STEP-1 (TS)" href="https://usmle.damsdelhi.com/usml-step1.php"><span class="sub-arrow"></span> USMLE STEP-1 (TS)</a></li>
                        <li><a title="USMLE STEP-2 (TS)" href="https://usmle.damsdelhi.com/usml-step2.php"><span class="sub-arrow"></span> USMLE STEP-2 (TS)</a></li>
                        <li><a title="USMLE Combo" href="https://usmle.damsdelhi.com/usmle-egde-combo.php"><span class="sub-arrow"></span> USMLE Combo</a></li>
                        <li><a title="USMLE Full Package" href="https://usmle.damsdelhi.com/usmle-edge-full-package.php"><span class="sub-arrow"></span> USMLE Full Package</a></li>
                      </ol>
                    </ul>
                  </div>
                </li>
                <li><span class="blue_arrow"></span><a href="privacy_policy.php">Privacy Policy</a></li>
                <li><span class="blue_arrow"></span><a href="disclaimer.php">Disclaimer</a></li>
                <li><span class="blue_arrow"></span><a href="terms_n_condition.php">Terms &amp; Conditions</a></li>
                <li><span class="blue_arrow"></span><a href="https://play.google.com/store/apps/details?id=com.gingerwebs.damsdelhi&amp;hl=en">Download DAMS Android App</a></li>
                <li><span class="blue_arrow"></span><a href="#">Download DAMS Iphone App</a></li>
              </ul>
            </div>
          </div>
        </aside>
        <aside class="gallery-right"> 
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>