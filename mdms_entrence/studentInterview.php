<?php
$pathCMS = constant::$pathCMS;
?>
<div class="news-update-box">
<div class="n-videos"><span></span> Students Interviews</div>
<div class="videos-content-box">
<?php
$courseId = $course_id;

// Logger::configure('config/log4php.xml');

$Dao = new dao();
$getIntVideo = $Dao->getIntVideo($courseId);
$count=count($getIntVideo);
for($i=0;$i<$count;$i++){
    /* you tube thumb */
    $youtubeUrl = rawurldecode($getIntVideo[$i][3]);
    $youtubeUrlthumb = substr($youtubeUrl,strripos($youtubeUrl,"/",0)+1,strlen($youtubeUrl));
    $imgSrc =  $pathCMS.'/youTubeThumbs/'.$youtubeUrlthumb.'_'.$getIntVideo[$i][6].'.jpg'; 
?>

    <div id="vd<?php echo $i;?>" <?php if($i==0) {?>class="display_block"<?php }else{ ?>class="display_none"<?php } ?>>
         <?php if($getIntVideo[$i][4]==1){ ?>
            <a target="_blank" href="<?php echo rawurldecode($getIntVideo[$i][3]); ?>" ><img class="border_none" src="<?php echo $imgSrc; ?>" width="100%" height="247" alt="<?php echo $youtubeUrlthumb. '.jpg'; ?>" frameborder="0"></a>
            <!--<iframe width="100%" height="236" src="<?php echo $getIntVideo[$i][3];?>" class="border_none"></iframe>-->
         <?php }else{ ?>
            <video width="100%" height="247" controls>
                <source src="<?php echo $pathCMS."/video/".$getIntVideo[$i][2]."/".$getIntVideo[$i][1]."/". $getIntVideo[$i][5];?>" type="video/mp4">
            </video>
         <?php } ?>
   </div>
<?php }?>






</div>

<div class="box-bottom-1">
<span class="mini-view-right"></span>
<div class="mini-view-midle">
    <a <?php if($getIntVideo[0][6]=='null'){?>href="#"<?php }else{?>href="achievements.php?c=<?php echo $courseId;?>"<?php }?> class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
<div class="slider-mini-dot"><ul>
 <?php for($i=0;$i<$count;$i++){?>
        <li id="v<?php echo $i?>" <?php if($i==0) {?>class="current"<?php }else{ ?>class=""<?php } ?> onClick="videoRs('<?php echo $i?>',<?php echo $count;?>);"></li>
<?php }?>
</ul></div>
</div>
<span class="mini-view-left"></span>
</div>
</div>
<script>
function videoRs(val){
    setTimeout('videointerval',5000);
 <?php for($i=0;$i<$count;$i++){ ?>
        if(val=='<?php echo $i; ?>'){
            <?php for($j=0;$j<$count;$j++){

            if($j!=$i){ ?>
            $("#vd<?php echo $j; ?>").hide();
            $("#vd<?php echo $j; ?>").removeClass("display_block");
            $("#v<?php echo $j; ?>").removeClass("current");
            <?php }else{ ?>
            $("#vd<?php echo $i; ?>").addClass("display_block");
            $("#vd<?php echo $i; ?>").fadeIn('fast','linear');
            $("#vd<?php echo $i; ?>").removeClass("display_none");
            $("#v<?php echo $i; ?>").addClass("current");
            <?php } } ?>
        }
      <?php } ?>
     }

</script>