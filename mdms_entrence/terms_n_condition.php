<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta content=True name=HandheldFriendly />
      <meta name=viewport content="width=device-width" />
      <meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
      <title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
      <meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
      <meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
      <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
      <link href="css/style.css" rel="stylesheet" type="text/css" />
      <link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
      <style>
          .outer-dounload-tab {
    width: 100%;
    display: inline-block;
}
.outer-dounload-tab .tab {
    overflow: hidden;
    position: relative;
    z-index: 99;
}
.outer-dounload-tab .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    font-size: 18px;
    padding: 16px 30px;
    background: #f1f1f1;
        background-color: rgb(241, 241, 241);
    margin-right: 8px;
    border-radius: 5px 5px 0px 0px;
    font-family: 'Roboto', sans-serif;
}
.outer-dounload-tab .tab button.active {
    background-color: #00a651;
    color: #fff !important;
}
.outer-dounload-tab .tabcontent {
    display: none;
    padding:0px 25px 25px;
    border: 1px solid #00A652;
    margin-top: -1px;
    font-family: 'Roboto', sans-serif;
    border-radius: 0px 5px 0px 0px;
    box-sizing: border-box;
    width: 100%;
    float: left;
}
.btn-data-dwinloa{
    width:100%;
    text-align:center;
}
.btn-data-dwinloa a{
    text-decoration: none;
display: inline-block;
color: #fff;
background: #2dcc70;
padding: 12px 46px;
border-radius: 5px;
font-size: 20px;
}
@media only screen and (max-width:768px) {
    .outer-dounload-tab .tab button{
        width:100%;
        box-sizing:border-box;
        margin:0px 0px 20px;
    }
}
      </style>
   </head>
   <body class="inner-bg">
      <?php include 'registration.php'; ?><?php include 'enquiry.php'; ?><?php include 'social-icon.php'; ?><?php include 'header.php'; ?>
      <section class="inner-banner">
         <div class="wrapper">
            <article>
               <aside class="banner-left">
                  <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
               </aside>
            </article>
         </div>
      </section>
      <section class="inner-gallery-content">
         <div class="wrapper">
            <div class="photo-gallery-main">
               <section class="event-container">
                  <aside class="gallery-left">
                     <div class="inner-left-heading responc-left-heading">
                         <div class="outer-dounload-tab">
			<div class="tab">
			  <button class="tablinks active" onclick="openCity(event, 'First')" id="defaultOpen">Terms & Conditions</button>
			  <button class="tablinks" onclick="openCity(event, 'Second')"> Migration policy</button>
				<button class="tablinks" onclick="openCity(event, 'third')">Refund Policy</button>
			</div>

			<div id="First" class="tabcontent" style="display: block;">
			  <article class="showme-main">
                           <div class="privacy-content">
<!--                               <h4>Terms &amp; Conditions</h4>-->
                              <p>In using, the Online Service of Delhi Academy of Medical Sciences Pvt. Ltd (DAMS) and you (The User) are deemed to have accepted the terms and conditions listed below. Delhi Academy of Medical Sciences Pvt. Ltd reserves the right to add, delete, alter or modify these terms and conditions at any time. The user is therefore advised to read carefully these terms and conditions each time he or she uses the DAMS Online Service(s) of DAMS. All products / services and information displayed constitute an "invitation to offer". Your order for purchase constitutes your "offer" which shall be subject to the terms and conditions as listed below. DAMS reserve the right to accept or reject your offer.</p>
                              <p>The agreement between you and the Delhi Academy of Medical Sciences Pvt. Ltd shall be subject to the following terms and conditions:</p>
                              <ul class="terms-list">
                                 <li><span class="list-arrow"></span> <span class="list-content">The User certifies that he/she is at least 18 (eighteen) years of age or has the consent of a parent or legal guardian.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">These terms and conditions supersede all previous representations, understandings, or agreements and shall prevail notwithstanding any variance with any other terms of any order submitted. By using the DAMS services, you agree to be bound by the Terms and Conditions.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">All prices, unless indicated otherwise are in Indian Rupees.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">All prices and availability of products are subject to change without prior notice at the sole discretion of DAMS.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve the right to refuse or cancel any order placed for a product that is listed at an incorrect price. This shall be regardless of whether the order has been confirmed and/or payment been levied via credit card. In the event the payment has been processed by DAMS online services the same shall be credited to your credit card account and duly notified to you by email or by SMS.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In a credit card transaction, you must use your own credit card. DAMS will not be liable for any credit card fraud. The liability to use a card fraudulently will be on the user and the onus to prove otherwise shall be exclusively on the user.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Any request for cancellations of orders once duly placed on the site, shall not be entertained.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In the event that a non-delivery occurs because of a mistake by you, (i.e. wrong name or address) any extra cost incurred by DAMS for redelivery shall be claimed from the User placing the order.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS shall not be liable for any delay / non-delivery of purchased goods (Study materials, Online Tests, any assignments) by the vendors, trade organization/s, manufacturers / shop etc. (vendors), flood, fire, wars, acts of God or any cause that is beyond the control of DAMS.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">The User agrees to use the services provided by DAMS through their website <a href="http://www.damsdelhi.com">www.damsdelhi.com</a> for lawful purposes only.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">The User agrees to provide authentic and true information. DAMS reserve the right to confirm and validate the information and other details provided by the User at any point of time. If upon confirmation such User details are found not to be true (wholly or partly), DAMS has the right in its sole discretion to reject the registration and debar the User from using <a href="http://www.damsdelhi.com" title="Delhi Academy of MedicalSciences">www.damsdelhi.com</a> and / or other affiliated websites without prior intimation whatsoever.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS will not be responsible for any damage suffered by users from use of the services on this site. This without limitation includes loss of revenue/data resulting from delays, non-deliveries, missed deliveries, or service interruptions as may occur because of any act / omission of the vendor. This disclaimer of liability also applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of contract, tortuous behavior, negligence, or under any other cause of action.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">The User expressly agrees that the use of service(s) DAMS is at the Users sole risk. The Service(s) of DAMS is provided on an as is basis without warranties of any kind, whether express or implied. Delhi Academy of Medical Sciences Pvt. Ltd. its affiliates, employees, agents, consultants, contracted companies make no warranties of any kind, whether expressed or implied, for the service it is providing or as to the results that may be obtained from use of the Service, or as to the accuracy, reliability or content of any information, service, or merchandise provided through this Service. DAMS do not represent or warrant maintaining the confidentiality of information, although DAMS current practice is to utilize reasonable efforts to maintain such confidentiality. It is also clearly understood that all warranties, implied or express take place between the vendors and the User.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">It is compulsory for DAMS users/students to send their self-attested photocopy of mark sheet of MBBS and attach recent two passport size photographs along with dully-filled Admission Form (hard copy) to registered office only.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">It is for the student to see whether he/she is eligible for a certain Entrance Exams or not. DAMS do not hold it responsible if a student's admission form cannot be forwarded or is rejected by the examination body on any ground whatsoever. Such a student cannot claim a refund of the whole or any part of the fee he/she has paid to DAMS.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">The responsibility of getting admission form forwarded to the examining body is that of the student himself/herself.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve the right to use the photograph for publicity in case the student secures position/success in any Exams national/international level test.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Any change in the correspondence address or phone number should be intimated to Registered Office immediate through a written application quoting the Name, Reference ID, and Roll No. of the student.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Students enrolled with DAMS have to provide photocopy of Admit Card of various Entrance Exams. As soon as they receive their admit cards from the examining body.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Each student will be issued an Identity Card. The entry into the classes will be permitted only with Identity Card. If any student is found misusing the I-Card, he/she will be rusticated from the Institute.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In case if any parent/guardian/student misbehaves with any staff members of the Institute unlawfully, his/her ward can be rusticated and no claim of refund of such student will be entertained.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Registered Students can be obtained duplicate I-Card, Set of books &amp; Bag from the Institute against the proof of registration along with payment of requisite fee as applicable from time to time.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If at any point of time, the Govt. further increases service tax, the tax will be borne by the students from the date of enforcement of the act by the Govt.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS reserve its right to make any alteration in its programs / venue / timing and days of classes without any prior notice to anybody. The decision of the Director will be final &amp; binding.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In case of any dispute, arising out of present terms &amp; conditions, it shall be referred to the sole arbitrator to be appointed by the Chairman of Delhi Academy of Medical Sciences Pvt. Ltd. The seat of arbitrator will be at Delhi only. In any case if any matter has to go to the court, only Delhi Court shall have jurisdiction to decide such matters</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Student shall not be entitled to claim any refund of the course fee/attending classes, if any student is found guilty of prohibited act. Students are prohibited from filming/ voice recording/copying any class room lectures/ notes/ study materials/ interactions by using mobile phone/ camera/ or any other manual/ digital recording device for any purposes whatsoever under any circumstances. DAMS reserves its rights to terminate such student with immediate effect and initiate legal action against him. Such a student shall also be liable indemnify the DAMS to the extent the damage incurred due to fault/action of student. </span></li>
                              </ul>
<!--                              <span>REFUND POLICY FOR CLASSROOM COURSES :</span>                
                              <ul class="terms-list">
                                 <li><span class="list-arrow"></span> <span class="list-content">No refund of Admission cum Scholarship Test fee shall be made under any circumstances whatsoever.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">No refund of Registration Fee shall be made under any circumstances whatsoever. It is towards administrative expenses incurred by the company.</span> </li>
                                 <li><span class="list-arrow"></span> <span class="list-content">No refund will be made under any circumstances after joining the Short Term Classroom Courses like Crash Courses / Test Series Courses /Postal Course. </span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If any student pays the fee for any course other than Short Term Classroom Courses and wants to withdraws/asks for refund before the commencement of classes / course in the Institute, the admission fee and first installment of tuition fee paid will be refunded along with PDCs submitted, if any. The registration fee and other government dues will not be refunded.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If any student leaves the classes/Institute after 15 days from the date of commencement of the classes/course in the Institute due to whatsoever reason, then no refund of admission fee and tuition fee paid &amp; encashed will be made under any circumstances. Only the PDC's (if any) which have not been encashed on the date of refund application shall be cancelled / returned.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">All refund cheques will be issued in the name of student / parents only.</span></li>
                              </ul>
                              <span>REFUND POLICY FOR CLASSROOM COURSES :</span>                
                              <ul class="terms-list">
                                 <li><span class="list-arrow"></span> <span class="list-content">The date of commencement of the batch will be considered &amp; not the date of joining of any student in the institute.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Students/Parents must insist for receipt of refund application from the front office. The date of submission of refund application will be considered and not by the number of classes attended by the student or from the date, the student stopped.</span></li>
                              </ul>
                              <span>TRANSFER POLICY :</span>                
                              <p>A student after joining any center of DAMS cannot shift to any other center of DAMS. However, DAMS may help provided there is a seat vacant in the desired course at the desired center. </p>
                              <p>The student will also submit proper details of fee paid at the first centre duly certified from the In-charge of that centre to the new Centre In-charge where transfer is required. Whatsoever amount of fee or its installments, a student might have paid at first centre, the balance fee of the new centre shall be paid at the new centre where transfer is required.</p>
                              <p>DAMS do not guarantee the course/schedules matching the destination center student himself/herself have to check the compatibility of DAMS center transfer cases. The student himself /herself would be responsible for course /schedule compatibility or otherwise.</p>-->
                           </div>
                        </article>
			</div>

			<div id="Second" class="tabcontent" style="display: none;">
			  <article class="showme-main">
                           <div class="privacy-content">
<!--                               <h4>MIGRATION OF COURSE/CENTER POLICY:</h4>-->
                              <p>DAMS do not encourage/promise migration of any course/center once admission is done.
The student himself/herself have to check the compatibility of DAMS course/center
before getting enrolled. The student himself /herself shall be solely responsible for course
/schedule compatibility or otherwise.</p>
                              <p>DAMS reserve all right to migrate any student from one course/center to the other in
exceptional case at its sole discretion.</p>
                              <ul class="terms-list">
                                 <li><span class="list-arrow"></span> <span class="list-content">Only those students will be eligible for migration whose batch/class have not
commenced and only in case if any seat is available at proposed center.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Migration request will be considered only if the student has paid full course fees at 1 st
center.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">The student shall submit request for migration along with proper details of fee paid at
the 1 st center duly certified from the In-charge of 1 st center to the proposed Centre In-
charge where migration is required/ sought and email @ info@damsdelhi.com .</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS do not guarantee the course/schedule compatibility at proposed center. The
student himself /herself shall be solely responsible for course /schedule compatibility
or otherwise.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In case student request for migration is accepted, then in addition to differential fees
(plus GST) student will have to again pay Rs.5000/- as admin charges.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">Study material will be provided from 1 st center in case of migration request is
accepted.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If student requests for migration from higher course fees to lower courses fees then,
he is entitled to the refund of differential fees subject to the following conditions: -
(a) Deduction of admin charge of Rs.5000/-
(b) Migration request must be made at least 1 month prior to the commencement of
class of 1 st course
(c) Student must not have taken any books/online services/E medicoz etc, else cost of
books/online services/E medicoz shall be deducted.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If student’s classes of one course/at 1 st center have commenced: - Admin charge of
Rs.5000/- and proportionate amount of fees (for class commenced) will be deducted
from fees already paid and balance paid fees will be adjusted with new course/center
fees.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In case student want to migrate in the beginning of new session then he has to inform
the management in writing either at time of registration of course or at least six
months before the commencement of session at proposed center.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">For calculation of proportionate fees, class commenced till date of acceptance of
migration application will be taken and not the number of classes actually attended by
the student.</span></li>

<li><span class="list-arrow"></span> <span class="list-content"><b>DAMS Management can withdraw/amend the migration policy at any time without
any prior notice.</b></span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content"><b>This policy does not construe any guarantee to migrate course/center in any case at
all.</b></span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content"><b>To accept and process migration request of student for any course/center is the
discretion of the management. Management can refuse to accept/process migration
request of any student without any intimation/ explanation.</b></span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content"><b>All students at the time of admission are expected to read the contents of the present
migration policy and only upon proper satisfaction may sign the present form. Once
the present policy/ form has been signed by the student, the said student will be
bound by the contractual obligations.</b></span></li>
                              </ul>  
                           </div>
                        </article>
                            <div class="btn-data-dwinloa">
                                <a href="pdf/request_migration.pdf" target="_blanck">Download</a>
                                
                            </div>
			</div>
			<div id="third" class="tabcontent" style="display: none;">
			  <article class="showme-main">
                           <div class="privacy-content">
                              <ul class="terms-list">
                                 <li><span class="list-arrow"></span> <span class="list-content">DAMS do not encourage/promise refund of course fees once admission is done. Student
himself/herself have to check the compatibility of DAMS course/center before getting enrolled.
The student himself /herself shall be solely responsible for course /schedule compatibility or
otherwise.
<br>
                                     However, DAMS reserve all right to refund fees in the exceptional cases for example death of
student, at its sole discretion.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">No refund of Admission cum Scholarship Test fee shall be made under any circumstances
whatsoever.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">In case student request for refund is accepted, then Rs.5000/- will be deducted as admin
charges.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">No refund shall be made under any circumstances for joining the Short Term Courses like
Crash Courses / Test Series Courses /Postal Course/CBT/DVT etc.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">If any student joins for course other than Short Term Courses and claims refund (subject to
genuine reason) before the commencement of classes / course in the Institute: -
<br>
(a) Admission fee paid (with GST) shall be deducted<br>
(b) Admin charge of Rs.5000/- shall be deducted<br>
(c) Student must not have taken/avail any books/online services/E medicoz etc.<br>
(d) Only the PDC&#39;s (if any) which have not been encashed on the date of refund application
shall be cancelled / returned.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">All refund cheque will be issued in the name of student / parents only.</span></li>
                                 <li><span class="list-arrow"></span> <span class="list-content">For calculation of proportionate fees, class commenced till date of acceptance of refund
application will be taken and not the number of classes actually attended by the student.</span></li>
<li><span class="list-arrow"></span> <span class="list-content"><b>DAMS Management can withdraw/amend the refund policy at any time without any prior
notice.</b></span></li>
<li><span class="list-arrow"></span> <span class="list-content"><b>This policy does not construe any guarantee to refund fees in any case at all.</b></span></li>
<li><span class="list-arrow"></span> <span class="list-content"><b>To accept and process refund request of student is the discretion of the management.
Management can refuse to accept/process refund request of any student without any
intimation/ explanation.</b></span></li>
<li><span class="list-arrow"></span> <span class="list-content"><b>All students at the time of admission are expected to read the contents of the present refund
policy and only upon proper satisfaction may sign the present form/ policy. Once the present</b></span></li>
                                   </ul>
                              </div>
                        </article>
                            <div class="btn-data-dwinloa">
                                <a href="pdf/request_letter_refund.pdf" target="_blanck">Download</a>
                            </div>
			</div>
		</div>
                     </div>
                  </aside>
                  <aside class="gallery-right">               <?php include 'enquiryform.php'; ?> </aside>
               </section>
            </div>
         </div>
      </section>
      <?php include 'footer.php'; ?><script type="text/javascript" src="js/html5.js"></script><script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><script type="text/javascript" src="js/registration.js"></script><script type="text/javascript" src="js/add-cart.js"></script>
   <script>
	function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
   </body>
</html>