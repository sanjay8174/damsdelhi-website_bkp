<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Book Store</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<!--<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script> -->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-store-banner">
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aipge_2014.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <!--<div class="banner-right-btns"> <a href="dams-store.php" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
     <div class="banner-right-btns"> <a href="http://damspublications.com/" target="_blank" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li style="background:none;"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <!--<li><a href="dams-store.php" title="DAMS Store">DAMS Store</a></li>-->
          <li><a href="http://damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>
          <li><a title="Publications" class="active-link">Publications</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Publications</h4>
            <article class="showme-main">
              <div class="main-books">
                <ul class="publication-book">
                  <li>
                    <div class="publication-box"> <img src="images/book1.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book2.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="publication-box"> <img src="images/book3.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book4.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="publication-box"> <img src="images/book5.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book6.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>MD/MS DAMS Store</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li style="border:none;"><a href="idams.php" title="iDAMS Tablet">iDAMS Tablet</a></li>
                  <!--<li><a href="dams-store-publication.php" title="DAMS Publications" class="active-store">DAMS Publications</a></li>-->
                  <li><a href="http://damspublications.com/" target="_blank" title="DAMS Publications" class="active-store">DAMS Publications</a></li>
                  <li><a href="online-test-series.php" title="Online Test Series">Online Test Series</a></li>
                  <li><a href="dams-test-series.php" title="DAMS Test Series">DAMS Test Series</a></li>
                  <li><a href="i-dams.php" title="Mobile Applications">Mobile Applications</a></li>
                </ul>
              </div>
            </div>
          </div>
          <?php include 'buynow-right-section.php'; ?>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here --> 
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>