<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Foundation Course for PG Medical Entrance Exam at DAMS, New Delhi, India, AIPG(NBE/NEET) PG, AIPG(NBE/NEET) PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the leading PG Medical Coaching Centre in India offering foundation courses for PG Medical Student" />
<meta name="keywords" content="foundation course for pg medical, Medical Coaching Institute, Post Graduate Medical Coaching, Post Graduate Coaching, Medical Coaching, Medical Coaching India, Medical Coaching Delhi, Medical Coaching Mumbai, Medical Coaching Bombay, AIIMS Entrance, pg coaching, pg medical coaching, pg md coaching, md coaching, medical pg coaching, dams classes, medical classes, pg medical tutor, aiims exam preparation, pg md preparation, pg md medical prepapration, medical preparation, pg medical entrance preparation" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 1;
$courseNav_id = 1;

?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="http://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Foundation Course" class="active-link">Foundation Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
              <h4><span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <h4>Foundation Course</h4>
            <article class="showme-main">
              <aside class="course-icons"> <img src="images/foundation-image.gif" title="Foundation Course" alt="Foundation Course" /> </aside>
              <aside class="course-detail">
                <p>DAMS Foundation courses for Pre Final/Final year Medical Students are highly specialized anDAMS Foundation courses for Pre Final/Final year Medical Students are highly specialized and extremely successful courses. These courses are targeting students who understand the magnitude of the study material these days and tough competition and want to give themselves AN ADDED EDGE by joining early.  As the competition for PG Medical Entrance Exam is day to day getting tougher people who start early surely have an advantage. Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort. </p>
                <p>We are the only medical coaching institute in the country which runs weekend classes for Prefinal and final year students and covers all subjects (of Prefinal &amp; Final Year Subjects)</p>
                <p> (Pre Final Year/Final Year) :<br />
                  Clinical subjects with emphasis on both MCQ and professional preparation. We are the only medical coaching institute which covers Medicine, Surgery, PSM, Pediatrics, Obs-Gyne, Orthopedics, Radiology, Anesthesia, Skin, Psychiatry, Ophthalmology and ENT in the 1st year.</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Course Highlights :-</div>
                <ul class="benefits">
                  <li><span></span>Note only DAMS cover Ophthalmology,  ENT, PSM in foundation courses.</li>
                  <li><span></span>We include SARP subjects in foundation year (skin-anesthesia-radiology-psychiatry)</li>
                  <li><span></span>We have HARRISON'S based teaching  for medicine and comprehensive SABISTON/SCHWATRZ based teaching in surgery</li>
                  <li><span></span>Extensive classroom tests included</li>
                  <li><span></span>Detailed printed notes provided</li>
                  <li><span></span>Our courses finish on time and we have shown that year after year for so many years, students flock to us for this very reason.</li>
                  <li><span></span>We are professionally run institute for the doctors by the doctors</li>
                </ul>
              </aside>
            </article>
          </div>
            
<!--/***********************by Abhinav sharma*******************************-->


            <div class="news-update-box" style="margin-top:30px;">
                <div class="n-videos"><span></span> Videos</div>
                <div class="videos-content-box" >
                <?php

                $count="1";
                for($i=0;$i<$count;$i++){

                  $youtubeUrlthumb="mdms_foundation";
                   $imgSrc="images/mdms_foundation.jpg";

                ?>

                    <div id="vd<?php echo $i;?>" <?php if($i==0) {?>class="display_block"<?php }else{ ?>class="display_none"<?php } ?>>
                            <a target="_blank" href="<?php echo "https://www.youtube.com/embed/1uALxu8LwoE"; ?>" ><img class="border_none" style="width: 100%;" src="<?php echo $imgSrc; ?>" width="100%" height="375" alt="<?php echo $youtubeUrlthumb. '.jpg'; ?>" frameborder="0"></a>
                    </div>
                <?php }?>


                </div>

                <div class="box-bottom-1">
                    <?php if($count>1){ ?>
                <sp an class="mini-view-right"  style="float:none;" ></span>
                <div class="mini-view-midle">
                    <a <?php if($getIntVideo[0][6]=='null'){?>href="#"<?php }else{?>href="achievements.php?c=<?php echo $courseId;?>"<?php }?> class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
                <div class="slider-mini-dot"><ul>
                 <?php for($i=0;$i<$count;$i++){?>
                        <li id="v<?php echo $i?>" <?php if($i==0) {?>class="current"<?php }else{ ?>class=""<?php } ?> onClick="videoRs('<?php echo $i?>',<?php echo $count;?>);"></li>
                <?php }?>
                </ul></div>
                </div>
                <span class="mini-view-left"></span>
                    <?php } ?>
                </div>
                <div style="clear:both;" ></div>
                </div>

<!--                <script>
                function videoRs(val){
                    setTimeout('videointerval',5000);
                 <?php for($i=0;$i<$count;$i++){ ?>
                        if(val=='<?php echo $i; ?>'){
                            <?php for($j=0;$j<$count;$j++){

                            if($j!=$i){ ?>
                            $("#vd<?php echo $j; ?>").hide();
                            $("#vd<?php echo $j; ?>").removeClass("display_block");
                            $("#v<?php echo $j; ?>").removeClass("current");
                            <?php }else{ ?>
                            $("#vd<?php echo $i; ?>").addClass("display_block");
                            $("#vd<?php echo $i; ?>").fadeIn('fast','linear');
                            $("#vd<?php echo $i; ?>").removeClass("display_none");
                            $("#v<?php echo $i; ?>").addClass("current");
                            <?php } } ?>
                        }
                      <?php } ?>
                     }

                </script>-->

<!-- **************************end by Abhinav sharma ******************************--> 

        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <div class="national-quiz-add"> <a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a> </div>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>