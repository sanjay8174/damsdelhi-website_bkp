<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 1;
$courseNav_id = 1;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="test-series">
<?php include 'md-ms-big-nav.php'; ?>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li class="bg_none"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Test Series" class="active-link">Test Series</a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading paddin-zero">
<h4>Postal Test Series <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
<article class="showme-main">
<div class="test-series-content paddin-zero">
<ul class="duration-content">

<li><label>Duration :</label> <span>1 Year</span></li>
<li><label>Total no of tests :</label> <span>Approximately 40 Tests</span></li>
<li><label>Total no. of questions per Test :</label> <span class="grand_inline">300 in GRAND TESTS & 200 IN SUBJECTWISE TESTS, WITH SPECIAL MOCK TEST FOR AIIMS, AIPG (NBE), PGI , NIMHANS, STATE PGs</span></li>
</ul>
<ul class="some-points">
<li><span class="blue_arrow postcourse"></span> <span><p>Questions are based on AIPG (NBE PATTERN), DNB(NBE PATTERN), AIIMS, JIPMER, PGI and various state examination pattern.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>Fully coloured image based questions are based on recent NBE and AIIMS pattern.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>We provide an appropriate mixture of previous years questions and new questions in the question paper to provide student a real examination hall like experience.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>We provide student with option for both online and offline examinations. We have various offline Test Centres for more competitive environment & online test giving platform for more comfortable approach.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>Fully explained solved answers key of all Questions will be give immediately after completion.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>All Students enrolled in between the session will get all previous Test papers and they will be provided an estimate of their ranks for exams they missed .</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>All India Ranking with detailed marks will be provided at our website: <a href="https://www.damsdelhi.com" title="PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG">www.damsdelhi.com</a></p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>All students who enroll in both SWT and GT ,will be offered a free membership to DAMS FACEBOOK EXCLUSIVE CLUB ,where they will be able to ask their personal doubts to the faculty directly.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>All students who enroll in both SWT and GT ,will be offered free access to DAMS CLOUD facilities to see detailed video discussion of GT in addition to the written solution of GT.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>Things have changed in the recent years with new pattern in AIPG with NBE making the exam with complex marking scheme of scaling & equating. Each question is allotted different credit or marks based on how</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>Things have changed in the recent years with new pattern in AIPG with NBE making the exam with complex marking scheme of scaling & equating. Each question is allotted different credit or marks based on how many students were able to do so and how good was the question to discriminate between a high scorer and a low scorer. We are only institute offering similar marking scheme to NBE/NEET.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>Special mock tests for AIIMS, AIPG (NBE), PGI , NIMHANS, STATE PG will conducted before these examinations and these mock tests will provide students with an unique final opportunity to get familar with the exam pattern and practise making minimum silly mistakes just days ahead of the real examination.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>A student progress report will be maintained at the online platform for every student where a time series line graph will enable each student to measure his progress and such time series line graph for last years toppers will also be provided for comparison.</p></span></li>
<li><span class="blue_arrow postcourse"></span> <span><p>DAMS has offline centres allover India and with students enrolled from all parts of India, DAMS test results are truely representive of the All India competition and DAMS ranking gives student a good estimate of their preparation with respect to the the All India competition.</p></span></li>

</ul>
<p>DAMS is the only institute offering authentic courses on the latest NBE pattern.<br></p>
<p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG, AIIMS, PGI, UPSC, DNB & MCI screening and we have been voted as the best or most popular coaching institute of India by various third party surveys.<br></p>
<p>DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG & AIIMS before. We assure to provide best coaching for AIPG (NBE/NEET PATTERN), AIIMS PG entrance, and PGI Chandigarh by our sincere effort. <br><br></p>
</div>
<ul class="idTabs"> 
<li><a href="#test1">Test Series - I</a></li>
<li><a href="#test2">Test Series - II</a></li>
<li><a href="#test3">Test Series - III</a></li>
<li><a href="#test4">Test Series - IV</a></li>
</ul>
<div id="test1">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/topic_distribution.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>05<sup>th </sup>Feb, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>12<sup>th </sup>Feb, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>19<sup>st </sup>Feb, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Feb, 2017</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>05<sup>th </sup>Mar, 2017</label> <span>Pathology including flow Cytometry</span></li>
<li><label>12<sup>th </sup>Mar, 2017</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Mar, 2017</label> <span><b>Grand Test </span></b></li>
<li><label>26<sup>th </sup>Mar, 2017</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>02<sup>nd </sup>Apr, 2017</label> <span>Opthalmology</span></li>
<li><label>09<sup>th </sup>Apr, 2017</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>AIIMS Mock Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>Medicine-I</span></li>
<li><label>30<sup>st </sup>Apr, 2017</label> <span>PGI Mock Test</span></li>
<!--<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>DNB MOCK TEST</span></li>
<li><label>30<sup>st </sup>May, 2017</label> <span>Medicine-I</span></li>-->
<li class="green_bg"><label>07<sup>th </sup>May, 2017</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>14<sup>th </sup>May, 2017</label> <span>Medicine-II</span></li>
<li><label>21<sup>nd </sup>May, 2017</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>28<sup>th </sup>May, 2017</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>04<sup>th </sup>Jun, 2017</label> <span>Surgery-I</span></li>
<li><label>11<sup>th </sup>Jun, 2017</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Jun, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Jun, 2017</label> <span>Orthopedic and Radiology</span></li>
<li><label>02<sup>nd </sup>Jul, 2017</label> <span>Dermatology-Psychiatry</span></li>
<li><label>09<sup>th </sup>Jul, 2017</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Jul, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Jul, 2017</label> <span>Paeds-II</span></li>
<li><label>30<sup>th </sup>Jul, 2017</label> <span>Oncology</span></li>
</ul>
</div>
</div></div>
<div id="test2">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/topic_distribution.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<!--<li><label>05<sup>th </sup>Feb, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>12<sup>th </sup>Feb, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>19<sup>st </sup>Feb, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Feb, 2017</label> <span>Preven<li><label>05<sup>th </sup>Feb, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>12<sup>th </sup>Feb, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>19<sup>st </sup>Feb, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Febtive &amp; Social Medicine including Biostatistics</span></li>-->
<li><label>05<sup>th </sup>Mar, 2017</label> <span>Pathology including flow Cytometry</span></li>
<li><label>12<sup>th </sup>Mar, 2017</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Mar, 2017</label> <span><b>Grand Test </span></b></li>
<li><label>26<sup>th </sup>Mar, 2017</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>02<sup>nd </sup>Apr, 2017</label> <span>Opthalmology</span></li>
<li><label>09<sup>th </sup>Apr, 2017</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>AIIMS Mock Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>Medicine-I</span></li>
<li><label>30<sup>st </sup>Apr, 2017</label> <span>PGI Mock Test</span></li>
<!--<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>DNB MOCK TEST</span></li>
<li><label>30<sup>st </sup>May, 2017</label> <span>Medicine-I</span></li>-->
<li class="green_bg"><label>07<sup>th </sup>May, 2017</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>14<sup>th </sup>May, 2017</label> <span>Medicine-II</span></li>
<li><label>21<sup>nd </sup>May, 2017</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>28<sup>th </sup>May, 2017</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>04<sup>th </sup>Jun, 2017</label> <span>Surgery-I</span></li>
<li><label>11<sup>th </sup>Jun, 2017</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Jun, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Jun, 2017</label> <span>Orthopedic and Radiology</span></li>
<li><label>02<sup>nd </sup>Jul, 2017</label> <span>Dermatology-Psychiatry</span></li>
<li><label>09<sup>th </sup>Jul, 2017</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Jul, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Jul, 2017</label> <span>Paeds-II</span></li>
<li><label>30<sup>th </sup>Jul, 2017</label> <span>Oncology</span></li>
<li><label>06<sup>th </sup>Aug, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>13<sup>th </sup>Aug, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>20<sup>th </sup>Aug, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>27<sup>th </sup>Aug, 2017</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
</ul>
</div>
</div></div>
<div id="test3">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/topic_distribution.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>02<sup>nd </sup>Apr, 2017</label> <span>Opthalmology</span></li>
<li><label>09<sup>th </sup>Apr, 2017</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>AIIMS Mock Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>Medicine-I</span></li>
<li><label>30<sup>st </sup>Apr, 2017</label> <span>PGI Mock Test</span></li>
<!--<li class="yellow_bg"><label>16<sup>th </sup>Apr, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Apr, 2017</label> <span>DNB MOCK TEST</span></li>
<li><label>30<sup>st </sup>May, 2017</label> <span>Medicine-I</span></li>-->
<li class="green_bg"><label>07<sup>th </sup>May, 2017</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>14<sup>th </sup>May, 2017</label> <span>Medicine-II</span></li>
<li><label>21<sup>nd </sup>May, 2017</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>28<sup>th </sup>May, 2017</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>04<sup>th </sup>Jun, 2017</label> <span>Surgery-I</span></li>
<li><label>11<sup>th </sup>Jun, 2017</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Jun, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Jun, 2017</label> <span>Orthopedic and Radiology</span></li>
<li><label>02<sup>nd </sup>Jul, 2017</label> <span>Dermatology-Psychiatry</span></li>
<li><label>09<sup>th </sup>Jul, 2017</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Jul, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Jul, 2017</label> <span>Paeds-II</span></li>
<li><label>30<sup>th </sup>Jul, 2017</label> <span>Oncology</span></li>
<li><label>06<sup>th </sup>Aug, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>13<sup>th </sup>Aug, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>20<sup>th </sup>Aug, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>27<sup>th </sup>Aug, 2017</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>03<sup>th </sup>Sep, 2017</label> <span>Pathology including flow Cytometry</span></li>
<li><label>10<sup>th </sup>Sep, 2017</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Sep, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Sep, 2017</label> <span>Microbiology including Microbial Biodegradation</span></li>
</ul>
</div>
</div></div>
<div id="test4">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/topic_distribution.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
 <li><label>14<sup>th </sup>May, 2017</label> <span>Medicine-II</span></li>
<li><label>21<sup>nd </sup>May, 2017</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>28<sup>th </sup>May, 2017</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>04<sup>th </sup>Jun, 2017</label> <span>Surgery-I</span></li>
<li><label>11<sup>th </sup>Jun, 2017</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Jun, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Jun, 2017</label> <span>Orthopedic and Radiology</span></li>
<li><label>02<sup>nd </sup>Jul, 2017</label> <span>Dermatology-Psychiatry</span></li>
<li><label>09<sup>th </sup>Jul, 2017</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Jul, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Jul, 2017</label> <span>Paeds-II</span></li>
<li><label>30<sup>th </sup>Jul, 2017</label> <span>Oncology</span></li>
<li><label>06<sup>th </sup>Aug, 2017</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>13<sup>th </sup>Aug, 2017</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>20<sup>th </sup>Aug, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>27<sup>th </sup>Aug, 2017</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>03<sup>th </sup>Sep, 2017</label> <span>Pathology including flow Cytometry</span></li>
<li><label>10<sup>th </sup>Sep, 2017</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Sep, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Sep, 2017</label> <span>Microbiology including Microbial Biodegradation</span></li>

<li><label>01<sup>st </sup>Oct, 2017</label> <span>Opthalmology</span></li>
<li><label>08<sup>th </sup>Oct, 2017</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>15<sup>th </sup>Oct, 2017</label> <span><b>Grand Test</b></span></li>
<li><label>22<sup>th </sup>Oct, 2017 </label> <span>Medicine-I</span></li>
<!--<li class="light_green_bg"><label>30<sup>th </sup>Oct, 2016</label> <span> AIIMS Mock Test</span></li>-->
</ul>
</div>
</div></div>

</article>
</div>
</aside>
<aside class="gallery-right">
<?php include 'right-accordion.php'; ?>
<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>