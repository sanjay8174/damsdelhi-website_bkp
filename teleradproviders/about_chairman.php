<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Teleradiology Providers - About Chairman</title>

<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="images/favicon.ico" />

<link rel="stylesheet" type="text/css" href="menu/chromestyle.css" />

<script type="text/javascript" src="menu/chrome.js"></script>

<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>



<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onload="MM_preloadImages('images/layout_711.gif','images/layout_741.gif','images/layout_771.gif','images/layout_7s7.gif')">
<div id="wrapper"><!--wrapper-->
<div id="container1"><!--headr-->


<?php include('header1.php')?>
<!--navigation-->

<div id="container2">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="250" valign="top">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="4">
			<img border="0" src="images/head_left.gif" width="10" height="40"></td>
			<td background="images/bg_head.gif">
			<h4><img src="images/bullet.gif" width="11" height="11" />&nbsp;&nbsp;About Us</h4>
			</td>
			<td width="4">
			<img border="0" src="images/head_right.gif" width="10" height="40"></td>
		</tr>
	</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">
<div id="box-nav-left">
<div class="wireframemenu">
<ul>
<li><a href="about_us.php">&nbsp;Teleradiology Providers</a></li>
<li><a href="about_chairman.php">&nbsp;&nbsp;About Chairman</a></li>
<li><a href="about_team.php">&nbsp;&nbsp;About Team</a></li>
<li><a href="misson_vision.php">&nbsp;&nbsp;Mission &amp; Vision</a></li>
</ul>
</div>
</div>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">&nbsp;
	</td>
						</tr>
						<tr>
							<td height="10">
							<img border="0" src="images/spacer.gif" width="1" height="1"></td>
						</tr>
						<tr>
							<td><table border="0" width="100%" cellspacing="0" cellpadding="0" height="0">
                <tr>
                  <td bgcolor="#CCCCCC"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
             <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="4"><img border="0" src="images/head2_left.gif" width="10" height="40" /></td>
                              <td background="images/bg_head2.gif" class="text3"><h5>
								Query Form</h5></td>
                              <td width="4"><img border="0" src="images/head2_right.gif" width="10" height="40" /></td>
                            </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td>
                        <table height="390"  border="0" width="100%" cellspacing="1" cellpadding="0">
                            <tr>
                              <td bgcolor="#FFFFFF">
                                <form name="frmform" id="frmform" method="post" onsubmit="return check_null();" action="contactus2.php">
                                  <table border="0" width="100%" cellspacing="0" cellpadding="10">
                                    <tr>
                                      <td align="center" width="420" bgcolor="#FFFFFF"><table class="text" width="90%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td width="39%" scope="col">Name</td>
                                            <td width="61%" scope="col" align="right"><label>
                                              <input type="text" name="name"  class="textfld1"id="textfield" />
                                              </label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <th colspan="2" scope="row" height="11"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row" >Hospital</td>
                                            <td align="right"><input type="text" name="hospital"  class="textfld1"id="textfield2" /></td>
                                          </tr>
                                          <tr>
                                            <th height="10" colspan="2" scope="row"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">Address</td>
                                            <td align="right"><textarea class="textfld2" name="address" id="textarea" cols="45" rows="5"></textarea>
                                            </td>
                                          </tr>
                                          <tr>
                                            <th colspan="2" scope="row" height="11"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">City</td>
                                            <td align="right"><input type="text" name="city"  class="textfld1"id="textfield5" /></td>
                                          </tr>
                                          <tr>
                                            <th height="10" colspan="2" scope="row"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">Country</td>
                                            <td align="right"><select name="txtcountry" size="1" class="textfldlist" id="txtcountry">
                                                <option value="choose">---Choose 
                                                  One---</option>
                                                <option value="Afghanistan"> Afghanistan </option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa"> American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antarctica">Antarctica </option>
                                                <option value="Antigua And Barbuda"> Antigua And Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan </option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh </option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Bouvet Island">Bouvet 
                                                  Island </option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Territory"> British Indian Ocean Territory</option>
                                                <option value="Brunei">Brunei</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina 
                                                  Faso </option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option>Cape Verde</option>
                                                <option value="Cayman Islands">Cayman 
                                                  Islands</option>
                                                <option value="Central African Republic"> Central African Republic</option>
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="China (Hong Kong S.A.R.)"> China (Hong Kong S.A.R.)</option>
                                                <option value="China (Macau S.A.R.)"> China (Macau S.A.R.)</option>
                                                <option value="Christmas Island"> Christmas Island</option>
                                                <option value="Cocos (Keeling) Islands"> Cocos (Keeling) Islands</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Congo, Democractic Republic of"> Congo, Democractic Republic of</option>
                                                <option value="Cook Islands">Cook 
                                                  Islands </option>
                                                <option value="Costa Rica">Costa Rica </option>
                                                <option value="Cote D'Ivoire (Ivory Coast)"> Cote D&#39;Ivoire (Ivory Coast)</option>
                                                <option value="Croatia (Hrvatska)"> Croatia (Hrvatska)</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech 
                                                  Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic"> Dominican Republic</option>
                                                <option value="East Timor">East Timor </option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El 
                                                  Salvador </option>
                                                <option value="Equatorial Guinea"> Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands (Malvinas)"> Falkland Islands (Malvinas)</option>
                                                <option value="Faroe Islands">Faroe 
                                                  Islands </option>
                                                <option value="Fiji Islands">Fiji 
                                                  Islands </option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French 
                                                  Guiana </option>
                                                <option value="French Polynesia"> French Polynesia</option>
                                                <option value="French Southern Territories"> French Southern Territories</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe </option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-Bissau"> Guinea-Bissau </option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Heard and McDonald Islands"> Heard and McDonald Islands</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India" selected="selected"> India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan </option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea">Korea</option>
                                                <option value="Korea, North">Korea, 
                                                  North </option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan </option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein"> Liechtenstein </option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg </option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar </option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands"> Marshall Islands</option>
                                                <option value="Martinique">Martinique </option>
                                                <option value="Mauritania">Mauritania </option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Micronesia">Micronesia </option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montserrat">Montserrat </option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique </option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherlands Antilles"> Netherlands Antilles</option>
                                                <option value="Netherlands, The"> Netherlands, The</option>
                                                <option value="New Caledonia">New 
                                                  Caledonia </option>
                                                <option value="New Zealand">New 
                                                  Zealand </option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk 
                                                  Island</option>
                                                <option value="Northern Mariana Islands"> Northern Mariana Islands</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau">Palau</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua new Guinea">Papua 
                                                  new Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines"> Philippines </option>
                                                <option value="Pitcairn Island"> Pitcairn Island</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto 
                                                  Rico </option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russia">Russia</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Saint Helena">Saint 
                                                  Helena </option>
                                                <option value="Saint Kitts And Nevis"> Saint Kitts And Nevis</option>
                                                <option value="Saint Lucia">Saint 
                                                  Lucia </option>
                                                <option value="Saint Pierre and Miquelon"> Saint Pierre and Miquelon</option>
                                                <option value="Saint Vincent And The Grenadin"> Saint Vincent And The Grenadin</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino </option>
                                                <option value="Sao Tome and Principe"> Sao Tome and Principe</option>
                                                <option value="Saudi Arabia">Saudi 
                                                  Arabia </option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Seychelles">Seychelles </option>
                                                <option value="Sierra Leone">Sierra 
                                                  Leone </option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands"> Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South 
                                                  Africa </option>
                                                <option value="South Georgia">South 
                                                  Georgia </option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Svalbard And Jan Mayen Islands"> Svalbard And Jan Mayen Islands</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland"> Switzerland </option>
                                                <option value="Syria">Syria</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan </option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad And Tobago"> Trinidad And Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan"> Turkmenistan </option>
                                                <option value="Turks And Caicos Islands"> Turks And Caicos Islands</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Emirates"> United Arab Emirates</option>
                                                <option value="United Kingdom">United 
                                                  Kingdom</option>
                                                <option value="United States">United 
                                                  States </option>
                                                <option value="United States Pacific Islands"> United States Pacific Islands</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan </option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Vatican City State (Holy See)"> Vatican City State (Holy See)</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands (British)"> Virgin Islands (British)</option>
                                                <option value="Virgin Islands (US)"> Virgin Islands (US)</option>
                                                <option value="Wallis And Futuna Islands"> Wallis And Futuna Islands</option>
                                                <option value="Western Sahara">Western 
                                                  Sahara</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Yugoslavia">Yugoslavia </option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                            </select></td>
                                          </tr>
                                          <tr>
                                            <th colspan="2" scope="row" height="10"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">Telephone</td>
                                            <td align="right"><input type="text" name="phone"  class="textfld1"id="phone" /></td>
                                          </tr>
                                          <tr>
                                            <th colspan="2" scope="row" height="11"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">Email</td>
                                            <td align="right"><input type="text" name="emailid"  class="textfld1"id="emailid" /></td>
                                          </tr>
                                          <tr>
                                            <th colspan="2" scope="row" height="11"></th>
                                          </tr>
                                          <tr>
                                            <td scope="row">Comments</td>
                                            <td align="right"><textarea class="textfld2" name="comments" id="comments" cols="45" rows="5"></textarea></td>
                                          </tr>
                                          <tr>
                                            <th scope="row" height="10"></th>
                                            <td>&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <th scope="row">&nbsp;</th>
                                            <td align="right"><input type="image" src="images/layout_52.gif" name="Image1" width="86" height="35" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','images/layout_7s7.gif',1)" onmouseout="MM_swapImgRestore()" /></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                  </table>
                                </form></td>
                            </tr>
                        </table></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td bgcolor="#CCCCCC"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  </table></td>
                </tr>
            </table></td>
						</tr>
						</table>
					</td>
					<td width="20">&nbsp;</td>
					<td valign="top">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="6">
			<img border="0" src="images/con_inner_topleft.gif" width="15" height="15"></td>
			<td bgcolor="#FFFFFF">
			<img border="0" src="images/spacer.gif" width="1" height="1"></td>
			<td width="4">
			<img border="0" src="images/con_inner_topright.gif" width="15" height="15"></td>
		</tr>
	</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="15">&nbsp;</td>
		<td width="530">
		<div class="innertab" id="chromemenu0">
<ul>
<li><a href="about_us.php">Teleradiology Providers</a></li>
<li class="about_chariman"><a href="about_chairman.php">About Chairman</a></li>
<li><a href="about_team.php" >About Team</a></li>
<li><a href="misson_vision.php" >Mission &amp; Vision</a></li>
</ul>
</div></td>
		<td background="images/content_topleft.gif">&nbsp;</td>
		<td width="15">&nbsp;</td>
	</tr>
	</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	  <td>&nbsp;</td>
	  <td style="border-left: 1px solid #DBDBDB; border-right: 1px solid #DBDBDB;">
  <table width="100%" border="0" class="text" cellspacing="15" cellpadding="0">
    <tr>
      <td><p><font color="#DC1301"><b>About Our Chairman</b></font></p>
        <p>Teleradiology  Providers (Unit of Prime Telerad Providers (P) Ltd) is lead by Dr S.C. Sethi  &nbsp;who is a very senior and reputed chest physician in Delhi who retired as  In-charge of Chest Centre in Delhi. During his vast career in India and abroad  he has served in various hospitals in Delhi, Punjab and Middle East including  Benghazi, Libya.</p>
        <p>He  believes that one of the most serious, but under-publicized, health care worker  shortages is the lack of radiologists.&nbsp; Further he says “given the  defensive medicine being followed, the demands on radiologists to be perfect  and accurate are increasing, and with more workload, radiologists will be  &nbsp;unable to cope with the pressure.” According to him “Teleradiology comes  to mind as the logical solution to this human logistics nightmare. Global  Radiology concept will ultimately enable the flow of radiology expertise from  areas of surplus to areas of requirement, worldwide. “</p>
        <p>Together  we believe in the Thomas Friedman’s philospophy of flatworld he says--“When you  start to think of the world as flat, a lot of things make sense in ways they  did not before. What the flattening of the world means is that we are now  connecting all the knowledge centers on the planet together into a single  global network.”</p>
        </td>
      </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      </tr>
    </table>
							</td>
	  <td>&nbsp;</td>
	  </tr>
	<tr>
		<td width="15">
			<img border="0" src="images/spacer.gif" width="1" height="1"></td>
		<td ><table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="6">
			<img border="0" src="images/con_inner_bottleft.gif" width="15" height="15"></td>
			<td background="images/innerbox_bott.gif" bgcolor="#FFFFFF">
			<img border="0" src="images/spacer.gif" width="1" height="1"></td>
			<td width="4">
			<img border="0" src="images/con_inner_bottright.gif" width="15" height="15"></td>
		</tr>
	</table></td>
		<td width="15">
			<img border="0" src="images/spacer.gif" width="1" height="1"></td>
	</tr>
	</table>
						  </td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">&nbsp;
  </td>
						</tr>
						<tr>
							<td>	</td>
						</tr>
					</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		</table>
  
  
  </div>
   <?php include('footer.php')?>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12539835-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>

<script language="javascript">
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}


function check_null()
{
if(document.frmform.name.value=="")
	{
		alert("Please Enter your Name")
		document.frmform.name.focus()
		return false
	}
	
		if(document.frmform.hospital.value=="")
	{
		alert("Please Enter your Hospital")
		document.frmform.hospital.focus()
		return false
	}
			if(document.frmform.address.value=="")
	{
		alert("Please Enter your Address")
		document.frmform.address.focus()
		return false
	}
	
	
	if(document.frmform.city.value=="")
	{
		alert("Please Enter your City")
		document.frmform.city.focus()
		return false
	}

	if((document.frmform.phone.value==null)||(document.frmform.phone.value==""))
	{
		alert("Please Enter your Phone Number")
		document.frmform.phone.focus()
		return false
	}
	
	if(isInteger(document.frmform.phone.value)==false)
	{
		alert("Please Enter a Valid Phone Number")
		document.frmform.phone.value=""
		document.frmform.phone.focus()
		return false
	}

	if(document.layers||document.getElementById||document.all)
	{
		var str=document.frmform.emailid.value
		var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		if (filter.test(str)==false)
		{
			alert("Please input a valid email address!")
			document.frmform.emailid.value=""
			document.frmform.emailid.focus()
			return false
		}
	
	}

}
</script>