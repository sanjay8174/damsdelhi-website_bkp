<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>

<title>TutToaster AJAX Contact Form</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

<style type="text/css">

/* CSS Document */

body {
color:#222;
font-size:16px;
font-family:helvetica, verdana, arial, san-serif;
line-height:1.5em;
}

a {
color:#06F;
text-decoration:underline;
}

p {
padding:10px 0;
font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px
}

#container {
width:900px;
margin:20px auto;
}

/*contact form*/
#mask {
background-color:#000;
display:none;
height:100%;
left:0;
position:fixed;
top:0;
width:100%;
z-index:9000;
}

#contact {
background-color:#fff;
display:none;
left:50%;
margin-left:-225px;
position:absolute;
top:40px;
width:450px;
z-index:9999;
border-radius:10px;
-moz-border-radius:10px;
-webkit-border-radius:10px;
padding:20px;
}

#close {
background:url(images/close.png) no-repeat right;
cursor:pointer;
font-family:arial, sans-serif;
font-size:20px;
font-weight:700;
line-height:24px;
text-decoration:underline;
text-align:right;
padding:11px 30px 11px 5px;
position:absolute;
top:5px;
right:5px
}

#contact_header {
font-family:calibri, Verdana, Arial, Helvetica, sans-serif;
font-size:22px;
font-weight:700;
line-height:50px;
padding:5px 5px 10px 30px
}

/* form components */
input,textarea {
border:1px solid silver;
color:#aaaaaa;
font-size:10px;
font-family:Verdana, Arial, sans-serif;
border-radius:5px;
-moz-border-radius:5px;
-webkit-border-radius:5px;
padding:10px;
}

input:hover[type=text],input:focus[type=text],textarea:hover,textarea:focus {
background-color:#eeeeee;
border:1px solid #000;
color:#666666
}

input[type=text],textarea {
width:300px;
float:right
}

#submit {
border:none;
background-image:url(images/butt_submit.gif);
background-repeat:no-repeat;
margin-right:5px;
width:106px;
height:26px;
color:#337c9d
}

#reset {
border:none;
background-image:url(images/butt_reset.gif);
background-repeat:no-repeat;
margin-right:5px;
width:93px;
height:26px;
color:#337c9d
}


#reset:hover {
cursor:pointer;
}

#submit:hover {
cursor:pointer;
}

/* alert messages */
.success,.error {
color:#000;
display:none;
font-size:15px;
font-weight:700;
border-radius:4px;
-moz-border-radius:4px;
-webkit-border-radius:4px;
padding:5px 10px 5px 10px;
margin-bottom: 10px;
margin-left:108px
}

.success {
background-color:#9F6;
border:1px solid #0F0;
}

.error {
background-color:#F66;
border:1px solid red;
}

</style>





<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="contact.js"></script>

</head>

<body>

<div id="container">

<h1>This is a web page</h1>

<p>This is the main body.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in lacus vitae tortor vehicula cursus a pretium odio. Suspendisse potenti. Phasellus semper nisl a massa vulputate at vulputate justo tempor. Quisque vel enim aliquam risus varius aliquam iaculis non odio. Pellentesque molestie, lorem vitae vestibulum porta, elit eros fringilla nunc, cursus ultricies nisi neque sed ipsum. Nam enim nisl, gravida sed consequat non, faucibus eget massa. Nullam pulvinar, diam et eleifend viverra, purus sem pellentesque sem, non tempor ante neque eu dolor. Quisque adipiscing, tortor ac feugiat feugiat, enim mi porttitor nunc, in bibendum diam sem eu leo. Morbi gravida, leo ut auctor cursus, urna nulla viverra eros, vitae dictum arcu quam sed odio. Proin eleifend urna a justo suscipit vehicula. Pellentesque vitae elit eros. Nulla vehicula velit nec dui vulputate viverra. Curabitur non dolor ac velit semper faucibus vel eget dolor.</p>

<p>A <a class="modal" href="">contact link</a>.</p>
</div><!--end container-->

<!--contact form-->

<div id="contact">
	<div id="close"></div>

	<div id="contact_header">Scanner Contact Form</div>
	<p class="success">Thanks! Your message has been sent.</p>

  <form action="send.php" method="post" name="contactForm" id="contactForm" style="width:400px; margin-left:30px">
 <p>Name: *<span><input name="name" id="name" type="text" size="30" value="" /></span></p>
  <p>Email: *<span><input name="email" id="email" type="text" size="30" value="" /></span></p>
  <p>Contact:<span><input name="cp" id="cp" type="text" size="30" value="" /></span></p>
    <p>Quantity:<span><input name="quantity" id="quantity" type="text" size="30" value="" /></span></p>
  <p>Message: *<span><textarea name="message" id="message" rows="2" cols="40"></textarea></span></p>
  
  <p style="clear:both"><span><!--<input name="cpt" id="cpt" type="text" size="30" value="" style="width:150px" />--></span></p>
  <input type="hidden"  name="scname" id="scname" value="">
  
  <p style="clear:both; margin-left:77px"><input type="submit" id="submit" name="submit" value="" /><input type="reset" id="reset" name="submit" value="" /></p>
 </form>
</div>

<div id="mask"></div>

<!--end contact form-->

</body>
</html>