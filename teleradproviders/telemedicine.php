<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Radiology, Remote Radiology, Online Radiology Reporting, New Delhi, India</title>

<meta name="Description" content= "Teleradiology Providers provides advanced remote radiology reporting services for your setup.  We have exposure in neuroradiology, usculoskeletal radiology, cardiac imaging, breast imaging and abdominal imaging."/>

<meta name="Keywords" content= "Radiology, Remote Radiology, Online Radiology, Radiology Reporting, Radiology Services"/>

<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="images/favicon.ico" />

<link rel="stylesheet" type="text/css" href="menu/chromestyle.css" />

<script type="text/javascript" src="menu/chrome.js"></script>
<script src="Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="Scripts/newjavascript.js"></script>
<link href="fonts/flaticon.css" type="text/css" rel="stylesheet"></link>
<link href="css/woco-accordion.css" type="text/css" rel="stylesheet"></link>
<script src="Scripts/woco.accordion.js" type="text/javascript"></script>

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onload="MM_preloadImages('images/layout_711.gif','images/layout_741.gif','images/layout_771.gif','images/layout_7s7.gif')">
<div id="wrapper"><!--wrapper-->
<div id="container1"><!--headr-->
<div id="hedr1">
  <script type="text/javascript">
AC_FL_RunContent( 'codebase','https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','146','height','122','src','ani/globe_','quality','high','pluginspage','https://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','ani/globe_' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="146" height="122">
    <param name="movie" value="ani/globe_.swf" />
    <param name="quality" value="high" />
    <embed src="ani/globe_.swf" quality="high" pluginspage="https://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="146" height="122"></embed>
  </object>
</noscript></div>
<div id="hedr2">
<div align="right" style=" padding:2px 2px 0 0; height:115px; width:830px">
  <script type="text/javascript">
AC_FL_RunContent( 'codebase','https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','777','height','113','src','ani/banner','quality','high','pluginspage','https://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','ani/banner' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="777" height="113">
    <param name="movie" value="ani/banner.swf" />
    <param name="quality" value="high" />
    <embed src="ani/banner.swf" quality="high" pluginspage="https://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="777" height="113"></embed>
  </object>
</noscript></div>
</div>
</div>
<!--headr-->
<!--add-->
<!--add-->
<div id="top-nav"><!--navigation-->
<div class="chromestyle" id="chromemenu">
<ul>
<li class="home"><a href="index.php">Home</a></li>
<li><a href="about_us.html" rel="dropmenu1">About Us </a></li>
<li><a href="#" rel="dropmenu2">Services</a></li>
<li><a href="#" rel="dropmenu3" >Telemedicine</a></li>
<li><a href="#" rel="dropmenu7">Second Opinion</a></li>
<li><a href="news.html">News</a></li>
<li><a href="press.html" >Press &amp; Event</a></li>


<li><a href="download.html" >Download</a></li>

<li><a href="career.html">Career</a></li>

<li><a href="contactus.html">Contact Us</a></li>



</ul>

</div>



<div id="dropmenu3" class="dropmenudiv" >

<a href="about-teledermotology.html">About Telemedicine</a>

<a href="teledermotology.html">Our Team of Teledermatolgists</a>

<a href="submission-method.html">Submission Method</a>
<a href="teledermotology-polocy.html">Disclaimer  & Privacy Policy</a>

</div>

<div id="dropmenu7" class="dropmenudiv" >

<a href="submission_method_why.php">Second Opinion-Why</a>

<a href="submission.php">How To Proceed?</a>

</div>



<!--1st drop down menu -->                                                   
<div id="dropmenu1" class="dropmenudiv">
<a href="about_us.html">About Tele Rad Providers</a>
<a href="about_chairman.html">About Chairman</a>
<a href="about_team.html">About Team</a>
<a href="misson_vision.html">Mission &amp; Vission</a>
</div>


<!--2nd drop down menu -->                                                
<div id="dropmenu2" class="dropmenudiv" >

<a href="radiology_services.html">Radiology Reporting</a>

<a href="3d_reconstruction.html">3 D Reconstruction</a>

<a href="rad_learning_programme.html">Rad Learning Programme</a>
<a href="international_preread_service.html">International Preread Service</a>

</div>

<!--3rd drop down menu -->                                                   
<div id="dropmenu3" class="dropmenudiv" >
<a href="#">News behind the News</a>
<a href="#">Readership</a>
<a href="#">Opinions</a>
<a href="#">Face to Face Interviews</a>
<a href="#">Curret Issues</a>
<a href="#">Archives</a>
<a href="#">Subscribe</a></div>


<!--4rd drop down menu -->                                                   
<div id="dropmenu4" class="dropmenudiv" >
<a href="#">Latest Study</a>
<a href="#">Archives</a>
<a href="#">Buy Online</a></div>


<!--5rd drop down menu -->                                                   
<div id="dropmenu5" class="dropmenudiv" >
<a href="#">Magzine Online</a>
<a href="#">Editorail Resorces</a>
<a href="#">Special Studeis Archives</a>
<a href="#">Specialized Outsources Services</a>



<!--6rd drop down menu -->                                                   
<div id="dropmenu6" class="dropmenudiv" >
<a href="#">Subscribe</a>
<a href="#">Advertise</a>
<a href="#">Careers</a>
<a href="#">Others</a></div>




<script type="text/javascript">

cssdropdown.startchrome("chromemenu")

</script>
</div>
<!--navigation-->
<div id="container2">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td width="250" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="4"><img border="0" src="images/head_left.gif" width="10" height="40" /></td>
                  <td background="images/bg_head.gif"><h4><img src="images/bullet.gif" width="11" height="11" />&nbsp;&nbsp;Radiology Reporting</h4></td>
                  <td width="4"><img border="0" src="images/head_right.gif" width="10" height="40" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td bgcolor="#FFFFFF">
              <div id="box-nav-left">
<div class="wireframemenu">
<ul>
<li><a href="radiology_services.html">&nbsp;&nbsp;Teleradiology Services</a></li>
<li><a href="radiology_tech.html">&nbsp;&nbsp;Technology</a></li>
<li><a href="radiology_quality_analysis.html">&nbsp;&nbsp;Quality Analysis Programme</a></li>
</ul>
</div>
</div>
</td>
            </tr>
            <tr>
              <td bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
              <td height="10"><img border="0" src="images/spacer.gif" width="1" height="1" /></td>
            </tr>
            <tr>
              <td><table border="0" width="100%" cellspacing="0" cellpadding="0" height="0">
                <tr>
                  <td bgcolor="#CCCCCC"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><img border="0" src="images/head2_left.gif" width="10" height="40" /></td>
                          <td background="images/bg_head2.gif" class="text3"><h5> Query Form</h5></td>
                          <td width="4"><img border="0" src="images/head2_right.gif" width="10" height="40" /></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table height="390"  border="0" width="100%" cellspacing="1" cellpadding="0">
                        <tr>
                          <td bgcolor="#FFFFFF">
                          <form name="frmform" id="frmform" method="post" onsubmit="return check_null();" action="contactus2.php">
                            <table border="0" width="100%" cellspacing="0" cellpadding="10">
                              <tr>
                                <td align="center" width="420" bgcolor="#FFFFFF"><table class="text" width="90%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td width="39%" scope="col">Name</td>
                                      <td width="61%" scope="col" align="right"><label>
                                        <input type="text" name="name"  class="textfld1"id="textfield" />
                                        </label>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" scope="row" height="11"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row" >Hospital</td>
                                      <td align="right"><input type="text" name="hospital"  class="textfld1"id="textfield2" /></td>
                                    </tr>
                                    <tr>
                                      <th height="10" colspan="2" scope="row"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">Address</td>
                                      <td align="right"><textarea class="textfld2" name="address" id="textarea" cols="45" rows="5"></textarea>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" scope="row" height="11"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">City</td>
                                      <td align="right"><input type="text" name="city"  class="textfld1"id="textfield5" /></td>
                                    </tr>
                                    <tr>
                                      <th height="10" colspan="2" scope="row"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">Country</td>
                                      <td align="right"><select name="txtcountry" size="1" class="textfldlist" id="txtcountry">
                                          <option value="choose">---Choose 
                                            One---</option>
                                          <option value="Afghanistan"> Afghanistan </option>
                                          <option value="Albania">Albania</option>
                                          <option value="Algeria">Algeria</option>
                                          <option value="American Samoa"> American Samoa</option>
                                          <option value="Andorra">Andorra</option>
                                          <option value="Angola">Angola</option>
                                          <option value="Anguilla">Anguilla</option>
                                          <option value="Antarctica">Antarctica </option>
                                          <option value="Antigua And Barbuda"> Antigua And Barbuda</option>
                                          <option value="Argentina">Argentina</option>
                                          <option value="Armenia">Armenia</option>
                                          <option value="Aruba">Aruba</option>
                                          <option value="Australia">Australia</option>
                                          <option value="Austria">Austria</option>
                                          <option value="Azerbaijan">Azerbaijan </option>
                                          <option value="Bahamas">Bahamas</option>
                                          <option value="Bahrain">Bahrain</option>
                                          <option value="Bangladesh">Bangladesh </option>
                                          <option value="Barbados">Barbados</option>
                                          <option value="Belarus">Belarus</option>
                                          <option value="Belgium">Belgium</option>
                                          <option value="Belize">Belize</option>
                                          <option value="Benin">Benin</option>
                                          <option value="Bermuda">Bermuda</option>
                                          <option value="Bhutan">Bhutan</option>
                                          <option value="Bolivia">Bolivia</option>
                                          <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina</option>
                                          <option value="Botswana">Botswana</option>
                                          <option value="Bouvet Island">Bouvet 
                                            Island </option>
                                          <option value="Brazil">Brazil</option>
                                          <option value="British Indian Ocean Territory"> British Indian Ocean Territory</option>
                                          <option value="Brunei">Brunei</option>
                                          <option value="Bulgaria">Bulgaria</option>
                                          <option value="Burkina Faso">Burkina 
                                            Faso </option>
                                          <option value="Burundi">Burundi</option>
                                          <option value="Cambodia">Cambodia</option>
                                          <option value="Cameroon">Cameroon</option>
                                          <option value="Canada">Canada</option>
                                          <option>Cape Verde</option>
                                          <option value="Cayman Islands">Cayman 
                                            Islands</option>
                                          <option value="Central African Republic"> Central African Republic</option>
                                          <option value="Chad">Chad</option>
                                          <option value="Chile">Chile</option>
                                          <option value="China">China</option>
                                          <option value="China (Hong Kong S.A.R.)"> China (Hong Kong S.A.R.)</option>
                                          <option value="China (Macau S.A.R.)"> China (Macau S.A.R.)</option>
                                          <option value="Christmas Island"> Christmas Island</option>
                                          <option value="Cocos (Keeling) Islands"> Cocos (Keeling) Islands</option>
                                          <option value="Colombia">Colombia</option>
                                          <option value="Comoros">Comoros</option>
                                          <option value="Congo">Congo</option>
                                          <option value="Congo, Democractic Republic of"> Congo, Democractic Republic of</option>
                                          <option value="Cook Islands">Cook 
                                            Islands </option>
                                          <option value="Costa Rica">Costa Rica </option>
                                          <option value="Cote D'Ivoire (Ivory Coast)"> Cote D&#39;Ivoire (Ivory Coast)</option>
                                          <option value="Croatia (Hrvatska)"> Croatia (Hrvatska)</option>
                                          <option value="Cuba">Cuba</option>
                                          <option value="Cyprus">Cyprus</option>
                                          <option value="Czech Republic">Czech 
                                            Republic</option>
                                          <option value="Denmark">Denmark</option>
                                          <option value="Djibouti">Djibouti</option>
                                          <option value="Dominica">Dominica</option>
                                          <option value="Dominican Republic"> Dominican Republic</option>
                                          <option value="East Timor">East Timor </option>
                                          <option value="Ecuador">Ecuador</option>
                                          <option value="Egypt">Egypt</option>
                                          <option value="El Salvador">El 
                                            Salvador </option>
                                          <option value="Equatorial Guinea"> Equatorial Guinea</option>
                                          <option value="Eritrea">Eritrea</option>
                                          <option value="Estonia">Estonia</option>
                                          <option value="Ethiopia">Ethiopia</option>
                                          <option value="Falkland Islands (Malvinas)"> Falkland Islands (Malvinas)</option>
                                          <option value="Faroe Islands">Faroe 
                                            Islands </option>
                                          <option value="Fiji Islands">Fiji 
                                            Islands </option>
                                          <option value="Finland">Finland</option>
                                          <option value="France">France</option>
                                          <option value="French Guiana">French 
                                            Guiana </option>
                                          <option value="French Polynesia"> French Polynesia</option>
                                          <option value="French Southern Territories"> French Southern Territories</option>
                                          <option value="Gabon">Gabon</option>
                                          <option value="Gambia">Gambia</option>
                                          <option value="Georgia">Georgia</option>
                                          <option value="Germany">Germany</option>
                                          <option value="Ghana">Ghana</option>
                                          <option value="Gibraltar">Gibraltar</option>
                                          <option value="Greece">Greece</option>
                                          <option value="Greenland">Greenland</option>
                                          <option value="Grenada">Grenada</option>
                                          <option value="Guadeloupe">Guadeloupe </option>
                                          <option value="Guam">Guam</option>
                                          <option value="Guatemala">Guatemala</option>
                                          <option value="Guinea">Guinea</option>
                                          <option value="Guinea-Bissau"> Guinea-Bissau </option>
                                          <option value="Guyana">Guyana</option>
                                          <option value="Haiti">Haiti</option>
                                          <option value="Heard and McDonald Islands"> Heard and McDonald Islands</option>
                                          <option value="Honduras">Honduras</option>
                                          <option value="Hungary">Hungary</option>
                                          <option value="Iceland">Iceland</option>
                                          <option value="India" selected="selected"> India</option>
                                          <option value="Indonesia">Indonesia</option>
                                          <option value="Iran">Iran</option>
                                          <option value="Iraq">Iraq</option>
                                          <option value="Ireland">Ireland</option>
                                          <option value="Israel">Israel</option>
                                          <option value="Italy">Italy</option>
                                          <option value="Jamaica">Jamaica</option>
                                          <option value="Japan">Japan</option>
                                          <option value="Jordan">Jordan</option>
                                          <option value="Kazakhstan">Kazakhstan </option>
                                          <option value="Kenya">Kenya</option>
                                          <option value="Kiribati">Kiribati</option>
                                          <option value="Korea">Korea</option>
                                          <option value="Korea, North">Korea, 
                                            North </option>
                                          <option value="Kuwait">Kuwait</option>
                                          <option value="Kyrgyzstan">Kyrgyzstan </option>
                                          <option value="Laos">Laos</option>
                                          <option value="Latvia">Latvia</option>
                                          <option value="Lebanon">Lebanon</option>
                                          <option value="Lesotho">Lesotho</option>
                                          <option value="Liberia">Liberia</option>
                                          <option value="Libya">Libya</option>
                                          <option value="Liechtenstein"> Liechtenstein </option>
                                          <option value="Lithuania">Lithuania</option>
                                          <option value="Luxembourg">Luxembourg </option>
                                          <option value="Macedonia">Macedonia</option>
                                          <option value="Madagascar">Madagascar </option>
                                          <option value="Malawi">Malawi</option>
                                          <option value="Malaysia">Malaysia</option>
                                          <option value="Maldives">Maldives</option>
                                          <option value="Mali">Mali</option>
                                          <option value="Malta">Malta</option>
                                          <option value="Marshall Islands"> Marshall Islands</option>
                                          <option value="Martinique">Martinique </option>
                                          <option value="Mauritania">Mauritania </option>
                                          <option value="Mauritius">Mauritius</option>
                                          <option value="Mayotte">Mayotte</option>
                                          <option value="Mexico">Mexico</option>
                                          <option value="Micronesia">Micronesia </option>
                                          <option value="Moldova">Moldova</option>
                                          <option value="Monaco">Monaco</option>
                                          <option value="Mongolia">Mongolia</option>
                                          <option value="Montserrat">Montserrat </option>
                                          <option value="Morocco">Morocco</option>
                                          <option value="Mozambique">Mozambique </option>
                                          <option value="Myanmar">Myanmar</option>
                                          <option value="Namibia">Namibia</option>
                                          <option value="Nauru">Nauru</option>
                                          <option value="Nepal">Nepal</option>
                                          <option value="Netherlands Antilles"> Netherlands Antilles</option>
                                          <option value="Netherlands, The"> Netherlands, The</option>
                                          <option value="New Caledonia">New 
                                            Caledonia </option>
                                          <option value="New Zealand">New 
                                            Zealand </option>
                                          <option value="Nicaragua">Nicaragua</option>
                                          <option value="Niger">Niger</option>
                                          <option value="Nigeria">Nigeria</option>
                                          <option value="Niue">Niue</option>
                                          <option value="Norfolk Island">Norfolk 
                                            Island</option>
                                          <option value="Northern Mariana Islands"> Northern Mariana Islands</option>
                                          <option value="Norway">Norway</option>
                                          <option value="Oman">Oman</option>
                                          <option value="Pakistan">Pakistan</option>
                                          <option value="Palau">Palau</option>
                                          <option value="Panama">Panama</option>
                                          <option value="Papua new Guinea">Papua 
                                            new Guinea</option>
                                          <option value="Paraguay">Paraguay</option>
                                          <option value="Peru">Peru</option>
                                          <option value="Philippines"> Philippines </option>
                                          <option value="Pitcairn Island"> Pitcairn Island</option>
                                          <option value="Poland">Poland</option>
                                          <option value="Portugal">Portugal</option>
                                          <option value="Puerto Rico">Puerto 
                                            Rico </option>
                                          <option value="Qatar">Qatar</option>
                                          <option value="Reunion">Reunion</option>
                                          <option value="Romania">Romania</option>
                                          <option value="Russia">Russia</option>
                                          <option value="Rwanda">Rwanda</option>
                                          <option value="Saint Helena">Saint 
                                            Helena </option>
                                          <option value="Saint Kitts And Nevis"> Saint Kitts And Nevis</option>
                                          <option value="Saint Lucia">Saint 
                                            Lucia </option>
                                          <option value="Saint Pierre and Miquelon"> Saint Pierre and Miquelon</option>
                                          <option value="Saint Vincent And The Grenadin"> Saint Vincent And The Grenadin</option>
                                          <option value="Samoa">Samoa</option>
                                          <option value="San Marino">San Marino </option>
                                          <option value="Sao Tome and Principe"> Sao Tome and Principe</option>
                                          <option value="Saudi Arabia">Saudi 
                                            Arabia </option>
                                          <option value="Senegal">Senegal</option>
                                          <option value="Seychelles">Seychelles </option>
                                          <option value="Sierra Leone">Sierra 
                                            Leone </option>
                                          <option value="Singapore">Singapore</option>
                                          <option value="Slovakia">Slovakia</option>
                                          <option value="Slovenia">Slovenia</option>
                                          <option value="Solomon Islands"> Solomon Islands</option>
                                          <option value="Somalia">Somalia</option>
                                          <option value="South Africa">South 
                                            Africa </option>
                                          <option value="South Georgia">South 
                                            Georgia </option>
                                          <option value="Spain">Spain</option>
                                          <option value="Sri Lanka">Sri Lanka</option>
                                          <option value="Sudan">Sudan</option>
                                          <option value="Suriname">Suriname</option>
                                          <option value="Svalbard And Jan Mayen Islands"> Svalbard And Jan Mayen Islands</option>
                                          <option value="Swaziland">Swaziland</option>
                                          <option value="Sweden">Sweden</option>
                                          <option value="Switzerland"> Switzerland </option>
                                          <option value="Syria">Syria</option>
                                          <option value="Taiwan">Taiwan</option>
                                          <option value="Tajikistan">Tajikistan </option>
                                          <option value="Tanzania">Tanzania</option>
                                          <option value="Thailand">Thailand</option>
                                          <option value="Togo">Togo</option>
                                          <option value="Tokelau">Tokelau</option>
                                          <option value="Tonga">Tonga</option>
                                          <option value="Trinidad And Tobago"> Trinidad And Tobago</option>
                                          <option value="Tunisia">Tunisia</option>
                                          <option value="Turkey">Turkey</option>
                                          <option value="Turkmenistan"> Turkmenistan </option>
                                          <option value="Turks And Caicos Islands"> Turks And Caicos Islands</option>
                                          <option value="Tuvalu">Tuvalu</option>
                                          <option value="Uganda">Uganda</option>
                                          <option value="Ukraine">Ukraine</option>
                                          <option value="United Arab Emirates"> United Arab Emirates</option>
                                          <option value="United Kingdom">United 
                                            Kingdom</option>
                                          <option value="United States">United 
                                            States </option>
                                          <option value="United States Pacific Islands"> United States Pacific Islands</option>
                                          <option value="Uruguay">Uruguay</option>
                                          <option value="Uzbekistan">Uzbekistan </option>
                                          <option value="Vanuatu">Vanuatu</option>
                                          <option value="Vatican City State (Holy See)"> Vatican City State (Holy See)</option>
                                          <option value="Venezuela">Venezuela</option>
                                          <option value="Vietnam">Vietnam</option>
                                          <option value="Virgin Islands (British)"> Virgin Islands (British)</option>
                                          <option value="Virgin Islands (US)"> Virgin Islands (US)</option>
                                          <option value="Wallis And Futuna Islands"> Wallis And Futuna Islands</option>
                                          <option value="Western Sahara">Western 
                                            Sahara</option>
                                          <option value="Yemen">Yemen</option>
                                          <option value="Yugoslavia">Yugoslavia </option>
                                          <option value="Zambia">Zambia</option>
                                          <option value="Zimbabwe">Zimbabwe</option>
                                      </select></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" scope="row" height="10"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">Telephone</td>
                                      <td align="right"><input type="text" name="phone"  class="textfld1"id="phone" /></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" scope="row" height="11"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">Email</td>
                                      <td align="right"><input type="text" name="emailid"  class="textfld1"id="emailid" /></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" scope="row" height="11"></th>
                                    </tr>
                                    <tr>
                                      <td scope="row">Comments</td>
                                      <td align="right"><textarea class="textfld2" name="comments" id="comments" cols="45" rows="5"></textarea></td>
                                    </tr>
                                    <tr>
                                      <th scope="row" height="10"></th>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">&nbsp;</th>
                                      <td align="right"><input type="image" src="images/layout_52.gif" name="Image1" width="86" height="35" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','images/layout_7s7.gif',1)" onmouseout="MM_swapImgRestore()" /></td>
                                    </tr>
                                </table></td>
                              </tr>
                            </table>
                          </form>
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td bgcolor="#CCCCCC"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  </table></td>
                </tr>
              </table></td>
            </tr>
          </table>
            <p><img src="images/callus.gif" width="257" height="82" /></p>
          
          
           <iframe src="//ring.md/widgets/hospital?id=18" width="320" height="520" frameborder="0" scrolling="no"></iframe>
            
          
          
          
          
          
          
          
          </td>
          <td width="20">&nbsp;</td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="6"><img border="0" src="images/con_inner_topleft.gif" width="15" height="15" /></td>
                      <td bgcolor="#FFFFFF"><img border="0" src="images/spacer.gif" width="1" height="1" /></td>
                      <td width="4"><img border="0" src="images/con_inner_topright.gif" width="15" height="15" /></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td bgcolor="#FFFFFF"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="15">&nbsp;</td>
                      

                      <td width="15">&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td bgcolor="#FFFFFF"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td>&nbsp;</td>
                      <td style="border-left: 1px solid #DBDBDB; border-right: 1px solid #DBDBDB;"><table width="100%" border="0" class="text" cellspacing="15" cellpadding="0">
                        <tr>
                            <td><p><img src="images/telemedicine.jpg" width="625" height="162" /></p>
                                      </td>
                          </tr>
                        <tr>
                                <td>
                                    <h2>Telemedicine Clinic (TC)</h2>
                                    <p>Dr Sumer Sethi, pioneer of medical Tele-education and Teleradiology (TeleRad Providers) in India has now taken the initiative to reach remote patients through telemedicine, it has been a pioneering medical education initiative since 1999 and has been pioneer in teaching Doctors for further education both in India and abroad. TeleRad Providers is one of the pioneers in the field of remote diagnosis and caters to remote radiology needs of various hospitals and diagnostic centers across North India. 
</p>
                                    <!--<p>TC (Telemedicine Clinic ) is healthcare wing of DAMS backed on vast knowledge and experience gained by DAMS and TeleRad Providers team.</p>-->
                                    
                                    <h2>About Telemedicine</h2>
                                    <p>Telemedicine is the use of telecommunication and information technologies to provide clinical health care at a distance. It helps eliminate distance barriers and can improve access to medical services that would often not be consistently available in distant rural communities.</p>
                                    <p>Equitable access to healthcare services is a challenge in India due to limited resources especially in rural and tier II cities. Technology has the power to enhance healthcare delivery across the geographic barrier. For developing countries, telemedicine and eHealth can be the only means of healthcare provision in remote areas. For example, the difficult financial situation in many African states and lack of trained health professionals has meant that the majority of the people in sub-Saharan Africa are badly disadvantaged in medical care, and in remote areas with low population density, direct healthcare provision is often very poor</p>
                                    
                                    
                            </td>
                            
                            </tr><tr>
                                <td width="530">
                                        <ul class="idTabs responce-show">
                                            <li><a id="telepsychiatry" class="selected">Telepsychiatry</a></li>
                                            <li><a  id="teleradiology" class="">Teleradiology</a></li>
                                            <li><a  id="teledramatology" class="">Telemedicine</a></li>
                                            <li><a  id="tele-Infertility" class="">Tele-Infertility</a></li>
                                            <li><a  id="faq" class="">Tele-Pad Nephrology</a></li>
                                    </ul>
                                        <div id="hide1" class="content" style="display: block">
                                                <div class="satellite-content tele1">
                                                        <div class="idams-box1">
                                                                <p>
                                                                    Telepsychiatry, another aspect of telemedicine, also utilizes videoconferencing for patients residing in underserved areas to access psychiatric services. It offers wide range of services to the patients and providers, such as consultation between the psychiatrists, educational clinical programs, diagnosis and assessment, medication therapy management, and routine follow-up meetings. 
                                                                    <br>
                                                                        <br>
Telepsychiatry is the application of Telemedicine to the field of Psychiatry. It has been the most successful of all the telemedicine applications so far, because of its need for only a good videoconferencing facility between the patient and the psychiatrist, especially for follow-up.
                                                            </p>
                                                                <div class="pg-medical-main" style="display: block">
                                                                        <div class="pg-heading">
                                                                            <span></span>Our Specialist
                                                                    </div>
                                                                        <div class="course-new-section">
                                                                                <div class="coures-list-box">
                                                                                        <div class="coures-list-box-content">
                                                                                                <div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #c4c4c4;">
                                                                                                        <div class="mrcp_dr_img dr_imgbordr1">
                                                                                                            <img src="images/Sachin.png" />
                                                                                                    </div>
                                                                                                        <div src="images/Sachin.png">
                                                                                                            <h1 class="mrcp_dr_ttl">Dr Sachin Arora</h1>
                                                                                                                <p>
                                                                                                                    <strong>Traning & Experience</strong>
                                                                                                            </p>
                                                                                                                <p style="font-size: 14px;">
                                                                                                                    Trained in neurology and EEG for 1 month at Dept of Neurology, Gauhati medical college.
                                                                                                                    <br>
                                                                                                                    
                                                                                                                    Experienced in administering modified electroconvulsive therapy
                                                                                                                    <br>
                                                                                                                    
                                                                                                                    Teaching experience for undergraduates and interns.
                                                                                                            </p>
                                                                                                                <p>
                                                                                                                    <strong>Additional achievements:</strong>
                                                                                                            </p>
                                                                                                            <p style="font-size: 14px;">
                                                                                                                    In under-graduation, participated in intra-college and inter-college quiz competitions,
                                                                                                                    <br>
                                                                                                                    
                                                                                                                    - 1st prize winner in pathology and microbiology quizzes
                                                                                                                    <br>
                                                                                                                        
                                                                                                                    - Runner up in paediatric (IAP) and surgery(ASI) quizzes
                                                                                                                    <br>
                                                                                                                     - Participated in medical quiz, psychiatry quiz(IPS).
                                                                                                            </p>
                                                                                                    </div>
                                                                                                    
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                        <div id="hide2" class="content" style="display: none">
                                                <div class="satellite-content tele1">
                                                        <div class="idams-box1">
                                                                <p>
                                                                    Teleradiology is the most popular use for telemedicine and accounts for at least 50% of all telemedicine usage. Teleradiology is the transmission of radiological patient images, such as x-rays, CTs, and MRIs, from one location to another for the purposes of sharing studies with other radiologists and physicians.
                                                                    <br>
                                                            </p>
                                                                <aside class="how-to-apply paddin-zero">
                                                                        <div class="how-to-apply-heading telemedicine1">
                                                                             An article published by Dr Sumer Sethi in Internet Journal of Radiology in 2007
My profile and picture
                                                                    </div>
                                                                        <div class="course-detail-content">
                                                                                <p style="padding: 10px 0px ">
                                                                                    <strong>The Internet Journal of Radiology. 2007 Volume 8 Number 1.</strong>
                                                                            </p>
                                                                                <span class="gry-course-box">
                                                                                     In his latest book, The World is Flat, Thomas Friedman describes the unplanned cascade of technological and social shifts that effectively leveled the economic world, and “accidentally made Beijing, Bangalore and Bethesda next-door neighbors.” Friedman's list of “flatteners” includes the fall of the Berlin Wall; the rise of Netscape and the dotcom boom that led to a trillion dollar investment in fiber optic cable; the emergence of common software platforms and open source code enabling global collaboration; and the rise of outsourcing, offshoring, supply chaining and insourcing.
                                                                            </span>
                                                                                <span class="blue-course-box">
                                                                                    According to the book these flatteners converged around the year 2000, and “created a flat world: a global, web-enabled platform for multiple forms of sharing knowledge and work, irrespective of time, distance, geography and increasingly, language.” Thomas Friedman further says “When you start to think of the world as flat, a lot of things make sense in ways they did not before. What the flattening of the world means is that we are now connecting all the knowledge centers on the planet together into a single global network.”
                                                                            </span>
                                                                            <span class="gry-course-box">
                                                                                    One of the most serious, but under-publicized, health care worker shortages is the lack of radiologists, radiology technicians, radiation therapists, and others. With so few radiologists available, an increasing demand for radiology services, and so many specialties to choose from, new radiologists do not want to perform the routine work of reading the x-rays in the middle of the nights and weekends. This forced hospitals to find a solution. 
                                                                            </span>
                                                                                <span class="blue-course-box">
                                                                                    Welcome to a flattened world! Nighthawks - highly educated and credentialed radiologist, many of whom were trained in the U.S. then returned to their homelands to practice medicine half-way around the world, are now reading and interpreting your X-rays, CAT scans and MRI images when you make a late-night visit to an emergency room or the radiologist is off on vacation.
                                                                            </span>
                                                                                <span class="gry-course-box">
                                                                                    Thanks to increasingly powerful Picture Archiving and Communications System (PACS) networks, diagnostic images such as MRIs, CT scans or radiographs can be transferred digitally to major centres that might otherwise be inaccessible to these patients. According to Tim Lougheed in Canadian Medical Association Journal in May 2004 Canada Health is investing tens of millions of dollars in digital radiology equipment at various sites across the country, along with PACS networks to transmit images from place to place. Question arises-Who will read them?
                                                                            </span>
                                                                                <span class="blue-course-box">
                                                                                    Given the defensive medicine being followed, the demands on radiologists to be perfect and accurate are increasing, and with more workload, radiologists are unable to cope with the pressure. There is a problem of long waiting periods in the UK. The newer radiologists coming out of the US, UK, and EU will not be able to meet the growing requirements. Teleradiology or flat world radiology comes to mind as the logical solution to this human logistics nightmare. Then, Global Radiology concept will then enable the flow of radiology expertise from areas of surplus to areas of requirement, worldwide.
                                                                            </span>
                                                                    </div>
                                                                        <div class="idams-box1">
                                                                                <div class="pg-medical-main" style="display:block;margin-bottom: 20px;">
                                                                                        <div class="pg-heading">
                                                                                            <span></span>Our Specialist
                                                                                    </div>
                                                                                        <div class="course-new-section">
                                                                                                <div class="coures-list-box">
                                                                                                        <div class="coures-list-box-content">
                                                                                                                <div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #c4c4c4;">
                                                                                                                        <div class="mrcp_dr_img ">
                                                                                                                            <img src="images/Sumer_Sethi.png" />
                                                                                                                    </div>
                                                                                                                        <div class="mrcp_inr_boxes">
                                                                                                                            <h1 class="mrcp_dr_ttl">Dr Sumer Sethi</h1>
                                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                                    <strong>CEO(TeleRad Providers)</strong>
                                                                                                                            </p>
                                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                                    <strong>Prime Telerad Providers Pvt Ltd </strong>
                                                                                                                            </p>
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </aside>
                                                    </div>
                                            </div>
                                    </div>
                                        <div id="hide3" class="content" style="display: none;">
                                                <div class="satellite-content tele1">
                                                        <div class="idams-box1">
                                                                <p>
                                                                    
                                                                Telemedicine allows dermatology consultations over a distance using audio, visual and data communication, and has been found to improve efficiency. 
                                                                <bR>
                                                                    <br>                                                                
                                                                        </p>
                                                                <div class="pg-medical-main" style="display:block;">
                                                                        <div class="pg-heading">
                                                                            <span></span>Our Specialist
                                                                    </div>
                                                                        <div class="course-new-section">
                                                                                <div class="coures-list-box">
                                                                                        <div class="coures-list-box-content">
                                                                                                <div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #c4c4c4;">
                                                                                                        <div class="mrcp_dr_img">
                                                                                                            <img src="images/Arvind.png" />
                                                                                                    </div>
                                                                                                        <div class="mrcp_inr_boxes">
                                                                                                            <h1 class="mrcp_dr_ttl">Dr Arvind Kumar</h1>
                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>Tele-Diabetology</strong>
                                                                                                            </p>
                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>MEMBERSHIPS</strong>
                                                                                                            </p>
                                                                                                                <p style="font-size:14px;">
                                                                                                                    1.American College of Clinical Endocrinologist(2012)
                                                                                                                    <br></br>
                                                                                                                    2.Research Society for Study of Diabetes in India(lifetime)
                                                                                                                    <br></br>
                                                                                                                    3.Delhi Diabetes Forum(lifetime)
                                                                                                                    <br></br>
                                                                                                                    4.Associations of Physicians of India(lifetime)
                                                                                                                    <br></br>
                                                                                                                    5.Indian Medical Association(lifetime)
                                                                                                            </p>
                                                                                                            <br></br>
                                                                                                                <p style="font-size:14px;">
                                                                                                                     Dr arvind kumar is one the dynamic and highly competent doctor for best treatment of diabetes,thyroid and other hormonal related disorders.He is considered as the doctor with most revisiting patients(after first consultation with dr arvind kumar,patients like to have long term follow up with him only) and
 best diabetologist with independent practice under the age of 40 years.Dr Arvind has received the prestigious “Best Diabetologist in Gurgaon” award by World wide achievers. 
                                                                                                            </p>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                            
                                                    </div>
                                            </div>
                                    </div>
                                    <div id="hide4" class="content" style="display: none;">
                                                <div class="satellite-content tele1">
                                                        <div class="idams-box1">
                                                                <p>
                                                                  <bR>
                                                                    <br>                                                                
                                                                        </p>
                                                                <div class="pg-medical-main" style="display:block;">
                                                                        <div class="pg-heading">
                                                                            <span></span>Our Specialist
                                                                    </div>
                                                                        <div class="course-new-section">
                                                                                <div class="coures-list-box">
                                                                                        <div class="coures-list-box-content">
                                                                                                <div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #c4c4c4;">
                                                                                                        <div class="mrcp_dr_img">
                                                                                                            <img src="images/Dr_deepti.png" />
                                                                                                    </div>
                                                                                                        <div class="mrcp_inr_boxes">
                                                                                                            <h1 class="mrcp_dr_ttl">Dr Deepti Bahl</h1>
                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>Consultant Gynecologist</strong>
                                                                                                            </p>
                                                                                                                <p style="font-size:14px;">
                                                                                                                    Dr Deepti Bahl is a passionate Gynecologist who has done her graduation from leading medical college in Delhi and followed up with her Residency in Obs-Gyne from Lady Hardinge medical college. She has worked in diverse settings after her MS in both leading government and private institutions in India. She has developed significant interest in medical education and is revered by medical graduates all over India for being thorough with latest guidelines and practice. She is a seasoned Obstetrician & Gynaecologist with significant experience in IVF infertility management. Her expertise in infertility management has helped many a infertile couples in Delhi/NCR and now through  Telemedicine Clinic expertise wants to share her honest opinion with infertile couples across the country and help them. 
                                                                                                            </p>
                                                                                                            
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                            
                                                    </div>
                                            </div>
                                    </div>
                                    <div id="hide5" class="content"style="display: none;">
                                                <div class="satellite-content tele1">
                                                        <div class="idams-box1">
                                                                <p>
                                                                  <bR>
                                                                    <br>                                                                
                                                                        </p>
                                                                <div class="pg-medical-main" style="display:block;">
                                                                        <div class="pg-heading">
                                                                            <span></span>Our Specialist
                                                                    </div>
                                                                        <div class="course-new-section">
                                                                                <div class="coures-list-box">
                                                                                        <div class="coures-list-box-content">
                                                                                                <div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #c4c4c4;">
                                                                                                        <div class="mrcp_dr_img">
                                                                                                            <img src="images/Siddharth_Sethi.png" />
                                                                                                    </div>
                                                                                                        <div class="mrcp_inr_boxes">
                                                                                                            <h1 class="mrcp_dr_ttl">Dr Siddharth Sethi</h1>
                                                                                                                <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>Pediatric Nephrology</strong>
                                                                                                            </p>
                                                                                                            <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>Indian Society of Pediatric Nephrology</strong>
                                                                                                            </p>
                                                                                                            <p class="mrcp_dr_sbtl">
                                                                                                                    <strong>Expertise:</strong>
                                                                                                            </p>
                                                                                                                <p style="font-size: 14px">
                                                                                                                    1.Pediatric Nephrology
                                                                                                                    <br></br>
                                                                                                                    2. Renal Replacement Therapy including renal transplantation
                                                                                                            </p>
                                                                                                            <br></br>
                                                                                                                <p style="font-size:14px;">
                                                                                                                    He was trained as a Fellow (International Pediatric Nephrology Association Fellowship (2007-2008)) and Senior Resident in Pediatric Nephrology at All India Institute of Medical Sciences and Division of Pediatric Nephrology and Transplant Immunology, Cedars Sinai Medical Centre, Los Angeles, California(2008-2009). He has been actively involved in the care of children with all kinds of complex renal disorders, including nephrotic syndrome, tubular disorders, urinary tract infections, hypertension, chronic kidney disease, and renal transplantation. </p>
                                                                                                            
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                            
                                                    </div>
                                            </div>
                                    </div>
                                </td>
                        </tr>
                      </table>
                      
                      </td>
                      
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15"><img border="0" src="images/spacer.gif" width="1" height="1" /></td>
                      <td ><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td width="6"><img border="0" src="images/con_inner_bottleft.gif" width="15" height="15" /></td>
                          <td background="images/innerbox_bott.gif" bgcolor="#FFFFFF"><img border="0" src="images/spacer.gif" width="1" height="1" /></td>
                          <td width="4"><img border="0" src="images/con_inner_bottright.gif" width="15" height="15" /></td>
                        </tr>
                      </table></td>
                      <td width="15"><img border="0" src="images/spacer.gif" width="1" height="1" /></td>
                    </tr>
                  </table></td>
                </tr>
<!--                              <div class="pg-heading">
                                       Why DAMS Telemedicine Clinic
                              </div>  <div class="pg-medical-main tab-hide" style="display: block">
                                  <div class="pg-heading">
                                       Why DAMS Telemedicine Clinic
                              </div>    -->
                      
                           <!--</div>-->
                      
                     
                <tr>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                  <td></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<div class="accordion">
                          <h2 background="images/bg_head.gif">Why Telemedicine Clinic</h2>
			<h1>Advantages of Telemedicine</h1>
                        <div>
                        <div class="coures-list-box-content" id="di1" style="display: block;">
                                                          <ul class="course-new-list">
                                                                  <li style="    padding: 0px 0px 10px 0px">
                                                                      <span class="sub-arrow"></span>
                                                                      <p style="padding-left: 10px;font-size: 14px;">Minimizes patient travel for medical opinion</p>
                                                              </li>
                                                              <li style="    padding: 0px 0px 10px 0px">
                                                                      <span class="sub-arrow"></span>
                                                                      <p style="padding-left: 10px;font-size: 14px;">Decreased relocation of medical specialists</p>
                                                              </li>
                                                              <li style="    padding: 0px 0px 10px 0px">
                                                                      <span class="sub-arrow"></span>
                                                                      <p style="padding-left: 10px;font-size: 14px;">Enhanced quality of care</p>
                                                              </li>
                                                      </ul>
                                                          <p style="font-size:14px;padding: 0px 10px;">
                                                              Also we have the unique differential from all other telemedicine providers in India, most of which are hospital based teams who have vested interest in their opinion. Telemedicine Clinic offers the only unique opinion based service where the doctors don’t have a vested interest to advise a surgery or a procedure for you.  We give “unbiased opinion” and we feel that we are only one of its kind service in India.
                                                              <br></br>
                                                              <br></br>
                                                              
  Second advantage is “Generic drug” prescription. Our team wherever possible advises only generic drugs and we want to make sure that the health care cost overall is reduced for the end user.
                                                          </p>
                                              </div>
                        </div>
			<h1>Why we use Telemedicne Services</h1>
                        <div>
                        <div class="coures-list-box-content" id="di1" style="display: block;">
                                                          <p style="font-size:14px;padding: 0px 10px;">
                                                          Support people in understanding their condition and acquiring the skills and habits of self management needed to achieve the best outcomes in terms of their health. This is particularly true about our Diabetes consult and chronic dermatological diseases. Psychological consults are also easily done in privacy of your home and you get to interact with specialist  on  a one to one basis. Infertility is one of the issues where one more opinion with the specialist with no “vested interest” would be help. That’s  why TC has chosen these services in the 1st phase.   
                                                          <br></br>
                                                          <br></br>
                                                           Minimise the waste of time and resource consumed by face to face interaction where the patient would prefer support to be provided remotely and where this support can be done effectively.
                                                           <br></br>
                                                           <br></br>
                                                            Telemedicine Clinic utilizes a mix of internet and satellite technology and video-conferencing equipment to conduct a real-time consultation between medical specialist and patients.
                                                          </p>
                                                      <br></br>
                                                          <p style="font-size:14px; padding: 0px 10px;">
                                                              <strong>GLOBALLY PROVEN</strong>
                                                              <br></br>
                                                              Telemedicine operates with no limitations worldwide. Thousands of consultations are occurring right now, between remotely located doctors and patients -everywhere.
                                                          </p>
                                                      <br></br>
                                                          <p style="font-size:14px; padding: 0px 10px;">
                                                              <strong>TECHNOLOGY</strong>
                                                              <br></br>
                                                              We leverage the latest technology to deliver affordable, accessible and quality healthcare that transcends geography. 
                                                              <br></br>
                                                                  We utilize Secure, private HIPAA compliant video chat and messaging for individuals or groups
                                                                <br></br>
                                                              The ability to schedule and view appointment availability in real-time
                                                              <br></br>
                                                                  Ability to share medical information with patients to improve knowledge of their health status using video or messaging
                                                                  <br></br>
                                                                  
Telemedicine allows patients and providers to maximize their daily routine by saving them travel time and wait time in crowded waiting rooms. The convenience of telemedicine allows for a variety of medical and mental health services to be addressed while patients and providers spend less time waiting, and more time improving well-being and treatment. 
<br></br>
                                                          </p>
                                                          <ul class="course-new-list">
                                                                  <li style="    padding: 0px 0px 10px 0px">
                                                                      <span class="sub-arrow"></span>
                                                                      <p style="padding-left: 10px;font-size: 14px;">Our practitioners are all highly esteemed professionals with several years of experience to offer</p>
                                                              </li>
                                                              <li style="    padding: 0px 0px 10px 0px">
                                                                  <span class="sub-arrow"></span>
                                                                  <p style="padding-left: 10px;font-size: 14px;">
                                                                      If you choose telemedicine, you enjoy the convenience of having your appointment at home or at a nearby telemedicine clinic</p></li>
                                                      </ul>
                                              </div>
                        </div>
			<h1>FAQ'S</h1>
                        <div>
			<div class="coures-list-box-content " id="di3">    
 <div class="satellite-content tele1" style="border: 0px solid #ccc;">
<div class="idams-box1">
<p class="fqa_qstyle"><b>Q.1</b>How do I pay for this service?</p>
<p class="fqa_ansstyle"><b>Ans.</b>The payment can be made before the consultation using credit cards, debit cards or netbanking. </p>
<p class="fqa_qstyle"><b>Q.2</b>How much time does it take to get the appointment with the doctor?</p>
<p class="fqa_ansstyle"><b>Ans.</b>The appointment is given based on doctor's availability but usually appointments are available within 1-2 days</p>
</div>
</div>
      </div>
		</div>
                      </div>
  <div id="contener5">
    <div id="contener5_txt1">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" valign="top" class="box-bott2"><font color="#FFFFFF"><b> <a href="#">Home</a><br />
            </b></font><font color="#FFFFFF"><b> <a href="press.html">Press &amp; Event</a><br />
             <a href="publications.html"> Publications</a></b></font><br />
            <font color="#FFFFFF"><b><a href="news.html">News</a></b></font><br />
            <font color="#FFFFFF"><b><a href="why_teleradiology.html">Why Teleradiology</a><br />
              <a href="rural_mission.html">Our Rural Mission</a></b></font></td>
          <td valign="top" class="box-bott"><strong><font color="#FFFFFF"> <a href="about_us.html">About Us</a></font></strong></td>
          <td class="box-bott" valign="top"><font color="#FFFFFF"><b> <a href="radiology_services.html">Services</a></b></font></td>
          <td class="box-bott" valign="top"><font color="#FFFFFF"><b> <a href="download.html"> Download</a></b></font></td>
          <td rowspan="2" valign="top" class="box-bott"><font color="#FFFFFF"><b> <a href="contactus.html">Contact Us</a></b></font><br />
              <font color="#FFFFFF"><b> <a href="quote.html">Request a Quote</a></b></font><br />
              <font color="#FFFFFF"><b> <a href="testimonial.html">Testimonial</a></b></font><br/>
               <font color="#FFFFFF"><b> <a href="sitemap.html">Site map</a></b></font>              </td>
        </tr>
        <tr class="box-bott">
          <td class="box-bott" style="width: 190px" height="101" valign="top"><font face="Tahoma">•&nbsp; </font>About Teleradioligy Providers<br />
              <font face="Tahoma">•&nbsp; </font>About Chairman<br />
              <font face="Tahoma">•&nbsp; </font>About Team<br />
              <font face="Tahoma">•&nbsp; </font>Mission &amp; Vision<br />
              <br />
            &nbsp;</td>
          <td class="box-bott" style="width: 170px" height="101" valign="top">•&nbsp; Radiology Reporting<br />
            •&nbsp; 3D Reconstruction<br />
          •&nbsp; Rad Learning Programme</td>
          <td class="box-bott" valign="top" style="width: 170px">
       •&nbsp; Sample Reports<br />
•&nbsp; Brochure<br /></td>
        </tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </div>
    <div id="contener5_txt2">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" align="center">© Teleradiology Providers. 
            All Rights Reserved (Terms of Use)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="https://www.gingerwebs.com">Website design</a> by Ginger Webs</td>
        </tr>
      </table>
    </div>
    <div id="contener5_txt2"> <img border="0" src="images/txt_findus.gif" width="58" height="13" /> &nbsp;&nbsp; <a href="https://www.linkedin.com/companies/prime-telerad-providers-pvt-ltd-teleradiology-providers-"><img src="images/linked.gif" width="61" height="19" border="0" /></a>&nbsp; <a href="https://www.facebook.com/#!/pages/Teleradiology-Providers/124283817603993"> <img border="0" src="images/butt_acebook.gif" width="67" height="19" /></a>&nbsp; <a href="https://www.twitter.com/sumersethi"> <img border="0" src="images/butt_twitter.gif" width="63" height="20" /></a></div>
  </div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12539835-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>

<script language="javascript">
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}


function check_null()
{
if(document.frmform.name.value=="")
	{
		alert("Please Enter your Name")
		document.frmform.name.focus()
		return false
	}
	
		if(document.frmform.hospital.value=="")
	{
		alert("Please Enter your Hospital")
		document.frmform.hospital.focus()
		return false
	}
			if(document.frmform.address.value=="")
	{
		alert("Please Enter your Address")
		document.frmform.address.focus()
		return false
	}
	
	
	if(document.frmform.city.value=="")
	{
		alert("Please Enter your City")
		document.frmform.city.focus()
		return false
	}

	if((document.frmform.phone.value==null)||(document.frmform.phone.value==""))
	{
		alert("Please Enter your Phone Number")
		document.frmform.phone.focus()
		return false
	}
	
	if(isInteger(document.frmform.phone.value)==false)
	{
		alert("Please Enter a Valid Phone Number")
		document.frmform.phone.value=""
		document.frmform.phone.focus()
		return false
	}

	if(document.layers||document.getElementById||document.all)
	{
		var str=document.frmform.emailid.value
		var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		if (filter.test(str)==false)
		{
			alert("Please input a valid email address!")
			document.frmform.emailid.value=""
			document.frmform.emailid.focus()
			return false
		}
	
	}

}
</script>
<script>
			$(".accordion").accordion();
		</script>
<script type="text/javascript">
 $(document).ready(function(){
                $("#telepsychiatry").click(function(){
                    $("#hide1").css("display","block");
                    $("#hide2").css("display","none");
                    $("#hide3").css("display","none");
                    $("#hide4").css("display","none");
                    $("#hide5").css("display","none");
                    $("#telepsychiatry").addClass("selected");
                    $("#teleradiology").removeClass("selected");
                    $("#teledramatology").removeClass("selected");
                    $("#tele-Infertility").removeClass("selected");
                    $("#faq").removeClass("selected");
                    
                });
                
                $("#teleradiology").click(function(){
                    $("#hide1").css("display","none");
                    $("#hide2").css("display","block");
                    $("#hide3").css("display","none");
                    $("#hide4").css("display","none");
                    $("#hide5").css("display","none");
                    $("#telepsychiatry").removeClass("selected");
                    $("#teleradiology").addClass("selected");
                    $("#teledramatology").removeClass("selected");
                    $("#tele-Infertility").removeClass("selected");
                    $("#faq").removeClass("selected");
                });
                
                $("#teledramatology").click(function(){
                    $("#hide1").css("display","none");
                    $("#hide2").css("display","none");
                    $("#hide3").css("display","block");
                    $("#hide4").css("display","none");
                    $("#hide5").css("display","none");
                    $("#telepsychiatry").removeClass("selected");
                    $("#teleradiology").removeClass("selected");
                    $("#teledramatology").addClass("selected");
                    $("#tele-Infertility").removeClass("selected");
                    $("#faq").removeClass("selected");
                    
                });
                
                $("#tele-Infertility").click(function(){
                    $("#hide1").css("display","none");
                    $("#hide2").css("display","none");
                    $("#hide3").css("display","none");
                    $("#hide4").css("display","block");
                    $("#hide5").css("display","none");
                    $("#telepsychiatry").removeClass("selected");
                    $("#teleradiology").removeClass("selected");
                    $("#teledramatology").removeClass("selected");
                    $("#tele-Infertility").addClass("selected");
                    $("#faq").removeClass("selected");
                });
                
                $("#faq").click(function(){
                    $("#hide1").css("display","none");
                    $("#hide2").css("display","none");
                    $("#hide3").css("display","none");
                    $("#hide4").css("display","none");
                    $("#hide5").css("display","block");
                    $("#telepsychiatry").removeClass("selected");
                    $("#teleradiology").removeClass("selected");
                    $("#teledramatology").removeClass("selected");
                    $("#tele-Infertility").removeClass("selected");
                    $("#faq").addClass("selected");
                });
            });         
</script>