<?php

include 'openconnection.php'; 
include 'config/constant.php';

?>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/datetimepicker_css.js"></script>
<script>function Submit_form_otsReg() {
    var quickEnquiry1=$("#studentname").val();
    var quickEnquiry2=$("#email").val();
    var quickEnquiry3=$("#phone").val();
    var quickEnquiry4=$("#dob").val();
    var quickEnquiry5=$("#mbbsscore").val();
    var quickEnquiry6=$("#rightque").val();
    var quickEnquiry7=$("#wrongque").val();
    var quickEnquiry8=$("#leque").val();
     var otsoffline = $("#otsoffline").val();
                 if ($.trim($("#studentname").val()).length == 0) {
                        $('#studentname').addClass('error_select1');
                        $("#studentname").val("");
                        $('#studentname').attr('placeholder', "Please Enter Your Name" + " ");
                        $("#studentname").focus();
                        return false;
                    }
                    
                      else if ($.trim($("#email").val()) == "") {
                        $('#email').addClass('error_select1');
                        $('#email').val("");
                        $('#email').attr('placeholder', "Please Enter Your Email Address");
                        $("#email").focus();
                        return false;
                    }
                    else if (isEMail($.trim($("#email").val()))) {
                        $('#email').addClass('error_select1');
                        $('#email').val("");
                        $('#email').attr('placeholder', "Please Enter Your Valid Email Address");
                        $("#email").focus();
                        return false;

                    }    else if ($.trim($("#phone").val()) == "") {
                        $('#phone').addClass('error_select1');
                        $('#phone').attr('placeholder', "Please Enter Your Phone No");
                        $("#phone").focus();
                        return false;
                    } else if ((IsNumeric($.trim($("#phone").val())) == false || $.trim($("#phone").val()).length < 10)) {
                        $('#phone').addClass('error_select1');
                        $('#phone').val("");
                        $('#phone').attr('placeholder', "Please Enter Valid Phone No" );
                        $("#phone").focus();
                        return false;
                    }
                    else   if ($.trim($("#dob").val()).length == 0) {
                        $('#dob').addClass('error_select1');
                        $("#dob").val("");
                        $('#dob').attr('placeholder', "Please Enter Your DOB" + " ");
                        $("#dob").focus();
                        return false;
                    }
//                    else if ((IsNumeric($.trim($("#dob").val())) == false || $.trim($("#dob").val()).length < 8)) {
//                        $('#dob').addClass('error_select1');
//                        $('#dob').val("");
//                        $('#dob').attr('placeholder', "Please Enter Valid DOB" );
//                        $("#dob").focus();
//                        return false;
//                    }
                    else   if ($.trim($("#state").val()).length == 0) {
                        $('#state').addClass('error_select1');
                        $("#state").val("");
                        $('#state').attr('placeholder', "Please Enter Your State" + " ");
                        $("#state").focus();
                        return false;
                    }
                    else if ($.trim($("#mbbsscore").val()).length == 0) {
                        $('#mbbsscore').addClass('error_select1');
                        $("#mbbsscore").val("");
                        $('#mbbsscore').attr('placeholder', "Please Enter Your Score" + " ");
                        $("#mbbsscore").focus();
                        return false;
                    }
                    else if ($.trim($("#rightque").val()).length == 0) {
                        $('#rightque').addClass('error_select1');
                        $("#rightque").val("");
                        $('#rightque').attr('placeholder', "Please Enter Your Right Question" + " ");
                        $("#rightque").focus();
                        return false;
                    }
                       else   if ($.trim($("#wrongque").val()).length == 0) {
                        $('#wrongque').addClass('error_select1');
                        $("#wrongque").val("");
                        $('#wrongque').attr('placeholder', "Please Enter Your Wrong Question" + " ");
                        $("#wrongque").focus();
                        return false;
                    }
                    else if ($.trim($("#leque").val()).length == 0) {
                        $('#leque').addClass('error_select1');
                        $("#leque").val("");
                        $('#leque').attr('placeholder', "Please Enter Your Left Question" + " ");
                        $("#leque").focus();
                        return false;
                    }
                    document.OTSRegistrationForm.submit();

}
function isEMail(s)
{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var booleanValue = emailPattern.test(s);
    if (!booleanValue)
    {
        return true;
    } else {
        return false;
    }
}
function IsNumeric(sText)

{
    var ValidChars = "0123456789";
    var IsNumber = true;
    var Char;


    for (i = 0; i < sText.length && IsNumber == true; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            IsNumber = false;
        }
    }
    return IsNumber;

}
function numbervalidation1(event)
{
   
    if(($("#phone").val()).length<10 || event.which==8 ||event.which==0){
    if((event.which>47 && event.which<=57)|| event.which==8 ||event.which==0 ){return true ;}else{return false;}}else{return false;}
    
}
function numbervalidation2(event)
{
   
    if(($("#rightque").val()).length<3 || event.which==8 ||event.which==0){
    if((event.which>47 && event.which<=57)|| event.which==8 ||event.which==0 ){return true ;}else{return false;}}else{return false;}
    
}
function numbervalidation3(event)
{
   
    if(($("#wrongque").val()).length<3 || event.which==8 ||event.which==0){
    if((event.which>47 && event.which<=57)|| event.which==8 ||event.which==0 ){return true ;}else{return false;}}else{return false;}
    
}
function numbervalidation4(event)
{
   
    if(($("#leque").val()).length<3 || event.which==8 ||event.which==0){
    if((event.which>47 && event.which<=57)|| event.which==8 ||event.which==0 ){return true ;}else{return false;}}else{return false;}
    
}
function isNumberDecimalKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
//  alert(charCode);
 if(charCode==46)
{
    if(($("#mbbsscore").val()).length!=2){return false};
}else{
    if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
    { return false;}else if(($("#mbbsscore").val()).length==2 && (charCode!=46  && charCode!=8 && charCode!=9)){return false;}
    else{return true;}
}
}

</script>


<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
      
	<title>PROJECTED RANK</title>
        <style>
            *{
                margin:0;
                padding: 0;
            }
            body{font-family: arial;font-size: 14px;}
            .form-wrapper{background-image:url(images/banner-bg.jpg);position: fixed;background-repeat: no-repeat;background-size: cover;background-attachment: fixed;width: 100%;top: 0;bottom: 0;padding: 0 0 10px;}
            .form-shade-wrapper{
                padding:0 0 20px;
            }
            .header-wrapper{
                padding: 1% 15px 20px;
                text-align: center;
            }
            .header-wrapper h3{
                color: #fff;
                font-size: 30px;
                margin: 10px 0 0 0;
            }
            .header-wrapper p{
                display: inline-block;
                padding: 0px 10px;
                background: #fff;
                border-radius: 5px;
            }
            .header-wrapper img{
                width:250px;
                margin: 5px 0;
            }
            .input-container-wrapper {margin: 0 0 8px;}
            .form-content-wrapper {
                width: 680px;
                margin: auto;
                background: #fff none repeat scroll 0 0;
                border-radius: 6px;
            }
            .form-content{padding: 15px 15px 22px;}
            .form-content h3{color:#666;margin:0px 10px 10px 10px;font-size:20px;line-height: 30px;text-align: center;}
            .personal-detail-wrapper{margin: 0 0 5px;}
            .width50{width: 50%;float: left;box-sizing: border-box;}
            .width33{width:33.3%;float: left;box-sizing: border-box;}
            .personal-detail-wrapper h5{padding: 6px 0px; color: #a5b5c5; font-size: 14px;border-bottom: 1px solid #e3e8ef; margin: 0 10px 15px;}
            .input-control-wrapper{margin: 0 0 10px;padding: 0 10px;font-size: 12px;}
            .input-control-wrapper label{
                margin: 5px 0 5px 0;
                display: block;
            }
            .input-control-wrapper input , .input-control-wrapper select{
                border: 1px solid #e3e8ef;
                padding: 5px 10px;
                width: 100%;
                box-sizing: border-box;
                color: #a5b5c5;
                font-size: 11px;
                height: 36px;
                border-radius: 3px;
            }
            .input-control-wrapper select{
                color:#a5b5c5;
            }
            .input-control-wrapper input:focus , .input-control-wrapper select:focus{
                border-color: #a5b5c5;
            }
            input.calender-img{
                background-image: url(images/calender.png);background-repeat: no-repeat;background-position: center right;
               
            }
            .submit-btn-wrapper{text-align: right;margin: 5px 10px 0;}
            .submit-btn-wrapper input{
                border: none;
                background: #f15a22;
                padding: 10px 20px;
                color: #fff;
                border-radius: 5px;
                font-size: 14px;
                transition: all 0.2s ease;
                -webkit-transition: all 0.2s ease;
                cursor: pointer;
            }
            .submit-btn-wrapper input::placeholder{color:#a5b5c5;}
            
            ::placeholder {
                color:#a5b5c5;/* Firefox */
            }

            :-ms-input-placeholder { /* Internet Explorer 10-11 */
               color:#a5b5c5;
            }

            ::-ms-input-placeholder { /* Microsoft Edge */
               color:#a5b5c5;
            }
            
            .submit-btn-wrapper input:hover{
                box-shadow: 0 0 0 2px rgba(241,90,34, 0.5);
            }
            .projected-rank-details{
                padding: 20px 20px 0;
            }
            .projected-rank-details ul{
                padding: 0;
                margin: 0;
            }
            .projected-rank-details ul li{
                font-size: 12px;
                line-height: 16px;
                margin: 10px 0;
            }

            @media screen and (max-width:550px){

                .width50 {
                    width: 100%;
                    float: none;
                }
                .width33 {
                    width: 100%;
                    float: none;
                }
                .form-wrapper {
                    position: static;
                }
                .form-shade-wrapper {
                    padding: 0 20px 20px;
                }
                .form-content-wrapper {
                    position: static;
                    width: auto;
                    height: auto;
                }
                .header-wrapper {
                    padding: 3% 15px 20px;
                }
            }
        </style>
    </head>

    <body>
        <div class="form-wrapper"> 
                <div class="header-wrapper">
                    <p><img src="images/logoots.jpg"></p>
                </div>
            <div class="form-content-wrapper" id="otsformshow">
                    <div class="container">
                        <form enctype="multipart/form-data" name="OTSRegistrationForm" id="OTSRegistrationForm" method="post" action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=submit" >
                        <div class="form-content">
                            <div class="input-container-wrapper">
                                <h3>Projected Rank for NEET PG</h3>
                                <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> Name:</label>
                                    <input type="text" name="studentname" id="studentname"class="input-control" placeholder="Enter Name">
                                    <div style="clear:both;"></div>
                                </div>
                                 <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> Email:</label>
                                    <input type="email" name="email" id="email" class="input-control" placeholder="Enter Email">
                                    <div style="clear:both;"></div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="input-container-wrapper">
                                <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> Phone no:</label>
                                    <input type="number" onKeypress="return numbervalidation1(event);"  name="phone" id="phone" class="input-control" placeholder="Enter Phone">
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> DOB</label>
                                    <input class="full_input calender-img" id="dob" type="text" name="dob" class="date-textfield date-position" onclick="NewCssCal('dob','ddmmyyyy','arrow')" readonly="true"  placeholder="Enter Date Of Birth"></input>
                                    <div style="clear:both;"></div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="input-container-wrapper">
                                <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> State(Domicile):</label>
                                    <input type="text" name="state" id="state" class="input-control" placeholder="Enter State">
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="input-control-wrapper width50">
                                    <label style="margin-top: 0;"><span>*</span> MBBS Percentage:</label>
                                    <input id="mbbsscore"  name="mbbsscore" maxlength="5" placeholder="00.00" onkeypress="return isNumberDecimalKey(event)" type="text" class="input-control" placeholder="Total score">
                                    <div style="clear:both;"></div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            

                            <div class="personal-detail-wrapper">
                                <div class="input-container-wrapper ">
                                    <div class="input-control-wrapper width33">
                                        <label style="margin-top: 0;"><span>*</span> Approx No. of Right questions :</label>
                                        <input type="number" onKeypress="return numbervalidation2(event);" name="rightque" id="rightque" class="input-control" placeholder="Right questions">
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div class="input-control-wrapper width33">
                                        <label style="margin-top: 0;"><span>*</span> Approx No. of wrong questions:</label>
                                        <input type="number" onKeypress="return numbervalidation3(event);" name="wrongque" id="wrongque" class="input-control" placeholder="wrong questions">
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div class="input-control-wrapper width33">
                                        <label style="margin-top: 0;"><span>*</span> Approx No. of left questions:</label>
                                        <input type="number" onKeypress="return numbervalidation4(event);" name="leque" id="leque" class="input-control" placeholder="left questions">
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>

                            
                            <div class="submit-btn-wrapper">
                                <input type="hidden" id="otsoffline" name="otsoffline" value="1" />
                               <input type="button" value="Submit" title="Submit" onclick="Submit_form_otsReg();"/>
                               
                            </div>
                            <div class="projected-rank-details">
                                <ul>
                                    <li>Input Your Anticipated Score (Correct and Wrong) <br/> &
Our software will tell you where you stand based on your submitted Data.</li>
                                    <li>(Data submitted upto 11th January 2018 will be considered for evaluation).</li>
                                    <li>Disclaimer: This All India Rank is just based on data submitted and DAMS doesn't take any responsibility for the same.</li>
                                
                                </ul>
                            </div>
                        </div></form>
                    </div>
                </div>
<!--                   <div class="form-content-wrapper"  id="otsformhide">
                    <div class="container">
                        <div class="form-content">
                            <div class="input-container-wrapper">
                                <h3>Projected Rank</h3>
                    </div></div>
                    </div></div>-->
                
                
                
                
                
            </div>
        </div>
    </body>
</html>