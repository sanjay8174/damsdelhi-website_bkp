<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, USMLE EDGE</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner"> 
      
      <!--<div class="big-nav">
<ul>
<li class="face-face active"><a href="usmle-edge.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="#" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="#" title="Achievement">Achievement</a></li>
</ul>
</div>-->
      
      <aside class="banner-left banner-left-postion">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usmle-edge.php" title="USMLE EDGE">USMLE EDGE</a></li>
          <li><a title="USMLE Edge Step 2" class="active-link">USMLE Edge Step 2</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>USMLE EDGE Step-2
              <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
            </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>Step-II :-</span>
                  <p>USMLE Step 2 is also a one-day Computer-based Test about clinical science. It assesses whether the aspirant can apply the knowledge and understanding of clinical science considered essential for the provision of patient care under supervision, including emphasis on health promotion and disease prevention. It stresses clinical topics and skills, such as diagnosis, prognosis, and preventive measures. It includes approximately 400 questions given in 9 hours of testing time. The testing time is divided into eight 60-minute blocks with approximately 50 questions in each block. The test items aren't grouped by clinical subject but are presented in a random, interdisciplinary sequence.</p>
                </div>
                <ul class="dnb-list">
                  <h5>There are six subjects tested on the exams. :-</h5>
                  <li><span>&nbsp;</span>Internal Medicine.</li>
                  <li><span>&nbsp;</span>Obstetrics and Gynaecology.</li>
                  <li><span>&nbsp;</span>Paediatrics.</li>
                  <li><span>&nbsp;</span>Preventive Medicine &amp; Public Health.</li>
                  <li><span>&nbsp;</span>Psychiatry.</li>
                  <li><span>&nbsp;</span>Surgery.</li>
                </ul>
                <div class="franchisee-box"> <span>Normal Conditions and disease categories.</span>
                  <p>10-15% Normal Growth and Development, General Principles of Care.</p>
                  <p>85-90% Individual Organ Systems or Types of Disorders.</p>
                </div>
                <div class="franchisee-box"> <span>Physicians Task</span>
                  <p>15-20% Promoting Health and Health Maintenance.</p>
                  <p>25-40% Understanding Disease Mechanisms.</p>
                  <p>25-40% Establishing a Diagnosis</p>
                  <p>10-20% Applying Principles of Management.</p>
                </div>
                <div class="franchisee-box"> <span>QUESTIONS FORMAT :-</span>
                  <p>USMLE questions are arranged by format. There are three multiple-choice formats used: single best answer, matching, and a relatively new format called "pick N." For all formats, there are 3 to 26 answer choices (for the one best answer format, there are usually five answer choices). Most items describe a patient and ask you to identify the underlying mechanism of the disease, propose a diagnosis, order diagnostic studies, or initiate treatment. There are few questions that incorporate graphic, tabular, and pictorial material.</p>
                </div>
                <ul class="dnb-list">
                  <h5>DAMS PACKAGES FOR USMLE</h5>
                  <p>(CLINICAL KNOWLEDGE)</p>
                  <li><span>&nbsp;</span>ONLINE TEST SERIES.</li>
                  <li><span>&nbsp;</span>10 BLOCKS.</li>
                  <li><span>&nbsp;</span>2 MONTHS.</li>
                  <li><span>&nbsp;</span>1 HR PER PAPER.</li>
                </ul>
              </div>
            </article>
            <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
            <!--<div class="buy-now-course-btn"><a href="cart.php" title="Book Your Seat"> <span class="b-cart-basket">&nbsp;</span> Book Your Seat</a></div>--> 
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'dams-usmle-edge.php'; ?>
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });	

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script> 
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>