<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <aside class="banner-right">
        <div class="banner-right-btns"> 
         <!--<a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>--> 
         <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> 
         <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> 
         <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> 
        </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="DAMS Sky" class="active-link">DAMS Sky</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading">
            <h4>DAMS-SKY  Best teachers at your doorstep </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="idams-box"> <span>Number 1 Technology being offered by DAMS-SKY classes</span>
                  <p>Studio transmissions are at high bandwidth rates of approx 1000 Kbps. This leads to Superior audio and video quality. DAMS-SKY system is two-way interactive for voice, video and data, wherein several classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them. Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty. Through the Smart Assessment System Student's attendance, MCQ-response and text messages reach the Faculty instantly.</p>
                </div>
                <div class="idams-box"> <span>Features</span>
                  <ul class="franchisee-list">
                    <li><span>&nbsp;</span>DAMS -Sky leads to Superior audio and video quality.</li>
                    <li><span>&nbsp;</span>DAMS-SKY system is two-way interactive for voice, video and data.</li>
                    <li><span>&nbsp;</span>DAMS Sky classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them.</li>
                    <li><span>&nbsp;</span>Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty.</li>
                  </ul>
                </div>
                <div class="idams-box"> <span>Why DAMS Sky Satellite Classes Not Internet Based Classes</span>
                  <p>Internet Video classes use the public internet which is not consistent because of variable internet speed and thus results in streaming and buffering causing loss of sync between voice and picture; whereas DAMS satellite classes use a private internet where transmission is at a consistent high bandwidth rate, thus leading to superior quality video &amp; audio. Satellite classes are more reliable than internet videoconferencing as it uses lesser terrestrial infrastructure. </p>
                </div>
                <div class="idams-box"> <span>Interaction</span>
                  <ul class="franchisee-list">
                    <li><span>&nbsp;</span>Common ways to interact is talking over MIC with faculty, he can hear you and answer you back. This interaction could be heard in all centres.</li>
                    <li><span>&nbsp;</span>Text Message through our real time collaboration system which reaches the faculty instantly.</li>
                  </ul>
                </div>
                <div class="idams-box"> <span>Doubt Solving</span>
                  <ul class="franchisee-list">
                    <li><span>&nbsp;</span>Normally class would have a doubt clearing section, this could be in the middle or at the end of class.</li>
                    <li><span>&nbsp;</span>You would also be provided with the membership to DAMS Exclusive club on Facebook which is famous for connecting students with faculty where you can put the queries and interact.</li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div class="enquiry-main">
            <div class="enquiry-heading">Enquiry Form</div>
            <div class="enquiry-content-main">
              <div class="enquiry-content">
                <div class="enquiry-content-more">
                  <form action="" method="get">
                    <input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
                    <input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
                    <input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />
                    <select name="" class="select-input">
                      <option value="0">Select Course</option>
                      <option>MD/MS Entrance</option>
                      <option>MCI Screening</option>
                      <option>MDS Quest</option>
                      <option>USMLE Edge</option>
                    </select>
                    <select name="" class="select-input">
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                    </select>
                    <textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
                    <div class="submit-enquiry"><a href="#" title="Submit"><span></span> Submit</a></div>
                  </form>
                </div>
              </div>
              <div class="enquiry-bottom"></div>
            </div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>