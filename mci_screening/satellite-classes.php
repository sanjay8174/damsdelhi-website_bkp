<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</head>

<body class="inner-bg">
<?php error_reporting(0);
include 'registration.php';
$course_id = 1;
$courseNav_id = 2;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="satellite-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>India's First Satellite<br>Based PG Medical Classes</h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Satellite Classes" class="active-link">Satellite Classes</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Satellite Classes</h4>
            <article class="showme-main">
              <ul class="idTabs">
                <li><a href="#satellite">Satellite Classes</a></li>
                <li><a href="#concept">Concept</a></li>
                <li><a href="#technology">Technology</a></li>
                <li><a href="#faq">FAQ</a></li>
              </ul>
              <div id="satellite">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>India's First Satellite Based PG Medical Classes</span>
                    <p>Ancient times Guru Shishya Pranali of imparting education was by the Guru at Guru's premises /Ashrams. Ages passed, social pattern changed, villages turned to towns, towns to cities &amp; metros. Civilization progressed with "Education". For good education, students have to migrate to better places &amp; this is the history as of yesterday. Now technology development removed these constraints and barriers. The Best Education of metros is available in House anywhere including the remote area. Quality education by teaching from the renowned professors and faculty is available at recipient's facilities. Best education at environment of individual's choice.</p>
                    <p>Medical Tele-Education is the Brain Child of Dr Sumer Sethi, our Director, widely known for his vision and innovations in field of PG Medical Education vertical. He is also one of the pioneers in field of Teleradiology &amp; Tablet based learning programme called as iDAMS. </p>
                  </div>
                  <div class="satellite-video">
                    <iframe width="100%" height="336" src="//www.youtube.com/embed/EAuRgGnUgJU" class="border_none"></iframe>
                  </div>
                </div>
              </div>
              <div id="concept">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>Concept of Satellite Classes</span>
                    <p>One acute problem faced by the coaching class industry, is paucity of competent faculty. This constraint imposes limitations on expansion beyond geographical boundaries. Satellite Learning Programme addresses this problem effectively without losing quality of coaching. An out of box Learning Experience; Satellite Learning Programme has two way audio video facilities consisting of three major elements:</p>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Teaching End.</li>
                      <li><span>&nbsp;</span>Remote Learning Centers called "Classrooms".</li>
                      <li><span>&nbsp;</span>The Satellite.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="technology">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>Number 1 Technology being offered by DAMS-SKY classes</span>
                    <p>Studio transmissions are at high bandwidth rates of approx 1000 Kbps. This leads to Superior audio and video quality. DAMS-SKY system is two-way interactive for voice, video and data, wherein several classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them. Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty. Through the Smart Assessment System Student's attendance, MCQ-response and text messages reach the Faculty instantly.</p>
                  </div>
                  <div class="idams-box1"> <span>Features</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>DAMS -Sky leads to Superior audio and video quality.</li>
                      <li><span>&nbsp;</span>DAMS-SKY system is two-way interactive for voice, video and data.</li>
                      <li><span>&nbsp;</span>DAMS Sky classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them.</li>
                      <li><span>&nbsp;</span>Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty.</li>
                    </ul>
                  </div>
                  <div class="idams-box1"> <span>Why DAMS Sky Satellite Classes Not Internet Based Classes</span>
                    <p>Internet Video classes use the public internet which is not consistent because of variable internet speed and thus results in streaming and buffering causing loss of sync between voice and picture; whereas DAMS satellite classes use a private internet where transmission is at a consistent high bandwidth rate, thus leading to superior quality video &amp; audio. Satellite classes are more reliable than internet videoconferencing as it uses lesser terrestrial infrastructure. </p>
                  </div>
                  <div class="idams-box1"> <span>Interaction</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Common ways to interact is talking over MIC with faculty, he can hear you and answer you back. This interaction could be heard in all centres.</li>
                      <li><span>&nbsp;</span>Text Message through our real time collaboration system which reaches the faculty instantly.</li>
                    </ul>
                  </div>
                  <div class="idams-box1"> <span>Doubt Solving</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Normally class would have a doubt clearing section, this could be in the middle or at the end of class.</li>
                      <li><span>&nbsp;</span>You would also be provided with the membership to DAMS Exclusive club on Facebook which is famous for connecting students with faculty where you can put the queries and interact.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="satellite-content" id="faq">
                  <div class="schedule-mini-series t-spacebig"> <span class="mini-heading">DAMS –SKY Centers are Located</span>
                    <table class="sat-tabl">
       <tr class="inner-sat-heading">
            <td>&nbsp;&nbsp;</td>
            <td>Face to Face Classes</td>
            <td>DAMS-SKY Classes</td>
       </tr>
       <tr>
           <table class="inner-sat-tabl">
                  <tr>
                      <td align="center">1</td>
                      <td width="45%">I am a Girl, travel to a big city is always risky and time consuming.</td>
                      <td width="45%">I am close of my residence. It is my city. I am comfortable here. I also know all the students and make friends with them.</td>
                  </tr>
                  <tr>
                      <td align="center">2</td>
                      <td width="45%">I have to come down from my hometown to Delhi to get the coaching from best faculty.</td>
                      <td width="45%">I attend the lectures from same/Better faculty at a center near to my house in my hometown.</td>
                  </tr>
                  <tr>
                      <td align="center">3</td>
                      <td width="45%">My coaching fees are costly in Metro.</td>
                      <td width="45%">My coaching fees are lesser compared to Metro fees.</td>
                  </tr>
                  <tr>
                      <td align="center">4</td>
                      <td width="45%">It is difficult and uncertain to get admission in the class as the seats are limited.</td>
                      <td width="45%">Centers are equipped with multiple classrooms, with Flexibility of expansion. I am assured of a seat.</td>
                  </tr>
                  <tr>
                      <td align="center">5</td>
                      <td width="45%">If I am absent in any of the classes, it becomes difficult to attend it.</td>
                      <td width="45%">If I miss the class for any reason, I get access to the same class from in rebroadcast.</td>
                  </tr>
                  <tr>
                      <td align="center">6</td>
                      <td width="45%">I spend approx 10,000/- per month at the Hostel in metros. The food quality is poor and rooms are cramped. The environment is also not good.</td>
                      <td width="45%">I have the flexibility of home food, home environment. I am able to study and prepare better, which increase my chances of succeeding at exams. I am happy to be at Home, safe and secure. I save over Rs. 10,000 per month with DAMS-SKY Classes. Money saved is money earned. I feel my parent's hard-earned money is well-spent with DAMS-SKY Classes.</td>
                  </tr>
           </table>
       </tr>
                    </table>
                  <p>DAMS Delhi have comes with new featues and better Interface.</p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
         <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry -->
        </aside>
      </section>
    </div>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

</body>
</html>