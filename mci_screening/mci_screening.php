<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = '2';
$courseNav_id = 5;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
$getNavContent=$Dao->getCourseNavContent($courseNav_id);
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <?php echo $getNavContent[0][0]; ?>
<!--        <p><br>
          <strong> 88% PASS RATE IN MCI SCREENING MARCH 2013<br>
          79% PASS RATE IN MCI SCREENING SEPTEMBER 2012<br>
          </strong> </p>
        <br>
        <p>DAMS has arrived in style in this segment.</p>
        <p> MCI screening passing rates were generally believed to be low, before we started exploring this side. Our focused efforts have consistently given high percentage selection in MCI screening. Poor quality coaching academies functional in this segment are largely believed to be the reason for students not passing this exam. Also this year the MCI screening exam showed a variance in trend with more trickier MCQs and PGME like questions which small tuition house are generally not able to cater. DAMS being a consortium of more than 100 post graduate specialist teachers has the knack of predicting MCQS &amp; helping FMG’s achieve their dreams. Moreover, we have even helped many foreign graduates to become PG in various subjects in India, unlike others who are actually a dead end for foreign medical graduates.<br>
          <br>
          So, our request to FMGE ASPIRANT is choose the leaders in medical education, who are in pg medical exam segment for last 15 years and have presence across India &amp; we promise you will pass the exam in the FIRST ATTEMPT WITH US. Premier institute for PG medical entrance in India, rated by students as number one consortium of educationists with branches all over India since 2000, spearheaded by Sumer Sethi, MD Radiologist and topper in various PG medical entrances now offers unique courses for MCI screening, high yielding and affordable as well. Registered privated limited firm having served more than 50,000 Indian medical graduates now offers its unique courses for FMGs as well. Easly reachable and well located in Gautam Nagar.<br>
          <br>
          Why BEAT ABOUT THE BUSH when we know exactly what is important for you?<br>
          Why waste money when you can get a 5month course with the best in the business?<br>
          Have you tried all other so far and yet results have not been there?<br>
          Do you know most of these so called institutes are not even run by Doctors?<br>
          Clearing MCI is still a dead end if you don’t get through PG special combo packages for PG as well. For PG Medical entrance this year MCI has started new national level exam called as AIPG(NBE/NEET) Pattern which DAMS is the pioneer for providing insight into it. Secure your future with these experienced professionals.<br>
          <br>
          <strong>COURSE DETAILS</strong><br>
          <strong>MCI Regular Course</strong><br>
          <strong>4 Months Programme</strong><br>
          &raquo;   Study Material<br>
          &raquo;   Full day sessions<br>
          &raquo;   Class Tests<br>
          &raquo;   &ldquo; High yielding Golden points &rdquo; after each discussion<br>
          &raquo;   Revision Test Series<br>
        </p>
        <p>So come and join DAMS to fulfill your dreams by preparing for FMGs entrance exams.</p>-->
      </div>
        <?php include 'middle-accordion.php'; ?>

 <?php include 'recentAchievement.php'; ?>
    </aside>
    <aside class="content-right">
    <?php include 'right-accordion.php'; ?>
    <?php include 'newsRight.php'; ?>
    <?php include 'studentInterview.php'; ?>

    </aside>
  </div>
</section>
<!-- Midle Content End Here -->

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>