<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'header.php'; ?>


<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">

<div class="big-nav">
<ul>
<li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="#" title="Satellite Classes">Satellite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
</ul>
</div>

<aside class="banner-left">
<h2>Be smart &amp;<br>
 take your future in Your Hand </h2>
<h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
</aside>

<?php include 'md-ms-banner-btn.php'; ?>

</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li style="background:none;"><a title="Combo Test Series - 2014" class="active-link">Combo Test Series - 2014</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>Combo Test Series - 2014</h4>
<article class="showme-main">

<script type="text/javascript" src="https://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
<link type="text/css" href="https://www.sunsean.com/idTabs/main.css">

<ul class="idTabs"> 
<li><a href="#test1">Test Series - I</a></li>
<li><a href="#test2">Test Series - II</a></li>
<li><a href="#test3">Test Series - III</a></li>
<li><a href="#test4">Test Series - IV</a></li>
</ul>

<div id="test1">
<div class="test-tab-content">
<div class="test-combo-content">
<ul>
<li><label>9<sup>th </sup>Feb, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>16<sup>th </sup>Feb, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>23<sup>rd </sup>Feb, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>2<sup>nd </sup>Mar, 2014</label> <span>Biostatistics</span></li>
<li><label>9<sup>th </sup>Mar, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>16<sup>th </sup>Mar, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>23<sup>rd </sup>Mar, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>30<sup>th </sup>Mar, 2014</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>Medicine-I</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Oncology</span></li>
</ul>
</div>
</div></div>

<div id="test2">
<div class="test-tab-content">
<div class="test-combo-content">
<ul>
<li><label>9<sup>th </sup>Mar, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>16<sup>th </sup>Mar, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>23<sup>rd </sup>Mar, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>30<sup>th </sup>Mar, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>Medicine-I</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Oncology</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
</ul>
</div>
</div>

</div>

<div id="test3">
<div class="test-tab-content">
<div class="test-combo-content">
<ul>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>Medicine-I</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology, Biochemistry, Molecular Biology, Forensic</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Medicine and Toxicology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Oncology</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>31<sup>st </sup>Aug, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>7<sup>th </sup>Sep, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>14<sup>th </sup>Sep, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
<li><label>21<sup>st </sup>Sep, 2014</label> <span>Grand Test + Online Test</span></li>
</ul>
</div></div>

</div>

<div id="test4">
<div class="test-tab-content">
<div class="test-combo-content">
<ul>
<li><label>18<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology, Biochemistry, Molecular Biology, Forensic</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Medicine and Toxicology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Oncology</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>31<sup>st </sup>Aug, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>7<sup>th </sup>Sep, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>14<sup>th </sup>Sep, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
<li><label>21<sup>st </sup>Sep, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>28<sup>th </sup>Sep, 2014</label> <span>Opthalmology</span></li>
<li><label>5<sup>th </sup>Oct, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>12<sup>th </sup>Oct, 2014</label> <span>Medicine-I</span></li>
<li><label>19<sup>th </sup>Oct, 2014</label> <span>Grand Test + Online Test</span></li>
<li><label>26<sup>th </sup>Oct, 2014</label> <span>Medicine-II</span></li>
</ul>
</div></div>

</div> 


</article>
</div>

</aside>

<aside class="gallery-right">

<div id="accor-wrapper">
<div class="accordionButton"><span></span>Interns/Post Intern students</div>
<div class="accordionContent" style="display: block;">
<div class="inner-accor">
<ul>
<li>Classroom Courses</li>
<ol>
<li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course (weekend)</a></li>
<li><a href="t&d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
<li><a href="crash_course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
<li><a href="combo-test-series.php" title="Combo Test Series"><span class="sub-arrow"></span> Combo Test Series</a></li>
<li><a href="mini-nlt-series.php" title="Mini Nlt Series"><span class="sub-arrow"></span> Mini Nlt Series</a></li>
</ol>

<li>Distant Learning Programme</li>
<ol>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
</ol>

<li>Test Series</li>
<li>Online</li>
<ol>
<li><a href="online-registration.php" title="Online Registration">Online Registration</a></li>
</ol>

<li>Offline</li>
</ul>
</div>
</div>
<div class="accordionButton"><span></span>Prefinal/Final Year Students</div>
<div class="accordionContent" style="display: none;">
<div class="inner-accor">
<ul>
<li>Classroom Courses</li>
<ol>
<li><a href="foundation_course.php" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
<li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol>

<li>2nd Professional  students</li>
<li>Classroom Courses</li>
<ol>
<li><a href="prefoundation_course.php" title="Prefoundation course"><span class="sub-arrow"></span> Prefoundation course</a></li>
</ol>
</ul>
</div>
</div>

<div class="accordionButton"><span></span>Distant Learning Programme</div>
<div class="accordionContent" style="display: none;">
<div class="inner-accor">
<ul>
<li>Tablet Based Course - IDAMS</li>
<li>Test Series</li>
<li>1st Professional Students</li>
<li>Classroom Courses</li>
<ol>
<li><a href="the_first_step.php" title="First Step course"><span class="sub-arrow"></span> First Step course</a></li>
<li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol>

</ul>
</div>
</div>
</div>







<div class="enquiry-main">
<div class="enquiry-heading">Enquiry Form</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<form action="" method="get">
<input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
<input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
<input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />



<select name="" class="select-input">
<option value="0">Select Course</option>
<option value="0">Select Course</option>
<option>MD/MS Entrance</option>
<option>MCI Screening</option>
<option>MDS Quest</option>
<option>USMLE Edge</option>
</select>


<select name="" class="select-input">
<option>Centre Interested</option>
<option>Centre Interested</option>
<option>Centre Interested</option>
</select>

<textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
<div class="submit-enquiry"><a href="#" title="Submit"><span></span>  Submit</a></div>
</form>
</div>

</div>
<div class="enquiry-bottom"></div>
</div>

</div>

</aside>

</section>

</div>
 
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->

</body>
</html>