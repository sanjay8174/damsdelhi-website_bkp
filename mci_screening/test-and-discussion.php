<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 2;
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Regular Course" class="active-link">Test and Discussion</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>TEST AND DISCUSSION<span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main paddin-zero">
              <aside class="about-content">
                  <div class="dams-data-nw">
                  <!-- <b>TEST AND DISCUSSION </b>-->
                     <p> &raquo; Not sure how to go about your preparation ?</p>
                      <p> &raquo;  Want to develop a knack of solving multiple choice questions ?</p>
                     <p> &raquo;  Want to solve questions in a requisite time limit ?</p>
                     <p>&nbsp;&nbsp;&nbsp;LEARN THE ART OF SOLVING QUESTIONS 
                       JOIN US
                       23rd July </p>
                     
                     <b>COURSE FEATURES</b>
                     <p> &raquo; Subject wise Test and Discussion</p>
                     <p> &raquo; Updated version of Questions prepared by our experts</p>
                     <p> &raquo; Detailed discussion for choosing appropriate answer</p>
                     <p> &raquo; Time management for solving questions</p>
                     <p> &raquo; separate tests for clinical and non-clinical subjects</p>
                     <p> &raquo; 2 mock test</p>
					  <br>
                     <p>Brace yourselves to crack MCI Screening Exam with the Best MCI and PG Preparation Academy in the Country --- Delhi Academy of Medical Sciences. For more Information Contact us : ( 8588874949 + drtp@damsdelhi.com )</p>
                <div class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></div>
                  </div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>