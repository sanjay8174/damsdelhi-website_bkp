<?php 
ob_start();
function verifyChecksum($MerchantId , $OrderId, $Amount, $AuthDesc, $WorkingKey,  $CheckSum)
	{
		$str = "";
		$str = "$MerchantId|$OrderId|$Amount|$AuthDesc|$WorkingKey";
		$adler = 1;
		$adler = adler32($adler,$str);
		if($adler==$CheckSum) return true;
		else return false;
	}

	function getChecksum($MerchantId, $OrderId, $Amount, $redirectUrl, $WorkingKey)  {
		$str = "$MerchantId|$OrderId|$Amount|$redirectUrl|$WorkingKey";
		$adler = 1;
		$adler = adler32($adler,$str);
		return $adler;
	}

	function adler32($adler , $str)
	{
		$BASE =  65521 ;

		$s1 = $adler & 0xffff ;
		$s2 = ($adler >> 16) & 0xffff;
		for($i = 0 ; $i < strlen($str) ; $i++)
		{
			$s1 = ($s1 + Ord($str[$i])) % $BASE ;
			$s2 = ($s2 + $s1) % $BASE ;
		}
		return leftshift($s2 , 16) + $s1;
	}

	function leftshift($str , $num)
	{

		$str = DecBin($str);

		for( $i = 0 ; $i < (64 - strlen($str)) ; $i++)
			$str = "0".$str ;

		for($i = 0 ; $i < $num ; $i++)
		{
			$str = $str."0";
			$str = substr($str , 1 ) ;
			//echo "str : $str <BR>";
		}
		return cdec($str) ;
	}

	function cdec($num)
	{
		$dec=0;
		for ($n = 0 ; $n < strlen($num) ; $n++)
		{
		   $temp = $num[$n] ;
		   $dec =  $dec + $temp*pow(2 , strlen($num) - $n - 1);
		}

		return $dec;
	}

include('openconnection.php');

$timezone = "Asia/Calcutta";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

function getCCavenueStatus($orderId){
     $str = file_get_contents("https://www.ccavenue.com/servlet/new_txn.OrderStatusTracker?Merchant_Id=M_acc27085_27085&Order_Id=".$orderId);
     if(strpos($str, "&AuthDesc=")!==false){
                $startPoint = strpos($str, "&AuthDesc=");
                $findValue = substr($str, $startPoint+10,1);
                return $findValue;
     } else {
                return "NotAvailable";
     }
}

function getCenterName($centerId){
    if($centerId == ""){
        return "NA";
    } else {
        if(strpos($centerId,"#")!==false){
            $centerDetail = explode("#",$centerId);

            if(!isset($centerDetail[0]) && !isset($centerDetail[1])){
                return "NA";
            }

            if($centerDetail[0]=="c"){
                $query = "select name from center where id='$centerDetail[1]' limit 1";
            } else if($centerDetail[0]=="tc"){
                $query = "select name from testcenter where id='$centerDetail[1]' limit 1";
            }

            $result = @mysql_query($query) or die("Could not get Center Name");
            if(mysql_num_rows($result)>0){
                $row = mysql_fetch_array($result);
                return $row["name"];
            } else {
                return "NA";
            }
        } else {
            return "NA";
        }

    }
}

function updateStatusForOrderID($orderID, $status){
    $dateTime = date("Y-m-d H:i:s");
    $query = "UPDATE STUDENT_PAYMENT SET STATUS='".$status."' WHERE ORDER_ID='".$orderID."'";
    $result = @mysql_query($query) or die("Could not Insert Data");
}

function getDataOfOrderId($orderID){
    $query = "SELECT * FROM STUDENT_PAYMENT AS SP where SP.ORDER_ID='".$orderID."'";
    $result = @mysql_query($query) or die("Could not get data");
    return $result;
}

function getCorseNameAmount($centerId){
    if($centerId == ""){
            $row["NAME"] = "NA";
            $row["AMOUNT"] = "NA";
            return $row;
    } else {
        $query = "select NAME,AMOUNT from COURSE where ID='$centerId' limit 1";
        $result = @mysql_query($query) or die("Could not get Center Name");
        if(mysql_num_rows($result)>0){
            $row = mysql_fetch_array($result);
            return $row;
        } else {
            $row["NAME"] = "NA";
            $row["AMOUNT"] = "NA";
            return $row;
        }
    }
}
?>
<?php   
        include "../mail/mailApiService.php";  
        $maildao    = new MailAPIService();
        if (isset($_REQUEST["encResp"]) && isset($_REQUEST["orderNo"])) {
        
        include('Crypto.php');
        $workingKey     = '82FFD3370D439136BD7CF62D2AD911A3';   //Working Key should be provided here.
        $encResponse    = $_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $MerchantId     = $_REQUEST["merchant_id"];
	$OrderId        = $_REQUEST["orderNo"];
	$Amount         = $_REQUEST["amount"];
	$AuthDesc       = $_REQUEST["AuthDesc"];
        $avnChecksum    = $_REQUEST["Checksum"];
        $error="false";
        $Checksum = verifyChecksum($MerchantId, $OrderId, $Amount, $AuthDesc, $workingKey, $avnChecksum);
        
        $rcvdString = decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	

        $order_status   = "";
	$decryptValues  = explode('&', $rcvdString);
	$dataSize       = sizeof($decryptValues);

        
        for($i = 0; $i < $dataSize; $i++) 
	{
		$information = explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
	}

                if($order_status==="Failure"){
                    $message="Sorry! Your Transaction is not successful.";
                    $mailMessage = "Transaction is not successful";
                    $status = "N";

                 } else if($order_status==="Success"){
                    $message="Thank you for Buy Course. Your credit card has been charged and your transaction is successful.";
                    $mailMessage = "Transaction successful";
                    $status = "Y";

                 } else if($order_status==="Aborted"){
                    $mailMessage="Your transaction is Aborted.";
                    $error="true";
                   
                 } else{
                    $message="Security Error! Illegal access detected.";
                    $error = "true";
	
		}
             
   } else{
       
        $message="Security Error! Illegal access detected.";
        $error = "true";
}

if($error==="false" && isset($status)){
    updateStatusForOrderID($OrderId, $status);

    /*------------Send Message-----------------------*/

    $result = getDataOfOrderId($OrderId);
    if($row = mysql_fetch_array($result)){
        $ids=$row['PRODUCT_IDS'];
	$idarr=  explode(',', $ids);
	
	$bookName='';
	for($i=0;$i<count($idarr);$i++){
          if($idarr[$i]!='' && $idarr[$i]!='0'){ 
		
		$query=mysql_query("select * from PRODUCT WHERE PRODUCT_ID =$idarr[$i]");
            while($rows=mysql_fetch_object($query)) {                
                   if($bookName==''){
				$bookName=urldecode($rows->PRODUCT_NAME);}else{
				$bookName.=",".urldecode($rows->PRODUCT_NAME);}   

                }}}
	$name           = $row["STUDENT_NAME"];
        $email          = $row["EMAIL"];
        $mobile         = $row["MOBILE"];
        $address        = $row["ADDRESS"];
        $college        = $row["COLLEGE"];
        $Amount         = $row["AMOUNT"];
        $Country        = $row["COUNTRY"];
        $State          = $row["STATE"];
        $City           = $row["CITY"];
        $Zip            = $row["ZIPCODE"];
        $dateTimeMail   = date("F j, Y g:i a");
    }


    if($status == "Y"){
        $message="Thank you for Buy Book:-<b>$CourseName</b>. Your credit card has been charged and <b>your transaction is successful</b>.";
    }
    
    $subject="Order Id: $OrderId,Online Transaction Status for Publication Books is submitted by ".$email;
    $message2="<html xmlns='https://www.w3.org/1999/xhtml'>
        <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Buy Course</title>

        </head>

        <body>
                <table border=0 cellpadding=9 cellspacing=9 style=border-collapse: collapse bordercolor=#111111 width=100% id=AutoNumber1>

        <tr>
        <td><font size=2 face=verdana>
        Hello,<br><br>
        Mr./Ms. $name has completed transaction process for buy Course by CCavenue.<br><br>
        Information about the visitor and the status of transaction process:<br>
        Order Id:           $OrderId <br>
        Name:               $name<br>
        E-mail Id:          $email<br>
        Mobile Number:      $mobile<br>
        Address:            $address<br>
        College/University: $college<br>
        Country:            $Country<br>
        State:              $State<br>
        City:               $City<br>
        Zip Code:           $Zip<br>
        Books:              $bookName<br>
        Total Amount:       $Amount INR<br>
        Time:               $dateTimeMail <br>
        Status: <b>         $mailMessage</b> <br><br><br>

        </font></td>
        </tr>
                                                </table>
                </body>
        </html>";

        $mailTextData = str_replace("\n.", "\n..", $message2);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        

        $emailArray = array("kapil@damsdelhi.com","info@damsdelhi.com","chandan@damsdelhi.com");
        
        for($j=0; $j<count($emailArray); $j++){
        $msgId = $maildao->sendMail_AWS($emailArray[$j], $mailTextData,$subject);
         
     }

}
header("Location:thanks.php?a=$order_status");
?>
