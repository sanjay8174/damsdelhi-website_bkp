<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="keywords" content="DAMS Mission &amp; Vision" />
<meta name="description" content="The mission &amp; Vision of DAMS student who joins us benefits to an extent that he comes out of the PG medical entrance" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php';
$path = constant::$path;
?>
<?php $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('<?php echo $path."/images/background_images/".$getAboutusContent[0][0];  ?>') right top no-repeat;width:100%;float:left;height:318px;">
<aside class="banner-left banner-left-postion">
<?php  echo $getAboutusContent[0][1]; ?>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
<li><a title="Mission &amp; Vision" class="active-link">Mission &amp; Vision</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<article class="showme-main">
<div class="idams-content">
<div class="idams-box">
<?php echo $getAboutusContent[0][2]; ?>
</div>
<div class="idams-right-video">
<iframe width="100%" height="300" src="//www.youtube.com/embed/3dS96gyIBWU" class="boder-none"></iframe>
</div></div></article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>