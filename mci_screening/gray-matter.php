<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, Concept of grey matter, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php error_reporting(0);
include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="national.php" title="National Quiz">National Quiz</a></li>
          <li><a title="Concept of Grey Matter" class="active-link">Concept of Grey Matter</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Concept of Grey Matter</h4>
            <article class="showme-main">
              <aside class="course-icons"> <img src="images/national.jpg" title="National Quiz" alt="National Quiz" /> </aside>
              <div class="idams-box full-space"> <span>ROUND – 2</span><br />
                <span>INTRODUCTION:</span><br />
                <span>Second Round – "Zonal Round":</span>
                <p>This is a written round and it will decide the team from each zone. This zonal round will be conducted at the nearest DAMS regional office in every zone.</p>
                <p>The results would be compiled for every zone i.e. East, West, North, and South.</p>
                <p>One team from every zone would finally represent their respective college in the Grand Finale, Finale, which would be held in NEW DELHI. Results
                  Finally!! the results of the zonal round of GREY MATTER - DAMS NATIONAL QUIZ have started coming. Teams in all the zones are having a neck to neck fight to go to the finals, but only one fortunate team would make it to finals from each zone. Most worthy medical brains are competing to get into the top 4. Battles are getting over but the war is still on. Right now teams from North Zone and East Zone have emerged. Teams from South and West zones would follow soon.</p>
              </div>
              <div class="idams-box"> <span>NORTH ZONE FINALISTS: </span>
                <p>Team from ALL INDIA INSTITUTE OF MEDICAL SCIENCES (AIIMS), NEW DELHI.</p>
                <p><b>Devanshu Bansal , Devika Kir</b></p>
              </div>
              <div class="idams-box"> <span>EAST ZONE FINALISTS:</span>
                <p>Team from VEER SURENDRA SAI MEDICAL COLLEGE &amp; HOSPITAL, SAMBALPUR, ODISHA.</p>
                <p><b>Deepamjyoti Nanda Nalinkanta Ghosh</b></p>
              </div>
              <div class="idams-box"> <span>SOUTH ZONE FINALISTS:</span>
                <p>Team from KASTURBA MEDICAL COLLEGE,</p>
                <p><b>Manipal , Himanshu , Isha Gambhir</b></p>
              </div>
              <div class="idams-box"> <span>WEST ZONE FINALISTS:</span>
                <p>Team from M.P. SHAH MEDICAL COLLEGE, JAMNAGAR.</p>
                <p><b>Ankit Madam , Shubham Dilipbhai Maheshwari</b></p>
              </div>
              <div class="achievment-videos-section">
                <ul>
                  <li>
                    <div class="video-box-1">
                      <iframe width="100%" height="247" src="//www.youtube.com/embed/947yqPU8uHI?wmode=transparent" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                    <div class="video-box-1">
                      <iframe width="100%" height="247" src="//www.youtube.com/embed/qwReQql-VVE?wmode=transparent" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'national-quiz-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script> 
</body>
</html>