<?php 
header('Location: https://www.damspublications.com/');exit;?>
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Book Store</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<!--<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script> -->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>
<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-store-banner">
      <?php include 'mci-big-nav.php';?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li class="bg_none"><a href="mci-screening.php" title="MCI Screening">MCI Screening</a></li>
          <!--<li><a href="mci-dams-store.php" title="DAMS Store">DAMS Store</a></li>-->
          <li><a href="http://damspublications.com/" target="_blank"  title="DAMS Store">DAMS Store</a></li>
          <li><a title="Publications" class="active-link">Publications</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Publications</h4>
            <article class="showme-main">
              <div class="main-books">
                <ul class="publication-book">
                  <li>
                    <div class="publication-box"> <img src="images/book1.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book2.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="publication-box"> <img src="images/book3.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book4.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="publication-box"> <img src="images/book5.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="publication-box right"> <img src="images/book6.jpg" title="Book" alt="Book" />
                      <div class="content">
                        <p>PG Medical Entrance Examination (May - 2013)</p>
                        <div class="price-list"> <span>Price: 150</span>
                          <div class="add-cart"><a href="mci-add-to-cart.php" title="Add to cart">Add to cart</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>MCI Screening DAMS Store</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li class="border_none"><a href="mci-idams.php" title="iDAMS Tablet">iDAMS Tablet</a></li>
                  <!--<li><a href="mci-dams-store-publication.php" title="DAMS Publications" class="active-store">DAMS Publications</a></li>-->
                  <li><a href="http://damspublications.com/" target="_blank"  title="DAMS Publications" class="active-store">DAMS Publications</a></li>
                  <li><a href="mci-online-test-series.php" title="Online Test Series">Online Test Series</a></li>
                  <li><a href="mci-dams-test-series.php" title="DAMS Test Series">DAMS Test Series</a></li>
                  <li><a href="../mci-i-dams.php" title="Mobile Applications">Mobile Applications</a></li>
                </ul>
              </div>
            </div>
          </div>
          <?php include 'mci-buynow-right-section.php';?>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php';?>

<!-- Footer Css End Here --> 
<script type="text/javascript">
	function showPopup(){
		$('#backPopup').show();
		$('#frontPopup').show();
	}
	function hidePopup(){
		$('#backPopup').hide();
		$('#frontPopup').hide();
	}
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });


//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
	

	
</script> 
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css"> 
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>