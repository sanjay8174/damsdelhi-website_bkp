<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 3;
$courseNav_id = 9;
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mds-quest-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MDS Quest Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest ">MDS Quest </a></li>
          <li><a title="Postal Course" class="active-link">Postal Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Postal Course <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <div class="test-series-content paddin-zero">
                  <ul class="duration-content">
                    <li><label>Price :</label><span class="price_font_block">15500/- <span class="including_tax_inline">(including s.tax)</span></span></li>
                  </ul>
                </div>
                <p>If you can not come to DAMS, our courses will come to you. Join our Postal Series Delhi Academy of Medical Sciences (DAMS) provides postal courses. The study material is developed by DAMS, which has been working since more than one decade. The study material is compact &amp; effective which is neither bulky nor vague. Instead it is easy to understand and has unique presentation of all the subject as per requirements of examination. DAMS team has worked hard to provide error free text and smart &amp; shortcut methods to solve problems.</p>
                <div class="franchisee-box paddin-zero">
                  <p class="paddin-zero"><span class="price_font_block">Dental Career Counselling for MDS :</span> 09999158131, 09999322163</p>
                </div>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span> About the exam :-</div>
                <p>AIPG (MDS) is conducted by AIIMS every yuear in January.  This paper encompasses MCQ's of the objective nature. Questions of the graduate level shall be posed in this paper. The time period of this examination is based on the many departments. Aspirants who have a clear knowledge about the pattern and syllabus of the MBBS course will be able to perform admirably in this exam.</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Course Highlights :-</div>
                <ul class="benefits">
                  <li><span></span>The study material includes full coverage of syllabus of PG medical entrance examination including high yielding text as well as cutting edge question banks.</li>
                  <li><span></span>The study material includes subject wise sequentially presented theory sections and practice sets which include solved questions with answers and explanations.</li>
                  <li><span></span>The previous feedback of the students is highly appreciable who have succeded in various examinations.</li>
                  <li><span></span>Study material is time to time updated and latest updation and current developments are sent time to time. </li>
                </ul>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>HOW TO APPLY ?</div>
                <p>Download Application form &amp; enclose Demand Draft (note that cheques are not accepted) of concerned fee as mentioned in fee structure (including postal charges).</p>
                <p>Duly filled Application form alongwith demand draft to be made in favour of "Delhi Academy of Medical Sciences Pvt. Ltd.", payable at New Delhi  should be sent by registered post / courier to :</p>
                <p>Delhi Academy of Medical Sciences
                  Grovers Chamber
                  4B, 3rd Floor, Pusa Road, 
                  New Delhi-110005</p>
                <p>Now courses are avaliable through online payment. For buying any course please go to <a href="https://damsdelhi.com/online-registration-new4.php">https://damsdelhi.com/online-registration-new4.php.</a></p>
              </aside>
            </article>
            <div class="book-ur-seat-btn"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>