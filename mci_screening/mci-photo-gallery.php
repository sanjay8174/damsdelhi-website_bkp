<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>


<body class="inner-bg">
<?php error_reporting(0);
$courseId = $_REQUEST['c'];
?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
      <?php// $getVirtualtourcontent = $Dao->getVirtualtourContent($courseId); ?>
            <!-- dams store background Image-->
            <article style="background:url('https://damsdelhi.com/images/background_images/<?php echo $getVirtualtourcontent[0][0];?>') right top no-repeat">
<!--    <article class="photo-gallery-banner">-->
      <?php include 'mci-big-nav.php';?>
      <aside class="banner-left">
          <?php echo $getVirtualtourcontent[0][1];?>
<!--        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>-->
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="index.php?c=<?php echo $courseId ?>" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Events" class="active-link">Events</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Events in DAMS</h4>
            <article class="showme-main">
              <div class="show-me"> <span> <!-- added by shruti for dropdown of events -->
                  <select id="changerdiv" style="vertical-align: middle;color: #6666666; padding: 7px 5px 5px 5px;color: #666666; border: 1px solid #ddd;outline: none;width: 200px;border-radius: 3px;" name="">
                    <option>Show Me</option>
                    <option onClick="divchange('1');" id="divgal1">Event 2011</option>
                    <option onClick="divchange('2');" id="divgal2">Event 2012</option>
                    <option onClick="divchange('3');" id="divgal3">Event 2013</option>
                    <option onClick="divchange('4');" id="divgal4">Videos</option>
                </select> </span>
                  <!-- commented by shruti for dropdown of events -->
<!--                <ul class="mini-list" id="changerdiv">
                  <li><a href="javascript:void(0);" title="Event 2011" class="present" onClick="divchange('1');" id="divgal1">Event 2011</a></li>
                  <li><a href="javascript:void(0);" title="Event 2012" class="" onClick="divchange('2');" id="divgal2">Event 2012</a></li>
                  <li><a href="javascript:void(0);" title="Event 2013" class="" onClick="divchange('3');" id="divgal3">Event 2013</a></li>
                  <li><a href="javascript:void(0);" title="Videos" class="" onClick="divchange('4');" id="divgal4">Videos</a></li>
                </ul>-->
                <select name="" class="mini-event-list">
                  <option>Event 20011</option>
                  <option>Event 20012</option>
                  <option>Event 20013</option>
                  <option>Videos</option>
                </select>
              </div>
              <div class="gallery-list display_block" id="gallery1">
                <ul id="ul1" class="display_block">
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul2" class="display_none">
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul3" class="display_none">
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
              </div>
              <div class="dots display_block" id="galdots1">
                <ul>
                  <li><a href="javascript:void(0);" onClick="blockchange1('1', 'gallery1');" class="active-dot" id="ula1"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('2', 'gallery1');" class="" id="ula2"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('3', 'gallery1');" class="" id="ula3"></a></li>
                </ul>
              </div>
              <div class="gallery-list display_none" id="gallery2">
                <ul id="ul4" class="display_block">
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul5" class="display_none">
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul6" class="display_none">
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
              </div>
              <div class="dots display_none" id="galdots2">
                <ul>
                  <li><a href="javascript:void(0);" onClick="blockchange1('4', 'gallery2');" class="active-dot" id="ula4"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('5', 'gallery2');" class="" id="ula5"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('6', 'gallery2');" class="" id="ula6"></a></li>
                </ul>
              </div>
              <div class="gallery-list display_none" id="gallery3">
                <ul id="ul7" class="display_block">
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul8" class="display_none">
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul9" class="display_none">
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
              </div>
              <div class="dots display_none" id="galdots3">
                <ul>
                  <li><a href="javascript:void(0);" onClick="blockchange1('7', 'gallery3');" class="active-dot" id="ula7"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('8', 'gallery3');;" class="" id="ula8"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('9', 'gallery3');" class="" id="ula9"></a></li>
                </ul>
              </div>
              <div class="gallery-list display_none" id="gallery4">
                <ul id="ul10" class="display_block">
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul11" class="display_none">
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
                <ul id="ul12" class="display_none">
                  <li><a href="images/gb-1.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-1.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-2.jpg" title="Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-2.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-3.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus."><img src="images/g-3.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-4.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo." style="margin-left:0px;"><img src="images/g-4.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-5.jpg" title="Consectetur adipiscing elit. Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-5.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-6.jpg" title="Nam non nisi et diam consequat cursus. Nam non nisi et diam consequat cursus. Donec eget molestie justo."><img src="images/g-6.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-7.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet." style="margin-left:0px;"><img src="images/g-7.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-8.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-8.jpg" title="Gallery" alt="Gallery"></a></li>
                  <li><a href="images/gb-9.jpg" title="Nam non nisi et diam consequat cursus. Donec eget molestie justo. Lorem ipsum dolor sit amet."><img src="images/g-9.jpg" title="Gallery" alt="Gallery"></a></li>
                </ul>
              </div>
              <div class="dots display_none" id="galdots4">
                <ul>
                  <li><a href="javascript:void(0);" onClick="blockchange1('10', 'gallery4');" class="active-dot" id="ula10"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('11', 'gallery4');;" class="" id="ula11"></a></li>
                  <li><a href="javascript:void(0);" onClick="blockchange1('12', 'gallery4');" class="" id="ula12"></a></li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
<script type="text/javascript">
    $(function() {
        $('#gallery1 a').lightBox();
    });
	$(function() {
        $('#gallery2 a').lightBox();
    });
	$(function() {
        $('#gallery3 a').lightBox();
    });
	$(function() {
        $('#gallery4 a').lightBox();
    });
	
function blockchange1(val, ids)
{
   var str=String(ids);
   var galarr = ["gallery1", "gallery2", "gallery3", "gallery4"];
   var countul1 = $('#gallery1 > ul').size();
   var countul2 = $('#gallery2 > ul').size();
   var countul3 = $('#gallery3 > ul').size();
   var countul4 = $('#gallery4 > ul').size();
   
   if(ids=="gallery1")
   {
      for(var g=1;g<=countul1;g++)
      {
	    if(val==g)
	    {
		  $("#ul"+g).fadeIn('slow','swing');
		  $("#ul"+g).removeClass('display_none');
		  $("#ul"+g).addClass('display_block');
	      $("#ula"+g).addClass("active-dot"); 
	    }
	    else
	    {
		  $("#ul"+g).fadeOut('fast','linear');
		  $("#ul"+g).removeClass('display_block');
		  $("#ul"+g).addClass('display_none');
	      $("#ula"+g).removeClass("active-dot"); 
	    }
      }
   }
   else if(ids=="gallery2")
   {
      for(var g=4;g<=(countul2+3);g++)
      {
	    if(val==g)
	    {
		  $("#ul"+g).fadeIn('slow','swing');
		  $("#ul"+g).removeClass('display_none');
		  $("#ul"+g).addClass('display_block');
	      $("#ula"+g).addClass("active-dot"); 
	    }
	    else
	    {
		  $("#ul"+g).fadeOut('fast','linear');
		  $("#ul"+g).removeClass('display_block');
		  $("#ul"+g).addClass('display_none');
	      $("#ula"+g).removeClass("active-dot"); 
	    }
      }
   }
   else if(ids=="gallery3")
   {
      for(var g=7;g<=(countul3+6);g++)
      {
	    if(val==g)
	    {
		  $("#ul"+g).fadeIn('slow','swing');
		  $("#ul"+g).removeClass('display_none');
		  $("#ul"+g).addClass('display_block');
	      $("#ula"+g).addClass("active-dot"); 
	    }
	    else
	    {
		  $("#ul"+g).fadeOut('fast','linear');
		  $("#ul"+g).removeClass('display_block');
		  $("#ul"+g).addClass('display_none');
	      $("#ula"+g).removeClass("active-dot"); 
	    }
      }
   }
   else if(ids=="gallery4")
   {
      for(var g=10;g<=(countul4+9);g++)
      {
	    if(val==g)
	    {
		  $("#ul"+g).fadeIn('slow','swing');
		  $("#ul"+g).removeClass('display_none');
		  $("#ul"+g).addClass('display_block');
	      $("#ula"+g).addClass("active-dot"); 
	    }
	    else
	    {
		  $("#ul"+g).fadeOut('fast','linear');
		  $("#ul"+g).removeClass('display_block');
		  $("#ul"+g).addClass('display_none');
	      $("#ula"+g).removeClass("active-dot"); 
	    }
      }
   }
};

function divchange(val)
{
//	var countdiv= $('#changerdiv > li').size();
        var countdiv= $('#changerdiv > option').size(); // added by shruti for dropdown of events
	for(var d=1;d<=countdiv;d++)
	{
		if(val==d)
		{
		  $("#divgal"+d).addClass("present");
		  $("#gallery"+d).fadeIn('slow','swing');
		  $("#gallery"+d).removeClass('display_none');
		  $("#gallery"+d).addClass('display_block');
		  $("#galdots"+d).fadeIn('slow','swing');
		  $("#galdots"+d).removeClass('display_none');
		  $("#galdots"+d).addClass('display_block');
		}
		else
		{
		  $("#divgal"+d).removeClass("present");
		  $("#gallery"+d).fadeOut('fast','linear');
		  $("#gallery"+d).removeClass('display_block');
		  $("#gallery"+d).addClass('display_none');
		  $("#galdots"+d).fadeOut('fast','linear');  
		  $("#galdots"+d).removeClass('display_block');
		  $("#galdots"+d).addClass('display_none');    
		}
	}
}
</script>
</body>
</html>
