<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-store-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include 'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a title="Dams Mobile Applications" class="active-link">Dams Mobile Applications</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Mobile Applications</h4>
            <article class="showme-main">
              <ul class="idTabs">
                <li><a href="#jquery"><span class="apple"></span> iPhone</a></li>
                <li><a href="#official"><span class="android"></span> Android</a></li>
              </ul>
              <div id="jquery">
                <div class="application-content">
                  <div class="app-store-box">
                    <div class="app-store-btn"><a href="#">&nbsp;</a></div>
                    <p>We invite you to experience the newly released <br />
                      DAMS Delhi for iOS. It is a breeze <br />
                      to use and looks a lot cooler.</p>
                  </div>
                  <p>DAMS Delhi have Launched it's new 1.0.3 version available for Download and installation on all Android Smartphones.
                    This Advance version comes with new featues and better Interface.</p>
                  <div class="screenshot"> <a href="#" title="Screenshot"><img src="images/screenshot-1.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-2.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-3.png" alt="Screenshot" title="Screenshot" /></a> </div>
                  <div class="feature-main-box">
                    <div class="added-feature">
                      <h4>Added Features-</h4>
                      <ul class="added-feature-list">
                        <li>1) Side slide menu.</li>
                        <li>2) Zoom feature added in Test,Solution Report and Bookmark page.</li>
                        <li>3) Sliding from one report to another report.</li>
                        <li>4) Setting module added with clearing app data and auto saving features.</li>
                        <li>5) Better Readability.</li>
                      </ul>
                      <p>DAMS (Delhi Academy of Medical Sciences) is number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.
                        Things have changed again with coming of DAMS Android App. DAMS Identify the need for a mobile based education solution for Indian students and with the growing android market, DAMS has launched an android app that enables students to prepare for exams at their finger tips over their mobile phones.
                        DAMS Android App is a powerful exam Application which enables students to give test in offline and online mode.
                        Practice Test also available to find out how much you score before you appear for your final exams.
                        User can download and give exam in offline mode and result will automatically synchronize to online system.
                        Download the android app on your mobile and take a step forward in your exams preparation.
                        Rate us and Share with your Friends.</p>
                    </div>
                    <div class="added-feature">
                      <h4>Some of the Key Features:</h4>
                      <ul class="some-key">
                        <li>More than 8000 questions that enhances all your skills.</li>
                        <li>You can invite your friend via facebook , email and Sms.</li>
                        <li>This application will work on offline mode , which means no internet connection will be required once the user download the test.</li>
                        <li>You can see Solution Report and Scorecard in offline mode also.</li>
                        <li>Mark Important question as Benchmark for future reference. </li>
                        <li>Get updated by latest News.</li>
                        <li>Auto-synchronization facility , which means you test data automatically sync to server as soon as you connect to internet.</li>
                        <li>Buy Package and again get ready to give more test.</li>
                        <li>Connect with admin to solve your query by using Email feature.</li>
                      </ul>
                      <div class="app-store-box">
                        <div class="app-store-btn"><a href="#">&nbsp;</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="official">
                <div class="application-content">
                  <div class="app-store-box">
                    <div class="android-store-btn"><a href="#">&nbsp;</a></div>
                    <p>We invite you to experience the newly released <br />
                      DAMS Delhi for iOS. It is a breeze <br />
                      to use and looks a lot cooler.</p>
                  </div>
                  <p>DAMS Delhi have Launched it's new 1.0.3 version available for Download and installation on all Android Smartphones.
                    This Advance version comes with new featues and better Interface.</p>
                  <div class="screenshot"> <a href="#" title="Screenshot"><img src="images/screenshot-1.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-2.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-3.png" alt="Screenshot" title="Screenshot" /></a> </div>
                  <div class="feature-main-box">
                    <div class="added-feature">
                      <h4>Added Features-</h4>
                      <ul class="added-feature-list">
                        <li>1) Side slide menu.</li>
                        <li>2) Zoom feature added in Test,Solution Report and Bookmark page.</li>
                        <li>3) Sliding from one report to another report.</li>
                        <li>4) Setting module added with clearing app data and auto saving features.</li>
                        <li>5) Better Readability.</li>
                      </ul>
                      <p>DAMS (Delhi Academy of Medical Sciences) is number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.
                        Things have changed again with coming of DAMS Android App. DAMS Identify the need for a mobile based education solution for Indian students and with the growing android market, DAMS has launched an android app that enables students to prepare for exams at their finger tips over their mobile phones.
                        DAMS Android App is a powerful exam Application which enables students to give test in offline and online mode.
                        Practice Test also available to find out how much you score before you appear for your final exams.
                        User can download and give exam in offline mode and result will automatically synchronize to online system.
                        Download the android app on your mobile and take a step forward in your exams preparation.
                        Rate us and Share with your Friends.</p>
                    </div>
                    <div class="added-feature">
                      <h4>Some of the Key Features:</h4>
                      <ul class="some-key">
                        <li>More than 8000 questions that enhances all your skills.</li>
                        <li>You can invite your friend via facebook , email and Sms.</li>
                        <li>This application will work on offline mode , which means no internet connection will be required once the user download the test.</li>
                        <li>You can see Solution Report and Scorecard in offline mode also.</li>
                        <li>Mark Important question as Benchmark for future reference. </li>
                        <li>Get updated by latest News.</li>
                        <li>Auto-synchronization facility , which means you test data automatically sync to server as soon as you connect to internet.</li>
                        <li>Buy Package and again get ready to give more test.</li>
                        <li>Connect with admin to solve your query by using Email feature.</li>
                      </ul>
                      <div class="app-store-box">
                        <div class="android-store-btn"><a href="#">&nbsp;</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <div class="content-right-div">
          <div id="store-wrapper">
            <div class="dams-store-block">
              <div class="dams-store-heading"><span></span>CATEGORIES</div>
              <div class="dams-store-accordion">
                <div class="dams-inner-store">
                  <ul>
                    <?php
include 'openconnection.php';
  $sql=mysql_query("select * FROM COURSE WHERE ACTIVE=1");
    $i=0;
while($row=  mysql_fetch_object($sql)){ ?>
                    <li id="lis<?php echo $row->COURSE_ID; ?>" onClick="accordion1(<?php echo $row->COURSE_ID; ?>)" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="background:url(images/minus-icon.png) 12px 6px no-repeat;border:none;cursor:pointer"<?php } else{ ?>style="background:url(images/plus-icon.png) 12px 6px no-repeat;border:none;cursor:pointer"<?php } ?>><a title="<?php echo urldecode($row->COURSE_NAME); ?>" ><?php echo urldecode($row->COURSE_NAME); ?></a></li>
                    <div class="inner-store" id="d<?php echo $row->COURSE_ID; ?>" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="display:block"<?php } ?>>
                      <ol>
                        <?php $sql1=mysql_query("select * FROM PRODUCT_CATEGORY WHERE COURSE_ID='$row->COURSE_ID' AND ACTIVE=1 ORDER BY PRODUCT_CATEGORY_ID");
$k=1;while($rows=  mysql_fetch_object($sql1)){ ?>
                        <li style="cursor:pointer"><a id="category<?php echo $row->COURSE_ID.".".$k; ?>" onclick="location.href='dams-publication.php?c=<?php echo $row->COURSE_ID; ?>'"><?php echo urldecode($rows->PRODUCT_CATEGORY_NAME); ?></a></li>
                        <?php $k++; } ?>
                        <input type="hidden" value="<?php echo $k; ?>" id="totalCategory<?php echo $row->COURSE_ID; ?>" />
                      </ol>
                    </div>
                    <?php $i++; } ?>
                    <li class="boder-none">
                      <input type="hidden" value="<?php echo $i; ?>" id="totalCourse" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <?php include 'buynow-right-section.php'; ?>
        </div>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script src="https://cdn.webrupee.com/js" type="text/javascript"></script>
<script type="text/javascript">
accordion(<?php echo $_REQUEST['c']; ?>);
function viewmore(val)
{
	if(val=='u1')
	{
		$('#u1>li').slideDown();
		$('#view-more').hide();
	}
}

function accordion(val)
{
 var lcount=$('#totalCourse').val();
   for(var f=1;f<=lcount;f++)
   {
       if(val!=f)
	   {

    	 $('#lis'+f).css('background','url(images/plus-icon.png) 12px 6px no-repeat');
		 $('#d'+f).hide();
	   }
   }

   $('#d'+val).toggle(function(){
	   if($(this).is(':visible'))
	   {
			$('#lis'+val).css('background','url(images/minus-icon.png) 12px 6px no-repeat');
       }
	   else
	   {
			$('#lis'+val).css('background','url(images/plus-icon.png) 12px 6px no-repeat');
       }
if(val==1){
$('#storeCourseName').html('<a href="javascript:void(0);" title="MD/MS">MD/MS</a>')
}
else if(val==2){
$('#storeCourseName').html('<a href="javascript:void(0);" title="MCI SCREENING">MCI SCREENING</a>')}
else if(val==3){
$('#storeCourseName').html('<a href="javascript:void(0);" title="MDS QUEST">MDS QUEST</a>')}
else if(val==4){
$('#storeCourseName').html('<a href="javascript:void(0);" title="USMLE EDGE">USMLE EDGE</a>')}
 var urlData="mode=leftCartData&c="+val;
    $.ajax({
           type: "post",
                url: "ajax.php",
                data: urlData ,
                async:false,
                error:
                function(result) {
                    //alert(result);
                },
                success: function(result){
                    $('#leftCartContainer').html(result);
                }
      });
   });
}
function courseChange(id){
    var urlData="mode=ChangeProductData&id="+id;
    $.ajax({
           type: "post",
                url: "ajax.php",
                data: urlData ,
                async:false,
                error:
                function(result) {
                    //alert(result);
                },
                success: function(result){
                    $('#topCartDiv').html(result);
                }
      });
}
function ShowCategoryProduct(cid,pcid){

var val=$('#totalCategory'+cid).val();
for(var i=1;i<=val;i++){    
        $('#leftCategory'+i).hide();
        $('#category'+cid+"."+i).removeClass('active-store');  
}
  $('#leftCategory'+pcid).show();
  $('#category'+cid+"."+pcid).addClass('active-store');
}
</script>
</body>
</html>
