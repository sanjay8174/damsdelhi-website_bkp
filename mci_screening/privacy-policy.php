<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <aside class="banner-left banner-left-postion">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Privacy Policy</h4>
            <article class="showme-main">
              <div class="privacy-content"> <span>You certainly understand and concur that :-</span>
                <p>Delhi Academy of Medical Sciences Pvt. Ltd. (<a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a>) has created this privacy policy in order to demonstrate own organization commitment to privacy. Throughout the cyberspace we want to contribute towards providing a safe and secure environment, safe information gathering and dissemination practices for all our sites. This policy applies only to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> which carries the Delhi Academy of Medical Sciences Pvt. Ltd. brand and not to other companies or organizations' websites to which we link.</p>
                <ul class="privacy-list">
                  <li>
                    <label>Registration / Information :</label>
                    <p>When you sign up for <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> we ask you for personal information. We may combine the information you submit under your account with information from other services / third parties in order to provide you with a better experience and to improve the quality of our services. For certain services, we may give you the opportunity to opt out of combining such information.
                      You may provide us with certain information such as your Name, E-mail address, Correspondence address when registering for certain services such as Online Registration / Submit Resume, Contests. This information will primarily be used for the purpose of providing personalization and verification.</p>
                  </li>
                  <li>
                    <label>Cookies :</label>
                    <p>A cookie is a small data file that certain websites write to your hard drive when you visit them. A cookie file can contain information such as a user ID that the site uses to track the pages you have visited. A cookie can contain information you supply yourself. A cookie can't read data of your hard disk or read cookie files created by other sites. <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> uses cookies to track user traffic patterns and for the personalization feature. </p>
                  </li>
                  <li>
                    <label>User communications :</label>
                    <p>When you send email or other communications to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> , we may retain those communications in order to process your inquiries, respond to your requests and improve our services. When you send and receive SMS messages to or from one of our services that provides SMS functionality, we may collect and maintain information associated with those messages, such as the phone number, the content of the message, and the date and time of the transaction. We may use your email address to communicate with you about our services.</p>
                  </li>
                  <li>
                    <label>Log information :</label>
                    <p>When you access <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> services via a browser, application or other client our servers automatically record certain information. These server logs may include information such as your web request, your interaction with a service, Internet Protocol address, browser type, browser language, the date and time of your request and one or more cookies that may uniquely identify your browser or your account.</p>
                  </li>
                  <li>
                    <label>Electronic Newsletter/E-mail :</label>
                    <p><a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> offers a free electronic newsletter to its users. We gather the e-mail addresses of users who voluntarily subscribe. Users may remove themselves from this mailing list by using the link provided in every newsletter.</p>
                  </li>
                  <li>
                    <label>Confidential :</label>
                    <p>DAMS Privacy Policy applies to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> services only. We do not exercise control over the sites displayed as search results, sites that include other applications, products or services, or links from within our various services. Personal information that you provide to other sites may be sent to <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> in order to deliver the service. We process such information under this Privacy Policy.</p>
                  </li>
                  <li>
                    <label>Feedback :</label>
                    <p>Our site's Feedback Form requires contact information of users like their name and e-mail address and demographic information like their zip code, age etc. for better services.</p>
                  </li>
                  <li>
                    <label>Further Improvement :</label>
                    <p>Apart from the above, we may use the information to provide, maintain, protect and improve our services and develop new services.</p>
                  </li>
                  <li>
                    <label>Queries regarding the Website :</label>
                    <p>If you have any questions about the practices of this site or your dealings with this website, regarding DAMS Privacy Policy, contact DAMS Corporate office <a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a>. </p>
                  </li>
                  <li>
                    <label>Electronic Newsletter/E-mail :</label>
                    <p><a href="https://www.damsdelhi.com" title="Delhi Academy of Medical Sciences">www.damsdelhi.com</a> offers a free electronic newsletter to its users. We gather the e-mail addresses of users who voluntarily subscribe. Users may remove themselves from this mailing list by using the link provided in every newsletter.</p>
                  </li>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>