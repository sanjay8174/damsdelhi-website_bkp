<?php
class dao extends Database{

    public function getCourse(){
         $i=0;
         $sql = parent::executeQuery("select COURSE_ID,COURSE_NAME,URL_LINK,ACTIVE,CONTENT_HOME from COURSE ;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseArray[$i][0] = urldecode($rows['COURSE_ID']);
          $getCourseArray[$i][1] = urldecode($rows['COURSE_NAME']);
          $getCourseArray[$i][2] = urldecode($rows['URL_LINK']);
          $getCourseArray[$i][3] = urldecode($rows['ACTIVE']);
          $getCourseArray[$i][4] = urldecode($rows['CONTENT_HOME']);

          $i++;
        }
        return $getCourseArray;
     }

      public function getAboutus(){
         $i=0;
         $sql = parent::executeQuery("select ID,URL_LINK,ABOUT_US_NAME from ABOUT_US where ACTIVE=1;");

        while ($rows = mysql_fetch_array($sql)){

          $getAboutusArray[$i][0] = urldecode($rows['ID']);
          $getAboutusArray[$i][1] = urldecode($rows['URL_LINK']);
          $getAboutusArray[$i][2] = urldecode($rows['ABOUT_US_NAME']);

          $i++;
        }
        return $getAboutusArray;
     }
     public function  getAboutusContent($Id){
         $Id= (int)$Id;
         $i=0;
         $sql = parent::executeQuery("select BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,CONTENT_HOME from ABOUT_US where ACTIVE=1 and ID=$Id;");

        while ($rows = mysql_fetch_array($sql)){

          $getAboutusContentArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getAboutusContentArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getAboutusContentArray[$i][2] = urldecode($rows['CONTENT_HOME']);
          
          $i++;
        }
        return $getAboutusContentArray;
     }

     public function getCourseNav($cId){
         $cId= (int)$cId;
         $i=0;
         $sql = parent::executeQuery("select COURSE_NAVIGATION_ID,NAME,URL_LINK from COURSE_NAVIGATION where COURSE_ID=$cId;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseNavArray[$i][0] = urldecode($rows['COURSE_NAVIGATION_ID']);
          $getCourseNavArray[$i][1] = urldecode($rows['NAME']);
          $getCourseNavArray[$i][2] = urldecode($rows['URL_LINK']);


          $i++;
        }
        return $getCourseNavArray;
     }
     public function getCourseNavContent($navId){
    $navId= (int)$navId;

     $i=0;
     $sql = parent::executeQuery("SELECT CN.COURSE_NAVIGATION_ID,CN.NAME,CN.URL_LINK,CN.COURSE_NAVIGATION_ORDER,CN.ACTIVE,CN.COURSE_ID,CN.CONTENT,C.COURSE_NAME FROM COURSE_NAVIGATION CN LEFT JOIN COURSE C ON CN.COURSE_ID=C.COURSE_ID where COURSE_NAVIGATION_ID=$navId");
     while ($rows = mysql_fetch_array($sql)){

          $getContentNavArray[$i][0] = urldecode($rows['CONTENT']);
          $i++;
        }
        return $getContentNavArray;
     }

     public function getCategory($cId,$cNId){
         $cId= (int)$cId;
         $cNId= (int)$cNId;
         $i=0;

         $sql = parent::executeQuery("select CATEGORY_NAME,CATEGORY_ID FROM CATEGORY where ACTIVE=1 and COURSE_ID=$cId and COURSE_NAVIGATION_ID=$cNId ORDER BY CATEGORY_ORDER");

        while ($rows = mysql_fetch_array($sql)){

          $getCategoryArray[$i][0] = urldecode($rows['CATEGORY_NAME']);
          $getCategoryArray[$i][1] = urldecode($rows['CATEGORY_ID']);
           


          $i++;
        }
        return $getCategoryArray;
     }

     public function getCategory2($categoryId,$cId){
           $categoryId  = (int)$categoryId;
           $cId         = (int)$cId;
         $i=0;

         $sqlA = parent::executeQuery("select ID,NAME from CATEGORY2 where ACTIVE=1 and CATEGORY1_ID=$categoryId and COURSE_ID=$cId ORDER BY CATEGORY_ORDER ");
         
        while($rowsA = mysql_fetch_object($sqlA)){

          $getCategory2Array[$i][0] = urldecode($rowsA->ID);
          $getCategory2Array[$i][1] = urldecode($rowsA->NAME);



          $i++;
        }
        return $getCategory2Array;
     }
     
     public function getSubCourse($categoryId2,$categoryId,$cId){
           $categoryId2 = (int)$categoryId2;
           $categoryId  = (int)$categoryId;
           $cId         = (int)$cId;
         $i=0;

         $sqlB = parent::executeQuery(" select SUB_COURSE_NAME,URL_LINK from SUB_COURSE where COURSE_ID=$cId and CATEGORY_ID=$categoryId and CATEGORY_ID2=$categoryId2");
         
        while($rowsB = mysql_fetch_object($sqlB)){

          $getSubCourseArray[$i][0] = urldecode($rowsB->SUB_COURSE_NAME);
          $getSubCourseArray[$i][1] = urldecode($rowsB->URL_LINK);



          $i++;
        }
        return $getSubCourseArray;
     }

     public function getNews($cId){
         $cId = (int)$cId;
         $i=0;

         $sqlD = parent::executeQuery("SELECT HEADING FROM NEWS WHERE COURSE_ID=$cId AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");

        while($rows = mysql_fetch_object($sqlD)){
          
          $newsDetail[$i] = urldecode($rows->HEADING);
          
         $i++;
        }
        return $newsDetail;
     }

     public function getRecentAchieve($cId){
         $cId = (int)$cId;
         $i=0;

         $sql = parent::executeQuery("select F.FILE_NAME,F.NAME,F.RANK,E.EVENT_NAME,F.EVENT_ID FROM FILE F LEFT JOIN EVENT E ON F.EVENT_ID = E.EVENT_ID WHERE F.COURSE_ID =$cId ORDER BY FILE_ID DESC LIMIT 0,8 ");

        while($rows = mysql_fetch_object($sql)){

          $getRecentAchieve[$i][0] = urldecode($rows->FILE_NAME);
          $getRecentAchieve[$i][1] = urldecode($rows->RANK);
          $getRecentAchieve[$i][2] = urldecode($rows->EVENT_NAME);
          $getRecentAchieve[$i][3] = urldecode($rows->EVENT_ID);
          $getRecentAchieve[$i][4] = urldecode($rows->NAME);

         $i++;
        }
        return $getRecentAchieve;
     }

     public function getHomepopup(){
         $i=0;

         $sql = parent::executeQuery("SELECT * from HOME_POPUP");

        while ($rows = mysql_fetch_array($sql)){

          $gethomePopupArray[$i][0] = urldecode($rows['IMAGE_PATH']);
          $gethomePopupArray[$i][1] = urldecode($rows['ACTIVE']);
          $gethomePopupArray[$i][2] = urldecode($rows['URL_LINK']);

          $i++;
        }
        return $gethomePopupArray;
     }

public function getHomephotogallery(){
         $i=0;

         $sql = parent::executeQuery("SELECT * from HOME_PHOTO_GALLERY order by PHOTO_GALLERY_ORDER;");

        while ($rows = mysql_fetch_array($sql)){

          $gethomephotogalleryArray[$i][0] = urldecode($rows['IMAGE_PATH']);
          $gethomephotogalleryArray[$i][1] = urldecode($rows['ACTIVE']);
          
          $i++;
        }
        return $gethomephotogalleryArray;
     }

     public function getFindcenterContent(){
     $i=0;
     $sql = parent::executeQuery("SELECT * FROM  FIND_CENTER_CONTENT");
     while ($rows = mysql_fetch_array($sql)){

          $getContentFindcenterArray[$i][0] = urldecode($rows['BACKGROUND_IMAGE_UPLOAD']);
          $getContentFindcenterArray[$i][1] = urldecode($rows['BACKGROUND_IMAGE_CONTENT']);
          $getContentFindcenterArray[$i][2] = urldecode($rows['ACTIVE']);
          $i++;
        }
        return $getContentFindcenterArray;
     }
    public function getYear($id){
         $id = (int)$id;
         $i=0;
        
         $sql = parent::executeQuery("SELECT distinct(YEAR),EVENT_NAME,EVENT_ID FROM EVENT where COURSE_ID=$id GROUP BY YEAR ORDER BY YEAR DESC");
        
        while ($rows = mysql_fetch_array($sql)){
    
          $getYearArray[$i][0] = urldecode($rows['YEAR']);
          $getYearArray[$i][1] = urldecode($rows['EVENT_NAME']);
          $getYearArray[$i][2] = urldecode($rows['EVENT_ID']);
          
          $i++;
        }
        return $getYearArray;
     }

     public function getEvent($cId,$year){
         $cId = (int)$cId;
         $i   = 0;

         $sql = parent::executeQuery("select * from EVENT where COURSE_ID = $cId and YEAR = $year ");

        while ($rows = mysql_fetch_array($sql)){

          $getEventArray[$i][0]= urldecode($rows['EVENT_NAME']);
          $getEventArray[$i][1] = urldecode($rows['EVENT_ID']);
          $getEventArray[$i][2] = urldecode($rows['YEAR']);
          $i++;
        }
        return $getEventArray;
     }

     public function getData($eventId){
         $eventId = (int)$eventId;
         $i=0;

         $sql = parent::executeQuery("select NAME,RANK,COURSE_ID,FILE_NAME from FILE where EVENT_ID =$eventId");

        while ($rows = mysql_fetch_array($sql)){

          $getDataArray[$i][0]= urldecode($rows['NAME']);
          $getDataArray[$i][1] = urldecode($rows['RANK']);
          $getDataArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getDataArray[$i][3] = urldecode($rows['FILE_NAME']);

          $i++;
        }
        return $getDataArray;
     }

     public function getDataVideo($eventId){
         $eventId = (int)$eventId;
         $i=0;

         $sql = parent::executeQuery("select NAME,RANK,COURSE_ID,URL_LINK,YOU_TUBE,FILE_NAME from VIDEO where EVENT_ID =$eventId");

        while ($rows = mysql_fetch_array($sql)){

          $getDataVideoArray[$i][0]= urldecode($rows['NAME']);
          $getDataVideoArray[$i][1] = urldecode($rows['RANK']);
          $getDataVideoArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getDataVideoArray[$i][3] = urldecode($rows['URL_LINK']);
          $getDataVideoArray[$i][4] = urldecode($rows['YOU_TUBE']);
          $getDataVideoArray[$i][5] = urldecode($rows['FILE_NAME']);

          $i++;
        }
        return $getDataVideoArray;
     }
     public function getIntVideo($cId){
         $cId = (int)$cId;
         $i=0;

         $sql = parent::executeQuery("select NAME,EVENT_ID,COURSE_ID,URL_LINK,YOU_TUBE,FILE_NAME from VIDEO where COURSE_ID =$cId limit 0,5");

        while ($rows = mysql_fetch_array($sql)){

          $getIntVideoArray[$i][0]= urldecode($rows['NAME']);
          $getIntVideoArray[$i][1] = urldecode($rows['EVENT_ID']);
          $getIntVideoArray[$i][2] = urldecode($rows['COURSE_ID']);
          $getIntVideoArray[$i][3] = urldecode($rows['URL_LINK']);
          $getIntVideoArray[$i][4] = urldecode($rows['YOU_TUBE']);
          $getIntVideoArray[$i][5] = urldecode($rows['FILE_NAME']);

          $i++;
        }
        return $getIntVideoArray;
     }

     public function getNavId($cid){
         $cid = (int)$cid;
         $i=0;

         $sql = parent::executeQuery("select COURSE_NAVIGATION_ID from COURSE_NAVIGATION where COURSE_ID=$cid and NAME like '%face%'");

      $rows = mysql_fetch_array($sql);


        return $rows[COURSE_NAVIGATION_ID];
     }
 public function getCourseHome(){
         $i=0;

         $sql = parent::executeQuery("select COURSE_ID,CONTENT_HOME from COURSE ;");

        while ($rows = mysql_fetch_array($sql)){

          $getCourseArray[$rows['COURSE_ID']] = urldecode($rows['CONTENT_HOME']);


        }
        return $getCourseArray;
     }

     public function getHomeBanner(){
         $sql = $sql = parent::executeQuery("SELECT * FROM HOME_BANNER WHERE ACTIVE = 1 ORDER BY ORDER_BANNER ASC");
         return $sql;
     }
     
     public function getUseFullLink(){
         $sql = parent::executeQuery("SELECT * FROM USEFUL_LINK WHERE ACTIVE = 1 ORDER BY LINK_ORDER ASC" );
         return $sql;
     }
     public function getAllNewsAndEvent(){
//         $sql = parent::executeQuery("SELECT * FROM HOME_NEWS WHERE ACTIVE = 1 ORDER BY NEWS_ORDER ASC limit 0,4" );   // comment by heena for latest news show
          $sql = parent::executeQuery("SELECT * FROM HOME_NEWS WHERE ACTIVE = 1 ORDER BY ID DESC limit 0,4" );
         return $sql;
     }
     
     public function getAllToperTalk(){
         $sql = parent::executeQuery("SELECT * FROM TOPPER_TALKS WHERE ACTIVE = 1 ORDER BY TALK_ORDER ASC" );
         return $sql;
     }
     public function getAllHomePhotoGall(){
         $sql = parent::executeQuery("SELECT * FROM PHOTO_GALLERY WHERE ACTIVE = 1 ORDER BY PHOTO_ORDER ASC" );
         return $sql;
     }
     
     public function getHomeCourse(){
         $sql = parent::executeQuery("SELECT IMG_EXT,COURSE_ID ,URL_LINK,CONTENT_HOME,COURSE_ORDER,COURSE_NAME FROM COURSE WHERE ACTIVE = 1 AND SINGLE_COURSE = 1 ORDER BY COURSE_ORDER ASC" );
         return $sql;
     }
     public function  getHomeNewsDescp($id){
         $id = (int)$id;
         $sql = parent::executeQuery("SELECT NEWS_DESCRIPTION FROM HOME_NEWS WHERE ID='$id' AND ACTIVE=1" );
         return $sql;
     }
     
     public function  getHomeNewsDesc($start,$limit){
       $start= (int)$start;
       $limit= (int)$limit;

        // $sql = parent::executeQuery("SELECT * FROM HOME_NEWS WHERE  ACTIVE = 1 ORDER BY NEWS_ORDER DESC LIMIT $start,$limit" ); //comment by heena for show latest news
          $sql = parent::executeQuery("SELECT * FROM HOME_NEWS WHERE  ACTIVE = 1 ORDER BY ID DESC LIMIT $start,$limit" );
         return $sql;
     }
     public function  getHomeNewsRow(){
         $sql = parent::executeQuery("SELECT * FROM HOME_NEWS WHERE  ACTIVE = 1" );
         return $sql;
     }
     
}
