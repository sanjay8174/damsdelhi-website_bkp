<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
include 'config/constant.php';
$pathCMS = constant::$pathCMS;
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>USMLE EDGE Coaching Institute, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
      <article class="usmle-edge-banner">
          <div class="big-nav" style="width: 100%;">
              <ul>
                  <li class="t-series active" style="float: right;"><a class="t-series-active" href="achievements.php?c=1" title="Test Series" >Achievement</a></li>
              </ul>
          </div>
          <aside class="banner-left banner_over">
        <h3>UNITED STATES MEDICAL LICENSING EXAM  (USMLE) <span>First Time in India: DYNAMIC LIVE INSTRUCTION <br>EVERY WEEKEND AT DAMS</span></h3>
<!--      <div  class="bannerlfthead"style="">
	   <p>Live Usmle courses first time in India, being launched on <span style="font-size:22px;  font-family: sans-serif;  font-weight: 600;">8<sup>th</sup> August.</span> </p>
	   <p> <span style="font-weight: 600;">It is an 16 weeks course only on weekends.</span></p>
	   <p>Handouts and question bank will be provide along with complete counselling for Usmle .</p>
       <p>Only counselling and Question bank packages also available . Please call for more details.</p>
	   <span class="book-ur-seat-btn1"><a href="https://192.168.0.250/damswebsiteNew/usmle_edge/online-registration.php" title="Book Your Seat"> Book Your Seat</a></span>
	</div>-->
       <div class="usmle-edge"><a href="https://usmle.damsdelhi.com/usmle-step2.php"><img style="width:100%" src="https://damsdelhi.com/images/Step-2.png" alt=""></a> </div>

      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>USMLE  Introduction <span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
        <p>USMLE- The United States Medical Licensing</p>
        <p>USMLE is a three-step examination for medical licensure in the United States and is sponsored by the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners NBME. Need to take Step 1 , 2CK and CS to obtain a certification to apply for a residency in the US.<br> </p>
        <p><b>For the first time in India an Initiative taken By DAMS to start Live Face to face Lectures for USMLE . No other Institute provides Live Coaching for USMLE in India.</b><br> </p>

      </div>

      <?php include 'usmle-middle-accordion_1.php'; ?>
    </aside>
    <aside class="content-right">

        <div class="content-date-venue res_css">
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 011-40094009</p>
            <p>(Enquiry Timing:- 10am To 8pm)</p>
        </div>

      <?php //include 'dams-usmle-edge.php'; ?>
      <?php
    include 'openconnection.php';
    $count = 0;
    $i = 0;
    $sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
    while ($row = mysql_fetch_array($sql)) {
        $newsDetail[$count] = urldecode($row['HEADING']);
        $count++;
    }
?>

      <div class="news-update-box">
        <div class="n-videos"><span></span> USMLE Lecture</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block">
              <a target="_blank" href="https://www.youtube.com/embed/zYO3yrb_eus"><img class="border_none" src="https://usmle.damsdelhi.com/images/images.png" width="100%" height="236" alt="https://damsdelhi.com" frameborder="0"></a>
          </div>
          <div id="vd1" class="display_none">
            <a target="_blank" href="https://www.youtube.com/embed/ES_tAnjfkyY?wmode=transparent"><img class="border_none" src="https://img.youtube.com/vi/ES_tAnjfkyY/0.jpg" width="100%" height="236" alt="z_xgJNXaWuQ.jpg" frameborder="0"></a>
          </div>
          <div id="vd2" class="display_none">
            <a target="_blank" href="https://www.youtube.com/embed/NaJovLT0TUo?wmode=transparent"><img class="border_none" src="https://img.youtube.com/vi/NaJovLT0TUo/0.jpg" width="100%" height="236" alt="NaJovLT0TUo.jpg" frameborder="0"></a>
          </div>

        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>

              </ul>
            </div>
          </div>

          <span class="mini-view-left"></span> </div>
        <?php include 'enquiryform.php'; ?>
      </div>
    </aside>
   <aside class="content-left" style="margin-top:10px;width: 100%">
        <div class="course-box">
         <p><b>Disclaimer</b>
         <br>
         <p style="text-align: justify;font-size: 12px">USMLE® is a joint program of the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME). The ECFMG® is a registered trademark of the Educational Commission for Foreign Medical Graduates. The Match® is a registered service mark of the National Resident Matching Program® (NRMP®). The NRMP is not affiliated with DAMS. Electronic Residency Application Service (ERAS®) is a program of the association of American Medical Colleges and is not affiliated with DAMS. Test names and other trademarks are the property of the respective trademark holders. None of the trademark holders are affiliated with DAMS or this website.<br>
        </div>
      </aside>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php  include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>