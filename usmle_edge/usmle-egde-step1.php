<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	

//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	
 
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usmle-edge.php" title="USMLE EDGE">USMLE EDGE</a></li>
          <li><a title="USMLE Edge Step 1" class="active-link">USMLE Edge Step 1</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>USMLE EDGE Step-1
              <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
            </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>Step-I :-</span>
                  <p>The USMLE step 1 is the one of the three examinations that you must pass in order to become a licensed physician in the United States. The purpose of USMLE step 1 is to test your understanding and application of important concepts in basic biomedical sciences, with an emphasis on principles and mechanisms of health, disease, and modes of therapy. USMLE Step 1 is a one-day Computer-based Test that can be taken at the end of second year of medical school. </p>
                  <p>It emphasizes basic science principles. Specifically, Step 1 covers Anatomy, Behavioral Science, Biochemistry, Microbiology, Pathology, Pharmacology, and Physiology. Interdisciplinary areas such as genetics, immunology, and nutrition are also tested. The exam consists of approximately 350 questions given in 8 hours of testing time. There are seven 60-minute blocks, in each block you have to answer about 50 questions. You will be able to skip back and forth among questions, but ONLY within a block of questions. Once the hour is over, you will be unable to return to those 50 items. The test items aren't grouped by clinical subject but are presented in a random, interdisciplinary sequence. All USMLE questions are in one best-answer format. There are no matching questions on the computerized USMLE Step 1.</p>
                </div>
                <div class="franchisee-box"> <span>Systems :-</span>
                  <p>40-50 % General Principles</p>
                  <p>50-60 % Individual Organ Systems – cardiovascular, Hemopoetic/ Lymphoreticular, Gasterointestinal, Nervous / Special Senses, Renal / Urinary, Skin / Connective tissue,<br />
                    Reproductive, Musculo-skeletal, Endocrine, Pulmonary / Respiratory</p>
                </div>
                <ul class="dnb-list">
                  <h5>Process :-</h5>
                  <li><span>&nbsp;</span>30-50 % Normal structure and function.</li>
                  <li><span>&nbsp;</span>30-50 % Abnormal Processes.</li>
                  <li><span>&nbsp;</span>15-25 % Principles of therapeutics.</li>
                  <li><span>&nbsp;</span>10-20 % Psychosocial, cultural, occupational and environmental considerations.</li>
                </ul>
                <ul class="dnb-list">
                  <h5>DAMS PACKAGES FOR USMLE :- :-</h5>
                  <li><span>&nbsp;</span>ONLINE TEST SERIES.</li>
                  <li><span>&nbsp;</span>10 BLOCKS.</li>
                  <li><span>&nbsp;</span>2 MONTHS.</li>
                  <li><span>&nbsp;</span>1 HR PER PAPER.</li>
                </ul>
              </div>
            </article>
            <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>   </aside>
        <aside class="gallery-right">
          <?php include '../dams-usmle-edge.php'; ?>
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>