<?php error_reporting(0);
include 'openconnection.php'; 
//include 'config/constant.php'; ?>
<div class="social-icon">
<a href="https://www.facebook.com/damsdelhiho" target="_blank" title="Connect with us on Facebook" class="l-facebook">Facebook</a>
<a href="https://twitter.com/damsdelhi" target="_blank" title="DAMS on Twitter" class="l-twitter">Twitter</a>
<a href="https://in.linkedin.com/pub/dams-delhi/40/415/570" target="_blank" title="DAMS on Linkedin" class="l-mail">LinkedIn</a>
<a href="https://www.youtube.com/user/damsdelhi" target="_blank" title="Watch DAMS Videos" class="l-print">YouTube</a>
<a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" title="Follow DAMS on Google+" class="l-share">Google Plus</a>
</div>
<div class="top-header">
<ul class="top-nav">
<li><a href="https://www.damsdelhi.com/contact.php" title="Contact us">Contact us</a></li>
<li><a href="https://www.damsdelhi.com/franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="https://blog.damsdelhi.com/" title="Blog">Blog</a></li>
<li><a href="https://www.damsdelhi.com/career.php" title="Career">Career</a></li>
<li><a href="https://www.damsdelhi.com/download.php" title="Download">Download</a></li>
<li class="bg_none"><a href="https://www.damsdelhi.com/" title="Home">Home</a></li>
</ul>
</div>

<!-- Header Css Start Here -->
<header>
<div class="wrapper">
<h1><a href="https://damsdelhi.com/index.php" class="mob_logo" title="Delhi Academy of Medical Sciences"><img src="images/logo_mobile.jpg" alt="DAMS Logo" /></a></h1> 
<div class="top_nav_bg"></div>
<aside class="t-right-side">
<div class="inner-right-top">
<?php include 'https://www.damsdelhi.com/addCart.php'; ?>
<div class="login-registration">
<a href="javascript:void(0);" class="l-r-left" id="student-login" title="Online / Offline Test Login"><span></span>Online / Offline<br>Test Login</a>
<a href="javascript:void(0);" class="l-r-middle" id="cloud-login" title="DAMS Cloud Student Login"><span></span>DAMS<br>Cloud</a>
<a href="javascript:void(0);" class="l-r-right" id="student-registration" title="New Student Registration"><span></span>New&nbsp;Student<br>Registration</a>
</div>
<div class="inner-some-numbers">
<div class="inner-helpline-no">Helpline&nbsp;no: <span>011-40094009</span></div>
<div class="inner-sms-enquiry display_none">SMS Enquiry: <span>sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span></div>
</div>
</div>
</aside>

<?php error_reporting(0);
include 'openconnection.php'; ?>

<!-- Responcive Navigation Section End Here -->
<div class="r-screen-b">
<div class="demo-box-d">
<div class="r-navigation demo1-d" style="display:block; right: 0px;">
<div class="r-navigation_button" id="menudrop"><span class="mob_menu">MENU</span></div>
</div>
<div class="demo3-d">
<div class="close-navigation demo1-d"> X </div>
<ul>
<li class="boder-none"><div class="modify-close">Close <span class="demo1-d">X</span></div></li>
<li><a href="https://www.damsdelhi.com/" title="Home">Home</a></li>
<li><a href="javascript:void(0);" title="About Us" onclick="menuslides(1);"><span>&nbsp;</span>About Us</a>
<ul class="inner-d-down" id="inul1" style="display:none;">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="https://www.damsdelhi.com/dams.php" title="About DAMS">About DAMS</a></li>
<li><a href="https://www.damsdelhi.com/dams_director.php" title="Director's Message">Director's Message</a></li>
<li><a href="https://www.damsdelhi.com/about_director.php" title="About Director">About Director</a></li>
<li><a href="https://www.damsdelhi.com/mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="https://www.damsdelhi.com/dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
</ul>
</li>

<li><a href="javascript:void(0);" title="Courses" onclick="menuslides(2);"><span>&nbsp;</span>Courses</a>
<ul class="inner-d-down" id="inul2" style="display:none;">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="javascript:void(0);" title="MD / MS ENTRANCE" onclick="innermenuslides(1);"><span>&nbsp;</span>MD / MS ENTRANCE</a>
<ul class="extra-inner" id="innrul1" style="display:none;">
<li><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="test-series.php" title="Test Series">Test Series</a></li>
<li><a href="aipge_2014.php" title="Achievement">Achievement</a></li>
<li><a href="photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>

<li><a href="javascript:void(0);" title="MCI SCREENING" onclick="innermenuslides(2);"><span>&nbsp;</span>MCI SCREENING</a>
<ul class="extra-inner" id="innrul2" style="display:none;">
<li><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="mci-satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="mci-test-series.php" title="Test Series">Test Series</a></li>
<li><a href="mciscreening_sep_2013.php" title="Achievement">Achievement</a></li>
<li><a href="mci-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>

<li><a href="javascript:void(0);" title="MDS QUEST" onclick="innermenuslides(3);"><span>&nbsp;</span>MDS QUEST</a>
<ul class="extra-inner" id="innrul3" style="display:none;">
<li><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="mds-satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="dams-mds-test-series.php" title="Test Series">Test Series</a></li>
<li><a href="mds-aipge-2014.php" title="Achievement">Achievement</a></li>
<li><a href="mds-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>
<li><a href="index.php" title="USMLE EDGE">USMLE EDGE</a></li>
</ul>
</li>

<li><a href="javascript:void(0);" title="DAMS Store" onclick="menuslides(3);"><span>&nbsp;</span>DAMS Store </a>
<ul class="inner-d-down" id="inul3" style="display:none;">
<li><span class="respo-arrow">&nbsp;</span></li>
<!--<li><a href="https://damsdelhi.com/dams-publication.php?c=1" title="MD / MS ENTRANCE">MD / MS ENTRANCE</a></li>
<li><a href="https://damsdelhi.com/dams-publication.php?c=2" title="MCI SCREENING">MCI SCREENING</a></li>
<li><a href="https://damsdelhi.com/dams-publication.php?c=3" title="MDS QUEST">MDS QUEST</a></li>
<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="USMLE EDGE">USMLE EDGE</a></li>-->
<li><a href="http://damspublications.com/" target="_blank" title="MD / MS ENTRANCE">MD / MS ENTRANCE</a></li>
<li><a href="http://damspublications.com/" target="_blank" title="MCI SCREENING">MCI SCREENING</a></li>
<li><a href="http://damspublications.com/" target="_blank" title="MDS QUEST">MDS QUEST</a></li>
<li><a href="http://damspublications.com/" target="_blank" title="USMLE EDGE">USMLE EDGE</a></li>
</ul>
</li>

<li><a href="javascript:void(0);" title="Find a Centre" onclick="menuslides(4);"><span>&nbsp;</span>Find a Centre</a>
<ul class="inner-d-down" id="inul4" style="display:none;">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="https://damsdelhi.com/find-center.php?c=1" title="FACE TO FACE CENTRE">FACE TO FACE CENTRE</a></li>
<li><a href="https://damsdelhi.com/find-center.php?c=2" title="SATELLITE CENTRE">SATELLITE CENTRE</a></li>
<li><a href="https://damsdelhi.com/find-center.php?c=3" title="TEST CENTRE">TEST CENTRE</a></li>
</ul>
</li>

<li><a href="https://www.damsdelhi.com/download.php" title="Download">Download</a></li>
<li><a href="https://www.damsdelhi.com/franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="https://www.damsdelhi.com/career.php" title="Career">Career</a></li>
<li><a title="Blog" href="https://blog.damsdelhi.com/">Blog</a></li>
<li><a href="https://www.damsdelhi.com/contact.php" title="Contact us">Contact us</a></li>

<li>
<div class="responc-social-icon">
<a href="https://www.facebook.com/damsdelhiho" target="_blank" title="Connect with us on Facebook" class="responc-l-facebook"><span>&nbsp;</span> Follow us on Facebook</a>
<a href="https://twitter.com/damsdelhi" target="_blank" title="DAMS on Twitter" class="responc-l-twitter"><span>&nbsp;</span> Follow us on Twitter</a>
<a href="https://in.linkedin.com/pub/dams-delhi/40/415/570" target="_blank" title="DAMS on Linkedin" class="responc-l-mail"><span>&nbsp;</span> Follow us on Linkedin</a>
<a href="https://www.youtube.com/user/damsdelhi" target="_blank" title="Watch DAMS Videos" class="responc-l-print"><span>&nbsp;</span> Connect With Us</a>
<a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" title="Follow DAMS on Google+" class="responc-l-share"><span>&nbsp;</span> Follow us on Google Plus</a>
</div>
</li>

<li>
<div class="res-helpline-no">
<span class="res-cell">&nbsp;</span> 
<div style="width:60%; float:left; text-align:left;">
Helpline&nbsp;no: 
<span class="res-mini-no">011-40094009</span>
</div>
</div>
</li>

<li>
<div class="responcive-sms-enquiry display_none">
<span class="res-sms">&nbsp;</span> 
<div style="width:60%; float:left; text-align:left;">
SMS Enquiry: <span class="iner-res-sms">sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span>
</div>
</div>
</li>
</ul>
</div>
</div>

<div class="user-use">
<div class="demo-box-d"><a href="javascript:void(0);" class="l-r-left demo-2-d" title="Online Test Student login"><span></span>
 <b style="font-weight:normal;">Log in</b></a>
</div>
<div class="demo6-d">
<div class="close-navigation demo-2-d">X</div>
<ul>
<li class="boder-none"><div class="modify-close">Close <span class="demo-2-d">X</span></div></li>
<li id="loginentry"><a href="javascript:void(0);" title="Online/Offline Test Login" class="active-res" id="login1" onclick="loginslides(1);">
<span>&nbsp;</span>Online/Offline Test Login</a>
<form id="loginFormnewresp" class="form" method="post" action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=submit" name="loginFormnewresp" onsubmit="return loginSubmit_resp();">
<div class="responcive-login-box" id="indiv1">
<div class="responcive-login-content">
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="email" id="emailLogin-resp" type="text" class="career-inp-1" placeholder="Roll No. / Email" /></div>
<span id="loginemailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="pass" id="passLogin-resp" type="password" class="career-inp-1" placeholder="Password" /></div>
<span id="loginpasserror-resp" class="errormsg" style="display:none">Please enter your Password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1">
<div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div>
<div class="responcive-fgpassword"><a href="javascript:void(0);" id="fg-password-resp" title="Forgot Password">Forgot Password ?</a></div>
</div></div>
<div class="responcive-career-box">
<div class="error_msg_box" id="loginresError" style="width:97%; margin-bottom:-15px; margin-top:10px;display:none">Please enter correct.</div>
</div>
</div></div>
</form>
</li>

<li id="forgtpasswd" style="display:none;"><a href="javascript:void(0);" title="Forgot Password" class="active-res" id="login3" onclick="loginslides(3);"><span>&nbsp;</span>Forgot Password</a>
<form action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=fgpassword" id="forgetpassformresp" class="form" method="post" name="forgetpassformresp" onsubmit="return forgetPasswordresp();">
<div class="responcive-login-box" id="indiv3">
<div class="responcive-login-content">
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="emaiID" id="fgemail-resp" type="text" class="career-inp-1" placeholder="Email Address" /></div>
<span id="fgemailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box"><div class="responcive-right-ip-1"><div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div></div></div>
<div class="responcive-career-box">
<div class="error_msg_box" id="fgerror" style="width:97%; margin-bottom:-15px; margin-top:10px;display:none">Please enter correct.</div>
</div>
</div></div>
</form>
</li>

<li><a href="javascript:void(0);" title="New Registration" id="login2" onclick="loginslides(2);"><span>&nbsp;</span>New Registration</a>
<form name="RegistrationFormresp" id="RegistrationFormresp" action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=inRegistration" method="post" onsubmit="return Submit_form_newReg_resp();">
<div class="responcive-login-box" id="indiv2" style="display:none;">
<div class="responcive-login-content">
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="email" id="email-resp" type="text" class="career-inp-1" placeholder="Email Address" /></div>
<span id="emailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="password" id="password-resp" type="password" class="career-inp-1" placeholder="Password" /></div>
<span id="passworderror-resp" class="errormsg" style="display:none">Please enter your password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="Cnpassword" ID="Cnpassword-resp" type="password" class="career-inp-1" placeholder="Confirm Password" />
</div><span id="Cnpassworderror-resp" class="errormsg" style="display:none">Please enter confirm password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="Stuidentname" ID="Stuidentname-resp" type="text" class="career-inp-1" placeholder="Name" /></div>
<span id="Stuidentnameerror-resp" class="errormsg" style="display:none">Please enter your name</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="mobile" id="mobile-resp" type="text" class="career-inp-1" placeholder="Mobile Number" /></div>
<span id="mobileerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1">
<select name="course_Data" id="Course-resp" class="career-select-input-1">
<option value="0">Select Course</option>
<?php $sql=mysql_query("SELECT COURSE_ID,COURSE_NAME FROM COURSE");
                                            while($row= mysql_fetch_array($sql)){?>
                                            <option value="<?php echo $row['COURSE_ID']; ?>">
                                           <?php  echo urldecode($row['COURSE_NAME']);?>
                                            </option>
                                            <?php } ?>
</select></div><span id="Courseerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><select name="state" id="state-resp" class="career-select-input-1" onchange="stateChangeResp(this.value)">
<option value="0">Select State</option>
<?php $sql=mysql_query("SELECT STATE_ID,STATE_NAME FROM STATE");
                                            while($row= mysql_fetch_array($sql)){?>
                                            <option value="<?php echo $row['STATE_ID']; ?>">
                                           <?php  echo urldecode($row['STATE_NAME']);?>
                                            </option>
                                            <?php } ?>
</select></div><span id="stateerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1" id="citydiv-resp">
<select name="city" id="city-resp" class="career-select-input-1">
<option value="0">Select City</option>
</select>
</div><span id="cityerror" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box"><div class="responcive-right-ip-1"><div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div></div></div>
<div class="responcive-career-box">
<div class="error_msg_box" id="regresError" style="width:97%; margin-bottom:-15px; margin-top:10px;display:none">Please enter correct.</div></div>
</div></div>
</form>
</li>

<li><a href="javascript:void(0);" title="DAMS Cloud Login" id="login4" onclick="loginslides(4);"><span>&nbsp;</span>DAMS Cloud Login</a>
<div class="responcive-login-box" id="indiv4" style="display:none;">
<div class="responcive-login-content">
<div class="responcive-career-box" id="iframebody2">
</div>
<div class="responcive-career-box"></div>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</header>
<!-- Header Css End Here -->
