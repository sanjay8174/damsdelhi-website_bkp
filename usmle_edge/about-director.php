<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS, AIPG(NBE/NEET) Pattern</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<?php include 'enquiry.php'; ?>

<?php include 'social-icon.php'; ?>

<?php include 'header.php'; ?>


<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article class="director-message">

<aside class="banner-left banner-left-postion">
<h2>Bringing Innovations in </h2>
<h3 class="page_title">PG Medical Education</h3>
</aside>
</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li style="background:none;"><a href="dams.php" title="About Us">About Us</a></li>
<li><a title="About Director" class="active-link">About Director</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>BE GUIDED BY THE BEST GUIDE IN THE BUSINESS - DAMS</h4>
<article class="showme-main">
<div class="about-content">

<span class="director-heading">Lead by known innovator &amp; prominent educationist</span>

<ul class="benefits">
<li><span></span> Test &amp; Discussion batches which are immensely popular in Delhi &amp; Mumbai, ever wondered who started them first- Dr Sumer Sethi, course specially designed for good students who want the extra-edge from DAMS. Many AIIMS toppers have been a result of this phenomenon.</li>
<li><span></span> Originator of the idea popularly called as Foundation course. Maximum number of students join DAMS in foundation courses, as the brain behind getting the students to study early is our promoter. The foundation course is his brain child.</li>
<li><span></span> USMLE Edge- integrated foundation course of PG and USMLE combined particularly suited for final year students who are still not decided on which way to go.</li>
<li><span></span> AIPG(NBE/NEET) Pattern Based test papers and online question bank called as Destination AIPG(NBE/NEET) Pattern Capsule. Unique offering by DAMS, brain child of Dr Sumer Sethi, first institute to familiarize students with AIPG(NBE/NEET) Pattern pattern questions.</li>
<li><span></span> Tablet based learning called as iDAMS, another paradigm shift in PG medical entrance preparation by Dr.Sumer Sethi taking PG coaching to remotest locations using tablets.</li>
<li><span></span> With his internet and web 2.0 knowledge, he has taken PG medical education by storm with unique uses of FACEBOOK, TWITTER AND DAMS BLOG. DAMS EXCLUSVE CLUB on Facebook is immensely popular amongst students. DAMS is the coaching institute of YOUR GENERATION.</li>
<li><span></span> His book called Review of Radiology which came out in 2003 was first Review book in India written by an expert and took the market by storm; many people followed his idea of creating Review book by subject matter experts.</li>
<li><span></span> To his claim is also India's Premier Teleradiology Firm   called as Teleradiology Providers, which is one of the pioneer in India in providing remote radiology reads.</li>
<li><span></span> He is also credited with world first and longest running Radiology Blog, critically acclaimed world over. </li>
<li><span></span> We have now connected satellite based class to help students in turns with no air connecting.</li>
</ul>
<span class="director-heading">
<strong>Board of Director</strong><br>
Dr. Sumer Sethi <br>
Dr. Deepti Sethi<br>
Dr. Rajiv Bhagi<br>
Dr. Ridhi Bhagi<br>
</span>
</div>

</article></div>

</aside>
<aside class="gallery-right">
<div id="store-wrapper">
<div class="dams-store-link"><span></span>About US</div>
<div class="dams-store-content">
<div class="inner-store">
<ul>
<li style="border:none;"><a href="dams.php" title="About DAMS">About DAMS</a></li>
<li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
<li><a href="about_director.php" title="About Director" class="active-store">About Director</a></li>
<li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
</ul>
</div></div>
</div>


<!--for Enquiry popup  -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry popup  -->
</aside>
</section>
</div> 
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->
</body></html>