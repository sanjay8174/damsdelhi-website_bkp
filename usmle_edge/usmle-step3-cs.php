<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, USMLE EDGE</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
          <li><a title="USMLE Step 3 Cs" class="active-link">USMLE Step 3 Cs</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>USMLE Step-3 CS</h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>STEP 3 CS</span>
                  <p>USMLE Step 3 is a two-day exam. The exam is usually taken by IMGs during the residency training program (Exception: Step 3 must be taken before the match in order to be eligible for an H-1B visa). It assesses whether you can apply the medical knowledge and understanding of basic and clinical science considered essential for the unsupervised practice of medicine. It emphasizes on patient management in ambulatory settings. The exam stresses on physicians tasks and clinical situations during initial patient evaluations, continuing care, and emergency. </p>
                  <p>This exam is administered by the medical board in each state. You must complete each day of testing within 8 hours. The first day of testing includes approximately 350 multiple-choice questions divided into blocks of 25 to 50 questions that have to be completed within 30 to 60 minutes. There is a maximum of 7 hours of testing on the first day, plus 45 minutes of break time and a 15-minute optional tutorial. The second day of testing includes approximately 150 multiple-choice questions and computer-based case simulations (CCS). The questions are divided into sets of 25 to 50 questions, each set takes from 30 to 60 minutes and must be completed within 3 hours. After answering the multiple choice questions there is a CCS tutorial for which a maximum of 30 minutes is allowed. The CCS tutorial is followed by approximately 9 case simulations using a new, proprietary simulation software called PRIMUM. The case simulations are presented in 1 or more cases per block and 3 hours, 45 minutes are allotted for them. The allotted break time for the second day is also 45 minutes.</p>
                </div>
                <ul class="dnb-list">
                  <h5>CONTENTS :-</h5>
                  <h5 style="padding:5px 0px;">Physician Task</h5>
                  <p>8 –12 % Obtaining History and Performing Physical Examination</p>
                  <p>8 –12 % Using Laboratory and Diagnostic Studies</p>
                  <p>8 –12 % Formulating Most Likely Diagnosis</p>
                  <p>8 –12 % Evaluating Severity of Patient’s Problems</p>
                  <p>8 –12 % Applying Scientific Concepts and Mechanisms of Disease</p>
                  <p>45-55 % Managing the Patient</p>
                  <li><span>&nbsp;</span>Health Maintenance.</li>
                  <li><span>&nbsp;</span>Clinical Intervention.</li>
                  <li><span>&nbsp;</span>Clinical Therapeutics.</li>
                  <li><span>&nbsp;</span>Legal and Ethical Issues.</li>
                  <h5 style="padding:5px 0px;">Clinical Encounter</h5>
                  <p>20 – 30% Initial Workups.</p>
                  <p>55 – 65 % Continued Care.</p>
                  <p>10 – 20 % Emergency Care.</p>
                </ul>
                <div class="franchisee-box"> <span>Computer-based Case Simulations (CCS) :-</span>
                  <p>CCS allows you to provide care for a simulated patient. You must balance the clinical information available with evidence of the acuity of the clinical problem in deciding what treatment to begin and when, monitoring the patient’s response appropriately throughout.</p>
                  <p>In CCS, you may request information from the patient’s history and physical examination, order lab studies, procedures and consultants, and start medications and other therapies. When there is nothing further you wish to do, you decide when to re-evaluate the patient by clicking on the clock to advance time. With the passage of time, the patient's condition changes based upon the underlying problem and your interventions. You cannot go back in time, but you can change orders to reflect an updated patient management plan. You can review vital signs, progress notes, nurses’ notes, test results and reports from your orders. You may see and move the patient among the office, home, emergency department, intensive care and ward settings.</p>
                  <p>After you complete the cases for a group of patients, your management strategy will be compared with those of experts. Actions closer to the ideal will produce a higher score.</p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>DAMS USMLE Edge</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li class="border_none"><a href="usml-step1.php" title="Step1">Step1</a></li>
                  <li><a href="usml-step2.php" title="Step2">Step2</a></li>
                  <li><a href="usmle-step2-cs.php" title="Step2 CS">Step2 CS</a></li>
                  <li><a href="usmle-step3-cs.php" title="Step3" class="active-store">Step3</a></li>
                </ul>
              </div>
            </div>
          </div>
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>