<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 1;
$courseNav_id = 1;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="test-series">
<?php include 'md-ms-big-nav.php'; ?>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li class="bg_none"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Test Series" class="active-link">Test Series</a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading paddin-zero">
<h4>Offline Test Series <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
<article class="showme-main">
<div class="test-series-content paddin-zero">
<ul class="duration-content">
<li><label>Duration :</label> <span>1 Year</span></li>
<li><label>Total no of tests :</label> <span>Approximately 40 Tests</span></li>
<li><label>Total no. of questions per Test :</label> <span style="display:inline-block; vertical-align:top; float:right; width:69%;">300 in GRAND TESTS &amp; 200 IN SUBJECTWISE TESTS, WITH SPECIAL MOCK TEST FOR AIIMS, AIPG (NBE), PGI , NIMHANS, STATE PGs</span></li>
</ul>
<ul class="some-points">
<li><span class="blue_arrow"></span> <span>Authentic explanations from our highly prestigious faculty with references.</span></li>
<li><span class="blue_arrow"></span> <span>Questions are based on AIPG (NBE PATTERN) AIPG(NBE/NEET) PatternPG, AIIMS, PGI  DNB, UPSC exam pattern.</span></li>
<li><span class="blue_arrow"></span> <span>We have various Test Centres for more competitive environment &amp; comfortable approach.</span></li>
<li><span class="blue_arrow"></span> <span>Fully explained solved answers key of all Questions will be give immediately after completion</span></li>
<li><span class="blue_arrow"></span> <span>All Students enrolled in between the session will get all previous Test papers.</span></li>
<li><span class="blue_arrow"></span> <span>All India Ranking with detailed marks will be provided at our website: <a href="https://www.damsdelhi.com" title="PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG">www.damsdelhi.com</a></span></li>
</ul>
<p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.<br><br></p>
</div>
<ul class="idTabs"> 
<li><a href="#test1">Test Series - I</a></li>
<li><a href="#test2">Test Series - II</a></li>
<li><a href="#test3">Test Series - III</a></li>
<li><a href="#test4">Test Series - IV</a></li>
</ul>
<div id="test1">
<div class="test-tab-content">
<a href="downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>9<sup>th </sup>Feb, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>16<sup>th </sup>Feb, 2014</label> <span>Grand Test</span></li>
<li><label>23<sup>rd </sup>Feb, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>2<sup>nd </sup>Mar, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>9<sup>th </sup>Mar, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>16<sup>th </sup>Mar, 2014</label> <span>Grand Test</span></li>
<li><label>23<sup>rd </sup>Mar, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>30<sup>th </sup>Mar, 2014</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>AIIMS Mock Test</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>PGI Mock Test</span></li>
<li><label>12<sup>th </sup>May, 2014</label> <span>Medicine-I</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Oncology</span></li>
</ul>
</div>
</div></div>
<div id="test2">
<div class="test-tab-content">
<a href="downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>9<sup>th </sup>Mar, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>16<sup>th </sup>Mar, 2014</label> <span>Grand Test</span></li>
<li><label>23<sup>rd </sup>Mar, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>30<sup>th </sup>Mar, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>AIIMS Mock Test</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>PGI Mock Test</span></li>
<li><label>12<sup>th </sup>May, 2014</label> <span>Medicine-I</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Oncology</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>31<sup>st </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
</ul>
</div>
</div></div>
<div id="test3">
<div class="test-tab-content">
<a href="downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>6<sup>th </sup>Apr, 2014</label> <span>Opthalmology</span></li>
<li><label>13<sup>th </sup>Apr, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>20<sup>th </sup>Apr, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Apr, 2014</label> <span>AIIMS Mock Test</span></li>
<li><label>4<sup>th </sup>May, 2014</label> <span>PGI Mock Test</span></li>
<li><label>12<sup>th </sup>May, 2014</label> <span>Medicine-I</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Oncology</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>31<sup>st </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>7<sup>th </sup>Sep, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>14<sup>th </sup>Sep, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>21<sup>st </sup>Sep, 2014</label> <span>Grand Test</span></li>
<li><label>28<sup>th </sup>Sep, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
</ul>
</div>
</div></div>
<div id="test4">
<div class="test-tab-content">
<a href="downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>12<sup>th </sup>May, 2014</label> <span>Medicine-I</span></li>
<li><label>18<sup>th </sup>May, 2014</label> <span>Medicine-II</span></li>
<li><label>25<sup>th </sup>May, 2014</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>1<sup>st </sup>Jun, 2014</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>8<sup>th </sup>Jun, 2014</label> <span>Surgery-I</span></li>
<li><label>15<sup>th </sup>Jun, 2014</label> <span>Grand Test</span></li>
<li><label>22<sup>nd </sup>Jun, 2014</label> <span>Surgery-II</span></li>
<li><label>29<sup>th </sup>Jun, 2014</label> <span>Orthopedic and Radiology</span></li>
<li><label>6<sup>th </sup>Jul, 2014</label> <span>Dermatology-Psychiatry</span></li>
<li><label>13<sup>th </sup>Jul, 2014</label> <span>Paeds-I</span></li>
<li><label>20<sup>th </sup>Jul, 2014</label> <span>Grand Test</span></li>
<li><label>27<sup>th </sup>Jul, 2014</label> <span>Paeds-II</span></li>
<li><label>3<sup>rd </sup>Aug, 2014</label> <span>Oncology</span></li>
<li><label>10<sup>th </sup>Aug, 2014</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>17<sup>th </sup>Aug, 2014</label> <span>Grand Test</span></li>
<li><label>24<sup>th </sup>Aug, 2014</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li><label>31<sup>st </sup>Aug, 2014</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>7<sup>th </sup>Sep, 2014</label> <span>Pathology including flow Cytometry</span></li>
<li><label>14<sup>th </sup>Sep, 2014</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li><label>21<sup>st </sup>Sep, 2014</label> <span>Grand Test</span></li>
<li><label>28<sup>th </sup>Sep, 2014</label> <span>Microbiology including Microbial Biodegradation</span></li>
<li><label>5<sup>th </sup>Oct, 2014</label> <span>Opthalmology</span></li>
<li><label>12<sup>th </sup>Oct, 2014</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li><label>19<sup>th </sup>Oct, 2014</label> <span>Grand Test</span></li>
</ul>
</div>
</div></div> 
</article>
</div>
</aside>
<aside class="gallery-right">
<?php include 'md-ms-right-accordion.php'; ?>
<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>