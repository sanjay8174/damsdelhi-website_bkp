<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
include 'config/constant.php';
$pathCMS = constant::$pathCMS;
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>USMLE EDGE Coaching Institute, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
<!--      <div  class="bannerlfthead"style=""> 
	   <p>Live Usmle courses first time in India, being launched on <span style="font-size:22px;  font-family: sans-serif;  font-weight: 600;">8<sup>th</sup> August.</span> </p>
	   <p> <span style="font-weight: 600;">It is an 16 weeks course only on weekends.</span></p> 
	   <p>Handouts and question bank will be provide along with complete counselling for Usmle .</p> 
       <p>Only counselling and Question bank packages also available . Please call for more details.</p>
	   <span class="book-ur-seat-btn1"><a href="https://192.168.0.250/damswebsiteNew/usmle_edge/online-registration.php" title="Book Your Seat"> Book Your Seat</a></span>
	</div>-->
        
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>USMLE  Introduction <span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
        <p>USMLE- The United States Medical Licensing</p>
        <p>USMLE is a three-step examination for medical licensure in the United States and is sponsored by the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners NBME. Need to take Step 1 , 2CK and CS to obtain a certification to apply for a residency in the US.<br> </p>
         <div class="franchisee-box">
              <h3 style="font-size:18px;padding: 15px 0px 10px 0px;">Three parts:</h3>
                   <ul class="course-new-list1 lineheight"style="line-height:40px;">
                          <li><span class="sub-arrow"></span>Step 1 (Preclinical MCQ) – 8 hours</li>
                          <li><span class="sub-arrow"></span>Step 2
                              <ul  class="course_detail">
                                  <li>Step 2 CK (Clinical MCQ) – 9 hours</li>
                                  <li>Step 2 CS (Clinical OSCE) – 8 hours (Only in the USA)</li>
                              </ul>
                          </li>
                           <li><span class="sub-arrow"></span>ECFMG Certification<sup>*</sup></li>
                           <li><span class="sub-arrow"></span>Step 3 – 16 hours (Only in the USA)
                               <ul>
                                   <li>To be taken after you start working, but can be taken after you are ECFMG certified</li>
                               </ul>
                           </li>
                           
                   </ul>
              </div>
      </div>

      <?php include 'usmle-middle-accordion_1.php'; ?>
    </aside>
    <aside class="content-right">
      <?php //include 'dams-usmle-edge.php'; ?>
      <?php
    include 'openconnection.php';
    $count = 0;
    $i = 0;
    $sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
    while ($row = mysql_fetch_array($sql)) {
        $newsDetail[$count] = urldecode($row['HEADING']);
        $count++;
    }
?>

      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block">
            <a target="_blank" href="https://www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent"><img class="border_none" src="<?php echo $pathCMS.'/youTubeThumbs/z_xgJNXaWuQ.jpg' ?>" width="100%" height="236" alt="z_xgJNXaWuQ.jpg" frameborder="0"></a>
            <!--<iframe width="100%" height="236" src="//www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent" class="border_none"></iframe>-->
          </div>
          <div id="vd1" class="display_none">
             <a target="_blank" href="https://www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent"><img class="border_none" src="<?php echo $pathCMS.'/youTubeThumbs/VRJ89h2DkS0.jpg' ?>" width="100%" height="236" alt="z_xgJNXaWuQ.jpg" frameborder="0"></a>
            <!--<iframe width="100%" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent" class="border_none"></iframe>-->
          </div>
          <div id="vd2" class="display_none">
              <a target="_blank" href="https://www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent"><img class="border_none" src="<?php echo  $pathCMS.'/youTubeThumbs/VRJ89h2DkS0.jpg' ?>" width="100%" height="236" alt="z_xgJNXaWuQ.jpg" frameborder="0"></a>
            <!--<iframe width="100%" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent" class="border_none"></iframe>-->
          </div>
          <div id="vd3" class="display_none">
            <a target="_blank" href="https://www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent"><img class="border_none" src="<?php echo $pathCMS.'/youTubeThumbs/z_xgJNXaWuQ.jpg' ?>" width="100%" height="236" alt="z_xgJNXaWuQ.jpg" frameborder="0"></a>
            <!--<iframe width="100%" height="236" src="//www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent" class="border_none"></iframe>-->
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
   <aside class="content-left" style="margin-top:10px;width: 100%">
        <div class="course-box">
         <p><b>Disclaimer</b>
         <br>
         <p style="text-align: justify;font-size: 12px">USMLE® is a joint program of the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME). The ECFMG® is a registered trademark of the Educational Commission for Foreign Medical Graduates. The Match® is a registered service mark of the National Resident Matching Program® (NRMP®). The NRMP is not affiliated with DAMS. Electronic Residency Application Service (ERAS®) is a program of the association of American Medical Colleges and is not affiliated with DAMS. Test names and other trademarks are the property of the respective trademark holders. None of the trademark holders are affiliated with DAMS or this website.<br>
        </div>
      </aside>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php  include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>