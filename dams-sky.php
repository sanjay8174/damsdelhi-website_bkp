<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <aside class="banner-right">
        <!--<div class="banner-right-btns"> <a href="dams-store.php" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
       <div class="banner-right-btns"> <a href="https://www.damspublications.com/" target="_blank" title="DAMS Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="DAMS Sky" class="active-link">DAMS Sky</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>DAMS-SKY  Best teachers at your doorstep </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>India's First Satellite Based PG Medical Classes</span>
                  <p>Ancient times Guru Shishya Pranali of imparting education was by the Guru at Guru's premises /Ashrams. Ages passed, social pattern changed, villages turned to towns, towns to cities & metros. Civilization progressed with "Education". For good education, students have to migrate to better places & this is the history as of yesterday. Now technology development removed these constraints and barriers. The Best Education of metros is available in House anywhere including the remote area. Quality education by teaching from the renowned professors and faculty is available at recipient's facilities. Best education at environment of individual's choice.</p>
                  <p>Medical Tele-Education is the Brain Child of Dr Sumer Sethi, our Director, widely known for his vision and innovations in field of PG Medical Education vertical. He is also one of the pioneers in field of Teleradiology & Tablet based learning programme called as iDAMS. </p>
                </div>
                <div class="idams-box"> <span>Concept of Satellite Classes</span>
                  <p>One acute problem faced by the coaching class industry, is paucity of competent faculty. This constraint imposes limitations on expansion beyond geographical boundaries. Satellite Learning Programme addresses this problem effectively without losing quality of coaching. An out of box Learning Experience; Satellite Learning Programme has two way audio video facilities consisting of three major elements:</p>
                  <ul class="franchisee-list">
                    <li><span>&nbsp;</span>Teaching End.</li>
                    <li><span>&nbsp;</span>Remote Learning Centers called "Classrooms".</li>
                    <li><span>&nbsp;</span>The Satellite.</li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div class="enquiry-main">
            <div class="enquiry-heading">Enquiry Form</div>
            <div class="enquiry-content-main">
              <div class="enquiry-content">
                <div class="enquiry-content-more">
                  <form action="" method="get">
                    <input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
                    <input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
                    <input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />
                    <select name="" class="select-input">
                      <option value="0">Select Course</option>
                      <option>MD/MS Entrance</option>
                      <option>MCI Screening</option>
                      <option>MDS Quest</option>
                      <option>USMLE Edge</option>
                    </select>
                    <select name="" class="select-input">
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                    </select>
                    <textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
                    <div class="submit-enquiry"><a href="#" title="Submit"><span></span> Submit</a></div>
                  </form>
                </div>
              </div>
              <div class="enquiry-bottom"></div>
            </div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>