<!DOCTYPE html>
<script>
        location.href = "https://www.idams.damsdelhi.com";
    </script>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>iDAMS, PG Medical Entrance Coaching institute, AIPG(NBE/NEET) PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article  class="dams-store-idams">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="iDAMS" class="active-link">iDAMS</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4 class="idams-heading">DAMS is proud to share that our Tablet based course<br>
              "iDAMS" has been awarded as the<br>
              "Best Tablet Based Medical Education Programme"<br>
              in India in the "Education Excellence Awards-2013"</h4>
            <article class="showme-main">
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Special Lectures in iDAMS</div>
              </div>
              <div class="idams-content">
                <div class="idams-box"> <span>Another Pioneering Effort By the Number 1 Institution in the country</span>
                  <p>First Time in India, after having many Firsts to our credit including Test &amp; Discussion Series, Bounce Back Series, regular classes, GT series, Foundation and Prefoundation Courses, NEET Mocks, First Step - DAMS the Pioneers in the PG Medical Entrance Coaching, have added another ACE in their courses-The iDAMS. This product was launched on 25th April 2011 in Delhi Head office with more than 1000 eager PG aspirants in the audience by our lead Educationist &amp; Visionary–Dr Sumer Sethi, Radiologist and Director, DAMS. iDAMS Program is a tablet based complete distance learning product that combines the best of classroom learning and self-study. iDAMS program gives access to real time video lectures along with test papers to test your skills. Combining this Tablet with DAMS facebook club and other postal options provides access to famous and learned educationists at DAMS while remaining at your home, which was not possible so far with any of the technologies.</p>
                </div>
                <div class="idams-box"> <span>BE SMART &amp; TAKE YOUR FUTURE IN YOUR HAND</span>
                  <p>With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</p>
                </div>
                <div class="idams-box"> <span>THE NEW GOLD STANDARD DISTANT LEARNING PROGRAMME</span>
                  <p>iDAMS (tablet course) + Membership to DAMS EXCLUSIVE CLUB on facebook + Unique Online GT series+ Subject wise series (online)+Updated DAMS Study Material(optional) “If you cannot come to DAMS-DAMS will come to you”</p>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Benefits of iDAMS:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>The content of iDAMS, study material and video lectures, has been designed and developed by a handpicked team of academic experts. The product provides, ready access to special MCQ based sessions by experts. A student can avail the advantage of learning from the best team that is known for producing amazing results year after year through a series of interactive tools and a personalized feedback system.</li>
                    <li><span>&nbsp;</span>It is a platform which enables students to learn anywhere, anytime.</li>
                    <li><span>&nbsp;</span>There is no need to travel to the local teacher for doubts. Now concepts can be reviewed sitting at home with the help of DAMS EXCLUSIVE CLUB on facebook.</li>
                  </ul>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Technical Features:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>Android as operating system</li>
                    <li><span>&nbsp;</span>Easy to internet surfing</li>
                    <li><span>&nbsp;</span>Good speed volume</li>
                    <li><span>&nbsp;</span>Wi-Fi connectivity</li>
                    <li><span>&nbsp;</span>Bluetooth connectivity</li>
                    <li><span>&nbsp;</span>32 GB Memory card inserted</li>
                  </ul>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Accessories:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>USB Cable</li>
                    <li><span>&nbsp;</span>Ear phones</li>
                    <li><span>&nbsp;</span>Charger</li>
                    <li><span>&nbsp;</span>Connector</li>
                  </ul>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Course includes:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>Tests &amp; explanations (19 subjects)</li>
                    <li><span>&nbsp;</span>Audio / Video lectures</li>
                  </ul>
                  <p> Also includes Grand test &amp; Subject wise test series (online)+ Recent updates &amp; Last look revision booklets<br>
                    <strong>Charges Rs.32,500/- (including courier charges)</strong> Book now.<br>
                    Now you can also make an online payment by visit our website <a href="www.damsdelhi.com">www.damsdelhi.com</a> or can send dd in favor of “Delhi Academy of Medical Sciences Pvt. Ltd” payable at “New Delhi”. </p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <div class="content-right-div">
          <div id="store-wrapper">
            <div class="dams-store-block">
              <div class="dams-store-heading"><span></span>CATEGORIES</div>
              <div class="dams-store-accordion">
                <div class="dams-inner-store">
                  <ul>
                    <?php
include 'openconnection.php';
  $sql=mysql_query("select * FROM COURSE WHERE ACTIVE=1");
    $i=0;
while($row=  mysql_fetch_object($sql)){ ?>
                    <li id="lis<?php echo $row->COURSE_ID; ?>" onClick="accordion1(<?php echo $row->COURSE_ID; ?>)" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="background:url(images/minus-icon.png) 12px 6px no-repeat;border:none;cursor:pointer"<?php } else{ ?>style="background:url(images/plus-icon.png) 12px 6px no-repeat;border:none;cursor:pointer"<?php } ?>><a title="<?php echo urldecode($row->COURSE_NAME); ?>" ><?php echo urldecode($row->COURSE_NAME); ?></a></li>
                    <div class="inner-store" id="d<?php echo $row->COURSE_ID; ?>" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="display:block"<?php } ?>>
                      <ol>
                        <?php $sql1=mysql_query("select * FROM PRODUCT_CATEGORY WHERE COURSE_ID='$row->COURSE_ID' AND ACTIVE=1 ORDER BY PRODUCT_CATEGORY_ID");
$k=1;while($rows=  mysql_fetch_object($sql1)){ ?>
                        <li style="cursor:pointer"><a id="category<?php echo $row->COURSE_ID.".".$k; ?>" onclick="location.href='dams-publication.php?c=<?php echo $row->COURSE_ID; ?>'"><?php echo urldecode($rows->PRODUCT_CATEGORY_NAME); ?></a></li>
                        <?php $k++; } ?>
                        <input type="hidden" value="<?php echo $k; ?>" id="totalCategory<?php echo $row->COURSE_ID; ?>" />
                      </ol>
                    </div>
                    <?php $i++; } ?>
                    <input type="hidden" value="<?php echo $i; ?>" id="totalCourse" />
                  </ul>
                </div>
              </div>
            </div>
          </div>          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </div>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
