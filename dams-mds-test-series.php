<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$course_id = 3;
$courseNav_id = 9;
?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="test-series">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br> take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest">MDS Quest</a></li>
          <li><a title="MDS Test Series" class="active-link">MDS Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>MDS Test Series - 2014</h4>
            <div class="test-series-content paddin-zero">
              <ul class="duration-content">
                <li>
                  <label>Price :</label>
                  <span class="price_font_block">6000/-
                   <span class="including_tax_inline">(including s.tax)</span>
                  </span></li>
                <li>
                  <label>Dental Career Counselling for MDS :</label>
                  <span class="price_font">09999158131, 09999322163</span></li>
              </ul>
            </div>
            <div class="showme-main book-hide test_series_inline">
              <ul class="idTabs mini-tab">
                <li><a href="#test1">Test Series - I</a></li>
                <li><a href="#test2">Test Series - II</a></li>
                <li><a href="#test3">Test Series - III</a></li>
                <li><a href="#test4">Test Series - IV</a></li>
                <li><a href="#test5">Test Series - V</a></li>
              </ul>
              <div id="test1">
                <div class="test-tab-content">
                  <a href="downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>9<sup>th </sup>Feb, 2014</label>
                        <span>Endodontics &amp; Conservative and related Dental Materials</span></li>
                      <li>
                        <label>16<sup>th </sup>Feb, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>23<sup>rd </sup>Feb, 2014</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>2<sup>nd </sup>Mar, 2014</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>9<sup>th </sup>Mar, 2014</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li>
                        <label>16<sup>th </sup>Mar, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>23<sup>rd </sup>Mar, 2014</label>
                        <span>Oral pathology(OMDR) 1</span></li>
                      <li>
                        <label>30<sup>th </sup>Mar, 2014</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>6<sup>th </sup>Apr, 2014</label>
                        <span>General medicine + General  Surgery</span></li>
                      <li>
                        <label>13<sup>th </sup>Apr, 2014</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>20<sup>th </sup>Apr, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Apr, 2014</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>4<sup>th </sup>May, 2014</label>
                        <span>AIIMS Mock Test</span></li>
                      <li>
                        <label>11<sup>th </sup>May, 2014</label>
                        <span>AIIMS EXAM</span></li>
                      <li>
                        <label>18<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>25<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                        <label>1<sup>st </sup>Jun, 2014</label>
                        <span>PGI Mock Test</span></li>
                      <li>
                        <label>8<sup>th </sup>Jun, 2014</label>
                        <span>PGI EXAM</span></li>
                      <li>
                        <label>15<sup>th </sup>Jun, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Jun, 2014</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>29<sup>th </sup>Jun, 2014</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                        <label>6<sup>th </sup>Jul, 2014</label>
                        <span>Prosthodontics 2 and related  Dental Materials</span></li>
                      <li>
                        <label>13<sup>nd </sup>Jul, 2014</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>20<sup>th </sup>Jul, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Jul, 2014</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>3<sup>rd </sup>Aug, 2014</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test2">
                <div class="test-tab-content">
                  <a href="downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>9<sup>th </sup>Mar, 2014</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li>
                        <label>16<sup>th </sup>Mar, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>23<sup>rd </sup>Mar, 2014</label>
                        <span>Oral pathology(OMDR) 1</span></li>
                      <li>
                        <label>30<sup>th </sup>Mar, 2014</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>6<sup>th </sup>Apr, 2014</label>
                        <span>General medicine + General  Surgery</span></li>
                      <li>
                        <label>13<sup>th </sup>Apr, 2014</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>20<sup>th </sup>Apr, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Apr, 2014</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>4<sup>th </sup>May, 2014</label>
                        <span>AIIMS Mock Test</span></li>
                      <li>
                        <label>11<sup>th </sup>May, 2014</label>
                        <span>AIIMS EXAM</span></li>
                      <li>
                        <label>18<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>25<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                        <label>1<sup>st </sup>Jun, 2014</label>
                        <span>PGI Mock Test</span></li>
                      <li>
                        <label>8<sup>th </sup>Jun, 2014</label>
                        <span>PGI EXAM</span></li>
                      <li>
                        <label>15<sup>th </sup>Jun, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Jun, 2014</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>29<sup>th </sup>Jun, 2014</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                        <label>6<sup>th </sup>Jul, 2014</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                        <label>13<sup>th </sup>Jul, 2014</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>20<sup>th </sup>Jul, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Jul, 2014</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>3<sup>rd </sup>Aug, 2014</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li>
                        <label>10<sup>th </sup>Aug, 2014</label>
                        <span>Endodontics &amp; Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>17<sup>th </sup>Aug, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>24<sup>th </sup>Aug, 2014</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>31<sup>st </sup>Aug, 2014</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test3">
                <div class="test-tab-content">
                  <a href="downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>6<sup>th </sup>Apr, 2014</label>
                        <span>General medicine + General  Surgery</span></li>
                      <li>
                        <label>13<sup>th </sup>Apr, 2014</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>20<sup>th </sup>Apr, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Apr, 2014</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>4<sup>th </sup>May, 2014</label>
                        <span>AIIMS Mock Test</span></li>
                      <li>
                        <label>11<sup>th </sup>May, 2014</label>
                        <span>Tentative Date for AIIMS EXAM</span></li>
                      <li>
                        <label>18<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>25<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                        <label>1<sup>st </sup>Jun, 2014</label>
                        <span>PGI Mock Test</span></li>
                      <li>
                        <label>8<sup>th </sup>Jun, 2014</label>
                        <span>PGI EXAM</span></li>
                      <li>
                        <label>15<sup>th </sup>Jun, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Jun, 2014</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>29<sup>th </sup>Jun, 2014</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                        <label>6<sup>th </sup>Jul, 2014</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                        <label>13<sup>th </sup>Jul, 2014</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>20<sup>th </sup>Jul, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Jul, 2014</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>3<sup>rd </sup>Aug, 2014</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li>
                        <label>10<sup>th </sup>Aug, 2014</label>
                        <span>Endodontics &amp; Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>17<sup>th </sup>Aug, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>24<sup>th </sup>Aug, 2014</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>31<sup>st </sup>Aug, 2014</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>7<sup>th </sup>Sep, 2014</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li>
                        <label>14<sup>th </sup>Sep, 2014</label>
                        <span>Oral pathology (OMDR) 1</span></li>
                      <li>
                        <label>21<sup>th </sup>Sep, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>28<sup>st </sup>Sep, 2014</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test4">
                <div class="test-tab-content">
                  <a href="downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>4<sup>th </sup>May, 2014</label>
                        <span>AIIMS Mock Test</span></li>
                      <li>
                        <label>11<sup>th </sup>May, 2014</label>
                        <span>AIIMS EXAM</span></li>
                      <li>
                        <label>18<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>25<sup>th </sup>May, 2014</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                        <label>1<sup>st </sup>Jun, 2014</label>
                        <span>PGI Mock Test</span></li>
                      <li>
                        <label>8<sup>th </sup>Jun, 2014</label>
                        <span>PGI EXAM</span></li>
                      <li>
                        <label>15<sup>th </sup>Jun, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Jun, 2014</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>29<sup>th </sup>Jun, 2014</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                        <label>6<sup>th </sup>Jul, 2014</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                        <label>13<sup>th </sup>Jul, 2014</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>20<sup>th </sup>Jul, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Jul, 2014</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>3<sup>rd </sup>Aug, 2014</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li>
                        <label>10<sup>th </sup>Aug, 2014</label>
                        <span>Endodontics &amp; Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>17<sup>th </sup>Aug, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>24<sup>th </sup>Aug, 2014</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>31<sup>st </sup>Aug, 2014</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>7<sup>th </sup>Sep, 2014</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li>
                        <label>14<sup>th </sup>Sep, 2014</label>
                        <span>Oral pathology (OMDR) 1</span></li>
                      <li>
                        <label>21<sup>st </sup>Sep, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>28<sup>th </sup>Sep, 2014</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>5<sup>th </sup>Oct, 2014</label>
                        <span>General medicine + General Surgery</span></li>
                      <li>
                        <label>12<sup>th </sup>Oct, 2014</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>19<sup>th </sup>Oct, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>26<sup>th </sup>Oct, 2014</label>
                        <span>General Pathology + Microbiology</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test5">
                <div class="test-tab-content">
                  <a href="downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>15<sup>th </sup>Jun, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Jun, 2014</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>29<sup>th </sup>Jun, 2014</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                        <label>6<sup>th </sup>Jul, 2014</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                        <label>13<sup>th </sup>Jul, 2014</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>20<sup>th </sup>Jul, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Jul, 2014</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>3<sup>rd </sup>Aug, 2014</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li>
                        <label>10<sup>th </sup>Aug, 2014</label>
                        <span>Endodontics &amp; Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>17<sup>th </sup>Aug, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>24<sup>th </sup>Aug, 2014</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>31<sup>st </sup>Aug, 2014</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>7<sup>th </sup>Sep, 2014</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li>
                        <label>14<sup>th </sup>Sep, 2014</label>
                        <span>Oral pathology (OMDR) 1</span></li>
                      <li>
                        <label>21<sup>st </sup>Sep, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>28<sup>th </sup>Sep, 2014</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>5<sup>th </sup>Oct, 2014</label>
                        <span>General medicine + General Surgery</span></li>
                      <li>
                        <label>12<sup>th </sup>Oct, 2014</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>19<sup>th </sup>Oct, 2014</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>26<sup>th </sup>Oct, 2014</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>2<sup>nd </sup>Nov, 2014</label>
                        <span>AIIMS Mock Test</span></li>
                      <li>
                        <label>9<sup>th </sup>Nov, 2014</label>
                        <span>AIIMS EXAM</span></li>
                      <li>
                        <label>16<sup>th </sup>Nov, 2014</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>23<sup>rd </sup>Nov, 2014</label>
                        <span>Oral Surgery 2</span></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry -->
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

</body>
</html>