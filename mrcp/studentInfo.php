<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS - PG Medical Coaching, NEET PG</title>
<meta name="description" content=" Address and telephone number of Delhi Academy of Medical Sciences (DAMS) one of the best pg medical coaching in New Delhi, India." />
<meta name="keywords" content=" DAMS contact, DAMS Address, PG Medical Coaching Contact, PG Medical Coaching Adddress " />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php error_reporting(0);?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="contact-banner"> 
<aside class="banner-left">
<h2>Reach out to us to be guided by the</h2>
<h3>Best team of faculty and motivators</h3>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<section class="event-container">
    <aside class="gallery-left" style="width: 100%;">
<div style="display: inline-block; width: 100%;">
        <span style="display: inline-block; width: 100%;" display:inline-block;="">
            <h1 style="color: rgb(0, 166, 81); font-size: 30px; font-family: &quot;pt_sansbold&quot;; font-weight: 400;">Student Information</h1>
            <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; float: left; margin: 10px; line-height: 20px; font-family: &quot;pt_sansregular&quot;;">Students please insist on seeking a fees receipt for every payment. DAMS genuine fees receipts can only be in the following format:</p>
        </span>
        <span style="display: inline-block; width: 100%;">
            <h1 style="color: rgb(0, 166, 81); font-size: 30px; width: 100%; font-family: &quot;pt_sansbold&quot;; font-weight: 400;">DAMS ERP generated fee receipt:</h1>
            <img src="images/feereceipt.jpg" style="float: left; right: 25px; margin: 26px 12px; width: 74%;">
            </span>
        <span style="display: inline-block; margin: 10px; width: 100%;">
        <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; line-height: 20px; font-family: &quot;pt_sansregular&quot;;">Student please be informed that any receipt other than in the ERP generated format and generated from DAMS ERP software is not valid and shall not be considered in case of any disputes or for any discounts and other benefits from DAMS in future.

Also, after your enrolment please login to cloud.damsdelhi.com and check if your profile and personal details are correctly entered in our database.

</p>
           <p style="color: rgb(76, 76, 74); font-size: 14px; font-weight: 400; line-height: 22px;margin-top:15px;">
Please note as a policy DAMS encourages students to pay using demand draft/pay in favour of Delhi Academy of medical sciences Pvt Ltd, DAMS Sky Pvt Ltd  or DAMS Dental Pvt. Ltd. whichever applicable. </p>
        </span>
    </div></aside>
</section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>