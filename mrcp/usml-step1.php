<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
          <li><a title="USMLE Edge Step 1" class="active-link">USMLE Edge Step 1</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>USMLE EDGE Step-1 <span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span> </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>Step-I :-</span>
                  <p>The USMLE step 1 is the one of the three examinations that you must pass in order to become a licensed physician in the United States. The purpose of USMLE step 1 is to test your understanding and application of important concepts in basic biomedical sciences, with an emphasis on principles and mechanisms of health, disease, and modes of therapy. USMLE Step 1 is a one-day Computer-based Test that can be taken at the end of second year of medical school. </p>
                  <p>It emphasizes basic science principles. Specifically, Step 1 covers Anatomy, Behavioral Science, Biochemistry, Microbiology, Pathology, Pharmacology, and Physiology. Interdisciplinary areas such as genetics, immunology, and nutrition are also tested. The exam consists of approximately 350 questions given in 8 hours of testing time. There are seven 60-minute blocks, in each block you have to answer about 50 questions. You will be able to skip back and forth among questions, but ONLY within a block of questions. Once the hour is over, you will be unable to return to those 50 items. The test items aren't grouped by clinical subject but are presented in a random, interdisciplinary sequence. All USMLE questions are in one best-answer format. There are no matching questions on the computerized USMLE Step 1.</p>
                </div>
                <div class="franchisee-box"> <span>Systems :-</span>
                  <p>40-50 % General Principles</p>
                  <p>50-60 % Individual Organ Systems – cardiovascular, Hemopoetic/ Lymphoreticular, Gasterointestinal, Nervous / Special Senses, Renal / Urinary, Skin / Connective tissue,<br />
                    Reproductive, Musculo-skeletal, Endocrine, Pulmonary / Respiratory</p>
                </div>
                <ul class="dnb-list">
                  <h5>Process :-</h5>
                  <li><span>&nbsp;</span>30-50 % Normal structure and function.</li>
                  <li><span>&nbsp;</span>30-50 % Abnormal Processes.</li>
                  <li><span>&nbsp;</span>15-25 % Principles of therapeutics.</li>
                  <li><span>&nbsp;</span>10-20 % Psychosocial, cultural, occupational and environmental considerations.</li>
                </ul>
                <ul class="dnb-list">
                  <h5>DAMS PACKAGES FOR USMLE :-</h5>
                  <li><span>&nbsp;</span>ONLINE TEST SERIES.</li>
                  <li><span>&nbsp;</span>10 BLOCKS.</li>
                  <li><span>&nbsp;</span>2 MONTHS.</li>
                  <li><span>&nbsp;</span>1 HR PER PAPER.</li>
                </ul>
              </div>
            </article>
            <div class="book-ur-seat-btn margn-zero"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'dams-usmle-edge.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>