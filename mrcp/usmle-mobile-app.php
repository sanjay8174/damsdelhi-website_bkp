<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	

//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	
 
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-store-banner">
      <aside class="banner-left">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usmle-edge.php" title="USMLE Edge">USMLE Edge</a></li>
          <!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="DAMS Store">DAMS Store</a></li>-->
              <li><a href="http://damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>
          <li><a title="Dams Mobile Applications" class="active-link">Dams Mobile Applications</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Mobile Applications</h4>
            <article class="showme-main"> 
              <script type="text/javascript" src="https://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
              <link type="text/css" href="https://www.sunsean.com/idTabs/main.css">
              <ul class="idTabs">
                <li><a href="#jquery"><span class="apple"></span> iPhone</a></li>
                <li><a href="#official"><span class="android"></span> Android</a></li>
              </ul>
              <div id="jquery">
                <div class="application-content">
                  <div class="app-store-box">
                    <div class="app-store-btn"><a href="#">&nbsp;</a></div>
                    <p>We invite you to experience the newly released <br />
                      DAMS Delhi for iOS. It is a breeze <br />
                      to use and looks a lot cooler.</p>
                  </div>
                  <p>DAMS Delhi have Launched it's new 1.0.3 version available for Download and installation on all Android Smartphones.
                    This Advance version comes with new featues and better Interface.</p>
                  <div class="screenshot"> <a href="#" title="Screenshot"><img src="images/screenshot-1.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-2.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-3.png" alt="Screenshot" title="Screenshot" /></a> </div>
                  <div class="feature-main-box">
                    <div class="added-feature">
                      <h4>Added Features-</h4>
                      <ul class="added-feature-list">
                        <li>1) Side slide menu.</li>
                        <li>2) Zoom feature added in Test,Solution Report and Bookmark page.</li>
                        <li>3) Sliding from one report to another report.</li>
                        <li>4) Setting module added with clearing app data and auto saving features.</li>
                        <li>5) Better Readability.</li>
                      </ul>
                      <p>DAMS (Delhi Academy of Medical Sciences) is number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB & MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG & AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.
                        Things have changed again with coming of DAMS Android App. DAMS Identify the need for a mobile based education solution for Indian students and with the growing android market, DAMS has launched an android app that enables students to prepare for exams at their finger tips over their mobile phones.
                        DAMS Android App is a powerful exam Application which enables students to give test in offline and online mode.
                        Practice Test also available to find out how much you score before you appear for your final exams.
                        User can download and give exam in offline mode and result will automatically synchronize to online system.
                        Download the android app on your mobile and take a step forward in your exams preparation.
                        Rate us and Share with your Friends.</p>
                    </div>
                    <div class="added-feature">
                      <h4>Some of the Key Features:</h4>
                      <ul class="some-key">
                        <li>More than 8000 questions that enhances all your skills.</li>
                        <li>You can invite your friend via facebook , email and Sms.</li>
                        <li>This application will work on offline mode , which means no internet connection will be required once the user download the test.</li>
                        <li>You can see Solution Report and Scorecard in offline mode also.</li>
                        <li>Mark Important question as Benchmark for future reference. </li>
                        <li>Get updated by latest News.</li>
                        <li>Auto-synchronization facility , which means you test data automatically sync to server as soon as you connect to internet.</li>
                        <li>Buy Package and again get ready to give more test.</li>
                        <li>Connect with admin to solve your query by using Email feature.</li>
                      </ul>
                      <div class="app-store-box">
                        <div class="app-store-btn"><a href="#">&nbsp;</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="official">
                <div class="application-content">
                  <div class="app-store-box">
                    <div class="android-store-btn"><a href="#">&nbsp;</a></div>
                    <p>We invite you to experience the newly released <br />
                      DAMS Delhi for iOS. It is a breeze <br />
                      to use and looks a lot cooler.</p>
                  </div>
                  <p>DAMS Delhi have Launched it's new 1.0.3 version available for Download and installation on all Android Smartphones.
                    This Advance version comes with new featues and better Interface.</p>
                  <div class="screenshot"> <a href="#" title="Screenshot"><img src="images/screenshot-1.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-2.png" alt="Screenshot" title="Screenshot" /></a> <a href="#" title="Screenshot"><img src="images/screenshot-3.png" alt="Screenshot" title="Screenshot" /></a> </div>
                  <div class="feature-main-box">
                    <div class="added-feature">
                      <h4>Added Features-</h4>
                      <ul class="added-feature-list">
                        <li>1) Side slide menu.</li>
                        <li>2) Zoom feature added in Test,Solution Report and Bookmark page.</li>
                        <li>3) Sliding from one report to another report.</li>
                        <li>4) Setting module added with clearing app data and auto saving features.</li>
                        <li>5) Better Readability.</li>
                      </ul>
                      <p>DAMS (Delhi Academy of Medical Sciences) is number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI, UPSC, DNB & MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG & AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.
                        Things have changed again with coming of DAMS Android App. DAMS Identify the need for a mobile based education solution for Indian students and with the growing android market, DAMS has launched an android app that enables students to prepare for exams at their finger tips over their mobile phones.
                        DAMS Android App is a powerful exam Application which enables students to give test in offline and online mode.
                        Practice Test also available to find out how much you score before you appear for your final exams.
                        User can download and give exam in offline mode and result will automatically synchronize to online system.
                        Download the android app on your mobile and take a step forward in your exams preparation.
                        Rate us and Share with your Friends.</p>
                    </div>
                    <div class="added-feature">
                      <h4>Some of the Key Features:</h4>
                      <ul class="some-key">
                        <li>More than 8000 questions that enhances all your skills.</li>
                        <li>You can invite your friend via facebook , email and Sms.</li>
                        <li>This application will work on offline mode , which means no internet connection will be required once the user download the test.</li>
                        <li>You can see Solution Report and Scorecard in offline mode also.</li>
                        <li>Mark Important question as Benchmark for future reference. </li>
                        <li>Get updated by latest News.</li>
                        <li>Auto-synchronization facility , which means you test data automatically sync to server as soon as you connect to internet.</li>
                        <li>Buy Package and again get ready to give more test.</li>
                        <li>Connect with admin to solve your query by using Email feature.</li>
                      </ul>
                      <div class="app-store-box">
                        <div class="android-store-btn"><a href="#">&nbsp;</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>USMLE Edge DAMS Store</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li class="border_none"><a href="usmle-idams.php" title="iDAMS Tablet">iDAMS Tablet</a></li>
                  <!--<li><a href="usmle-dams-store-publication.php" title="Dams Publications">Dams Publications</a></li>-->
                  <li><a href="http://damspublications.com/" target="_blank" title="Dams Publications">Dams Publications</a></li>
                  <li><a href="usmle-online-test-series.php" title="Online Test Series">Online Test Series</a></li>
                  <li><a href="usmle-dams-test-series.php" title="DAMS Test Series">DAMS Test Series</a></li>
                  <li><a href="../usmle-i-dams.php" title="Mobile Applications" class="active-store">Mobile Applications</a></li>
                </ul>
              </div>
            </div>
          </div>
          <?php include 'usmle-buynow-right-section.php'; ?>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>