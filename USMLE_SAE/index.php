<?php
error_reporting(0);
ob_start();

session_start();
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$log = Logger::getLogger(__CLASS__);
$p = $_GET['p'];
$offline = $_GET['offline'];
$timezone = "Asia/Calcutta";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
if($p==''){
    include('Home.php');
}else {
    include($p.'.php');
}


?>