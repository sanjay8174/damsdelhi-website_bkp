/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 function trim(val){
    return val.replace(/^\s+|\s+$/g,'');
}
function validation(type,value){
    var ck_name = /^[\w-]+[A-Za-z ./]{1,50}$/;
    var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i ;
    var ck_username = /^[A-Za-z0-9_]{1,20}$/;
    var ck_password =  /^[A-Za-z0-9.!@#$%^&*()_]{6,20}$/;//change by shubham
    var ck_contact = /^\d{10}$/;
    var ck_pincode = /^\d{6}$/;
    var check  = true;
    switch(type){
        case 'name':
                    check=ck_name.test(value);
                    break;
        case 'email':
                    check=ck_email.test(value);
                    break;
        case 'password':
                    check=ck_password.test(value);
                    break;
        case 'username':
                    check=ck_username.test(value);
                    break;
        case 'contact':
                    check=ck_contact.test(value);
                    break;
        case 'pincode':
                    check=ck_pincode.test(value);
                    break;
        case 'null':
                    if(value==''){
                        check=false;
                    }
                    break;
        default:
                    check=false;
    }
    return check;

}
$(document).ready(function(){
   $("#checkRegisterationForm").click(function(){
      if($("#emailValid").val()==1){
           alert("Email id already register please try to register with another email id");
           $("#emailAddress").focus();
           return false;
       }
       var candidateName =trim($("#candidateName").val());
       if($("#StudentType").val()==""){
           alert("Please select Student Type.");
            $("#StudentType").val("");
           $("#StudentType").focus();
           return false;
       }else if(candidateName==""){
           alert("Please enter your name.");
            $("#candidateName").val("");
           $("#candidateName").focus();
           return false;
       }else if(!validation('name',$("#candidateName").val())){
           alert("Please enter your valid name.");
           $("#candidateName").focus();
           return false;
       }else if($("#dob").val()==""){
           alert("Please enter your DOB.");
           $("#dob").focus();
           return false;
       }else if($("#emailAddress").val()==""){
           $("#emailAddress").attr("readonly",false);
           alert("Please enter your email address.");
           $("#emailAddress").focus();
           return false;
       }else if(!validation('email',$("#emailAddress").val())){
           $("#emailAddress").attr("readonly",false);
           alert("Please enter correct email address.");
           $("#emailAddress").focus();
           return false;
       }else if($("#confirmEmail").val()==""){
           $("#confirmEmail").attr("readonly",false);
           alert("Please confirm your email address.");
           $("#confirmEmail").focus();
           return false;
       }else if($("#emailAddress").val()!=$("#confirmEmail").val()){
           $("#confirmEmail").attr("readonly",false);
           alert("email address and confirm email address doesn't match.");
           $("#confirmEmail").focus();
           return false;
       }else if($("#mobile").val()==""){
           $("#mobile").attr("readonly",false);
           alert("Please enter your mobile number.");
           $("#mobile").focus();
           return false;
       }else if(!validation('contact',$("#mobile").val())){
           alert("Please enter your correct mobile number.");
           $("#mobile").focus();
           return false;
       }else if($("#confirmMobile").val()==""){
           $("#confirmMobile").attr("readonly",false);
           alert("Please confirm your mobile number.");
           $("#confirmMobile").focus();
           return false;
       }else if($("#mobile").val()!=$("#confirmMobile").val()){
           alert("your mobile number and confirm mobile number are not match.");
           $("#confirmMobile").focus();
           return false;
       }else if($("#gender").val()=="0" || $("#gender").val()==""){
       alert("Please select your gender.");
       $("#gender").focus();
       return false;
       }else if($("#college").val()==""){
       alert("Please enter your college name.");
       $("#college").focus();
       return false;
       }else if($("#country").val()==0){
           $("#country").attr("readonly",false);
           alert("Please enter your country name.");
           $("#country").focus();
           return false;
       }else if($("#securityCode").val()==""){
           $("#securityCode").attr("readonly",false);
           alert("Please enter your security code.");
           $("#securityCode").focus();
           return false;
       }else if($("#securityCode").val()!=""){
           var urlData="mode=checkCaptcha&captcha="+trim($("#securityCode").val());
           var check=2;
            $.ajax({
                type:"post",
                url:"index.php?p=submit",
                data:urlData,
                async:false,
                error:function(result){
                    $("#preloader").hide();
                    alert("It seems you are not connected to Internet.");
                    return false;
                },
                success:function(result){
                    if(result==1){
                    alert("Please enter the correct security code.");
                    $("#securityCode").focus();
                    check=1;
                    return false;
                    }else if(result==0){
                        check=0;
                    }
                }
             
            });
       if(check==1){
           return false;
       }else if(check==0){
       if($("#iagree").is(":checked")==false){
           alert("Please accept the declaration.");
           $("#iagree").focus();
           return false;
       }else if($("#emailValid").val()==0){
           checkDuplicateEmail();
           if($("#emailValid").val()==0){
           $("#mode").val("Add");
           document.registration.submit();
           $("#preloader").show();
           }
       }
   }
   }
       
   }); 
   $("#loginSubmit").click(function(){
       var userId=trim($("#userId").val());
       var password=trim($("#password").val());
       if(userId==""){
           alert("Please enter the Email-id.");
           $("#userId").focus();
           $("#preloader").hide();
           return false;
       }else if(!validation('email',userId)){
           alert("Please enter the valid Email-id.");
           $("#userId").focus();
           $("#preloader").hide();
           return false;
       }else if(password==""){
           alert("Please enter your password");
           $("#password").focus();
           $("#preloader").hide();
           return false;
       }else{
                  $("#preloader").show();
           var urlData="userId="+userId+"&password="+password+"&mode=checkLogin";
           $.ajax({
              type:"post",
              url:"index.php?p=submit",
              data:urlData,
              async:false,
              error:function(result){$("#preloader").hide();
                  alert("It seems you are not connected to the internet.");
                  return false;
              },
              success:function(result){
                  result = result.split(",");
                  result[0]=trim(result[0]);
                  if(result[0]=="Y"){
                      $("#studentIdConfirm").val(result[1]);
                      document.success.submit();
                    //location.href="index.php?p=lastPage&id="+result[1];  
                  }else if(result[0]==1){
                      $("#studentId").val(result[1]);
                      document.loginForm.action="index.php?p=application_process";
                      document.loginForm.submit();
//                      location.href="index.php?p=application_process&id="+result[1];
                  }else{
                      $("#preloader").hide();
                      alert("Your Email Id and Password doesn't match.");
                      document.loginForm.reset();
                  }
              }
           });    
       }
   });
   $("#disableN").click(function(){
     $("#categoryDiv").hide();  
     $("#percentageDiv").hide();  
     $("#wheelchairDiv").hide(); 
     $("#obcPic").hide();
     $("#pwdPic").hide();
     $("#obcUpload").hide();
     $("#pwdUpload").hide();
     $("#disableYN").val("0");
   });
   $("#disableY").click(function(){
     $("#categoryDiv").show();  
     $("#percentageDiv").show();  
     $("#wheelchairDiv").show();
     $("#obcPic").show();
     $("#pwdPic").show();
     $("#obcUpload").show();
     $("#pwdUpload").show();
     $("#disableYN").val("1");
   });

   
   if($("#dyes").val()==1){
       $("#cost").html("Rs. 500");
       $("#damosianYN").val("1");
       $("#examCost").html("Rs. 500");
       $("#packageCost").val("500");
       $("#packageId").val("1");
   }else if($("#dno").is(":checked")==true){
       $("#cost").html("Rs. 800");
       $("#damosianYN").val("0");
       $("#enrollDiv").hide();
       $("#examCost").html("Rs. 800");
       $("#packageCost").val("800");
       $("#packageId").val("3");
   }
      if($("#dno").is(":checked")==true){
       $("#cost").html("Rs. 800");
       $("#damosianYN").val("0");
       $("#enrollDiv").hide();
       $("#examCost").html("800");
       $("#packageCost").val("800");
       $("#packageId").val("3");
   }
   $("#dyes").click(function(){
       $("#enrollDiv").show();
       $("#damosianYN").val("1");
       $("#cost").html("Rs. 500");
       $("#examCost").html("Rs. 500");
       $("#packageCost").val("500");
       $("#packageId").val("1");
   });
   $("#dno").click(function(){
       $("#enrollDiv").hide();
       $("#damosianYN").val("0");
       $("#cost").html("800");
       $("#examCost").html("800");
       $("#packageCost").val("800");
       $("#packageId").val("3");
   });
   $("#submitPayment").click(function(){
       
       if($("#couponSelect").is(":checked")==false && $("#onlineSelect").is(":checked")==false){
         alert("Please select the mode of payment.");
//         $("#couponSelect").focus();
         return false;
       }
       if($("#couponSelect").is(":checked")==true){
           var packageId = $("#packageId").val();
           var studentId = $("#studentId").val();
           var email = $("#").val();
           if($("#couponCode").val()==""){
               alert("Please enter your coupon code.");
               $("#couponCode").focus();
               return false;
           }else if($("#couponCode").val()!="" && !validation('username',$("#couponCode").val())){
               alert("Please enter the valid coupon code.");
               $("#couponCode").focus();
               return false;
           }else if($("#couponCode").val()!=""){
            $("#preloader").show();
            var urlData = "mode=checkCoupon&couponCode=" + $("#couponCode").val()+"&packageId="+packageId+"&studentId="+studentId+"&cost="+$("#packageCost").val();
               $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData,
                    error: function () {
                        $("#preloader").hide();
                        alert("It seems you are not connected to the internet.");
                    },
                    success:function(result){
                        $("#preloader").hide();
                        if(result==4){
                        $("#preloader").hide();
                        alert("Coupon code Invalid.");
                        $("#couponCode").focus();
                        return false;}
                    else if(result==2){
                        $("#preloader").hide();
                        alert("Your payment done successfull,but you are guest from the admin side.");
                        $("#couponCode").focus();
                        return false;
                        }else if(result==3){
                        $("#preloader").hide();
                        alert("You are successfully enrolled.");
                        location.href="index.php?p=lastPage&payment=c";
                        }
                    }
                });
           }
       } 
       
          if($("#onlineSelect").is(":checked")==true){
                 document.redirectPayment.submit();
          }
   });
   $("#validatePersonalDetail").click(function(){
      if(trim($("#dob").val())==""){
       alert("Please enter your DOB.");
       $("#dob").focus();
       return false;
      }else if($("#gender").val()=="0" || $("#gender").val()==""){
       alert("Please select your gender.");
       $("#gender").focus();
       return false;
       }else if($("#state").val()=="0"){
       alert("Please select your state.");
       $("#state").focus();
       return false;
       }else if($("#nationality").val()=="0"){
       alert("Please select your Country.");
       $("#nationality").focus();
       return false;
       }else if(trim($("#address1").val())==""){
       alert("Please enter your communication address.");
       $("#address1").focus();
       return false;
       }else if(trim($("#city").val())==""){
       alert("Please enter your city.");
       $("#city").focus();
       return false;
       }else if(trim($("#pincode").val())==""){
       alert("Please enter the pincode.");
       $("#pincode").focus();
       return false;
       }else if(!validation('pincode',trim($("#pincode").val()))){
           alert("Please enter the valid 6 digit pincode.");
           $("#pincode").focus();
           return false;
       }else if($("#cState").val()=="0" || $("#cState").val()==""){
       alert("Please select your state.");
       $("#cState").focus();
       return false;
       }else if($("#cCountry").val()=="0" || $("#cCountry").val()==""){
       alert("Please select your country.");
       $("#cCountry").focus();
       return false;
       }
       else if($("#imageExist").val()=="0"){
       alert("Please upload your profile picture.");
       $("#preview").focus();
       return false;    
       } 
      $("#complete").val("1");
       document.personalDetail.submit();
   });
    $("#academic").submit(function () {
        var damosian = $("#dyes").is(":checked");
        var nDamosian = true;
        var studentId = $("#studentId").val();
        var isDamosian = false;
        if (damosian == false && nDamosian == false) {
            alert("Please select you are a damosians or not.");
            $("#dyes").focus();
            return false;
        } else if (damosian == true) {
            var enroll = trim($("#enroll").val());
            if (enroll == "") {
                alert("Please enter your DAMS-2016 Roll Number.");
                $("#enroll").val("");
                $("#enroll").focus();
                return false;
            } else if (enroll != "") {
                var prefix = enroll.substring(0,3);
                var prefix = prefix.toUpperCase();
//                if(prefix == "BRH") {
//                     enrollNo = enroll.toUpperCase();
//                     var rollNumberChk = /^[A-Z]{3}[0-9]{3,6}$/;
//                     if(!rollNumberChk.test(enrollNo)) {
//                         alert("You are not DAMS-2016 student for the given roll number.");
//                         $("#enroll").focus();
//                         return false;
//                     }
//                         if ($("#centre1").val() == 0) {
//                                alert("Please select your first prefered location");
//                                $("#centre1").focus();
//                                return false;
//                            } else if ($("#centre2").val() == 0) {
//                                alert("Please select your second prefered location");
//                                $("#centre2").focus();
//                                return false;
//                            } 
////                            else if ($("#centre1").val() == $("#centre2").val()) {
////                                alert("First choice and second choice cant be same");
////                                $("#centre2").focus();
////                                return false;
////                            } else if ($("#centre3").val() == 0) {
////                                alert("Please select your third prefered location");
////                                $("#centre3").focus();
////                                return false;
////                            } else if ($("#centre2").val() == $("#centre3").val()) {
////                                alert("second choice and third choice cant be same");
////                                $("#centre3").focus();
////                                return false;
////                            } 
////                            else if ($("#centre1").val() == $("#centre3").val()) {
////                                alert("first choice and third choice cant be same");
////                                $("#centre3").focus();
////                                return false;
////                            }
//isDamosian=1;
//                            var urlData = "mode=saveCentre&centre1=" + $("#centre1").val() + "&damosian=" + isDamosian + "&centre2=" + $("#centre2").val() + "&centre3=" + $("#centre3").val() + "&enroll=" + $("#enroll").val() + "&studentId=" + studentId;
//                            $.ajax({
//                                type: 'post',
//                                url: 'index.php?p=submit',
//                                data: urlData,
//                                async: false,
//                                error: function (result) {
//                                    $("#preloader").hide();
//                                    alert("It seems you are not connected to the internet.");
//                                },
//                                success: function () {
//                                    $("#complete").val("2");
//                                    location.href = "index.php?p=application_process&c=last";
//                                }
//                            });
//                $("#preloader").show();
//                }
//                else
//                {
                var urlData = "mode=checkDamosian&enrollNumber=" + enroll+"&studentId=" + studentId;;
                $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData,
                    async:false,
                    error: function () {
                        $("#preloader").hide();
                        alert("It seems you are not connected to the internet.");
                    },
                    success: function (result) {
                        $("#preloader").hide();
                       if(trim(result) == "Exist" ) {
                            alert("This Enrollment Number is already used.");
                            $("#enroll").focus();
                            return false;
                        }else if (result == 0) {
                            alert("You are not DAMS-2016 student for the given roll number.");
                            return false;
                        } else if (result >= 1) {
                            $("#preloader").hide();
                            if ($("#centre1").val() == 0) {
                                alert("Please select your first prefered location");
                                $("#centre1").focus();
                                return false;
                            } else if ($("#centre2").val() == 0) {
                                alert("Please select your second prefered location");
                                $("#centre2").focus();
                                return false;
                            } 
//                            else if ($("#centre1").val() == $("#centre2").val()) {
//                                alert("First choice and second choice cant be same");
//                                $("#centre2").focus();
//                                return false;
//                            } else if ($("#centre3").val() == 0) {
//                                alert("Please select your third prefered location");
//                                $("#centre3").focus();
//                                return false;
//                            } else if ($("#centre2").val() == $("#centre3").val()) {
//                                alert("second choice and third choice cant be same");
//                                $("#centre3").focus();
//                                return false;
//                            } 
//                            else if ($("#centre1").val() == $("#centre3").val()) {
//                                alert("first choice and third choice cant be same");
//                                $("#centre3").focus();
//                                return false;
//                            }
isDamosian=1;
                            var urlData = "mode=saveCentre&centre1=" + $("#centre1").val() + "&damosian=" + isDamosian + "&centre2=" + $("#centre2").val() + "&centre3=" + $("#centre3").val() + "&enroll=" + $("#enroll").val() + "&studentId=" + studentId;
                            $.ajax({
                                type: 'post',
                                url: 'index.php?p=submit',
                                data: urlData,
                                async: false,
                                error: function (result) {
                                    $("#preloader").hide();
                                    alert("It seems you are not connected to the internet.");
                                },
                                success: function () {
                                    $("#complete").val("2");
                                    location.href = "index.php?p=application_process&c=last";
                                }
                            });

                        }
                    }
                });


                }

           // }
        }else if(nDamosian==true){
         if ($("#centre1").val() == 0) {
                                alert("Please select your first prefered location");
                                $("#centre1").focus();
                                return false;
                            } else if ($("#centre2").val() == 0) {
                                alert("Please select your second prefered location");
                                $("#centre2").focus();
                                return false;
                            } else if ($("#centre1").val() == $("#centre2").val()) {
                                alert("First choice and second choice can't be same");
                                $("#centre2").focus();
                                return false;
                            } else if ($("#centre3").val() == 0) {
                                alert("Please select your third prefered location");
                                $("#centre3").focus();
                                return false;
                            } 
                            /*else if ($("#centre2").val() == $("#centre3").val()) {
                                alert("second choice and third choice can't be same");
                                $("#centre3").focus();
                                return false;
                            } 
                            else if ($("#centre1").val() == $("#centre3").val()) {
                                alert("first choice and third choice can't be same");
                                $("#centre3").focus();
                                return false;
                            }*/
isDamosian=0;
                            var urlData = "mode=saveCentre&centre1=" + $("#centre1").val() + "&damosian=" + isDamosian + "&centre2=" + $("#centre2").val() + "&centre3=" + $("#centre3").val() + "&enroll=" + $("#enroll").val() + "&studentId=" + studentId;    
            $.ajax({
                                type: 'post',
                                url: 'index.php?p=submit',
                                data: urlData,
                                async: false,
                                error: function (result) {
                                    $("#preloader").hide();
                                    alert("It seems you are not connected to the internet.");
                                },
                                success: function () {
                                    $("#complete").val("2");
                                    location.href = "index.php?p=application_process&c=last";
                                }
                            });
        }
    }); 
    
   $("#workDetail").click(function(){
       var count = $("#countworkActive").val();
       var studentId = $("#studentId").val();
       var checkCount=0;
       for(i=0;i<count;i++){
           if($("#check"+i).is(":checked")==true){
               if($("#expTime"+i).val()==""){
                   alert("Please enter the work experience.");
                   $("#expTime"+i).focus();
                   return false;
               }
                var urlData="mode=saveWork&workId="+$("#check"+i).val()+"&workArea="+$("#exp"+i).val()+"&studentId="+studentId+"&workEsperience="+$("#expTime"+i).val();
               $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData,
                    error: function () {
                        $("#preloader").hide();
                        alert("It seems you are not connected to the internet.");
                    },
                    success: function () {
                    }
                });
           }else if($("#expTime"+i).val()!=""){
               alert("Please select the checkbox");
               $("#check"+i).focus();
               return false;
           }
       }
       location.href="index.php?p=application_process&c=3"
   });
   $("#programCentre").click(function(){
        var count = $("#countprogramActive").val();
        var studentId = $("#studentId").val();
       var checkCount=0;
       for(i=0;i<count;i++){
           if($("#checkProgram"+i).is(":checked")==true){
               if($("#location"+i).val()=="0"){
                   alert("Please select the interview location first.");
                   $("#location"+i).focus();
                   return false;
               }
           }else if($("#location"+i).val()!="0"){
               alert("Please check the program.");
               $("#checkProgram"+i).focus();
               return false;
           }
       }
       if($("#centre1").val()==0){
           alert("Please select your first prefered location");
           $("#centre1").focus();
           return false;
       }else if($("#centre2").val()==0){
           alert("Please select your second prefered location");
           $("#centre2").focus();
           return false;
       }else if($("#centre1").val() == $("#centre2").val()){
           alert("First choice and second choice cant be same");
           $("#centre2").focus();
           return false;
       }else if($("#centre3").val()==0){
           alert("Please select your third prefered location");
           $("#centre3").focus();
           return false;
       }else if($("#centre2").val() == $("#centre3").val()){
           alert("second choice and third choice cant be same");
           $("#centre3").focus();
           return false;
       }else if($("#centre1").val() == $("#centre3").val()){
           alert("first choice and third choice cant be same");
           $("#centre3").focus();
           return false;
       }else{
                for(i=0;i<count;i++){
           if($("#checkProgram"+i).is(":checked")==true){
               if($("#location"+i).val()=="0"){
                   alert("Please select the interview location first.");
                   $("#location"+i).focus();
                   return false;
               }else {
      var urlData1="mode=saveProgram&programId="+$("#checkProgram"+i).val()+"&interviewLocation="+$("#location"+i).val()+"&studentId="+studentId;   
          $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData1,
                    async:false,
                    error: function (result) {
                        alert("It seems you are not connected to the internet.");
                        $("#preloader").hide();
                    },
                    success: function () {
                    }
                });
               }
           }
       }  
           
       var centre1 = $("#centre1").val();
       var centre2 = $("#centre2").val();
       var centre3 = $("#centre3").val();
      var urlData="mode=saveCentre&centre1="+centre1+"&centre2="+centre2+"&centre3="+centre3+"&studentId="+studentId;
               $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData,
                    async:false,
                    error: function (result) {
                        alert("It seems you are not connected to the internet.");
                        $("#preloader").hide();
                    },
                    success: function () {
                    }
                });    
       }
  location.href='index.php?p=application_process&c=last';
   });
   
   $("#couponSelect").click(function(){
   if($("#couponSelect").is(":checked")==true){
       $("#onlineSelect").prop("checked",false);
       $("#couponBox").show();
   }else{
       $("#couponBox").hide();
   }    
   });
   $("#onlineSelect").click(function(){
   if($("#onlineSelect").is(":checked")==true){
       $("#couponSelect").prop("checked",false);
       $("#couponBox").hide();
   }    
   });
   
   /************ forgot password ************///////////////
   
   $("#forget_login").click(function(){
         var forget_email=trim($("#forget_email").val());
          if(!validation('email',forget_email))
          {alert("Please enter the valid Email-id");
           $("#forget_email").focus();
           return false;
          }else{
              $("#preloader").show();
              var urlData="email="+forget_email+"&mode="+"ForgotPass";
                  $.ajax({
                   type:"POST",
                   url:"index.php?p=submit",
                   data:urlData,
                   async:false,
                   error:function(error){$("#preloader").hide();},
                   success:function(result){
                       if(result==true){
                        $("#preloader").hide();
                        alert("your password has been send to your email");
                        $("#forget_email").val("");
                        $("#dialog").hide();}
                   else{
                     $("#preloader").hide();
                     alert("your Email Id doesn't match");
                     $("#forget_email").val("");}}
                });
          }
          
      });
 $(".dwnlMyAdmitCard").click(function(){
     var stuId=$("#studentId").val();
     var urlData = 'mode=checkAdmitCard&studentId='+stuId;
     $.ajax({
      type:'post',
      url:'index.php?p=submit',
      data:urlData,
      async:false,
      error:function(result){
          alert("Error"+result);
      },
      success:function(result){
          if(result==0){
              alert("Your admit card is not release yet.");
              //location.href='index.php?p=admitCard&id='+stuId;
          }else{
              location.href='index.php?p=admitCard&id='+stuId;
          }
      }
         
     });
 });
 /************** end here ******************//////////
});
function checkDuplicateEmail(){
    var email = trim($("#emailAddress").val());
    if(email!=""){
        $("#preloader").show();
        var urlData="mode=checkDuplicate&email="+email;
        $.ajax({
           type:"post",
           url:"index.php?p=submit",
           data:urlData,
           async:false,
           error:function(result){
               alert("It seems you are disconnected with internet");
               $("#emailValid").val("0");
               $("#preloader").hide();
           },
           success:function(result){
               $("#preloader").hide();
               if(result==1){
                   $("#preloader").hide();
                   alert("Email Id already registerd.Please try the another one");
                   $("#emailAddress").focus();
                   $("#emailValid").val("1");
                   return false;
               }else if(result==0){
                   $("#emailValid").val("0");
               }else{
                   $("#emailValid").val("2");
               }
           }
        });
    }
}
function checkRollNumber(){
    $("#preloader").show();
    var enroll = $("#enroll").val();
    var studentId = $("#studentId").val();
  var urlData = "mode=checkDamosian&enrollNumber=" + enroll+"&studentId=" + studentId;
               $.ajax({
                    type: 'post',
                    url: 'index.php?p=submit',
                    data: urlData,
                    error: function () {
                        alert("It seems you are not connected to the internet.");
                        $("#preloader").hide();
                    },
                    success: function (result) {
                        
                        $("#preloader").hide();
                        if(trim(result) == "Exist" ) {
                            alert("This Enrollment Number is already used.");
                            $("#enroll").focus();
                            return false;
                        }else if(result==0){
                            $("#preloader").hide();
                            alert("You are not DAMS-2016 student for the given roll number.");
                            return false;
                        }else if(result>=1){
                            return true;
                        }
                    }
                });
}

/************** image preview **************//////////
function showimagepreview(input)
{    
      var val= $("#fileuploadPic").val();
      $("#fileuploadPic").attr('name','files');
      $("#fileuploadPic1").attr('name','');
      var studentId = $("#studentId").val();
      var r =$("#fileupload").val();
      var ext =val.split('.').pop().toLowerCase();
  if (($.inArray(ext, ['jpeg','jpg','png']) == -1)){
      $("#fileuploadPic").val("");
       alert("Invalid file format.please select jpg,jpeg or png image.");
       $("#imageExist").val("0");
       $("#preview").attr("src","PHOTO/photo.jpg");
       return false;
  } else if((input.files[0].size)<5120 || (input.files[0].size)>204800){
      $("#fileuploadPic").val("");
         alert ("File size is not between 5KB to 200 KB.");
         $("#imageExist").val("0");
         $("#preview").attr("src","PHOTO/photo.jpg");
          return false;
  }else{
                $("#imageExist").val("1");
                $("#picChange").val("1");
  }
  
      if (input.files && input.files[0])
       {     $("#fileupload").val("1");
           var filerdr = new FileReader();
            filerdr.onload = function(e)
            {$("#preview").attr('src', e.target.result);}
        filerdr.readAsDataURL(input.files[0]);
    }
}
function showimagepreview1(input)
{     var val= $("#fileuploadPic1").val();
      $("#fileuploadPic1").attr('name','files');
      $("#fileuploadPic").attr('name','');
      var studentId = $("#studentId").val();
      var r =$("#fileupload").val();
      var ext =val.split('.').pop().toLowerCase();
  if (($.inArray(ext, ['jpeg','jpg','png']) == -1)){
      $("#fileuploadPic1").val("");
       alert("Invalid file format.please select jpg,jpeg or png image.");
       $("#imageExist").val("0");
       $("#preview").attr("src","PHOTO/photo.jpg");
       return false;
  } else if((input.files[0].size)<5120 || (input.files[0].size)>204800){
      $("#fileuploadPic1").val("");
         alert ("File size is not between 5KB to 200 KB.");
         $("#imageExist").val("0");
         $("#preview").attr("src","PHOTO/photo.jpg");
         return false;
  } else{
                $("#imageExist").val("1");
                $("#picChange").val("1");
  }
  
      if (input.files && input.files[0])
       {     $("#fileupload").val("1");
           var filerdr = new FileReader();
            filerdr.onload = function(e)
            {$("#preview").attr('src', e.target.result);}
        filerdr.readAsDataURL(input.files[0]);
    }
}
/************** End image preview **************//////////