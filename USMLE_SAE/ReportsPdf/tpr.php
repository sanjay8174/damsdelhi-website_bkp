<?php
session_start();
ini_set("max_execution_time", "8000");
ini_set("memory_limit","2048M");
ob_start();
require ('../../../config/autoLoaderStudent.php');
require ('../student/Constantstudent.php');
require ('../Constant.php');
include("../../sComponents/profile/ProfileDao.php");
include("../../sComponents/reports/ReportsDao.php");
include("mpdf.php");

$database = new Database();
$database->connect();
$studentid = $_REQUEST['stuid'];
$subid1 = $_REQUEST['subid1'];
if(isset($_REQUEST['stuenroll'])){
    $enrollMentNo = $_REQUEST['stuenroll'];
} else{
    $enrollMentNo = $_SESSION['ENROLL'];
}
$profiledao = new ProfileDao();
$testIdLists = "";
$perArray = array();
$topicArray = array();
$getTestMixData=array();
$testIdsArray = array();
$topicWiseMarks = array();
$topicWiseArray = array();
$getMixDataArray = array();
$getSubjectWiseStudentRankDetail1=array();
$studentDataArray = array();
$testNameAbbreviation = array();
$i=0;$mergeTestString='';$testStringUnmerge='';
$courseName = '';
$courseName = $profiledao->getCourseName($_SESSION['course_id']);
$i=0;$mergeTestString='';
$studentTest = $profiledao->studentTest($studentid);
while($row = mysql_fetch_object($studentTest)){
    $countArray[$i] = count(explode(",", $row->TEST_ID));
    $studentDataArray[$i][0] = $row->TEST_ID;
    $studentDataArray[$i][1] = $row->IIT_TYPE;
    $studentDataArray[$i][2] = $row->MERGE_TEST_ID;
    $studentDataArray[$i][3] = $row->START_DATE;
    $studentDataArray[$i][4] = $row->TEST_NAME;
    $studentDataArray[$i][5] = $row->EXAM_TYPE;
    $studentDataArray[$i][6] = round($row->TOTAL_MARKS,2);
    $studentDataArray[$i][7] = round($row->TEST_MARKS,2);
    $studentDataArray[$i][8] = $row->TEST_PI;
    $studentDataArray[$i][9] = $row->RANK;
    if($row->MERGE_TEST_ID != 0){
        $getSubjectWiseStudentRankDetail1[$i]=0;
        if($mergeTestString == ''){
            $mergeTestString = $row->MERGE_TEST_ID;
        } else{
            $mergeTestString = $mergeTestString.",".$row->MERGE_TEST_ID;
        }
    } else{
        $getSubjectWiseStudentRankDetail1[$i]=1;
        if($testStringUnmerge == ''){
            $testStringUnmerge = $row->TEST_ID;
        }else{
        $testStringUnmerge = $testStringUnmerge.",".$row->TEST_ID;
        }
   }
    $testNameAbbreviation[$row->TEST_ID] = "T0".($i+1);
    if($testIdLists == ""){
        $testIdLists = $row->TEST_ID;
    }else{
        $testIdLists = $testIdLists .",".$row->TEST_ID ;
    }
    $i++;
}
$testIdsArray = explode(",", $testIdLists);
$topicArray = $profiledao->topicArray($testIdsArray,$subid1);
$getSubject = array();$getSubSubject = array();
$reportdao = new ReportsDao();
$getSubject = $reportdao->getSubjectForReport($testIdsArray,'tpr');
$getSubSubject  = $reportdao ->getSubSubject($testIdsArray,$studentid,'tpr');

if(count($testIdsArray)>0){
    $topicWiseMarks = $profiledao->topicWisemarks($testIdsArray,$studentid);
    $topicWiseArray = $profiledao->topicWiseOverall($testIdsArray);
    $getTestMixData   = $profiledao->getTestMixData($testIdsArray);
}

if($mergeTestString!=''){
    $lm=0;$lmn=0;$abcd=array();$bcde=array(); $fegh=array();$ko=0;$abcde=array();$abcdef=array();$feghi=array();$feghij=array();$average=array();
    $getMixData = $profiledao->getMixData("$mergeTestString");
    while($rows1 = mysql_fetch_object($getMixData)){
        $getMixDataArray[$lm][0] = $rows1->TEST_ID;
        $getMixDataArray[$lm][1] = round($rows1->TOTAL,2);
        $getMixDataArray[$lm][2] = round($rows1->TOTAL_AVG,2);
        $getMixDataArray[$lm][3] = $rows1->TOTAL_STUDENT;
        $getMixDataMaxsubjectMarks = $profiledao->getMixDataMaxsubjectMarks($rows1->TEST_ID);
         while($rows2 = mysql_fetch_object($getMixDataMaxsubjectMarks)){
            $getMixDataMaxsubjectMarksArray[$ko][0] = $rows2->SUBJECT_IDS;
            $getMixDataMaxsubjectMarksArray[$ko][1] = $rows2->SUBJECT_WISE_MARKS;
            $abcde[$ko]=explode(",",$getMixDataMaxsubjectMarksArray[$ko][0]);
            $abcdef[$ko]=explode("#",$getMixDataMaxsubjectMarksArray[$ko][1]);
            for($j=0;$j<count($abcde[$ko]);$j++){

            if($subid1 == $abcde[$ko][$j]){

              $feghi[$ko]=$abcdef[$ko][$j];
            }
       }

             $ko++;
         }
    if($subid1!=all){
         $feghij[$lm]=max($feghi);
         $average[$lm] = round(array_sum($feghi) / count($feghi),2);
    }
        $lm++;
    }
    $getSubjectwisedataMerge=$profiledao->getSubjectwisedataMerge("$mergeTestString",$studentid);
      while($rows1 = mysql_fetch_object($getSubjectwisedataMerge)){
        $getMixDataArrayMerge[$lmn][0] = $rows1->SUBJECT_IDS;
        $getMixDataArrayMerge[$lmn][1] = $rows1->SUBJECT_WISE_MARKS;
        $getMixDataArrayMerge[$lmn][2] = $rows1->TEST_IDS;
      $abcd[$lmn]=explode(",",$getMixDataArrayMerge[$lmn][0]);
      $bcde[$lmn]=explode("#",$getMixDataArrayMerge[$lmn][1]);

      if($subid1!='' && $subid1!=all){
        $getSubjectwiseMaxMarksMerge=$profiledao->getSubjectwiseMaxMarksMerge($getMixDataArrayMerge[$lmn][2],$subid1);
 while($rows3 = mysql_fetch_object($getSubjectwiseMaxMarksMerge)){
     $getMixDataArrayMerge[$lmn][3] = $rows3->SUBJECTMARKS;

 }
      }
       for($j=0;$j<count($abcd[$lmn]);$j++){

            if($subid1 == $abcd[$lmn][$j]){

              $fegh[$lmn]=$bcde[$lmn][$j];
            }
       }
       if($subid1!=all){
       $percentagemerge[$lmn]= round(($fegh[$lmn]/$getMixDataArrayMerge[$lmn][3])*100,2);
       }
    $lmn++;
 }
}
if($testStringUnmerge!='' && $subid1!=all ){

    $i=0;$getSubjectWiseMarksForUnmergeDetail=array();$getSubjectWiseTotalMarksForUnmergeDetail=array();$k=0;$l=0;
    $getSubjectWiseMarksForUnmerge=$profiledao->getSubjectWiseMarksForUnmerge("$testStringUnmerge",$studentid,$subid1);
              while ($rows = mysql_fetch_array($getSubjectWiseMarksForUnmerge)) {

                $getSubjectWiseMarksForUnmergeDetail[$i][0] = round($rows['TOTAL_MARKS'],2);
                 $getSubjectWiseMarksForUnmergeDetail[$i][3] = $rows['TEST_ID'];
                $getSubjectWiseMarksForUnmergeDetail[$i][1] = round($rows['SUB_MARKS'],2);
                 $subjectWisePercentage= round(($getSubjectWiseMarksForUnmergeDetail[$i][0]/$getSubjectWiseMarksForUnmergeDetail[$i][1])*100,2);
                $getSubjectWiseMarksForUnmergeDetail[$i][2] = $subjectWisePercentage;
                 $i++;
            }

$getSubjectWiseTotalMarksForUnmerge=$profiledao->getSubjectWiseTotalMarksForUnmerge("$testStringUnmerge",$subid1);
while ($rows = mysql_fetch_array($getSubjectWiseTotalMarksForUnmerge)) {

                $getSubjectWiseTotalMarksForUnmergeDetail[$k][0] = round($rows['MAX'],2);

                $getSubjectWiseTotalMarksForUnmergeDetail[$k][1] = round($rows['AVG'],2);

                 $k++;
            }
            $getSubjectWiseStudentRank=$profiledao->getSubjectWiseStudentRank("$testStringUnmerge",$subid1,$studentid);
    while ($rows = mysql_fetch_array($getSubjectWiseStudentRank)) {

                $getSubjectWiseStudentRankDetail[$l] = $rows['RANK'];
               $l++;
            }
           
}
$getSubjectWiseStudentRankDetail2=array();$jj=0;
for($i=0;$i<count($getSubjectWiseStudentRankDetail1);$i++){
if($getSubjectWiseStudentRankDetail1[$i]==1){
$getSubjectWiseStudentRankDetail2[$i]=$getSubjectWiseStudentRankDetail[$jj];
$jj++;
} else {
$getSubjectWiseStudentRankDetail2[$i]=0;

}
}

$mergeDataLength = count($getMixData);
if($_SESSION['BATCH_NAME']=='E'){
    $stream = "JEE(Main)";
} else if($_SESSION['BATCH_NAME'] == 'T'){
    $stream = "JEE(Main + Advanced)";
} else if($_SESSION['BATCH_NAME'] == 'M'){
    $stream = "Pre-Medical";
} else if($_SESSION['BATCH_NAME'] == 'A'){
    $stream = "Advanced Engineering";
} else if ($_SESSION['BATCH_NAME'] == "") {
    $stream = "NA";
} else {
    $stream = "Pre-Nurture";
}
$html = "";
$html = $html ."<html><head>";
$html = $html ."
<style>
        body{
        margin:0px ;
        padding:0px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
        background-color:#ffffff
    }
    .ineerDiv{
        position: relative;
        padding:0px;
        margin-top:0px;
        width:auto;
        font-size:11px;
        height:auto;
    }
    .header{
        color:#424242;font-weight: bold;font-size:9px;margin-top:20px
    }
    .header1{font-size: 16px;width:100%;color:#183883;}
    .header2{font-size: 10px;width:100%;color:#444444;font-weight: bold;margin-top:0px}
    .innerTable{
        border-collapse:collapse;
        font-size: 11px;
        line-height: 20px;
        text-align: center;
        border:1px solid #d4d4d4;
        color: black;
        background-color: white;
        vertical-align: middle;
        font-weight:bold !important ;
        width:100%;
    }
    .innerTable td {
        line-height: 15px;
        border: 1px solid #d4d4d4;
        font-weight:bold !important ;
        padding:3px;
    }
    .innerTable th {
        border: 1px solid #d4d4d4;
    }
    .class1{
        background-color: #f4f4f4;
    }
    .report_head{ background-color:#f4f4f4;}
    .class1 td{padding:3px}
    .class4 td{padding:3px;background-color: #e0ffff;}
</style><title>Reports</title><link rel='shortcut icon' href='../../../".Constantstudent::$favicon_image."' />
</head>";
$html = $html ."<body>";
    $html = $html ."<div class='ineerDiv' id='ineerDiv13'>";

        $html = $html."<div class='header'>";
                $html = $html."<div style='float:left;width:150px'>";
                    $html = $html."<img src='../../../student/images/allen logo.jpg' alt='Allen Logo' >";
                $html = $html."</div>";
                $html = $html."<div style='float:right;margin-right: 0px;text-align: right'>";
                $html = $html.Constantstudent::$ADDRESS;
                $html = $html."</div>";
        $html = $html."</div>";

        $html = $html."<div class='header1'>";
            $html = $html."<div style='margin: 12px 0px 12px 450px;'>";
                $html = $html."<b>Topic Performance Report</b>";
            $html = $html."</div>";
        $html = $html."</div>";

        $html = $html."<div class='header2'>";
            $html = $html."<div style='position: absolute;margin-left: 0px;'>";
                $html = $html."<div style='width:15%;float:left'>REG/ROLL NO&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$enrollMentNo."</div><div style='width:25%;float:left'>STUDENT NAME&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".ucwords(strtolower($_SESSION['studentName']))."</div><div style='width:27%;float:left'>COURSE&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$courseName."</div><div style='width:14%;float:left'>STREAM&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$stream."</div><div style='text-align:right;width:18%;float:left;'>STUDY CENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".Constantstudent::$STUDY_CENTER."</div>";
            $html = $html."</div>";
            $html = $html."<div style='margin-top:10px;display:none'><div style='float:left;width:10%;'>Test Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</div>";
                $html = $html."<div style='float:right;width:90%;'>";
                    $m4=0;for($m4=0;$m4<count($studentDataArray);$m4++){
                        $noOfMergeTestids = explode(",",$studentDataArray[$m4][0]);
                        if(count($noOfMergeTestids) == 1){
                            $html = $html ."<b>".$testNameAbbreviation[$studentDataArray[$m4][0]]."=<span>".$studentDataArray[$m4][4]."</span></b>&nbsp;&nbsp;&nbsp;&nbsp;";
                        } else{
                            $html = $html ."<b>".$testNameAbbreviation[$studentDataArray[$m4][0]]."=<span>".$studentDataArray[$m4][4]."(Paper1 & Paper2)</span></b>&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                    }
                $html = $html."</div>";
            $html = $html."</div>";
        $html = $html."</div>";



        $html = $html ."<div  style='margin-top:20px;width:100%'>";
            $html = $html ."<table class='innerTable'>";
                $html = $html ."<tr>";
                if($mergeDataLength>0){
                    $html = $html ."<th colspan='3'>Test Code</th>";
                }else{
                    $html = $html ."<th colspan='2'>Test Code</th>";

                }
                    $ct=0;
                    for($ct=0;$ct<count($studentDataArray);$ct++){
                        $colspan = 3;
                        $html = $html ."<th colspan='".$colspan."' bgcolor='#424242' color='#ffffff'>".$testNameAbbreviation[$studentDataArray[$ct][0]]."</th>";
                        $perArray[$ct][0] = $testNameAbbreviation[$studentDataArray[$ct][0]];
                    }
                $html = $html ."</tr>";
                $html = $html ."<tr>";
                 if($mergeDataLength>0){
                    $html = $html ."<th colspan='3'>Test Date</th>";
                 }else{
                     $html = $html ."<th colspan='2'>Test Date</th>";

                 }
                    $ct=0;
                    for($ct=0;$ct<count($studentDataArray);$ct++){
                        $colspan = 3;
                        $html = $html ."<td colspan='".$colspan."' ><b>".date('d M Y', strtotime($studentDataArray[$ct][3]))."</b></td>";
                    }
                $html = $html ."</tr>";
                $html = $html ."<tr>";
                 if($mergeDataLength>0){
                    $html = $html ."<th colspan='3'>Test Type</th>";
                 }else{
                      $html = $html ."<th colspan='2'>Test Type</th>";
                 }
                    $ct=0;
                    for($ct=0;$ct<count($studentDataArray);$ct++){
                        $examTypeVar = '';
                        if($studentDataArray[$ct][5] == '1'){
                            if($studentDataArray[$ct][1] == '1'){
                                $examTypeVar = "JEE(Main)";
                            }  else{
                                $examTypeVar = "JEE(Main+Advanced)";
                            }
                        } else if($studentDataArray[$ct][5] == '2'){
                            $examTypeVar = "JEE ( Main )";
                        } else if($studentDataArray[$ct][5] == '3'){
                            $examTypeVar = "AIEEE";
                        } else if($studentDataArray[$ct][5] == '4'){
                            $examTypeVar = "NEET-UG";
                        } else if($studentDataArray[$ct][5] == '5'){
                            $examTypeVar = "AIPMT";
                        } else if($studentDataArray[$ct][5] == '6'){
                            $examTypeVar = "AIIMS";
                        } else if($studentDataArray[$ct][5] == '7'){
                            $examTypeVar = "GUJCET";
                        } else if($studentDataArray[$ct][5] == '8'){
                            $examTypeVar = "STATE - PMT";
                        } else if($studentDataArray[$ct][5] == '9'){
                            $examTypeVar = "NTSE";
                        } else if($studentDataArray[$ct][5] == '10'){
                            $examTypeVar = "KVPY";
                        } else if($studentDataArray[$ct][5] == '11'){
                            $examTypeVar = "Other";
                        }
                        $colspan = 3;
                        $html = $html ."<td colspan='".$colspan."'><b>".$examTypeVar."</b></td>";
                    }
                $html = $html ."</tr>";
                $html = $html ."<tr>";
                    if(count($getMixDataArray)>0){
                        $html = $html ."<th rowspan='2' width='5%'>Subject Name</th>";
                        $html = $html ."<th rowspan='2' width='12%'>Topic Name</th>";
                        $html = $html ."<th rowspan='2' width='12%'>Paper #</th>";
                    } else{
                        $html = $html ."<th rowspan='2' width='5%'>Subject Name</th>";
                        $html = $html ."<th rowspan='2' width='12%'>Topic Name</th>";
                    }
                    $ct=0;
                    for($ct=0;$ct<count($studentDataArray);$ct++){
                        $colspan = 3;
                        $html = $html ."<td colspan='".$colspan."'><b>Test Detail</b></td>";
                    }
                $html = $html ."</tr>";
               
                $html = $html ."<tr>";
                    $ct=0;
                    for($ct=0;$ct<count($studentDataArray);$ct++){
                            $html = $html ."<td>Score</td>";
                            $html = $html ."<td>Avg.</td>";
                            $html = $html ."<td>M.M.</td>";
                       
                    }
                $html = $html ."</tr>";
                $ct=0;
                for($ct=0;$ct<count($topicArray);$ct++){
                    if($ct % 2 == 0){
                        $html = $html ."<tr class='class1'>";
                    } else{
                        $html = $html ."<tr class='class2 class4'>";
                    }
                    $kl2=0;
                    $subjectName = '';$subjectId='';$subSubjectName = '';
                    for($kl2=0;$kl2<count($getSubject);$kl2++){
                        if($getSubject[$kl2][0] == $topicArray[$ct][3]){
                            $subjectName = $getSubject[$kl2][1];
                            $subjectId = $getSubject[$kl2][0];
                            $total_topic = $getSubject[$kl2][2];
                        }
                    }
                    $kl2=0;
                    for($kl2=0;$kl2<count($getSubSubject);$kl2++){
                        if($getSubSubject[$kl2][0] == $topicArray[$ct][2]){
                            $subSubjectName = $getSubSubject[$kl2][1];
                        }
                    }
                    $initialsubject = $subjectId;
                    if($initialsubject!=$finalsubject){
                         if($mergeDataLength>0){
                             $ra=2*$total_topic;

                        $html = $html ."<th class='report_head' rowspan=".$ra."><b>".$subjectName."</b></th>";
                         }else{
                             $html = $html ."<th class='report_head' rowspan=".$total_topic."><b>".$subjectName."</b></th>";
                         }


                        }
 
                    if($subjectId == Constant::$IIT_CHEM && $topicArray[$ct][2]!=0){
                      if($mergeDataLength>0){
                        $html = $html ."<td rowspan='2'><b>".$topicArray[$ct][1]."(<span style='color:#305491;font-weight:bold'>".$subSubjectName."</span>)"."</b></td>";
                      }else{
                      $html = $html ."<td><b>".$topicArray[$ct][1]."(<span style='color:#305491;font-weight:bold'>".$subSubjectName."</span>)"."</b></td>";    
                          
                      }

                        } else{
                        if($mergeDataLength>0){
                        
                        $html = $html ."<td rowspan='2'><b>".$topicArray[$ct][1]."</b></td>";
                        }else{
                             $html = $html ."<td><b>".$topicArray[$ct][1]."</b></td>";
                            
                        }
                   }
                   if($mergeDataLength>0){
                       $html = $html ."<td>".Paper1."</td>";
                   }
                    $ct1=0;
                    for($ct1=0;$ct1<count($studentDataArray);$ct1++){
                        $found = 0;$topicAvg='';$topicMarks='';
                        if($studentDataArray[$ct1][2]!=0){
                            $mergeTestIdArray = array();
                            $mergeTestIdArray = explode(",", $studentDataArray[$ct1][0]);
                           
                                $ct2=0;
                                for($ct2=0;$ct2<count($topicWiseMarks);$ct2++){
                                    if($topicArray[$ct][0] == $topicWiseMarks[$ct2][0] && $mergeTestIdArray[0] == $topicWiseMarks[$ct2][2]){
                                        $found = 1;
                                        $topicMarks = $topicWiseMarks[$ct2][1];
                                        $topicMaxMarks = $topicWiseMarks[$ct2][3];
                                        $topicAvg = $topicWiseArray[$ct2][3];
                                    }
                                }
                                if($found == 0){
                                    $html = $html ."<td>-</td>";
                                    $html = $html ."<td>-</td>";
                                    $html = $html ."<td>-</td>";
                                } else{
                                    $html = $html ."<td>".round($topicMarks,2)."</td>";
                                    $html = $html ."<td>".round($topicAvg,2)."</td>";
                                    $html = $html ."<td>".round($topicMaxMarks,2)."</td>";
                                }
                            
                        } else{
                            $ct2=0;
                            for($ct2=0;$ct2<count($topicWiseMarks);$ct2++){
                                if($topicArray[$ct][0] == $topicWiseMarks[$ct2][0] && $studentDataArray[$ct1][0] == $topicWiseMarks[$ct2][2]){
                                    $found = 1;
                                    $topicMarks = $topicWiseMarks[$ct2][1];
                                    $topicMaxMarks = $topicWiseMarks[$ct2][3];
                                    $topicAvg = $topicWiseArray[$ct2][3];
                                }
                            }
                            if($found == 0){
                                $html = $html ."<td>-</td>";
                                $html = $html ."<td>-</td>";
                                $html = $html ."<td>-</td>";
                            } else{
                                $html = $html ."<td>".round($topicMarks,2)."</td>";
                                $html = $html ."<td>".round($topicAvg,2)."</td>";
                                $html = $html ."<td>".round($topicMaxMarks,2)."</td>";
                            }
                        }
                    }
                     $html = $html ."</tr>";
                     if($mergeDataLength>0){
                      $html = $html ."<tr>";
                      $html = $html ."<td>".Paper2."</td>";
                      $ct1=0;
                    for($ct1=0;$ct1<count($studentDataArray);$ct1++){
                        $found = 0;$topicAvg='';$topicMarks='';
                        if($studentDataArray[$ct1][2]!=0){
                            $mergeTestIdArray = array();
                            $mergeTestIdArray = explode(",", $studentDataArray[$ct1][0]);
                            
                                $ct2=0;
                                for($ct2=0;$ct2<count($topicWiseMarks);$ct2++){
                                    if($topicArray[$ct][0] == $topicWiseMarks[$ct2][0] && $mergeTestIdArray[1] == $topicWiseMarks[$ct2][2]){
                                        $found = 1;
                                        $topicMarks = $topicWiseMarks[$ct2][1];
                                        $topicMaxMarks = $topicWiseMarks[$ct2][3];
                                        $topicAvg = $topicWiseArray[$ct2][3];
                                    }
                                }
                                if($found == 0){
                                    $html = $html ."<td>-</td>";
                                    $html = $html ."<td>-</td>";
                                    $html = $html ."<td>-</td>";
                                } else{
                                    $html = $html ."<td>".round($topicMarks,2)."</td>";
                                    $html = $html ."<td>".round($topicAvg,2)."</td>";
                                    $html = $html ."<td>".round($topicMaxMarks,2)."</td>";
                                }
                            }
                         else{
                           
                                $html = $html ."<td>-</td>";
                                $html = $html ."<td>-</td>";
                                $html = $html ."<td>-</td>";
                            
                        }
                    }


                      

                         $html = $html ."</tr>";
                     }
                    $finalsubject = $subjectId;
                   
                }
                $totalTopic = count($topicArray);
                if($totalTopic%2 == 0){
                    $class1 = "class1";
                    $class2 = "class2 class4";
                } else{
                    $class1 = "class2 class4";
                    $class2 = "class1";
                }
                $html = $html ."<tr class='".$class1."'>";
                if($mergeDataLength>0){
                    $html = $html ."<td class='report_head' colspan='3'><b>Grand Total</b></td>";
                }else{
                    $html = $html ."<td class='report_head' colspan='2'><b>Grand Total</b></td>";
                }
                $ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $colspan = 3;
                            if($subid1!=all){
                            $grandTotalmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $grandTotalmarks  = $fegh[$lm1];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getSubjectWiseMarksForUnmergeDetail);$kl++){
                                    if($getSubjectWiseMarksForUnmergeDetail[$kl][3] == $studentDataArray[$ct][0]){

                                        $grandTotalmarks= $getSubjectWiseMarksForUnmergeDetail[$kl][0];
                                    }
                                }
                            }
                             $html = $html ."<td colspan='".$colspan."'>". round($grandTotalmarks,2)."</td>";
                            
                        }else{ 
                             $html = $html ."<td colspan='".$colspan."'>".round($studentDataArray[$ct][6],2)."</td>";
                        } }                    
                $html = $html ."</tr>";
                $html = $html ."<tr class='".$class2."'>";
                if($mergeDataLength>0){
                    $html = $html ."<td class='report_head' colspan='3'><b>Maximum Marks</b></td>";
                }else{
                    $html = $html ."<td class='report_head' colspan='2'><b>Maximum Marks</b></td>";
                    
                }

                 $ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $colspan = 3;
                            if($subid1!=all){
                            $grandTotalmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $grandTotalmarks  = $getMixDataArrayMerge[$lm1][3];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getSubjectWiseMarksForUnmergeDetail);$kl++){
                                    if($getSubjectWiseMarksForUnmergeDetail[$kl][3] == $studentDataArray[$ct][0]){
                                        $grandTotalmarks= $getSubjectWiseMarksForUnmergeDetail[$kl][1];
                                    }
                                }
                            }
                             $html = $html ."<td colspan='".$colspan."'>".round($grandTotalmarks,2)."</td>";
                            
                        }else{ 
                            $html = $html ."<td colspan='".$colspan."'>".round($studentDataArray[$ct][7],2)."</td>";
                        } }
                 $html = $html ."</tr>";
                $html = $html ."<tr class='".$class1."'>";
                if($mergeDataLength>0){
                    $html = $html ."<td class='report_head' colspan='3'><b>Percentage</b></td>";
                }else{
                    $html = $html ."<td class='report_head' colspan='2'><b>Percentage</b></td>";
                }
                
                  $ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $colspan = 3;
                            if($subid1!=all){
                            $grandTotalmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $grandTotalmarks  = $percentagemerge[$lm1];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getSubjectWiseMarksForUnmergeDetail);$kl++){
                                    if($getSubjectWiseMarksForUnmergeDetail[$kl][3] == $studentDataArray[$ct][0]){
                                        $grandTotalmarks=$getSubjectWiseMarksForUnmergeDetail[$kl][2];
                                    }
                                }
                            }
                              if($grandTotalmarks<0){
                               $grandTotalmarks=0 ;
                             }
                              $perArray[$ct][1] = $grandTotalmarks;

                            $html = $html ."<td colspan='".$colspan."'>".round($grandTotalmarks,2)."</td>";
                        }else{
                            $perArray[$ct][1]=round(($studentDataArray[$ct][6]/$studentDataArray[$ct][7])*100,2);
                            if($perArray[$ct][1]<0){ $perArray[$ct][1]=0; }
                             $html = $html ."<td colspan='".$colspan."'>".$perArray[$ct][1]."</td>";

                        } }
        
                $html = $html ."</tr>";
                $html = $html ."<tr class='".$class2."'>";
                 if($mergeDataLength>0){
                    $html = $html ."<td class='report_head' colspan='3'><b>Highest Marks</b></td>";
                 }else{
                     $html = $html ."<td class='report_head' colspan='2'><b>Highest Marks</b></td>";
                 }
$ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $colspan = 3;
                                if($subid1!=all){
                            $grandTotalmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $grandTotalmarks  = $feghij[$lm1];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getSubjectWiseMarksForUnmergeDetail);$kl++){
                                    if($getSubjectWiseMarksForUnmergeDetail[$kl][3] == $studentDataArray[$ct][0]){
                                        $grandTotalmarks=$getSubjectWiseTotalMarksForUnmergeDetail[$kl][0];
                                    }
                                }
                            }
                    $html = $html ."<td colspan='".$colspan."'>".round($grandTotalmarks,2)."</td>";
                            
                        }else{
                            $highestmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $highestmarks = $getMixDataArray[$lm1][1];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getTestMixData);$kl++){
                                    if($getTestMixData[$kl][0] == $studentDataArray[$ct][0]){
                                        $highestmarks = $getTestMixData[$kl][2];
                                    }
                                }
                            }
                             $html = $html ."<td colspan='".$colspan."'>".round($highestmarks,2)."</td>";
                        }}                 
                $html = $html ."</tr>";
                $html = $html ."<tr class='".$class1."'>";
                if($mergeDataLength>0){

                    $html = $html ."<td class='report_head' colspan='3'><b>Average Marks</b></td>";
                }else{
                    $html = $html ."<td class='report_head' colspan='2'><b>Average Marks</b></td>";
                    
                }
                   $ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
 $colspan = 3;
                             if($subid1!=all){
                            $grandTotalmarks = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $grandTotalmarks  = $average[$lm1];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getSubjectWiseMarksForUnmergeDetail);$kl++){
                                    if($getSubjectWiseMarksForUnmergeDetail[$kl][3] == $studentDataArray[$ct][0]){
                                        $grandTotalmarks= $getSubjectWiseTotalMarksForUnmergeDetail[$kl][1];
                                    }
                                }
                            }
$html = $html ."<td colspan='".$colspan."'>".round($grandTotalmarks,2)."</td>";
                            
                        }else{
                            $totalAvg = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $totalAvg = $getMixDataArray[$lm1][2];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getTestMixData);$kl++){
                                    if($getTestMixData[$kl][0] == $studentDataArray[$ct][0]){
                                        $totalAvg     = round($getTestMixData[$kl][1],2);
                                    }
                                }
                            }
                            $html = $html ."<td colspan='".$colspan."'>".round($totalAvg,2)."</td>";
                        } }
                $html = $html ."</tr>";
               
                $html = $html ."<tr class='".$class1."'>";
                 if($mergeDataLength>0){
                    $html = $html ."<td colspan='3'><b>Rank / Total Students</b></td>";
                 }else{
                     $html = $html ."<td colspan='2'><b>Rank / Total Students</b></td>";
                 }
                   $ct=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $outOfStudent = '';
                            if($studentDataArray[$ct][2]!=0){
                                $lm1=0;
                                for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                    if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                        $outOfStudent = $getMixDataArray[$lm1][3];
                                    }
                                }
                            } else{
                                $kl=0;
                                for($kl=0;$kl<count($getTestMixData);$kl++){
                                    if($getTestMixData[$kl][0] == $studentDataArray[$ct][0]){
                                        $outOfStudent = $getTestMixData[$kl][3];
                                    }
                                }
                            }
                             $colspan = 3;
                            if($subid1!=all){

                       if($getSubjectWiseStudentRankDetail2[$ct]!=0 && $getSubjectWiseStudentRankDetail2[$ct]!=''){
          $html = $html ."<td colspan='".$colspan."'>".$getSubjectWiseStudentRankDetail2[$ct]."/<b>".$outOfStudent."</bn></td>";
        }elseif($studentDataArray[$ct][9]!=0 && $studentDataArray[$ct][9]!=''){

               $html = $html ."<td colspan='".$colspan."'>".$studentDataArray[$ct][9]."/<b>".$outOfStudent."</bn></td>";
              }else{
            $html = $html ."<td colspan='".$colspan."'>NA</td>";

                                           }

                   } else {
                       if($studentDataArray[$ct][9]!=0 && $studentDataArray[$ct][9]!=''){

               $html = $html ."<td colspan='".$colspan."'>".$studentDataArray[$ct][9]."/<b>".$outOfStudent."</bn></td>";
              }else{
            $html = $html ."<td colspan='".$colspan."'>NA</td>";

                                           }


                   }
                       
                        
                    }
                $html = $html ."</tr>";
            $html = $html ."</table>";

            $html = $html ."<br><div>".stripslashes($_REQUEST['tprgraphsvg'])."</div>";
        $html = $html ."</div>";

    $html = $html ."</div>";
    $html = $html."<div style='padding:10px'><span style='color:red;font-weight:bold'>Note:- </span><b>M.M. - Maximum Marks | Avg. - Average Marks </b></div>";
$html = $html ."</body></html>";
//echo $html;
//exit();
$mpdf=new mPDF('utf-8', 'A4-L','','',10,10,10,10,10,10,'L');
$mpdf->mirrorMargins = 1;

$mpdf->defaultheaderfontsize = 10;
$mpdf->defaultheaderfontstyle = B;
$mpdf->defaultheaderline = 1;
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->defaultfooterfontsize = 12;
$mpdf->defaultfooterfontstyle = B;
$mpdf->defaultfooterline = 1;

$mpdf->SetHTMLFooter($header);
$mpdf->SetHTMLFooter($header,'E');
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->showImageErrors = true;
$mpdf->Output('Reports.pdf','I');
ob_end_flush();
?>