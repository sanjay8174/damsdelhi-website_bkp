<?php
require_once "./aws/aws-autoloader.php";

use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;
class funDams extends Database{

    function getCountry(){
        $sql="Select * from COUNTRY";
        $result=parent::executeQuery($sql);
        return $result;
    }
    function getState(){
        $sql="Select * from STATE";
        $result=parent::executeQuery($sql);
        return $result;
    }

    function getCity($id=null){
        if($id!=''){
           $whereState="where STATE_ID=$id";
        }
        $sql="Select * from CITY ".$whereState;
        $result=parent::executeQuery($sql);
        return $result;
    }

    function getCenter(){
        $sql="Select * from RFNE_REGION";
        $result=parent::executeQuery($sql);
        return $result;
    }
 function getStudentAllData($getId){
        $sql = "SELECT S.*,ST.CENTRE_ID,SAT.STATE_NAME,C.COUNTRY_NAME,R.NAME AS REGION_NAME,TS.SESSION_ID FROM STUDENT S LEFT JOIN STUDENT_TEST ST ON S.STUDENT_ID=ST.STUDENT_ID LEFT JOIN STATE SAT ON S.cSTATE_ID=SAT.STATE_ID LEFT JOIN COUNTRY C ON C.COUNTRY_ID=S.cCOUNTRY_ID LEFT JOIN REGION R ON S.REGION1=R.REGION_ID left JOIN TEST_SESSION TS ON ST.TEST_SESSION_ID=TS.TEST_SESSION_ID WHERE S.STUDENT_ID='$getId' limit 0,1";
        $result = parent::executeQuery($sql);
        return mysql_fetch_object($result);
    }
    function sendConfirmationMail($name,$email,$password,$dob,$mobile,$gender,$college,$StudentType,$rollNo){      
    ini_set("max_execution_time", "2000");
        ini_set('memory_limit', '512M');
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('d M,Y');
        include 'emailManDrill/Mandrill.php';
        $apikey = 'Q-RyOsLjBKHK_Zh5wW7Wqg';
        $mandrill = new Mandrill($apikey);
        //$email = "ranjan.kumar@gingerwebs.in";
        $txtemail = "donotreply@damsdelhi.com";
        $from = "DAMS MDS-CBT";
        $subjectText = "Registration confirmation link";
        $recieveEmail = array($email,'tejpal@damsdelhi.com','usmle@damsdelhi.com');
        $content = "<html xmlns='https://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Registration Confirmation</title>
<style>
p{font-size:12px;word-wrap: normal;}
</style>
</head>
<body>
<div>
<p> Dear $name,</p>
<p>Thank you for registering with DAMS USMLE Edge.</p>

<p><b>First time in India, DAMS is the only institute to conducting the USMLE Self-Assessment Exam from 22 Aug to 29 Aug 2016. It is a COMPUTER BASED TEST full length USMLE exam. The ultimate MOCK test will be conducted in our designated centers on USMLE exam software. This will be based on the Item response theory and other guidelines provided by ECFMG for the USMLE Exam.</b></p>

<p><b>Exam will be conducted over an 8 hours with 7 slots. Each Slot will be for an hour and the break time of 1 hour can be taken by the student as in the real exam.</b></p>

<p><b>The results will be analyzed with equating and scaling as per the actual exam guidelines. It will be an opportunity to see and predict your final performance in an exam like set up.</b></p>
<p>The MOCK USMLE Exam will predict the relative importance of all the subjects that comprise the USMLE syllabus. The relative importance of each subject depends on not only the number of questions asked from each subject but also on the merit given to each question of that subject. The software will compute and tell you based on your performance the level of expertise in each subject.</b></p>

<p>To confirm your seat please fill up the form you will get it on your mail id for the payment. If you have not received any mail for the payment then please mail us on usmle@damsdelhi.com</p>

<p><b>Or contact undersigned.</b></p>

<p>DAMS USMLE Edge </p>

<p >011-40094009</p>

<p style='color:red;'><b>Student Details</b></p>
<p><b>Student Type:</b>$StudentType</p>
<p><b>RollNo:</b>$rollNo</p>
<p><b>Name:</b>$name</p>
<p><b>Date of Birth:</b>$dob</p>
<p><b>Email:</b>$email</p>
<p><b>Mobile:</b>$mobile</p>
<p><b>Gender:</b>$gender</p>
<p><b>College:</b>$college</p>
<p>
</div>                     
</body>
</html>";
        
     $emailArray = array($email);   
        for($i=0;$i<count($emailArray);$i++){
        
        $mailTextData = "$content";

        $mailTextData = str_replace("\n.", "\n..", $mailTextData);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        $maildao=new funDams();
        for($i=0;$i<=2;$i++){
        $msgId = $maildao->sendMail($recieveEmail[$i], $mailTextData,$subjectText);
        }
//        $message = array(
//            "html" => "$mailTextData",
//            "subject" => "$subjectText",
//            "from_email" => "$txtemail",
//            "from_name" => "$from",
//            "to" => array(
//                array(
//                    "email" => "$emailArray[$i]"
//                )
//            ),
////        "headers" => array("Reply-To" => "info@thinkexams.in"),
//            "important" => true,
//            'track_opens' => false,
//            'track_clicks' => false,
//            'auto_text' => false,
//            'auto_html' => false,
//            'inline_css' => true,
//            'url_strip_qs' => false,
//            "async" => false,
//            "send_at" => "$date"
//        );
//        $datetime = date('d-m-Y');
//        try{
//        $response = $mandrill->messages->send($message, true);
//        }  catch (Exception $ee){
//            return $ee;
//        }
        }
    }
    function sendConfirmationSMS($candiadateName,$email,$password,$mobile){
        $getName = explode(" ",$candiadateName);
        $candiadateName = $getName[0];
         $msg='Dear '.$candiadateName.',%0AThank you very much for REGISTRATION DAMS MDS-CBT.'
                . '%0AEMAIL_ID - '. $email.
                ' %0APASSWORD-'.$password.''
                . ' ,%0Ago to the link goo.gl/0EPLgP.';
        $msg = urlencode($msg);
        $url = "https://web.insignsms.com/api/sendsms?username=damstrans&password=damstrans&senderid=DAMSPG&message=$msg&numbers=$mobile&dndrefund=1";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
    }
    function getWorkDetail(){
        $sql = "SELECT * FROM WORK_EXPERIENCE_DETAIL WHERE ACTIVE=1";
        $result = parent::executeQuery($sql);
        return $result;
    }
    function getStudentWorkDetail($studentId){
        $sql = "select * from WORK_EXPERIENCE where STUDENT_ID='$studentId'";
        $result = parent::executeQuery($sql);
        return $result;
    }
    function getProgramDetail(){
        $sql = "select * from PROGRAM_DETAIL where ACTIVE=1";
        $result = parent::executeQuery($sql);
        return $result;
    }
    function getExamCentre(){
        $sql = "select * from REGION where PUBLISH=1 order by NAME";
        $result = parent::executeQuery($sql);
        return $result;
    }
   function getProgramStudentDetail($studentId){
        $sql = "SELECT * FROM PROGRAM WHERE STUDENT_ID='$studentId'";
        $result = parent::executeQuery($sql);
        return $result;
    }
   function getPaymentGateway($studentId){
        $sql = "SELECT * FROM PAYMENT_GATEWAY WHERE ID='1'";
        $result = parent::executeQuery($sql);
        return $result;
    }
   function getStudentProfile($studentId){
        $sql = "SELECT S.*,STATE_NAME FROM STUDENT S LEFT JOIN STATE ST ON S.cSTATE_ID=ST.STATE_ID  WHERE STUDENT_ID='$studentId'";
        $result = parent::executeQuery($sql);
        return $result;
    }
    function insertNewOrderId($studentId, $packageId, $extension, $onlineCost, $packAllCost) {

        $date = date('Y-m-d');
        $time = date('H:i:s');
        $arrayOrder = array("STUDENT_ID" => "$studentId", "PACKAGE_ID" => "$packageId", "EXTENSION" => "$extension", "TRAN_TYPE" => "I", "DATE" => "$date", "TIME" => "$time", "ONLINE_COST" => "$onlineCost");
//        print_r($arrayOrder);exit();
        parent::insertAssociativeArray('ONLINE_TRANSACTION', $arrayOrder);
        $lastInsertID = mysql_insert_id();
        return $extension . $lastInsertID;
    }
    function getOriginalOrderId($orderID) {
        $sql = "select ORDER_ID,concat(EXTENSION,ORDER_ID) AS ORG_ORDER_ID from ONLINE_TRANSACTION having ORG_ORDER_ID='" . $orderID . "' limit 0,1";
        $result = parent::executeQuery($sql);
        $row = mysql_fetch_object($result);
        return $row->ORDER_ID;
    }
    function updateOrderId($orderId, $orderIdStaus) {
        $date = date("Y-m-d");
        parent::executeQuery("update ONLINE_TRANSACTION SET TRAN_TYPE='" . $orderIdStaus . "' where ORDER_ID=" . $orderId);
    }
    function insertIntoPaymentRec($orderId,$studentid){
        $studentStatus=$this->getStatus($studentid);
        if($studentStatus[0]==14){
             $this->updateStatus($studentid);
        }
        $sql="select OT.STUDENT_ID,OT.PACKAGE_ID,P.COST from ONLINE_TRANSACTION OT LEFT JOIN PACKAGE P on OT.PACKAGE_ID=P.PACKAGE_ID where OT.ORDER_ID=$orderId limit 0,1";
        $result=parent::executeQuery($sql);
        $rows=mysql_fetch_object($result);
            $packagecost=$rows->COST;
            $packageId=$rows->PACKAGE_ID;
            $date = date("Y/m/d");
            $today = date("H:i:s");
            $invoiceno=0;
            $randomNo = '1234567890';
            $string1 = str_shuffle($randomNo);
            $transactionno = $orderId . "=" . $packagecost;
            $packageTransactionId = $string1{0} . $string1{1} . $string1{2} . $string1{3} . $string1{4};
            $packageTransactionId = "ONL" . $packageTransactionId;
            $paymentreceived = array("RECEIPT_DATE" => "$date", "INVOICE_NO" => $invoiceno, "RECEIPT_DETAIL" => "v", "PAYMENT_MODE" => "o", "TRANSACTION_NO" => "$transactionno",
            "AMOUNT" => $packagecost, "TRANSACTION_ID" => "$packageTransactionId", "TIME" => $today, "STUDENT_ID" => "$studentid", "PACKAGE_ID" => $packageId,"TYPE"=>"P","COUPON_STATUS"=>"1");
            if(parent::duplicateField("PAYMENT_RECEIVED", "STUDENT_ID","STUDENT_ID=$studentid AND PACKAGE_ID=$packageId")==0){
            $this->insertPaymentReceived($paymentreceived);
        }
            
         //   $this->insertPaymentReceived($paymentreceived);
//            $this->sendMailOfSuccess($orderId);/*Add by Gaurav for send mail on success on-11/2/2014*/
    }
    function insertPaymentReceived($paymentreceived) {
        parent::insertAssociativeArray('PAYMENT_RECEIVED', $paymentreceived);
    }
    function packCost($packageid) {
        $sql=parent::executeQuery("SELECT COST FROM PACKAGE WHERE PACKAGE_ID='$packageid' limit 0,1");
        $row = mysql_fetch_object($sql);
        return $row->COST;
    }
    function sendMailOfPaymentSuccess($presentOrderId,$name,$email,$studentId){ 
    $sql2 = parent::executeQuery("SELECT * FROM PAYMENT_GATEWAY WHERE ID='1' ");
    $result2 = mysql_fetch_object($sql2);
    $ext = $result2->EXTENSION;
    $presentOrderId=$ext.$presentOrderId; 
    
    $sql = parent::executeQuery("SELECT S.STUDENT_NAME,S.EMAIL,S.MOBILE,S.ENROLLMENT_NO,S.DAMOSIAN,R.NAME,PR.AMOUNT"
            . " FROM STUDENT S LEFT JOIN REGION R ON S.REGION1=R.REGION_ID LEFT JOIN PAYMENT_RECEIVED PR"
            . " ON S.STUDENT_ID=PR.STUDENT_ID WHERE S.STUDENT_ID='$studentId'");
    $result = mysql_fetch_object($sql);
    $name = $result->STUDENT_NAME;
    $email = $result->EMAIL;
    $mobile = $result->MOBILE;
    $enrollmentNo = $result->ENROLLMENT_NO;
    $region = $result->NAME;
    $damosian = $result->DAMOSIAN;
    $Amount=$result->AMOUNT;
    
    if($damosian==1){
        $html = "<html xmlns='https://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Registration Confirmation</title>
<style>
p{font-size:12px;word-wrap: normal;}
</style>
</head>
<body>
<div>
<center><h3>DAMS</h3></center>
<p>Your Payment is received by us</span>.</p>
<p>
Detail of payment is as given below.
<br />
Name - $name <br>
Email Id - $email<br>
Mobile - $mobile <br>
Enrollment Number - $enrollmentNo <br>
Region - $region <br>
Order Id - $presentOrderId<br>
Amount - $Amount<br>
</p>

<p>
Admit Card: After the last date for receipt of applications, you will be allotted Centre as per the availability of seats.
</p>
<br />
<br />
<br />
</div>                     
</body>
</html>";
    }else{
        $html = "<html xmlns='https://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Registration Confirmation</title>
<style>
p{font-size:12px;word-wrap: normal;}
</style>
</head>
<body>
<div>
<center><h3>DAMS</h3></center>
<p>Your Payment is received by us</span>.</p>

<p>
Detail of payment is as given below.
<br />
Name - $name <br>
Email Id - $email<br>
Mobile - $mobile <br>
Region - $region <br>
Order Id - $presentOrderId<br>
Amount - $Amount<br>
</p>

<p>
Admit Card: After the last date for receipt of applications, you will be allotted Centre as per the availability of seats.
</p>
<br />
<br />
<br />
</div>                     
</body>
</html>";
    }
       ini_set("max_execution_time", "2000");
        ini_set('memory_limit', '512M');
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('d M,Y');
        include 'emailManDrill/Mandrill.php';
        $apikey = '1qa7srdTBN-Ib_QiwHxmHg';
        $mandrill = new Mandrill($apikey);
        //$email = "harshvardhan.gupta@gingerwebs.in";
        $txtemail = "donotreply@damsdelhi.com";
        $from = "DAMS";
        $subjectText = "Payment Confirmation $presentOrderId";
        $content = "$html";
        $mailTextData = "$content";
        $emailArray = array($email);
        
        $mailTextData = str_replace("\n.", "\n..", $mailTextData);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        $maildao=new funDams();
        for($j=0;$j<count($emailArray);$j++){
      
        $msgId = $maildao->sendMail($emailArray[$j], $mailTextData,$subjectText);
//        $message = array(
//            "html" => "$mailTextData",
//            "subject" => "$subjectText",
//            "from_email" => "$txtemail",
//            "from_name" => "$from",
//            "to" => array(
//                array(
//                    "email" => "$emailArray[$j]"
//                )
//            ),
////        "headers" => array("Reply-To" => "info@thinkexams.in"),
//            "important" => true,
//            'track_opens' => false,
//            'track_clicks' => false,
//            'auto_text' => false,
//            'auto_html' => false,
//            'inline_css' => true,
//            'url_strip_qs' => false,
//            "async" => false,
//            "send_at" => "$date"
//        );
//        $datetime = date('d-m-Y');
//        try{
//        $response = $mandrill->messages->send($message, true);
//        }  catch (Exception $ee){
//            return $ee;
//        }
        
        }     
        
    }
    function checkCoupon($couponNo,$cost){
        $sql="SELECT * FROM COUPON WHERE LOWER(COUPON_NO)='$couponNo' AND AMOUNT='$cost' AND STATUS='1' limit 0,1";
        $result = parent::executeQuery($sql);
        return $result;
    }
    function paymentRecCheck($studentId,$packageId){
        $sql="SELECT * FROM PAYMENT_RECEIVED WHERE STUDENT_ID=$studentId AND PACKAGE_ID=$packageId";
        $result = parent::executeQuery($sql);
        return $result;
    }
   function getAllCoupon(){
        $sql="SELECT * FROM PAYMENT_RECEIVED";
        $result = parent::executeQuery($sql);
        return $result;
    }
  function entryCoupon($couponNo,$studentId,$packageId){
        $sql="SELECT MAX(INVOICE_NO) MAXCOUNT FROM PAYMENT_RECEIVED";
        $result=parent::executeQuery($sql);
        while($row=  mysql_fetch_array($result)){
            $maxcount = $row['MAXCOUNT'];
        }

        $sql="SELECT COUPON_NO,AMOUNT FROM COUPON WHERE LOWER(COUPON_NO)='$couponNo'";
        $result=parent::executeQuery($sql);
        while($row=  mysql_fetch_array($result)){
            $amount = $row['AMOUNT'];
            $couponNo = $row['COUPON_NO'];
        }

        $maxcount=$maxcount+1;
        $receiptDate=date('Y-m-d');
        $invoiceNo=$maxcount;
        $paymentMode='c';
        $transactionNo=$couponNo.'='.$amount;
        $transactionId='CPN'.rand(80000, 999999);
        $time = date('h:m:s');
        $type='P';
        $validity=0;
        $couponStatus=1;



        $sql="INSERT INTO PAYMENT_RECEIVED (RECEIPT_DATE,INVOICE_NO,PAYMENT_MODE,TRANSACTION_NO,AMOUNT,TRANSACTION_ID,TIME,STUDENT_ID,PACKAGE_ID,TYPE,VALIDITY,COUPON_STATUS) VALUES('$receiptDate',$invoiceNo,'$paymentMode','$transactionNo',$amount,'$transactionId','$time',$studentId,$packageId,'$type',$validity,$couponStatus)";
        $result = parent::executeQuery($sql);
        $paymentId=mysql_insert_id();
        $sqlUpdateC="UPDATE COUPON SET CUSTOMER_ID=$studentId ,STATUS=0,SALE_DATE=CURDATE() WHERE LOWER(COUPON_NO)='$couponNo' ";
        $resultUpdateC = parent::executeQuery($sqlUpdateC);
        $sqlUpdate = "UPDATE STUDENT SET STATUS=7 , AUTHENTICATIONSTATUS=1 WHERE STUDENT_ID=$studentId";
        $resultUpdate = parent::executeQuery($sqlUpdate);
        $this->sendMailOfCoupanSuccess($paymentId,$couponNo,$amount,$_SESSION['studNmeReg'],$_SESSION['studEmlReg'],$studentId);
        return $result;
    }
    public function updateAppId($appID){
        $sql ="INSERT INTO APPLICATION_ID (`VALUE_1`) values($appID)";
        $result = parent::executeQuery($sql);
        return $result;
    }
    public function sendMailOfCoupanSuccess($paymentId,$couponNo,$amount,$name,$email,$studentId){
    $sql = parent::executeQuery("SELECT S.STUDENT_NAME,S.EMAIL,S.MOBILE,S.ENROLLMENT_NO,S.DAMOSIAN,R.NAME FROM STUDENT S LEFT JOIN REGION R ON S.REGION1=R.REGION_ID WHERE S.STUDENT_ID='$studentId'");
    $result = mysql_fetch_object($sql);
    $name = $result->STUDENT_NAME;
    $email = $result->EMAIL;
    $mobile = $result->MOBILE;
    $enrollmentNo = $result->ENROLLMENT_NO;
    $region = $result->NAME;
    $damosian = $result->DAMOSIAN;
    
    if($damosian==1){
        $html = "<html xmlns='https://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Registration Confirmation</title>
<style>
p{font-size:12px;word-wrap: normal;}
</style>
</head>
<body>
<div>
<center><h3>DAMS</h3></center>
<p>Your Payment is received by us</span>.</p>
<p>
Detail of payment is as given below.
<br />
Name - $name <br>
Email Id - $email<br>
Mobile - $mobile <br>
Enrollment Number - $enrollmentNo <br>
Region - $region <br>
Coupon No. - $couponNo<br>
Amount - $amount<br>
</p>

<p>
Admit Card: After the last date for receipt of applications, you will be allotted Centre as per the availability of seats.
</p>
<br />
<br />
<br />
</div>                     
</body>
</html>";
    }else{
        $html = "<html xmlns='https://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Registration Confirmation</title>
<style>
p{font-size:12px;word-wrap: normal;}
</style>
</head>
<body>
<div>
<center><h3>DAMS</h3></center>
<p>Your Payment is received by us</span>.</p>

<p>
Detail of payment is as given below.
<br />
Name - $name <br>
Email Id - $email<br>
Mobile - $mobile <br>
Region - $region <br>
Coupon No. - $couponNo<br>
Amount - $amount<br>
</p>

<p>
Admit Card: After the last date for receipt of applications, you will be allotted Centre as per the availability of seats.
</p>
<br />
<br />
<br />
</div>                     
</body>
</html>";
    }
    
      ini_set("max_execution_time", "2000");
        ini_set('memory_limit', '512M');
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('d M,Y');
        include 'emailManDrill/Mandrill.php';
        $apikey = '1qa7srdTBN-Ib_QiwHxmHg';
        $mandrill = new Mandrill($apikey);
        //$email = "harshvardhan.gupta@gingerwebs.in";
        $txtemail = "donotreply@damsdelhi.in";
        $from = "DAMS";
        $subjectText = "Payment Confirmation $couponNo";
        $content ="$html";
        $mailTextData = "$content";
        $emailArray = array($email);
        
        $maildao=new funDams();
        for($j=0;$j<count($emailArray);$j++){
        $mailTextData = str_replace("\n.", "\n..", $mailTextData);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        
        $msgId = $maildao->sendMail($emailArray[$j], $mailTextData,$subjectText);
//        $message = array(
//            "html" => "$mailTextData",
//            "subject" => "$subjectText",
//            "from_email" => "$txtemail",
//            "from_name" => "$from",
//            "to" => array(
//                array(
//                    "email" => "$emailArray[$j]"
//                )
//            ),
////        "headers" => array("Reply-To" => "info@thinkexams.in"),
//            "important" => true,
//            'track_opens' => false,
//            'track_clicks' => false,
//            'auto_text' => false,
//            'auto_html' => false,
//            'inline_css' => true,
//            'url_strip_qs' => false,
//            "async" => false,
//            "send_at" => "$date"
//        );
//        $datetime = date('d-m-Y');
//        try{
//        $response = $mandrill->messages->send($message, true);
//        }  catch (Exception $ee){
//            return $ee;
//        }  
        }
    }
    
  /************ forgot pass ***********/ 
    
    function getForgotPassword($email)
    {
        $sql="SELECT STUDENT_NAME,PASSWORD FROM STUDENT WHERE EMAIL='$email' limit 0,1";
        $result = parent::executeQuery($sql);
        return $result;
    }

    function ForgotPasswordMail($name,$email,$password){
        ini_set("max_execution_time", "2000");
        ini_set('memory_limit', '512M');
        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('d M,Y');
        include 'emailManDrill/Mandrill.php';
        $apikey = '1qa7srdTBN-Ib_QiwHxmHg';
        $mandrill = new Mandrill($apikey);
        //$email = "rohit.kumar@gingerwebs.in";
        $txtemail = "donotreply@damsdelhi.com";
        $from = "DAMS";
        $subjectText = "Reset Password";
        $content = "<html xmlns='https://www.w3.org/1999/xhtml'>
            <head>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
                <title>Registration Confirmation</title>
                <style>
                p{font-size:12px;word-wrap: normal;}
                </style>
                 </head>
                <body>
                <div>
                <center><h3>DAMS</h3></center>
        </div>
        <div>
        <h4>Dear $name,</p></h4>
        We have received the request for the recovery of your password.<br />
        your email and password is as below

               <p>User_Id-<span>$email</span>
                <br />
                Password-<span>$password</span>
                </p>
                <p>DAMS</p>
                </div>
                </body>
                </html>";

     $mailTextData = "$content";

        $mailTextData = str_replace("\n.", "\n..", $mailTextData);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        $maildao = new funDams();
        $msgId = $maildao->sendMail($email, $mailTextData, $subjectText);

//        $message = array(
//            "html" => "$mailTextData",
//            "subject" => "$subjectText",
//            "from_email" => "$txtemail",
//            "from_name" => "$from",
//            "to" => array(
//                array(
//                    "email" => "$email"
//                )
//            ),
////        "headers" => array("Reply-To" => "info@thinkexams.in"),
//            "important" => true,
//            'track_opens' => false,
//            'track_clicks' => false,
//            'auto_text' => false,
//            'auto_html' => false,
//            'inline_css' => true,
//            'url_strip_qs' => false,
//            "async" => false,
//            "send_at" => "$date"
//        );
//        $datetime = date('d-m-Y');
//        try{
//        $response = $mandrill->messages->send($message, true);
//        }catch(Exception $ee){
//            echo $ee;
//            
//        }
    }
    
    /**********end here  forgot *********/////////////
     public function updateStatus($studentid) {
        $query = "update STUDENT set STATUS=7 where STUDENT_ID=$studentid";
        parent::executeQuery($query);
    }
public function getStatus($studentid) {
        $array=array();
        $sqlforenroll = "select STATUS,EMAIL from STUDENT where STUDENT_ID=$studentid";
        $result = parent::executeQuery($sqlforenroll);
        $rows = mysql_fetch_object($result);
        $array[0]=$rows->STATUS;
        $array[1]=$rows->EMAIL;
        return $array;
    }
    public function getCentre($region){
        $i=0;
        $data = array();
        
        $sql=parent::executeQuery("SELECT C.*,SUM(L.SEATS) SEATS,GROUP_CONCAT(DISTINCT(TS.SESSION_ID)) SESSIONS,GROUP_CONCAT(DISTINCT(SE.NAME)) SESSION_NAMES,GROUP_CONCAT(DISTINCT(CONCAT(TIME_FORMAT(SE.START_TIME,'%h:%i %p'),'-',TIME_FORMAT(SE.END_TIME,'%h:%i %p')))) SESSION_TIMES FROM CENTRE C LEFT JOIN LAB L ON C.CENTRE_ID = L.CENTRE_ID LEFT JOIN TEST_SESSION TS ON TS.LAB_ID=L.LAB_ID LEFT JOIN SESSION SE ON TS.SESSION_ID=SE.SESSION_ID WHERE C.REGION_ID='$region' and C.PUBLISH=1 GROUP BY C.CENTRE_ID order by C.NAME,SE.NAME");
        while($row = mysql_fetch_array($sql)){
            $data[$i][0] = $row['CENTRE_ID'];
            $data[$i][1] = $row['NAME'];
            $data[$i][2] = $row['ADDRESS'];
            $data[$i][3] = $row['LANDMARK'];
            $data[$i][4] = $row['SEATS'];
            
            //gives the total student associated with the centre
            $get = parent::executeQuery("CALL STUDENT_ASSO_IN_CENTRE(".$row['CENTRE_ID'].",@result);");
            $procResult = mysql_fetch_array(parent::executeQuery("select @result"));
            $data[$i][5] = $procResult[0];
            $data[$i][6] = $row['SESSIONS'];
            
            $sessionIds = explode(',',$data[$i][6]);
            $sessionCount = "";$km=0;
            for($j=0;$j<sizeOf($sessionIds);$j++) :
                $sesFun = parent::executeQuery("SELECT SUM(L.SEATS) AS SEATS_COUNT,(SELECT COUNT(STUDENT_ID) FROM STUDENT_TEST ST WHERE ST.TEST_SESSION_ID = TS.TEST_SESSION_ID) AS  STUDENT_CNT FROM LAB L LEFT JOIN TEST_SESSION TS ON L.LAB_ID = TS.LAB_ID WHERE L.CENTRE_ID = '".$data[$i][0]."' AND TS.SESSION_ID = '$sessionIds[$j]' limit 0,1");
                $funcResult = mysql_fetch_array($sesFun);
                if($km == 0 ) :
                    $sub = $funcResult['SEATS_COUNT']-$funcResult['STUDENT_CNT'];
                    $sessionCount = $sub;
                else :
                    $sub = $funcResult['SEATS_COUNT']-$funcResult['STUDENT_CNT'];
                    $sessionCount = $sessionCount.",".$sub;
                endif;
                $km++;
            endfor;
            $data[$i][7] = $row['SESSION_NAMES'];
            $data[$i][8] = $row['SESSION_TIMES'];
            $data[$i][9] = $sessionCount;
            $i++;
        }
        echo json_encode($data);
    }
    
    public function getCentreCount($centre,$sessionId){
        $i=0;
        $data = array();
        $sql=parent::executeQuery("SELECT CENTRESEATCOUNT($centre) AS CNT");
        while($row = mysql_fetch_array($sql)){
            $data[$i][0] = $row['CNT'];
            $get = parent::executeQuery("SELECT CENTRESCOUNT($centre,$sessionId) AS CNTSESSION;");
            $procResult = mysql_fetch_array($get);
            $data[$i][1] = $procResult[0];
            $i++;
        }
        echo json_encode($data);
    }
    
     public function getCentreCountSessionWise($centre,$sessionId){
        $i=0;
        $data = array();
        //gives total seats into the lab of the given centre id
        $sql=parent::executeQuery("SELECT SEATS_IN_LAB_OF_CENTRE($centre) AS CNT;");
        while($row = mysql_fetch_array($sql)){
            $data[$i][0] = $row['CNT'];
            //gives total seats in the selected centre of particular session
            $get = parent::executeQuery("SELECT SEATS_IN_SELECTED_SESSION($centre,$sessionId) AS CNTSESSION;");
            $procResult = mysql_fetch_array($get);
            $data[$i][1] = $procResult[0];
            $i++;
        }
        echo json_encode($data);
    }
    
    public function associateStudentWithCentre($eventId,$centre,$studentId,$appId,$sessionId,$regionId){
        $check='';
        $totalData = parent::executeQuery("SELECT TS.EVENT_MANAGE_ID,EM.CENTRE_ID,TS.LAB_ID,TS.TEST_ID,L.SEATS,TS.SESSION_ID,TS.TEST_SESSION_ID,(SELECT COUNT(*) FROM STUDENT_TEST ST LEFT JOIN TEST_SESSION TS1 ON ST.TEST_SESSION_ID=TS1.TEST_SESSION_ID WHERE TS1.LAB_ID=TS.LAB_ID AND ST.TEST_ID=TS.TEST_ID AND TS1.SESSION_ID=TS.SESSION_ID GROUP BY TS1.TEST_SESSION_ID) count FROM EVENT_MANAGE EM LEFT JOIN TEST_SESSION TS ON EM.EVENT_MANAGE_ID=TS.EVENT_MANAGE_ID LEFT JOIN LAB L ON L.LAB_ID=TS.LAB_ID WHERE EM.EVENT_ID='$eventId' AND EM.CENTRE_ID='$centre' and TS.SESSION_ID='$sessionId';");
        if(mysql_num_rows($totalData)==0){
            echo '1';
        }
			while($getDetail = mysql_fetch_array($totalData)){
			$labId = $getDetail['LAB_ID'];
			$testId = $getDetail['TEST_ID'];
			$testSessionId = $getDetail['TEST_SESSION_ID'];
			$seats = $getDetail['SEATS'];	
			$session = $getDetail['SESSION_ID'];
			$totalAllot = $getDetail['count'];
			//echo "Lab Id:".$labId." TestId:".$testId." TestSessionId:".$testSessionId." seats:".$seats." session:".session." totalAllot:".$totalAllot."\n";
			$checkStudent = parent::executeQuery("SELECT * FROM STUDENT_TEST WHERE STUDENT_ID='$studentId'");
			if(mysql_num_rows($checkStudent)==1){
				if($totalAllot<$seats){
				$associate=parent::executeQuery("update STUDENT_TEST SET TEST_ID='$testId',TEST_SESSION_ID='$testSessionId',CENTRE_ID='$centre',PUBLISH='1' WHERE STUDENT_ID='$studentId'");
                                $check="0";
                                exit;
                                }else{
                                $check="1";
                                }
		} if(mysql_num_rows($checkStudent)==0){
                    if($totalAllot<$seats){
				$associate=parent::executeQuery("INSERT INTO STUDENT_TEST (STUDENT_ID,TEST_ID,CBT_ID,TEST_SESSION_ID,CENTRE_ID,OBS,PUBLISH,ADD_DATE) VALUES($studentId,$testId,$appId,$testSessionId,$centre,1,1,CURDATE())");
                                
                                $check = "0";
                                exit;
                                }else{
                                $check = "1";

                                }
                }
        }
        if(mysql_num_rows($totalData)!=0){
            echo $check;
        }  
    }
   public function getEmailOfStudents(){
       $sql = "SELECT EMAIL FROM STUDENT where EMAIL!=''";
       $getData = parent::executeQuery($sql);
       return $getData;
   }
   
   public function getStudentCentre($studentId) {
       $sql = "SELECT C.NAME CENTRE_NAME,C.ADDRESS,S.NAME SESSION_NAME,TS.START_TIME,TS.END_TIME FROM STUDENT_TEST ST LEFT JOIN CENTRE C ON ST.CENTRE_ID = C.CENTRE_ID LEFT JOIN TEST_SESSION TS ON ST.TEST_SESSION_ID = TS.TEST_SESSION_ID LEFT JOIN SESSION S ON TS.SESSION_ID = S.SESSION_ID WHERE ST.STUDENT_ID='$studentId' LIMIT 0,1";
       $getData = parent::executeQuery($sql);
       return $getData;
   }
   public function getStudentResult($stuId) {
       $sql = "SELECT RE.RIGHT_QUE, RE.LEFT_QUE, RE.WRONG, RE.NBE_TOTAL_MARKS, RE.NBE_RANK FROM RESULT_EXAMINATION RE WHERE RE.STUDENT_ID='$stuId' LIMIT 0,1";
       $getResult = parent::executeQuery($sql);
       return $getResult;
   }
   function getBatch(){
        $sql="Select * from BATCH";
        $result=parent::executeQuery($sql);
        return $result;
    } 
    public function sendMail($email,$mailTextData, $subjectText){ 
        
        $maindrill='0';
        $aws='0';
        if(constant::$AWS_ON=='1'){
            $aws=$this->sendMail_AWS($email,$mailTextData, $subjectText);
        }
        if($aws=='0' && constant::$MANDRILL_ON==1){
             $mainDrill= $this->sendMail_MANDRILL($email, $mailTextData, $subjectText);   
            }
        if($aws==0 && $maindrill=='0'){
            $this->sendMail_PHP($email, $mailTextData, $subjectText);   ; 
        }
    }
    public function sendMail_AWS($email,$mailTextData,$subjectText) {
        $apikey = constant::$AWS_KEY;
        $secretKey=constant::$AWS_SECRET_KEY;
        $region=constant::$AWS_REGION;
        try {
             $ses = SesClient::factory(array(
                        'key' => "$apikey",
                        'secret' =>"$secretKey",
                        'region' => "$region"
            ));
            } catch (Exception $e) {
            echo($e->getMessage());
        }

        $timezone = "Asia/Calcutta";
        if (function_exists('date_default_timezone_set'))
             date_default_timezone_set($timezone);
        $date = date('d M,Y');
        $txtemail = (string)constant::$Email_From; 
        $companyName = constant::$MailcompanyName;
        $msg = array();
        $msg['Source'] = "$txtemail";
     //   $msg['']="$companyName";
//ToAddresses must be an array
        $msg['Destination']['ToAddresses'][] = "$email";
        $msg['Message']['Subject']['Data'] = "$subjectText";
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] = "$mailTextData";
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";

        try {
            $result = $ses->sendEmail($msg);

            //save the MessageId which can be used to track the request
            $msg_id = $result->get('MessageId');
            return $msg_id; 
        } catch (Exception $e) {
//            //An error happened and the email did not get sent 
            return "0";//($e->getMessage());
        }
    } 
    
    //**********This MANDRILL mail will only execute when AWS mail will not sent to student
    public function sendMail_MANDRILL($email,$mailTextData,$subjectText) {
       $timezone = "Asia/Calcutta";
    if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
    $date = date('d M,Y');
    $apikey = constant::$MANDRILL_KEY;
    $mandrill = new Mandrill($apikey);
    $txtemail = (string) constant::$Email_From;
    $companyName = constant::$MailcompanyName;
    $message = array(
            "html" => "$mailTextData",
            "subject" => "$subjectText",
            "from_email" => "$txtemail",
            "from_name" => "$companyName",
            "to" => array(
                array(
                    "email" => "$email"
                )
             ),
            "important" => true,
            'track_opens' => false,
            'track_clicks' => false,
            'auto_text' => false,
            'auto_html' => false,
            'inline_css' => true,
            'url_strip_qs' => false,
            "async" => false,
            "send_at" => "$date"
        );
        $response = $mandrill->messages->send($message, true);

    }
public function sendMail_PHP($email,$mailTextData,$subjectText) {
       $timezone = "Asia/Calcutta";
    if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
    $companyName = constant::$MailcompanyName;
     $txtemail = (string) constant::$Email_From;
    $date = date('d M,Y');
   
             $headers  = 'MIME-Version: 1.0' . "\r\n";
             $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             $headers.="From:.$txtemail.\r\n";

             $headers1  = 'MIME-Version: 1.0' . "\r\n";
             $headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             $headers1.='From:'.$companyName."\r\n";
             $success=mail("$email","$subjectText",$mailTextData, $headers1,'-f'.$email);
    }
    public function checkDamosian($studentId)
    {
       $query="select DAMOSIAN FROM STUDENT WHERE STUDENT_ID='$studentId' ";
       $result = parent::executeQuery($query);
       $result= mysql_fetch_object($result);
       return $result;
    }
     public function getDataforDamsErpApi($orderId)
    {
       $query="SELECT ENROLLMENT_NO,STUDENT_NAME,EMAIL,MOBILE,ONLINE_COST FROM ONLINE_TRANSACTION OT LEFT JOIN STUDENT S ON S.STUDENT_ID= OT.STUDENT_ID WHERE ORDER_ID='$orderId'  ";
       $result = parent::executeQuery($query);
       $result= mysql_fetch_object($result);
       return $result;
    }
    
    public function getFranchiseId($enrollmentNo)
            {
            $query="SELECT FRANCHISE_CODE FROM STUDENT S LEFT JOIN FRANCHISE F ON S.FRANCHISE_ID=F.FRANCHISE_ID WHERE S.ENROLLMENT_NO='$enrollmentNo' ";
            $result = parent::executeQuery1($query); 
            $result= mysql_fetch_object($result);
            return $result;
            
            }
}
?>
