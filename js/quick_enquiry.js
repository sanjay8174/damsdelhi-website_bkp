/*start here for quick enquiry*/
function validateQuickEnquiry_Demo(){
   var quickEnquiry1=$("#quickEnquiry1").val();
   var quickEnquiry2=$("#quickEnquiry2").val();
   var quickEnquiry3=$("#quickEnquiry3").val();
   var quickEnquiry4=$("#quickEnquiry4").val();
   var quickEnquiry5=$("#quickEnquiry5").val();
   var quickEnquiry6=$("#quickEnquiry6").val();
   if(quickEnquiry1=="" || quickEnquiry1==null || quickEnquiry1=="Your Name"){
        $('#quickEnquiryerror').html("Please enter the name.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry1").focus();
        $("#quickEnquiry1").addClass('error_msg');

       return false;
   }
    if(quickEnquiry2=="" || quickEnquiry2==null || quickEnquiry2=="Email Address"){

       $('#quickEnquiryerror').html("Please enter your Email Address.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry2").focus();
        $("#quickEnquiry2").addClass('error_msg');
       return false;
   }
    if (!ValidateEmail(quickEnquiry2)) {
           
            $('#quickEnquiryerror').html("Please enter valid Email Address.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry2").focus();
        $("#quickEnquiry2").addClass('error_msg');
            return false;
        }
    if(quickEnquiry3=="" || quickEnquiry3==null || quickEnquiry3=="Phone Number"){
      
       $('#quickEnquiryerror').html("Please enter your Phone Number.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry3").focus();
        $("#quickEnquiry3").addClass('error_msg');
       return false;
   }
    if (!ValidatePhone(quickEnquiry3)){
           
            $('#quickEnquiryerror').html("Please enter valid Phone Number.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry3").focus();
        $("#quickEnquiry3").addClass('error_msg');
            return false;
        }
    if(quickEnquiry4=="" || quickEnquiry4==null || quickEnquiry4=="Select Course"){
      
       $('#quickEnquiryerror').html("Please select your Course.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry4").focus();
        $("#quickEnquiry4").addClass('error_msg');
       return false;
   }
    if(quickEnquiry5=="" || quickEnquiry5==null || quickEnquiry5=="Centre Interested"){
       
       $('#quickEnquiryerror').html("Please select your Centre.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry5").focus();
        $("#quickEnquiry5").addClass('error_msg');
       return false;
   }
    if(quickEnquiry6=="" || quickEnquiry6==null){
      
       $('#quickEnquiryerror').html("Please enter your Query.");
        $('#quickEnquiryerror').show();
        
        $("#quickEnquiry6").focus();
        $("#quickEnquiry6").addClass('error_msg');
       return false;
   }
    var mobile=$("#quickEnquiry3").val();
         var url="mobile="+mobile+"&mode=optres";
         $.ajax({
                type: "post",
                url: "ajax.php",
                data: url ,
                async:false,
                error: function(data) {
                },
                success: function(data)
                {
                    alert($.trim(data));
                }
            });
var urldata="quickEnquiry1="+quickEnquiry1+"&quickEnquiry2="+quickEnquiry2+"&quickEnquiry3="+quickEnquiry3+"&quickEnquiry4="+quickEnquiry4+"&quickEnquiry5="+quickEnquiry5+"&quickEnquiry6="+quickEnquiry6+"&mode=quickEnquiry";
 $.ajax({
        type: "post",
        url:   'enquiryAjax.php',
        data: urldata ,
async:false,
        error:
        function() {
        },
        success: function(result){
           //alert("Thank you,Your Information has been sent to Delhi Academy of Medical Sciences.");
           location.href="thanks.php"; 
            }
        });
}
/*end here for quick enquiry*/

/*start added for enquiry form */
function validateEnquiry(){
    var enquiryform1=$("#enquiryform1").val();
    var enquiryform2=$("#enquiryform2").val();
    var enquiryform3=$("#enquiryform3").val();
    var enquiryform4=$("#enquiryform4").val();
    var enquiryform5=$("#enquiryform5").val();
    var enquiryform6=$("#enquiryform6").val();
    if(enquiryform1=="" || enquiryform1==null || enquiryform1=="Your Name"){
        
        $('#enquiryError').html("Please enter the name.");
        $('#enquiryError').show();
       
        $("#enquiryform1").focus();
        $("#enquiryform1").addClass('error_msg');
        return false;
    }
    if(enquiryform2=="" || enquiryform2==null || enquiryform2=="Email Address"){
      $('#enquiryError').html("Please enter your Email Address.");
        $('#enquiryError').show();
       
        $("#enquiryform2").focus();
        $("#enquiryform2").addClass('error_msg');
        return false;
    }
    if (!ValidateEmail(enquiryform2)) {
           $('#enquiryError').html("Please enter valid Email Address.");
        $('#enquiryError').show();
       
        $("#enquiryform2").focus();
        $("#enquiryform2").addClass('error_msg');
            return false;
        }
    if(enquiryform3=="" || enquiryform3==null || enquiryform3=="Phone Number"){
        $('#enquiryError').html("Please enter your Phone Number.");
        $('#enquiryError').show();
       
        $("#enquiryform3").focus();
        $("#enquiryform3").addClass('error_msg');
        return false;
    }
    if (!ValidatePhone(enquiryform3)){
            $('#enquiryError').html("Please enter valid Phone Number.");
        $('#enquiryError').show();
       
        $("#enquiryform3").focus();
        $("#enquiryform3").addClass('error_msg');
            return false;
        }
    if(enquiryform4=="" || enquiryform4==null || enquiryform4=="0"){
        $('#enquiryError').html("Please select your Course.");
        $('#enquiryError').show();
       
        $("#enquiryform4").focus();
        $("#enquiryform4").addClass('error_msg');
        return false;
    }
    if(enquiryform5=="" || enquiryform5==null || enquiryform5=="Centre Interested" || enquiryform5=="0"){
     $('#enquiryError').html("Please select your Centre.");
        $('#enquiryError').show();
       
        $("#enquiryform5").focus();
        $("#enquiryform5").addClass('error_msg');
        return false;
    }
    if(enquiryform6=="" || enquiryform6==null){
       $('#enquiryError').html("Please enter your Query.");
        $('#enquiryError').show();
       
        $("#enquiryform6").focus();
        $("#enquiryform6").addClass('error_msg');
        return false;
    }
var mobile=$("#enquiryform3").val();
         var url="mobile="+mobile+"&mode=optres";
         $.ajax({
                type: "post",
                url: "ajax.php",
                data: url ,
                async:false,
                error: function(data) {
                },
                success: function(data)
                {
                    alert($.trim(data));
                }
            });
    var urldata="enquiryform1="+enquiryform1+"&enquiryform2="+enquiryform2+"&enquiryform3="+enquiryform3+"&enquiryform4="+enquiryform4+"&enquiryform5="+enquiryform5+"&enquiryform6="+enquiryform6+"&mode1=enquiryform";
 $.ajax({
        type: "post",
        url:   'enquiryAjax.php',
        data: urldata ,
	async:false,
        error:
        function() {
           // alert("error");
        },
        success: function(result){
          //alert("Thank you,Your Information has been sent to Delhi Academy of Medical Sciences.");
        //location.reload();
		location.href="thanks.php"; 
        }
        });
}
/*end added for enquiry form */

/*start email validation code here*/
function ValidateEmail(email){
 var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  return expr.test(email);
    };
/*end email validation code here*/

/*start mobile number validation code here*/
function ValidatePhone(number){
        var pattern = /^\d{10}$/;
        return pattern.test(number);
    }
       /*end mobile number validation code */
