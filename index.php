<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<meta name=description content=" DAMS-Delhi Academy of Medical Sciences is one of the best PG medical entrance coaching institute provides coaching for PG Entrance, and MD/MS coaching in New Delhi, India." />
<meta name=keywords content=" PG Medical entrance coaching institute, MD Coaching Institute, MS Coaching Institute, Post Graduate Medical Coaching, Post Graduate Coaching, Medical Coaching, Medical Coaching India, Medical Coaching Delhi, Medical Coaching Mumbai, All India PG Medical Entrance, AIIMS PG Medical Entrance, State PG Medical Entrance" />
<title>PG Medical Entrance Coaching Institute, NEET PG</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href=css/style.css rel=stylesheet type=text/css />
<link href=css/responcive_css.css rel=stylesheet type=text/css />
<link href="css/stylehome1.css?v=1" rel=stylesheet type=text/css />
<link href=css/screenhome.css rel=stylesheet type=text/css />
<style>
div#preloader { position: fixed;
                left: 0; top: 0; z-index: 999;
                width: 100%;
                height: 100%;
                overflow: visible;
                background: transparent url('images/dams.gif') no-repeat center center;
}
</style>
</head>
<body class="inner-bg">
<?php include 'registration.php';?>
<?php // include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'headernew.php'; ?>
<?php echo include 'dao.php'; ?>
<?php 
     $Dao = new dao();
	$homeBanneeSql  = $Dao->getHomeBanner();
//	$projectName = 'damswebadmin' ;  // for local Or li.gingertab.com ,comment this for live
  //       $projectName = 'damswebsite_v1.3/damsCMS' ; for local 
    $projectName = 'damsCMS' ;// for live, comment this for localhost
         //$new = "/damswebsiteNew" ; // comment this for live 
?>
    <section class="sld_section" >
<div class="demo-centering">
    
    <div id="sliderB" class="slider">

    <?php while ( $rowHomeBanner = mysql_fetch_object($homeBanneeSql)): ?>
        <div>
           <a href="<?php echo $rowHomeBanner->BANNER_LINK;?>" target="_blank"><img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/bannerImage/'.$rowHomeBanner->ID.'.'.$rowHomeBanner->BANNER_PATH ; ?>" /></a>  

<!-- <a href="<?php echo $rowHomeBanner->BANNER_LINK;?>" target="_blank"><img src="<?php echo 'https://s3.ap-south-1.amazonaws.com/dams-banner/bannerImage/'.$rowHomeBanner->ID.'.'.$rowHomeBanner->BANNER_PATH ; ?>" /></a>  -->
                <div class="main_wrappr1 slidecont">
                    <div class="bnr_ovr_txt" style="display:none;">
                        <div class="bnr_over_info" style="display:none;">
                            <h2 class="bold_font"><?php echo  html_entity_decode(rawurldecode($rowHomeBanner->BANNER_NAME)); ?></h2>
                            <span class="test_dateicon"><?php echo rawurldecode($rowHomeBanner->BANNER_TEXT); ?></span> 
                        </div>
                    </div>
                </div>
            </div>
   <?php endwhile; ?>         
        

        
     </div>
   </div>
   
</div> 
 </section>
 <section class="cont">
     <div class="shadow"></div>
 <div class="main_wrappr course"> 
      <div class="course_panel">
         <?php 
		 
            $homePageCourse = $Dao->getHomeCourse();
            while ($rowHomeCourse = mysql_fetch_object($homePageCourse)): 
                $getCourseNav = $Dao->getCourseNav($rowHomeCourse->COURSE_ID);
                ?>
         <div class="course_div">
             <div class="course_detail">
                 <div class="course_img"> 
                     <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/courseImages/'.$rowHomeCourse->COURSE_ID.'.'.$rowHomeCourse->IMG_EXT ; ?>"/>

                 </div>
                 <div class="course_des">
                     <h3><?php echo urldecode(html_entity_decode($rowHomeCourse->COURSE_NAME)); ?></h3>    
                     <p class="course_desp"><?php echo rawurldecode($rowHomeCourse->CONTENT_HOME); ?></p>
                 </div>
                 <div class="course_viewmore">
                     <a target="_blank" href="<?php echo urldecode($rowHomeCourse->URL_LINK).'?c='.$rowHomeCourse->COURSE_ID.'&n='.$getCourseNav[0][0]; ?>"> <span>READ MORE</span></a>
                 </div> 
             </div>
         </div>   
         <?php endwhile; ?>
            <div class="course_div ">
             <div class="course_detail">
                 <div class="course_img">
                     <img src="images/neetssmedicalias.jpg"/>
                 </div>
 <div class="course_des">
                     <h3>Medical-IAS</h3>    
                  
                       <p class="course_desp">DAMS has always been a pioneer in taking steps to assist students.MedIas -Medical Science assistance program for civil services again proves DAMS as a leader. This program is for Medical who are preparing for prestigious civil service examination. </p>
                      
                         </div>
                        <div class="course_viewmore">
						<a target="_blank" href="https://med-ias.damsdelhi.com/"><span>READ MORE</span></a>
						</div>
 
                    
        
                 </div>
                 
             </div>   
   <div class="course_div ">
             <div class="course_detail">
                 <div class="course_img">
                     <img src="images/neetss1.jpg"/>
                 </div>
                  <div class="course_des">
                     <h3>NEET SS</h3>    
					<p class="course_desp">DAMS now offers the course for superspeciality aspirants. This is also a part of DAMS vision to be a one stop center for all your needs as a medical student. </a>
                    </div>
					 <div class="course_viewmore">
						<a target="_blank" href="https://damsdelhi.com/neetss/?c=13&n="><span>READ MORE</span></a>
						</div>
                 </div>
                 
             </div>			 
         <div class="course_div ">
             <div class="course_detail uk">
                 <div class="course_img">
                     <img src="images/Courses_5.jpg"/>
                 </div>
                 <div class="course_des ukdetail">
                     <h3>UK COURSES </h3>    
                     <ul>

                         <li><a class="course_name" href="https://mrcp.damsdelhi.com/index.php" target=_blank"><i></i><span>MRCP</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcp.damsdelhi.com/index.php" target=_blank">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://mrcog.damsdelhi.com/index.php" target=_blank"><i></i><span>MRCOG</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcog.damsdelhi.com/index.php" target=_blank">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://plab.damsdelhi.com/index.php" target=_blank"><i></i><span>PLAB</span></a><div class="vmbot1"><a class="more_btn" href="https://plab.damsdelhi.com/index.php" target=_blank">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://mrcgp.damsdelhi.com/index.php" class="last" target=_blank"><i></i><span>MRCGP</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcgp.damsdelhi.com/index.php" target=_blank">READ MORE</a></div></li>

                     </ul>
        
                 </div>
                 
             </div>
         </div>
          <div class="course_div ">
         <div class="course_detail uk">
             <div class="course_img">
                 <img src="images/Courses_7.jpg"/>
             </div>
             <div class="course_des ukdetail">
                 <h3>CLINICAL COURSES </h3>    
                 <ul>
                     <li><a class="course_name" href="https://blsacls.damsdelhi.com/index.php" target=_blank"><i></i><span>BLS/ACLS </span></a><div class="vmbot1"> <a class="more_btn" href="https://blsacls.damsdelhi.com/index.php" target=_blank">READ MORE</a></div></li> 
<!--                    <li><a class="course_name" href="https://dentistry.damsdelhi.com/index.php?c=10&n=" target=_blank"><i></i><span>CLINICAL DENTISTRY</span></a><div class="vmbot1"> <a class="more_btn" href="https://dentistry.damsdelhi.com/index.php?c=9&n=" target=_blank">READ MORE</a></div></li>
-->
<li class="dentistry_course"><a class="course_name middle_course" href="https://dentistry.damsdelhi.com/index.php?c=10&n=" target=_blank"><i></i><span>Basic Implant Course</span></a><div class="vmbot1"> <a class="more_btn" href="https://dentistry.damsdelhi.com/index.php?c=9&n=" target=_blank">READ MORE</a></div></li>
                     <li class="drtp_course"><a class="course_name last_course" href="https://drtp.damsdelhi.com/index.php?c=11&n=" target=_blank"><i></i><span>Research Methodology Training Workshop</span></a><div class="vmbot1"> <a class="more_btn" href="https://drtp.damsdelhi.com/index.php?c=11&n=" target=_blank">READ MORE</a></div></li>
                 </ul>
             </div>

         </div>
         </div>
		 
       
		
			
         </div>
<div style="float: left;">
    <img src="images/feereceipt.jpg" style="width: 100%;">
    <p style="text-align:center;font-size:18px;color:#000;padding:20px 0px;">The ERP generated receipt is the above format is the only valid receipt. <br> Receipt is any other format / Manual receipts are INVALID and will not be Honoured.</p>
</div> 
 </div>
<!--     <div class="shadow1"></div>-->
      </section>
    <section class="cont">

      <div class="shadow"></div>  
     <div class="main_wrappr info">
        <div id="push" class="mnu_icon_lft">
            <span class="horizontal1"></span>
            <span class="horizontal2"></span>
            <span  class="horizontal3" style="margin-bottom:0;"></span>
        </div>
                
 <!-- start: Use Full Link  --> 
      
<div id="slide-menu">
    <div class="leftsection" style="height:auto;">
        <div id="close"> <h3 class="inr_title"><i class="spritehome_new usefulllink"></i>Useful Link</h3></div>
        <div id='cssmenu'>
            <ul>    
                <?php
                    $getUseFullLink = $Dao->getUseFullLink() ;
                    while ($rowUseLink = mysql_fetch_object($getUseFullLink)):
                       
                        $subLinkText = $rowUseLink->SUB_LINK_TEXT;
                        $subLinkTextarr = explode("#", $subLinkText);  
                       
                        $subLink = $rowUseLink->SUB_LINK;
                        $subLinkarr = explode("#", $subLink);  
                        
                        $openInNew = $rowUseLink->OPEN_IN_NEW;
                       if(count($subLinkTextarr) > 0 && $subLinkTextarr[0] !=="" && $subLinkTextarr[0] !=="NULL"){
                            echo "<li class ='active has-sub'><a href='".$rowUseLink->URL_LINK."'><span>".rawurldecode($rowUseLink->LINK_TEXT)."</span></a><ul>";
                            
                            for ( $i=0; $i<count($subLinkTextarr); $i++){
                                if($openInNew == 1){
                                     echo "<li class = 'has-sub'><a href='".$subLinkarr[$i]."' target='_blank'>".rawurldecode($subLinkTextarr[$i])."</a></li>";
                            
                                }else{
                                    echo "<li class = 'has-sub'><a href='".$subLinkarr[$i]."'>".rawurldecode($subLinkTextarr[$i])."</a></li>";
                                }
                            }
                            
                        echo "</ul>";
                       }else{
                           if($openInNew == 1){
                              echo "<li><a href='".$rowUseLink->URL_LINK."' target='_blank' >".rawurldecode($rowUseLink->LINK_TEXT)."</a>"; 
                           }else{
                                echo "<li><a href='".$rowUseLink->URL_LINK."' >".rawurldecode($rowUseLink->LINK_TEXT)."</a>";   
                            }
                        }
 
                      echo "</li>";
                   endwhile;   
                ?>
               <li><a href="javascript:void(0);" class="sow-data">NEXT (National Exit Test )</a></li>
            </ul>
        </div>
   </div></div>

 <!-- End: Use Full Link   --> 
         
     <div class="rgtsection">
        <div class="sec_cont">
            <div class="news_sec ">
                <div class="news_detail">
                    <div> <h3 class="inr_title"><i class="spritehome_new news"></i>News & Events</h3></div>
                    <?php 
                        $getAllNewsAndEvent = $Dao->getAllNewsAndEvent();
                        while($rows = mysql_fetch_object($getAllNewsAndEvent)):
                        $dateArr    = explode(" ",date("jS M", strtotime($rows->DATE)));
                        
                           $wordLengthText = strlen("$rows->NEWS_TEXT");
                           $wordLengthDesc = strlen("$rows->NEWS_DESCRIPTION");
                           
                            if ($wordLengthText < 44){
                                    $retWordText = $rows->NEWS_TEXT;
                                }else{
                                     
                                     $retWordText = substr($rows->NEWS_TEXT,0,43)."..";
                                    }
                                               
                           
                            if ($wordLengthDesc < 45){
                                    $retWordDesc = rawurldecode($rows->NEWS_DESCRIPTION);
                                }else{
                                     
                                     $retWordDesc = substr(rawurldecode($rows->NEWS_DESCRIPTION),0,44)."..";
                                    }
                         
                    ?>
                    <div class="news_cont" >
                  
                        <div class="news_cont1" >
                            <div class="news_date_div" >
                                <div class="news_month" ><span class="month"style=""><?php echo $dateArr[1]; ?></span></div>
                                <div class="news_date"  ><span class="date"style=""><?php echo $dateArr[0]; ?><sup style="font-size:11px;"></sup></span></div>
                            </div>
                            <div class="news_info">
                                
                                <div class="news_cont_detail"><?php echo $retWordText ; ?><br>
                                    <span class="desc_cont"><?php echo $retWordDesc ; ?></span></div>
                                <div class="rade_more1"><span class="read_more" ><a href="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$new.'/home-news-detail.php?&id='.$rows->ID ; ?>" >READ MORE</a></span></div>
                            </div>
                        </div>
                       
                    </div>
                   <?php  $count++; endwhile; ?> 
                <div  class="view_more"><a class="view_more_a" href="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$new.'/home-news.php' ; ?>">View More<span></span></a></div>
                </div>                   
            </div>
            <div class="enquery_form">
                <div class="form_detail">
                 <div> <h3 class="inr_title"><i class="spritehome_new form"></i>Quick Enquiry Form</h3></div>
                 <div class="enquiry_content_main">
<div class="enquiry_content">
<div class="enquiry_content_more">
<div id="preloader" style="display: none;"></div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry1" name="userName" class="career_inp_1" value="First Name" onfocus="if(this.value=='First Name')this.value=''" onblur="if(this.value=='')this.value='First Name'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry7" name="userName" class="career_inp_1" value="Last Name" onfocus="if(this.value=='Last Name')this.value=''" onblur="if(this.value=='')this.value='Last Name'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry2" name="userEmail" class="career_inp_1" value="Email Address" onfocus="if(this.value=='Email Address')this.value=''" onblur="if(this.value=='')this.value='Email Address'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry3" name="userMobile" class="career_inp_1" value="Phone Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onfocus="if(this.value=='Phone Number')this.value=''" onblur="if(this.value=='')this.value='Phone Number'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<select name="userCourse" class="career_select_input_1" id="quickEnquiry4">
<option value="">Select Course</option>
<option>
DRTP</option>
<option>
MD/MS ENTRANCE</option>
<option>
MCI SCREENING</option>
<option>
MDS QUEST</option>
<option>
USMLE EDGE</option>
<option>
MRCP</option>
<option>
MRCOG</option>
<option>
PLAB</option>
<option>
MRCGP</option>
<option>
BLS/ACLS</option>
<option>
DENTISTRY</option>
<option>NEET SS</option>
</select></div></div>
<div class="career_box">
<div class="right_ip_1">
    <select name="userCentre" class="career_select_input_1" id="quickEnquiry5">
        <option value="">Centre Interested</option>
        <option>Ahmedabad</option><option>Amritsar</option>
        <option>Aurangabad</option><option>Bengaluru</option><option>Bhopal</option><option>Bhuvneshwar</option><option>Chandigarh</option>
        <option>Chennai</option><option>Guwahati</option><option>Hyderabad</option><option>Indore</option><option>
            Jaipur</option><option>Jodhpur</option><option>Kanpur </option><option>Kolkata</option><option>Lucknow</option><option>
            Mangalore</option><option>Manipal</option><option>Nagpur</option><option>Patna</option><option>Pune</option>
        <option>Raipur</option><option>Rohtak</option><option>Surat</option><option>Udaipur</option><option>Vadodara</option>
        <option>Vijayawada</option><option>Agartala</option><option>Bathinda</option><option>Belgaum</option><option>Bikaner</option>
        <option>Bilaspur</option><option>Davangere</option><option>Dhule</option><option>Gorakhpur</option><option>Haldwani</option><option>
            Hyderabad</option><option>Kota</option><option>Latur</option><option>Lucknow</option><option>Ludhiana</option><option>
            Mangalore</option><option>Miraj</option><option>Mumbai</option<option>Pune</option><option>Rohtak</option><option>
            Vijayawada</option><option>Vishakapatnam</option><option>Agroha</option><option>Aligarh</option><option>Allahabad</option><option>
            Amravati</option><option>Berhampur</option><option>Bikaner</option><option>Cuttack</option><option>Warangal</option><option>
            Dhule</option><option>Faridkot</option><option>Goa</option><option>Gwalior</option><option>Kakinada</option><option>
            Karad</option><option>Karim Nagar </option><option>Khamam</option><option>Kolhapur</option><option>Kurnool</option>
        <option>Latur</option><option>Loni</option><option>Ludhiana</option><option>Meerut</option><option>Miraj</option><option>
            Patiala</option><option>Solapur</option><option>Talegaon</option><option>Tirupati</option><option>Varanasi</option><option>
            Vishakhapatnam</option><option>Tanda</option><option>Maysore</option><option>Agra</option><option>Bagalkot</option><option>Hubli</option><option>
            Dehradun</option><option>Shimla</option><option>New Delhi (Karol Bagh)</option><option>New Delhi (Gautam Nagar)</option><option>Nasik</option>
        <option>Jammu</option><option>Agroha</option><option>Dibrugrah</option><option>Gulbarga</option><option>Aligarh</option><option>
            Manipal</option><option>Cochin</option><option>Ahemadabad</option><option>Aurangabad</option><option>
            Bareilly</option><option>Chennai</option><option>Cochin</option><option>Dehradun</option><option>Saifai</option><option>
            Jaipur</option><option>Jagdal Pur</option><option>Jabalpur</option><option>Kanpur</option><option>Kohlapur</option>
        <option>Kolkata</option><option>Mysore</option><option>Meerut</option><option>Pondicherry</option><option>Ranchi</option><option>
            Mumbai</option><option>JALANDHAR</option><option>RABINDRANATH FOUNDATION</option><option>Srinagar J&amp;K</option><option>
            Tamilnadu</option><option>Siliguri</option><option>Solapur</option><option>Ahemadnagar </option><option>mphal</option><option>
            Ajmer</option><option>Satara</option><option>Srinagar UK</option><option>DAMS (Delhi Accademy of Medical Sciences Pvt.Ltd.)</option><option>
            Silchar</option><option>DAMS Jorhat</option><option>Nanded</option><option>Rewa</option>
    </select></div></div>
    <div class="right-ip-1">
<textarea id="quickEnquiry6" class="career-inp-1" placeholder="Write your Query here" rows="1" name="userMessage"></textarea>
</div>   
<div class="career_box1">
<div class="right_ip_1">
<div class="career_box"><div class="right_ip_1">
<div class="submit_enquiry"><a href="javascript:void(0)" title="Submit" onclick="validateQuickEnquiry()"> SUBMIT</a></div><div class="error_msg_box" id="quickEnquiryerror" style="display:none">
Please enter correct email address.</div></div></div></div></div>
<div class="enquiry_bottom"></div>
</div>
                </div>
             </div>
                </div>
                 </div>   
            </div>
                
        <div class="sec_cont">   

           
            <div class="news_sec ">
                <div class="news_detail topper">
                    <div> <h3 class="inr_title"><i class="spritehome_new topper"></i>Toppers Talk</h3></div>
                    <div class="video_cont">
                    <div id="owl-demo4" class="owl-carousel">
                         <?php 
                       $topperTalk = $Dao->getAllToperTalk();
                       while( $rowTopper = mysql_fetch_object($topperTalk)):  ?>
                        <div class="item">
                            <div class="left_prt">
                                <a target="_blank" href="<?php echo rawurldecode($rowTopper->URL);?>">    <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/youTubeThumbs/'.$rowTopper->ID.'.jpg' ; ?>"/>

                                </a>
                            </div>
                            <div class="rgt_prt">
                                <div class="stu_name">
                                    <p class="first_para"><?php echo $rowTopper->STUDENT_NAME ;?></p>
                                    <?php if($rowTopper->RANK !="NULL" && $rowTopper->RANK !="") { ?>
                                       <p class="second_para">Rank-<?php echo $rowTopper->RANK ; ?></p>
                                    <?php } ;?>
                                </div>
                               <div class="stu_info"> <?php echo substr(rawurldecode($rowTopper->TEXT),0,120);?></div>
                            </div>
                        </div>
                       <?php endwhile; ?> 
                   </div>

                    </div>
                </div>                   
            </div>
            
            <div class="enquery_form">
                <div class="form_detail gallary">
                    <div> <h3 class="inr_title"><i class="spritehome_new photo"></i>Photo Gallery</h3></div>
                    <div class="inr_cnttr">
                <div class="arwsldcntr">
                    <div id="owl-demo3" class="owl-carousel">
                      <?php  $getAllPhotoGal = $Dao->getAllHomePhotoGall();  
                            while($rowPhotoGall = mysql_fetch_object($getAllPhotoGal)):
                        ?>  
                        <div class="item">
                            <div class="left_prt">
                                <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/photoGall/'.$rowPhotoGall->ID.'.'.$rowPhotoGall->PHOTO_PATH ; ?>"/>
                            </div>
                            <div class="rgt_prt">
                                <div class="stu_name">
<!--                                    <p><?php echo $rowPhotoGall->STUDENT_NAME; ?></p>
                                    <p>Rank-<?php echo $rowPhotoGall->RANK;?></p>-->
                                    <p class="first_para"><?php echo $rowPhotoGall->STUDENT_NAME; ?></p>
                                    <p class="second_para">Rank-<?php echo $rowPhotoGall->RANK;?></p>
                                </div>
                                <div class="stu_info"><?php echo substr(rawurldecode($rowPhotoGall->PHOTO_TEXT),0,100);?></div>

                            </div>
                        </div>
                        <?php endwhile; ?>

                    </div>
                </div>
            </div>
                 
                 
                </div>
             </div>
     </div>
    
      
         </div>    </div>
    <div class="shadow1"></div>
		<!--add by satya-->
		<div class="popupmain-part dams-image-pop" style="display:block;">
<div class="popupposition-pro"></div>
<div class="popupcenter-div-pop">
    <div class="dams-image-pop" style="display:block;">
	<a href="https://cbt.damsdelhi.com/" target="_blank"><img src="images/popupnew.png"></a>
	<span class="cancels">
    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.112 31.112" style="enable-background:new 0 0 31.112 31.112;" xml:space="preserve" height="12px" width="12px" fill="#000">
<polygon points="31.112,1.414 29.698,0 15.556,14.142 1.414,0 0,1.414 14.142,15.556 0,29.698 1.414,31.112 15.556,16.97 
	29.698,31.112 31.112,29.698 16.97,15.556 "></polygon>
<g>
</g>
</svg>
</span>
	</div>
</div>
</div>
		
		<!--End by satyam-->
       </section> 

<?php include 'footernew.php'; ?>
 <div style="display:none ;">
<div class="homeloader">
 <div id="onloadPopup" class="onloadPopup home_page">
    <div class="imgholder">
<a href="https://quiz.damsdelhi.com/index.php" target="_blank" ><img onclick="hide1()" src="http://damstestseries.com/images/damspopup17june.gif"></a>      <a href="javascript:void(0)" id="onloadPopup-close" onclick="hide1()"><img src="images/crossbutton.png"></a>
    </div>
</div>
</div>
<div id="blackPopup1" class="blackPopup"></div>
 
 </div>   
<div class="slidepop" >
  <div href="javascript:void(0)" onclick="hideRight();" class="cont_div_slidepopup " id="left">
  <i class="slide_pop_close"><img src="images/Pop-upcross.png"/></i>
  <div class="iconname"><img src="images/DAMS_Popup.png"/></div>
 </div>
</div>
    
<script type=text/javascript async src=js/html5.js></script>
<script type=text/javascript src=js/jquery-1.10.2.min.js></script>
<script type=text/javascript async src=js/registration.js?v=5></script>
<script type=text/javascript async src=js/add-cart.js></script>
<script type=text/javascript async src=js/script.js></script>
</body>

<script type="text/javascript" src="js/jquery.excoloSlider.js"></script>
<script type="text/javascript" src="js/jquery.excoloSlider.min.js"></script>
<script type="text/javascript">
  function hide1(){ $("#onloadPopup,#blackPopup1").fadeOut(700)();
//$("#onloadPopup,#blackPopup")fadeOut(2000);
//$(".ui").hide(1000);
}
    $(document).ready(function(){
  
    $("#left").addClass('appicon_show');
	$("#left").removeClass("appicon_hide");
	
    });
  function hideRight(){
	$("#left").removeClass("appicon_show");
	$("#left").addClass("appicon_hide");
	}
</script>
<script type="text/javascript">
           $(function () {
            $("#sliderB").excoloSlider();
        });

</script>

<script>
    $(document).ready(function() {
        
      $("#owl-demo3").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
      stopOnHover:true,
       autoPlay:3000
      });
      
      $("#owl-demo4").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
     autoPlay:true,
     stopOnHover:true

      
      });
    });
    </script>
<script type="text/javascript"src="js/owl.carousel.js"></script>
<script>
    $(document).ready(function () {
        $("#push").click(function(){
            if($(this).hasClass( "active" )){
                     $(this).removeClass("active");   
            } else {
                     $(this).addClass("active");   
            }
       });
     //$(".active").removeClass("active");
        $('#push, #close').click(function () {
            var $navigacia = $('#slide-menu'),
            val = $navigacia.css('left') === '250px' ? '0px' : '250px';
            $navigacia.animate({
                       left: val
                       }, 300)

       });	
   });
    var button = document.getElementsByClassName("navButton");

    button.click = function() {
    button.setAttribute("class", "active");
    button.setAttribute("src", "images/arrows/top_o.png");
    }
	
</script> 
<script>
    $(document).ready(function(){
    $("#popup-btn").click(function(){
        $(".popupmain-part").show();
    });
    $(".cancel").click(function(){
    $(".popupmain-part").hide();
    });
		 $(".cancels").click(function(){
    $(".popupmain-part").hide();
    });
    $(".popupCancel").click(function(){
    $(".parent").hide();                
    })
    $(".sow-data").click(function(){
    $(".parent").show();                
    })
});
</script>

</html>
