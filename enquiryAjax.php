<?php

if($_REQUEST['mode']=='quickEnquiry'){
    $userName=$_REQUEST['quickEnquiry1'];
     $userlastName=$_REQUEST['quickEnquiry7'];
    $userEmail=trim($_REQUEST['quickEnquiry2']);
    $userMobile=$_REQUEST['quickEnquiry3'];
    $userCourse=$_REQUEST['quickEnquiry4'];
    $redirectUrl=$_REQUEST['redirectUrl'];
    $userCentre=$_REQUEST['quickEnquiry5'];
    $userMessage=$_REQUEST['quickEnquiry6'];
    $subject="Quick Enquiry From ".  ucwords(strtolower($userName));
    $message="<body style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:18px;'>

<table style='width:700px;'>
<tr>
<td>
  <table width='700px' border='0' cellspacing='0' cellpadding='0'>
    <tr>
      <td style='width:585px; padding: 15px 0 15px 0; font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#3a8ccf'>Quick Enquiry</td>
      <td style='text-align:left; width:115px; padding: 15px 0 15px 0;'><img src='https://www.damsdelhi.com/images/logo.gif' width='115' height='33' alt='' /></td>
    </tr>
  </table>


<table style=' padding:20px; border:1px solid #CCCCCC;'>
<tr>
<td>

<table style='width:660px; padding:0;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:18px;' cellspacing='0' cellpadding='0'>

	<tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Name</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userName</td>
	</tr>


		<tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Email</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userEmail</td>
	</tr>




		<tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Mobile</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userMobile</td>
	</tr>



		<tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Course</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userCourse</td>
	</tr>


		<tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Centre</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userCentre</td>
	</tr>


        <tr>
		<td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Message</td>
		<td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userMessage</td>
	</tr>
	</table>
    </td>
</tr>
</table>
</td>
</tr>
</table>
</body>";
    /* curl used to hit page */
    $userMessage = urlencode($userMessage);
    $userCourse = urlencode($userCourse);
    $userName = urlencode($userName);
    $userCentre = urlencode($userCentre);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://182.71.42.230/ConVox3.0/enquiry_lead.php?phone_number=$userMobile&name=$userName&email_id=$userEmail&course=$userCourse&centre=$userCentre&query=$userMessage");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "Hello=World&Foo=Bar&Baz=Wombat");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    curl_close($ch);
    /* curl used to hit page end */

    /* curl used to send sms to student */
    $msgStudent=urlencode('Dear Doctor,'.
    '%0aThanks for your query. Your query has been assigned to us, we will revert back you soon.');
    $urlStudent = "https://web.insignsms.com/api/sendsms?username=damstrans&password=damstrans&senderid=DAMSPG&message=$msgStudent&numbers=$userMobile&dndrefund=1";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlStudent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $curl_scraped_page = curl_exec($ch);
    curl_close($ch); /* curl used to send sms to student END */

    /* sending mail to dams admin. */
    $message = str_replace("\n.", "\n..", $message);
    $message = wordwrap($message, 70, "\r\n");
    $email =str_replace(array("\r", "\n", "%OA", "%oa", "%OD", "%od", "Content-Type:"), "", $userEmail);
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:'."$userName<".$email.">" . "\r\n";
    //    $headers .= 'Cc:'."Ankit Khandelwal <ankit@gingerwebs.com>,Yuvraj Dhakad<yuvraj@gingerwebs.com" . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
    ini_set('sendmail_from','info@thinkexam.com');
     //echo $success=mail("support@gingerwebs.com",$subject,$message,$headers);
   //  echo $success=mail("info@damsdelhi.com",$subject,$message,$headers);

    /* sending mail to dams admin */

    /* sending mail to STUDENT */
    $userEmail1='info@damsdelhi.com';
    $subject1="Quick Enquiry Respond From DAMS";
    $message1 =urldecode('
    <div width="100%" style="float: right;"><img src="https://www.damsdelhi.com/images/logo.gif" width="115" height="33" alt="" /></div>
    <div width="100%" style="float: left;">
    Dear Doctor,'.'<br/><br/>
    Thank you very much for submitting your Query, we like to inform that you are precious
    student of DAMS. Your query has been assigned to us, we will revert back you soon. Your query will undergo the
    normal review process. The process normally takes 72hrs to complete.Please do expect slight delay if the review period overlaps.<br/><br/>
    Once again, thank you very much for your query to the DAMS.<br/>
    For further information, please contact on <span style="color:#3a8ccf">Ph: 011-40094009</span>
    </div>');
    $message1 = str_replace("\n.", "\n..", $message1);
    $message1 = wordwrap($message1, 70, "\r\n");
    $email =str_replace(array("\r", "\n", "%OA", "%oa", "%OD", "%od", "Content-Type:"), "", $userEmail1);
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:'."<".$email.">" . "\r\n";
    //    $headers .= 'Cc:'."Ankit Khandelwal <ankit@gingerwebs.com>,Yuvraj Dhakad<yuvraj@gingerwebs.com" . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
    ini_set('sendmail_from','info@thinkexam.com');
    //echo $success=mail("info@damsdelhi.com",$subject,$message,$headers);
//    echo $success=mail($userEmail,$subject1,$message1,$headers);
    /* sending mail to STUDENT END */
    
    
//    Added by priyanka sharmna 6/feb/2018(adding data to zoho cms)
    
    $xml_data = array(
                                           "data"=>array(
                                               array(
                                                "First_Name"=>"$userName",
                                                "Last_Name"=> "$userlastName",
                                                "Email"=> "$userEmail",
                                                "Mobile"=> "$userMobile",
                                                "Query"=> "$userMessage",
                                                "Stream"=> "$userCourse",
                                                "Center_Interested"=> "$userCentre"
                                                                                          
                                                )  
                                            ),
                                           "trigger" =>array(
                                            "approval",
                                            "workflow",
                                            "blueprint"
                                           )
                                       );
         $xml_data= json_encode($xml_data);

         
         
         
         
$zoho_url="https://www.zohoapis.com/crm/v2/Leads"; 
$token="1000.8767604c94922af68462cf9abef893f1.cf5485ca2e86ed9c4cd5a05017583ae4"; 
$header = array();
//                           $header[] = 'Content-length: 0';
//                           $header[] = 'Content-type: application/json';
                           $header[] = "Authorization: Zoho-oauthtoken $token";
                           $ch = curl_init();
                           curl_setopt($ch, CURLOPT_HEADER, 0);                           
                           curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                           curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
                           curl_setopt($ch, CURLOPT_URL, $zoho_url);
                           $output = curl_exec($ch);
                           if(curl_error($ch))
                           {
                               echo 'error:' . curl_error($ch);
                           }
                           var_dump($output);
                           curl_close($ch);
    
}
if($_REQUEST['mode1']=='enquiryform'){
    $userName=$_REQUEST['enquiryform1'];
    $userlastName=$_REQUEST['enquiryform7'];
    $userEmail=$_REQUEST['enquiryform2'];
    $userMobile=$_REQUEST['enquiryform3'];
    $userCourse=$_REQUEST['enquiryform4'];
    $userCentre=$_REQUEST['enquiryform5'];
    $userMessage=$_REQUEST['enquiryform6']; //echo $userName.'--'.$userlastName.'--'.$userEmail.'--'.$userMobile.'---'.$userCourse.'---'.$userCentre.'--'.$userMessage;die;
    $redirectUrl=$_REQUEST['redirectUrl'];//echo $userName;die;
    $subject="Quick Enquiry From ".  ucwords(strtolower($userName));
    $message="<body style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:18px;'>

    <table style='width:700px;'>
    <tr>
    <td>
      <table width='700px' border='0' cellspacing='0' cellpadding='0'>
        <tr>
          <td style='width:585px; padding: 15px 0 15px 0; font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#3a8ccf'>Quick Enquiry</td>
          <td style='text-align:left; width:115px; padding: 15px 0 15px 0;'><img src='https://www.damsdelhi.com/images/logo.gif' width='115' height='33' alt='' /></td>
        </tr>
      </table>


    <table style=' padding:20px; border:1px solid #CCCCCC;'>
    <tr>
    <td>

    <table style='width:660px; padding:0;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:18px;' cellspacing='0' cellpadding='0'>

            <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Name</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userName</td>
            </tr>


                    <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Email</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userEmail</td>
            </tr>




                    <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Mobile</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userMobile</td>
            </tr>



                    <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Course</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userCourse</td>
            </tr>


                    <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Centre</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userCentre</td>
            </tr>


            <tr>
                    <td style='padding:5px 0px 5px 15px; background-color:#eff0f0; border-bottom:1px solid #CCCCCC;width:150px; font-weight:bold;'>Message</td>
                    <td style='padding:5px 0px 5px 10px; background-color:#FFFFFF; border-bottom:1px solid #CCCCCC;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333;'>$userMessage</td>
            </tr>
            </table>
        </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </body>";

        $userMessage = urlencode($userMessage);
        $userCourse = urlencode($userCourse);
        $userName = urlencode($userName);
        $userCentre = urlencode($userCentre);
        /* curl used to hit page*/
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://182.71.42.230/ConVox3.0/enquiry_lead.php?phone_number=$userMobile&name=$userName&email_id=$userEmail&course=$userCourse&centre=$userCentre&query=$userMessage");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "Hello=World&Foo=Bar&Baz=Wombat");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        curl_close($ch);
        /* curl used to hit page end */

        /* curl used to send sms to student */
        $msgStudent=urlencode('Dear Doctor,'.
        '%0aThanks for your query. Your query has been assigned to us we will revert back you soon.');
        $urlStudent = "https://web.insignsms.com/api/sendsms?username=damstrans&password=damstrans&senderid=DAMSPG&message=$msgStudent&numbers=$userMobile&dndrefund=1";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlStudent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch); /* curl used to send sms to student END */

        /* sending mail to dams admin */
        $message = str_replace("\n.", "\n..", $message);
        $message = wordwrap($message, 70, "\r\n");
        $email =str_replace(array("\r", "\n", "%OA", "%oa", "%OD", "%od", "Content-Type:"), "", $userEmail);
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From:'.$userName."<".$email.">" . "\r\n";
//		$headers .= 'Cc:'."Ankit Khandelwal <ankit@gingerwebs.com>,Yuvraj Dhakad<yuvraj@gingerwebs.com" . "\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
        ini_set('sendmail_from','info@thinkexam.com');
//		echo $success=mail("shipra.singh@gingerwebs.com",$subject,$message,$headers);
      //  echo $success=mail("info@damsdelhi.com",$subject,$message,$headers);      
        //echo $success=mail("support@gingerwebs.com",$subject,$message,$headers);
        /* sending mail to dams admin end */

    /* sending mail to STUDENT */
    $userEmail1='info@damsdelhi.com';
    $subject1="Quick Enquiry Respond From DAMS";
    $message1 =urldecode('
    <div width="100%" style="float: right;"><img src="https://www.damsdelhi.com/images/logo.gif" width="115" height="33" alt="" /></div>
    <div width="100%" style="float: left;">
    Dear Doctor,'.'<br/><br/>
    Thank you very much for submitting your Query, we like to inform that you are precious
    student of DAMS. Your query has been assigned to us, we will revert back you soon. Your query will undergo the normal review process. The process normally   takes 72hrs to complete.
    Please do expect slight delay if the review period overlaps.<br/><br/>
    Once again, thank you very much for your query to the DAMS.<br/>
    For further information,please contact on <span style="color:#3a8ccf">Ph: 011-40094009</span>
    </div>');
    $message1 = str_replace("\n.", "\n..", $message1);
    $message1 = wordwrap($message1, 70, "\r\n");
    $email =str_replace(array("\r", "\n", "%OA", "%oa", "%OD", "%od", "Content-Type:"), "", $userEmail1);
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:'."<".$email.">" . "\r\n";
    //    $headers .= 'Cc:'."Ankit Khandelwal <ankit@gingerwebs.com>,Yuvraj Dhakad<yuvraj@gingerwebs.com" . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
    ini_set('sendmail_from','info@thinkexam.com');
    //echo $success=mail("info@damsdelhi.com",$subject,$message,$headers);
   // echo $success=mail($userEmail,$subject1,$message1,$headers);
    /* sending mail to STUDENT END */
    
    //added by ABHINAV 3/feb/2020
         $xml_data = array(
                                           "data"=>array(
                                               array(
                                                "First_Name"=>"$userName",
                                                "Last_Name"=> "$userlastName",
                                                "Email"=> "$userEmail",
                                                "Mobile"=> "$userMobile",
                                                "Query"=> "$userMessage",
                                                "Stream"=> "$userCourse",
                                                "Center_Interested"=> "$userCentre"
                                                                                          
                                                )  
                                            ),
                                           "trigger" =>array(
                                            "approval",
                                            "workflow",
                                            "blueprint"
                                           )
                                       );
         $xml_data= json_encode($xml_data);

         
         
         
         
$zoho_url="https://www.zohoapis.com/crm/v2/Leads"; 
$token="1000.8767604c94922af68462cf9abef893f1.cf5485ca2e86ed9c4cd5a05017583ae4"; 
$header = array();
//                           $header[] = 'Content-length: 0';
//                           $header[] = 'Content-type: application/json';
                           $header[] = "Authorization: Zoho-oauthtoken $token";
                           $ch = curl_init();
                           curl_setopt($ch, CURLOPT_HEADER, 0);                           
                           curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                           curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
                           curl_setopt($ch, CURLOPT_URL, $zoho_url);
                           $output = curl_exec($ch);
                           if(curl_error($ch))
                           {
                               echo 'error:' . curl_error($ch);
                           }
                           var_dump($output);
                           curl_close($ch);
  }

/* for BOOK YOUR SEAT */
if($_REQUEST['mode']="OnReg"){
    $studentName = $_REQUEST['OnRegName'];
    $studentEmail = trim($_REQUEST['OnRegemail']);
    $studentMobile = $_REQUEST['OnRegMobile'];

    /* curl used to send sms to student */
    $msgStudent1=urlencode('Dear Doctor,'.
    '%0aThank you very much for REGISTRATION.'.
'%0aYou will get confirmation of your payment from our side on your email id and on your contact number within 72 hrs.');
    $urlOnRegStudent1 = "https://web.insignsms.com/api/sendsms?username=damstrans&password=damstrans&senderid=DAMSPG&message=$msgStudent1&numbers=$studentMobile&dndrefund=1";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlOnRegStudent1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $curl_scraped_page = curl_exec($ch);
    curl_close($ch); /* curl used to send sms to student END */
        
    /* sending mail to STUDENT */
    $userEmail1='info@damsdelhi.com';
    $Onregsubject="Online Registration Confirmation In DAMS";
    $OnregMsg =urldecode('
    <div width="100%" style="float: right;"><img src="https://www.damsdelhi.com/images/logo.gif" width="115" height="33" alt="" /></div>
    <div width="100%" style="float: left;">
    Dear Doctor,'.'<br/><br/>
    Thank you very much for REGISTRATION, we like to inform that you are precious student of DAMS.<br/>
    You will get confirmation of your payment from our side on your email id and on your contact number. The process normally takes 72hrs to complete. Please do expect slight delay if the review period overlaps.<br/><br/>
    Once again, thank you very much for your registration in DAMS.<br/>
    For further information, please contact on <span style="color:#3a8ccf">Ph:011-40094009</span>
    </div>');
    $OnregMsg = str_replace("\n.", "\n..", $OnregMsg);
    $OnregMsg = wordwrap($OnregMsg, 70, "\r\n");
    $email =str_replace(array("\r", "\n", "%OA", "%oa", "%OD", "%od", "Content-Type:"), "", $userEmail1);
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:'."<".$email.">" . "\r\n";
    //    $headers .= 'Cc:'."Ankit Khandelwal <ankit@gingerwebs.com>,Yuvraj Dhakad<yuvraj@gingerwebs.com" . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
    ini_set('sendmail_from','info@thinkexam.com');
    //echo $success=mail("info@damsdelhi.com",$subject,$message,$headers);
    echo $success=mail($studentEmail,$Onregsubject,$OnregMsg,$headers);
    /* sending mail to STUDENT END */
    

}

?>
