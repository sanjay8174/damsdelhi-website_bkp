<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>iDAMS, PG Medical Entrance Coaching institute, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	

//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	
 
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-store-idams">
      <aside class="banner-left banner-left-postion">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li class="bg_none"><a href="usmle-edge.php" title="USMLE Edge">USMLE Edge</a></li>
          <!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="DAMS Store">DAMS Store</a></li>-->
         <li><a href="https://www.damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>
          <li><a title="iDAMS Store" class="active-link">iDAMS Store</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>iDAMS</h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="idams-box"> <span>Another Pioneering Effort By the Number 1 Institution in the country</span>
                  <p>First Time in India, after having many Firsts to our credit including AIPG(NBE/NEET) Pattern Mocks, Test &amp; Discussion Series, Bounce Back Series, regular classes, GT series, Foundation and Prefoundation Courses, First Step - DAMS the Pioneers in the PG Medical Entrance Coaching, have added another ACE in their courses-The iDAMS. This product was launched on 25th April in Delhi Head office with more than 1000 eager PG aspirants in the audience by our lead Educationist &amp; Visionary–Dr Sumer Sethi, Radiologist and Director, DAMS. iDAMS Program is a tablet based complete distance learning product that combines the best of classroom learning and self-study. iDAMS program gives access to real time video lectures along with test papers to test your skills. Combining this Tablet with DAMS facebook club and other postal options provides access to famous and learned educationists at DAMS while remaining at your home, which was not possible so far with any of the technologies.</p>
                </div>
                <div class="idams-box"> <span>THE NEW GOLD STANDARD DISTANT LEARNING PROGRAMME</span>
                  <p>iDAMS (tablet course) + Membership to DAMS EXCLUSIVE CLUB on facebook + Unique AIPG(NBE/NEET) Pattern based Online GT series(optional) + Updated DAMS Study Material(optional) “If you cannot come to DAMS-DAMS will come to you”</p>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Benefits of iDAMS:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>The content of iDAMS, study material and video lectures, has been designed and developed by a handpicked team of academic experts. The product provides, ready access to special MCQ based sessions by experts. A student can avail the advantage of learning from the best team that is known for producing amazing results year after year through a series of interactive tools and a personalized feedback system.</li>
                    <li><span>&nbsp;</span>It is a platform which enables students to learn anywhere, anytime.</li>
                    <li><span>&nbsp;</span>There is no need to travel to the local teacher for doubts. Now concepts can be reviewed sitting at home with the help of DAMS EXCLUSIVE CLUB on facebook.</li>
                  </ul>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Technical Features:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>Android as operating system</li>
                    <li><span>&nbsp;</span>Easy to internet surfing</li>
                    <li><span>&nbsp;</span>Good speed volume</li>
                    <li><span>&nbsp;</span>Wi-Fi connectivity</li>
                    <li><span>&nbsp;</span>Bluetooth connectivity</li>
                    <li><span>&nbsp;</span>32 GB Memory card inserted</li>
                  </ul>
                </div>
              </div>
              <div class="idams-apply">
                <div class="how-to-apply-heading"><span></span> Special Gifts:-</div>
                <div class="how-to-content">
                  <ul class="benefits">
                    <li><span>&nbsp;</span>USB device</li>
                    <li><span>&nbsp;</span>Headphones</li>
                    <li><span>&nbsp;</span>Charger</li>
                    <li><span>&nbsp;</span>Leather Cover pack</li>
                    <li><span>&nbsp;</span>Tablet Stand</li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>USMLE Edge DAMS Store</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li style="border:none;"><a href="usmle-idams.php" title="iDAMS Tablet" class="active-store">iDAMS Tablet</a></li>
                  <!--<li><a href="https://damsdelhi.com/dams-publication.php?c=4" title="Dams Publications">Dams Publications</a></li>-->
                  <li><a href="https://www.damspublications.com/" target="_blank" title="Dams Publications">Dams Publications</a></li>
                  <li><a href="usmle-online-test-series.php" title="Online Test Series">Online Test Series</a></li>
                  <li><a href="usmle-dams-test-series.php" title="DAMS Test Series">DAMS Test Series</a></li>
                  <li><a href="../usmle-i-dams.php" title="Mobile Applications">Mobile Applications</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="idams-right-video">
            <iframe width="100%" height="236" src="//www.youtube.com/embed/qcBCIHAvwzA" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="addertisment"> <span>&nbsp;</span> </div>
          <?php include 'usmle-buynow-right-section.php'; ?>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>