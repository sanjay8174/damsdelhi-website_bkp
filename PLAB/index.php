<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PLAB Coaching Institute, PLAB</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<style>
div#preloader1 { position: fixed; 
                left: 0; top: 0; z-index: 999; 
                width: 100%; 
                height: 100%; 
                overflow: visible; 
                background: transparent url('../images/dams.gif') no-repeat center center; 
}
</style>
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include '../header.php'; ?>
<!-- Banner Start Here -->

<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner" style="width: 100%;float: left;">
        <div class="big-nav" style="width: 100%;">
<!--<ul>
    <li class="t-series active" style="float: right;"><a class="t-series-active" href="https://plabtest.damsdelhi.com/" title="Test Series" target="_blank">Test Series</a></li>
</ul>-->
</div>
      <aside class="banner-left">
          <h2>PLAB PART 1 PREPARATORY COURSE</h2>
          <!--<h3>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS -UK</h3>-->
          
<!--          <h3>Date: 24th August</h3>-->
      </aside>
      <aside class="banner-right">
  <div class="banner-right-btns">
      <a href="#" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>
      <a href="#" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a>
      <a href="#" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
</aside>
    </article>
  </div>
</section>
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box mrcog_crsbox ">
       <ul style="display:none" class="idTabs responce-show">
        <li><a href="#part1" class="selected">PART 1</a></li>
        <li><a href="mrcog-part2.php">PART 2</a></li>
       </ul>
      <div id="part1" style="display: block; ">
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;width: 100%;box-sizing: border-box;">
              <h3><span class="book-ur-seat-btn" style="display:none"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
        <div class="outer-plab">
          <div class="ftr">
         <h1 class="main_title" style="color:#3ac353">Features</h1>
     <ul style="list-style-type:none;">
  <li><div class="ftrs_prdct">
   <div class="report">
   <h2 class="semibold_font">Detailed Reports</h2>
   <p>Our online exam provides a detailed analysis about the students score card covering each section individually.</p>
      </div></div></li>
  <li><div class="ftrs_prdct">
  <div class="sup">
   <h2 class="semibold_font">24 X 7 Support</h2>
   <p>Our help desk is at your disposal to answer your queries and issues relating to the portal. Just click on help button and write in to us.</p>
      </div></div></li>
  <li><div class="ftrs_prdct">
 <div class="analysis">
      <h2 class="semibold_font">Comparative Analysis</h2>
   <p>C.P gives you an opportunity to boost your performance taking exams at home itself. It compares your marks, performance and time.</p>
      </div></div></li>
  <li><div class="ftrs_prdct">
 <div class="study">
   <h2 class="semibold_font">Study Anywhere</h2>
   <p>Respecting the time frames and study schedule of different students C.P exam series enables you to take exams anytime, anywhere,24 X 7 .</p>
      </div></div></li>
  <li><div class="ftrs_prdct last">
    <div class="learn">
   <h2 class="semibold_font">Learn &amp; Improve</h2>
   <p>Taking these exams regularly would help you at different levels of your preparation,
   <!-- Increases your practice at all levels <br>
    Gives you a chance to make mistakes and correct them too so as to not repeat them during exams.--></p>
      </div></div></li></ul>
     </div>
        </div>
        
          </div>
        
          </div>
        
        <div id="part1" style="display: block; margin-top: 20px;">
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;"> 
 <h3 style="font-size: 24px;color: #00A651;">FEATURE OF PLAB PART-1 PREPARATORY COURSE</h3>     
              <p>&nbsp;</p>
 <div class="fstr_smtr plab">
   <ul class="plab_ques">
    <li>Do you want to live and work in the UK?</li>
    <li>Do you want to gain higher professional qualification in the UK?</li>
    <li>Do you want to enhance your professional medical career?</li>
   </ul>
    <p>Then look no further and enrol to our PLAB Part 1 Course – A complete package from Delhi Academy of Medical Sciences and Arlington British Medical Academy partnership.</p>
    <p>This course is “First of its kind in India” and guarantees a success for aspiring doctors wishing to take PLAB test.</p>
    <p>This is a golden opportunity, not to be missed. The time is perfect as plenty of opportunities are unravelling due to current doctor’s recruitment crisis in the UK</p>
   
   <h3 style="color: #3AC353;">DAMS - ABMA PLAB Course Unique features:</h3>
   <ul class="plab_feature">
      <li> THOUSANDS OF ONLINE QUESTIONS &amp; ANSWERS - NEW FORMAT (SBA)</li>
      <li>ONLINE VIDEO LECTURES FOR PLAB PART 1</li>
      <li>MOCK TESTS</li>
   </ul>
   </div>
</div>
      

        
       <div class="course-box mrcog_crsbox " style="margin-top: 20px;display: block">
      <img src="images/PLAB_2020.jpg"  style="width:100%;">
        
       </div>
      </div></aside></div>
      
      
        
    <aside class="content-right">
    
       <!--  <div class="content-royal-college res_css">
      <img src="images/logo-royal-college.png" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF PHYSICIANS OF THE UNITED KINGDOM</p>
        </div> -->
    <div class="content-date-venue res_css">
      <h1> PLAB WORKSHOP Date</h1>
            <p>14th & 15th January 2020</p>
         <h1> Fee For PLAB 2020.</h1> 
         <p>FEE : Rs 15000 + GST = Rs 17,700/- </p>
        </div>
        <div class="content-date-venue res_css">
            <!--<h1>DATES</h1>
            <p style="font-weight:bold;font-size:18px;">Monday 04th & Tuesday 05th December, 2017</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>-->
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 011-40094009</p>
      <p>(Enquiry Timing:- 10am To 8pm)</p>
        </div>
        
        <div id="part1">
      
        <div class="content-date-venue res_css" style="display: none;">
            <h1 style="color:black">What is unique about this course</h1>
            <p style="color:black">The faculty have attended some of the most popular UK MRCOG Part 1 preparation courses and picked the best aspects from each to deliver this course. Instead of the traditional week of tiring endless lectures full of slides, we hope to prepare candidates with a whistle stop tour of MRCOG Part 1. We will cover 18 core modules of the MRCOG Part 1. We will provide concise invigorating and motivational preparatory content to guide candidates on a path to MRCOG Part 1 success.</p>
        </div>
      
        </div>
        
        
       
          

<div class="enquiry-main">


  <div class="enquiry-heading">Enquiry Form</div>
  <div class="enquiry-content-main">
    <div class="enquiry-content">
      <div class="enquiry-content-more">
           <div id="preloader1" style="display: none;"></div>
        <form onsubmit="return validateEnquiry();" action="#" method="post" name="enquiryform" id="enquiryform">
          <input value="enquiryform" id="mode1" name="mode1" type="hidden">
          <input value="https://localhost/damswebsite/mrcog/index.php?c=6&amp;n=" name="redirectUrl" type="hidden">
          <div class="career-box">
            <div class="right-ip-1">
              <input onblur="if(this.value=='')this.value='First Name';" onfocus="if(this.value=='First Name')this.value='';" value="First Name" id="enquiryform1" class="enquiry-input" name="userName" type="text">
              </div>
          </div>
          <div class="career-box">
<div class="right-ip-1">
<input name="userLastName" type="text" class="enquiry-input" id="enquiryform7" value="Last Name" onfocus="if(this.value=='Last Name')this.value='';" onblur="if(this.value=='')this.value='Last Name';" />
</div>
</div>
          <div class="career-box">
            <div class="right-ip-1">
              <input onblur="if(this.value=='')this.value='Email Address';" onfocus="if(this.value=='Email Address')this.value='';" value="Email Address" id="enquiryform2" class="enquiry-input" name="userEmail" type="text">
               </div>
          </div>
          <div class="career-box">
            <div class="right-ip-1">
              <input onblur="if(this.value=='')this.value='Phone Number';" onfocus="if(this.value=='Phone Number')this.value='';" value="Phone Number" id="enquiryform3" class="enquiry-input" name="userMobile" type="text">
               </div>
          </div>
          <div class="career-box">
            <div class="right-ip-1">
              <select id="enquiryform4" class="select-input" name="userCourse">
                <option value="0">Select Course</option>
                                <option> DRTP </option>
                                <option> MD/MS ENTRANCE </option>
                                <option> MCI SCREENING </option>
                                <option> MDS QUEST </option>
                                <option> USMLE EDGE </option>
                                <option> MRCP </option>
                                <option> MRCOG </option>
                                <option> PLAB  </option>
                                <option> MRCGP </option>
                                <option> BLS/ACLS </option>
                                <option> DENTISTRY </option>
                                <option> NEET SS </option>
                                <option> MEDICAL-IAS</option>
                              </select>
               </div>
          </div>
          <div class="career-box">
            <div class="right-ip-1">
              <select id="enquiryform5" class="select-input" name="userCentre">
                <option value="0">Centre Interested</option>
                                <option> Opp Patang Hotel, </option>
                                <option> Amritsar </option>
                                <option> Aurangabad </option>
                                <option> Bengaluru </option>
                                <option> Bhopal </option>
                                <option> Bhuvneshwar </option>
                                <option> Chandigarh </option>
                                <option> Chennai </option>
                                <option> Guwahati </option>
                                <option> Hyderabad </option>
                                <option> Indore </option>
                                <option> Jaipur </option>
                                <option> Jodhpur </option>
                                <option> Kanpur  </option>
                                <option> Kolkata </option>
                                <option> Lucknow </option>
                                <option> Mangalore </option>
                                <option> Manipal </option>
                                <option> Nagpur </option>
                                <option> Patna </option>
                                <option> Pune </option>
                                <option> Raipur </option>
                                <option> Rohtak </option>
                                <option> Surat </option>
                                <option> qwer </option>
                                <option> Vadodara </option>
                                <option> Vijayawada </option>
                                <option> Agartala </option>
                                <option> Bathinda </option>
                                <option> Belgaum </option>
                                <option> Bikaner </option>
                                <option> Bilaspur </option>
                                <option> Davangere </option>
                                <option> Dhule </option>
                                <option> Gorakhpur </option>
                                <option> Haldwani </option>
                                <option> Hyderabad </option>
                                <option> Kota </option>
                                <option> Latur </option>
                                <option> Lucknow </option>
                                <option> Ludhiana </option>
                                <option> Mangalore </option>
                                <option> Miraj </option>
                                <option> Mumbai </option>
                                <option> Pune </option>
                                <option> Rohtak </option>
                                <option> Vijayawada </option>
                                <option> Vishakapatnam </option>
                                <option> Agroha </option>
                                <option> Aligarh </option>
                                <option> Allahabad </option>
                                <option> Amravati </option>
                                <option> Berhampur </option>
                                <option> Bikaner </option>
                                <option> Cuttack </option>
                                <option> Warangal </option>
                                <option> Dhule </option>
                                <option> Faridkot </option>
                                <option> Goa </option>
                                <option> Gwalior </option>
                                <option> Jammu </option>
                                <option> Kakinada </option>
                                <option> Karad </option>
                                <option> Karim Nagar  </option>
                                <option> Khamam </option>
                                <option> Kolhapur </option>
                                <option> Kurnool </option>
                                <option> Latur </option>
                                <option> Loni </option>
                                <option> Ludhiana </option>
                                <option> Meerut </option>
                                <option> Miraj </option>
                                <option> Patiala </option>
                                <option> Rewa </option>
                                <option> Solapur </option>
                                <option> Srinagar </option>
                                <option> Talegaon </option>
                                <option> Tirupaty </option>
                                <option> Varanasi </option>
                                <option> Vishakhapatnam </option>
                                <option> Tanda </option>
                                <option> Maysore </option>
                                <option> Agra </option>
                                <option> Ambala </option>
                                <option> Bagalkot </option>
                                <option> Hubli </option>
                                <option> Dehradun </option>
                                <option> Shimla </option>
                                <option> New Delhi (Karol Bagh) </option>
                                <option> New Delhi (Gautam Nagar) </option>
                                <option> Nasik </option>
                                <option> Jammu </option>
                                <option> Agroha </option>
                                <option> Dibrugrah </option>
                                <option> Gulbarga </option>
                                <option> Aligarh </option>
                                <option> Aligarh </option>
                                <option> Manipal </option>
                                <option> Cochin </option>
                                <option> Ahemadabad </option>
                                <option> Aurangabad </option>
                                <option> Bareilly </option>
                                <option> Chennai </option>
                                <option> Cochin </option>
                                <option> Dehradun </option>
                                <option> Etawah </option>
                                <option> Jaipur </option>
                                <option> Jagdal Pur </option>
                                <option> Jabalpur </option>
                                <option> Karmsad </option>
                                <option> Kanpur </option>
                                <option> Kohlapur </option>
                                <option> Kolkata </option>
                                <option> Mysore </option>
                                <option> Meerut </option>
                                <option> Pondicherry </option>
                                <option> Srinagar </option>
                                <option> Thrissur </option>
                                <option> Ranchi </option>
                                <option> Mumbai </option>
                                <option> JALANDHAR </option>
                                <option> RABINDRANATH FOUNDATION </option>
                                <option> Tamilnadu  </option>
                                <option> asd </option>
                                <option> bbbbbbbbbbbbbbb </option>
                                <option> rrrrrrrrrrrrrrqqqqqq </option>
                                <option> Surajmal </option>
                              </select>
               </div>
          </div>
          <div class="career-box">
            <div class="right-ip-1">
              <textarea placeholder="Write your Query here" id="enquiryform6" class="enquiry-message" rows="3" name="userMessage"></textarea>
               </div>
          </div>
          <div class="submit-enquiry"><a onclick="validateEnquiry();" title="Submit" href="javascript:void(0);"><span></span> Submit</a></div>
          <div style="display:none" id="enquiryError" class="error_msg_box"> Please enter correct email address. </div>
        </form>
      </div>
    </div>
    <div class="enquiry-bottom"></div>
  </div>
</div>
    <div style="width:78%;float:left;margin-top:30px;">
      <div class="outer-info">
        <div class="news-update-box">
<div class="n-videos"><span></span>Video Gallery</div>
<div class="videos-content-box" style="border: 1px solid #00a651;">

    <div id="vd0" class="">
                     <a target="_blank" href="https://youtu.be/kuhpYAM5Mv8"><img class="border_none" src="images/plab.png" alt="zvRSvxwOfwc.jpg" frameborder="0" width="100%" height="247" style="width:100%;"></a>
            </div>

</div>
</div>
      </div>
    </div>
    </aside>
<!--        <div class="parent"  style="display:block">
      <div ></div>
      <div class="child" style="height:650px">
        <div class="banr-img"><img src="images/Plab_11_june.jpg"></div>
		<p class="popupCancel"><span class="close thick"></span></p>
	</div>
</div>-->
        </div>
</section>

<?php include 'footer.php'; ?>

<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<!--<footer>
<div class="qustion-chat"> <img src="images/question-chat.png" title="Question" alt="Question"> </div>
<div class="wrapper">
<div class="footer-left">
<div class="footer-left-top"> <a href="https://damsdelhi.com/privacy_policy.php" title="Privacy Policy ">Privacy Policy</a> <span class="v-line">|</span> <a href="https://damsdelhi.com/disclaimer.php" title="Disclaimer">Disclaimer</a> <span class="v-line">|</span> <a href="https://damsdelhi.com/terms_n_condition.php" title="Terms &amp; Conditions">Terms &amp; Conditions</a> <span class="v-line">|</span> <a href="https://damsdelhi.com/contact.php" title="Contact">Contact</a> <span class="v-line">|</span> <a href="https://damsdelhi.com/sitemap.php" title="Sitemap">Sitemap</a> </div>
<div class="footer-left-bottom">&copy; Delhi Academy of Medical Sciences Pvt. Ltd. All rights reserved.<br>
<span>Website Design by <a href="https://www.gingerwebs.com">Ginger webs</a> | <a href="https://www.thinkexam.com">Online Exam Software</a> Powered by Think Exam</span></div>
</div>
<div class="footer-right">
<div class="google-plus"><a href="https://play.google.com/store/apps/details?id=com.gingerwebs.damsdelhi&amp;hl=en" title="Download DAMS Android App">Download DAMS Android App</a></div>
<div class="apple-store"><a href="https://itunes.apple.com/us/app/dams/id896538499?mt=8" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>
</div>
</div>
</footer>-->
<script src="//www.google-analytics.com/analytics.js" async></script><script>(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create","UA-37551965-1","damsdelhi.com");ga("send","pageview");</script>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js?v=1"></script>
<script type="text/javascript" src="js/add-cart.js"></script>

<script>

    $('.popupCancel').click(function(){
        $('.parent').css('display','none');
    });

</script>

</body>
</html>
