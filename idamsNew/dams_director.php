<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<?php $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
      $getAboutus = $Dao->getAboutus();
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('images/background_images/<?php echo $getAboutusContent[0][0];  ?>') right top no-repeat;width:100%;float:left;height:318px;">
<aside class="banner-left">
<?php  echo $getAboutusContent[0][1]; ?>
</aside></article></div></section> 
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
<li><a title="Director's Message" class="active-link">Director's Message</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<style>
#directormsgcontent ol li {
list-style-type: decimal;
margin-left: 30px;
padding: 4px;
}
</style>
<div id="directormsg" class="inner-left-heading responc-left-heading paddin-zero">
<article class="showme-main">
<?php echo $getAboutusContent[0][2]; ?>
</article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>