<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php error_reporting(0); ?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php';
?>
<section class="inner-banner">
<div class="wrapper">
<article>
<aside class="banner-left">
<h2>Be smart &amp;<br>take your future in Your Hand </h2>
<h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>who otherwise couldn't take the advantage of the DAMS teaching<br>because of various reasons like non-availability of DAMS centre<br>in the vicinity would be greatly benefited.</h3>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> 
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li class="bg_none"><a title="Download" class="active-link">Download</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading resp-down paddin-zero">
<h4>Download Documents</h4>
<article class="showme-main paddin-zero">
<div class="download-documents">
<ul>
<li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/damsbrouchre.pdf" title="Download"> 
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> DAMS Brochure</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/REGISTRATION%20FORM%20for%20website.pdf" title="Download"> 
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> Registration Form</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/RequisitionforstudyMaterial1.pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> Requisition For Study Material</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/dpg_paper-I_(2010).pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> DPG PAPER-I (2010)</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/dpg_paper-II_(2010).pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> DPG PAPER-II (2010)</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/mppg_paper_(2010).pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> MPPG PAPER (2010)</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/AIPG%20-%202011.pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> AIPG-2011</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/MAHARASTRA%20PAPER%20-%202011.pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> Maharastra Paper (2011)</div>
</div></li><li>
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/JIPMER.pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> JIPMER-2011 PG PAPER</div>
</div></li>
<li class="boder-none">
<div class="download-list">
<div class="right-download">
<a href="downloadpdf/UPSC%202012%20Paper&nbsp;1.pdf" title="Download">
<span class="download-icon"></span> <b>Download</b>
</a></div>
<div class="left-download"><span class="dam-arrow"></span> UPSC 2012 paper 1</div>
</div></li></ul></div></article></div></aside>
<aside class="gallery-right">          
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>