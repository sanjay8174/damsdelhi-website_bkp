<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>
<?php
error_reporting(0);
$courseId=$_REQUEST['c'];
$courseNav_id = 3;
?>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
    $path = constant::$path;
?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
<!--    <article class="online-test-series">-->
    <?php $getNavContent=$Dao->getCourseNavContent($courseNav_id); ?>
      <article style="width:100%;float:left;height:318px;background:url('<?php echo $path."/images/background_images/".$getNavContent[0][2]; ?>') right top no-repeat;">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
          <?php echo $getNavContent[0][1]; ?>
<!--        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>-->
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="index.php?c=<?php echo $courseId ?>" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Test Series" class="active-link">Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Online Test Series 2016<span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <div class="showme-main">
                <style>
                    #showTestSeriesCKData ul li {
                        list-style: disc;
                        margin-left: 30px;
                        padding: 4px;
                    }
                </style>
              <div id="showTestSeriesCKData" class="test-series-content paddin-zero">
                  <?php echo $getNavContent[0][0]; ?>
<!--                <ul class="duration-content">
                  <li>
                    <label>Duration :</label>
                    <span>1 Year</span></li>
                  <li>
                    <label>Total no of tests :</label>
                    <span>Approximately 40 Tests</span></li>
                  <li>
                    <label>Total no. of questions per Test :</label>
                    <span class="grand_inline">300 in GRAND TESTS &amp; 200 IN SUBJECTWISE TESTS, WITH SPECIAL MOCK TEST FOR AIIMS, AIPG (NBE), PGI , NIMHANS, STATE PGs</span></li>
                </ul>
                <ul class="some-points">
                  <li><span class="blue_arrow"></span> <span>Authentic explanations from our highly prestigious faculty with references.</span></li>
                  <li><span class="blue_arrow"></span> <span>Questions are based on AIPG (NBE PATTERN) AIPG(NBE/NEET) PatternPG, AIIMS, PGI  DNB, UPSC exam pattern.</span></li>
                  <li><span class="blue_arrow"></span> <span>We have various Test Centres for more competitive environment &amp; comfortable approach.</span></li>
                  <li><span class="blue_arrow"></span> <span>Fully explained solved answers key of all Questions will be give immediately after completion</span></li>
                  <li><span class="blue_arrow"></span> <span>All Students enrolled in between the session will get all previous Test papers.</span></li>
                  <li><span class="blue_arrow"></span> <span>All India Ranking with detailed marks will be provided at our website: <a href="https://www.damsdelhi.com" title="PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG">www.damsdelhi.com</a></span></li>
                </ul>
                <p>Things have changed in the recent years with new pattern in AIPG with NBE making the exam with complex marking schem of scaling &amp; equating. DAMS is the only institute offering courses on the latest NBE pattern. We are only institute offering similar marking scheme to NBE/NEET. Only DAMS uses Scaling method fopr result calculuation.  We are the number 1 coaching institute for the PG medical entrance examinations AIPG, AIIMS, PGI, UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG (NBE/NEET PATTERN), AIIMS PG entrance, and PGI Chandigarh by our sincere effort.<br>
                  <br>
                </p>-->
              </div>
              <ul class="idTabs mdms_test">
                  <li><a href="#test5" style="font-size:12px">SPL FOR AIIMS MAY EXAM 2016</a></li>
                <li><a href="#test1" style="font-size:12px">SWT Test Series - I</a></li>
                <li><a href="#test2" style="font-size:12px">SWT Test Series - II</a></li>
                <li><a href="#test3" style="font-size:12px">SWT Test Series - III</a></li>
                <li><a href="#test4" style="font-size:12px">SWT Test Series - IV</a></li>
              </ul>
<div id="test5">
<div class="test-tab-content">
    <a href="<?php echo $path; ?>/downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>27<sup>th </sup>Dec, 2015</label> <span>Anatomy</span></li>
<li><label>3 <sup>rd  </sup>Jan, 2016</label> <span>Physio</span></li>
<li><label>10<sup>th </sup>Jan, 2016</label> <span>Biochem</span></li>
<li><label>17<sup>th </sup>Jan, 2016</label> <span>FMT+Anesthesia +GT</span></li>
<li><label>24<sup>th </sup>Jan, 2016</label> <span>Pathology</span></li>
<li><label>31<sup>st </sup>Jan, 2016</label> <span>Pharmacology</span></li>
<li><label>7<sup>th </sup>Feb, 2016</label> <span>Micro</span></li>
<li><label>14<sup>th </sup>Feb, 2016</label> <span>PSM</span></li>
<li><label>21<sup>st </sup>Feb, 2016</label> <span>Ent+Opthal+GT</span></li>
<li><label>28<sup>th </sup>Feb, 2016</label> <span>Dermat+Psychy</span></li>
<li><label>6<sup>th </sup>Mar, 2016</label> <span>Radio+Ortho</span></li>
<li><label>13<sup>th </sup>Mar, 2016</label> <span>Surgery-I</span></li>
<li><label>20<sup>th </sup>Mar, 2016</label> <span>Surgery-II+GT</span></li>
<li><label>27<sup>th </sup>Mar, 2016</label> <span>Medicine-I</span></li>
<li><label>3<sup>rd </sup>Apr, 2016</label> <span>Medicine-II</span></li>
<li><label>10<sup>th </sup>Apr, 2016</label> <span>Obg</span></li>
<li><label>17<sup>th </sup>Apr, 2016</label> <span>Paeds+GT</span></li>
<li><label>24<sup>th </sup>Apr, 2016</label> <span>AIIMS Mock-I</span></li>
<li><label>1<sup>st </sup>May, 2016</label> <span>AIIMS Mock-II</span></li>
</ul>
</div>
</div></div>
<div id="test1">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>07<sup>th </sup>Feb, 2016</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>14<sup>th </sup>Feb, 2016</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>21<sup>st </sup>Feb, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>28<sup>th </sup>Feb, 2016</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>06<sup>th </sup>Mar, 2016</label> <span>Pathology including flow Cytometry</span></li>
<li><label>13<sup>th </sup>Mar, 2016</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>20<sup>th </sup>Mar, 2016</label> <span><b>Grand Test </span></b></li>
<li><label>27<sup>th </sup>Mar, 2016</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>03<sup>rd </sup>Apr, 2016</label> <span>Opthalmology</span></li>
<li><label>10<sup>th </sup>Apr, 2016</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Apr, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Apr, 2016</label> <span>Medicine-I</span></li>
<li><label>01<sup>st </sup>May, 2016</label> <span>AIIMS Mock Test</span></li>
<li class="green_bg"><label>08<sup>th </sup>May, 2016</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>15<sup>th </sup>May, 2016</label> <span>Medicine-II</span></li>
<li><label>22<sup>nd </sup>May, 2016</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>29<sup>th </sup>May, 2016</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>05<sup>th </sup>Jun, 2016</label> <span>Surgery-I</span></li>
<li><label>12<sup>th </sup>Jun, 2016</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Jun, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Jun, 2016</label> <span>Orthopedic and Radiology</span></li>
<li><label>03<sup>rd </sup>Jul, 2016</label> <span>Dermatology-Psychiatry</span></li>
<li><label>10<sup>th </sup>Jul, 2016</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Jul, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Jul, 2016</label> <span>Paeds-II</span></li>
<li><label>31<sup>st </sup>Jul, 2016</label> <span>Oncology</span></li>
</ul>
</div>
</div></div>
<div id="test2">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>06<sup>th </sup>Mar, 2016</label> <span>Pathology including flow Cytometry</span></li>
<li><label>13<sup>th </sup>Mar, 2016</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>20<sup>th </sup>Mar, 2016</label> <span><b>Grand Test </span></b></li>
<li><label>27<sup>th </sup>Mar, 2016</label><span>Microbiology including Microbial Biodegradation</span></li>
<li><label>03<sup>rd </sup>Apr, 2016</label> <span>Opthalmology</span></li>
<li><label>10<sup>th </sup>Apr, 2016</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Apr, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Apr, 2016</label> <span>Medicine-I</span></li>
<li class="light_green_bg"><label>01<sup>st </sup>May, 2016</label> <span>AIIMS Mock Test</span></li>
<li class="green_bg"><label>08<sup>th </sup>May, 2016</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>15<sup>th </sup>May, 2016</label> <span>Medicine-II</span></li>
<li><label>22<sup>nd </sup>May, 2016</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>29<sup>th </sup>May, 2016</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>05<sup>th </sup>Jun, 2016</label> <span>Surgery-I</span></li>
<li><label>12<sup>th </sup>Jun, 2016</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Jun, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Jun, 2016</label> <span>Orthopedic and Radiology</span></li>
<li><label>03<sup>rd </sup>Jul, 2016</label> <span>Dermatology-Psychiatry</span></li>
<li><label>10<sup>th </sup>Jul, 2016</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Jul, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Jul, 2016</label> <span>Paeds-II</span></li>
<li><label>31<sup>st </sup>Jul, 2016</label> <span>Oncology</span></li>
<li><label>07<sup>th </sup>Aug, 2016</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>14<sup>th </sup>Aug, 2016</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>21<sup>st </sup>Aug, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>28<sup>th </sup>Aug, 2016</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
</ul>
</div>
</div></div>
<div id="test3">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li><label>03<sup>rd </sup>Apr, 2016</label> <span>Opthalmology</span></li>
<li><label>10<sup>th </sup>Apr, 2016</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Apr, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Apr, 2016</label> <span>Medicine-I</span></li>
<li class="light_green_bg"><label>01<sup>st </sup>May, 2016</label> <span>AIIMS Mock Test</span></li>

<li class="green_bg"><label>08<sup>th </sup>May, 2016</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>15<sup>th </sup>May, 2016</label> <span>Medicine-II</span></li>
<li><label>22<sup>nd </sup>May, 2016</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>29<sup>th </sup>May, 2016</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>05<sup>th </sup>Jun, 2016</label> <span>Surgery-I</span></li>
<li><label>12<sup>th </sup>Jun, 2016</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Jun, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Jun, 2016</label> <span>Orthopedic and Radiology</span></li>
<li><label>03<sup>rd </sup>Jul, 2016</label> <span>Dermatology-Psychiatry</span></li>
<li ><label>10<sup>th </sup>Jul, 2016</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Jul, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Jul, 2016</label> <span>Paeds-II</span></li>
<li><label>31<sup>st </sup>Jul, 2016</label> <span>Oncology</span></li>
<li><label>07<sup>th </sup>Aug, 2016</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>14<sup>th </sup>Aug, 2016</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>21<sup>st </sup>Aug, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>28<sup>th </sup>Aug, 2016</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>04<sup>th </sup>Sep, 2016</label> <span>Pathology including flow Cytometry</span></li>
<li><label>11<sup>th </sup>Sep, 2016</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Sep, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Sep, 2016</label> <span>Microbiology including Microbial Biodegradation</span></li>
</ul>
</div>
</div></div>
<div id="test4">
<div class="test-tab-content">
<a href="<?php echo $path; ?>/downloadpdf/swt TOPICS  DISTRIBUTION.pdf" target="_blank" id="newtopic" class="newtopicpdf">Topic Distribution</a>
<div class="test-combo-content">
<ul>
<li class="green_bg"><label>08<sup>th </sup>May, 2016</label> <span><b>Tentative date for AIIMS EXAM</b></span></li>
<li><label>15<sup>th </sup>May, 2016</label> <span>Medicine-II</span></li>
<li><label>22<sup>nd </sup>May, 2016</label> <span>Obstetrics &amp; Gynecology</span></li>
<li><label>29<sup>th </sup>May, 2016</label> <span>Biochemistry, Molecular Biology, Forensic Medicine and Toxicology</span></li>
<li><label>05<sup>th </sup>Jun, 2016</label> <span>Surgery-I</span></li>
<li><label>12<sup>th </sup>Jun, 2016</label> <span>Surgery-II</span></li>
<li class="yellow_bg"><label>19<sup>th </sup>Jun, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>26<sup>th </sup>Jun, 2016</label> <span>Orthopedic and Radiology</span></li>
<li><label>03<sup>rd </sup>Jul, 2016</label> <span>Dermatology-Psychiatry</span></li>
<li><label>10<sup>th </sup>Jul, 2016</label> <span>Paeds-I</span></li>
<li class="yellow_bg"><label>17<sup>th </sup>Jul, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>24<sup>th </sup>Jul, 2016</label> <span>Paeds-II</span></li>
<li><label>31<sup>st </sup>Jul, 2016</label> <span>Oncology</span></li>
<li><label>07<sup>th </sup>Aug, 2016</label> <span>Anatomy including Embryology and Neuroanatomy</span></li>
<li><label>14<sup>th </sup>Aug, 2016</label> <span>Physiology with recent advances in Molecular Physiology</span></li>
<li class="yellow_bg"><label>21<sup>st </sup>Aug, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>28<sup>th </sup>Aug, 2016</label> <span>Preventive &amp; Social Medicine including Biostatistics</span></li>
<li><label>04<sup>th </sup>Sep, 2016</label> <span>Pathology including flow Cytometry</span></li>
<li><label>11<sup>th </sup>Sep, 2016</label> <span>Pharmacology including Pharmacogenetics</span></li>
<li class="yellow_bg"><label>18<sup>th </sup>Sep, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>25<sup>th </sup>Sep, 2016</label> <span>Microbiology including Microbial Biodegradation</span></li>

<li><label>02<sup>th </sup>Oct, 2016</label> <span>Opthalmology</span></li>
<li><label>09<sup>th </sup>Oct, 2016</label> <span>Otolarngology including Head and Neck Surgery / Anesthesia</span></li>
<li class="yellow_bg"><label>16<sup>th </sup>Oct, 2016</label> <span><b>Grand Test</b></span></li>
<li><label>23<sup>th </sup>Oct, 2016</label> <span>Medicine-I</span></li>
<li class="light_green_bg"><label>30<sup>th </sup>Oct, 2016</label> <span> AIIMS Mock Test</span></li>
</ul>
</div>
</div></div>
            </div>
          </div>
        </aside>
        <div class="content-right-div">
          <div id="store-wrapper">
            <div class="dams-store-block">
              <div class="dams-store-heading" ><span></span>CATEGORIES</div>
              <div class="dams-store-accordion">
                <div class="dams-inner-store" >
                      <ul>
                        <?php
                            include 'openconnection.php';
                            $sql=mysql_query("select * FROM COURSE WHERE ACTIVE=1");
                            $i=0;
                            while($row=  mysql_fetch_object($sql)){ if($i==0){ ?>
                                                <li id="lis<?php echo $row->COURSE_ID; ?>" onClick="accordion1(<?php echo $row->COURSE_ID; ?>)" <?php if($_REQUEST['c']==$row->COURSE_ID){  ?>style="background:url(images/minus-icon.png) 12px 6px no-repeat;border:none; cursor:pointer"<?php } else{ ?>style="background:url(images/minus-icon.png) 12px 6px no-repeat;border:none; cursor:pointer"<?php } ?>>
                                                    <a title="<?php echo urldecode($row->COURSE_NAME); ?>" ><?php echo urldecode($row->COURSE_NAME); ?></a>
                                                </li>
                                               <?php } else { ?>
                                                <li id="lis<?php echo $row->COURSE_ID; ?>" onClick="accordion1(<?php echo $row->COURSE_ID; ?>)" <?php if($_REQUEST['c']==$row->COURSE_ID){  ?>style="background:url(images/minus-icon.png) 12px 6px no-repeat;border:none; cursor:pointer"<?php } else{ ?>style="background:url(images/plus-icon.png) 12px 6px no-repeat;border:none; cursor:pointer"<?php } ?>><a title="<?php echo urldecode($row->COURSE_NAME); ?>" ><?php echo urldecode($row->COURSE_NAME); ?></a></li>
                                                <?php } ?>
                                                <?php if($i==0){ ?>
                                                        <div class="inner-store" style="display: block" id="d<?php echo $row->COURSE_ID; ?>" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="display:block"<?php } ?>>
                                                            <ol>
                                                                <?php $sql1=mysql_query("select * FROM PRODUCT_CATEGORY WHERE COURSE_ID='$row->COURSE_ID' AND ACTIVE=1 ORDER BY PRODUCT_CATEGORY_ID");
                                                                $k=1;while($rows=  mysql_fetch_object($sql1)){ ?>
                                                                <li style="cursor:pointer"><a id="category<?php echo $row->COURSE_ID.".".$k; ?>" onclick="location.href='dams-publication.php?c=<?php echo $row->COURSE_ID; ?>'"><?php echo urldecode($rows->PRODUCT_CATEGORY_NAME); ?></a></li>
                                                                    <?php $k++; } ?>
                                                                    <input type="hidden" value="<?php echo $k; ?>" id="totalCategory<?php echo $row->COURSE_ID; ?>" />
                                                           </ol>
                                                        </div>
                                                <?php } else { ?>
                                                        <div class="inner-store" id="d<?php echo $row->COURSE_ID; ?>" <?php if($_REQUEST['c']==$row->COURSE_ID){ ?>style="display:block"<?php } ?>>
                                                            <ol>
                                                                <?php $sql1=mysql_query("select * FROM PRODUCT_CATEGORY WHERE COURSE_ID='$row->COURSE_ID' AND ACTIVE=1 ORDER BY PRODUCT_CATEGORY_ID");
                                                                $k=1;while($rows=  mysql_fetch_object($sql1)){ ?>
                                                                <li style="cursor:pointer"><a id="category<?php echo $row->COURSE_ID.".".$k; ?>" onclick="location.href='dams-publication.php?c=<?php echo $row->COURSE_ID; ?>'"><?php echo urldecode($rows->PRODUCT_CATEGORY_NAME); ?></a></li>
                                                                    <?php $k++; } ?>
                                                                    <input type="hidden" value="<?php echo $k; ?>" id="totalCategory<?php echo $row->COURSE_ID; ?>" />
                                                           </ol>
                                                        </div>
                                                <?php }?>
                                                <?php $i++; } ?>
                                              <li class="boder-none">
                                                  <input type="hidden" value="<?php echo $i; ?>" id="totalCourse" />
                                              </li>
                      </ul>
                </div>
              </div>
            </div>
          </div>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry -->
        </div>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
