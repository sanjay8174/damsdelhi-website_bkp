<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php 
session_start();
session_destroy();
include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="contact">
<aside class="banner-left">
<h2>Be smart &amp;<br>take your future in Your Hand </h2>
<h3 class="with_the_launch">If you have the passion to teach &amp; excel,<br>Sky is limit to grow in DAMS</h3>
</aside></article></div></section>
<section class="inner-gallery-content removepadding">
<div class="thanks-note"> Your Information has been sent. </div>
<div class="thankyou">Thank You!</div>
<script type="text/javascript">
var google_conversion_id = 1049334284;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "kk3wCNjsgAIQjKSu9AM";
var google_conversion_value = 1.000000;
var google_remarketing_only = false;
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" class="boder-none" alt="" src="//www.googleadservices.com/pagead/conversion/1049334284/?value=1.000000&amp;label=kk3wCNjsgAIQjKSu9AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>  
</body>
</html>
