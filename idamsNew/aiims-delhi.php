<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<style type="text/css">
.students-box p span{text-transform:none;}
</style>
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a href="national.php" title="National Quiz">National Quiz</a></li>
          <li><a title="National Round" class="active-link">National Round</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li class="light-blue border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aiims-delhi.php" title="All India Institute of Medical Sciences, New Delhi">AIIMS, New Delhi</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="avmc-pondicherry.php" title="AVMC &amp; Hospital, Pondicherry">AVMC &amp; Hospital, Pondicherry</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="cnmc-kolkata.php" title="CNMC Kolkata">CNMC Kolkata</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="gmc-nagpur.php" title="Govt. Medical College, Nagpur">Govt. Medical College, Nagpur</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="jipmer-pondicherry.php" title="JIPMER, Pondicherry">JIPMER, Pondicherry</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="lhmc-delhi.php" title="Lady Hardinge Medical College, New Delhi">LHMC New-Delhi</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="mamc-delhi.php" title="Maulana Azad Medical College, New Delhi">MAMC New-Delhi</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="mims.php" title="Mandya Institute of Medical Science">Mandya Institute of Medical Science</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="smvmc-puducherry.php" title="Sri Manakula Vinayagar Medical College, Puducherry">SMVM College, Puducherry</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="season2round1.php" title="Round 1">Round 1</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="season2round2.php" title="Round 2">Round 2</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="grand2013.php" title="Finale">Finale</a></li>
                </ol>
              </li>
              <li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol3">
                  <li><span class="mini-arrow">&nbsp;</span><a href="round1.php" title="Round 1">Round 1</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="round2.php" title="Round 2">Round 2</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="grand.php" title="Finale">Finale</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>ALL INDIA INSTITUTE OF MEDICAL SCIENCES, NEW DElHI</h4>
            <section class="showme-main">
               <!--<ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="ROUND -1">ROUND -1</a></li>
               <li><a href="#official" title="Video">Video</a></li>
              </ul>-->
              <div id="jquery">
                <article class="interview-photos-section">
                  <!--<div class="idams-box"> <span>FIRST ROUND – "CAMPUS ROUND"</span><br>
                    <p>This is a written qualifying round.
                      The first round will be conducted in the respective medical college itself.
                      Once the test is conducted the answer sheets shall be sent to us for evaluation and the result of the test would be compiled. The first two toppers of this round would qualify to represent their college in the next level i.e. the zonal level.</p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>The winners of GREY MATTER – DAMS NATIONAL QUIZ, ROUND – 1 are out!!! There has been a great battle for the top two spots in every college, as the top two would represent the college in the next round. The students have shown great character in Round – 1 and those with real mettle have triumphed. By this event we have kicked off healthy competitition amongst the best brains. Medical minds all over the country have shown great enthusiasm in both conducting and participating in this prestigious National event. Good show!! Thanks!!!</p>
                  </div>
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
                        <p><span>Dr. DEVANSHU BANSAL</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student5.jpg" alt="Student" title="Student" />
                        <p><span>Dr. SHWETA SHUBHDARSHINI</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
                        <p><span>Dr. DEVIKA KIR </span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
                        <p><span>Dr. ANURAG CHAHAL</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>-->
                  <div class="schedule-mini-series paddin-zero">
                    <!--<h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>-->
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">LOKESH AGARWAL</span> <span class="one-parts">38</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">ZAINAB VORA</span> <span class="one-parts">33</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">SIDDHARTH JAIN</span> <span class="one-parts">31</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">KARTIK GUPTA</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">RITURAJ UPADHAYAY</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">DIVYA AGARWAL</span> <span class="one-parts">29</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">TUNGISH BANSAL</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SRISHTI SAHA</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SELMA C.H.</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMBER AMAR BHAYANA</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">IPSITA SAHOO</span> <span class="one-parts">25</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">SOUMYA SAGAR</span> <span class="one-parts">23</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">MUFEED A K</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREENATH V</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SUNNY SINGHAL</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">NELLAI KRISHNANS</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED RAHMAN</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ROHITH K P</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUPAM KANODIA</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">NAZWIN N K</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">CHNDRA KUMAR KHANDE</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">BASIL</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">VARUN T S</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">SHAFNEED C H</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED</span> <span class="one-parts">14</span> <span class="three-part">14</span> </li>
                        <li> <span class="two-parts schedule-left-line">EBIN SEBASTIEN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAZLU REHMAN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">SACHIN JOSE</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">NITIN PADMANABAN</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">KESAVAN P S</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAWAZ YOUSUF</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SYED SIYAZ BADR</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREELAL T V</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">RAGHU NANDHAN</span> <span class="one-parts">9</span> <span class="three-part">18</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMAL RAZIK</span> <span class="one-parts">7</span> <span class="three-part">19</span> </li>
                      </ul>
                    </div>
                  </div>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                </article>
              </div>
              <!--<div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/947yqPU8uHI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Grey Matter National Quiz Round - 1 Season - 1 - 2012 </span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/qwReQql-VVE?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Grey Matter National Quiz Round - 2 Season - 1 - 2012</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>-->
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script> 
</body>
</html>