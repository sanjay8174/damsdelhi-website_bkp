<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <aside class="banner-right">
        <!--<div class="banner-right-btns"> <a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
        <div class="banner-right-btns"> <a href="http://damspublications.com/" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a title="Result" class="active-link">Result</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading">
            <article class="showme-main">
              <div class="idams-content">
                <div class="schedule-mini-series"> <span class="mini-heading">AIIMS Nov - Result 2011</span>
                  <div class="schedule-mini-top"> <span class="one-part">Rank</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                  <div class="schedule-mini-content">
                    <ul>
                      <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. ABHIMANYU NEHRA</span> </li>
                      <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. NIRANJAN</span> </li>
                      <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. VIVEK TIWARI</span> </li>
                      <li><span class="one-parts">13</span> <span class="two-parts schedule-left-line">Dr. SUVRIT JAIN</span> </li>
                      <li><span class="one-parts">18</span> <span class="two-parts schedule-left-line">Dr. PRIYANKA MADAAN</span> </li>
                      <li><span class="one-parts">14</span> <span class="two-parts schedule-left-line">Dr. DEEPTI PANDIT</span> </li>
                      <li><span class="one-parts">16</span> <span class="two-parts schedule-left-line">Dr. SHREYANS MUTHA</span> </li>
                      <li><span class="one-parts">20</span> <span class="two-parts schedule-left-line">Dr. ALPESH GOYAL</span> </li>
                      <li><span class="one-parts">32</span> <span class="two-parts schedule-left-line">Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM</span> </li>
                      <li><span class="one-parts">38</span> <span class="two-parts schedule-left-line">Dr. SAURABH SINGHAL</span> </li>
                      <li><span class="one-parts">40</span> <span class="two-parts schedule-left-line">Dr. PAWAN KUMAR</span> </li>
                      <li><span class="one-parts">56</span> <span class="two-parts schedule-left-line">Dr. RENU AGRAWAL</span> </li>
                      <li><span class="one-parts">16</span> <span class="two-parts schedule-left-line">Dr. KARAD ABHIJEET BHAUSAHEB</span> </li>
                      <li><span class="one-parts">60</span> <span class="two-parts schedule-left-line">Dr. ANAND BANSAL</span> </li>
                      <li><span class="one-parts">68</span> <span class="two-parts schedule-left-line">Dr. PRIYANKA MINOCHA</span> </li>
                      <li><span class="one-parts">85</span> <span class="two-parts schedule-left-line">Dr. PANANDIKAR GAJANAN</span> </li>
                      <li><span class="one-parts">91</span> <span class="two-parts schedule-left-line">Dr. SUMAN CHATTERJEE</span> </li>
                      <li><span class="one-parts">94</span> <span class="two-parts schedule-left-line">Dr. APURVA ANAND</span> </li>
                      <li><span class="one-parts">95</span> <span class="two-parts schedule-left-line">Dr. ATUL SAROGI</span> </li>
                      <li><span class="one-parts">99</span> <span class="two-parts schedule-left-line">Dr. PINTU SHARMA</span> </li>
                      <li><span class="one-parts">100</span> <span class="two-parts schedule-left-line">Dr. KOUSTAV JANA</span> </li>
                      <li><span class="one-parts">101</span> <span class="two-parts schedule-left-line">Dr. ATUL JAIN</span> </li>
                      <li><span class="one-parts">108</span> <span class="two-parts schedule-left-line">Dr. MANSI BAJAJ</span> </li>
                      <li><span class="one-parts">114</span> <span class="two-parts schedule-left-line">Dr. UMA SHAMKAMAT</span> </li>
                      <li><span class="one-parts">120</span> <span class="two-parts schedule-left-line">Dr. SOURABH JAIN</span> </li>
                      <li><span class="one-parts">128</span> <span class="two-parts schedule-left-line">Dr. ABHISHEK KUMAR</span> </li>
                      <li><span class="one-parts">136</span> <span class="two-parts schedule-left-line">Dr. RAGHU SAMALA</span> </li>
                      <li><span class="one-parts">140</span> <span class="two-parts schedule-left-line">Dr. JAYANT KUMAR</span> </li>
                      <li><span class="one-parts">149</span> <span class="two-parts schedule-left-line">Dr. ANUSHA SURKARA</span> </li>
                      <li><span class="one-parts">155</span> <span class="two-parts schedule-left-line">Dr. PARVATIKAR MITAKI MADHUSUDAN</span> </li>
                      <li><span class="one-parts">156</span> <span class="two-parts schedule-left-line">Dr. ABHINAV SINGH</span> </li>
                      <li><span class="one-parts">157</span> <span class="two-parts schedule-left-line">Dr. SHEJOL SUMIT</span> </li>
                      <li><span class="one-parts">158</span> <span class="two-parts schedule-left-line">Dr. WALZADE PRIYANKA</span> </li>
                      <li><span class="one-parts">161</span> <span class="two-parts schedule-left-line">Dr. N V SAI SUDHA GORTHI</span> </li>
                      <li><span class="one-parts">171</span> <span class="two-parts schedule-left-line">Dr. SHRIVASTAV MANISH</span> </li>
                      <li><span class="one-parts">174</span> <span class="two-parts schedule-left-line">Dr. SUMIT VERMA</span> </li>
                      <li><span class="one-parts">176</span> <span class="two-parts schedule-left-line">Dr. KAPIL JETHA</span> </li>
                      <li><span class="one-parts">178</span> <span class="two-parts schedule-left-line">Dr. REENA SINGH</span> </li>
                      <li><span class="one-parts">185</span> <span class="two-parts schedule-left-line">Dr. FADNIS DEVENDRA PRAKASH</span> </li>
                      <li><span class="one-parts">227</span> <span class="two-parts schedule-left-line">Dr. GAGANPREET S TANEJA</span> </li>
                      <li><span class="one-parts">293</span> <span class="two-parts schedule-left-line">Dr. SHAH JAPAN HARINBHAI</span> </li>
                      <li><span class="one-parts">375</span> <span class="two-parts schedule-left-line">Dr. GYAN RANJAN</span> </li>
                      <li><span class="one-parts">392</span> <span class="two-parts schedule-left-line">Dr. SHELAR TEJASHREE CHANDRAKANT</span> </li>
                      <li><span class="one-parts">405</span> <span class="two-parts schedule-left-line">Dr. AMANDEEP RIYAT</span> </li>
                      <li><span class="one-parts">410</span> <span class="two-parts schedule-left-line">Dr. DEEPIKA GUPTA</span> </li>
                      <li><span class="one-parts">490</span> <span class="two-parts schedule-left-line">Dr. SUDHANSHU BUDAKOTY</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. DEEPIKA KHATRI</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. DEEPIKA SINGH</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. SANJEEV KUMAR</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. ANANDA KUMAR BEHERA</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. ANKIT GUPTA</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. ANTIMA RATHORE</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. GAURAV</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. DINESH SHARMA</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. ABHINAV SINGH</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. DEEPIKA</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. VIJAYA SOLIRIYA</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line">Dr. SANDEEP KUMAR</span> </li>
                      <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more..</span> </li>
                    </ul>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>