<?php
$Dao = new dao();
$count = 0;
$newsDetail= $Dao->getNews($course_id);
$count= count($newsDetail);
?>
<script type="text/javascript">
function newsRs(val){

    <?php for($i=0;$i<$count;$i++){?>
    if(val==<?php echo $i?>){
         <?php for($j=0;$j<$count;$j++){
            if($j!=$i){?>
                $("#ul<?php echo $j?>").hide();
                $("#u<?php echo $j?>").removeClass("current");

            <?php }else{?>
                $("#ul<?php echo $i?>").slideDown(1000,'linear');
                $("#u<?php echo $i?>").addClass("current");
         <?php } } ?>
    }
    <?php } ?>
}
</script>
<div class="news-update-box">
    <div class="n-heading"><span></span> News &amp; Updates</div>
    <div class="news-content-box">
        <div style="width:100%; float:left; height:228px; overflow:hidden;">
                                <?php $j = 0; $i = 0;
                                for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                                    <ul id="ul<?php echo $i;?>" <?php if($i == '0'){ ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
                                        <li>
                                            <span></span>

                                            <p><a style="text-decoration:none" href="news.php?c=<?php echo $course_id;?>"><?php echo $newsDetail[$j++]; ?></a></p>
                                        </li>
                                <?php if ($newsDetail[$j] != '') { ?>
                                        <li class="orange">
                                            <span></span>
                                            <p><a style="text-decoration:none" href="news.php?c=<?php echo $course_id;?>"><?php echo $newsDetail[$j++]; ?></a></p>
                                        </li>
                                <?php } ?>
                                <?php if ($newsDetail[$j] != '') { ?>
                                        <li>
                                            <span></span>
                                            <p><a style="text-decoration:none" href="news.php?c=<?php echo $course_id;?>"><?php echo $newsDetail[$j++]; ?></a></p>
                                        </li>
                                <?php } ?>
                                    </ul><?php
                                }
                                ?>

        </div>
    </div>
                        <div class="box-bottom-1">
                            <span class="mini-view-right"></span>
                            <div class="mini-view-midle">
                                <a href="news.php?c=<?php echo $course_id;?>" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
                                <div class="slider-mini-dot">
                                    <ul>
                                        <?php $i = 0;
                                            for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                                                <li id="u<?php echo $i; ?>" <?php if ($i == '0') { ?> class="current" <?php } ?> onClick="newsRs('<?php echo $i; ?>');"></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <span class="mini-view-left"></span>
                        </div>

</div>