<div class="social-icon">
<a href="https://www.facebook.com/damsdelhiho" target="_blank" title="Connect with us on Facebook" class="l-facebook">Facebook</a>
<a href="https://twitter.com/damsdelhi" target="_blank" title="DAMS on Twitter" class="l-twitter">Twitter</a>
<a href="https://in.linkedin.com/pub/dams-delhi/40/415/570" target="_blank" title="DAMS on Linkedin" class="l-mail">LinkedIn</a>
<a href="https://www.youtube.com/user/damsdelhi" target="_blank" title="Watch DAMS Videos" class="l-print">YouTube</a>
<a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" title="Follow DAMS on Google+" class="l-share">Google Plus</a>
</div>
<div class="top-header">
<ul class="top-nav">
<li><a href="contact.php" title="Contact us">Contact us</a></li>
<li><a href="franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="https://blog.damsdelhi.com/" title="Blog">Blog</a></li>
<li><a href="career.php" title="Career">Career</a></li>
<!--<li><a href="testimonials.php" title="Testimonial">Testimonial</a></li>-->
<li><a href="download.php" title="Download">Download</a></li>
<li style="background:none;"><a href="index.php" title="Home">Home</a></li>
</ul>
</div>
<!-- Header Css Start Here -->

<header>
<div class="wrapper">
<h1><a href="index.php" title="Delhi Academy of Medical Sciences">Delhi Academy of Medical Sciences</a></h1>
<div class="mini-nav-effect">
<!-- Menu 1 -->
<div class="menu">
<div class="menu_button" onclick="Menu.changeMenu()"></div>
</div>
<!-- Menu 2 -->

<div class="menu2">
<div class="header"><a class="link" title="Fixed Digital Agency" href="index.php"><img class="logo" src="navigation/menu_logo.jpg" /></a>
<div class="close" onclick="Menu.changeMenu(false)"></div>
</div>

<div class="menu-content">
<ul>
<li><a href="course-detail.php" title="Face to Face Classes">Face to Face Classes</a></li>
<li><a href="course-detail.php" title="Satelite Classes">Satelite Classes</a></li>
<li><a href="test-series.php" title="Test Series">Test Series</a></li>
<li><a href="achievement.php" title="Achievement">Achievement</a></li>
<li><a href="scholarship.php" title="Scholarship">Scholarship</a></li>
<li><a href="contact.php" title="Contact Us">Contact Us</a></li>
</ul>
</div>

</div> 
<!-- End -->
</div>


<aside class="t-right-side">
<div class="inner-right-top">

<div class="add-cart-section">
<span class="cart-count">5</span>
<a href="javascript:void(0);" class="cart-arrow" id="profileLinkA">&nbsp;</a>

<div style="display:none; width:290px; right:-1px; padding:3px 0px; z-index:99; top:38px; border:1px solid #f68b43; background:#fff; position:absolute;" id="profilePopA" >
<div class="cart-box b-none">
<div class="cart-box-l">AIIMS MDS Examination with Solution Nov...</div>
<div class="cart-box-r"><a href="#" title="Close"></a>
<div class="price-r">Price :  <span><b class="WebRupee">&#8377;</b> 107.75</span></div>
</div></div>

<div class="cart-box">
<div class="cart-box-l">AIIMS MDS Examination with Solution Nov...</div>
<div class="cart-box-r"><a href="#" title="Close"></a>
<div class="price-r">Price :  <span><b class="WebRupee">&#8377;</b> 107.75</span></div>
</div></div>

<div class="total-box" style="border-top:2px solid #E8E8E8; background:none; margin:0px 3%; padding:6px 1% 9px; width:91%;">
<div class="paynow-btn"><a title="Pay Now" href="cart.php">Pay Now</a></div>
<p>Total Price :  <span style="color:#23A61D;"><b class="WebRupee">₹</b><b>323.25</b></span></p>
</div>

</div>


<span class="basket-mini">&nbsp;</span>
</div>


<div class="login-registration">
<a href="javascript:void(0);" class="l-r-left" id="student-login" title="Online Test Student login"><span></span>Online Test<br>Login</a>
<!--<a href="javascript:void(0);" class="l-r-middle" id="cloud-login" title="DAMS Cloud Student login" style="display:none"><span></span>DAMS<br>Cloud</a>-->
<a href="https://dams.sisonline.in/" class="l-r-middle" title="DAMS Cloud Student login" style="display:none"><span></span>DAMS<br>Cloud</a>
<a href="javascript:void(0);" class="l-r-right" id="student-registration" title="New Student Registration"><span></span>New&nbsp;Student<br>Registration</a>
</div>

<div class="inner-some-numbers">
<div class="inner-helpline-no">Helpline&nbsp;no: <span>011-40094009</span></div>
<div class="inner-sms-enquiry">SMS Enquiry: <span>sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span></div>
</div>

</div>
 
</aside>

</div>
</header>
<!-- Header Css End Here -->



<?php ?>