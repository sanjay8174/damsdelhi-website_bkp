

<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Book Store</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var showI=0;
	var showJ=0;

	$('#profileLinkA').click(function(){
		if(showJ==0){
			$('#profilePopA').show();
			showJ=1;
		}else{
			$('#profilePopA').hide();
			showJ=0;
		}
	});
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
	
});

</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'cart-header.php'; ?>



<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
    <article class="dams-store-banner">

<div class="big-nav">
<ul>
<li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aipge_2014.php" title="Achievement">Achievement</a></li>
</ul>
</div>

<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<aside class="banner-right">
<div class="banner-right-btns">
<!--<a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>-->
    <a href="http://damspublications.com/" target="_blank"  title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>
<a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a>
<a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a>
</div></aside>
</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
<ul>
<li style="background:none;"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<!--<li><a href="dams-store.php" title="DAMS Store">DAMS Store</a></li>-->
<li><a href="http://damspublications.com/" target="_blank" title="DAMS Store">DAMS Store</a></li>

<!--<li><a title="Publications" href="dams-store-publication.php">Publications</a></li>-->
<li><a title="Publications" href="http://damspublications.com/" target="_blank">Publications</a></li>
<li><a title="Add To Cart" href="add-to-cart.php">Add To Cart</a></li>
<li><a title="Cart" class="active-link">Cart</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<article class="showme-cart-main">

<link rel="stylesheet" type="text/css" href="https://cdn.webrupee.com/font" />
<script src="https://cdn.webrupee.com/js" type="text/javascript"></script> 


<aside class="cart-main">
<div class="cart-top">
<span class="cart-one">Item Name</span>
<span class="cart-two">Qty.</span>
<span class="cart-three">Unit</span>
<span class="cart-four">Total</span>
<span class="cart-five">&nbsp;</span>
</div>

<div class="cart-contener">
<div class="cart-content white-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
<div class="cart-content gry-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
<div class="cart-content white-bg">
<span class="cart-one">
<img src="images/mini-book.jpg" width="58" height="55" alt="Book"> <p>AIIMS MDS Examination with Solution NOV. (2013)</p> </span>
<span class="cart-two"><input name="" value="1" type="text" class="qty-inp" /></span>
<span class="cart-three"><b>&#8377;</b> 107.00</span>
<span class="cart-four"><b>&#8377;</b> 107.00</span>
<span class="cart-five"><a href="#" title="Remove" class="remove-cart"></a></span>
</div>
</div>

<div class="cart-subtotal">
<span class="cart-left">Total Items : 3</span>
<span class="cart-right">Sub Total : <b>&#8377;</b> 321.00</span>
</div>

<div class="cart-buttons">

<div class="submit-enquiry"><a href="#" title="Checkout">Checkout</a></div>
<div class="submit-enquiry" style="margin-left:10px"><a href="dams-store-publication.php" title="Continue"><span></span>Continue</a></div>


</div>

</aside>



</article>
</div>

</aside>

<aside class="gallery-right">

<div class="enquiry-main">
<div class="enquiry-heading">Enquiry Form</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<form action="" method="get">
<input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';">
<input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';">
<input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';">



<select name="" class="select-input">
<option value="0">Select Course</option>
<option>MD/MS Entrance</option>
<option>MCI Screening</option>
<option>MDS Quest</option>
<option>USMLE Edge</option>
</select>


<select name="" class="select-input">
<option>Centre Interested</option>
<option>Centre Interested</option>
<option>Centre Interested</option>
</select>

<textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
<div class="submit-enquiry"><a href="#" title="Submit"><span></span> Submit</a></div>
</form>
</div>
</div>
<div class="enquiry-bottom"></div>
</div>

</div>

</aside>
</section>
</div>
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->
</body></html>