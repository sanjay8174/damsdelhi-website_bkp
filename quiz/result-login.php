<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>
<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 style="font-size:15px; padding-top:10px;">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <aside class="banner-right">
        <!--<div class="banner-right-btns"> <a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
      <div class="banner-right-btns"> <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a title="Result" class="active-link">Result</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left" style=" width:70%; margin:0 auto; float:none; overflow:hidden;">
          <div class="inner-left-heading">
            <article class="showme-main">
              <div class="enquiry-content-main">
                <div class="enquiry-content" style="background:none;">
                  <h4>Result Login</h4>
                  <div class="enquiry-content-more" style="background:none;">
                    <div class="career-box">
                      <label>User (Roll No)</label>
                      <div class="right-ip">
                        <input name="" type="text"  class="career-inp" />
                        <span>Please enter your correct Username</span></div>
                    </div>
                    <div class="career-box">
                      <label>Password</label>
                      <div class="right-ip">
                        <input name="" type="text"  class="career-inp" />
                        <span class="trans">Please enter your correct Password</span></div>
                    </div>
                    <div class="career-box">
                      <label>&nbsp;</label>
                      <div class="right-ip">
                        <div class="submit-enquiry"><a href="#" title="Login"> Login</a></div>
                      </div>
                      <div class="form-info"> <strong>Note:</strong> You have to fill your roll number as username and fill password which your admin have given you </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>