<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left banner-l-heading">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li id="accor5" onClick="ontab('5');" class="light-blue border_none"><span id="accorsp5" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2015">2015</a>
<ol class="achievment-inner display_block" id="aol5">
<li><span class="mini-arrow">&nbsp;</span><a href="wb_2015.php" title="West Bengal">WB 2015</a></li>
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aipgme_2015.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_nov_2014.php" title="JIPMER NOV">JIPMER NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_nov_2014.php" title="PGI NOV">PGI NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims-nov-2014.php" title="AIIMS NOV">AIIMS NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_none" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_none" id="aol4">
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>

</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>AIPGME 2015</h4>
<section class="showme-main">

<ul class="idTabs idTabs5">
<li><a href="#jquery" class="selected" title="AIPGME 2015">AIPGME 2015 </a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">
<ul class="main-students-list">
<li class="tp-border">
<div class="students-box border_left_none">
<img src="images/students/Ranjan_patel.jpg" alt="Dr. Ranjan Patel" title="Dr. Ranjan Patel" />
<p><span>Dr. Ranjan Patel</span> Rank: 1</p>
</div>

<div class="students-box">
<img src="images/students/Jagneet_chatta.jpg" alt="Dr. Jagneet Chatta" title="Dr. Jagneet Chatta" />
<p><span>Dr. Jagneet Chatta</span> Rank: 16</p>
</div>

<div class="students-box">
<img src="images/students/pinkesh.jpg" alt="Dr. Pinkesh" title="Dr. Pinkesh" />
<p><span>Dr. Pinkesh</span> Rank: 28</p>
</div>

<div class="students-box">
<img src="images/students/shurti_mittal.jpg" alt="Dr. Shurti Mittal" title="Dr. Shurti Mittal" />
<p><span>Dr. Shurti Mittal</span> Rank: 30</p>
</div>
</li>

<li>
<div class="students-box border_left_none">
<img src="images/students/Shubhra_mishra.jpg" alt="Dr. Shubhra Mishra" title="Dr. Shubhra Mishra" />
<p><span>Dr. Shubhra Mishra</span> Rank: 34</p>
</div>

<div class="students-box">
<img src="images/students/Tanya_yadav.jpg" alt="Dr. Tanya Yadav" title="Dr. Tanya Yadav" />
<p><span>Dr. Tanya Yadav</span> Rank: 35</p>
</div>

<div class="students-box">
<img src="images/students/Sonai_suri.jpg" alt="Dr. Sonai Suri" title="Dr. Sonai Suri" />
<p><span>Dr. Sonai Suri</span> Rank: 37</p>
</div>

<div class="students-box">
<img src="images/students/Akhila_bhandarkar.jpg" alt="Dr. Akhila Bhandarkar" title="Dr. Akhila Bhandarkar" />
<p><span>Dr. Akhila Bhandarkar</span> Rank: 43</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Tanvi_modi.jpg" alt="Dr. Tanvi Modi" title="Dr. Tanvi Modi" />
<p><span>Dr. Tanvi Modi</span> Rank: 44</p>
</div>

<div class="students-box">
<img src="images/students/Samrat_mondal.jpg" alt="Dr. Samrat Mondal" title="Dr. Samrat Mondal" />
<p><span>Dr. Samrat Mondal</span> Rank: 45</p>
</div>

<div class="students-box">
<img src="images/students/Sudeshna_malkar.jpg" alt="Dr. Sudeshna Malkar" title="Dr. Sudeshna Malkar" />
<p><span>Dr. Sudeshna Malkar</span> Rank: 52</p>
</div>

<div class="students-box">
<img src="images/students/Jayashri_desi.jpg" alt="Dr. Jayashri Desi" title="Dr. Jayashri Desi" />
<p><span>Dr. Jayashri Desi</span> Rank: 59</p>
</div>
    
<div class="students-box border_left_none">
<img src="images/students/Mohd_ahmad.jpg" alt="Dr. Mohd Ahmad" title="Dr. Mohd Ahmad" />
<p><span>Dr. Mohd Ahmad</span> Rank: 63</p>
</div>
    
<div class="students-box">
<img src="images/students/Jigar_desai.jpg" alt="Dr. Jigar Desai" title="Dr. Jigar Desai" />
<p><span>Dr. Jigar Desai</span> Rank: 71</p>
</div>

<div class="students-box">
<img src="images/students/kruthi_malur.jpg" alt="Dr. Kruthi Malur" title="Dr. Kruthi Malur" />
<p><span>Dr. Kruthi Malur</span> Rank: 80</p>
</div>

<div class="students-box">
<img src="images/students/Abhigyan_mukherji.jpg" alt="Dr. Abhigyan Mukherji" title="Dr. Abhigyan Mukherji" />
<p><span>Dr. Abhigyan Mukherji</span> Rank: 88</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Bhuvan_k_verma.jpg" alt="Dr. Bhuvan K Verma" title="Dr. Bhuvan K Verma" />
<p><span>Dr. Bhuvan K Verma</span> Rank: 92</p>
</div>

<div class="students-box">
<img src="images/students/Shushank_rai.jpg" alt="Dr. Shushank Rai" title="Dr. Shushank Rai" />
<p><span>Dr. Shushank Rai</span> Rank: 99</p>
</div>

<div class="students-box">
<img src="images/students/Pinkesh.jpg" alt="Dr. Pinkesh" title="Dr. Pinkesh" />
<p><span>Dr. Pinkesh</span> Rank: AIR - 28 BMC</p>
</div>

<div class="students-box">
<img src="images/students/Kunal.jpg" alt="Dr. Kunal" title="Dr. Kunal" />
<p><span>Dr. Kunal</span> Rank: AIR - 144 BMC/BJMC </p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Sridhar.jpg" alt="Dr. Sridhar" title="Dr. Sridhar" />
<p><span>Dr. Sridhar</span> Rank: AIR - 157 BMC</p>
</div>

<div class="students-box">
<img src="images/students/Bhavya.jpg" alt="Dr. Bhavya Shah" title="Dr. Bhavya Shah" />
<p><span>Dr. Bhavya Shah</span> Rank: AIR - 265 BJMC</p>
</div>

 <div class="students-box">
<img src="images/students/Jayesh.jpg" alt="Dr. Jayesh" title="Dr. Jayesh" />
<p><span>Dr. Jayesh</span> Rank: AIR - 502 SMIMER</p>
</div>
    
<div class="students-box">
<img src="images/students/Harsh.jpg" alt="Dr. Harsh" title="Dr. Harsh" />
<p><span>Dr. Harsh</span> Rank: AIR - 590 BJMC</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Shachhi_shan.jpg" alt="Dr. Shachi Shah" title="Dr. Shachi Shah" />
<p><span>Dr. Shachi Shah</span> Rank: AIR - 608 <BR/> DNB - 204 SMIMER </p>
</div>

<div class="students-box">
<img src="images/students/Jigar.jpg" alt="Dr. Jigar Patel" title="Dr. Jigar Patel" />
<p><span>Dr. Jigar Patel</span> Rank: AIR - 637 BMC</p>
</div>

<div class="students-box">
<img src="images/students/Bhavya_2.jpg" alt="Dr. Bhavya Shah" title="Dr. Bhavya Shah" />
<p><span>Dr. Bhavya Shah</span> Rank: AIR - 896 NHL</p>
</div>

<div class="students-box">
<img src="images/students/Bansari.jpg" alt="Dr. Bansari" title="Dr. Bansari" />
<p><span>Dr. Bansari</span> Rank: AIR - 1043 GMCS</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/akhachy.jpg" alt="Dr. Akshay" title="Dr. Akshay" />
<p><span>Dr. Akshay</span> Rank: AIR - 1194 <br/> M.P. SHAH</p>
</div>
    
<div class="students-box">
<img src="images/students/Arti_dalwar.jpg" alt="Dr. Arti Dalwar" title="Dr. Arti Dalwar" />
<p><span>Dr. Arti Dalwar</span> Rank: AIR - 1331 BMC</p>
</div>
    
<div class="students-box">
<img src="images/students/Prashant.jpg" alt="Dr. Prashant" title="Dr. Prashant" />
<p><span>Dr. Prashant</span> Rank: AIR - 1406 <br/> DNB - 567 <br/>BMC</p>
</div>
    
 <div class="students-box">
<img src="images/students/Shadev.jpg" alt="Dr. Shadev Patel" title="Dr. Shadev Patel" />
<p><span>Dr. Shadev Patel</span> Rank: AIR - 1500 <br/> DNB - 1200 BMC </p>
</div>
    
<div class="students-box border_left_none">
<img src="images/students/Unnati.jpg" alt="Dr. Unnati" title="Dr. Unnati" />
<p><span>Dr. Unnati</span> Rank: AIR - 1716 GMCS</p>
</div>

 <div class="students-box">
<img src="images/students/Neel_patel.jpg" alt="Dr. Neel Patel" title="Dr. Neel Patel" />
<p><span>Dr. Neel Patel</span> Rank: AIR - 1904 DNB - 1000 <br/> M.P. SHAH </p>
</div>

<div class="students-box">
<img src="images/students/Harsh_2.jpg" alt="Dr. Harsh Patel" title="Dr. Harsh Patel" />
<p><span>Dr. Harsh Patel</span> Rank: AIR - 1971 <br/> DNB - 1270 <br/> M.P. SHAH </p>
</div>

<div class="students-box">
<img src="images/students/Dhrumil.jpg" alt="Dr. Dhrumil" title="Dr. Dhrumil" />
<p><span>Dr. Dhrumil</span> Rank: AIR - 2106 <br/> DNB - 2702 NHL</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Jai_patel.jpg" alt="Dr. Jay Patel" title="Dr. Jay Patel" />
<p><span>Dr. Jay Patel</span> Rank: AIR - 2218 <br/> BMC</p>
</div>
    
<div class="students-box">
<img src="images/students/Sagar_saah.jpg" alt="Dr. Sagar Shah" title="Dr. Sagar Shah" />
<p><span>Dr. Sagar Shah</span> Rank: AIR - 2550 <br/> DNB - 550 GMCS</p>
</div>
    
 <div class="students-box">
<img src="images/students/Jil_patel.jpg" alt="Dr. Jil Patel" title="Dr. Jil Patel" />
<p><span>Dr. Jil Patel</span> Rank: AIR - 2568 <br/> DNB - 3323 <br/> M.P. Shah</p>
</div>
    
<div class="students-box">
<img src="images/students/Niraj_patel.jpg" alt="Dr. Niraj Patel" title="Dr. Niraj Patel" />
<p><span>Dr. Niraj Patel</span> Rank: AIR - 2886 <br/> DNB - 6054 <br/> M.P. Shah</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Dhruvil.jpg" alt="Dr. Dhruvil" title="Dr. Dhruvil" />
<p><span>Dr. Dhruvil</span> Rank: AIR - 3139 <br/> M.P. Shah</p>
</div>

<div class="students-box">
<img src="images/students/Pradeep.jpg" alt="Dr. Pradeep" title="Dr. Pradeep" />
<p><span>Dr. Pradeep</span> Rank: AIR - 3323 <br/>GMCS</p>
</div>

 <div class="students-box">
<img src="images/students/Sandeep.jpg" alt="Dr. Sandeep" title="Dr. Sandeep" />
<p><span>Dr. Sandeep</span> Rank: AIR - 3768 SMIMER</p>
</div>

<div class="students-box">
<img src="images/students/Pratik.jpg" alt="Dr. Pratik" title="Dr. Pratik" />
<p><span>Dr. Pratik</span> Rank: AIR - 3787 <br/> GMCS</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Dhabal.jpg" alt="Dr. Dhaval" title="Dr. Dhaval" />
<p><span>Dr. Dhaval</span> Rank: AIR - 4049 <br/>BMC</p>
</div>

<div class="students-box">
<img src="images/students/kushali.jpg" alt="Dr. Khushali" title="Dr. Khushali" />
<p><span>Dr. Khushali</span> Rank: AIR - 4122 <br/>BMC</p>
</div>

 <div class="students-box">
<img src="images/students/Dipti_patel.jpg" alt="Dr. Dipti Patel" title="Dr. Dipti Patel" />
<p><span>Dr. Dipti Patel</span> Rank: AIR - 4149 <br/>DNB - 2313 <br/> M.P SHAH</p>
</div>

<div class="students-box">
<img src="images/students/piyush.jpg" alt="Dr. Piyush" title="Dr. Piyush" />
<p><span>Dr. Piyush</span> Rank: AIR - 4203 <br/>DNB - 1173 <br/> SMIMER</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Sulay.jpg" alt="Dr. Sulay" title="Dr. Sulay" />
<p><span>Dr. Sulay</span> Rank: AIR - 4262 <br/>BJMC</p>
</div>

 <div class="students-box">
<img src="images/students/Sailesh.jpg" alt="Dr. Sailesh" title="Dr. Sailesh" />
<p><span>Dr. Sailesh</span> Rank: AIR - 4754 <br/> M.P SHAH</p>
</div>

<div class="students-box">
<img src="images/students/Saumya.jpg" alt="Dr. Saumya" title="Dr. Saumya" />
<p><span>Dr. Saumya</span> Rank: AIR - 4889 <br/>GMCS</p>
</div>

<div class="students-box">
<img src="images/students/Prashanna.jpg" alt="Dr. Prasanna" title="Dr. Prasanna" />
<p><span>Dr. Prasanna</span> Rank: AIR - 1967<br/> SMIMER</p>
</div>

<div class="students-box border_left_none">
<img src="images/students/Rupam.jpg" alt="Dr. Rupam" title="Dr. Rupam" />
<p><span>Dr. Rupam  </span> Rank: AIR - 1082 <br/>GMCS</p>
</div>
<div class="students-box">
<img src="images/students/Ankita_ujjwal_shah.jpg" alt="Dr. Ankita Ujjwal Shah" title="Dr. Ankita Ujjwal Shah" />
<p><span>Dr. Ankita Ujjwal Shah  </span> Rank: AIR - 40</p>
</div>
<div class="students-box">
<img src="images/students/pankaj_kumar_sahu.jpg" alt="Dr. Pankaj Kumar Sahu" title="Dr. Pankaj Kumar Sahu" />
<p><span>Dr. Pankaj Kumar Sahu  </span> Rank: AIR - 98</p>
</div>
<div class="students-box">
<img src="images/students/aipg_1.jpg" alt="Dr. Fahmina Faridi" title="Dr. Fahmina Faridi" />
<p><span>Dr. Fahmina Faridi</span> Rank: 279</p>
</div>
<div class="students-box">
<img src="images/students/aipg_2.jpg" alt="Dr. Ankita Maheshwari" title="Dr. Ankita Maheshwari" />
<p><span>Dr. Ankita Maheshwari</span> Rank: 349</p>
</div>
<div class="students-box">
<img src="images/students/aipg_3.jpg" alt="Dr. Naved Ahmad" title="Dr. Naved Ahmad" />
<p><span>Dr. Naved Ahmad</span> Rank: 967</p>
</div>
 <div class="students-box">
<img src="images/students/aipg_4.jpg" alt="Dr. Mohd. Faisal" title="Dr. Mohd. Faisal" />
<p><span>Dr. Mohd. Faisal</span> Rank: 974</p>
</div>
<div class="students-box">
<img src="images/students/aipg_5.jpg" alt="Dr. Arpita Katheria" title="Dr. Arpita Katheria" />
<p><span>Dr. Arpita Katheria</span> Rank: 1055</p>
</div>
<div class="students-box">
<img src="images/students/aipg_6.jpg" alt="Dr. Mukesh Maurya" title="Dr. Mukesh Maurya" />
<p><span>Dr. Mukesh Maurya</span> Rank: 1062</p>
</div>
<div class="students-box">
<img src="images/students/aipg_7.jpg" alt="Dr. Kriti Singh" title="Dr. Kriti Singh" />
<p><span>Dr. Kriti Singh</span> Rank: 1232</p>
</div>
<div class="students-box">
<img src="images/students/aipg_8.jpg" alt="Dr. Anuja Sapre" title="Dr. Anuja Sapre" />
<p><span>Dr. Anuja Sapre</span> Rank: Air - 1700</p>
</div>
<div class="students-box">
<img src="images/students/aipg_9.jpg" alt="Dr. Abhishek Verma" title="Dr. Abhishek Verma" />
<p><span>Dr. Abhishek Verma</span> Rank: 2550</p>
</div>
<div class="students-box">
<img src="images/students/aipg_10.jpg" alt="Dr. Alok Dubey" title="Dr. Alok Dubey" />
<p><span>Dr. Alok Dubey</span> Rank: 2600</p>
</div>
<div class="students-box">
<img src="images/students/aipg_11.jpg" alt="Dr. Isha Chaturvedi" title="Dr. Isha Chaturvedi" />
<p><span>Dr. Isha Chaturvedi</span> Rank: Air - 2141</p>
</div>
<div class="students-box">
<img src="images/students/aipg_12.jpg" alt="Dr. Saquib Khan" title="Dr. Saquib Khan" />
<p><span>Dr. Saquib Khan</span> Rank: 2421</p>
</div>
<div class="students-box">
<img src="images/students/aipg_13.jpg" alt="Dr. Priyanka Yadav" title="Dr. Priyanka Yadav" />
<p><span>Dr. Priyanka Yadav</span> Rank: 2615</p>
</div>
<div class="students-box">
<img src="images/students/aipg_14.jpg" alt="Dr. Anand Kumar Yadav" title="Dr. Anand Kumar Yadav" />
<p><span>Dr. Anand Kumar Yadav</span> Rank: 2673</p>
</div>
<div class="students-box">
<img src="images/students/aipg_15.jpg" alt="Dr. Priyanka Tiwari" title="Dr. Priyanka Tiwari" />
<p><span>Dr. Priyanka Tiwari</span> Rank: 3106</p>
</div>
 <div class="students-box">
<img src="images/students/aipg_16.jpg" alt="Dr. Amit Singh" title="Dr. Amit Singh" />
<p><span>Dr. Amit Singh</span> Rank: 3114</p>
</div>
<div class="students-box">
<img src="images/students/aipg_17.jpg" alt="Dr. Kanchan Singh" title="Dr. Kanchan Singh" />
<p><span>Dr. Kanchan Singh</span> Rank: 3147</p>
</div>
<div class="students-box">
<img src="images/students/aipg_18.jpg" alt="Dr. Alok Srivastava" title="Dr. Alok Srivastava" />
<p><span>Dr. Alok Srivastava</span> Rank: 3254</p>
</div>
<div class="students-box">
<img src="images/students/aipg_20.jpg" alt="Dr. Sweta Yadav" title="Dr. Sweta Yadav" />
<p><span>Dr. Sweta Yadav</span> Rank: 3500</p>
</div>
<div class="students-box">
<img src="images/students/aipg_21.jpg" alt="Dr. Manjul Maurya" title="Dr. Manjul Maurya" />
<p><span>Dr. Manjul Maurya</span> Rank: 4277</p>
</div>
<div class="students-box">
</div>
</li>

</ul>
<br style="clear:both;" />
<br style="clear:both;" />
<div class="schedule-mini-series">

<span class="mini-heading">Record 3000+ Selections in TOP 5000 AIPGME 2015</span>

<div class="schedule-mini-top">
<span class="one-part">Rank</span>
<span class="two-part">Student Name</span>
<span class="three-part">&nbsp;</span>
</div>
<div class="schedule-mini-content">
<ul>
<li class="boder-none"><span class="one-parts">1</span>
<span class="two-parts schedule-left-line">Dr. Ranjan Patel</span>
</li>
<li><span class="one-parts">16</span>
<span class="two-parts schedule-left-line">Dr. Jagneet Chatha</span>
</li>
<li><span class="one-parts">28</span>
<span class="two-parts schedule-left-line">Dr. Pinkesh</span>
</li>
<li><span class="one-parts">30</span>
<span class="two-parts schedule-left-line">Dr. Shruti Mittal</span>
</li>
<li><span class="one-parts">34</span>
<span class="two-parts schedule-left-line">Dr. Shubhra Mishra</span>
</li>
<li><span class="one-parts">35</span>
<span class="two-parts schedule-left-line">Dr. Tanya Yadav</span>
</li>
<li><span class="one-parts">37</span>
<span class="two-parts schedule-left-line">Dr. Soali Suri</span>
</li>
<li><span class="one-parts">40</span>
<span class="two-parts schedule-left-line">Dr. Ankita Ujjwal Shah</span>
</li>
<li><span class="one-parts">43</span>
<span class="two-parts schedule-left-line">Dr. Akhila Bhandarkar</span>
</li>
<li><span class="one-parts">44</span>
<span class="two-parts schedule-left-line">Dr. Tanvi Modi</span>
</li>
<li><span class="one-parts">45</span>
<span class="two-parts schedule-left-line">Dr. Samrat Mondal</span>
</li>
<li><span class="one-parts">52</span>
<span class="two-parts schedule-left-line">Dr. Sudeshna Malkar</span>
</li>
<li><span class="one-parts">59</span>
<span class="two-parts schedule-left-line">Dr. Jayashri Desai</span>
</li>
<li><span class="one-parts">63</span>
<span class="two-parts schedule-left-line">Dr. Mohd Ahmad</span>
</li>
<li><span class="one-parts">65</span>
<span class="two-parts schedule-left-line">Dr. Abhinaya Venkatakrishnan</span>
</li>
<li><span class="one-parts">71</span>
<span class="two-parts schedule-left-line">Dr. Jigar Desai</span>
</li>
<li><span class="one-parts">80</span>
<span class="two-parts schedule-left-line">Dr. Kruthi Malur</span>
</li>
<li><span class="one-parts">88</span>
<span class="two-parts schedule-left-line">Dr. Abhigyan Mukherjee</span>
</li>
<li><span class="one-parts">92</span>
<span class="two-parts schedule-left-line">Dr. Bhuwan. K. Verma</span>
</li>
<li><span class="one-parts">98</span>
<span class="two-parts schedule-left-line">Dr. Pankaj Kumar Sahu</span>
</li>
<li><span class="one-parts">99</span>
<span class="two-parts schedule-left-line">Dr. Shashank Raj</span>
</li>
<li><span class="one-parts">246</span>
<span class="two-parts schedule-left-line">Dr. Rohitha C</span>
</li>
<li><span class="one-parts">655</span>
<span class="two-parts schedule-left-line">Dr. Varun B R</span>
</li>
<li><span class="one-parts">1006</span>
<span class="two-parts schedule-left-line">Dr. Sunisha Arora</span>
</li>
<li><span class="one-parts">1242</span>
<span class="two-parts schedule-left-line">Dr. Kumar B N</span>
</li>
<li><span class="one-parts">1761</span>
<span class="two-parts schedule-left-line">Dr. Jayanth H</span>
</li>
<li><span class="one-parts">2146</span>
<span class="two-parts schedule-left-line">Dr. Anaga Pujari</span>
</li>
<li><span class="one-parts">2226</span>
<span class="two-parts schedule-left-line">Dr. Sunayana N</span>
</li>
<li><span class="one-parts">2565</span>
<span class="two-parts schedule-left-line">Dr. Ashay Telang</span>
</li>
<li><span class="one-parts">2994</span>
<span class="two-parts schedule-left-line">Dr. Sanjana Singh </span>
</li>
<li><span class="one-parts">3431</span>
<span class="two-parts schedule-left-line">Dr. Jasmine Kandagal</span>
</li>
<li><span class="one-parts">3600</span>
<span class="two-parts schedule-left-line">Dr. Jaidev</span>
</li>
<li><span class="one-parts">4198</span>
<span class="two-parts schedule-left-line">Dr. Samarth B S</span>
</li>
<li><span class="one-parts">28</span>
<span class="two-parts schedule-left-line">Dr. Pinkesh Parmar</span>
</li>
<li><span class="one-parts">144</span>
<span class="two-parts schedule-left-line">Dr. Kunal Jobanputra </span>
</li>
<li><span class="one-parts">157</span>
<span class="two-parts schedule-left-line">Dr. Shridhar Joshi </span>
</li>
<li><span class="one-parts">265</span>
<span class="two-parts schedule-left-line">Dr. Bhavya Shah </span>
</li>
<li><span class="one-parts">502</span>
<span class="two-parts schedule-left-line">Dr. Jayesh Kakadiya</span>
</li>
<li><span class="one-parts">590</span>
<span class="two-parts schedule-left-line">Dr. Haresh Lathiya</span>
</li>
<li><span class="one-parts">608</span>
<span class="two-parts schedule-left-line">Dr. Sachi Shah</span>
</li>
<li><span class="one-parts">657</span>
<span class="two-parts schedule-left-line">Dr. Jigar Patel </span>
</li>
<li><span class="one-parts">896</span>
<span class="two-parts schedule-left-line">Dr. Bhavya Shah</span>
</li>
<li><span class="one-parts">1043</span>
<span class="two-parts schedule-left-line">Dr. Bansari Prajapati </span>
</li>
<li><span class="one-parts">1082</span>
<span class="two-parts schedule-left-line">Dr. Rupam Jain</span>
</li>
<li><span class="one-parts">1194</span>
<span class="two-parts schedule-left-line">Dr. Akshay Pendkar</span>
</li>
<li><span class="one-parts">1331</span>
<span class="two-parts schedule-left-line">Dr. Arti Dalwani</span>
</li>
<li><span class="one-parts">1406</span>
<span class="two-parts schedule-left-line">Dr. Prashant Patel </span>
</li>
<li><span class="one-parts">1500</span>
<span class="two-parts schedule-left-line">Dr. Sahdev Patel</span>
</li>
<li><span class="one-parts">1716(Category)</span>
<span class="two-parts schedule-left-line">Dr. Unnati Parmar</span>
</li>
<li><span class="one-parts">1904</span>
<span class="two-parts schedule-left-line">Dr. Neel Patel</span>
</li>
<li><span class="one-parts">1971</span>
<span class="two-parts schedule-left-line">Dr. Harsh Patel</span>
</li>
<li><span class="one-parts">2106</span>
<span class="two-parts schedule-left-line">Dr. Dhurmil Dave</span>
</li>
<li><span class="one-parts">2218</span>
<span class="two-parts schedule-left-line">Dr. Jay Patel</span>
</li>
<li><span class="one-parts">2550</span>
<span class="two-parts schedule-left-line">Dr. Sagar Shah</span>
</li>
<li><span class="one-parts">2568</span>
<span class="two-parts schedule-left-line">Dr. Jil Patel</span>
</li>
<li><span class="one-parts">2886</span>
<span class="two-parts schedule-left-line">Dr. Niraj Patel</span>
</li>
<li><span class="one-parts">3139</span>
<span class="two-parts schedule-left-line">Dr. Dhruvin J Mehta</span>
</li>
<li><span class="one-parts">3323</span>
<span class="two-parts schedule-left-line">Dr. Pradeep Vekaryia</span>
</li>
<li><span class="one-parts">3768</span>
<span class="two-parts schedule-left-line">Dr. Sandip Talwani</span>
</li>
<li><span class="one-parts">3787</span>
<span class="two-parts schedule-left-line">Dr. Prateek Siddhpuria</span>
</li>
<li><span class="one-parts">4049</span>
<span class="two-parts schedule-left-line">Dr. Dhaval M Patel</span>
</li>
<li><span class="one-parts">4122</span>
<span class="two-parts schedule-left-line">Dr. Khushali Kotecha</span>
</li>
<li><span class="one-parts">4149</span>
<span class="two-parts schedule-left-line">Dr. Dipti Patel</span>
</li>
<li><span class="one-parts">4262</span>
<span class="two-parts schedule-left-line">Dr. Sulay A Sachdev</span>
</li>
<li><span class="one-parts">4754</span>
<span class="two-parts schedule-left-line">Dr. Shailesh Pandya</span>
</li>
<li><span class="one-parts">4889</span>
<span class="two-parts schedule-left-line">Dr. Saumya Iyer</span>
</li>
<li><span class="one-parts">52</span>
<span class="two-parts schedule-left-line">Dr Sudeshna Malakar(Regular)</span>
</li>
<li><span class="one-parts">195</span>
<span class="two-parts schedule-left-line">Dr Dr Amit Mondal</span>
</li>
<li><span class="one-parts">204</span>
<span class="two-parts schedule-left-line">Dr Manisha Agrawal(Regular)</span>
</li>
<li><span class="one-parts">283</span>
<span class="two-parts schedule-left-line">Dr Subarna Dutta</span>
</li>
<li><span class="one-parts">346</span>
<span class="two-parts schedule-left-line">Dr Arunava Sarkar(T&D)</span>
</li>
<li><span class="one-parts">416</span>
<span class="two-parts schedule-left-line">Dr Usnish Adhikari(T&D)</span>
</li>
<li><span class="one-parts">423</span>
<span class="two-parts schedule-left-line">Dr Aditya Ganeriwala(Regular)</span>
</li>
<li><span class="one-parts">443</span>
<span class="two-parts schedule-left-line">Dr Sayoni Roy Chowdhury(T&D)</span>
</li>
<li><span class="one-parts">503</span>
<span class="two-parts schedule-left-line">Dr Vivek Goel(Regular)</span>
</li>
<li><span class="one-parts">543</span>
<span class="two-parts schedule-left-line">Dr Neerja Pandey</span>
</li>
<li><span class="one-parts">559</span>
<span class="two-parts schedule-left-line">Dr Arijita Chatterjee</span>
</li>
<li><span class="one-parts">630</span>
<span class="two-parts schedule-left-line">Dr Mudit Shah</span>
</li>
<li><span class="one-parts">640</span>
<span class="two-parts schedule-left-line">Dr Keka Mandal</span>
</li>
<li><span class="one-parts">666</span>
<span class="two-parts schedule-left-line">Dr Debanjan Adak</span>
</li>
<li><span class="one-parts">723</span>
<span class="two-parts schedule-left-line">Dr Juhi Dhanawat</span>
</li>
<li><span class="one-parts">726</span>
<span class="two-parts schedule-left-line">Dr Abhishek Kumar</span>
</li>
<li><span class="one-parts">768</span>
<span class="two-parts schedule-left-line">Dr Himadri Koley</span>
</li>
<li><span class="one-parts">942</span>
<span class="two-parts schedule-left-line">Dr Indranil Dutta(T&D)</span>
</li>
<li><span class="one-parts">279</span>
<span class="two-parts schedule-left-line">Dr. Fahmina Faridi</span>
</li>
<li><span class="one-parts">349</span>
<span class="two-parts schedule-left-line">Dr. Ankita Maheshwari</span>
</li>
<li><span class="one-parts">967</span>
<span class="two-parts schedule-left-line">Dr. Naved Ahmad</span>
</li>
<li><span class="one-parts">974</span>
<span class="two-parts schedule-left-line">Dr. Mohd. Faisal</span>
</li>
<li><span class="one-parts">1055</span>
<span class="two-parts schedule-left-line">Dr. Arpita Katheria</span>
</li>
<li><span class="one-parts">1062</span>
<span class="two-parts schedule-left-line">Dr. Mukesh Maurya</span>
</li>
<li><span class="one-parts">1232</span>
<span class="two-parts schedule-left-line">Dr. Kriti Singh</span>
</li>
<li><span class="one-parts">1700</span>
<span class="two-parts schedule-left-line">Dr. Anuja Sapre</span>
</li>
<li><span class="one-parts">2550</span>
<span class="two-parts schedule-left-line">Dr. Abhishek Verma</span>
</li>
<li><span class="one-parts">2600</span>
<span class="two-parts schedule-left-line">Dr. Alok Dubey</span>
</li>
<li><span class="one-parts">2141</span>
<span class="two-parts schedule-left-line">Dr. Isha Chaturvedi</span>
</li>
<li><span class="one-parts">2421</span>
<span class="two-parts schedule-left-line">Dr. Saquib Khan</span>
</li>
<li><span class="one-parts">2615</span>
<span class="two-parts schedule-left-line">Dr. Priyanka Yadav</span>
</li>
<li><span class="one-parts">2673</span>
<span class="two-parts schedule-left-line">Dr. Anand Kumar Yadav</span>
</li>
<li><span class="one-parts">3106</span>
<span class="two-parts schedule-left-line">Dr. Priyanka Tiwari</span>
</li>
<li><span class="one-parts">3114</span>
<span class="two-parts schedule-left-line">Dr. Amit Singh</span>
</li>
<li><span class="one-parts">3147</span>
<span class="two-parts schedule-left-line">Dr. Kanchan Singh</span>
</li>
<li><span class="one-parts">3254</span>
<span class="two-parts schedule-left-line">Dr. Alok Srivastava</span>
</li>
<li><span class="one-parts">3500</span>
<span class="two-parts schedule-left-line">Dr. Sweta Yadav</span>
</li>
<li><span class="one-parts">4277</span>
<span class="two-parts schedule-left-line">Dr. Manjul Maurya</span>
</li>
<li><span class="one-parts">&nbsp;</span>
<span class="two-parts schedule-left-line" style="color:#000;">And many more...</span>
</li>
</ul>
</div>
</div>
</article></div>

<div id="official">
<article class="interview-photos-section">

<div class="achievment-videos-section">
<ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Cn1YckjnbhY" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Shashank Raj</span></p>
                            <p>Rank: 4th PGI, Rank: 28th AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OWv9mXT_fxI" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Abhenil Mittal</span></p>
                            <p>Rank: 6th AIIMS Nov 2014, Rank: 7th PGI</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OFy6m-azegA" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Siddharth Jain</span></p>
                            <p>Rank: 1st AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/EbUbqGEiuVM?list=PLcou4I3N5obYaiRIyAx4TKlraHGS5VdHa" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Ravi Sharma</span></p>
                            <p>Rank: 2nd AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
                            <p>Rank: 1st AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
                            <p>Rank: 56th AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
                            <p>Rank: 6th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
                            <p>Rank: 24th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>

                    </ul>
</div>
</article>
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
