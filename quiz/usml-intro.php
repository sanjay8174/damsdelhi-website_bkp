<!DOCTYPE html>
<?php
$course_id = '4';
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
$Dao = new dao();
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USMLE EDGE Coaching Institute, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>USMLE EDGE</h3>
        <p>It is getting progressively difficult each year to get into the US medical system, yet we at DAMS believe that it is the lack of information that is the biggest drawback that an IMG experiences to get to medical licensure in the United States. We at DAMS now take up that responsibility to provide the right information and guide to do the right thing at the right time to achieve success in United States Medical Licensing Examination.<br>
          <br>
          We propose our own unique course which is integrated <strong>Foundation PG course with USMLE EDGE. How many of us are sure in the prefinal-final year stage about USMLE? How many times have you asked this question-we need a course which gives us both? Yes DAMS now offers the only course which offers online simulated exams for both steps and counselling sessions for USMLE integrated with its very popular PG foundation course.</strong><br>
          <br>
          <strong>Another course, we are now offering is the USMLE Simulated Test Series with counselling sessions for people who want to appear for only USMLE. Very soon we will be launching our own class room programme for the USMLE as well.</strong><br>
          <br>
          The USMLE assesses a physician's ability to apply knowledge, concepts, and principles, and to demonstrate fundamental patient-centered skills, that are important in health and disease and that constitute the basis of safe and effective patient care. Each of the three Steps of the USMLE complements the others; no Step can stand alone in the assessment of readiness for medical licensure. </p>
        <p>The United States Medical Licensing Examination is a three-step examination for medical licensure in the United States and is sponsored by the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME).</p>
      </div>
      <?php include 'usmle-middle-accordion.php'; ?>
    </aside>
    <aside class="content-right">
      <?php include 'dams-usmle-edge.php'; ?>
      <?php include 'newsRight.php'; ?>
      <?php include 'studentInterview.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');
	   }
	 }
   });
   $("#next").click(function(){
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }
   });

   window.onload = function()
   {
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");
	       l++;
        }
     videointerval = setInterval(setvideo, 5000);
   }
 });
 function news(val)
 {
   if(val=='0')
   {
	  $("#ul1").hide();
	  $("#u1").removeClass("current");
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current");
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current");
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current");
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
 }

 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {
      $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd0").addClass("display_block");
	  $("#vd0").fadeIn('fast','linear');
	  $("#vd0").removeClass("display_none");
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd1").addClass("display_block");
	  $("#vd1").fadeIn('fast','linear');
	  $("#vd1").removeClass("display_none");
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd2").addClass("display_block");
	  $("#vd2").fadeIn('fast','linear');
	  $("#vd2").removeClass("display_none");
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").addClass("display_block");
	  $("#vd3").fadeIn('fast','linear');
	  $("#vd3").removeClass("display_none");
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);
 }

 function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {
       if(val!=d)
	   {
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }
   }

   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script>-->
</body>
</html>