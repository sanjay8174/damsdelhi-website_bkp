<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article>
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Video Conferencing" class="active-link">Video Conferencing</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Video Conferencing</h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <p>'Video Conferencing' Classes for PG Medical Entrance Exam available All Over India
                  Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations.</p>
                <p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>
                <p>We have seen that student in small town &amp; tier II cities are at a disadvantage that our classroom centres are mainly in metropolitan cities. Now with technology we have launched in country which can have exclusive Video Conferencing for PG Medical Entrance Exam in 242 centres across the country in partnership with Reliance World.</p>
                <p>After a huge success of Virtual Classroom Coaching in 2011. The new session for the Virtual Classroom Coaching is starting soon.</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Features :-</div>
                <ul class="benefits">
                  <li><span></span>33 Sessions</li>
                  <li><span></span>Individual sessions for 4 hrs (Total 132 hrs)</li>
                  <li><span></span>Classes twice in a week</li>
                  <li><span></span>Available in all "Reliance Web World" centre</li>
                  <li><span></span>Interactive classes by best exclusive DAMS faculty.</li>
                  <li><span></span>Best results in business</li>
                  <li><span></span>Small batches</li>
                  <li><span></span>Two way communications centers</li>
                  <li><span></span>The participants get to interact with professors just the way they would in a real classroom (which means they can see, hear &amp; even ask questions). Several premier educational institutes are successfully using this platform to expand the reach of their courses beyond campus-based programmes.</li>
                </ul>
              </aside>
              <span class="additional-heading">Additional Features :-</span>
              <p>Online Test Series (includes Grand Tests, Neet Mocks &amp; Subject wise test series)</p>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Note:- Free Demo class is also available.</div>
                <div class="download-pdf">
                  <ul>
                    <li>
                      <div class="download">
                        <div class="orange-box">Download the Registration form</div>
                        <a href="https://damsdelhi.com/REGISTRATION%20FORM%20for%20website.pdf"><img src="images/download-arrow.png" title="Download" alt="Download" /> <span>Download</span></a> </div>
                    </li>
                  </ul>
                </div>
              </aside>
            </article>
            <div class="video-conferencing">
              <h4>Video conferecing Demo Session 2012</h4>
              <article class="achievment-videos-section">
                <ul>
                  <li>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/_fmH7rgZQFs" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/BE2SCC6x_1s" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </article>
            </div>
            <div class="video-conferencing">
              <h4>Video conferecing Demo Session 2011</h4>
              <article class="achievment-videos-section">
                <ul>
                  <li>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/fwAqUS-nWM0" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                    <div class="video-box">
                      <iframe width="329" height="247" src="//www.youtube.com/embed/iaGhCWPT4wk" class="border_none"></iframe>
                      <div class="video-content">
                        <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                        <p>Rank: 28th AIIMS May 2012</p>
                        <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </article>
            </div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry -->        
          <div class="virtual-class">
            <div class="virtual-heading"><a href="#" title="Click Here"><span class="white-arrow"></span>Click Here</a> Virtual Class Room Centre</div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
