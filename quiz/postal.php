<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Postal Course, Postal Coaching – PG Medical Entrance Examination, NEET PG</title>
<meta name="description" content=" DAMS, one best PG Medical Entrance Coaching Institute, offers you Postal Medical Entrance Coaching for PG Medical Entrance, " />
<meta name="keywords" content="Postal Coaching, Postal Course for PG Medical Entrance Exam, MD/MS Entrance Examination, Medical Entrance Coaching, Medical Entrance Coaching Delhi, Medical Coaching, Delhi, Medical Coaching India" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 1;
$courseNav_id = 1;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Postal Course" class="active-link">Postal Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Postal Course <span class="book-ur-seat-btn book-hide"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <p>If you can not come to DAMS, our courses will come to you. Join our Postal Series Delhi Academy of Medical Sciences (DAMS) provides postal courses. The study material is developed by DAMS, which has been working since last one decade. The study material is compact &amp; effective which is neither bulky nor vague. Instead it is easy to understand and has unique presentation of all the subject as per requirements of examination. DAMS team has worked hard to provide error free text and smart &amp; shortcut methods to solve problems</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span> About the exam :-</div>
                <p>NIMHANS PG Medical Entrance Exam has a single paper. This paper encompasses MCQ's of the objective nature. Questions of the graduate level shall be posed in this paper. The time period of this examination is based on the many departments. Aspirants who have a clear knowledge about the pattern and syllabus of the MBBS course will be able to perform admirably in this exam.</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Course Highlights :-</div>
                <ul class="benefits">
                  <li><span></span>The study material includes full coverage of syllabus of PG medical entrance examination including high yielding text as well as cutting edge question banks.</li>
                  <li><span></span>The study material includes subject wise sequentially presented theory sections and practice sets which include solved questions with answers and explanations.</li>
                  <li><span></span>The study material is proved to be highly useful for all the examinations including AIPG, AIIMS, PGI Chandigarh, State PG exams, UPSC, DNB and MCI screening examinations. The previous feedback of the students is highly appreciable who have succeded in various examinations.</li>
                  <li><span></span>Date of admission test: Last week of June.</li>
                  <li><span></span>Study material is time to time updated and latest updation and current developments are sent time to time . Study material includes previous solved questions of 6 to 10 years.</li>
                </ul>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>HOW TO APPLY ?</div>
                <p>Download Application form &amp; enclose Demand Draft (note that cheques are not accepted) of concerned fee as mentioned in fee structure (including postal charges).</p>
                <p>Duly filled Application form alongwith demand draft to be made in favour of "Delhi Academy of Medical Sciences Pvt. Ltd.", payable at New Delhi  should be sent by registered post / courier to :</p>
                <p>Delhi Academy of Medical Sciences
                  Grovers Chamber
                  4B, 3rd Floor, Pusa Road, 
                  New Delhi-110005</p>
                <p>Now courses are avaliable through online payment. For buying any course please go to <a href="http://registration.damsdelhi.com" target="_blank">https://damsdelhi.com/online-registration.php.</a></p>
              </aside>
            </article>
            <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <div class="national-quiz-add"> <a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a> </div>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>