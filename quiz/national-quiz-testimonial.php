<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS, Concept of grey matter, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
//	$('div.accordionButton').click(function() {
//		$('div.accordionContent').slideUp('normal');	
//		$(this).next().slideDown('normal');
//	});		
//	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a href="national.php" title="National Quiz">National Quiz</a></li>
          <li><a title="Testimonials" class="active-link">Testimonials</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Testimonials Grey Matter Participants - Season-1 All India Instt. Of Medical Sciences, New Delhi</h4>
            <article class="showme-main">
              <div class="main-testimonials">
                <div class="testimonials-box" style="padding:0px; background:none;">
                  <div class="testimonials-content">I was a great experience to be a part of a National Quiz. It was fun too.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Devanshu Bansal">Devanshu Bansal</a> <span>AIIMS</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content">Good opportunity to know where you stand and learn new questions. Also a fun experience.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Dr. Shweta Shubhdarshini">Dr. Shweta Shubhdarshini</a> <span>AIIMS</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content"> A quiz on a national level with great exposure to all types of questions. Also competing with students all around India helps in building a strong base. </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Devansh Yadav">Devansh Yadav</a> <span>AIIMS</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content">It is very good platform for medical students to judge their capability.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Sumitabh Singh">Sumitabh Singh</a> <span>AIIMS</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content">It was a good, thought provoking questionnaire.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Devika Kir">Devika Kir</a> <span>AIIMS</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content">I think it's a good platform for UG students preparing for various competitive exams to get to know where they stand and to compete among students selected from all the colleges. Quizzing is always fun!!</div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Anurag Chahal">Anurag Chahal</a> <span>AIIMS</span> </div>
                </div>
              </div>
            </article>
            <h4>Testimonials Gry Matter Principants - Season-1 vardhman Mahavir Medical College, New Delhi</h4>
            <article class="showme-main">
              <div class="main-testimonials">
                <div class="testimonials-box" style="padding:0px; background:none;">
                  <div class="testimonials-content">An innovative approach to find out among doctors, better than the best brains with respect to knowledge.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Aman Chopra">Aman Chopra</a> <span>VMMC</span> </div>
                </div>
                <div class="testimonials-box">
                  <div class="testimonials-content">A good experience ! MCQs after long time. Atrue eye opener.
                    <p>&nbsp;</p>
                  </div>
                  <div class="testimonial-title"> <a href="javascript:void(0);" title="Abhishek Mittal">Abhishek Mittal</a> <span>VMMC</span> </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'national-quiz-accord-second.php'; ?>
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>