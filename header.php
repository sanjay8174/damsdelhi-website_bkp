<?php
error_reporting(0);
if(isset($_REQUEST['c'])){
    $course_id = $_REQUEST['c'];
}
require("config/autoloader.php");
include_once 'config/constant.php';
Logger::configure('config/log4php.xml');
$Dao = new dao();
$getCourse = $Dao->getCourse();
$getAboutus = $Dao->getAboutus();
?>
<div class="top-header">
<ol class="top-nav">
<li><a href="https://cbt.damsdelhi.com/" target="_blank" class="link">DAMS-CBT</a></li>
<!--<li><a href="<?php echo Constant::$path ?>/mdscbt2015" target="_blank" class="link">DAMS MDS-CBT</a></li>-->
<li><a  href="https://dvt.damsdelhi.com/" target="_blank" class="link">DAMS-DVT</a></li>
<li><a href="contact.php" title="Contact us">Contact us</a></li>
<li><a href="<?php echo Constant::$path ?>/studentInfo.php" title="Student Info">Student Info</a></li>
<li><a href="<?php echo Constant::$path ?>/franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="https://blog.damsdelhi.com/" target="_blank" title="Blog">Blog</a></li>
<li><a href="<?php echo Constant::$path ?>/career.php" title="Career">Career</a></li>
<li><a href="next.php" class="link">Next<sup style="color:green;vertical-align: top;">New</sup></a></li>
<li><a href="<?php echo Constant::$path ?>/download.php" title="Download">Download</a></li>
<li class="bg_none"><a href='<?php echo Constant::$path; ?>' title="Home">Home</a></li>
</ol>
</div>
<header>
<div class="wrapper">
<h1><a href='<?php echo Constant::$path; ?>' class="mob_logo" title="Delhi Academy of Medical Sciences"><img src="images/logo_mobile.jpg" alt="DAMS Logo" /></a></h1>
<div class="top_nav_bg">
</div>
<aside class="t-right-side">
<div class="right-top">
<?php include 'addCart.php'; ?>
<div class="login-registration">
<a href="javascript:void(0)" class="l-r-left" id="student-login" title="Online / Offline Test login"><span></span>Online / Offline<br>Test Login</a>
<!--<a href="javascript:void(0)" class="l-r-middle" id="cloud-login" title="DAMS Cloud Student login"><span></span>DAMS<br>Cloud</a>-->
<a href="javascript:void(0)" class="l-r-middle" id="cloud-login" title="DAMS Cloud Student login" style="display:none"><span></span>DAMS<br>Cloud</a>
<a href="javascript:void(0)" class="l-r-right" id="student-registration" title="New Student Registration"><span></span>New&nbsp;Student<br>Registration</a>
</div>
<div class="some-numbers">
<div class="helpline-no">Helpline&nbsp;no: <span>011-40094009</span></div>
<div class="sms-enquiry display_none">SMS Enquiry: <span>sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span></div>
</div>
</div>
<nav class="right-bottom">
    <ul>
        <li><a href="javascript:void(0)" title="About Us"><span class="about-ico"></span>About Us</a>
            <div class="profilePop">
                <div class="nav-popup" style="width: 235px;">
                    <span class="top-arrow"></span>
                    <ul>
                        <?php 
                        for($i=0;  $i<sizeof($getAboutus); $i++){ ?>
                        <li>
                            <a href="<?php echo $getAboutus[$i][1];?>?a=<?php echo $getAboutus[$i][0];?>" title="<?php echo $getAboutus[$i][2];?>" class="br-top-none"><span class="dropdown-arrow"></span><?php echo $getAboutus[$i][2];?></a>
                        </li>
                        <?php }  ?>
                    </ul>
                    
                    
                    
                </div>
            </div>
        </li>
        <li><a href="javascript:void(0)" title="Courses"><span class="course-ico"></span>Courses</a>
            <div class="profilePop">
                <div class="nav-popup">
                <span class="top-arrow"></span>
                    <ul>
                        <?php for($i=0;$i<sizeof($getCourse);$i++){
                                    $navId=$Dao->getNavId($getCourse[$i][0]);
                                        if($getCourse[$i][3]==1){
                        ?>
                        <li>
                            <a href="<?php echo $getCourse[$i][2];?>?c=<?php echo $getCourse[$i][0];?>&n=<?php echo $navId;?>" title="<?php echo $getCourse[$i][1];?>" <?php if($i==0){?>class="br-top-none"<?php }else{?>class=" "<?php }?>><span class="dropdown-arrow"></span><?php echo $getCourse[$i][1];?></a>
                        </li>
                        <?php } }?>
                    </ul>
                </div>
            </div>
        </li>
        <li><a href="https://www.damspublications.com/" target="_blank" title="DAMS Store"><span class="dams-ico"></span>DAMS Store</a>
<!--            <div class="profilePop">
            <div class="nav-popup">
            <span class="top-arrow"></span>
            <ul>
            <li>
            <a href="<?php echo Constant::$path?>/mdms_entrence/dams-publication.php?c=1" title="MD / MS Entrance" class="br-top-none"><span class="dropdown-arrow"></span>MD / MS Entrance</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/mci_screening/dams-publication.php?c=2" title="MCI Screening"><span class="dropdown-arrow"></span>MCI Screening</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/mds_quest/dams-publication.php?c=3" title="MDS Quest"><span class="dropdown-arrow"></span>MDS Quest</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/usmle_edge/dams-publication.php?c=4" title="USMLE Edge"><span class="dropdown-arrow"></span>USMLE Edge</a>
            </li>
            <li>
            <a title="Ebook Learning" target="_blank" href="https://ebooks.damsdelhi.com/store"><span class="dropdown-arrow"></span>Ebook Learning</a>
            </li>
            </ul>
            </div>
            </div>-->
        </li>
        <li><a href="javascript:void(0)" title="Find a Centre"><span class="find-ico"></span>Find a Centre</a>
            <div class="profilePop">
            <div class="nav-popup new-part">
            <span class="top-arrow"></span>
            <ul>
            <li>
            <a href="<?php echo Constant::$path?>/find-center.php?c=1" title="Face to Face Centres" class="br-top-none"><span class="dropdown-arrow"></span>Face to Face Centres</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/find-center.php?c=2" title="Satellite Centres"><span class="dropdown-arrow"></span>Satellite Centres</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/find-center.php?c=3" title="Test Centres"><span class="dropdown-arrow"></span>Test Centres</a>
            </li>
            <li>
            <a href="<?php echo Constant::$path?>/find-center.php?c=4" title="Dental Satellite Centers"><span class="dropdown-arrow"></span>Dental Satellite Centers</a>
            </li>
            </ul>
            </div>
            </div>
        </li>
    </ul>
</nav>
</aside>
<div class="r-screen-b">
<div class="demo-box-d mini-use">
<div class="r-navigation demo1-d" style="display:block;right:0">
<div class="r-navigation_button" id="menudrop"><span class="mob_menu">MENU</span></div>
</div>
<div class="demo3-d">
<div class="close-navigation demo1-d"> X </div>
<ul>
<li class="boder-none"><div class="modify-close">Close <span class="demo1-d">X</span></div></li>
<li><a href="index.php" title="Home">Home</a></li>
<li><a href="javascript:void(0)" title="About Us" onclick="menuslides(1)"><span>&nbsp;</span>About Us</a>
<ul class="inner-d-down" id="inul1" style="display:none">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="dams.php" title="About DAMS">About DAMS</a></li>
<li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
<li><a href="about_director.php" title="About Director">About Director</a></li>
<li><a href="mission_vision.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
</ul>
</li>
<li><a href="javascript:void(0)" title="Courses" onclick="menuslides(2)"><span>&nbsp;</span>Courses</a>
<ul class="inner-d-down" id="inul2" style="display:none">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="javascript:void(0)" title="MD / MS ENTRANCE" onclick="innermenuslides(1)"><span>&nbsp;</span>MD / MS ENTRANCE</a>
<ul class="extra-inner" id="innrul1" style="display:none">
<li><a href="https://mdms.damsdelhi.com/index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="https://mdms.damsdelhi.com/satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="https://mdms.damsdelhi.com/online-test-series.php" title="Test Series">Test Series</a></li>
<li><a href="https://mdms.damsdelhi.com/achievements.php?c=1" title="Achievement">Achievement</a></li>
<li><a href="https://mdms.damsdelhi.com/photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>
<li><a href="javascript:void(0)" title="MCI SCREENING" onclick="innermenuslides(2)"><span>&nbsp;</span>MCI SCREENING</a>
<ul class="extra-inner" id="innrul2" style="display:none">
<li><a href="https://mci.damsdelhi.com/index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="https://mci.damsdelhi.com/mci-satellite-classes.php" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="https://mci.damsdelhi.com/mci-test-series.php" title="Test Series">Test Series</a></li>
<li><a href="https://mci.damsdelhi.com/achievements.php?c=2" title="Achievement">Achievement</a></li>
<li><a href="https://mci.damsdelhi.com/mci-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>
<li><a href="javascript:void(0)" title="MDS QUEST" onclick="innermenuslides(3)"><span>&nbsp;</span>MDS QUEST</a>
<ul class="extra-inner" id="innrul3" style="display:none">
<li><a href="https://mds.damsdelhi.com/index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li><a href="https://mds.damsdelhi.com/mds-satellite-classes.php?c=3" title="Satellite Classes">Satellite Classes</a></li>
<li><a href="https://mds.damsdelhi.com/dams-mds-test-series.php" title="Test Series">Test Series</a></li>
<li><a href="https://mds.damsdelhi.com/achievements.php?c=3" title="Achievement">Achievement</a></li>
<li><a href="https://mds.damsdelhi.com/mds-photo-gallery.php" title="Virtual Tour">Virtual Tour</a></li>
</ul>
</li>
<li><a href="https://usmle.damsdelhi.com/index.php" title="USMLE EDGE">USMLE EDGE</a></li>
</ul>
</li>
<li><a href="javascript:void(0)" title="DAMS Store" onclick="menuslides(3)"><span>&nbsp;</span>DAMS Store </a>
<ul class="inner-d-down" id="inul3" style="display:none">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="dams-publication.php?c=1" title="MD / MS ENTRANCE">MD / MS ENTRANCE</a></li>
<li><a href="dams-publication.php?c=2" title="MCI SCREENING">MCI SCREENING</a></li>
<li><a href="dams-publication.php?c=3" title="MDS QUEST">MDS QUEST</a></li>
<li><a href="dams-publication.php?c=4" title="USMLE EDGE">USMLE EDGE</a></li>
</ul>
</li>
<li><a href="javascript:void(0)" title="Find a center" onclick="menuslides(4)"><span>&nbsp;</span>Find a center</a>
<ul class="inner-d-down" id="inul4" style="display:none">
<li><span class="respo-arrow">&nbsp;</span></li>
<li><a href="find-center.php?c=1" title="FACE TO FACE CENTRE">FACE TO FACE CENTRE</a></li>
<li><a href="find-center.php?c=2" title="SATELLITE CENTRE">SATELLITE CENTRE</a></li>
<li><a href="find-center.php?c=3" title="TEST CENTRE">TEST CENTRE</a></li>
<li><a href="find-center.php?c=4" title="DENTAL SATELLITE CENTRES">DENTAL SATELLITE CENTRES</a></li>

</ul>
</li>
<li><a href="download.php" title="Download">Download</a></li>
<li><a href="franchisee.php" title="Franchisee">Franchisee</a></li>
<li><a href="career.php" title="Career">Career</a></li>
<li><a title="Blog" href="https://blog.damsdelhi.com/">Blog</a></li>
<li><a href="contact.php" title="Contact us">Contact us</a></li>
<li>
<div class="responc-social-icon">
<a href="https://www.facebook.com/damsdelhiho" target="_blank" title="Connect with us on Facebook" class="responc-l-facebook"><span>&nbsp;</span> Follow us on Facebook</a>
<a href="https://twitter.com/damsdelhi" target="_blank" title="DAMS on Twitter" class="responc-l-twitter"><span>&nbsp;</span> Follow us on Twitter</a>
<a href="https://in.linkedin.com/pub/dams-delhi/40/415/570" target="_blank" title="DAMS on Linkedin" class="responc-l-mail"><span>&nbsp;</span> Follow us on Linkedin</a>
<a href="https://www.youtube.com/user/damsdelhi" target="_blank" title="Watch DAMS Videos" class="responc-l-print"><span>&nbsp;</span> Connect With Us</a>
<a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" title="Follow DAMS on Google+" class="responc-l-share"><span>&nbsp;</span> Follow us on Google Plus</a>
</div>
</li>
<li>
<div class="res-helpline-no">
<span class="res-cell">&nbsp;</span>
<div class="sms_enquiry">
Helpline&nbsp;no:
<span class="res-mini-no">011-40094009</span>
</div>
</div>
</li>
<li class="display_none">
<div class="responcive-sms-enquiry">
<span class="res-sms">&nbsp;</span>
<div class="helpline_inline">
SMS Enquiry: <span class="iner-res-sms">sms&nbsp;DAMSHO&nbsp;your&nbsp;name&nbsp;to&nbsp;56677</span>
</div>
</div>
</li>
</ul>
</div>
</div>
<div class="user-use">
<div class="demo-box-d"><a href="javascript:void(0)" class="l-r-left demo-2-d" title="Online Test Student login">
<span><b id="lightbox-image-details-caption">Log in</b></span></a></div>
<div class="demo6-d">
<div class="close-navigation demo-2-d">X</div>
<ul>
<li class="boder-none"><div class="modify-close">Close <span class="demo-2-d">X</span></div></li>
<li id="loginentry"><a href="javascript:void(0)" title="Online/Offline Test Login" class="active-res" id="login1" onclick="loginslides(1)">
<span>&nbsp;</span>Online/Offline Test Login</a>
<form id="loginFormnewresp" class="form" method="post" action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=submit" name="loginFormnewresp" onsubmit="return loginSubmit_resp()">
<div class="responcive-login-box" id="indiv1">
<div class="responcive-login-content">
<div class="responcive-career-box">
    <div class="responcive-right-ip-1" style="display: none;">
<select name="sessionY" class="career-inp" style="padding-left: 11px; width: 99.5%;">
    <option value="0">2016</option>
    <option value="1">2015</option>
</select>    
</div>
<div class="responcive-right-ip-1">
<input name="email" id="emailLogin-resp" type="text" class="career-inp-1" placeholder="Roll No. / Email" />
</div>
<span id="loginemailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="pass" id="passLogin-resp" type="password" class="career-inp-1" placeholder="Password" /></div>
<span id="loginpasserror-resp" class="errormsg" style="display:none">Please enter your Password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1">
<div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div>
<div class="responcive-fgpassword"><a href="javascript:void(0)" id="fg-password-resp" title="Forgot Password">Forgot Password ?</a></div>
</div></div>
<div class="responcive-career-box">
<div class="error_msg_box" id="loginresError1" style="width:97%;margin-bottom:-15px;margin-top:10px;display:none">Please enter correct.</div>
</div>
</div>
</div>
</form>
</li>
<li id="forgtpasswd" style="display:none">
<a href="javascript:void(0)" title="Forgot Password" class="active-res" id="login3" onclick="loginslides(3)">
<span>&nbsp;</span>Forgot Password
</a>
<form action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=fgpassword" id="forgetpassformresp" class="form" method="post" name="forgetpassformresp" onsubmit="return forgetPasswordresp()">
<div class="responcive-login-box" id="indiv3">
<div class="responcive-login-content">
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="emaiID" id="fgemail-resp" type="text" class="career-inp-1" placeholder="Email Address" /></div>
<span id="fgemailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-right-ip-1"><div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div></div>
<div class="responcive-career-box"><div class="error_msg_box error_inline2" id="fgerror1">Please enter correct.</div></div>
</div>
</div>
</form>
</li>
<li><a href="javascript:void(0)" title="New Registration" id="login2" onclick="loginslides(2)"><span>&nbsp;</span>New Registration</a>
<form name="RegistrationFormresp" id="RegistrationFormresp" action="https://<?php echo Constant::$loginLink ?>/index.php?pageName=inRegistration" method="post" onsubmit="return Submit_form_newReg_resp()">
<div class="responcive-login-box" id="indiv2" style="display:none">
<div class="responcive-login-content">
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="email" id="email-resp" type="text" class="career-inp-1" placeholder="Email Address" /></div>
<span id="emailerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="password" id="password-resp" type="password" class="career-inp-1" placeholder="Password" /></div>
<span id="passworderror-resp" class="errormsg" style="display:none">Please enter your password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="Cnpassword" ID="Cnpassword-resp" type="password" class="career-inp-1" placeholder="Confirm Password" />
</div><span id="Cnpassworderror-resp" class="errormsg" style="display:none">Please enter confirm password</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="Stuidentname" ID="Stuidentname-resp" type="text" class="career-inp-1" placeholder="Name" /></div>
<span id="Stuidentnameerror-resp" class="errormsg" style="display:none">Please enter your name</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><input name="mobile" id="mobile-resp" type="text" class="career-inp-1" placeholder="Mobile Number" /></div>
<span id="mobileerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1">
<select name="course_Data" id="Course-resp" class="career-select-input-1">
<option value="0">Select Course</option>
<?php $sql=mysql_query("SELECT COURSE_ID,COURSE_NAME FROM COURSE");
while($row= mysql_fetch_array($sql)){?>
<option value="<?php echo $row['COURSE_ID']; ?>">
<?php  echo urldecode($row['COURSE_NAME']);?>
</option>
<?php } ?>
</select>
</div><span id="Courseerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1"><select name="state" id="state-resp" class="career-select-input-1" onchange="stateChangeResp(this.value)">
<option value="0">Select State</option>
<?php $sql=mysql_query("SELECT STATE_ID,STATE_NAME FROM STATE");
while($row= mysql_fetch_array($sql)){?>
<option value="<?php echo $row['STATE_ID']; ?>">
<?php  echo urldecode($row['STATE_NAME']);?>
</option>
<?php } ?>
</select>
</div><span id="stateerror-resp" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box">
<div class="responcive-right-ip-1" id="citydiv-resp">
<select name="city" id="city-resp" class="career-select-input-1">
<option value="0">Select City</option>
</select></div>
<span id="cityerror" class="errormsg" style="display:none">Please enter your email address</span></div>
<div class="responcive-career-box"><div class="responcive-right-ip-1"><div class="responcive-submit-enquiry"><input type="submit" value="Submit" title="Submit" /></div></div></div>
<div class="responcive-career-box"><div class="error_msg_box error_inline1" id="regresError1">Please enter correct.</div></div>
</div></div>
</form>
</li>
<li><a href="javascript:void(0)" title="DAMS Cloud Login" id="login" onclick="loginslides(4)"><span>&nbsp;</span>DAMS Cloud Login</a>
<div class="responcive-login-box" id="indiv4" style="display:none">
<div class="responcive-login-content">
<div class="responcive-career-box" id="iframebody2">
</div>
<div class="responcive-career-box"><div class="error_msg_box error_inline" id="loginError">Please enter correct.</div></div>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</header>
<?php include 'social-icon.php'; ?>
