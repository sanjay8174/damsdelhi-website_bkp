<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left banner-l-heading">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li id="accor1" class="border_none" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol1">
                  <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2013.php" title="MCI Screening September">MCI Screening September</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_mar_2013.php" title="MCI Screening March">MCI Screening March</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');" class="light-blue-inner"><span id="accorsp2" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_block" id="aol2">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2012.php" title="MCI Screening September">MCI Screening September</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>MCI Screening September 2012</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="MCI Screening September 2012">MCI Screening September 2012</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/KELA-RAKESH-KUMAR.jpg" alt="Dr. KELA RAKESH KUMAR" title="Dr. KELA RAKESH KUMAR" />
                        <p><span>Dr. KELA RAKESH KUMAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/DILIP-DANDOTIYA.jpg" alt="Dr. DILIP DANDOTIYA" title="Dr. DILIP DANDOTIYA" />
                        <p><span>Dr. DILIP DANDOTIYA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/INAM-ANSA.jpg" alt="Dr. INAM ANSA" title="Dr. INAM ANSA" />
                        <p><span>Dr. INAM ANSA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/GAURAW-PRAKASH-SAHAY.jpg" alt="Dr. GAURAW PRAKASH SAHAY" title="Dr. GAURAW PRAKASH SAHAY" />
                        <p><span>Dr. GAURAW PRAKASH SAHAY</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/NEHA-SINGH.jpg" alt="Dr. NEHA SINGH" title="Dr. NEHA SINGH" />
                        <p><span>Dr. NEHA SINGH</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/SONAM-VERMA.jpg" alt="Dr. SONAM VERMA" title="Dr. SONAM VERMA" />
                        <p><span>Dr. SONAM VERMA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/MOHAMMED-SHAH-ANAWAZ-AHMAD.jpg" alt="Dr. MOHAMMED SHAH ANAWAZ AHMAD" title="Dr. MOHAMMED SHAH ANAWAZ AHMAD" />
                        <p><span>Dr. MOHAMMED SHAH ANAWAZ AHMAD</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/PRIYANKA-SINGH.jpg" alt="Dr. PRIYANKA SINGH" title="Dr. PRIYANKA SINGH" />
                        <p><span>Dr. PRIYANKA SINGH</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/DESETTI-RAJEEV.jpg" alt="Dr. DESETTI RAJEEV" title="Dr. DESETTI RAJEEV" />
                        <p><span>Dr. DESETTI RAJEEV</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/YAIPHAREMBL.jpg" alt="Dr. YAIPHAREMBL" title="Dr. YAIPHAREMBL" />
                        <p><span>Dr. YAIPHAREMBL</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/RAJA-SEKHAR.jpg" alt="Dr. RAJA SEKHAR" title="Dr. RAJA SEKHAR" />
                        <p><span>Dr. RAJA SEKHAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/SUMIT-SEHGAL.jpg" alt="Dr. SUMIT SEHGAL" title="Dr. SUMIT SEHGAL" />
                        <p><span>Dr. SUMIT SEHGAL</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/RABINA-KHATOON.jpg" alt="Dr. RABINA KHATOON" title="Dr. RABINA KHATOON" />
                        <p><span>Dr. RABINA KHATOON</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/RASHAD-RAHIB.jpg" alt="Dr. RASHAD RAHIB" title="Dr. RASHAD RAHIB" />
                        <p><span>Dr. RASHAD RAHIB</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/BIBHASH-DATTA.jpg" alt="Dr. BIBHASH DATTA" title="Dr. BIBHASH DATTA" />
                        <p><span>Dr. BIBHASH DATTA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/NEHA-BAHRI.jpg" alt="Dr. NEHA BAHRI" title="Dr. NEHA BAHRI" />
                        <p><span>Dr. NEHA BAHRI</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/HRITIK-SHARMA.jpg" alt="Dr. HRITIK SHARMA" title="Dr. HRITIK SHARMA" />
                        <p><span>Dr. HRITIK SHARMA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/AABID-RASOOL-BHAT.jpg" alt="Dr. AABID RASOOL BHAT" title="Dr. AABID RASOOL BHAT" />
                        <p><span>Dr. AABID RASOOL BHAT</span></p>
                      </div>
                      <div class="students-box"></div>
                      <div class="students-box"></div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> <span class="mini-heading">MCI Screening September - Result 2012</span>
                    <div class="schedule-mini-top"> <span class="one-part">S. No.</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. BHUPENDRA SHARMA</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. DILEEP DANDOTIYA</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. INAM ANSA</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. KELA RAKESH KUMAR</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. NEHA SINGH</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. SONAM VERMA</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. RUP NARAIN</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. SOHAIL SHEIKH MOHAMMAD</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. SHRIDEVI GOPI</span> </li>
                        <li><span class="one-parts">10</span> <span class="two-parts schedule-left-line">Dr. PRIYANKA SINGH</span> </li>
                        <li><span class="one-parts">11</span> <span class="two-parts schedule-left-line">Dr. YAIPHAREMBI</span> </li>
                        <li><span class="one-parts">12</span> <span class="two-parts schedule-left-line">Dr. NITIN KRISHNA PATIL</span> </li>
                        <li><span class="one-parts">13</span> <span class="two-parts schedule-left-line">Dr. SUMIT SEHGAL</span> </li>
                        <li><span class="one-parts">14</span> <span class="two-parts schedule-left-line">Dr. HRITIK SHARMA</span> </li>
                        <li><span class="one-parts">15</span> <span class="two-parts schedule-left-line">Dr. AABID RASOOL BHAT</span> </li>
                        <li><span class="one-parts">16</span> <span class="two-parts schedule-left-line">Dr. NEHA BAHRI</span> </li>
                        <li><span class="one-parts">17</span> <span class="two-parts schedule-left-line">Dr. RISHI SRIVASTAVA</span> </li>
                        <li><span class="one-parts">18</span> <span class="two-parts schedule-left-line">Dr. RAJA SEKHAR</span> </li>
                        <li><span class="one-parts">19</span> <span class="two-parts schedule-left-line">Dr. MOHAMMED SHAHANAWAZ AHMAD</span> </li>
                        <li><span class="one-parts">20</span> <span class="two-parts schedule-left-line">Dr. RASHAD RAHIB</span> </li>
                        <li><span class="one-parts">21</span> <span class="two-parts schedule-left-line">Dr. BIBHASH DATTA</span> </li>
                        <li><span class="one-parts">22</span> <span class="two-parts schedule-left-line">Dr. DESETTI RAJEEV</span> </li>
                        <li><span class="one-parts">23</span> <span class="two-parts schedule-left-line">Dr. RABINA KHATOON</span> </li>
                        <li><span class="one-parts">24</span> <span class="two-parts schedule-left-line">Dr. GAURAW PRAKASH SAHAY</span> </li>
                        <li><span class="one-parts">25</span> <span class="two-parts schedule-left-line">Dr. SHADAB FASIH KHAN</span> </li>
                        <li><span class="one-parts">26</span> <span class="two-parts schedule-left-line">Dr. S.ADITHYA</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/cC2a-En6XK4?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Sonam Verma</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/VK0ol9h2h8c?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rup Narayan Jha</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/YJaxKtFiAMA?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rashad Rahib</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/zvRSvxwOfwc?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rakesh Kela</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>