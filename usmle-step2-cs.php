<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, USMLE EDGE</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<?php include 'enquiry.php'; ?>

<?php include 'coures-header.php'; ?>



<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article class="usmle-edge-banner">

<aside class="banner-left">
<h2>USMLE EDGE</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>


<?php include'usmle-banner-btn.php'; ?>
</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li style="background:none;"><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
<li><a title="USMLE Step 2 Cs" class="active-link">USMLE Step 2 Cs</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>USMLE Step-2 CS</h4>
<article class="showme-main">
<div class="idams-content">

<ul class="dnb-list">
<h5>STEP 2 CS (CLINICAL SKILL) PART-II</h5>
<li><span>&nbsp;</span>LIVE TRAINING PROGRAM.</li>
<li><span>&nbsp;</span>3 DAYS PROGRAM.</li>
<li><span>&nbsp;</span>WEEKEND CLASSES.</li>
<li><span>&nbsp;</span>3-4 HRS SESSION.</li>
<li><span>&nbsp;</span> SINGLE DAY MOCK TEST.</li>
<li>BY AUTHOR OF POPULAR DVD’s USMLE EXPERTS ECFMG CERTIFIED</li>
</ul>

<div class="franchisee-box">
<span>TESTIMONIALS</span>
<p>I had the pleasure of attending Dr. Rohan Khandelwal's 3 day CS course at DAMS Delhi and it was because of his excellent training that I cleared the exam in the first attempt. The way he conducts his course is ideal for IMG's who like me are not well versed with Communication and Interpersonal Skills. His DVD and the tips which he gives during the sessions act as ideal preparation material for the exam. I would recommend this course to all students. Thank you for helping me out sir!</p>
<p><em>Dr. Gurmeet</em><br />
<b>Punjab</b></p>
<p>&nbsp;</p>

 
<p>I took my USMLE Step 2 CS exam in January and before appearing for the exam I appeared for a single day mock test at DAMS. The test was conducted by Dr. Rohan, whose vast experience in CS proved very useful for me during my exam. He discussed each case with me at the end of the test and showed me the mistakes which I had made by playing back the videos which he had recorded during the encounter. The skills lab at DAMS had excellent facilities and the room resembled the rooms which I had to the actual exam. It was a wonderful experience, which was made better by the fact that I cleared my exam. Thank you DAMS and Dr. Rohan.</p>
<p><em>Dr. Hussain</em><br />
<b>Delhi</b></p>
</div>


<div class="idams-right-video">
<iframe width="100%" height="300" src="//www.youtube.com/embed/EfqCkxgIDXg" frameborder="0" allowfullscreen></iframe>
</div>



</div>


</article>
</div>

</aside>

<aside class="gallery-right">

<div id="store-wrapper">
<div class="dams-store-link"><span></span>DAMS USMLE Edge</div>
<div class="dams-store-content">
<div class="inner-store">

<ul>
<li style="border:none;"><a href="usml-step1.php" title="Step1">Step1</a></li>
<li><a href="usml-step2.php" title="Step2">Step2</a></li>
<li><a href="usmle-step2-cs.php" title="Step2 CS" class="active-store">Step2 CS</a></li>
<li><a href="usmle-step3-cs.php" title="Step3">Step3</a></li>
</ul>
</div>
</div>

</div>

<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->

</aside>

</section>

</div>
 
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->

</body></html>